<?php

class Controller {
	
	const SIDDILLONTITLE = " | Sid Dillon Referrals";
    
    public function __construct() {
        $this -> view = new View();
		$this -> json = new JSONparse();
		$this -> pages = new Pages();
		$this-> recordedTime = new Time();
		$this -> email = new Email();
		$this -> time = new Time();
		$this -> redirect = new Redirect();
    }
	
    /**
     * 
     * @param string $name Name of the model
     * @param string $path Location of the models
     */
    public function loadModel($name, $modelPath = 'models/') {
        
        $path = $modelPath . $name.'_model.php';
        
        if (file_exists($path)) {
            require $modelPath .$name.'_model.php';
            
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }        
    }
	
	public function MetaData() {
		return json_encode(array('SchemaMetaData' => array("Name" => "", 
														   "Description" => "",
														   "ImageURL" => ""),
								 'TwitterMetaData' => array("Title" => "", 
															"Description" => "",
															"URL" => "",
															"ImageURL" => ""),
								 'OpenGraphMetaData' => array("Title" => "", 
															  "Type" => "",
															  "URL" => "",
															  "Image" => "",
															  "Description" => "",
															  "ArticlePublishTime" => "",
															  "ArticleModifiedTime" => "",
															  "ArticleSection" => "",
															  "ArticleTag" => ""),
								 'StructuredData' => array("Description" => "")));
		
		
	}
	

}