<?php 

class View {
	
	
	public function __construct() {
		$this-> recordedTime = new Time();
		$this -> form = new Form();
		$this -> visible = New Permissions();
		$this -> pages = new Pages();
		$this -> pagination = new Pagination();
	}
	
	public function render($name, $noInclude = false) {

		if ($noInclude == true) {
			require 'view/' . $name . '.php';
		} else {
			require 'view/header.php';
			require 'view/' . $name . '.php';
			require 'view/footer.php';
		}
		
	}
	
	public function tooltip($msg) {
		return "<a href='javascript:void(0)' data-toggle='tooltip' data-placement='right' title='" . $msg . "' ><img src='" . PATH . "public/images/ToolTipHelp.png' /></a>";
	}
	
	public function states() {
		return array('AL'=>'Alabama',
					 'AK'=>'Alaska', 
					 'AR'=>'Arkansas',
					 'AZ'=>'Arizona',
					 'CA'=>'California',
					 'CO'=>'Colorado',
					 'CT'=>'Connecticut',
					 'DE'=>'Delaware', 
					 'DC'=>'District of Columbia',
					 'FL'=>'Florida', 
					 'GA'=>'Georgia',
					 'HI'=>'Hawaii',
					 'IA'=>'Iowa',  
					 'ID'=>'Idaho',
					 'IL'=>'Illinois',
					 'IN'=>'Indiana', 
					 'KS'=>'Kansas', 
					 'KY'=>'Kentucky',
					 'LA'=>'Louisiana',
					 'ME'=>'Maine', 
					 'MD'=>'Maryland', 
					 'MA'=>'Massachusetts', 
					 'MI'=>'Michigan', 
					 'MN'=>'Minnesota', 
					 'MO'=>'Missouri',
					 'MS'=>'Mississippi', 
					 'MT'=>'Montana',
					 'NE'=>'Nebraska', 
					 'NM'=>'New Mexico', 
					 'NV'=>'Nevada',
					 'NH'=>'New Hampshire', 
					 'NJ'=>'New Jersey', 
					 'NY'=>'New York', 
					 'NC'=>'North Carolina', 
					 'ND'=>'North Dakota',
					 'OH'=>'Ohio', 
					 'OK'=>'Oklahoma', 
					 'OR'=>'Oregon', 
					 'PA'=>'Pennsylvania',
					 'RI'=>'Rhode Island', 
					 'SC'=>'South Carolina',
					 'SD'=>'South Dakota',
					 'TX'=>'Texas', 
					 'TN'=>'Tennessee',
					 'UT'=>'Utah',  
					 'VT'=>'Vermont', 
					 'VA'=>'Virginia',
					 'WA'=>'Washington', 
					 'WV'=>'West Virginia',
					 'WI'=>'Wisconsin',
					 'WY'=>'Wyoming');
	}
	
	public function Departments() {
				return array(''=>'Please Select',
							'Management' => 'Management',
							'Sales' => 'Sales',
							'Parts' => 'Parts',
							'Service' => 'Service',
							'Finance' => 'Finance',
							'Internet' => 'Internet',
							'Admin & Office' => 'Admin & Office',
							'Accessories & Apparel' => 'Accessories & Apparel');
				
				
	}
	
	public function CurrentWebsiteLinks() {
		return array('' => "Home Page",
					 'brand' => "Brands We Carry",
					 'inventory' => "Main Unit Inventory",
					 'apparel' => 'Apparel Department',
					 'service' => 'Service Department',
					 'parts' => 'Parts Department',
					 'shop' => 'Shopping Cart',
					 'specials' => 'Specials',
					 'blog' => 'Blog',
					 'events' => 'Events Page',
					 'photos' => 'Photos Page',
					 'newsletters' => 'NewsLetter Page',
					 'rewards' => 'VIB Rewards Page',
					 'contact' => 'Contact Page');
	}

	public function SubDomains() {
		return array('http://learntoride.dillon-brothers.com/' => "Learn to Ride");
		
	}
	
	public function Stores() {
		return array('2'=>'Dillon Motor Sports',
					 '3' => 'Dillon Harley',
					 '4' => 'Dillon Indian');
	}
	
	public function VehicleCategories() {
		return array('Motorcyle' => 'Motorcyle',
					 'ATV' => 'ATV',
					 'Side By Side' => 'Side By Side',
					 'Scooters' => 'Scooters',
					 'Watercraft' => 'Watercraft',
					 'Other' => 'Other');
	}
	
	public function SpecialTypes() {
		return array('' => 'Please Select Percentage/Dollar off/Special Financing',
					 'Percentage Off' => 'Percentage Off',
					 'Dollar Off' => 'Dollar Off',
					 'Special Financing' => 'Special Financing');
	}
	
	public function MetaDataForm($metaData = NULL) {
		$structuredDataDescription = NULL;	
		$SchemaMetaData = array('Name' => '',
								'ImageURL' => '',
								'Description' => '');
		$TwitterMetaData = array('Title' => '',
								 'Description' => '',
								 'URL' => '',
								 'ImageURL' => '');
		$OpenGraphMetaData = array('Title' => '',
								   'Type' => '',
								   'Description' => '',
								   'URL' => '',
								   'Image' => '',
								   'ArticleSection' => '',
								   'ArticleTag' => '');
			
		if(isset($metaData)) {
			$MetaDataDecoded = json_decode($metaData, true);
			$structuredDataDescription = $MetaDataDecoded['StructuredData']['Description'];
			
			$SchemaMetaData['Name'] = $MetaDataDecoded['SchemaMetaData']['Name'];
			$SchemaMetaData['ImageURL'] = $MetaDataDecoded['SchemaMetaData']['ImageURL'];
			$SchemaMetaData['Description'] = $MetaDataDecoded['SchemaMetaData']['Description'];
			
			$TwitterMetaData['Title'] = $MetaDataDecoded['TwitterMetaData']['Title'];
			$TwitterMetaData['Description'] = $MetaDataDecoded['TwitterMetaData']['Description'];
			$TwitterMetaData['URL'] = $MetaDataDecoded['TwitterMetaData']['URL'];
			$TwitterMetaData['ImageURL'] = $MetaDataDecoded['TwitterMetaData']['ImageURL'];
			
			$OpenGraphMetaData['Title'] = $MetaDataDecoded['OpenGraphMetaData']['Title'];
			$OpenGraphMetaData['Type'] = $MetaDataDecoded['OpenGraphMetaData']['Type'];
			$OpenGraphMetaData['Description'] = $MetaDataDecoded['OpenGraphMetaData']['Description'];
			$OpenGraphMetaData['URL'] = $MetaDataDecoded['OpenGraphMetaData']['URL'];
			$OpenGraphMetaData['Image'] = $MetaDataDecoded['OpenGraphMetaData']['Image'];
			$OpenGraphMetaData['ArticleSection'] = $MetaDataDecoded['OpenGraphMetaData']['ArticleSection'];
			$OpenGraphMetaData['ArticleTag'] = $MetaDataDecoded['OpenGraphMetaData']['ArticleTag'];
		}	
			
									
		return 	"<input type='hidden' name='MetaDataContent' id='MetaDataString' value='" .$metaData . "' />		
				<div class='row'>
					<div class='col-md-12'>
						<div class='contentSections'>
							<div class='subHeader'>Meta Data</div>
							<div class='subSectionHeader'>Structured Data</div>
						</div>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-12'>" . $this -> form -> Input("text", "Description", "structuredDataDescription", NULL, $structuredDataDescription, 'StructuredDataDescription', 160, "Max length is 160 Characters") . "</div>
				</div>
				<div class='row'><div class='col-md-12'><div class='subSectionHeader'>Schema.org</div></div></div>
				<div class='row'>
					<div class='col-md-6'>" . $this -> form -> Input("text", "Name", "schemaName", NULL, $SchemaMetaData['Name'], 'SchemaOrgName', 60, "Max length is 60 Characters"). "</div>
					<div class='col-md-6'>" . $this -> form -> Input("text", "ImageUrl", "schemaImageUrl", NULL, $SchemaMetaData['ImageURL']). "</div>
				</div>
				<div class='row'><div class='col-md-12'>" . $this -> form -> Input("text", "Description", "schemaDescription", NULL, $SchemaMetaData['Description'], 'SchemaOrgDescription', 160, "Max length is 160 Characters") . "</div></div>
				<div class='row'><div class='col-md-12'><div class='subSectionHeader'>Twitter</div></div></div>
				<div class='row'>
					<div class='col-md-6'>" . $this -> form -> Input("text", "Title", "twitterMetaTagTitle", NULL, $TwitterMetaData['Title'], 'TwitterTitle', 60, "Max length is 60 Characters") . "</div>
					<div class='col-md-6'>" . $this -> form -> Input("text", "URL", "twitterMetaTagURL", NULL, $TwitterMetaData['URL']) . "</div>
				</div>
				<div class='row'>
					<div class='col-md-12'>" . $this -> form -> Input("text", "Description", "twitterMetaTagDescription", NULL, $TwitterMetaData['Description'], 'TwitterDescription', 160, "Max length is 160 Characters") . "</div>
				</div>
				<div class='row'>
					<div class='col-md-6'>" . $this -> form -> Input("text", "ImageURL", "twitterMetaTagImageURL", NULL, $TwitterMetaData['ImageURL']) . "</div>
				</div>
				<div class='row'><div class='col-md-12'><div class='subSectionHeader'>Open Graph</div></div></div>
				<div class='row'>
					<div class='col-md-6'>" . $this -> form -> Input("text", "Title", "openGraphMetaTagTitle", NULL, $OpenGraphMetaData['Title'], 'OpenGraphTitle', 60, "Max length is 60 Characters") . "</div>
					<div class='col-md-6'>" . $this -> form -> Input("text", "type", "openGraphMetaTagType", NULL, $OpenGraphMetaData['Type'], 'OpenGraphType') . "</div>
				</div>
				<div class='row'>
					<div class='col-md-12'>" . $this -> form -> Input("text", "Description", "openGraphMetaTagDescription", NULL, $OpenGraphMetaData['Description'], 'OpenGraphDescription', 160, "Max length is 160 Characters") . "</div>
				</div>
				<div class='row'>
					<div class='col-md-6'>" . $this -> form -> Input("text", "URL", "openGraphMetaTagURL", NULL, $OpenGraphMetaData['URL']) . "</div>
					<div class='col-md-6'>" . $this -> form -> Input("text", "Image", "openGraphMetaTagImage", NULL, $OpenGraphMetaData['Image']) . "</div>
				</div>
				<div class='row'>
					<div class='col-md-6'>" . $this -> form -> Input("text", "Article Section", "openGraphMetaTagArticleSection", NULL, $OpenGraphMetaData['ArticleSection']) . "</div>
					<div class='col-md-6'>" . $this -> form -> Input("text", "Article Tag", "openGraphMetaTagArticleTag", NULL, $OpenGraphMetaData['ArticleTag']) . "</div>
				</div>";
	}
	
	public function PopupReadOnly($title, $closePopup) {
		return "<a href='javascript:void(0);' onclick='" . $closePopup . "'>
					<div class='Overlay'></div>
				</a>
				<div class='Popup'>
					<div class='formHeader'>"
						. $title. "			
						<a href='javascript:void(0);' onclick='" . $closePopup . "'>
							<div class='close' style='margin-top: 4px;'>
								<i class='fa fa-times' aria-hidden='true'></i>
							</div>						
						</a>	
					</div>
					<div class='FormContent' style='margin-right: 15px;'>
					</div>
				</div>";
	}
	
	public function PhoneFormat($phone) {
		$areaCode = substr($phone, 0, 3);
		$RestOfPhone1 = substr($phone, 3, 3);
		$RestOfPhone2 = substr($phone, 6, 7); 
		return "(" .$areaCode . ") ". $RestOfPhone1 . "-" . $RestOfPhone2;
	}
	
	
	
}