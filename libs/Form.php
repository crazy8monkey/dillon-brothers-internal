<?php
Class Form {
	
	public function ValiationMessage($idName, $loadingID, $messagesID) {
		return "<div class='flashMesssages' id='" .$idName ."'>
					<div class='loadingIndicator' id='" . $loadingID . "'><img src='" . PATH . "public/images/ajax-loader.gif' /></div>
					<div class='messagesText' id='" . $messagesID . "'></div>
				</div>";			
	}
	
	public function tooltip($msg) {
		return "<a href='javascript:void(0)' data-toggle='tooltip' data-placement='right' title='" . $msg . "' ><img src='" . PATH . "public/images/ToolTipHelp.png' /></a>";
	}
	
	
	public function Input($type, $label, $name, $ID = NULL, $value = NULL, $MetaDataUpdate = NULL, $maxLength = NULL, $toolTip = NULL) {
		$valueString = NULL;	
		$onChangeString = NULL;
		$onKeyUpString = NULL;
		$maxLengthString = NULL;
		$tooltipText = NULL;
		
		if(isset($value)) {
			$valueString = "value='" . $value. "'";
		}	
			
		if(isset($ID)) {
			$onChangeString = "onchange='Globals.removeValidationMessage(". $ID . ")'";
		}	
		
		if(isset($maxLength)) {
			$maxLengthString = " maxlength='" . $maxLength . "'";
		}
		
		if(isset($toolTip)) {
			$tooltipText = "<span style='margin-left:5px;'>". $this -> tooltip($toolTip) . "</span>";
		}
		
		if(isset($MetaDataUpdate)) {
			$method = "Globals.UpdateMetaString('" . $MetaDataUpdate . "', this)";
			
			$onKeyUpString = ' onkeyup="' . $method . '"';
		}
			return "<div class='input'>
					<div class='inputLabel'>". $label . $tooltipText. "</div>
					<input id='". $name. "' class='". $name. "' type='". $type. "' name='". $name. "'" . $valueString . $onChangeString . $onKeyUpString . $maxLengthString. " />
				</div>";
			
	}
	
	public function select($label, $name, $options = array(), $ID = NULL, $selectedValue = NULL) {
		$onChangeString = NULL;	
		$optionHTML = "<option value=''></option>";	
		foreach($options as $key => $value) {
			$selectedText = NULL;	
			if(isset($selectedValue)) {
				if($selectedValue == $key) {
					$selectedText = 'selected';	
				}
			}
			$optionHTML .= "<option value='" . $key . "' ". $selectedText . ">" . $value . "</option>";	
		}	
		
		if(isset($ID)) {
			$onChangeString = "onchange='Globals.removeValidationMessage(". $ID . ")'";
		}	
			
		return "<div class='input'>
					<div class='inputLabel'>". $label . "</div>
					<select " . $onChangeString . " name='" . $name . "'id='" . $name . "'>" . $optionHTML . "</select>
				</div>";
	}
	
	public function textarea($label, $name, $ID = NULL, $value = NULL) {
		$onChangeString = NULL;	
		$valueChange = NULL;	
			
		if(isset($ID)) {
			$onChangeString = "onchange='Globals.removeValidationMessage(". $ID . ")'";
		}	
		
		if(isset($value)) {
			$valueChange = $value;
		}
		
		return "<div class='input'>
					<div class='inputLabel'>". $label . "</div>
					<textarea id='" . $name . "' name='" . $name ."' " . $onChangeString . ">" . $valueChange ."</textarea>
				</div>";
	}
	
	public function radio($label, $name, $radio = array(), $ID = NULL) {
		$radioInput = NULL;	
		$onChangeString = NULL;	
		$checked = array();
		
		if(isset($ID)) {
			$onChangeString = "onchange='Globals.removeValidationMessage(". $ID . ")'";
		}
		
		foreach($radio as $key => $value) {
			
			
			
			
			
			$radioInput .= "<div style='margin-bottom:3px;'><input type='radio' name='" .$name . "' value='" . $key . "' style='float:left; margin-right: 2px;'" . $onChangeString . ">" . $value . "</div>";
		}
			
		//
		return "<div class='input'>
					<div class='inputLabel'>". $label . "</div>" . $radioInput . "
				</div>";
		
	}
	
	
	public function checkboxes($label, $name, $radio = array(), $ID = NULL, $selectedCheckboxes = NULL) {
		$radioInput = NULL;	
		$onChangeString = NULL;	
		
		if(isset($ID)) {
			$onChangeString = "onchange='Globals.removeValidationMessage(". $ID . ")'";
		}
		
		foreach($radio as $key => $value) {
			
			$checkedString = NULL;
			if(isset($selectedCheckboxes)) {
				$checked = $selectedCheckboxes;
				foreach($checked as $checkbox) {
					switch($name) {
						case "store[]":
							if($key == $checkbox['RelatedStoreID']) {
								$checkedString = "checked";
							}
							break;
						case "blogcategory[]":
							if($value['blogPostCategoryID'] == $checkbox['BlogCategoryID']) {
								$checkedString = "checked";
							}
							break;
					}
					
						
						
										
					
				}
			}
			
			switch($name) {
				case "store[]":
					$radioInput .= "<div style='margin-bottom:3px;'><input type='checkbox' name='" .$name . "' value='" . $key . "' style='float:left; margin-right: 2px;'" . $onChangeString . " " . $checkedString .">" . $value . "</div>";	
					break;
				
				case "blogcategory[]";
					$radioInput .= "<div style='margin-bottom:3px;'><input type='checkbox' name='" .$name . "' value='" . $value['blogPostCategoryID'] . "' style='float:left; margin-right: 2px;'" . $onChangeString . " " . $checkedString .">" . $value['blogCategoryName'] . "</div>";
					break;
			}
			
			
			
			
		}
			
		//
		return "<div class='input'>
					<div class='inputLabel'>". $label . "</div>" . $radioInput . "
				</div>";
		
	}

	public function checkbox($label, $name, $selectedCheckbox = false, $value = false, $ID = NULL, $notArray = false) {
		$radioInput = NULL;	
		$checkedString = NULL;	
		$valueString = NULL;
		$onChangeString = NULL;	
		$inputHidden = NULL;
		
		if($selectedCheckbox == true) {
			$checkedString = "checked";	
		}
				
		if($value == true) {
			$valueString = "value='" . $value . "'";
		}
		
		if(isset($ID)) {
			$onChangeString = "onchange='Globals.removeValidationMessage(". $ID . ")'";
		}
		
		if($notArray == true) {
			$inputHidden = "<input type='hidden' name='" .$name . "' value='0' />";
		}
		
		$radioInput .= "<div style='margin-bottom:3px;'>" .$inputHidden . "<input type='checkbox' ". $onChangeString . " name='" .$name . "'" . $valueString . " style='float:left; margin-right: 2px;'" . $checkedString . ">" . $label . "</div>";	
		
		return "<div class='inputCheckbox'>" . $radioInput . "</div>";
		
	}
	
	
	public function Submit($value, $className, $updateModifiedTime = false) {
		$onclickString = NULL;	
		if($updateModifiedTime == true) {
			$onclickString = "onclick='Globals.MetaDataUpdateModifiedTime()'";
		}
		return "<div class='input'>
					<input type='submit' value='" . $value. "' class='". $className . "' " . $onclickString . "/>
				</div>";
	}
	
	public function FileInput($label = NULL, $name) {
		$FullInput = NULL;	
		if($label != NULL) {
			$FullInput = "<div class='input'><div class='inputLabel'>". $label . "</div><input type='file' name='" . $name ."' /></div>";
		} else {
			$FullInput = "<input type='file' name='" . $name ."' />";
		}
		
		return $FullInput;
	}

}
?>