<?php
Class Redirect {
	
	public function redirectPage($path) {
		return header('location:' . $path);
	}
	
	public function Redirect301($path) {
		return header('location:' . $path, true, 301);
	}
	
	public function Redirect410($path) {
		header($_SERVER["SERVER_PROTOCOL"]." 410 Gone"); 
    	header("Refresh: 0; url=". $path);
		echo ' ';
		//return header('location:' . $path, true, 410);
	}
	
	public function JSONRedirect($path) {
		return PATH . $path;
	}
}
?>