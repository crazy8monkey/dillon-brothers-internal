<?php
Class Image {
	
	public Static function deleteImage($imageName) {
		unlink($imageName);
	}
	/**
	 * renameImage
	 * @param imageFile $imageUploaded
	 * @param string $recipeName
	 * @return renamed image
	 */
	public static function renameImage($albumName, $imageUploaded, $newName) {
			
		$firstStageImage = $imageUploaded;
		$kaboom = explode(".", $firstStageImage);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);

		$finalImage = $newName . '.' . $fileExt;
		//preg_replace('#[^a-z.0-9]#i', '-', $recipeName . '.' . $fileExt);
		rename($imageUploaded, $albumName . $finalImage);
	}

	public static function MoveAndRenameImage($tmpImage, $ImagePathParts = array(), $uploadedImage, $newImageName, $type) {
		$movableDirectory = NULL;	
		$ParentUploadeDirectory = NULL;
			
		switch($type) {
			//photo archive	
			case 1:
				$movableDirectory = PHOTO_PATH . $ImagePathParts['Year'] . $ImagePathParts['type'] . $ImagePathParts['AlbumName'] . '/' . $uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . $ImagePathParts['Year'] . $ImagePathParts['type'] . $ImagePathParts['AlbumName'] . '/';
				break;
			//PartsBrands aftermarket brand
			case 2:
				$movableDirectory = PHOTO_PATH . $ImagePathParts['PartsBrand'] . '/' . $uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . $ImagePathParts['PartsBrand'] . '/';
				break;
			//employees
			case 3:
				$movableDirectory = PHOTO_PATH . $ImagePathParts['Employees'] . '/' . $uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . $ImagePathParts['Employees'] . '/';
				break;
			//specials
			case 4:
				$movableDirectory = PHOTO_PATH . $ImagePathParts['SpecialsParent'] . '/' . $ImagePathParts['FolderName'] . '/' .$uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . $ImagePathParts['SpecialsParent'] . '/'. $ImagePathParts['FolderName'] . '/';
				break;
			//super event photos
			case 5:
				$movableDirectory = PHOTO_PATH . $ImagePathParts['SuperEventFolder'] . '/' .$uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . $ImagePathParts['SuperEventFolder'] . '/';
				break;
			//social media
			case 6:
				$movableDirectory = PHOTO_PATH . 'SocialMediaContent' . $ImagePathParts['SocialMediaType'] . '/' .$uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . 'SocialMediaContent' . $ImagePathParts['SocialMediaType'] . '/';
				break;	
			//inventory
			case 7:
				$movableDirectory = PHOTO_PATH . 'inventory' . $ImagePathParts['vin'] . '/' .$uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . 'inventory' . $ImagePathParts['vin'] . '/';
				break;
			//userprofilepic
			case 8:
				$movableDirectory = PHOTO_PATH . 'Accounts' . '/' .$uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . 'Accounts' . '/';
				break;
			//event pdfs	
			case 9:
				$movableDirectory = PHOTO_PATH . 'EventPDFs' . '/' .$uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . 'EventPDFs' . '/';
				break;	
			//Year photo
			case 10:
				$movableDirectory = PHOTO_PATH . $ImagePathParts['YearDirectory']. '/' .$uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . $ImagePathParts['YearDirectory'] . '/';
				break;	
			//water mark image
			case 11:
				$movableDirectory = PHOTO_PATH . 'WaterMark/' .$uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . 'WaterMark/';
				break;	
			//ecommerce products	
			case 12:
				$movableDirectory = PHOTO_PATH . 'ecommerce/' . $ImagePathParts['EcommerceProductFolder']. '/' .$uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . 'ecommerce/' .$ImagePathParts['EcommerceProductFolder'] . '/';
				break;
			//branding landing page content
			case 13:
				$movableDirectory = PHOTO_PATH . 'BrandLandingPageContent/' . $uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . 'BrandLandingPageContent/';
				break;
			//oem brands
			case 14:
				$movableDirectory = PHOTO_PATH . 'brands/' . $uploadedImage;	
				$ParentUploadeDirectory = PHOTO_PATH . 'brands/';
				break;
			
		}	
			
		
		
		move_uploaded_file($tmpImage, $movableDirectory);
		
		//gettingFileExtension
		$kaboom = explode(".", $uploadedImage);		
		$fileExt = end($kaboom);
		
		$finalNewImage = $newImageName . '.' . $fileExt;
		rename($movableDirectory, $ParentUploadeDirectory . $finalNewImage);
	}


	public static function getFileExt($image) {
		$fileExtStart = $image;
		$kaboom = explode(".", $fileExtStart);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);
		return $fileExt;
	}
	/**
	 * moveImage
	 * @param imageFile $imageUploaded
	 * @param imageFileTemp $recipeName
	 * @param session $imagePathDirectory
	 * @param string $recipeName
	 */
	public static function moveImage($image, $imageTemp, $ImagePathDirectory) {				
		$fileName = $image;
		$fileTmpLoc = $imageTemp;
		// Path and file name
		$pathAndName = $ImagePathDirectory.$fileName;
		// Run the move_uploaded_file() function here
		move_uploaded_file($fileTmpLoc, $pathAndName);
	}
	

	public static function resizeImage($target, $newcopy, $w, $h) {
	    list($w_orig, $h_orig) = getimagesize($target);
   		$scale_ratio = $w_orig / $h_orig;
   		if (($w / $h) > $scale_ratio) {
    	       $w = $h * $scale_ratio;
    	} else {
    	       $h = $w / $scale_ratio;
    	}
		
		$fileExtStart = $target;
		$kaboom = explode(".", $fileExtStart);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);
		
    	$img = "";
    	$ext = strtolower($fileExt);
		
    	switch($ext) {
			case "gif":
				$img = imagecreatefromgif($target);
				break;
			case "png":
				$img = imagecreatefrompng($target);
				break;
			case "jpg":
				$img = imagecreatefromjpeg($target);
				break;
    	}
    	
		$tci = imagecreatetruecolor($w, $h);
		
		//preserve transparency
		if($ext == "gif" or $ext == "png"){
			imagecolortransparent($tci, imagecolorallocatealpha($tci, 0, 0, 0, 127));
		    imagealphablending($tci, false);
		    imagesavealpha($tci, true);
		}
		
		// imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    	imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
		
		switch($ext) {
			case "gif":
				imagegif($tci, $newcopy, 75);
				break;
			case "png":
				imagepng($tci, $newcopy, 7.5);
				break;
			case "jpg":
				 imagejpeg($tci, $newcopy, 75);
				break;	
		}
    	
	}


	
	
	
	/**
	 * moveImage
	 * @param imageFile $image
	 * @param int $crop_width
	 * @param int $crop_height
	 * @param session $imageDirectory
	 * @param string $recipeName
	 */
	public static function cropImage($image, $crop_width, $crop_height, $imageDirectory) {
		
		$kaboom = explode(".", $image);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);
		list($orig_width, $orig_height) = getimagesize($imageDirectory . $image);
		
		
		//adjust image width
		$sourceRatio = $orig_width / $orig_height;
		$targetRatio = $crop_width / $crop_height;
		
		$centerImageY = round($new_height / 2);
		
		$tci = imagecreatetruecolor($crop_width, $crop_height);
		
		switch ($fileExt) {
			case 'jpg';
				$img = imagecreatefromjpeg($imageDirectory . $image);
			    imagecopyresampled($tci, $img, 0, 0, $left, $top, $crop_width, $crop_height, $orig_width, $orig_height);
				imagejpeg($tci, $imageDirectory . $image, 90);
				break;
			case 'jpeg';
				$img = imagecreatefromjpeg($imageDirectory . $image);
			    imagecopyresampled($tci, $img, 0, 0, 0, 0, $crop_width, $crop_height, $orig_width, $orig_height);
				imagejpeg($tci, $imageDirectory . $image, 90);
				break;	
			case 'gif';
				$img = imagecreatefromgif($imageDirectory . $image);
			    imagecopyresampled($tci, $img, 0, 0, 0, 0, $crop_width, $crop_height, $orig_width, $orig_height);
				imagegif($tci, $imageDirectory . $image);
				break;
			case 'png';
				$img = imagecreatefrompng($imageDirectory . $image);
			    imagecopyresampled($tci, $img, 0, 0, 0, 0, $crop_width, $crop_height, $orig_width, $orig_height);
				imagepng($tci, $imageDirectory . $image);
				break;
		}		
	}
	public static function copyImage($source, $destination, $quantity) {
		
		list($w_orig, $h_orig) = getimagesize($source);
		if($w_orig > 1000) {
			Image::resizeImage($source, $source, 1000, 1000);
		}
		
		
		
		
		copy($source, $destination);
		
		Image::compress($destination, $destination, $quantity);
	}


	public static function compress($source, $destination, $quality) {

	    $info = getimagesize($source);
		
		switch ($info['mime']) {
			case 'image/jpg';
				$image = imagecreatefromjpeg($source);
				imagejpeg($image, $destination, $quality);
				break;
			case 'image/jpeg';
				$image = imagecreatefromjpeg($source);
				imagejpeg($image, $destination, $quality);
				break;
			case 'image/gif';
				$image = imagecreatefromgif($source);
				imagegif($image, $destination, $quality);
				break;
			case 'image/png';
				$image = imagecreatefrompng($source);
			
				// Do required operations
				// Turn off alpha blending and set alpha flag
				imagealphablending($image, true);
				imagesavealpha($image, true);
			
				imagepng($image, $destination, ($quality/10));
				break;
		}

		return $destination;
	}

//$source_img = 'source.jpg';
//$destination_img = 'destination .jpg';

//$d = compress($source_img, $destination_img, 90);

	
	
	/**
	 * validateImageWidth
	 * @param imageFile $image
	 * @param int $crop_width
	 * @param int $crop_height
	 * @param session $imageDirectory
	 * @param string $recipeName
	 */
	public static function getImageWidth($imageDirectoryFolder, $image) {
		list($width) = getimagesize($imageDirectoryFolder . $image);
		return $width;
	}
}//class image
?>