<?php

Class Validation {
	
	public function emptyInput($userInput) {
		return empty($userInput);
	}
	public function notUsingLetters($userInput) {
		return !ctype_alpha(str_replace(' ', '', $userInput));
	}
	public function noWhiteSpace($userInput) {
		return strpos($userInput, " ") !== false;
	}
	public function correctEmailFormat($email) {
		return !Email::verifyEmailAddress($email);
	}
	public function passwordLength($password, $length) {
		return strlen($password) < $length;
	}
	public function notMatchingPasswords($confirmPassword, $password) {
		return $confirmPassword != $password;
	}
	public function onlyLettersNumbers($password) {
		return !ctype_alnum($password);
	}
	public function noEmailMatch($emailInput, $emailDatabase) {
		return $emailInput != $emailDatabase;	
	}
	public function matchingText($userInput, $matchingText) {
		return $userInput == $matchingText;
	}
	public function DecodeHTMLCharacters($userInput) {
		return htmlspecialchars_decode($userInput);
	}
	public function EncodeHTMLCharacters($userInput) {
		return htmlspecialchars($userInput);
	}
	public function SpecialCharacters($userInput) {
		return htmlentities($userInput);
	}
	public function onlyNumbers($userInput) {
		return !ctype_digit($userInput);
	}
	
	public function RemoveSpaces($userInput) {
		return str_replace(' ', '', $userInput);
	}
	public function HasLetters($userInput) {
		//return ctype_alnum($userInput);
		return preg_match("/[a-z]/i", $userInput);
		
	}
	public function emptyDropDown($userInput) {
		return $userInput == 0 or empty($userInput);
	}
	
	
}
