<?php

Class Folder {
	
	public static function DeleteDirectory($dir) {
	    foreach(scandir($dir) as $file) {
	        if ('.' === $file || '..' === $file) {
	        	continue;
	        }		
			
			if (is_dir("$dir/$file")) {
				Folder::DeleteDirectory("$dir/$file");
			} else {
				unlink("$dir/$file");
			}
	    }
	    rmdir($dir);
		
	}	
}
