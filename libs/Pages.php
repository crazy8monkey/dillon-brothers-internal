<?php

class Pages {
	
	public function photos() {return PATH . 'photos';}
	public function newsletters() {return PATH . 'newsletters';}
	public function specials() {return PATH . 'specials';}
	public function employees() {return PATH . 'employees';}
	public function events() {return PATH . 'events';}
	public function blog() {return PATH . 'blog';}
	public function users() {return PATH . 'users';}
	public function login() {return PATH . 'login';}
	public function socialmedia() {return PATH . 'socialmedia';}
	public function inventory() {return PATH . 'inventory';}
}