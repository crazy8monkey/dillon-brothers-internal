<?php

Class Pagination {
	private $mysqlCount;
	private $limit;
	private $targetpage;
	
	public $pagination;
	
	public function __construct() {}
	
	
	
	public function paginationLinks($setCount, $setLimit, $setTargetPage, $pageVariable, $misc = false) {		
		$adjacents = 2;
		$page = isset($pageVariable) ? max((int)$pageVariable, 1) : 1;
		
		$statusString = "";
		$paginationLink = "";

		if($misc) {
			$statusString = $misc;	
		}

		if ($page == 0)
			$page = 1;
		//previous page is page - 1
		$prev = $page - 1;
		$leftArrow = "<i class='fa fa-caret-left' aria-hidden='true'></i>";
		$rightArrow = "<i class='fa fa-caret-right' aria-hidden='true'></i>";
		//next page is page + 1
		$next = $page + 1;
		$lastpage = ceil($setCount / $setLimit);
		$this->pagination = "";
		if ($lastpage > 1) {
			$this->pagination .= "<div class='paginationObject'>";
			//previous button
			if ($page > 1) {
				$this->pagination .= $this -> TogglePaginationLinks($setTargetPage, $leftArrow, $prev, true, $statusString, "pagination-left");
			} else {
				$this->pagination .= $this -> TogglePaginationLinks($setTargetPage, $leftArrow, $prev, false, $statusString, "pagination-left");
				//$this->pagination .= "<a href='$setTargetPage?page=$prev'><div class='pagination-left'>&#8249;</div></a>";
			}
				
			//else
			//	$this->pagination .= "<div class='pagination-left'>&#8249;</div>";

			//pages
			if ($lastpage < 7 + ($adjacents * 2))//not enough pages to bother breaking it up
			{
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page) {
						$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);
					} else {
						$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $statusString, true);						
					}
						
				}
			} elseif ($lastpage > 5 + ($adjacents * 2))//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if ($page < 1 + ($adjacents * 2)) {
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);
						} else {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $statusString, true);
						}
							
					}
					
					$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $statusString, true);					
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);
					$this->pagination .= $this -> paginationLink($setTargetPage, $lastpage, $statusString, true);
					
				}
				//in middle; hide some front and some back
				elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
					$this->pagination .= $this -> paginationLink($setTargetPage, 1, $statusString, true);	
					$this->pagination .= $this -> paginationLink($setTargetPage, 2, $statusString, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);
					
					//$this->pagination .= "...";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);	
						} else {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $statusString, true);
						}
							
					}
					$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $statusString, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);
					$this->pagination .= $this -> paginationLink($setTargetPage, $lastpage, $statusString, true);					
				}
				//close to end; only hide early pages
				else {
					$this->pagination .= $this -> paginationLink($setTargetPage, 1, $statusString, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, 2, $statusString, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);

					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);	
						} else {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $statusString, true);
						}
							
					}
				}
			}
			//next button
			if ($page < $counter - 1) {
				$this->pagination .= $this -> TogglePaginationLinks($setTargetPage, $rightArrow, $next, true, $statusString, "pagination-right");
			} else {
				$this->pagination .= $this -> TogglePaginationLinks($setTargetPage, $rightArrow, $next, false, $statusString, "pagination-right");
			}
			
			$this->pagination .= "<div style='clear:both'></div></div>";
		}

		return $this->pagination;
	    
	}
	
	private function TogglePaginationLinks($targetPage, $leftRightArrow, $leftRightCounter, $isLink = false, $statusString = false, $className) {
		
		$leftRight = "";
		if($isLink == true) {
			if(!empty($statusString)) {
				$leftRight .= "<a href='". $targetPage . $leftRightCounter . '?'. $statusString . "'><div class='" . $className . "'>". $leftRightArrow . "</div></a>";
			} else {
				$leftRight .= "<a href='". $targetPage . $leftRightCounter . "'><div class='" . $className . "'>". $leftRightArrow . "</div></a>";	
			}
			
			
		} else {
			$leftRight .= "<div class='". $className . " linkInactive'>". $leftRightArrow . "</div>";
		}
		return $leftRight;
	}
	private function paginationLink($targetPage, $counter, $statusString = false, $isLink = false, $selected = false) {
		$paginationLink = "";
		if($isLink == true) {
			if(!empty($statusString)) {
				$paginationLink .= "<a href='". $targetPage . $counter . '?'. $statusString . "'><div class='pagination-number'>". $counter. "</div></a>";
			} else {
				$paginationLink .= "<a href='". $targetPage . $counter ."'><div class='pagination-number'>". $counter. "</div></a>";	
			}
				
		} else {
			if($selected == true) {
				$paginationLink .= "<div class='pagination-number pagination-number-selected'>" . $counter ."</div>";	
			} else {
				$paginationLink .= "<div class='pagination-number pagination-number-ellipse'>" . $counter ."</div>";
			}
			
		}
		
		
		return $paginationLink;
	}

	public function getPaginationLinks() {
		return $this-> pagination;	
	}
}