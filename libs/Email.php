<?php

Class Email {
			
	public $to;
	public $subject;
	private $msg;
	private $header;
	
	public function __construct() {
		$this -> header = 'From: Dillon Brothers Internal <DillonBrothersInternal@donotreply.com>'. "\r\n";
		$this -> header .= 'MIME-Version: 1.0' . "\r\n";
		$this -> header .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
		$this -> header .= 'Bcc: netadmin@siddillon.com, adam.schmidt@siddillon.com' . "\r\n";	
	}
	
	private function emailLogo() {
		return "<img src='" . PATH . "public/images/DillonBrothers.png' />";
	}

	
	
	public function Credentials($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:white; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='font-family: arial; text-align:center; padding:0px 10px 30px 10px'>" . $this -> emailLogo() ."</td>
									</tr>
									<tr>
										<td style='padding:0px;'>
											<table cellpadding='0' cellspacing='0' width='100%'>
												<tr>
													<td style='padding:0px 0px 15px 0px'>
														<table cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td style='color:#c30000; font-size:16px; font-family:arial; padding:0px 0px 10px 0px'>Password</td>
															</tr>
															<tr>
																<td style='color:black; font-size:24px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:0px 0px 5px 0px'>" . $content['password'] . "</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . PATH . "' style='text-decoration:underline; color:white;'>Login</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
		$this -> SendEmail();				
						
	}
	
	public function PasswordReset($link) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:white; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='font-family: arial; text-align:center; padding:0px 10px 30px 10px'>" . $this -> emailLogo() ."</td>
									</tr>
									<tr>
										<td style='padding:0px;'>
											<table cellpadding='0' cellspacing='0'>
												<tr>
													<td style='font-family:arial; text-align:center; padding:0px 0px 20px 0px; font-size:20px;'>Click the link below to reset your password</td>
												</tr>
												<tr>
													<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . $link . "' style='text-decoration:underline; color:white;'>Reset your Password</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	public function DeclinedPost($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:white; padding:30px 0px 30px 0px; margin:0px;'>		
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='font-family: arial; text-align:center; padding:0px 10px 30px 10px'>" . $this -> emailLogo() ."</td>
									</tr>
									<tr>
										<td style='padding: 0px 0px 20px 0px; font-size: 24px; font-family: arial; font-weight:bold;'>" . $content["type"] . " Post Declined</td>
									</tr>
									<tr>
										<td style='padding:0px;'>
											<table cellpadding='0' cellspacing='0' width='100%'>
												<tr>
													<td style='padding:0px 0px 20px 0px'>
														<table cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td style='color:#c30000; font-size:16px; font-family:arial; padding:0px 0px 10px 0px'>Your Post</td>
															</tr>
															<tr>
																<td style='color:black; font-size:16px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:0px 0px 5px 0px'>" . $content['post-content'] . "</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:0px 0px 15px 0px'>
														<table cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td style='color:#c30000; font-size:16px; font-family:arial; padding:0px 0px 10px 0px'>Reason</td>
															</tr>
															<tr>
																<td style='color:black; font-size:16px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:0px 0px 5px 0px'>" . $content['decline-notes'] . "</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . PATH . "' style='text-decoration:underline; color:white;'>Login</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
			
		$this -> SendEmail();
	}
	
	public function UrgentPost($content) {
			$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#white; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='font-family: arial; text-align:center; padding:0px 10px 30px 10px'>" . $this -> emailLogo() ."</td>
									</tr>
									<tr>
										<td style='padding: 0px 0px 20px 0px; font-size: 24px; font-family: arial; font-weight:bold;'>Urgent Social Media Post</td>
									</tr>
									<tr>
										<td style='padding:0px;'>
											<table cellpadding='0' cellspacing='0' width='100%'>
												<tr>
													<td style='padding:0px 0px 20px 0px'>
														<table cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td style='color:#c30000; font-size:16px; font-family:arial; padding:0px 0px 10px 0px'>Store Relation</td>
															</tr>
															<tr>
																<td style='color:black; font-size:24px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:0px 0px 5px 0px'>" . $content['storeName'] . "</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:0px 0px 15px 0px'>
														<table cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td style='color:#c30000; font-size:16px; font-family:arial; padding:0px 0px 10px 0px'>Needs to be posted</td>
															</tr>
															<tr>
																<td style='color:black; font-size:24px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:0px 0px 5px 0px'>". $content['urgentTime'] . "</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . PATH . "' style='text-decoration:underline; color:white;'>Login</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	public function EmaiSummary($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#white; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='font-family: arial; text-align:center; padding:0px 10px 30px 10px'>" . $this -> emailLogo() ."</td>
									</tr>" . 
									$content 
								."
								<tr>
									<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
										<a href='" . PATH . "' style='text-decoration:underline; color:white;'>Login</a>
									</td>
								</tr>
								</table>
							</body>
						</html>";
			
		$this -> SendEmail();
	}
	
	public function InventorySpecLookup($Results) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#white; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='padding: 0px 0px 20px 0px; font-size: 24px; font-family: arial; font-weight:bold;'>Vin Specifications Lookup Result</td>
									</tr>
									<tr>
										<td style='padding:0px;'>
											<table cellpadding='0' cellspacing='0' width='100%'>
												<tr>
													<td style='padding:0px 0px 20px 0px'>
														<table cellpadding='0' cellspacing='0' width='100%'>";
															if($Results['RequireValidation'] == true) {
																$this -> msg .= "<tr>
																					<td style='padding:0px 5px 10px 0px; width:250px'>
																						<a href='" . $Results['BackupInventoryLink'] . "'>
																							<table cellpadding='0' cellspacing='0' width='100%'>
																								<tr>
																									<td style='background:#c30000; color:white; font-size:16px; font-family:arial; text-align:center; font-weight:bold; padding:10px'>Validation Required!</td>
																								</tr>
																							</table>
																						</a>
																					</td>
																					<td style='padding:0px 0px 10px 5px; width:250px'>
																						<a href='" . $Results['BikeLookup'] . "'>
																							<div style='background:#3366cc; color:white; font-size:16px; font-family:arial; text-align:center; padding:10px'>
																								<strong>Lookup Bike</strong>
																							</div>
																						</a>
																					</td>
																				</tr>";				
															}
									
														
										   $this -> msg .= "<tr>
																<td colspan='2' style='color:black; font-size:16px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:0px 0px 10px 0px'>" . $Results['ResultType'] . "</td>
															</tr>
															<tr>
																<td style='color:#c30000; font-size:16px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:10px 0px 10px 0px'>Vin Number</td>
																<td style='color:black; font-size:16px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:10px 0px 10px 0px'>" . $Results['vinNumber'] . "</td>
															</tr>
															<tr>
																<td style='color:#c30000; font-size:16px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:10px 0px 10px 0px'>Bike</td>
																<td style='color:black; font-size:16px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:10px 0px 10px 0px'>" . $Results['year'] . ' ' . $Results['make'] . ' ' . $Results['model'] . "</td>
															</tr>
															<tr>
																<td style='color:#c30000; font-size:16px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:10px 0px 10px 0px'>Current Inventory ID</td>
																<td style='color:black; font-size:16px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:10px 0px 10px 0px'>" . $Results['inventoryID'] . "</td>
															</tr>";
															
															if($Results['RequireValidation'] == true) {
																$this -> msg .= "<tr>
																					<td colspan='2' style='color:#c30000; font-size:16px; font-family:arial; padding:10px 0px 10px 0px'>Scrapped Data Response</td>
																				</tr>
																				<tr>
																					<td colspan='2' style='color:black; font-size:12px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:10px 0px 10px 0px; width:500px'><pre>" . var_export($Results['saved-response'], true) . "</pre></td>
																				</tr>";		
															}
																
															
										$this -> msg .= "</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
						
			$this -> SendEmail();			
	}
	
	public function TrackOrder($url) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:white; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
																<td style='padding: 0px 0px 20px 0px; text-align:center'>" . $this -> emailLogo() . "</td>
															</tr>
									<tr>
										<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
											<a href='" . $url. "' style='text-decoration:underline; color:white;'>Track your order</a>
										</td>
									</tr>
									<tr>
										<td style='font-family:arial; border-top: 1px solid #cacaca;text-align:center; padding:10px; font-size:12px;'>Copyright &copy; " . date('Y') . " Dillon Brothers. All Rights Reserved.</td>
									</tr>
								</table>
							</body>
						</html>";
		$this -> SendEmail();		
	}
	
	public function ChatNotification() {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:white; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='padding: 0px 0px 20px 0px; text-align:center'>" . $this -> emailLogo() . "</td>
									</tr>
									<tr>
										<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
											You have a new chat member
										</td>
									</tr>
									<tr>
										<td style='font-family:arial; border-top: 1px solid #cacaca;text-align:center; padding:10px; font-size:12px;'>Copyright &copy; " . date('Y') . " Dillon Brothers. All Rights Reserved.</td>
									</tr>
								</table>
							</body>
						</html>";
		$this -> SendEmail();		
	}
	
	
	public function DillonBrothersInternalError($content) {
		$this -> msg = "<table cellpadding='0' cellspacing='5'>" . $content ."</table>";
		$this -> SendEmail();
	}
		
	private function SendEmail() {
		mail($this -> to, $this -> subject . EMAIL_SUBJECT, $this -> msg, $this -> header);
	}
	
	public static function verifyEmailAddress($emailInput) {
		return preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$emailInput);
	}
}