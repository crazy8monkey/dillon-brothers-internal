<?php


class ColorObject extends BaseObject {
	
	private $_id;
	public $ColorText;
	public $FilterText;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($colorID) {
        $instance = new self();
        $instance->_id = $colorID;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM colors WHERE colorID = :colorID');
        $sth->execute(array(':colorID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> ColorText = $row['ColorText'];
		$this -> FilterText = $row['FilterColor'];
    }
	
	public function GetID() {
		return $this->_id;
	}

	public function Validate() {
		if(!isset($this->_id)) {
            $colorCheck = $this->db->prepare('SELECT ColorText FROM colors WHERE ColorText = ?');
            $colorCheck->execute(array($this->ColorText));
        } else {
            $colorCheck = $this->db->prepare('SELECT * FROM colors WHERE ColorText = :Text AND colorID <> :colorID');
            $colorCheck->execute(array(':colorID' => $this->_id, 
            						   ':Text' => $this -> ColorText));
        }
		
		
		$validationErrors = array();
		

		//empty first name
		if(count($colorCheck -> fetchAll())) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'There is already that color entered into the database, please type in a different color'));
		}
		
		

		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Save() {
		try {

			
			if(isset($this -> _id)) {
				
				if(isset($this -> FilterText)) {
					$postData = array('ColorText' => $this -> ColorText,
								  	  'FilterColor' => $this -> FilterText);	
				} else {
					$postData = array('ColorText' => $this -> ColorText);	
				}
									
				$this->db->update('colors', $postData, array('colorID' => $this -> _id));
				
				$this -> redirect -> redirectPage(PATH. 'settings/inventorycolors');
				
			} else {
				
				
				
				
				$NewColor = $this -> db -> insert('colors', array('ColorText' => $this -> ColorText));
																  
				$this -> json -> outputJqueryJSONObject("NewColor", array("Text" => $this -> ColorText,
																		  "ID" => $NewColor));													  
			
			}
				
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Event Save Error: " . $e->getMessage();
			$TrackError -> type = "EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	

		

}