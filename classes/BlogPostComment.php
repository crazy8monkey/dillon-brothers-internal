<?php


class BlogPostComment extends BaseObject {
	
	public $commentID;	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	
	public function ApproveComment() {
		$postData = array('commentVisible' => 1,
						  'commentDatePublished' => date("Y-m-d", $this -> time -> NebraskaTime()) . 'T'. date("H:i:s", $this -> time -> NebraskaTime()));				
		$this->db->update('blogcomments', $postData, array('blogCommentID' => $this -> commentID));
		$this -> redirect -> redirectPage(PATH . "blog/comments");
	}
	
	public function DeleteComment() {
		$sth = $this -> db -> prepare("DELETE FROM blogcomments WHERE blogCommentID = :blogCommentID");
		$sth -> execute(array(':blogCommentID' => $this -> commentID));
		$this -> redirect -> redirectPage(PATH . "blog/comments");
	}
}