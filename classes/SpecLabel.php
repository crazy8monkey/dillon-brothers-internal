<?php

class SpecLabel extends BaseObject {
	
	private $_id;
	private $_idExists;
	
	private $_similiarText;
	
	public $labelID;
	public $labelText;
	public $LabelCategories;
	public $RelatedGroupID;
	
	public $newOrder;
	
	public $NewSpecLabelID;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($labelID) {
        $instance = new self();
        $instance->_id = $labelID;
        $instance->loadById();
        return $instance;
    }
	
	public static function WithCheckText($text) {
		$instance = new self();
        $instance->_similiarText = $groupID;
        $instance->loadByLikeText();
        return $instance;
	}
	
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM speclabels WHERE specLabelID = :specLabelID');
        $sth->execute(array(':specLabelID' => $this->_id));	
		$record = $sth -> fetch();
		$this->fill($record);
    }
	
	protected function loadByLikeText() {
    	$sth = $this -> db -> prepare('SELECT * FROM speclabels WHERE labelText LIKE ?');
        $sth->execute(array('%' . $this -> _similiarText. '%'));	
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
		    $this->fill($record);
			$this -> _idExists = true;
		} else {
			$this -> _idExists = false;
		}
		
    }
	
	
    protected function fill(array $row){
    	$this -> labelID = $row['specLabelID'];
		$this -> labelText = $row['labelText'];
		$this -> LabelCategories = $row['LinkedInventoryCategories'];
		$this -> RelatedGroupID = $row['relatedSpecGroupID'];
    }	
	
	public function GetID() {
		return $this -> _id;
	}
	
	
	public function Validate() {
		$validationErrors = array();
		

		//empty first name
		if($this -> validate -> emptyInput($this -> labelText)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		}
		
		if($this -> LabelCategories == NULL) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		}
		
		if(!isset($this->_id)) {
			if($this -> groupID == 0) {
				array_push($validationErrors, array("inputID" => 3,
													'errorMessage' => 'Required'));
			}	
		}
		

		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	
	public function SaveFromInventoryCheck() {
		$groupID = $this -> db -> prepare('SELECT * FROM specgroups WHERE GroupVisible = 0 LIMIT 1');
		$groupID -> execute();
		$getGroup = $groupID -> fetch();
		$newGroupID = $getGroup['specGroupID'];
		
		$this -> NewSpecLabelID = $this -> db -> insert('speclabels', array('labelText' => $this -> labelText,
																 			'relatedSpecGroupID' => $newGroupID));
	}
	
	
	public function Save() {
		$categoryIDS = NULL;
			
		foreach($this -> LabelCategories as $labelSingle) {
			$categoryIDS .= $labelSingle. ',';
		}	
			
		if(isset($this->_id)) {
			$labelID = $this -> _id;
			
			
			
			$postData = array('labelText' => $this -> labelText,
							  'relatedSpecGroupID' => $this -> groupID,  
							  'LinkedInventoryCategories' => rtrim($categoryIDS, ','));
				
			$this->db->update('speclabels', $postData, array('specLabelID' => $this->_id));
		} else {
			
			$this -> NewSpecLabelID = $this -> db -> insert('speclabels', array('labelText' => $this -> labelText,
																 'relatedSpecGroupID' => $this -> groupID,
																 'LinkedInventoryCategories' => rtrim($categoryIDS, ',')));	
		}
					
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'settings/specifications');
										   					
	}
	
	public function GetNewSpecLabelID() {
		return $this -> NewSpecLabelID;
	}
	
	public function Delete() {
		$sth = $this -> db -> prepare("DELETE FROM speclabels WHERE specLabelID = :specLabelID");
		$sth -> execute(array(':specLabelID' => $this -> _id));	
		
		$this -> redirect -> redirectPage(PATH. 'settings/specifications');
	}
	
	public function reorderLabels() {
		
		$count = 1;
		foreach ($this -> labelID as $value) {
			$postData = array('labelOrder' => $count);
				
			$this->db->update('speclabels', $postData, array('specLabelID' => $value));			
			$count++;
		}
		
		$this -> json -> outputJqueryJSONObject('SavedOrder', true);
	}
	
	public function UpdateOrder() {
		$postData = array('labelOrder' => $this -> newOrder);
				
		$this->db->update('speclabels', $postData, array('specLabelID' => $this->_id));			
		
	}
	
	private function ValidateCategories($type) {
		$ReturnValue = NULL;
		foreach ($type as $key => $value) {
			if(!isset($value)) {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}
	
	


}