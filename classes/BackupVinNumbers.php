<?php

class BackupVinNumber extends BaseObject {
	
	public $id;
	public $BackupVinNumber;
	public $inventoryBackupInventoryID;
	public $CurrentBackupID;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }
	
	public function Save() {
		try {			
			$this -> db -> insert('vinnumberbackup', array('backedupVinNumber' => $this -> BackupVinNumber,
														   'inventoryBackupBikeID' => $this -> inventoryBackupInventoryID, 
														   'date' => date("Y-m-d", $this -> time -> NebraskaTime()),
														   "time" => date("H:i:s", $this -> time -> NebraskaTime())));			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Employee Save Error: " . $e->getMessage();
			$TrackError -> type = "EMPLOYEE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	public function Validate() {
		$validationErrors = array();
		
		if($this -> inventoryBackupInventoryID == 0) {
				array_push($validationErrors, array('inputID' => 99,
													'errorMessage' => 'Please select your option to reassign or remove vin'));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function MakeDecision() {
		if($this -> inventoryBackupInventoryID == '-1') {
			$this -> Delete();
		} else {
			$this -> Update();
		}
		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'inventory/backup/1');	
	}
	
	public function MakeDecisionReassign() {
		foreach($this -> inventoryBackupInventoryID as $Single) {
			switch($Single) {
				case '-1':
					break;
				case '-2':
					$this -> Update();
					break;
				default:
					$this -> Update();
					break;
			}	
		}
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'inventory/editbackup/' . $this -> CurrentBackupID);
	}
	
	public function Update() {
		$postData = array('inventoryBackupBikeID' => $this -> inventoryBackupInventoryID);
		
		$this->db->update('vinnumberbackup', $postData, array('vinBackupID' => $this -> id));
	}
	
	
	public function Redirect() {
		$this -> redirect -> redirectPage(PATH. 'inventory/backup/1');
	}
	
	public function Delete() {
		$sth = $this -> db -> prepare("DELETE FROM vinnumberbackup WHERE vinBackupID = :vinBackupID");
		$sth -> execute(array('vinBackupID' => $this -> id));
	}

	


}