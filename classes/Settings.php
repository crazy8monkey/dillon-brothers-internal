<?php

class SettingsObject extends BaseObject {

	public $FacebookList;
	public $TwitterList;
	public $EmailSummary;
	public $UrgentEmail;
	public $DonationEmail;
	public $InventorySpecNotifications;
	public $WaterMarkImage;
	public $NewWaterMarkImageName;
	public $NewWaterMarkImageTemp;
	public $SMSDisclosure;
	
	
	private $_permissions = array();

    public function __sleep()
    {
        parent::__sleep();
        return array('WaterMarkImage');
    }

    public function __wakeup()
    {
        parent::__wakeup();
		return array('WaterMarkImage');
    }


    public function __construct(){
        parent::__construct();
		return array('WaterMarkImage');
    }

    public static function Init(){
        $instance = new self();
        $instance->loadSettings();
        return $instance;
    }


    protected function loadSettings() {
        $sth = $this -> db -> prepare('SELECT * FROM settings WHERE settingsID = 1');
        $sth -> execute();
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
		$this -> FacebookList = $row['socialMediaFacebook'];
		$this -> TwitterList = $row['socialMediaTwitter'];
		$this -> EmailSummary = $row['EmailSummary'];
		$this -> UrgentEmail = $row['UrgentEmail'];
		$this -> DonationEmail = $row['DonationEmail'];
		$this -> InventorySpecNotifications = $row['inventorySpecNotifcations'];
		$this -> WaterMarkImage = $row['WaterMarkImage'];
		$this -> SMSDisclosure = $row['SMSDisclosure'];
    }

    public function ValidateSocialMedia() {
    	$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> FacebookList)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		} else if($this -> validate -> onlyNumbers($this -> FacebookList)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Only Numbers'));
		}
		
		
		if($this -> validate -> emptyInput($this -> TwitterList)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		} else if($this -> validate -> onlyNumbers($this -> TwitterList)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Only Numbers'));
		}
		
		if($this -> validate -> emptyInput($this -> EmailSummary)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> UrgentEmail)) {
			array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => 'Required'));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}	
    }
	
	public function ValidateInventory() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> InventorySpecNotifications)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		} 
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}	
	}
	
	public function ValidateApplicationSettings() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> NewWaterMarkImageName)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'A photo must be uploaded if you are changing the water mark'));
		} 
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}	
		
		
	}
	
	public function ValidateDonation() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> DonationEmail)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		} 
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}	
	}
	
	public function ValidateSMSDisclaimer() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> SMSDisclosure)) {
			array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => 'Required'));
		} 
		
		if (strpos($this -> SMSDisclosure, '[EmployeeName]') === false) {
		    array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => '[EmployeeName] needs to be placed in the textarea'));
		}
		
		if (strpos($this -> SMSDisclosure, '[StoreName]') === false) {
		    array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => '[StoreName] needs to be placed in the textarea'));
		}
		
		if (strpos($this -> SMSDisclosure, '[DepartmentName]') === false) {
		    array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => '[DepartmentName] needs to be placed in the textarea'));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}	
	}

	private function NewWaterMarkPhoto() {
		$ImagePathParts = array();
		Image::MoveAndRenameImage($this -> NewWaterMarkImageTemp, 
								  $ImagePathParts,
								  $this -> NewWaterMarkImageName,
								  'OveralWaterMark', 11);
	}

    public function Save($type) {
    	try {
		
			$redirectPATH = NULL;
			switch($type) {
				case "socialmedia":					
					$postData = array('socialMediaFacebook' => $this -> FacebookList,
		               				  'socialMediaTwitter' => $this -> TwitterList,
									  'EmailSummary' => $this -> EmailSummary,
									  'UrgentEmail' => $this -> UrgentEmail);
									  
					$redirectPATH = PATH . "settings/socialmedia";				  
					break;
				case "inventory":
					$postData = array('inventorySpecNotifcations' => $this -> InventorySpecNotifications);
					$redirectPATH = PATH . "settings/specifications";
					break;
				case "WaterMark":
					$this -> NewWaterMarkPhoto();
					
					$postData = array('WaterMarkImage' => 'OveralWaterMark.' . Image::getFileExt($this -> NewWaterMarkImageName));
					$redirectPATH = PATH . "settings/photos";
					break;
				case "Donation":
					$postData = array('DonationEmail' => $this -> DonationEmail);
					$redirectPATH = PATH . "settings/donation";
					break;
				case "SMSDisclaimer":
					$postData = array('SMSDisclosure' => $this -> SMSDisclosure);
					$redirectPATH = PATH . "settings/sms";
					break;
			}
			
			$this->db->update('settings', $postData, array('settingsID' => 1));	
		
			$this -> json -> outputJqueryJSONObject("redirect", $redirectPATH);
		} catch (Exception $e) {
			
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_GENERAL_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			if(isset($this->_userId)) {
				$this -> json -> outputJqueryJSONObject("MySqlError", $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} else {
				$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				$this -> json -> outputJqueryJSONObject("redirect", PATH . "cms/users");
			}
			
		
		}

    }

	


}