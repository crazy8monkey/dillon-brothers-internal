<?php


class PhotoAlbum extends BaseObject {
	
	private $_id;
	private $_uploadProgress;
	
	private $_albumFolderName;
	
	private $_newDirectoryID;
	private $_eventID;
	
	public $albumID; 
		
	public $folderName;
	public $year;
	
	public $_albumName;
	
	public $fileUploads;
	public $photoName;
	public $altText;
	public $titleText;
	
	public $NewAlbumName;
	
	private $_createdByFirstName;
	private $_createdByLastName;
	
	public $createdDate;
	public $createdTime;
	
	public $DateOfEvent;
	
	public $photoCount;
	public $UploadCount;
	
	public $StoreName;
	
	public $photoNames;
	public $photoAltTexts;
	public $photoTitleTexts;
	
	public $albumThumbNail;
	
	//1 = event album
	//2 = blog album
	public $AlbumType;
	
	public $AlbumFolderName;
	public $LinkedEventID;
	public $LinkedBlogID;
	public $LinkedSuperEventID;
	
	public $redirectPage;
	
	private $UploadPhotoProgress;
	
	public $SessionUploadID;
	public $FormName;
	public $ProgressKey;
	
	public $BlogMainPhoto;
	public $EventMainPhoto;
	public $EventThumbNailPhoto;
	
	public $relatedBlogID;
	public $relatedEventID;
	public $AlbumThumbNailName;
	public $AlbumThumbNailExt;
	
	public $Visible;
	
	public $LinkedEvent;
	
	public $publishedDate;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithAlbumName($albumName) {
        $instance = new self();
        $instance->_albumName = $albumName;
        $instance->loadByName();
        return $instance;
    }
	
	public static function WithFolderName($folderName) {
		$instance = new self();
        $instance->_albumFolderName = $folderName;
        $instance->loadByFolderName();
        return $instance;
	}
	
	public static function WithID($albumID) {
        $instance = new self();
        $instance->_id = $albumID;
        $instance->loadByID();
        return $instance;
    }
		
	public static function WIthEventID($eventID) {
		$instance = new self();
        $instance -> _eventID = $eventID;
        $instance -> loadByEventID();
        return $instance;
	}	
		
	protected function loadByName() {
    	$sth = $this -> db -> prepare('SELECT * FROM photoalbums LEFT JOIN users ON photoalbums.createdBy = users.userID WHERE albumName = :albumName');
        $sth->execute(array(':albumName' => $this->_albumName));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT *, (SELECT COUNT(*) FROM photos WHERE photoAlbumID = :albumID) PhotoCount FROM photoalbums 
    										LEFT JOIN users ON photoalbums.createdBy = users.userID 
    										LEFT JOIN photos ON photoalbums.albumThumbNail = photos.photoID WHERE albumID = :albumID');
        $sth->execute(array(':albumID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	protected function loadByEventID() {
		$sth = $this -> db -> prepare('SELECT * FROM photoalbums WHERE albumEventID = :albumEventID');
        $sth->execute(array(':albumEventID' => $this -> _eventID));
        $record = $sth -> fetch();
        $this->fill($record);
	}
	
	protected function loadByFolderName() {
		$sth = $this -> db -> prepare('SELECT * FROM photoalbums 
												LEFT JOIN users ON photoalbums.createdBy = users.userID WHERE albumFolderName = :albumFolderName');
        $sth->execute(array(':albumFolderName' => $this->_albumFolderName));
        $record = $sth -> fetch();
        $this->fill($record);
	}
	

    protected function fill(array $row){
    	$this -> albumID = $row['albumID'];
		$this -> year = $row['ParentDirectory'];
		$this -> _albumName = $row['albumName'];
		if(isset($this->_albumFolderName)) {
			$this -> _createdByFirstName = $row['firstName'];
			$this -> _createdByLastName = $row['lastName'];	
		}
		
		$this -> createdDate = $row['createdDate'];
		$this -> createdTime = $row['createdTime'];
		$this -> UploadCount = $row['UploadCount'];
		$this -> AlbumType = $row['AlbumType'];
		$this -> AlbumFolderName = $row['albumFolderName'];
		$this -> albumThumbNail = $row['albumThumbNail'];
		$this -> relatedBlogID = $row['albumBlogID'];
		$this -> relatedEventID = $row['albumEventID'];
		$this -> Visible = $row['photoAlbumVisible'];
		$this -> LinkedEvent = $row['linkedSuperEventID'];
		$this -> publishedDate = $row['albumPublishedDate'];    
		if(isset($this->_id)) {
			$this -> AlbumThumbNailName = $row['photoName'];
			$this -> AlbumThumbNailExt = $row['ext'];	
			$this -> photoCount = $row['PhotoCount'];
		}
		
    }
	
	public function GetCreatedByName() {
		return $this -> _createdByFirstName . ' ' .$this -> _createdByLastName;
	}
	
	public function GetAlbumName() {
		return $this -> _albumName;
	}
	
	
	private function getType() {
			
		switch($this -> AlbumType) {
			case 1:
				$path = "/events/";	
				break;
			case 2:
				$path = "/blog/";	
				break;
			case 3:
				$path = "/misc/";	
				break;
		}
		return $path;
	}
	
	public function Validate() {
		$path = NULL;	
		
		switch($this -> AlbumType) {
			case 1:
				$path = "/events/";	
				break;
			case 2:
				$path = "/blog/";	
				break;
			case 3:
				$path = "/misc/";	
				break;
		}
		
		$fileDirectoryCheck = PHOTO_PATH . $this -> year . $path . parent::seoUrlSpaces($this -> folderName);
		if($this -> validate -> emptyInput($this -> folderName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Album Name"));
			//$this -> json -> outputJqueryJSONObject('errorMessage', $fileDirectoryCheck);
			return false;
		} else if(file_exists($fileDirectoryCheck)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "Folder Already Exists");
			return false;
		} 
		
		return true;
	}
	

	
	public function NewDirectory($enteredYear = NULL) {
		
		Session::init();
		
		$FileDirectoryPathCheck = PHOTO_PATH . $this -> year;
		$newFolder = NULL;
		
		switch($this -> AlbumType) {
			//event albums	
			case 1:				
				$newFolder = PHOTO_PATH . $this -> year . '/events/' . parent::seoUrlSpaces($this -> folderName);		
				break;
			case 2:
				$newFolder = PHOTO_PATH . $this -> year . '/blog/' . parent::seoUrlSpaces($this -> folderName);
				break;
			case 3:
				$newFolder = PHOTO_PATH . $this -> year . '/misc/' . parent::seoUrlSpaces($this -> folderName);
				break;
		}	
		
		if(!file_exists($FileDirectoryPathCheck)) {
			$this -> EnterNewYearDirectory();
		}
		
		mkdir($newFolder);
					
		if($this -> AlbumType == 1) {
			
		}			
					
		$this -> _newDirectoryID = $this -> db -> insert('photoalbums', array('albumName' => $this -> folderName, 
																				  'albumFolderName' => parent::seoUrlSpaces($this -> folderName),
																				  'albumSEOurl' => parent::seoUrlSpaces($this -> folderName),
																				  'createdDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
																				  'createdTime' => date("H:i:s", $this -> time -> NebraskaTime()),
																				  'createdBy' => $_SESSION['user'] -> _userId,
																				  'ParentDirectory' => (isset($enteredYear) ? $enteredYear : $this -> year),
																				  'AlbumType' => $this -> AlbumType,
																				  'albumEventID' => ($this -> AlbumType == 1 ? $this -> LinkedEventID : 0),
																				  'albumBlogID' => ($this -> AlbumType == 2 ? $this -> LinkedBlogID : 0),
																				  'linkedSuperEventID' => $this -> LinkedSuperEventID));
		
		
		
	}

	public function RedirectYearTypeDirectory() {
		$redirectLink = NULL;
		switch($this -> AlbumType) {
			case 1:
				$redirectLink = $this -> pages -> photos() . '/type/events/' . $this -> year;
				break;	
			case 2:
				$redirectLink = $this -> pages -> photos() . '/type/blog/' . $this -> year;
				break;	
			case 3:
				$redirectLink = $this -> pages -> photos() . '/type/misc/' . $this -> year;
				break;	
		}
		$this -> json -> outputJqueryJSONObject('redirect', $redirectLink);
	}

	public function RedirectToPhotosMainPage() {
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'photos');
	}

	public function ValidateYear() {
		$validationErrors = array();
		
		$yearCheck = $this->db->prepare('SELECT YearText FROM yearphotos WHERE YearText = ?');
        $yearCheck -> execute(array($this -> year));
		
		if($this -> validate -> emptyInput($this -> year)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Please enter a year'));
		} else if(count($yearCheck -> fetchAll())) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'The Year Directory already exists'));
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
	}


	private function uploadPicture() {
		$path = NULL;	
		switch($this -> AlbumType) {
			case 1:
				$path = '/events/';
				break;
			case 2:
				$path = '/blog/';
				break;
			case 3:
				$path = '/misc/';
				break;
				
		}
		
		$ImagePathParts = array();
		$ImagePathParts['Year'] = $this -> year;
		$ImagePathParts['type'] = $path;
		$ImagePathParts['AlbumName'] = $this -> AlbumFolderName;
		
		Image::MoveAndRenameImage($_FILES['albumThumbNail']['tmp_name'], 
								  $ImagePathParts,
								  $this -> albumThumbNail,
								  parent::seoUrl($this -> _albumName) . 'AlbumThumb', 1);
		
		
		$currentPhoto = PHOTO_PATH . $this -> year . $path . $this -> AlbumFolderName . '/' . parent::seoUrl($this -> _albumName) . 'AlbumThumb.' .Image::getFileExt($this -> albumThumbNail);
		Image::resizeImage($currentPhoto, $currentPhoto, 500, 500);						  						  
	}

	public function SetAlbumThumbNail($photoID = NULL) {
		
		if($photoID != NULL) {
			$postData = array('albumThumbNail' => $photoID);	
			$this->db->update('photoalbums', $postData, array('albumID' => $this -> _id));
		} 
		
	}

	public function ValidateAlbum() {
		$validationErrors = array();
		
		if(isset($this -> _id)) {
			$albumNameCheck = $this->db->prepare('SELECT albumName FROM photoalbums WHERE albumName = :albumName AND ParentDirectory = :ParentDirectory AND albumID <> :albumID');
        	$albumNameCheck -> execute(array(':albumName' => $this -> NewAlbumName,
										     ':ParentDirectory' => $this -> year,
											 ':albumID' => $this -> _id));
		} else {
			$albumNameCheck = $this->db->prepare('SELECT albumName FROM photoalbums WHERE albumName = ?');
        	$albumNameCheck -> execute(array($this -> NewAlbumName));	
		}
		
		
		
		if(count($albumNameCheck -> fetchAll())) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'There is already an album name associated to what you entered, please enter a new album name'));
		} 
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function SaveAlbumDetails() {
		
		if($this -> publishedDate != NULL) {
			$publishedDate = $this -> publishedDate;
		} else {
			$publishedDate = date("Y-m-d", $this -> time -> NebraskaTime()) . 'T'. date("H:i:s", $this -> time -> NebraskaTime());
		}
		
		$postData = array('albumName' => $this -> NewAlbumName,
						  'linkedSuperEventID' => $this -> LinkedEvent,
						  'albumPublishedDate' =>$publishedDate, 
						  'albumModifedDate' => date("Y-m-d", $this -> time -> NebraskaTime()) . 'T'. date("H:i:s", $this -> time -> NebraskaTime()),
						  'photoAlbumVisible' => $this -> Visible);	
		
				
		$this->db->update('photoalbums', $postData, array('albumID' => $this -> _id));
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'photos/view/album/' . $this->_id);		
	}

	public function EnterNewYearDirectory() {
		$this -> NewYearDirectory();
		$this -> db -> insert('yearphotos', array('YearText' => $this -> year));
		
	}


	private function NewYearDirectory() {
		mkdir(PHOTO_PATH . $this -> year);
		mkdir(PHOTO_PATH . $this -> year . '/events');
		mkdir(PHOTO_PATH . $this -> year . '/misc');
		mkdir(PHOTO_PATH . $this -> year . '/blog');
	}

	public function NewDirectoryID() {
		return $this -> _newDirectoryID;
	}
		

	public function PhotosLink() {
		return "<a href='" . $this -> pages -> photos() . "'>Photos</a>";
	}
	
	public function YearLink() {
		return "<a href='" . $this -> pages -> photos() . "/year/" . $this -> year . "'>" . $this -> year . "</a>";
	}

	public function TypePhotosCategory() {
		$linkString = NULL;
		switch($this -> AlbumType) {
			case 1:
				$linkString = "<a href='" . $this -> pages -> photos() . "/type/events/" . $this -> year . "'>events</a>";
				break;
			case 2:
				$linkString = "<a href='" . $this -> pages -> photos() . "/type/blog/" . $this -> year . "'>blog</a>";
				break;
			case 3:
				$linkString = "<a href='" . $this -> pages -> photos() . "/type/misc/" . $this -> year . "'>misc</a>";
				break;
		}
		return $linkString;
	}
	
	
	public function AlbumLink() {
		return "<a href='" . $this -> pages -> photos() . "/view/album/" . $this -> albumID. "'>" . $this -> _albumName . "</a>";
	}

	public function GenerateAlbumViewBreadCrumbs() {
		return $this -> PhotosLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" .
			   $this -> YearLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" .
			   $this -> TypePhotosCategory() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . $this -> _albumName;
	}
	
	
	public function GetAddPhotosBreadCrumbs() {
		return $this -> PhotosLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> YearLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> TypePhotosCategory() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> AlbumLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>Add Photos";	
	}
	
	private function return_bytes($val) {
	    $val = trim($val);
	    $last = strtolower($val[strlen($val)-1]);
	    switch($last) {
	        // The 'G' modifier is available since PHP 5.1.0
	        case 'g':
	            $val *= 1024;
	        case 'm':
	            $val *= 1024;
	        case 'k':
	            $val *= 1024;
	    }
	
	    return $val;
	}
	
	public function ValidateUpload() {
		//http://stackoverflow.com/questions/2840755/how-to-determine-the-max-file-upload-limit-in-php
		//http://www.kavoir.com/2010/02/php-get-the-file-uploading-limit-max-file-size-allowed-to-upload.html
		$fileUploadSize = NULL;
		$this -> _photoCount = 0;
		foreach($_FILES['photoUpload']['size'] as $size) {
			$fileUploadSize += $size;
			$this -> _photoCount++;
		}
		
		if($this -> photoCount > (int)ini_get('max_file_uploads')) {
	    	$this -> json -> outputJqueryJSONObject('errorMessage', "File Upload count exceeds the maximum limit: ". ini_get('max_file_uploads'));
			return false;
	    } else if($fileUploadSize > $this -> return_bytes(ini_get('upload_max_filesize'))) {
			//$this -> json -> outputJqueryJSONObject('errorMessage', $fileUploadSize);		
       		//$this -> json -> outputJqueryJSONObject('errorMessage', $this -> return_bytes(ini_get('upload_max_filesize')));
			$this -> json -> outputJqueryJSONObject('errorMessage', "File size upload exceeds the maximum limit: ". ini_get('upload_max_filesize'));
			return false;
	    }
	    return true;
	}

	private function ValidateImageNames() {
		$ReturnValue = NULL;
		foreach ($this -> photoNames as $key => $value) {
			if(empty($value)) {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}

	private function ValidateImageAltTags() {
		$ReturnValue = NULL;
		foreach ($this -> photoAltTexts as $key => $value) {
			if(empty($value)) {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}
	
	private function ValidateImageTitleTags() {
		$ReturnValue = NULL;
		foreach ($this -> photoAltTexts as $key => $value) {
			if(empty($value)) {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}
	
	
	public function UploadMultiplePhotos() {
		$currentUploadedCount = ($this -> UploadCount) + ($this -> photoCount);
		foreach ($this -> fileUploads as $key => $file) {
		 	$photo = new Photo();
			$photo -> photoName = $this -> _albumName . '-' . $currentUploadedCount;
			$photo -> fileExt = Image::getFileExt($_FILES['files']['name'][$key]);
			$photo -> parentDirectory = $this -> year;				
			$photo -> _albumFolderName = $this -> AlbumFolderName;
			$photo -> AlbumType = $this -> AlbumType;
			$photo -> altText = $this -> _albumName;
			$photo -> uploadedImage = $_FILES['files']['name'][$key];
			$photo -> uploadedTempImage = $_FILES['files']['tmp_name'][$key];
			$photo -> titleText = $this -> _albumName;
			$photo -> AlbumID = $this -> albumID;
			$photo -> UploadedByID = $_SESSION['user'] -> _userId;
			$photo -> SavePhoto();
			$currentUploadedCount++;
		}
	}
	
	
	public function UploadPhotos() {
		
			
		try {
			set_time_limit(0);
			$latestPhotoID = NULL;
			
			$finishedLoop = 0;
			$currentUploadedCount = ($this -> UploadCount) + ($this -> photoCount);
			//$this -> _uploadProgress = "0";
			/*foreach ($this -> fileUploads as $key => $file) {
				Session::init();
				
				
				
				
				$photo = new Photo();
				$photo -> photoName = $this -> _albumName . '-' . $currentUploadedCount;
				$photo -> fileExt = Image::getFileExt($_FILES['file']['name'][$key]);
				$photo -> parentDirectory = $this -> year;				
				$photo -> _albumFolderName = $this -> AlbumFolderName;
				$photo -> AlbumType = $this -> AlbumType;
				$photo -> altText = $this -> _albumName;
				$photo -> uploadedImage = $_FILES['file']['name'][$key];
				$photo -> uploadedTempImage = $_FILES['file']['tmp_name'][$key];
				$photo -> titleText = $this -> _albumName;
				$photo -> AlbumID = $this->_id;
				$photo -> UploadedByID = $_SESSION['user'] -> _userId;
				
				
				
				$photo -> SavePhoto();
				
				
				$latestPhotoID = $photo -> GetLatestPhotoID();
				
				$finishedLoop++;
				
				
				$_SESSION['UploadImage'] = ($finishedLoop / count($this -> fileUploads)) * 100;
				//$this -> GetUploadProgress();
				//echo ($finishedLoop / count($this -> fileUploads)) * 100;
				session_write_close();
	     		
				sleep(1);
				
				$currentUploadedCount++;
				//$this -> json -> outputJqueryJSONObject('progress', $this -> UploadPhotoProgress);
			} */

			$photo = new Photo();
			$photo -> photoName = $this -> _albumName . '-' . $currentUploadedCount;
			$photo -> fileExt = Image::getFileExt($_FILES['file']['name']);
			$photo -> parentDirectory = $this -> year;				
			$photo -> _albumFolderName = $this -> AlbumFolderName;
			$photo -> AlbumType = $this -> AlbumType;
			$photo -> altText = $this -> _albumName;
			$photo -> uploadedImage = $_FILES['file']['name'];
			$photo -> uploadedTempImage = $_FILES['file']['tmp_name'];
			$photo -> titleText = $this -> _albumName;
			$photo -> AlbumID = $this->_id;
			$photo -> UploadedByID = $_SESSION['user'] -> _userId;	
				
			$photo -> SavePhoto();
				
				
			$latestPhotoID = $photo -> GetLatestPhotoID();
				


				
			$postData = array('UploadCount' => ($this -> UploadCount) + ($this -> photoCount),
							  'albumModifedDate' => date("Y-m-d", $this -> time -> NebraskaTime()) . 'T' . date("H:i:s", $this -> time -> NebraskaTime()));	
			
			
				
			$this->db->update('photoalbums', $postData, array('albumID' => $this->_id));		
			
			if(isset($this -> BlogMainPhoto)) {
				$post = BlogPost::WithID($this -> relatedBlogID);
				$post -> blogPhoto = $latestPhotoID;
				$post -> UpdateBlogMainPhoto();
				$this -> SetAlbumThumbNail($latestPhotoID);
				
				$log = new EditHistoryLog();
				$log -> itemID = $this->_id;
				$log -> userID = $_SESSION['user'] -> _userId;
				$log -> itemType = 1;		
				$log -> Save();	
								
				$this -> json -> outputJqueryJSONObject('blogPhotoSaved', true);
			} else if(isset($this -> EventMainPhoto)) {
				$event = Event::WithID($this -> relatedEventID);
				$event -> EventPhoto = $latestPhotoID;	
				$event -> UpdateEventMainPhoto();
				$this -> SetAlbumThumbNail($latestPhotoID);
				
				
				$log = new EditHistoryLog();
				$log -> itemID = $this -> relatedEventID;
				$log -> userID = $_SESSION['user'] -> _userId;
				$log -> itemType = 3;		
				$log -> Save();	
				
				$this -> json -> outputJqueryJSONObject('eventPhotoSaved', true);
			} else if(isset($this -> EventThumbNailPhoto)) {
				$event = Event::WithID($this -> relatedEventID);
				$event -> EventPhoto = $latestPhotoID;	
				
				$event -> UpdateEventThumbNailPhoto();
				
				$log = new EditHistoryLog();
				$log -> itemID = $this -> relatedEventID;
				$log -> userID = $_SESSION['user'] -> _userId;
				$log -> itemType = 3;		
				$log -> Save();	
				
				$this -> json -> outputJqueryJSONObject('eventPhotoThumbSaved', true);
			} else {
				$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> photos() . '/view/album/' . $this->_id);		
			}
			
			
			
	
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Upload Photo Error: " . $e->getMessage();
			$TrackError -> type = "UPLOAD PHOTO ERROR";
			$TrackError -> SendMessage();
		}

	}

	public function GetPhotosOfAlbum() {
		return $this -> db -> select("SELECT * FROM photos LEFT JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE photoAlbumID = " . $this->_id);
	}
	
	public function SaveThumbNail() {
		$postData = array('albumThumbNail' => $this -> albumThumbNail);	
		$this->db->update('photoalbums', $postData, array('albumID' => $this -> _id));
		$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> photos() . '/view/album/' . $this -> _id);
	}

	public function WaterMarkAlbumPhotos() {
		$photos = new PhotosList();
		
		foreach($photos -> PhotosByAlbum($this -> _id) as $photoSingle) {
			$imageSmall = PHOTO_PATH. $this -> year . $this -> getType() . $this -> AlbumFolderName . '/' . $photoSingle['photoName'] . '-s.' . $photoSingle['ext'];
			$imageMedium = PHOTO_PATH. $this -> year . $this -> getType() . $this -> AlbumFolderName . '/' . $photoSingle['photoName'] . '-s.' . $photoSingle['ext'];
			$imageLarge = PHOTO_PATH. $this -> year . $this -> getType() . $this -> AlbumFolderName . '/' . $photoSingle['photoName'] . '-s.' . $photoSingle['ext'];
			
			$this -> WaterMarkPhoto($imageSmall, $photoSingle['photoName'] . '-s.' . $photoSingle['ext'], $photoSingle['ext']);
			$this -> WaterMarkPhoto($imageMedium, $photoSingle['photoName'] . '-m.' . $photoSingle['ext'], $photoSingle['ext']);
			$this -> WaterMarkPhoto($imageLarge, $photoSingle['photoName'] . '-l.' . $photoSingle['ext'], $photoSingle['ext']);
		
			$postData = array('photowatermark' => 1);	
			$this->db->update('photos', $postData, array('photoID' => $photoSingle['photoID']));
		}

		$this -> redirect -> redirectPage(PATH. 'photos/view/album/'. $this -> _id);
		
	}

	private function WaterMarkPhoto($img, $newImage, $ext) {
		$settings = SettingsObject::Init();
		
		$waterMark = imagecreatefrompng(PHOTO_PATH . 'WaterMark/' . $settingsObj -> WaterMarkImage);
		
		if(strtolower($ext) == "gif") { 
			$image = imagecreatefromgif($img);
    	} else if(strtolower($ext) == "png") { 
    		$image = imagecreatefrompng($img);
		} else {
			$image = imagecreatefromjpeg($img); 
    	}
		
		
		
		$marge_right = 0;
		$marge_bottom = 0;
		$sx = imagesx($waterMark);
		$sy = imagesy($waterMark);
		$sximg = imagesx($image);
		
		$percent = $sximg * 0.15;
		
		//Create the final resized watermark stamp
	    $dest_image = imagecreatetruecolor($percent, $percent);
		
	    //keeps the transparency of the picture
	    imagealphablending( $dest_image, false );
	    imagesavealpha( $dest_image, true );
	    
	    //resizes the stamp
	    imagecopyresampled($dest_image, $waterMark, 0, 0, 0, 0, $percent, $percent, $sx, $sy);
		
		imagecopy($image, 
				  $dest_image, 
				  imagesx($image) - imagesx($dest_image) - $marge_right, 
				  imagesy($image) - imagesy($dest_image) - $marge_bottom, 
				  0, 
				  0, 
				  $percent, 
				  $percent);
		
		if(strtolower($ext) == "gif") {
			imagegif($image, PHOTO_PATH . $this -> year . $this -> getType(). $this -> AlbumFolderName . '/' . $newImage);	
		} else if(strtolower($ext) == "png") {
			imagepng($image, PHOTO_PATH . $this -> year . $this -> getType(). $this -> AlbumFolderName . '/' . $newImage);	
    	} else {
    		imagejpeg($image, PHOTO_PATH . $this -> year . $this -> getType(). $this -> AlbumFolderName . '/' . $newImage); 
    	}	
		
		
		
	}

	private function CreateYearDirectory() {
		
	} 
	
	
	public function Delete() {
		Folder::DeleteDirectory(PHOTO_PATH . $this -> year . $this -> getType(). $this -> AlbumFolderName);
		
		//delete photos	
		$photosDelete = $this -> db -> prepare('DELETE FROM photos WHERE photoAlbumID = :photoAlbumID');
		$photosDelete -> execute(array(":photoAlbumID" => $this -> albumID));
		
		//delete album	
		$photosDelete = $this -> db -> prepare('DELETE FROM photoalbums WHERE albumID = :albumID');
		$photosDelete -> execute(array(":albumID" => $this -> albumID));
		
		
		
		$this -> redirect -> redirectPage(PATH. 'photos/type'. $this -> getType() . $this -> year);
		
	}

	
	private function send_message($id, $message, $progress) {
	    $d = array('message' => $message , 'progress' => $progress);
	      
	    echo "id: $id" . PHP_EOL;
	    echo "data: " . json_encode($d) . PHP_EOL;
	    echo PHP_EOL;
	      
	    ob_flush();
	    flush();
	}
	


}