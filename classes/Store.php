<?php

class Store extends BaseObject {
	
	
	private $_id;
	private $StoreID; 
	
	public $StoreImage;
	public $storeName;
	public $seoURL;
	public $MetaData;
	public $weeklyProgress;
	
	public $CSVName;
	private $csvFile;
	
	public $WaterMarkImage;
	
	public $ContactUsEmail;
	public $ScheduleTestRide;
	public $OnlineOffer;
	public $TradeValue;
	public $SendToFriendBcc;
	public $ServicDeptEmail;
	public $PartsDeptEmail;
	public $ApparelDeptEmail;
	
	public $NewWaterMarkName;
	public $NewWaterMarkTempName;
		
	
    public function __sleep() {
        parent::__sleep();
		 return array('_id');
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($storeID) {
        $instance = new self();
        $instance -> _id = $storeID;
        $instance -> loadById();
        return $instance;
    }
	
	public static function WithBrand($name) {
        $instance = new self();
        $instance -> storeName = $name;
        $instance -> loadByStoreName();
        return $instance;
    }
	
	public static function WithCSV($CSV) {
		$instance = new self();
        $instance -> csvFile = $CSV;
        $instance -> loadByCSV();
        return $instance;
	}
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT *, ( ((SELECT LEAST(COUNT(*), (SELECT socialMediaFacebook FROM settings)) FROM socialmediaposts WHERE submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = :storeID AND socialMediaType = 1) + (SELECT LEAST(COUNT(*), (SELECT socialMediaTwitter FROM settings)) FROM socialmediaposts WHERE submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = :storeID AND socialMediaType = 2)) / (SELECT (socialMediaFacebook + socialMediaTwitter) WeeklyProgress FROM settings) ) WeeklyPercentage FROM stores WHERE storeID = :storeID');
        $sth->execute(array(':storeID' => $this->_id,
								':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
								':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));	
			
        $record = $sth -> fetch();
        $this->fill($record);
    }

	protected function loadByCSV() {
		$sth = $this -> db -> prepare('SELECT * FROM stores WHERE CSVInventory = :CSVInventory');
        $sth->execute(array(':CSVInventory' => $this -> csvFile));
		$record = $sth -> fetch();
        $this->fill($record);
	}

	protected function loadByStoreName() {
		$sth = $this -> db -> prepare('SELECT * FROM stores WHERE seoUrl = :seoUrl');
        $sth->execute(array(':seoUrl' => $this -> storeName));
		$record = $sth -> fetch();
        $this->fill($record);
	}
	
    protected function fill(array $row){
    	$this -> StoreID = $row['storeID'];
		$this -> storeName = $row['StoreName'];
    	$this -> seoURL = $row['seoUrl'];
		$this -> MetaData = $row['MetaData'];
		if(isset($this -> _id)) {
			$this -> weeklyProgress = $row['WeeklyPercentage'];	
		}
		$this -> CSVName = $row['CSVInventory'];
		$this -> WaterMarkImage = $row['WaterMarkImage'];
		$this -> ContactUsEmail = $row['contactUsEmail'];
		$this -> ScheduleTestRide = $row['ScheduleTestRideEmail'];
		$this -> OnlineOffer = $row['OnlineOfferEmail'];
		$this -> TradeValue = $row['TradeValueEmail'];
		$this -> SendToFriendBcc = $row['SendToFriendBcc'];
		$this -> StoreImage = $row['storeImage'];
    }	

	public function GetID() {
		return $this -> StoreID;
	}

	public function GetStoreImage() {
		return PHOTO_URL . 'Stores/' . $this -> StoreImage;
	}

	
	public function ValidateNotifications() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> ContactUsEmail)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> ScheduleTestRide)) {
			array_push($validationErrors, array('inputID' => 2,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> OnlineOffer)) {
			array_push($validationErrors, array('inputID' => 3,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> TradeValue)) {
			array_push($validationErrors, array('inputID' => 4,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> sendToFriendEmail)) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> ServicDeptEmail)) {
			array_push($validationErrors, array('inputID' => 6,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> PartsDeptEmail)) {
			array_push($validationErrors, array('inputID' => 7,
												'errorMessage' => 'Required'));
		}
		
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function ValidateWaterMark() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> NewWaterMarkName)) {
			array_push($validationErrors, array('inputID' => 6,
												'errorMessage' => 'An Image is required to upload a new water mark photo'));
		}
			
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	private function NewWaterMarkPhoto($storeWaterMarkImage) {
		$ImagePathParts = array();
		Image::MoveAndRenameImage($this -> NewWaterMarkTempName, 
								  $ImagePathParts,
								  $this -> NewWaterMarkName,
								  $storeWaterMarkImage, 11);
	}
	
	public function SaveNotifications() {
		$storeUpdateDB = array();
		switch($this -> _id) {
			case 2:
				$waterMarkImage = 'MotorSportWaterMark';
				break;
			case 3:
				$waterMarkImage = 'HarleyWaterMark';
				break;
			case 4:
				$waterMarkImage = 'IndianWaterMark';
				break;
		}
		
		$storeUpdateDB['contactUsEmail'] = $this -> ContactUsEmail;
		$storeUpdateDB['OnlineOfferEmail'] = $this -> OnlineOffer;
		$storeUpdateDB['ScheduleTestRideEmail'] = $this -> ScheduleTestRide;
		$storeUpdateDB['TradeValueEmail'] = $this -> TradeValue;
		$storeUpdateDB['SendToFriendBcc'] = $this -> sendToFriendEmail;
		$storeUpdateDB['ServiceEmailNotification'] = $this -> ServicDeptEmail;
		$storeUpdateDB['PartsEmailNotification'] = $this -> PartsDeptEmail;
		$storeUpdateDB['ApparelEmailNotification'] = $this -> ApparelDeptEmail;
		
		
		if(!empty($this -> NewWaterMarkName)) {
			$this -> NewWaterMarkPhoto($waterMarkImage);
			
			$storeUpdateDB['WaterMarkImage'] = $waterMarkImage . '.' . Image::getFileExt($this -> NewWaterMarkName);					  
				
		}
				
		$this->db->update('stores', $storeUpdateDB, array('storeID' => $this -> _id));
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'settings/stores');	
		
	}
	
	public function SaveWaterMark() {
		
		
		
		$postData = array('WaterMarkImage' => $waterMarkImage . '.' . Image::getFileExt($this -> NewWaterMarkName));	
				
		$this->db->update('stores', $postData, array('storeID' => $this -> _id));
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'settings/edit/store/'. $this -> _id);
	}
	
	public function Save() {
		try {
	
			$postData = array('MetaData' => $this -> SaveMetaData());	
				
			$this->db->update('stores', $postData, array('storeID' => $this -> _id));
				
			$this -> redirect -> redirectPage($this -> pages -> specials() . '/brand/' . $this -> seoURL .'/Service');
			
			$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> specials() . '/' . $this -> seoURL .'/Service');
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Store Save Error: " . $e->getMessage();
			$TrackError -> type = "STORE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	



}