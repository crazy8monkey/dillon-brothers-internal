<?php

class InventoryBackupHistory extends BaseObject {
	
	private $_id;
	
	public $InventoryBackupID;
	
	
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($backupID) {
        $instance = new self();
        $instance->_id = $backupID;
        $instance->loadById();
        return $instance;
    }
	
	
	
	
	public function Save() {
		try {
			
			Session::init();
			
			$newBackup = $this -> db -> insert('inventorybackuphistory', array('backedupInventoryBackupID' => $this -> InventoryBackupID,
																			   'date' => date("Y-m-d", $this -> time -> NebraskaTime()),
																			   'time' => date("H:i:s", $this -> time -> NebraskaTime()),
																			   'editedByUserID' => $_SESSION['user'] -> _userId));																   
																 
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Employee Save Error: " . $e->getMessage();
			$TrackError -> type = "EMPLOYEE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}

	


	
	


}