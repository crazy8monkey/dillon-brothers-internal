<?php

class SettingSMSNumber extends BaseObject {

	public $SMSDeptID;

	public $SalesSMSNumber;
	public $ServiceSMSNumber;
	public $PartsSMSNumber;

    public function __sleep()
    {
        parent::__sleep();
    }

    public function __wakeup()
    {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }

    public static function Init(){
        $instance = new self();
        $instance->loadSettings();
        return $instance;
    }


    protected function loadSettings() {
        $sth = $this -> db -> prepare('SELECT * FROM settings WHERE settingsID = 1');
        $sth -> execute();
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){

    }

	public function ValidateSMS() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> SalesSMSNumber)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		} 
		
		
		if($this -> validate -> emptyInput($this -> ServiceSMSNumber)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		} 
		
		if($this -> validate -> emptyInput($this -> PartsSMSNumber)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Required'));
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}	
	}
	

    public function Save($type) {
    	try {
		
			switch($type) {
				case "storesms":		
					$updateNumberDB = array();
					foreach($this -> SMSDeptID as $key => $smsID) {
						if($key == 0) {
							$updateNumberDB['deptNumber'] = preg_replace("/[^0-9]/", "", $this -> SalesSMSNumber);
						}
						if($key == 1) {
							$updateNumberDB['deptNumber'] = preg_replace("/[^0-9]/", "", $this -> ServiceSMSNumber);
						}
						if($key == 2) {
							$updateNumberDB['deptNumber'] = preg_replace("/[^0-9]/", "", $this -> PartsSMSNumber);
						}
						
						$this->db->update('settingsmsnumbers', $updateNumberDB, array('smsdeptID' => $smsID));	
					}			
					
					
									  
					break;
			
			}
			
			
		
			$this -> json -> outputJqueryJSONObject("redirect", PATH . 'settings/sms');
		} catch (Exception $e) {
			
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_GENERAL_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			if(isset($this->_userId)) {
				$this -> json -> outputJqueryJSONObject("MySqlError", $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} else {
				$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				$this -> json -> outputJqueryJSONObject("redirect", PATH . "cms/users");
			}
			
		
		}

    }

	


}