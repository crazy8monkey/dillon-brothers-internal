<?php


class BlogCategory extends BaseObject {
	
	private $_id;
	
	public $blogCategoryName;
	
	private $RelatedBlogID;
	
	
	public $structuredDataDescription;
		
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($eventID) {
        $instance = new self();
        $instance->_id = $eventID;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM blogpostcategories LEFT JOIN blogcatories ON blogpostcategories.blogPostCategoryID = blogcatories.BlogCategoryID WHERE blogPostCategoryID = :blogPostCategoryID');
        $sth->execute(array(':blogPostCategoryID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
		$this -> blogCategoryName = $row['blogCategoryName'];	
		$this -> RelatedBlogID = $row['RelatedBlogPostID'];
    }
	
	
	
	public function Validate() {
		$validationErrors = array();
		
		if(!isset($this->_id)) {
            $categoryNameCheck = $this->db->prepare('SELECT blogCategorySEOUrl FROM blogpostcategories WHERE blogCategorySEOUrl = ?');
            $categoryNameCheck -> execute(array(parent::seoUrlSpaces($this -> blogCategoryName)));
        } else {
            $categoryNameCheck = $this->db->prepare('SELECT blogCategorySEOUrl FROM blogpostcategories WHERE blogCategorySEOUrl = :blogCategorySEOUrl AND blogPostCategoryID <> :blogPostCategoryID');
            $categoryNameCheck ->execute(array(':blogCategorySEOUrl' => parent::seoUrlSpaces($this -> blogCategoryName), ':blogPostCategoryID' => $this->_id));
        }
		
		
		
		
		if (count($categoryNameCheck -> fetchAll())) {
            array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'There is already a category created in this name'));
        }
				
					
		
		
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	

	public function GetID() {
		return $this->_id;
	}

	
	public function Save() {
		try {
			$redirectID = NULL;
			if(isset($this->_id)) {
				
				$postData = array('blogCategoryName' => $this -> blogCategoryName,
								  'blogCategorySEOUrl' => parent::seoUrlSpaces($this -> blogCategoryName));		
			
				$this->db->update('blogpostcategories', $postData, array('blogPostCategoryID' => $this -> _id));
				
				
				
				$redirectID = $this->_id;
			} else {
				$publishedDate = date("Y-m-d", $this -> time -> NebraskaTime()) . 'T' . date("H:i:s", $this -> time -> NebraskaTime());
				
				
				$newInsertData = array('blogCategoryName' => $this -> blogCategoryName,
									   'blogCategorySEOUrl' => parent::seoUrlSpaces($this -> blogCategoryName),
									   'blogCategoryPublishedTime' => $publishedDate);
				
				$NewBlogPost = $this -> db -> insert('blogpostcategories', $newInsertData);	
														 
				$redirectID = $NewBlogPost;										 
			}
			
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'blog/edit/category/' . $redirectID);
			
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Event Save Error: " . $e->getMessage();
			$TrackError -> type = "EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	
	
	
	
	
	
	
	

	
	public function Delete() {
		//this should be linking by blog posts
		$blogPosts = new BlogPostList();
		
		$blogPostID = NULL;
		
		$BlogCount = count($blogPosts -> BlogsByCategory($this -> _id));
		
		if(count($blogPosts -> BlogsByCategory($this -> RelatedBlogID)) > 1) {
			foreach($blogPosts -> BlogsByCategory($this -> RelatedBlogID) as $key => $blogSingle) {
				$blogcategorydelete = $this -> db -> prepare('DELETE FROM blogcatories WHERE BlogCategoryID = :BlogCategoryID');
				$blogcategorydelete -> execute(array(":BlogCategoryID" => $this -> _id));
			}
		} else {
			$postData = array('BlogCategoryID' => 0);		
			$this->db->update('blogcatories', $postData, array('RelatedBlogPostID' => $this -> RelatedBlogID));	
		}
			
		
		$sth = $this -> db -> prepare("DELETE FROM blogpostcategories WHERE blogPostCategoryID = :blogPostCategoryID");
		$sth -> execute(array(':blogPostCategoryID' => $this -> _id));	
		
		
		$this -> redirect -> redirectPage(PATH. 'blog/categories');
	}	
	

		

}