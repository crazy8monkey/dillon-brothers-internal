<?php


class Event extends BaseObject {
	
	private $_id;
	
	public $eventName;
	public $eventHtmlJSON;
	
	public $eventImage;
	public $startDate;
	public $startTime;
	public $endDate;
	public $endTime;
	
	public $CurrentSEOUrl;
	public $seoURL;
	
	public $albumName;
	public $albumFolderName;
	public $AlbumID;
	public $ParentDirectory;
	
	public $Store;
	public $storeIDS;
	
	public $PerformerInfo;
	public $EventPrice;
	public $Longitutde;
	Public $Latitude;
	
	
	public $EventPhoto;
	public $SuperEventPhoto;
	public $GoogleEventID;
	
	public $visible;
	
	public $address;
	public $city;
	public $state;
	public $zip;
	public $ParentAlbumDirectory;
	
	
	public $GoogleEventDescription;

	
	public $CurrentEventImageBig;
	public $CurrentEventImageSmall;
	
	public $PublishedDateFull;
	
	public $SuperEvent;
	public $SubEvent;
	public $SuperEventID;
	public $_CurrentSuperEventID;
	
	public $SuperEventDescription;
	public $SuperEventName;
	public $SuperEventURL;
	
	public $StructuredData;
	public $photoName;
	public $photExt;
	
	public $EventIsPublished;
	
	public $SuperEventIDString;
	public $EventDirectory;
	public $ShortLinkID;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($eventID) {
        $instance = new self();
        $instance->_id = $eventID;
        $instance->loadByID();
        return $instance;
    }
	
	public static function WithHTMLID($eventID) {
		$instance = new self();
        $instance->_id = $eventID;
        $instance->loadByHTMLID();
        return $instance;
	}
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT *, (SELECT eventName FROM events WHERE eventID = subevents.SuperEventID) SuperEventName, 
    											 (SELECT seoUrl FROM events WHERE eventID = subevents.SuperEventID) SuperEventURL, 
    											 (CONCAT(EventMainImage.photoName, "-l.", EventMainImage.ext)) MainEventImageImage,
    											 (CONCAT(EventThumbNailImage.photoName, "-l.", EventThumbNailImage.ext)) ThumbNailImage FROM events 
    											 LEFT JOIN photoalbums ON events.albumID = photoalbums.albumID LEFT JOIN subevents ON events.eventID = subevents.SubEventID 
    											 LEFT JOIN photos AS EventMainImage ON events.eventImage = EventMainImage.photoID 
    											 LEFT JOIN photos AS EventThumbNailImage ON events.eventThumbImage = EventThumbNailImage.photoID 
    											 LEFT JOIN shortlinks ON events.eventID = shortlinks.relatedEventID WHERE eventID = :eventID');
        $sth->execute(array(':eventID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	protected function loadByHTMLID() {
		$sth = $this -> db -> prepare('SELECT * FROM events WHERE eventID = :eventID');
        $sth->execute(array(':eventID' => $this->_id));
        $record = $sth -> fetch();
        $this->fillHtml($record);
	}

    protected function fill(array $row){
    	$this -> eventName = $row['eventName'];
		$this -> startDate = $row['startDate'];
		$this -> startTime = $row['startTime'];
		$this -> endDate = $row['endDate'];
		$this -> endTime = $row['endTime'];
		$this -> storeIDS = $row['stores'];
    	$this -> eventHtmlJSON = $row['eventHTMLJSON'];
		$this -> albumName = $row['albumName'];
		$this -> albumFolderName = $row['albumFolderName'];
		$this -> ParentDirectory = $row['ParentDirectory'];
		$this -> AlbumID = $row['albumID'];
		$this -> CurrentEventImageBig = $row['MainEventImageImage'];
		$this -> CurrentEventImageSmall = $row['ThumbNailImage'];
		$this -> visible = $row['eventVisible'];
		$this -> GoogleEventID = $row['GoogleEventID'];
		$this -> address = $row['eventAddress'];
		$this -> city = $row['eventCity'];
		$this -> state = $row['eventState'];
		$this -> zip = $row['eventZip'];
		$this -> GoogleEventDescription = $row['GoogleEventDescription'];
		$this -> SuperEvent = $row['SuperEvent'];
		$this -> SubEvent = $row['SubEvent'];
		$this -> CurrentSEOUrl = $row['seoUrl'];
		$this -> SuperEventDescription = $row['SuperEventDescription'];
		$this -> _CurrentSuperEventID = $row['SuperEventID'];
		$this -> SuperEventName = $row['SuperEventName'];
		$this -> SuperEventURL = $row['SuperEventURL'];
		$this -> ParentAlbumDirectory = $row['ParentDirectory'];
		$this -> StructuredData = $row['StructuredDataInfo'];
		$this -> photoName = $row['photoName'];
		$this -> photExt = $row['ext'];
		$this -> PublishedDateFull = $row['publishDateFull'];
		$this -> EventDirectory = $row['EventDirectory'];
		$this -> ShortLinkID = $row['shortLinkID'];
    }

	protected function fillHtml(array $row) {
		$this -> eventHtmlJSON = $row['eventHTMLJSON'];
	}
	
	public function GetMainPhoto() {
		$image = NULL;	
		if($this -> SuperEvent == 1) {
			$image = PHOTO_URL . 'SuperEvents/' . $this -> CurrentEventImageBig;
		} else {
			$image = PHOTO_URL . $this -> ParentDirectory . '/events/' . $this -> albumFolderName . '/' . $this -> photoName . '-m.' . $this -> photExt;	
		}
		return $image;
	}
	
	public function GetEventID() {
		return $this -> _id;
	}
	
	public function ValidateSuperEvent() {
		$validationErrors = array();
		
		//$EventYear = explode("-", $this -> startDate);
		
		if(isset($this->_id)) {
			$duplicateSuperEventName = $this -> db -> prepare('SELECT * FROM events WHERE SuperEvent = 1 AND eventName = :eventName AND eventID <> :eventID');
			$duplicateSuperEventName -> execute(array(":eventName" => $this -> eventName,
													  ":eventID" => $this->_id));
													  
			
			$shortLinkCheck = $this -> db -> prepare('SELECT shortUrl FROM shortlinks WHERE shortUrl = :shortUrl AND shortLinkActive = 1 AND shortLinkID <> :shortLinkID');
			$shortLinkCheck -> execute(array(":shortUrl" => $this -> seoURL,
											 ":shortLinkID" => $this -> ShortLinkID));
													  
		} else {
			$duplicateSuperEventName = $this -> db -> prepare('SELECT * FROM events WHERE SuperEvent = 1 AND eventName = ?');
			$duplicateSuperEventName -> execute(array($this -> eventName));	
			
			$shortLinkCheck = $this -> db -> prepare('SELECT shortUrl FROM shortlinks WHERE shortLinkActive = 1 AND shortUrl = ?');
			$shortLinkCheck -> execute(array($this -> seoURL));
		}
		
		//if(isset($this->_id)) {
				
				
		//	$duplicateSEOSlug = $this -> db -> prepare('SELECT * FROM events WHERE SuperEvent = 1 AND seoUrl = :seoUrl AND eventID <> :eventID');
		//	$duplicateSEOSlug -> execute(array(":seoUrl" => $this -> seoURL,
		//									   ":eventID" => $this->_id));
			
		//	if($duplicateSEOSlug -> fetchAll()) {
		//		array_push($validationErrors, array('inputID' => 20,
		//											'errorMessage' => 'There is a URL Slug associated with a different Super Event, please type in a Unique Slug name'));					   
		//	}
			
		//}		
		
		if(isset($this->_id)) {
			if($this -> validate -> emptyInput($this -> seoURL)) {
				array_push($validationErrors, array('inputID' => 20,
													'errorMessage' => 'Required'));
			}	
		} else {
			if (count($shortLinkCheck -> fetchAll())) {
		     	array_push($validationErrors, array('inputID' => 20,
													'errorMessage' => 'There is already a short url already entered in the system, please type in a unique short name'));
		    }
		}
		
		
		if($this -> validate -> emptyInput($this -> eventName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if($duplicateSuperEventName -> fetchAll()) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'There is a Super Event entered in the system, please choose a different super event name'));					   
		}
		
		if($this -> validate -> emptyInput($this -> SuperEventDescription)) {
			array_push($validationErrors, array('inputID' => 14,
												'errorMessage' => 'Required'));
		}
			
		if($this -> validate -> emptyInput($this -> SuperEventIDString)) {
			array_push($validationErrors, array('inputID' => 19,
												'errorMessage' => 'Required'));
		}
		
	
		if($this -> validate -> emptyInput($this -> PerformerInfo)) {
			array_push($validationErrors, array('inputID' => 15,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> EventPrice)) {
			array_push($validationErrors, array('inputID' => 16,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> Latitude)) {
			array_push($validationErrors, array('inputID' => 17,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> Longitutde)) {
			array_push($validationErrors, array('inputID' => 18,
												'errorMessage' => 'Required'));
		}
		
		
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function ValidateSubEvent() {
		$validationErrors = array();
		
		$EventYear = explode("-", $this -> startDate);
		
		if($this -> SuperEventID == 0) {
			array_push($validationErrors, array('inputID' => 20,
												'errorMessage' => 'Required'));
		}
		
		if(isset($this->_id)) {
			$duplicateSubEventName = $this -> db -> prepare('SELECT * FROM events LEFT JOIN subevents ON events.eventID = subevents.SubEventID WHERE SubEvent = 1 AND eventName = :eventName 
																																								  AND SuperEventID = :SuperEventID 
																																								  AND events.eventID <> :eventID
																																								  AND EventDirectory = :EventDirectory');
			$duplicateSubEventName -> execute(array(":eventName" => $this -> eventName,
													":SuperEventID" => $this -> SuperEventID,
													":eventID" => $this->_id,
													":EventDirectory" => $EventYear[0]));
													
													
			$duplicateSEOSlug = $this -> db -> prepare('SELECT * FROM events LEFT JOIN subevents ON events.eventID = subevents.SubEventID WHERE SubEvent = 1 AND seoUrl = :seoUrl 
																																								  AND SuperEventID = :SuperEventID 
																																								  AND events.eventID <> :eventID
																																								  AND EventDirectory = :EventDirectory');
			$duplicateSEOSlug -> execute(array(":seoUrl" => $this -> seoURL,
											   ":SuperEventID" => $this -> SuperEventID,
											   ":eventID" => $this->_id,
											   ":EventDirectory" => $EventYear[0]));
											   
			$shortLinkCheck = $this -> db -> prepare('SELECT shortUrl FROM shortlinks WHERE shortUrl = :shortUrl AND shortLinkID <> :shortLinkID');
			$shortLinkCheck -> execute(array(":shortUrl" => $this -> seoURL,
											 ":shortLinkID" => $this -> ShortLinkID));								   
											   
			
			if (count($shortLinkCheck -> fetchAll())) {
				array_push($validationErrors, array('inputID' => 21,
													'errorMessage' => 'There is a URL Slug associated with a different Sub Event, please type in a Unique Slug name'));					   
			}

			//if(isset($this->_id)) {
			//	if($this -> validate -> emptyInput($this -> seoURL)) {
			//		array_push($validationErrors, array('inputID' => 20,
			//											'errorMessage' => 'Required'));
			//	}	
			//} else {
			//	if (count($shortLinkCheck -> fetchAll())) {
			//     	array_push($validationErrors, array('inputID' => 20,
			//											'errorMessage' => 'There is already a short url already entered in the system, please type in a unique short name'));
			//    }
			//}
			
			
													
		} else {
			
			$duplicateSubEventName = $this -> db -> prepare('SELECT * FROM events LEFT JOIN subevents ON events.eventID = subevents.SubEventID WHERE SubEvent = 1 AND eventName = :eventName 
																																								  AND SuperEventID = :SuperEventID 
																																								  AND EventDirectory = :EventDirectory');
			$duplicateSubEventName -> execute(array(":eventName" => $this -> eventName,
													":SuperEventID" => $this -> SuperEventID,
													":EventDirectory" => $EventYear[0]));		
		}
		
		
		
		
		
		if($this -> validate -> emptyInput($this -> eventName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if($this -> SuperEventID != 0 && !empty($this -> startDate)) {
			if($duplicateSubEventName -> fetchAll()) {
				array_push($validationErrors, array('inputID' => 1,
													'errorMessage' => 'There is a Sub Event entered in the system within the same year, please choose a different sub event name'));					   
			}	
		}
		
		
		
		
		if($this -> validate -> emptyInput($this -> startDate)) {
			array_push($validationErrors, array('inputID' => 3,
												'errorMessage' => 'Required'));
		}	
			
		if($this -> validate -> emptyInput($this -> startTime)) {
			array_push($validationErrors, array('inputID' => 4,
												'errorMessage' => 'Required'));
		}
			
			
		$startDate = new DateTime($this -> startDate);
		$endDate = new DateTime($this -> endDate);	
			
		if($this -> validate -> emptyInput($this -> endDate)) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => 'Required'));
		} else if($startDate > $endDate) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => "End date can't be below the start date"));
		}
			
		if($this -> validate -> emptyInput($this -> endDate)) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => 'Required'));
		}
			
		if($this -> validate -> emptyInput($this -> endTime)) {
			array_push($validationErrors, array('inputID' => 6,
												'errorMessage' => 'Required'));
		}
	
		if($this -> validate -> emptyInput($this -> PerformerInfo)) {
			array_push($validationErrors, array('inputID' => 15,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> EventPrice)) {
			array_push($validationErrors, array('inputID' => 16,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> Latitude)) {
			array_push($validationErrors, array('inputID' => 17,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> Longitutde)) {
			array_push($validationErrors, array('inputID' => 18,
												'errorMessage' => 'Required'));
		}
		
		if($this -> Store == '0') {
			array_push($validationErrors, array('inputID' => 7,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> GoogleEventDescription)) {
			array_push($validationErrors, array('inputID' => 13,
												'errorMessage' => 'Required'));
		}
		
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function ValidateStandaloneEvent() {
		$validationErrors = array();
		$EventYear = explode("-", $this -> startDate);
		
		if(isset($this->_id)) {
			$duplicateSubEventName = $this -> db -> prepare('SELECT * FROM events LEFT JOIN subevents ON events.eventID = subevents.SubEventID WHERE SubEvent = 1 AND eventName = :eventName AND SuperEventID = :SuperEventID AND events.eventID <> :eventID');
			$duplicateSubEventName -> execute(array(":eventName" => $this -> eventName,
													":SuperEventID" => $this -> SuperEventID,
													":eventID" => $this->_id));
													
			$shortLinkCheck = $this -> db -> prepare('SELECT shortUrl FROM shortlinks WHERE shortUrl = :shortUrl AND shortLinkActive = 1 AND shortLinkID <> :shortLinkID AND CurrentYear = :CurrentYear');
			$shortLinkCheck -> execute(array(":shortUrl" => $this -> seoURL,
											 ":shortLinkID" => $this -> ShortLinkID,
											 ":CurrentYear" => $EventYear[0]));										
													
		} else {
			$duplicateSubEventName = $this -> db -> prepare('SELECT * FROM events LEFT JOIN subevents ON events.eventID = subevents.SubEventID WHERE SubEvent = 1 AND eventName = :eventName AND SuperEventID = :SuperEventID');
			$duplicateSubEventName -> execute(array(":eventName" => $this -> eventName,
													":SuperEventID" => $this -> SuperEventID));												
			
			$shortLinkCheck = $this -> db -> prepare('SELECT shortUrl FROM shortlinks WHERE shortLinkActive = 1 AND shortUrl = :shortUrl AND CurrentYear = :CurrentYear');
			$shortLinkCheck -> execute(array(":shortUrl" => $this -> seoURL, ":CurrentYear" => $EventYear[0]));										
													
		}
		
		
		
		if($this -> validate -> emptyInput($this -> eventName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if($this -> SuperEventID != 0) {
			if($duplicateSubEventName -> fetchAll()) {
				array_push($validationErrors, array('inputID' => 1,
													'errorMessage' => 'There is a Sub Event entered in the system, please choose a different super event name'));					   
			}	
		}
		
		
		
		
		if($this -> validate -> emptyInput($this -> startDate)) {
			array_push($validationErrors, array('inputID' => 3,
												'errorMessage' => 'Required'));
		}	
			
		if($this -> validate -> emptyInput($this -> startTime)) {
			array_push($validationErrors, array('inputID' => 4,
												'errorMessage' => 'Required'));
		}
			
			
		$startDate = new DateTime($this -> startDate);
		$endDate = new DateTime($this -> endDate);	
			
		if($this -> validate -> emptyInput($this -> endDate)) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => 'Required'));
		} else if($startDate > $endDate) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => "End date can't be below the start date"));
		}
			
		if($this -> validate -> emptyInput($this -> endDate)) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => 'Required'));
		}
			
		if($this -> validate -> emptyInput($this -> endTime)) {
			array_push($validationErrors, array('inputID' => 6,
												'errorMessage' => 'Required'));
		}
	
		if($this -> validate -> emptyInput($this -> PerformerInfo)) {
			array_push($validationErrors, array('inputID' => 15,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> EventPrice)) {
			array_push($validationErrors, array('inputID' => 16,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> Latitude)) {
			array_push($validationErrors, array('inputID' => 17,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> Longitutde)) {
			array_push($validationErrors, array('inputID' => 18,
												'errorMessage' => 'Required'));
		}
		
		if($this -> Store == '0') {
			array_push($validationErrors, array('inputID' => 7,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> GoogleEventDescription)) {
			array_push($validationErrors, array('inputID' => 13,
												'errorMessage' => 'Required'));
		}
		
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Validate() {
		$validationErrors = array();
		
		
		
		if($this -> validate -> emptyInput($this -> eventName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if($this -> SubEvent == 1) {
			$duplicateSubName = $this -> db -> prepare('SELECT * FROM events LEFT JOIN subevents ON events.eventID = subevents.SubEventID WHERE events.SubEvent = 1 AND 
																																			  events.EventDirectory = :EventDirectory AND 
																																			  events.eventID <> :eventID AND 
																																			  events.eventName = :eventName AND
																																			  subevents.SuperEventID = :SuperEventID');
			$duplicateSubName -> execute(array(":EventDirectory" => $this -> EventDirectory, 
											   ":eventID" => $this -> _id, 
											   ":eventName" => $this -> eventName,
											   ":SuperEventID" => $this -> _CurrentSuperEventID));	
											   
			if($duplicateSubName -> fetchAll()) {
				array_push($validationErrors, array('inputID' => 1,
													'errorMessage' => 'There is an sub event name associated with a different event, please choose a different name'));
			}								   
											   
		}
		
		
		
		
		if($this -> SubEvent != '0') {
			if(isset($this -> _id)) {
				if($this -> validate -> emptyInput($this -> eventHtmlJSON)) {
					array_push($validationErrors, array('inputID' => 2,
														'errorMessage' => 'Please create your page structure'));
				}	
			}			
		}
		
		
		if($this -> SuperEvent == '0') {
			if($this -> validate -> emptyInput($this -> startDate)) {
				array_push($validationErrors, array('inputID' => 3,
													'errorMessage' => 'Required'));
			}	
			
			if($this -> validate -> emptyInput($this -> startTime)) {
				array_push($validationErrors, array('inputID' => 4,
													'errorMessage' => 'Required'));
			}
			
			
			$startDate = new DateTime($this -> startDate);
			$endDate = new DateTime($this -> endDate);	
			
			if($this -> validate -> emptyInput($this -> endDate)) {
				array_push($validationErrors, array('inputID' => 5,
													'errorMessage' => 'Required'));
			} else if($startDate > $endDate) {
				array_push($validationErrors, array('inputID' => 5,
													'errorMessage' => "End date can't be below the start date"));
			}
			
			if($this -> validate -> emptyInput($this -> endDate)) {
				array_push($validationErrors, array('inputID' => 5,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> endTime)) {
				array_push($validationErrors, array('inputID' => 6,
													'errorMessage' => 'Required'));
			}
			
			
		}
		
		
		
		
		
		
		if($this -> Store == '0') {
			array_push($validationErrors, array('inputID' => 7,
												'errorMessage' => 'Required'));
		}
		
		
		
		if($this -> SuperEvent == '0') {
			
			if($this -> validate -> emptyInput($this -> address)) {
				array_push($validationErrors, array('inputID' => 9,
													'errorMessage' => 'Required'));
			}	
			
			if($this -> validate -> emptyInput($this -> city)) {
				array_push($validationErrors, array('inputID' => 10,
													'errorMessage' => 'Required'));
			}	
			
			if($this -> validate -> emptyInput($this -> state)) {
				array_push($validationErrors, array('inputID' => 11,
													'errorMessage' => 'Required'));
			}	
			
			if($this -> validate -> emptyInput($this -> zip)) {
				array_push($validationErrors, array('inputID' => 12,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> GoogleEventDescription)) {
				array_push($validationErrors, array('inputID' => 13,
													'errorMessage' => 'Required'));
			}
			
			
		}
		
		if($this -> SuperEvent == 1) {
			if($this -> validate -> emptyInput($this -> SuperEventDescription)) {
				array_push($validationErrors, array('inputID' => 14,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> SuperEventIDString)) {
				array_push($validationErrors, array('inputID' => 19,
													'errorMessage' => 'Required'));
			}
			
		}
		

		if($this -> validate -> emptyInput($this -> PerformerInfo)) {
			array_push($validationErrors, array('inputID' => 15,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> EventPrice)) {
			array_push($validationErrors, array('inputID' => 16,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> Latitude)) {
			array_push($validationErrors, array('inputID' => 17,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> Longitutde)) {
			array_push($validationErrors, array('inputID' => 18,
												'errorMessage' => 'Required'));
		}
		
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	
	
	
	private function uploadPicture($newImageName) {
		$ImagePathParts = array();			
		$imageUploadType = NULL;	
			
		$ImagePathParts['SuperEventFolder'] = "/SuperEvents/";
		$imageUploadType = 5;
					
		Image::MoveAndRenameImage($_FILES['SuperEventPhoto']['tmp_name'], 
								  $ImagePathParts,
								  $this -> SuperEventPhoto,
								  parent::seoUrl($newImageName), $imageUploadType);
		
		
			
		
		
		//move_uploaded_file($_FILES['EventPhoto']['tmp_name'], EVENTS_PHOTO_PATH . $folderName . '/' . $this -> EventPhoto);
		//$Folder = EVENTS_PHOTO_PATH . $folderName . '/';
		///$RenamedImage = EVENTS_PHOTO_PATH  . $folderName . '/' . $this -> EventPhoto;
		
		//PHOTO_PATH
	}
	
	private function LogEditHistory($eventID) {
		$log = new EditHistoryLog();
		$log -> itemID = $eventID;
		$log -> userID = $_SESSION['user'] -> _userId;
		$log -> itemType = 3;		
		$log -> Save();	
	}

	private function GenerateJSONHTML($eventID, $JSON) {
		$fileName = JSON_EVENT_PATH . 'eventID' . $eventID . '.json';
		$JSONEdit = fopen($fileName, "w");
		fwrite($JSONEdit, $JSON);
		fclose($JSONEdit);	
	}
	
	public function SaveSuperEvent() {
		try {
			$superEventDBData = array();	
			$eventID = NULL;
			$storeIDS = NULL;

			$createAlbum = false;		
				
			
			$StructuredData = json_encode(array("Performer" => $this -> PerformerInfo,
												"EventPrice" => $this -> EventPrice,
												"Latitude" => $this -> Latitude,
												"Longitude" => $this -> Longitutde,
												"SuperEventIDText" => $this -> SuperEventIDString));
			
			$shortLink = new ShortLinks();
			
			if(isset($this -> _id)) {
				
				$superEventDBData['eventName'] = $this -> eventName;
				$superEventDBData['StructuredDataInfo'] = $StructuredData;
				$superEventDBData['seoUrl'] = parent::seoUrlSpaces($this -> seoURL);
				$superEventDBData['SuperEventDescription'] = parent::DescriptionFormat($this -> SuperEventDescription);
				$superEventDBData['modifiedDateFull'] = parent::TimeStamp();
				
				$eventID = $this -> _id;
				
				
				
				if(!empty($this -> SuperEventPhoto)) {
					$this -> uploadPicture($this -> eventName . "-thumb");
					$superEventDBData['eventImage'] = parent::seoUrl($this -> eventName . "-thumb") . '.' . Image::getFileExt($this -> SuperEventPhoto);
				}
					
				$this->db->update('events', $superEventDBData, array('eventID' => $this -> _id));
				
				
				$shortLink -> CurrentShortCode = $this -> CurrentSEOUrl;
				$shortLink -> NewShortLink = parent::seoUrlSpaces($this -> seoURL);	
								
			} else {
				
				$superEventDBData['eventName'] = $this -> eventName;
				$superEventDBData['seoUrl'] = parent::seoUrlSpaces($this -> eventName);
				$superEventDBData['StructuredDataInfo'] = $StructuredData;
				$superEventDBData['SuperEvent'] = 1;
				$superEventDBData['SubEvent'] = 0;
				$superEventDBData['eventVisible'] = 0;
				
				$NewEvent = $this -> db -> insert('events', $superEventDBData);
				
				$eventID = $NewEvent;		
					
				$shortLink -> CurrentShortCode = parent::seoUrlSpaces($this -> eventName);
				$shortLink -> NewShortLink = parent::seoUrlSpaces($this -> eventName);	
			}
			
			
			$this -> LogEditHistory($eventID);
			
			$shortLink -> EventID = $eventID;
			$shortLink -> CurrentYear = $EventYear[0];
			$shortLink -> SaveEventShortLink();
			
			
			$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> events() . '/edit/SuperEvent/'. $eventID);
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Super Event Save Error: " . $e->getMessage();
			$TrackError -> type = "SUPER EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	
	public function SaveSubEvent() {
		try {
			$subEventDBData = array();
			$storeIDS = NULL;
			foreach($this -> Store as $storeID) {
				$storeIDS .= $storeID . ',';
			}
			$storeIDS = rtrim($storeIDS, ',');
			
			$StructuredData = json_encode(array("Performer" => $this -> PerformerInfo,
												"EventPrice" => $this -> EventPrice,
												"Latitude" => $this -> Latitude,
												"Longitude" => $this -> Longitutde));
					
			
			
			
			if(isset($this -> _id)) {
				$eventID = $this -> _id;
				$sth = $this -> db -> prepare("DELETE FROM pagedata WHERE linkedPageTypeID = :linkedPageTypeID");
				$sth -> execute(array(':linkedPageTypeID' => $this -> _id));
				
				$this -> WriteJSONFile('eventID' . $eventID . '.json');
				$this -> InsertPageData($this -> eventHtmlJSON);
				
				if($this -> visible == 1) {
					$this -> UpdateGoogleEvent();	
				}
				
				$subEventDBData['eventName'] = $this -> eventName;
				$subEventDBData['startTime'] = $this -> startTime;
				$subEventDBData['startDate'] = $this -> startDate;
				$subEventDBData['endTime'] = $this -> endTime;
				$subEventDBData['endDate'] = $this -> endDate;
				$subEventDBData['stores'] = $storeIDS;
				$subEventDBData['eventAddress'] = $this -> address;
				$subEventDBData['eventCity'] = $this -> city;
				$subEventDBData['eventState'] = $this -> state;
				$subEventDBData['eventZip'] = $this -> zip;
				$subEventDBData['StructuredDataInfo'] = $StructuredData;
				$subEventDBData['seoUrl'] = parent::seoUrlSpaces($this -> eventName);
				$standaloneEventDBData['modifiedDateFull'] = parent::TimeStamp();
				$subEventDBData['GoogleEventDescription'] = str_replace("<br />", '\n',$this -> GoogleEventDescription);
				
				$this->db->update('events', $subEventDBData, array('eventID' => $this -> _id));
				
			} else {
				
				$EventYear = explode("-", $this -> startDate);
				
				$subEventDBData['eventName'] = $this -> eventName;
				$subEventDBData['startTime'] = $this -> startTime;
				$subEventDBData['startDate'] = $this -> startDate;
				$subEventDBData['endTime'] = $this -> endTime;
				$subEventDBData['endDate'] = $this -> endDate;
				$subEventDBData['stores'] = $storeIDS;
				$subEventDBData['eventAddress'] = $this -> address;
				$subEventDBData['eventCity'] = $this -> city;
				$subEventDBData['eventState'] = $this -> state;
				$subEventDBData['eventZip'] = $this -> zip;
				$subEventDBData['StructuredDataInfo'] = $StructuredData;
				$subEventDBData['seoUrl'] = parent::seoUrlSpaces($this -> eventName);
				$subEventDBData['GoogleEventDescription'] = str_replace("<br />", '\n',$this -> GoogleEventDescription);
				$subEventDBData['SuperEvent'] = 0;
				$subEventDBData['SubEvent'] = 1;
				$subEventDBData['eventVisible'] = 0;
				$subEventDBData['EventDirectory'] = $EventYear[0];
				
				$NewEvent = $this -> db -> insert('events', $subEventDBData);
				
				$eventID = $NewEvent;
				
				$SubEvent = $this -> db -> prepare("INSERT IGNORE INTO subevents(SuperEventID, SubEventID) VALUES(:SuperEventID, :SubEventID)");
				$SubEvent -> execute(array(":SuperEventID" => $this -> SuperEventID, ":SubEventID" => $eventID));
				
				$this -> CreateEventAlbum($EventYear[0], $NewEvent, $this -> SuperEventID);
				
				$this -> WriteJSONFile('eventID' . $eventID . '.json', $eventID, true);
				
			}


			//$this -> GenerateJSONHTML($this -> _id, $this -> eventHtmlJSON);			
			$this -> UpdateSuperEventDates();
				
			$this -> LogEditHistory($eventID);
		
			$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> events() . '/edit/SubEvent/'. $eventID);
		
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Sub Event Save Error: " . $e->getMessage();
			$TrackError -> type = "SUB EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}	
			
	}

	public function SaveStandaloneEvent() {
		try {
			$standaloneEventDBData = array();
			$EventYear = explode("-", $this -> startDate);
			
			$StructuredData = json_encode(array("Performer" => $this -> PerformerInfo,
												"EventPrice" => $this -> EventPrice,
												"Latitude" => $this -> Latitude,
												"Longitude" => $this -> Longitutde));
			
			$storeIDS = NULL;
			foreach($this -> Store as $storeID) {
				$storeIDS .= $storeID . ',';
			}
			$storeIDS = rtrim($storeIDS, ',');
			
			if(isset($this -> _id)) {
				
				$eventID = $this -> _id;
				$sth = $this -> db -> prepare("DELETE FROM pagedata WHERE linkedPageTypeID = :linkedPageTypeID");
				$sth -> execute(array(':linkedPageTypeID' => $this -> _id));
				
				$this -> WriteJSONFile('eventID' . $eventID . '.json');
				$this -> InsertPageData($this -> eventHtmlJSON);
				
				if($this -> visible == 1) {
					$this -> UpdateGoogleEvent();	
				}
				
				$standaloneEventDBData['eventName'] = $this -> eventName;
				$standaloneEventDBData['startTime'] = $this -> startTime;
				$standaloneEventDBData['startDate'] = $this -> startDate;
				$standaloneEventDBData['endTime'] = $this -> endTime;
				$standaloneEventDBData['endDate'] = $this -> endDate;
				$standaloneEventDBData['stores'] = $storeIDS;
				$standaloneEventDBData['eventAddress'] = $this -> address;
				$standaloneEventDBData['eventCity'] = $this -> city;
				$standaloneEventDBData['eventState'] = $this -> state;
				$standaloneEventDBData['eventZip'] = $this -> zip;
				$standaloneEventDBData['StructuredDataInfo'] = $StructuredData;
				$standaloneEventDBData['seoUrl'] = parent::seoUrlSpaces($this -> seoURL);
				$standaloneEventDBData['modifiedDateFull'] = parent::TimeStamp();
				$standaloneEventDBData['GoogleEventDescription'] = str_replace("<br />", '\n', $this -> GoogleEventDescription);
				
				$this->db->update('events', $standaloneEventDBData, array('eventID' => $this -> _id));
				
				if($this -> visible == 1) {
					$this -> UpdateShortCode($this -> CurrentSEOUrl, 
											 parent::seoUrlSpaces($this -> seoURL), 
											 $this -> _id, 
											 $EventYear[0]);
				}
				
				
			} else {
				
				$standaloneEventDBData['eventName'] = $this -> eventName;
				$standaloneEventDBData['startTime'] = $this -> startTime;
				$standaloneEventDBData['startDate'] = $this -> startDate;
				$standaloneEventDBData['endTime'] = $this -> endTime;
				$standaloneEventDBData['endDate'] = $this -> endDate;
				$standaloneEventDBData['stores'] = $storeIDS;
				$standaloneEventDBData['eventAddress'] = $this -> address;
				$standaloneEventDBData['eventCity'] = $this -> city;
				$standaloneEventDBData['eventState'] = $this -> state;
				$standaloneEventDBData['eventZip'] = $this -> zip;
				$standaloneEventDBData['StructuredDataInfo'] = $StructuredData;
				$standaloneEventDBData['seoUrl'] = parent::seoUrlSpaces($this -> eventName);
				$standaloneEventDBData['GoogleEventDescription'] = str_replace("<br />", '\n',$this -> GoogleEventDescription);
				$standaloneEventDBData['SuperEvent'] = 0;
				$standaloneEventDBData['eventVisible'] = 0;
				$standaloneEventDBData['SubEvent'] = 0;
				$standaloneEventDBData['EventDirectory'] = $EventYear[0];
				
				$NewEvent = $this -> db -> insert('events', $standaloneEventDBData);
				//$this -> CreateEventAlbum($EventYear[0], $NewEvent);
				
				$this -> CreateEventAlbum($EventYear[0], $NewEvent, NULL, parent::seoUrlSpaces(preg_replace('/[0-9]+/', '', $this -> eventName)));
				
				$this -> db -> insert('eventphotoalbumlink', array('EventNameText' => preg_replace('/[0-9]+/', '', $this -> eventName),
																   'LinkedEventNameText' => parent::seoUrlSpaces(preg_replace('/[0-9]+/', '', $this -> eventName))));
				
				$eventID = $NewEvent;
				
				$this -> WriteJSONFile('eventID' . $eventID . '.json', $eventID, true);
				
			}
			
			$this -> LogEditHistory($eventID);
			
			
			
			
			
			$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> events() . '/edit/Standalone/'. $eventID);
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Standalone Event Save Error: " . $e->getMessage();
			$TrackError -> type = "STANDALONE EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}	
		}
	}

	private function UpdateShortCode($current, $new, $id, $year) {
		$shortLink = new ShortLinks();
		$shortLink -> CurrentShortCode = $current;
		$shortLink -> NewShortLink = $new;	
		$shortLink -> EventID = $id;
		$shortLink -> CurrentYear = $year;
		$shortLink -> SaveEventShortLink();
	}
	
	public function UpdateEventMainPhoto() {
		$postData = array('eventImage' => $this -> EventPhoto);		
		$this->db->update('events', $postData, array('eventID' => $this->_id));
	}
	
	public function UpdateEventThumbNailPhoto() {
		$postData = array('eventThumbImage' => $this -> EventPhoto);		
		$this->db->update('events', $postData, array('eventID' => $this->_id));
	}
	
	private function CreateEventAlbum($year, $eventID, $superEventID = NULL, $linkedEvent = NULL) {
		//create new directory
		$photoAlbum = new PhotoAlbum();
		
		$photoAlbum -> year = $year;
		$photoAlbum -> AlbumType = 1;
		$photoAlbum -> LinkedEventID = $eventID;
		if($superEventID != NULL) {
			$superEventName = $this -> db -> prepare('SELECT * FROM subevents LEFT JOIN events ON subevents.SuperEventID = events.eventID WHERE subevents.SubEventID = :SubEventID');
			$superEventName -> execute(array(':SubEventID' => $eventID));
			$getSuperEvent = $superEventName -> fetch();
			
			
			$photoAlbum -> LinkedSuperEventID = $superEventID;		
			$photoAlbum -> folderName = $getSuperEvent['eventName'] . ':' . $this -> eventName;
		} else {
			$photoAlbum -> LinkedSuperEventID = $linkedEvent;
			$photoAlbum -> folderName = $this -> eventName;
		}
		
					
		$photoAlbum -> NewDirectory();
					
		$postData = array('albumID' => $photoAlbum -> NewDirectoryID());	
		$this->db->update('events', $postData, array('eventID' => $eventID));
			
		
	}
	
	private function WriteJSONFile($fileName, $eventID = NULL, $new = false) {
		if($new == true) {
			$JSON = fopen( JSON_EVENT_PATH . $fileName, 'w');
			fwrite($JSON, '[]');
			fclose($JSON);
			
			$postData = array('eventHTMLJSON' => $fileName);	
			$this->db->update('events', $postData, array('eventID' => $eventID));	
		} else {
			//$finalEventJSON = array();
			
			//$eventCoded = json_decode($this -> eventHtmlJSON, true);
			
			//foreach($eventCoded as $row) {
			//	$columns = array();
				
			//	if(isset($row['Columns'])) {
			//		foreach($row['Columns'] as $columnSingle) {
						
			//			$contentArray = array();
			//			if(isset($columnSingle['Content'])) {
			//				foreach($columnSingle['Content'] as $contentSingle) {
			//					array_push($contentArray, array('ContentID' => $contentSingle['ContentID'],
			//													'ContentType' => $contentSingle['ContentType'],
			//													'Value' => filter_var($contentSingle['Value'], FILTER_SANITIZE_STRING)));	
			//				}	
							
			//				array_push($columns, array('columnID' => $columnSingle['columnID'],
			//									   'ClassID' => $columnSingle['ClassID'],
			//									   'Order' => $columnSingle['Order'],
			//									   'Content' => $contentArray));
							
			//			}	else {
			//				array_push($columns, array('columnID' => $columnSingle['columnID'],
			//									   'ClassID' => $columnSingle['ClassID'],
			//									   'Order' => $columnSingle['Order']));
			//			}					   
												   
												   
			//		}
					
			//		array_push($finalEventJSON, array("rowID" => $row['rowID'],
			//										  "Order" => $row['Order'],
			//										  "Columns" => $columns));	
			//	} else {
			//		array_push($finalEventJSON, array("rowID" => $row['rowID'],
			//										  "Order" => $row['Order']));	
			//	}
				
				
			//}
			
			
			
			$fileName = JSON_EVENT_PATH . $fileName;
			$JSONEdit = fopen($fileName, "w");
			fwrite($JSONEdit, $this -> eventHtmlJSON);
			fclose($JSONEdit);	
		}
	}

	public function GetJSONFile() {
		return file_get_contents(JSON_EVENT_URL . $this -> eventHtmlJSON);
	}
	
	public function SaveJSON() {
		$this -> WriteJSONFile('eventID' . $this -> _id . '.json');
	}
	
	
	private function UpdateSuperEventDates() {
		$superEvent = $this -> db -> prepare("SELECT * FROM events WHERE eventID = :eventID");
		$superEvent -> execute(array("eventID" => $this -> _CurrentSuperEventID));
		$getSuperEvent = $superEvent -> fetch();
		
					
		$superEventStartDate = new DateTime($getSuperEvent['startDate']);
		$newSuperEventStartDate = new DateTime($this -> startDate);
		
		$superEventEndDate = new DateTime($getSuperEvent['endDate']);
		$newSuperEventEndDate = new DateTime($this -> endDate);
		
		
		$postData = array('startDate' => ($getSuperEvent['startDate'] == NULL || $superEventStartDate > $newSuperEventStartDate ? $this -> startDate : $getSuperEvent['startDate']),
						  'startTime' => ($getSuperEvent['startDate'] == NULL || $superEventStartDate > $newSuperEventStartDate ? $this -> startTime : $getSuperEvent['startTime']),
						  'endDate' => ($getSuperEvent['endDate'] == NULL || $newSuperEventEndDate > $superEventEndDate ? $this -> endDate : $getSuperEvent['endDate']),
						  'endTime' => ($getSuperEvent['endTime'] == NULL || $newSuperEventEndDate > $superEventEndDate ? $this -> endTime : $getSuperEvent['endTime']));														 
					
		
															 
		$this->db->update('events', $postData, array('eventID' => $this -> _CurrentSuperEventID));	
	}
	
	private function InsertPageData($JSONFile) {
		
		
		$newJSONData = json_decode($JSONFile, true);
				
		foreach($newJSONData as $key => $row) {
			if(isset($row['Columns'])) {
				foreach($row['Columns'] as $column) {
					if(isset($column['Content'])) {
						foreach($column['Content'] as $content) {
							if($content['ContentType'] == "Photo") {
								$this -> db -> insert('pagedata', array('linkedPageTypeID' => 1,
																		'photosSelected' => $content['Value'],
														 				'linkedPageTypeID' => $this -> _id,
																		'pageType' => 1));										
							}	
							
							if($content['ContentType'] == "PhotoAlbum") {
								$this -> db -> insert('pagedata', array('linkedPageTypeID' => 1,
																		'albumsSelected' => $content['Value'],
														 				'linkedPageTypeID' => $this -> _id,
																		'pageType' => 1));
							}
						}				
					}
				}
			}
		}	
	}
	
	private function GoogleGetLocaiton() {
		return $this -> address .', ' .$this -> city . ', ' .$this -> state . ' '. $this -> zip;
	}
	
	
	public function GetEventMainImage() {
		$image = NULL;	
		if($this -> SuperEvent == 1) {
			$image = PHOTO_URL . 'SuperEvents/' . $this -> CurrentEventImageBig;
		} else {
			$image = PHOTO_URL . $this -> ParentDirectory . '/events/' . $this -> albumFolderName . '/' . $this -> CurrentEventImageBig;	
		}
		return $image;
	}

	public function GetMainPhotoSmall() {
		return PHOTO_URL . $this -> ParentDirectory . '/events/' . $this -> albumFolderName . '/' . $this -> CurrentEventImageSmall;
	}
	
	
	public function Publish() {
		if(LIVE_SITE == true) {
			require '/var/www/vhosts/dillon-brothers.com/GoogleAPI/autoload.php';	
		} else {
			require 'libs/GoogleAPI/autoload.php';	
		}
				
		$googleClient = new Google_Client();
		
		
		$googleClient->setApplicationName("Dillon Brothers");
		$googleClient->setAuthConfig('client_secret.json');
  		$googleClient->setDeveloperKey(GOOGLE_CALENDAR_API_KEY);
		$googleClient->setScopes(Google_Service_Calendar::CALENDAR);
  		$googleClient->setAccessType('offline');
		$service = new Google_Service_Calendar($googleClient);
	
		if($this -> SubEvent == 1) {
			$eventName = $this -> SuperEventName . ': '. $this -> eventName;	
		} else {
			$eventName = $this -> eventName;	
		}
		
		
		$event = new Google_Service_Calendar_Event(array(
		  'summary' => $eventName,
		  'location' => $this -> GoogleGetLocaiton(),
		  'description' => str_replace("\\n", '+%5C',$this -> GoogleEventDescription),
		  'start' => array(
		    'dateTime' => $this -> startDate.'T'. $this -> startTime. ':00-06:00',
		    'timeZone' => 'America/Chicago',
		  ),
		  'end' => array(
		    'dateTime' => $this -> endDate.'T'. $this -> endTime . ':00-06:00',
		    'timeZone' => 'America/Chicago',
		  ),
		  'reminders' => array(
		    'useDefault' => FALSE,
		    'overrides' => array(
		      array('method' => 'email', 'minutes' => 24 * 60),
		      array('method' => 'popup', 'minutes' => 10),
		    ),
		  ),
		));
		
		$calendarId = CALENDAR_ID;
		$event = $service->events->insert($calendarId, $event);
		
		
		
		if(!empty($this -> PublishedDateFull)) {
			$publishedDate = $this -> PublishedDateFull;
		} else {
			$publishedDate = date("Y-m-d", $this -> time -> NebraskaTime()) . 'T' . date("H:i:s", $this -> time -> NebraskaTime());	
		}
		
		$postData = array('eventVisible' => 1,
						  'modifiedDateFull' => date("Y-m-d", $this -> time -> NebraskaTime()) . 'T' . date("H:i:s", $this -> time -> NebraskaTime()),
						  'publishDateFull' => $publishedDate,
						  'GoogleEventID' => $event -> id);		
		$this->db->update('events', $postData, array('eventID' => $this -> _id));
		
		$EventYear = explode("-", $this -> startDate);
		
		$this -> UpdateShortCode($this -> CurrentSEOUrl, 
								 $this -> CurrentSEOUrl, 
								 $this -> _id, 
								 $EventYear[0]);
		
		
		if($this -> SubEvent == 1) {
			$this -> redirect -> redirectPage(PATH. 'events/edit/SubEvent/'. $this -> _id);	
		} else if($this -> SubEvent == 0 && $this -> SuperEvent == 0) {
			$this -> redirect -> redirectPage(PATH. 'events/edit/Standalone/'. $this -> _id);	
		}
		
	}

	public function UpdateGoogleEvent() {
		if(LIVE_SITE == true) {
			require '/var/www/vhosts/dillon-brothers.com/GoogleAPI/autoload.php';	
		} else {
			require 'libs/GoogleAPI/autoload.php';	
		}
		
		$googleClient = new Google_Client();
		
		
		$googleClient->setApplicationName("Dillon Brothers");
		$googleClient->setAuthConfig('client_secret.json');
  		$googleClient->setDeveloperKey(GOOGLE_CALENDAR_API_KEY);
		$googleClient->setScopes(Google_Service_Calendar::CALENDAR);
  		$googleClient->setAccessType('offline');
		$service = new Google_Service_Calendar($googleClient);
		
		
		//echo $this -> GoogleEventID;
		$event = $service->events->get(CALENDAR_ID, $this -> GoogleEventID);
		

		if($this -> SubEvent == 1) {
			$eventName = $this -> SuperEventName . ': '. $this -> eventName;	
		} else {
			$eventName = $this -> eventName;	
		}
		
		$start = new Google_Service_Calendar_EventDateTime();
		$start->setDateTime($this -> startDate.'T'. $this -> startTime. ':00-06:00');  
		$event->setStart($start);
		$end = new Google_Service_Calendar_EventDateTime();
		$end->setDateTime($this -> endDate.'T'. $this -> endTime . ':00-06:00');  
		$event->setEnd($end);
		
		
		
		$updatedEvent = $service->events->update(CALENDAR_ID, $event->getId(), $event);
		
		
		
	}

	private function RemoveGoogleEvent() {
		if(LIVE_SITE == true) {
			require '/var/www/vhosts/dillon-brothers.com/GoogleAPI/autoload.php';	
		} else {
			require 'libs/GoogleAPI/autoload.php';	
		}
		
		
		
		$googleClient = new Google_Client();
		
		
		$googleClient->setApplicationName("Dillon Brothers");
		$googleClient->setAuthConfig('client_secret.json');
  		$googleClient->setDeveloperKey("AIzaSyBybGZTZuBMVV989W_P2WPMkiBiOTfWOqE");
		$googleClient->setScopes(Google_Service_Calendar::CALENDAR);
  		$googleClient->setAccessType('offline');
		
		$service = new Google_Service_Calendar($googleClient);
		
		
		$calendarId = CALENDAR_ID;
		$service->events->delete($calendarId, $this -> GoogleEventID);
	}

	public function UnPublish() {
		$this -> RemoveGoogleEvent();	
			
		$postData = array('eventVisible' => 0,
						  'GoogleEventID' => '');		
		
		
		
		$this->db->update('events', $postData, array('eventID' => $this -> _id));
		
		$shortLink = new ShortLinks();
		$shortLink -> DeleteEventShortUrl($this -> _id);
		
		if($this -> SubEvent == 1) {
			$this -> redirect -> redirectPage(PATH. 'events/edit/SubEvent/'. $this -> _id);	
		} else if($this -> SubEvent == 0 && $this -> SuperEvent == 0) {
			$this -> redirect -> redirectPage(PATH. 'events/edit/Standalone/'. $this -> _id);	
		}
	}
	
	public function GetHtml() {
		return file_get_contents(JSON_EVENT_PATH . $this -> eventHtmlJSON);
	}
	
	public function Delete() {
		//delete photo album	
		Folder::DeleteDirectory(PHOTO_PATH . $this -> ParentAlbumDirectory . "/events/" . $this -> albumFolderName);
		
		//delete photos	
		$photosDelete = $this -> db -> prepare('DELETE FROM photos WHERE photoAlbumID = :photoAlbumID');
		$photosDelete -> execute(array(":photoAlbumID" => $this -> AlbumID));
			
		//delete albums	
		$photoAlbumDelete = $this -> db -> prepare('DELETE FROM photoalbums WHERE albumID = :albumID');
		$photoAlbumDelete -> execute(array(":albumID" => $this -> AlbumID));
		
		//delete events history
		$eventHistory = $this -> db -> prepare('DELETE FROM edithistorylog WHERE relatedItemID = :relatedItemID AND editHistoryType = 3');
		$eventHistory -> execute(array(":relatedItemID" => $this -> _id));		
		
		
		//delete any sub events records
		$subevent = $this -> db -> prepare('DELETE FROM subevents WHERE SubEventID = :SubEventID');
		$subevent -> execute(array(":SubEventID" => $this -> _id));		
		
		//remove google event if it is already on there
		if($this -> visible == 1) {
			$this -> RemoveGoogleEvent();	
		}
		
		//delete event		
		$sth = $this -> db -> prepare("DELETE FROM events WHERE eventID = :eventID");
		$sth -> execute(array(':eventID' => $this->_id));	
		
		
		$this -> redirect -> redirectPage(PATH. 'events');
	}
	
	

		

}