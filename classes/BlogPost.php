<?php


class BlogPost extends BaseObject {
	
	private $_id;
	
	public $blogPostName;
	public $SeoURL;
	public $blogPostContent;
	public $visible;
	public $blogPhoto;
	public $currentBlogPhoto;
	
	public $photoAlbumID;
	
	public $OptionalURLSlug;
	public $CurrentOptionalURLSlug;
	public $MetaData;
	public $MetaDataContent;
	
	public $PostID;
	//meta data
	public $ShortLinkID;
	
	
	
	public $CurrentBlogImage;
	
	private $_publishDate;
	private $_publishTime;
	
	private $_publishedDateFull;
	
	private $_MetaDataString;
	
	public $category;
	public $photoAlbumYearDirectory;
	public $photoAlbumName;
	
	public $CurrentViewedPhoto;
	public $NewPhotoName;
	public $NewPhotoAltText;
	public $NewPhotoTitleText;
	
	public $showUserNameOnPublish;
	
	public $CurrentBlogPhotoImageName;
	public $CurrentBlogPhotoImageExt;
	public $createdByFullName;
	
	public $KeyWords;
	
	public $lastEditedRow = array();
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($postID) {
        $instance = new self();
        $instance->_id = $postID;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM blogposts LEFT JOIN photoalbums On blogposts.blogPostID = photoalbums.albumBlogID 
    									LEFT JOIN photos On blogposts.blogPostImage = photos.photoID 
    									LEFT JOIN users ON blogposts.createdByID = users.userID 
    									LEFT JOIN shortlinks ON blogposts.blogPostID = shortlinks.relatedBlogID
    									WHERE blogPostID = :blogPostID');
        $sth->execute(array(':blogPostID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this ->_id = $row['blogPostID'];
    	$this -> blogPostName = $row['blogPostName'];
		$this -> currentBlogPhoto = $row['blogPostImage'];
		$this -> blogPostContent = $row['blogPostContent'];
		$this -> visible = $row['blogVisible'];
		$this -> _publishedDateFull = $row['publishedDateFull'];
		$this -> _publishDate = $row['publishedDate'];
		$this -> _publishTime = $row['publishedDate'];
		$this -> photoAlbumYearDirectory = $row['ParentDirectory'];
		$this -> photoAlbumName = $row['albumFolderName'];
		$this -> photoAlbumID = $row['albumID'];
		$this -> CurrentBlogPhotoImageName = $row['photoName'];
		$this -> CurrentBlogPhotoImageExt = $row['ext'];
		$this -> showUserNameOnPublish = $row['showUserNameOnPublish'];
		$this -> createdByFullName = $row['firstName'] . ' ' . substr($row['lastName'], 0, 1);
		$this -> PhotoAlbumID = $row['albumID'];
		$this -> CurrentBlogImage = $row['blogPostImage'];
		$this -> KeyWords = $row['keywords'];
		$this -> SeoURL = $row['blogPostSEOurl'];
		$this -> CurrentOptionalURLSlug = $row['OptionalBlogSEOUrl'];
		$this -> ShortLinkID = $row['shortLinkID'];
    }
	
	private function GetLatestEditedRow() {
		$sth = $this -> db -> prepare('SELECT * FROM blogedithistory LEFT JOIN users ON blogedithistory.edittedByUserID = users.userID WHERE relatedBlogPostID = :relatedBlogPostID ORDER BY editHistoryID DESC LIMIT 1');
        $sth->execute(array(':relatedBlogPostID' => $this->_id));
		$this -> lastEditedRow = $sth -> fetch();
	}

	
	public function GetCreatedByName() {
		
	}
	
	public function GetBlogEditHistory() {
		return $this -> db -> select('SELECT * FROM blogedithistory LEFT JOIN users ON blogedithistory.edittedByUserID = users.userID WHERE relatedBlogPostID = ' . $this->_id);
	}
	
	
	public function Validate() {
		$validationErrors = array();
		
		if(!isset($this->_id)) {
            $blogNameCheck = $this->db->prepare('SELECT blogPostSEOurl FROM blogposts WHERE blogPostSEOurl = ?');
            $blogNameCheck -> execute(array(parent::seoUrlSpaces($this -> blogPostName)));
			
        } else {
            $blogNameCheck = $this->db->prepare('SELECT blogPostSEOurl FROM blogposts WHERE blogPostSEOurl = :blogPostSEOurl and blogPostID <> :blogPostID');
            $blogNameCheck ->execute(array(':blogPostSEOurl' => parent::seoUrlSpaces($this -> blogPostName), ':blogPostID' => $this->_id));
			
			if(!empty($this -> OptionalURLSlug)) {
				$shortLinkCheck = $this -> db -> prepare('SELECT shortUrl FROM shortlinks WHERE shortUrl = :shortUrl AND shortLinkActive = 1 AND shortLinkID <> :shortLinkID');
				$shortLinkCheck -> execute(array(":shortUrl" => $this -> OptionalURLSlug,
												 ":shortLinkID" => $this -> ShortLinkID));	
			}
			
				
			
        }
		
		if(empty($this -> OptionalURLSlug)) {
			if($this -> validate -> emptyInput($this -> blogPostName)) {
				array_push($validationErrors, array('inputID' => 1,
													'errorMessage' => 'Required'));
			} else if (count($blogNameCheck -> fetchAll())) {
	            array_push($validationErrors, array('inputID' => 1,
													'errorMessage' => 'There is already a blog post created in this name'));
	        }
        } else {
        	if (count($shortLinkCheck -> fetchAll())) {
		           array_push($validationErrors, array('inputID' => 2,
														'errorMessage' => 'There is already a short url already entered in the system, please type in a unique short name'));
		    }
        }
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	
	public function GetID() {
		return $this->_id;
	}
	
	private function uploadPicture($submittedYear = NULL) {
		$ImagePathParts = array();		
		
		$ImagePathParts['Year'] = ($submittedYear != NULL ? $submittedYear : $this -> photoAlbumYearDirectory);
		$ImagePathParts['type'] = "/blog/";
		$ImagePathParts['AlbumName'] = $this -> photoAlbumName;
			
		Image::MoveAndRenameImage($_FILES['blogPostPhoto']['tmp_name'], 
								  $ImagePathParts,
								  $this -> blogPhoto,
								  parent::seoUrl($this -> blogPostName). 'PostThumb', 1);
	}
	
	public function Save() {
		try {
			
			
			
			
			$redirectID = NULL;
			if(isset($this->_id)) {
				
				$sth = $this -> db -> prepare("DELETE FROM blogcatories WHERE RelatedBlogPostID = :RelatedBlogPostID");
				$sth -> execute(array(':RelatedBlogPostID' => $this->_id));
				
				
				$updateBlogDBData = array();
				$updateBlogDBData['blogPostName'] = $this -> blogPostName;
				$updateBlogDBData['blogPostSEOurl'] = parent::seoUrlSpaces($this -> blogPostName);
				$updateBlogDBData['blogPostPreview'] = strip_tags(mb_substr($this -> blogPostContent, 0, 1000));
				$updateBlogDBData['blogPostContent'] = $this -> blogPostContent;
				$updateBlogDBData['showUserNameOnPublish'] = $this -> showUserNameOnPublish;
				$updateBlogDBData['dateModifiedDate'] = parent::TimeStamp();
				$updateBlogDBData['keywords'] = $this -> KeyWords;
				
				$shortLink = new ShortLinks();
				
				if(!empty($this -> OptionalURLSlug)) {
					$blogShortURL = parent::seoUrlSpaces($this -> OptionalURLSlug);
					$updateBlogDBData['OptionalBlogSEOUrl'] = $blogShortURL;	
					$shortLink -> ShortLinkValue = $blogShortURL;
					$shortLink -> ShortCodeLinkActive = 1;
				} else {
					$updateBlogDBData['OptionalBlogSEOUrl'] = '';
					$shortLink -> UpdateShortLinkInfo = true;
					$shortLink -> ShortLinkValue = $this -> CurrentOptionalURLSlug;
					$shortLink -> ShortCodeLinkActive = 0;
				}

				$shortLink -> BlogID = $this->_id;
				$shortLink -> OriginatedSource = parent::seoUrlSpaces($this -> blogPostName);
				
				$shortLink -> UpdateBlogShortLink();
				
				$this->db->update('blogposts', $updateBlogDBData, array('blogPostID' => $this->_id));
				
				
				$redirectID = $this->_id;
			} else {
				Session::init();
				
				
				$NewBlogPost = $this -> db -> insert('blogposts', array('blogPostName' => $this -> blogPostName,
																  	    'blogPostSEOurl' => parent::seoUrlSpaces($this -> blogPostName),
																	    'createdByID' => $_SESSION['user'] -> _userId,
																	    'dateModifiedDate' => date("Y-m-d", $this -> time -> NebraskaTime()) . 'T'. date("H:i:s", $this -> time -> NebraskaTime())));	
				
				$this -> NewFolderDirectory($NewBlogPost);
				
				
				$redirectID = $NewBlogPost;
										 
			}

			if(isset($this -> category)) {
				foreach($this -> category as $categorySingle) {
							$this -> db -> insert('blogcatories', array('BlogCategoryID' => $categorySingle,
																        'RelatedBlogPostID' => $redirectID));	
				}								
			} else {
				$this -> db -> insert('blogcatories', array('BlogCategoryID' => 0,
															'RelatedBlogPostID' => $redirectID));
			}

			
			

			//$this -> json -> outputJqueryJSONObject('Metatest', $this -> MetaDataContent);
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'blog/edit/post/' . $redirectID);
			
			$log = new EditHistoryLog();
			$log -> itemID = $redirectID;
			$log -> userID = $_SESSION['user'] -> _userId;
			$log -> itemType = 1;		
			$log -> Save();	
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Blog Save Error: " . $e->getMessage();
			$TrackError -> type = "BLOG SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	
	public function UpdateBlogMainPhoto() {
		$postData = array('blogPostImage' => $this -> blogPhoto);		
		$this->db->update('blogposts', $postData, array('blogPostID' => $this->_id));
	}
	
	
	private function NewFolderDirectory($insertedID) {
		$photoAlbum = new PhotoAlbum();
		$photoAlbum -> AlbumType = 2;
		$photoAlbum -> folderName = $this -> blogPostName;
		$photoAlbum -> year = date("Y", $this -> time -> NebraskaTime());
		$photoAlbum -> LinkedBlogID = $insertedID;
		
		$photoAlbum -> MetaData = "blah";
			
								
		$photoAlbum -> NewDirectory();
		
		if(!empty($this -> blogPhoto)) {		
			$this -> uploadPicture($submittedYear);	
		}
	}
	
	
	
	public function GetCategories() {
		return $this -> db -> select("SELECT * FROM blogcatories WHERE RelatedBlogPostID = " . $this -> _id);
	}
	
	
	public function Publish() {
		$this -> _publishDate = date("Y-m-d", $this -> time -> NebraskaTime());
		$this -> _publishTime = date("H:i:s", $this -> time -> NebraskaTime());
			
		$postDataPublishedDate = array('blogVisible' => 1);		
		
		$this->db->update('blogposts', $postDataPublishedDate, array('blogPostID' => $this -> _id));
		
		if($this -> _publishedDateFull == NULL) {
			$this -> UpdatePublishedDateStatus();	
		}
		
		$this -> redirect -> redirectPage(PATH. 'blog/edit/post/'. $this -> _id);
	}

	private function UpdatePublishedDateStatus() {
		$PublishedDate = array('publishedDate' => $this -> _publishDate,
							   'publishedTime' => $this -> _publishTime,
							   'publishedDateFull' => $this -> _publishDate. 'T' . $this -> _publishTime);		
		
		$this->db->update('blogposts', $PublishedDate, array('blogPostID' => $this -> _id));
		//'publishedDateFull' => $this -> _publishDate. 'T' . $this -> _publishTime,
	}

	public function UnPublish() {
		$postData = array('blogVisible' => 0);		
		
		$this->db->update('blogposts', $postData, array('blogPostID' => $this -> _id));
		$this -> redirect -> redirectPage(PATH. 'blog/edit/post/'. $this -> _id);
	}
	
	public function UpdateMainPhoto() {
		$postData = array('blogVisible' => 0);		
		
		$this->db->update('blogposts', $postData, array('blogPostID' => $this -> _id));
		$this -> redirect -> redirectPage(PATH. 'blog/edit/post/'. $this -> _id);
	}
	
	public function Delete() {
		Folder::DeleteDirectory(PHOTO_PATH . $this -> photoAlbumYearDirectory . "/blog/" . $this -> photoAlbumName);
		
		//delete photos	
		$photosDelete = $this -> db -> prepare('DELETE FROM photos WHERE photoAlbumID = :photoAlbumID');
		$photosDelete -> execute(array(":photoAlbumID" => $this -> photoAlbumID));
			
		//delete albums	
		$photoAlbumDelete = $this -> db -> prepare('DELETE FROM photoalbums WHERE albumBlogID = :albumBlogID');
		$photoAlbumDelete -> execute(array(":albumBlogID" => $this -> _id));
		
		//delete blog history
		$blogHistory = $this -> db -> prepare('DELETE FROM edithistorylog WHERE relatedItemID = :relatedItemID AND editHistoryType = 1');
		$blogHistory -> execute(array(":relatedItemID" => $this -> _id));		
		
		//delete blog category record		
		$blogCategory = $this -> db -> prepare('DELETE FROM blogcatories WHERE RelatedBlogPostID = :RelatedBlogPostID');
		$blogCategory -> execute(array(":RelatedBlogPostID" => $this -> _id));		
		
		//delete blog comments
		$blogComments = $this -> db -> prepare('DELETE FROM blogcomments WHERE postID = :postID');
		$blogComments -> execute(array(":postID" => $this -> _id));
				
		$sth = $this -> db -> prepare("DELETE FROM blogposts WHERE blogPostID = :blogPostID");
		$sth -> execute(array(':blogPostID' => $this -> _id));	
		
		
		$this -> redirect -> redirectPage(PATH. 'blog');
	}	

	public function UpdateBlogPostContentImages() {
		$editedImage = NULL;
		
		$currentPhoto = PHOTO_URL . $this -> photoAlbumYearDirectory . "/blog/" . $this -> photoAlbumName. "/" . $this -> CurrentViewedPhoto;
		$newPhoto = PHOTO_URL . $this -> photoAlbumYearDirectory . "/blog/" . $this -> photoAlbumName. "/" . $this -> NewPhotoName;
		
		$dom = new DOMDocument;
		$dom->loadHTML(mb_convert_encoding($this -> blogPostContent, 'HTML-ENTITIES', 'UTF-8'));
		
		$imgtags = $dom->getElementsByTagName('img');
		
		foreach($imgtags as $imgNode) {
			if($imgNode -> getAttribute('src') == $currentPhoto) {
				$imgNode -> setAttribute('src', $newPhoto);
				$imgNode -> setAttribute('alt', $this -> NewPhotoAltText);
				$imgNode -> setAttribute('title', $this -> NewPhotoTitleText);
			}
			
		}
		
	
		
		
		//echo preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $dom->saveHTML());
		
		$postData = array('blogPostContent' => preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $dom->saveHTML()));		
		
		$this->db->update('blogposts', $postData, array('blogPostID' => $this->_id));
		
	}
	

		

}