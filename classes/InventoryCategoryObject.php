<?php


class InventoryCategoryObject extends BaseObject {
	
	private $_id;
	public $CategoryText;
	public $ParentCategoryText;
	
	public $MonkeyLearnID;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($categoryID) {
        $instance = new self();
        $instance->_id = $categoryID;
        $instance->loadByID();
        return $instance;
    }
		
		
	public static function MonkeyLearnID($monkeyLearnID) {
		$instance = new self();
        $instance-> MonkeyLearnID = $monkeyLearnID;
        $instance->loadByMonkeyLearnID();
        return $instance;
	}	
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM inventorycategories WHERE inventoryCategoryID  = :inventoryCategoryID');
        $sth->execute(array(':inventoryCategoryID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

	protected function loadByMonkeyLearnID() {
		$sth = $this -> db -> prepare('SELECT * FROM inventorycategories WHERE MonkeyLearnCategoryID  = :MonkeyLearnCategoryID');
        $sth->execute(array(':MonkeyLearnCategoryID' => $this-> MonkeyLearnID));
        $record = $sth -> fetch();
        $this->fill($record);
	}

    protected function fill(array $row){
    	$this -> _id = $row['inventoryCategoryID'];
    	$this -> CategoryText = $row['inventoryCategoryName'];
		$this -> ParentCategoryText = $row['ParentCategory'];
		$this -> MonkeyLearnID = $row['MonkeyLearnCategoryID'];
    }
	
	public function GetID() {
		return $this -> _id;
	}

	public function Validate() {
		if(isset($this -> _id)) {
			$categoryTextCheck = $this->db->prepare('SELECT * FROM inventorycategories WHERE inventoryCategoryName = :inventoryCategoryName AND inventoryCategoryID <> :inventoryCategoryID');
			$categoryTextCheck -> execute(array(":inventoryCategoryName" => $this -> CategoryText, ":inventoryCategoryID" => $this -> _id));
		} else {
			$categoryTextCheck = $this->db->prepare('SELECT inventoryCategoryName FROM inventorycategories WHERE inventoryCategoryName = ?');
			$categoryTextCheck -> execute(array($this -> CategoryText));	
		}
		
		
		
		
		$validationErrors = array();
		

		//empty first name
		if($this -> validate -> emptyInput($this -> CategoryText)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		} else if(count($categoryTextCheck -> fetchAll())) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'There is already a category entered, please type in a new category'));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Save() {
		try {

			
			if(isset($this -> _id)) {
				if(!empty($this -> MonkeyLearnID)) {
					require 'libs/MonkeyLearn/autoload.php';
		
					$module_id = 'cl_4LLnXuzp';
					
					$ml = new MonkeyLearn\Client('c6046ac2da646a10e42c70be661ecf6fbe565c21');
			
					$parent_category_id = 13854345;	
					
					$res = $ml->classifiers->categories->edit($module_id, $this -> MonkeyLearnID, $this -> CategoryText, $parent_category_id);	
				}
				
					
					
				$postData = array('inventoryCategoryName' => $this -> CategoryText,
								  'ParentCategory' => $this -> ParentCategoryText);
								  					
				$this->db->update('inventorycategories', $postData, array('inventoryCategoryID' => $this -> _id));
				
				
				
				
			} else {
				
				$this -> db -> insert('inventorycategories', array('inventoryCategoryName' => $this -> CategoryText,
																   'ParentCategory' => $this -> ParentCategoryText));
																  
																	  
			
			}
				
			$this -> json -> outputJqueryJSONObject("redirect", PATH . 'settings/inventorycategories');
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Event Save Error: " . $e->getMessage();
			$TrackError -> type = "EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	
	
	public function UpdateMonkeyLearnCategoryID() {
		$postData = array('MonkeyLearnCategoryID' => $this -> MonkeyLearnID);					
		$this->db->update('inventorycategories', $postData, array('inventoryCategoryID' => $this -> _id));
		
	}

		

}