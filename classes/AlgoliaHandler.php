<?php


class AlgoliaHandler {
	
	private $_AlgoliaClient;
	
	//for main unit inventory
	public $MainUnitInventoryID;
	public $MainUnitCondition;
	public $MainUnitYear;
	public $MainUnitMake;
	public $MainUnitModelNumber;
	public $MainUnitFriendlyName;
	public $MainUnitSEOUrl;
	public $MainUnitCategoryName;
	public $MainUnitVinNumber;
	public $MainUnitStock;
	public $MainUnitStoreName;
	public $MainUnitMarkAsSold;
	public $MainUnitOverlayText;
	public $MainUnitTimeInserted;
	public $MainUnitMileage;
	public $MainUnitEngineSizeCC;
	public $MainUnitMSRP;
	public $MainUnitSmallMainImage;
	public $MainUnitStoreID;
	
	//for shopping cart
    public $ecommerceProductID;
	public $ecommerceProductImage;
	public $ecommerceProductSEOName;
	public $ecommerceProductName;
	public $ecommerceProductPrice;
	public $ecommerceProductStoreID;
	
 	
    public function __construct() {
		$this -> _AlgoliaClient = new \AlgoliaSearch\Client("943L0R18H9", "eba4ec01de1347a0bff85f56d79ad18d");
    }
	
	public function BulkPushToInventory() {
		if(LIVE_SITE == TRUE) {
			$index = $this -> _AlgoliaClient -> initIndex('MainUnitInventory');	
		} else {
			$index = $this -> _AlgoliaClient -> initIndex('MainUnitInventoryTest');	
		}
		
		$index->clearIndex();
		$inventoryList = new CurrentInventoryList();
		
		$algoBatch = array();
		
		foreach($inventoryList -> DillonBrothersMainLiveInventory() as $currentInventorySingle) {
			$algoliaObj = new AlgoliaHandler();
			$algoliaObj -> MainUnitInventoryID = $currentInventorySingle['inventoryID'];
			$algoliaObj -> MainUnitCondition = $currentInventorySingle['Conditions'];
			$algoliaObj -> MainUnitYear = $currentInventorySingle['Year'];
			$algoliaObj -> MainUnitMake = $currentInventorySingle['Manufacturer'];
			$algoliaObj -> MainUnitModelNumber = $currentInventorySingle['ModelName'];
			$algoliaObj -> MainUnitFriendlyName = $currentInventorySingle['FriendlyModelName'];
			$algoliaObj -> MainUnitSEOUrl = $currentInventorySingle['inventorySeoURL'];
			$algoliaObj -> MainUnitCategoryName = $currentInventorySingle['inventoryCategoryName'];
			$algoliaObj -> MainUnitVinNumber = $currentInventorySingle['VinNumber'];
			$algoliaObj -> MainUnitStock = $currentInventorySingle['Stock'];
			$algoliaObj -> MainUnitStoreName = $currentInventorySingle['StoreName'];
			$algoliaObj -> MainUnitMarkAsSold = $currentInventorySingle['MarkAsSoldDate'];
			$algoliaObj -> MainUnitOverlayText = $currentInventorySingle['OverlayText'];
			$algoliaObj -> MainUnitTimeInserted = $currentInventorySingle['TimeInserted'];
			$algoliaObj -> MainUnitMileage = $currentInventorySingle['Mileage'];
			$algoliaObj -> MainUnitEngineSizeCC = $currentInventorySingle['EngineSizeCC'];
			$algoliaObj -> MainUnitMSRP = $currentInventorySingle['MSRP'];
			$algoliaObj -> MainUnitSmallMainImage = $currentInventorySingle['inventoryPhotoName'] . '-s.' . $currentInventorySingle['inventoryPhotoExt'];
			$algoliaObj -> MainUnitStoreID = $currentInventorySingle['InventoryStoreID'];
			$algoliaObj -> AddUpdateMainUnitInventory();
		}
	}


	public function AddUpdateMainUnitInventory() {
		$colorList = new ColorsList();
		if(LIVE_SITE == TRUE) {
			$index = $this -> _AlgoliaClient -> initIndex('MainUnitInventory');	
		} else {
			$index = $this -> _AlgoliaClient -> initIndex('MainUnitInventoryTest');	
		}
		
		//condition
		if($this -> MainUnitCondition == 0) {
			$condition = "New";
			$bikeURL = 'new/';
		} else {
			$condition = "Used";
			$bikeURL = 'used/';
		}	
		
		//friendly name check
		if($this -> MainUnitFriendlyName != NULL) {
			$modelName = $this -> MainUnitFriendlyName;
		} else {
			$modelName = $this -> MainUnitModelNumber;
		}
		
		//photo path
		$photoPath = PHOTO_URL . 'inventory/'.  $this -> MainUnitVinNumber . '/' . $this -> MainUnitSmallMainImage;
		
		//getting current colors
		$relatedFilterdColors = array();	
		$currentColors = array();
		$relatedColors = $colorList -> RelatedInventoryColors($this -> MainUnitInventoryID);
				
		foreach($relatedColors as $colorSingle) {
			array_push($relatedFilterdColors, $colorSingle['FilterColor']);
			array_push($currentColors, $colorSingle['ColorText']);
		}
		
		//creating store classname for button
		switch($this -> MainUnitStoreID) {
				case 2:
					$className="motorsportBtn";
					break;
				case 3:
					$className="harleyBtn";
					break;
				case 4:
					$className="indianBtn";
					break;
		}
		
		$inventoryObjInsert = array();
		$inventoryObjInsert['objectID'] = $this -> MainUnitInventoryID;
		$inventoryObjInsert['Conditions'] = $condition;
		$inventoryObjInsert['Year'] = intval($this -> MainUnitYear);
		$inventoryObjInsert['BikeName'] = $this -> MainUnitYear . ' ' . $this -> MainUnitMake . ' ' . $modelName;
		$inventoryObjInsert['BikeURL'] = WEBSITE_PATH . 'inventory/' . $bikeURL . $this -> MainUnitSEOUrl;
		$inventoryObjInsert['Manufacturer'] = $this -> MainUnitMake;
		$inventoryObjInsert['inventoryCategoryName'] = $this -> MainUnitCategoryName;
		$inventoryObjInsert['VinNumber'] = $this -> MainUnitVinNumber;
		$inventoryObjInsert['Stock'] = $this -> MainUnitStock;
		$inventoryObjInsert['StoreName'] = $this -> MainUnitStoreName;
		$inventoryObjInsert['MarkAsSoldDate'] = $this -> MainUnitMarkAsSold;
		$inventoryObjInsert['OverlayText'] = $this -> MainUnitOverlayText;
		$inventoryObjInsert['TimeInserted'] = $this -> MainUnitTimeInserted;
		$inventoryObjInsert['CurrentColors'] = implode('/', $currentColors);
		$inventoryObjInsert['FilteredColors'] = $relatedFilterdColors;	
		$inventoryObjInsert['Mileage'] = number_format($this -> MainUnitMileage, 0);
		$inventoryObjInsert['MileageInt'] = intval($this -> MainUnitMileage);		
		$inventoryObjInsert['EngineSizeCC'] = intval($this -> MainUnitEngineSizeCC);	
		$inventoryObjInsert['Image'] = $photoPath;		
		$inventoryObjInsert['ModelName'] = $this -> MainUnitModelNumber;			
		
		if(empty($this -> MainUnitFriendlyName)) {
			$inventoryObjInsert['FriendlyModelName'] = $this -> MainUnitModelNumber;
		} else {
			$inventoryObjInsert['FriendlyModelName'] = $this -> MainUnitFriendlyName;	
		}
		
		$inventoryObjInsert['InventoryClassName'] = $className;				    
		
		if($this -> MainUnitMSRP > CALLTOPRICE_THRESHOLD) {
			$inventoryObjInsert['Price'] = '$' . number_format($this -> MainUnitMSRP, 0);	
			$inventoryObjInsert['PriceInt'] = intval($this -> MainUnitMSRP);	
		} else {
			$inventoryObjInsert['Price'] = 'Call For Price';
		}
		
		$index->saveObject($inventoryObjInsert);		
	}

	public function UpdateMainUnitColors() {
		$colorList = new ColorsList();
		if(LIVE_SITE == TRUE) {
			$index = $this -> _AlgoliaClient -> initIndex('MainUnitInventory');	
		} else {
			$index = $this -> _AlgoliaClient -> initIndex('MainUnitInventoryTest');	
		}
		
		$relatedFilterdColors = array();	
		$currentColors = array();
		$relatedColors = $colorList -> RelatedInventoryColors($this -> MainUnitInventoryID);
				
		foreach($relatedColors as $colorSingle) {
			array_push($relatedFilterdColors, $colorSingle['FilterColor']);
			array_push($currentColors, $colorSingle['ColorText']);
		}
		
		$index->partialUpdateObject(
		  [
		    'objectID' => $this -> MainUnitInventoryID,
		    'CurrentColors' => implode('/', $currentColors),
		    'FilteredColors' => $relatedFilterdColors
		  ]
		);		
		
	}


	public function AddUpdateShoppinInventory() {
		if(LIVE_SITE == TRUE) {
			$index = $this -> _AlgoliaClient -> initIndex('ShoppingCartInventory');	
		} else {
			$index = $this -> _AlgoliaClient -> initIndex('ShoppingCartInventoryTest');	
		}
		
		switch($this -> ecommerceProductStoreID) {
			case 2;
				$priceCss = 'MotorSportPrice';
				break;
			case 3;
				$priceCss = 'HarleyPrice';
				break;
			case 4;
				$priceCss = 'IndianPrice';
				break;
		}
		
				
		$index->saveObject(
		  [
		    'objectID' => $this -> ecommerceProductID,
		    'productImage' => $this -> ecommerceProductImage,
		    'productSEOName' => $this -> ecommerceProductSEOName,
		    'productName'  => $this -> ecommerceProductName,
		    'productPrice' => $this -> ecommerceProductPrice,
		    'className' => $priceCss
		  ]
		);
	}


	public function RemoveMainUnitInventory() {
		if(LIVE_SITE == TRUE) {
			$index = $this -> _AlgoliaClient -> initIndex('MainUnitInventory');	
		} else {
			$index = $this -> _AlgoliaClient -> initIndex('MainUnitInventoryTest');	
		}
		
		$index->deleteObject($this -> MainUnitInventoryID);
	}
	
	
	public function RemoveSHoppingInventory() {
		if(LIVE_SITE == TRUE) {
			$index = $this -> _AlgoliaClient -> initIndex('ShoppingCartInventory');	
		} else {
			$index = $this -> _AlgoliaClient -> initIndex('ShoppingCartInventoryTest');	
		}
		
		$index->deleteObject($this -> ecommerceProductID);
		
	}
	
	
}