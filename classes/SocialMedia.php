<?php

class SocialMediaObject extends BaseObject {
	
	private $_id;
	
	public $SocialMediaType;
	public $Photo;
	public $photoTemp;
	public $newName;
	
	public $PostContent;
	public $KeyWords;
	
	public $submittedBy;
	public $StoreID;
	public $UrgentDate;
	public $IsUrgent;
	public $submittedByFirstName;
	public $submittedByLastName;
	
	public $StoreName;
	
	public $SubmittedDate;
	public $SubmittedTime;
	
	public $DeclineNotes;
	
	public $isEditable;
	
	public $IsApproved;
	public $IsDeclined;
	public $IsPosted;
	
	public $ApprovalDeclineStatus;
	public $ApprovalDeclineByPerson;
	public $ApprovalDeclineDate;
	public $ApprovalDeclineTime;
	
	private $UserSubmittedEmail;
	
	public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }
	
		
	public static function WithID($socialMediaID) {
        $instance = new self();
        $instance->_id = $socialMediaID;
        $instance->loadById();
        return $instance;
    }
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM socialmediaposts 
    												 LEFT JOIN stores ON socialmediaposts.StoreID = stores.storeID
    												 LEFT JOIN users ON socialmediaposts.submittedBy = users.userID 
    												 LEFT JOIN (
    												 	SELECT socialmediastatus.mediaPostID, 
    												 							(concat(users.firstName, " ", users.lastName)) StatusByPerson, 
    												 							(socialmediastatus.Status) ApproveDeclineStatus, 
    												 							socialmediastatus.approvedDeclineDate, 
    												 							socialmediastatus.approveDeclineTime FROM socialmediastatus 
    												 																	  LEFT JOIN users ON socialmediastatus.userSubmittedBy = users.userID WHERE mediaPostID = :socialID 
																) ApproveDeclineStatus ON socialmediaposts.socialID = ApproveDeclineStatus.mediaPostID WHERE socialID = :socialID');
		
		
        $sth->execute(array(':socialID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
		
    }
	
    protected function fill(array $row){
    	$this -> PostContent = $row['Content'];
		$this -> KeyWords = $row['KeyWords'];
		$this -> submittedByFirstName = $row['firstName'];
		$this -> submittedByLastName = $row['lastName'];
		$this -> SocialMediaType = $row['socialMediaType'];
		$this -> SubmittedDate = $row['submittedDate'];
		$this -> SubmittedTime = $row['time'];
		$this -> IsUrgent = $row['isUrgent'];
		$this -> UrgentDate = $row['isUrgentDate'];
		$this -> isEditable = $row['isEditable'];
		$this -> IsApproved = $row['isApproved'];
		$this -> IsDeclined = $row['isDeclined'];
		$this -> IsPosted = $row['isPosted'];
		$this -> ApprovalDeclineStatus = $row['ApproveDeclineStatus'];
		$this -> ApprovalDeclineByPerson = $row['StatusByPerson'];
		$this -> ApprovalDeclineDate = $row['approvedDeclineDate'];
		$this -> ApprovalDeclineTime = $row['approveDeclineTime'];
		$this -> UserSubmittedEmail = $row['UserEmail'];
		$this -> StoreName = $row['StoreName'];
		$this -> IsPosted = $row['isPosted'];
    }	
	
	public function GetID() {
		return $this->_id;
	}
	
	
	public function UploadPostPhoto() {
		$ImagePathParts = array();
		$subPath = NULL;
		
		switch($this -> SocialMediaType) {
			case 1:
				$subPath = "/Facebook";
				break;
			case 2:
				$subPath = "/Twitter";
				break;
		}
		
		$ImagePathParts['SocialMediaType'] = $subPath;
		
		Image::MoveAndRenameImage($this -> photoTemp, 
								  $ImagePathParts,
								  $this -> Photo,
								  $this -> newName, 6);	
	}
	
	public function Validate() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> PostContent)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} 							
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function ValidateDeclinePost() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> DeclineNotes)) {
			array_push($validationErrors, array('inputID' => "Decline",
												'errorMessage' => 'Required'));
		} 							
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Save() {
	
		if(isset($this->_id)) {
			
			$postData = array('Content' => $this -> PostContent,
							  'KeyWords' => $this -> KeyWords);	
				
			$this->db->update('socialmediaposts', $postData, array('socialID' => $this -> _id));
		
		} else {
			$this -> db -> insert('socialmediaposts', array('socialMediaType' => $this -> SocialMediaType,
															'Content' => $this -> PostContent,
															'KeyWords' => $this -> KeyWords,
															'StoreID' => $this -> StoreID,
															'submittedDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
															'time' => date("H:i:s", $this -> time -> NebraskaTime()),
															'submittedBy' => $this -> submittedBy,
															'isUrgent' => (!empty($this -> UrgentDate) ? 1 : 0),
															'isUrgentDate' => (!empty($this -> UrgentDate) ? $this -> UrgentDate : NULL)));	
															
			if(LIVE_SITE == true) {
				if(!empty($this -> UrgentDate)) {
					$setting = SettingsObject::Init();	
					$storeSingle = Store::WithID($this -> StoreID);
						
					$emailContent = array(); 	
				
					$emailContent['storeName'] = $storeSingle -> storeName;
					$emailContent['urgentTime'] = $this -> time -> formatShortDate($this -> UrgentDate);
					
					$this -> email -> to = $setting -> UrgentEmail;	
					$this -> email -> subject = "Urgent Post Submission";
					$this -> email -> UrgentPost($emailContent);
					
				}						
			}						
															
															
		}
	
		
												 
												 
		
	}
	
	public function ApprovePost() {
		Session::init();	
		$postData = array('isApproved' => 1,
						  'isEditable' => 0);	
				
		$this->db->update('socialmediaposts', $postData, array('socialID' => $this -> _id));
		
		$this -> db -> insert('socialmediastatus', array('Status' => "Approved",
														 'userSubmittedBy' => $_SESSION['user'] -> _userId,
														 'approvedDeclineDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
														 'approveDeclineTime' => date("H:i:s", $this -> time -> NebraskaTime()),
														 'mediaPostID' => $this -> _id));	
		
		$this -> redirect -> redirectPage(PATH. 'socialmedia/view/post/' . $this -> _id);
		
	}
	
	public function DeclinePost() {
		Session::init();	
		$postData = array('isDeclined' => 1,
						  'isEditable' => 0,
						  'isDeclinedNotes' => $this -> DeclineNotes);	
				
		$this->db->update('socialmediaposts', $postData, array('socialID' => $this -> _id));
		
		$this -> db -> insert('socialmediastatus', array('Status' => "Declined",
														 'userSubmittedBy' => $_SESSION['user'] -> _userId,
														 'approvedDeclineDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
														 'approveDeclineTime' => date("H:i:s", $this -> time -> NebraskaTime()),
														 'mediaPostID' => $this -> _id));	
		
		
		if(LIVE_SITE == true) {
			$emailContent = array(); 	
			switch($this -> SocialMediaType) {
				case '1':
					$emailContent["type"] = "Facebook";
					$this -> email -> subject = "Facebook Post Declined";
					break;
				case '2':
					$emailContent["type"] = "Twitter";
					$this -> email -> subject = "Twitter Post Declined";
					break;
			}
			
			$emailContent['post-content'] = $this -> PostContent;
			$emailContent['decline-notes'] = $this -> DeclineNotes;
			
			$this -> email -> to = $this -> UserSubmittedEmail;	
			$this -> email -> DeclinedPost($emailContent);
		}
				
		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'socialmedia/view/post/' . $this -> _id);	
	}
	
	
	public function MarkAsPosted() {
		$postData = array('isPosted' => 1,
						  'isPostedDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
						  'isPostedDateTime' => date("H:i:s", $this -> time -> NebraskaTime()));	
				
		$this->db->update('socialmediaposts', $postData, array('socialID' => $this -> _id));
		
				
		$this -> redirect -> redirectPage(PATH. 'socialmedia/view/post/' . $this -> _id);
	}
	
	public function RedirectToStore() {
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'socialmedia/yourstore/' . $this -> StoreID);
	}
		
	public function RedirectToPost() {
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'socialmedia/view/post/' . $this -> _id);
	}
	

	

}