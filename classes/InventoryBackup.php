<?php

class InventoryBackup extends BaseObject {
	
	private $_id;
	private $_vinCheckSQL;
	
	private $_newBackupID;
	
	private $_newBackup;
	
	public $inventoryBackupVinNumber;
	public $inventoryBackupYear;
	public $inventoryBackupManufactur;
	public $inventoryBackupModelName;
	public $NewModelNumber;
	public $inventoryBackupModelFriendlyName;
	public $inventoryBackupSpecJSON;
	
	public $inventoryBackupDescription;
	public $newDescription;
	
	public $NewEngizeCC;
	public $enginSizeCC;
	
	public $backupNewSpecs;
	
	public $backupInventoryFromCurrentInventory = false;
	
	public $VinNumberInventoryBackupID;
	
	public $NewCategory;
	public $Category;
	
	
	public $CurrentInventoryUsing = array();
	public $LinkedInventoryArray = array();
	
	public $NewManufactureText;
	
	public $vehicleVerified;
	
	public $TalonYear;
	public $TalonMake;
	public $TalonModel;
	
	public $CategoryName;
	
	public $AssociatedVinOptions;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($backupID) {
        $instance = new self();
        $instance->_id = $backupID;
        $instance->loadById();
        return $instance;
    }
	
	public static function WithVIN($vin) {
        $instance = new self();
        $instance->_vinCheckSQL = $vin;
        $instance->loadByVIN();
        return $instance;
    }
	
	protected function loadByVIN() {
		$sth = $this -> db -> prepare('SELECT * FROM vinnumberbackup 
										LEFT JOIN inventorybackupsystem ON vinnumberbackup.inventoryBackupBikeID = inventorybackupsystem.inventoryBackupID 
									    LEFT JOIN inventorycategories ON inventorybackupsystem.inventoryBackupCategory = inventorycategories.inventoryCategoryID
									    WHERE vinnumberbackup.backedupVinNumber = :VinNumber');
        $sth->execute(array(':VinNumber' => $this->_vinCheckSQL));	
        $record = $sth -> fetch();
        $this->fill($record);
	}
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM inventorybackupsystem LEFT JOIN inventorycategories ON inventorybackupsystem.inventoryBackupCategory = inventorycategories.inventoryCategoryID WHERE inventoryBackupID = :inventoryBackupID');
        $sth->execute(array(':inventoryBackupID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
		$this -> CurrentUsingBackup();
    }
	
    protected function fill(array $row){
    	$this -> _id = $row['inventoryBackupID'];
		$this -> inventoryBackupYear = $row['inventoryBackupYear'];
		$this -> inventoryBackupManufactur = $row['inventoryBackupManufactur'];
		$this -> inventoryBackupModelName = $row['inventoryBackupModelName'];
		$this -> inventoryBackupModelFriendlyName = $row['inventoryBackupModelFriendlyName'];
		$this -> inventoryBackupSpecJSONFile = $row['inventoryBackupSpecJSON'];
		$this -> inventoryBackupDescription = $row['inventoryBackupDecription'];
		$this -> enginSizeCC = $row['backupEngineSizeCC'];
		$this -> Category = $row['inventoryBackupCategory'];
		$this -> vehicleVerified = $row['BackupVehicleVerified'];
		$this -> TalonYear = $row['backupYearCheck'];
		$this -> TalonMake = $row['backupManufactureCheck'];
		$this -> TalonModel = $row['backupModelCheck'];	
		$this -> CategoryName = $row['inventoryCategoryName'];
		if(isset($this->_vinCheckSQL)) {
			
			$this -> VinNumberInventoryBackupID = $row['inventoryBackupID'];	
		}
    }	
	
	public function GetID() {
		return $this -> _id;
	}
	
	public function GetSpecs() {
		return file_get_contents(INVENTORY_VEHICLE_INFO_URL . 'backup/' .$this -> inventoryBackupSpecJSONFile);
	}

	private function CurrentUsingBackup() {
		$currentUsing = $this -> db -> prepare("SELECT * FROM inventorybackupsystem LEFT JOIN vinnumberbackup ON inventorybackupsystem.inventoryBackupID = vinnumberbackup.inventoryBackupBikeID INNER JOIN inventory ON vinnumberbackup.backedupVinNumber = inventory.VinNumber WHERE inventoryBackupID = :inventoryBackupID");
		$currentUsing -> execute(array(":inventoryBackupID" => $this->_id));
		$this -> CurrentInventoryUsing = $currentUsing -> fetchAll();
	}

	public function UpdateManufacture() {
		$postData = array('inventoryBackupManufactur' => $this -> NewManufactureText);
		$this->db->update('inventorybackupsystem', $postData, array('inventoryBackupID' => $this->_id));
	}
	
	public function UpdateVerificationStatus() {
		$postData = array('BackupVehicleVerified' => $this -> vehicleVerified);
		$this->db->update('inventorybackupsystem', $postData, array('inventoryBackupID' => $this->_id));
	}
	
	private function GenerateNewJsonFileName() {
		$jsonFileName = '';
		
		$jsonNameArray = array();
		
		array_push($jsonNameArray, parent::CleanJSONString($this -> inventoryBackupYear));
		array_push($jsonNameArray, parent::CleanJSONString($this -> inventoryBackupManufactur));
		array_push($jsonNameArray, parent::CleanJSONString($this -> NewModelNumber));
		array_push($jsonNameArray, parent::CleanJSONString($this -> inventoryBackupModelFriendlyName));
		
		foreach($jsonNameArray as $item) {
			$jsonFileName .= $item. '-';
		}
		
		$jsonFileName = rtrim($jsonFileName, '-');
					
		$jsonFileName .= '.json';
		
		return $jsonFileName;
	}
	
	private function SaveNewSpecs($specJSONstring) {
		//current json file name
		$oldJSONFilename = INVENTORY_VEHICLE_INFO_PATH . 'backup/' . $this -> inventoryBackupSpecJSONFile;
		
		//need to update the json string first before it is renamed
		$newSpecs = json_decode($specJSONstring, true);
		
		$newSavedStringArray = array();
		
		
		$labelList = new SpecList();
		
		foreach($labelList -> LabelsByCategory($this -> NewCategory) as $label) {
			$specKey = array_search($label['specLabelID'], array_column($newSpecs, 'LabelID'));
			
			if($specKey !== false) {
				
				array_push($newSavedStringArray, array("LabelID" => $label['specLabelID'],
													   "Content" => filter_var($newSpecs[$specKey]['Content'], FILTER_SANITIZE_STRING)));
			}
				
		}
		
		$JSONVinInfo = fopen($oldJSONFilename, "w");
		fwrite($JSONVinInfo, json_encode($newSavedStringArray));
		fclose($JSONVinInfo);	
		
		$newJSONFileName = INVENTORY_VEHICLE_INFO_PATH . 'backup/' . $this -> GenerateNewJsonFileName();
		rename($oldJSONFilename, $newJSONFileName);
	}
	
	public function DeleteValidate() {
		$validationErrors = array();
		
		foreach($this -> AssociatedVinOptions as $vinOptionSingle) {
			if($vinOptionSingle == 0) {
				array_push($validationErrors, array('inputID' => 99,
												    'errorMessage' => 'There are missing input options for vin, please choose to reassign/no assignment'));
				break;								
			}
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	function ReassignValidate() {
		$validationErrors = array();
		
		foreach($this -> AssociatedVinOptions as $vinOptionSingle) {
			if($vinOptionSingle == 0) {
				array_push($validationErrors, array('inputID' => 99,
												'errorMessage' => 'There are missing input options for vin, please choose to delete/reassign/no assignment'));
				break;								
			}
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Update() {
		
		$this -> SaveNewSpecs($this -> backupNewSpecs);		
		
		$updateBikeDBData = array();
		$updateBikeDBData['inventoryBackupYear'] = $this -> inventoryBackupYear;
		$updateBikeDBData['inventoryBackupManufactur'] = $this -> inventoryBackupManufactur;
		$updateBikeDBData['inventoryBackupModelName'] = filter_var($this -> NewModelNumber, FILTER_SANITIZE_STRING);
		
		if($this -> NewModelNumber != $this -> inventoryBackupModelName) {
			$updateBikeDBData['backupModelCheck'] = parent::CleanStringNoSpaces(strtolower(filter_var($this -> NewModelNumber, FILTER_SANITIZE_STRING)));
		}
		
		$updateBikeDBData['inventoryBackupModelFriendlyName'] = filter_var($this -> inventoryBackupModelFriendlyName, FILTER_SANITIZE_STRING);
		$updateBikeDBData['inventoryBackupSpecJSON'] = $this -> GenerateNewJsonFileName();		
		$updateBikeDBData['inventoryBackupDecription'] = $this -> newDescription;			
		$updateBikeDBData['backupEngineSizeCC'] = $this -> NewEngizeCC;			
		$updateBikeDBData['inventoryBackupCategory'] = $this -> NewCategory;			
		$updateBikeDBData['IsNewBackup'] = 0;					
				

		if(count($this -> CurrentInventoryUsing) > 0) {
			
		 	foreach($this -> CurrentInventoryUsing as $vinSingle) {
		 		$currentInventoryUpdate = InventoryObject::WithID($vinSingle['inventoryID']);
				$currentInventoryUpdate -> Year = $this -> inventoryBackupYear;
				$currentInventoryUpdate -> Manufacture = $this -> inventoryBackupManufactur;
				$currentInventoryUpdate -> NewModel = filter_var($this -> NewModelNumber, FILTER_SANITIZE_STRING);
				$currentInventoryUpdate -> ModelFriendlyName = filter_var($this -> inventoryBackupModelFriendlyName, FILTER_SANITIZE_STRING);
				$currentInventoryUpdate -> SpecJSON = $this -> backupNewSpecs;
				$currentInventoryUpdate -> Category = $this -> NewCategory;
				$currentInventoryUpdate -> EngineSizeCC = $this -> NewEngizeCC;
						
				$currentInventoryUpdate -> SavedFromBackup();
				$currentInventoryUpdate -> UpdateAlgoIndexItem($vinSingle['inventoryID']);
		 	}
		}
				
		$this->db->update('inventorybackupsystem', $updateBikeDBData, array('inventoryBackupID' => $this -> _id));
		
		$this -> _newBackup = $this -> _id;
		
		$log = new EditHistoryLog();
		$log -> itemID = $this -> _id;
		$log -> userID = $_SESSION['user'] -> _userId;
		$log -> itemType = 4;		
		$log -> Save();	
				
	}


	//this is executed from current inventoryitem
	public function UpdateBackupItem() {
			
		$this -> SaveNewSpecs($this -> backupNewSpecs);	
				
		$postData = array('inventoryBackupYear' => $this -> inventoryBackupYear,
						  'inventoryBackupManufactur' => $this -> inventoryBackupManufactur, 
						  'inventoryBackupModelName' => $this -> inventoryBackupModelName,
						  'inventoryBackupModelFriendlyName' => $this -> inventoryBackupModelFriendlyName,
						  'inventoryBackupSpecJSON' => $this -> GenerateNewJsonFileName(),
						  'backupEngineSizeCC' => $this -> NewEngizeCC,
						  'inventoryBackupCategory' => $this -> NewCategory,
						  "IsNewBackup" => 0);	
				
		$this->db->update('inventorybackupsystem', $postData, array('inventoryBackupID' => $this -> _id));
		
		
		$this -> _newBackup = $this -> _id;
		
		$log = new EditHistoryLog();
		$log -> itemID = $this -> _id;
		$log -> userID = $_SESSION['user'] -> _userId;
		$log -> itemType = 4;		
		$log -> Save();			
	}
	

	public function GetNewBackupID() {
		return $this -> _newBackupID;
	}
	
	public function SaveBackup() {
		try {
			
			$jsonNameArray = array();
			
			array_push($jsonNameArray, parent::CleanJSONString($this -> inventoryBackupYear));
			array_push($jsonNameArray, parent::CleanJSONString($this -> inventoryBackupManufactur));
			array_push($jsonNameArray, parent::CleanJSONString($this -> NewModelNumber));
			array_push($jsonNameArray, parent::CleanJSONString($this -> inventoryBackupModelFriendlyName));
			
			
			$descriptionTxtFile = NULL;
			$jsonFileName = NULL;
			foreach($jsonNameArray as $item) {
				$jsonFileName .= $item. '-';
			}
			
			$jsonFileName = rtrim($jsonFileName, '-');
			
			
			$jsonFileName .= '.json';
			
			$newInsertDBData = array();
			$newInsertDBData['inventoryBackupYear'] = parent::CleanString($this -> inventoryBackupYear);
			$newInsertDBData['inventoryBackupManufactur'] = parent::CleanString($this -> inventoryBackupManufactur);
			$newInsertDBData['inventoryBackupModelName'] = parent::CleanString($this -> NewModelNumber);
			$newInsertDBData['inventoryBackupModelFriendlyName'] = parent::CleanString($this -> inventoryBackupModelFriendlyName);
			$newInsertDBData['inventoryBackupDecription'] = $this -> newDescription;
			$newInsertDBData['backupYearCheck'] = parent::CleanString($this -> inventoryBackupYear);
			$newInsertDBData['backupModelCheck'] = parent::CleanStringNoSpaces(strtolower($this -> NewModelNumber));
			$newInsertDBData['backupManufactureCheck'] = parent::CleanStringNoSpaces(strtolower($this -> inventoryBackupManufactur));
			$newInsertDBData['inventoryBackupSpecJSON'] = $jsonFileName;
			$newInsertDBData['inventoryBackupCategory'] = $this -> NewCategory;
			$newInsertDBData['backupEngineSizeCC'] = $this -> NewEngizeCC;
			
			$newBackup = $this -> db -> insert('inventorybackupsystem', $newInsertDBData);		
			
			if(isset($this -> inventoryBackupVinNumber)) {
				$backupVin = new BackupVinNumber();
				$backupVin -> BackupVinNumber = $this -> inventoryBackupVinNumber;
				$backupVin -> inventoryBackupInventoryID = $newBackup;													 						
				$backupVin -> Save();	
			}	
														
			$this -> _newBackup = $newBackup;
															
			$fileName = INVENTORY_VEHICLE_INFO_PATH . 'backup/' . $jsonFileName;
			$JSONVinInfo = fopen($fileName, "w");
			fwrite($JSONVinInfo, '[]');
			fclose($JSONVinInfo);
			
			$this -> _newBackupID = $newBackup;
			
			$this -> redirect -> redirectPage(PATH. 'inventory/editbackup/' . $this -> _newBackup);
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Employee Save Error: " . $e->getMessage();
			$TrackError -> type = "EMPLOYEE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}

	public function Delete() {
		$vins = new BackupVinsList();
		$vinNumber = new BackupVinNumber();
		foreach($vins -> ByBackedupID($this->_id) as $key => $vinSingle) {
			$vinNumber -> id = $vinSingle['vinBackupID'];	
			if($this -> AssociatedVinOptions[$key] == '-1') {
				$vinNumber -> Delete();	
			} else if($this -> AssociatedVinOptions[$key] == '-2') {
				$vinNumber -> inventoryBackupInventoryID = 0;
				$vinNumber -> Update();
			} else {
				$vinNumber -> inventoryBackupInventoryID = $this -> AssociatedVinOptions[$key];
				$vinNumber -> Update();
			}			
		}
		
		unlink(INVENTORY_VEHICLE_INFO_PATH . 'backup/' . $this -> inventoryBackupSpecJSONFile);
		
		$sth = $this -> db -> prepare("DELETE FROM inventorybackupsystem WHERE inventoryBackupID = :inventoryBackupID");
		$sth -> execute(array('inventoryBackupID' => $this->_id));
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'inventory/backup/1');		
	}

	public function ReassignVinNumbers() {
		$vins = new BackupVinsList();
		$vinNumber = new BackupVinNumber();
		foreach($vins -> ByBackedupID($this->_id) as $key => $vinSingle) {
			$vinNumber -> id = $vinSingle['vinBackupID'];	
			if($this -> AssociatedVinOptions[$key] == '-1') {
				//$vinNumber -> Delete();	
			} else if($this -> AssociatedVinOptions[$key] == '-2') {
				$vinNumber -> inventoryBackupInventoryID = 0;
				$vinNumber -> Update();
			} else {
				$vinNumber -> inventoryBackupInventoryID = $this -> AssociatedVinOptions[$key];
				$vinNumber -> Update();
			}			
		}
		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'inventory/editbackup/' . $this->_id);	
	}

	public function RedirectToBackup() {
		$this -> redirect -> redirectPage(PATH. 'inventory/editbackup/' . $this -> _newBackup);
	}
	
	public function SendToMonkeyLearn() {
		$backup = new BackupInventoryList();
		require 'libs/MonkeyLearn/autoload.php';		
		$module_id = 'cl_4LLnXuzp';
		$ml = new MonkeyLearn\Client('c6046ac2da646a10e42c70be661ecf6fbe565c21');
		$parent_category_id = 13854345;
		
		foreach($backup -> NonMonkeyLearnBackedUpInventory() as $backupItemSingle) {
			$category = InventoryCategoryObject::WithID($backupItemSingle['inventoryBackupCategory']);
						
			if(!empty($category -> MonkeyLearnID)) {
				$categoryTextPlacement = NULL;	
				
				if($category -> CategoryText == "Youth ATV") {$categoryTextPlacement = '/ATV/';}
				if($category -> CategoryText == "Sport ATV") {$categoryTextPlacement = '/ATV/';}
				if($category -> CategoryText == "Utility ATV") {$categoryTextPlacement = '/ATV/';}
				
				$samples = array(
				    array($backupItemSingle['inventoryBackupManufactur'] . ' ' . $backupItemSingle['inventoryBackupModelName'], $categoryTextPlacement . $category -> CategoryText)
				);
				$newSample = $ml->classifiers->upload_samples($module_id, $samples);
				
			} else {
				$newCategory = $ml->classifiers->categories->create($module_id, $category -> CategoryText, $parent_category_id);
				
				$samples = array(
				    array($this -> inventoryBackupManufactur . ' ' . $this -> inventoryBackupModelName, $newCategory -> result['category']['id'])
				);
				$newSample = $ml->classifiers->upload_samples($module_id, $samples);
				
				$category -> MonkeyLearnID = $newCategory->result['category']['id'];
				$category -> UpdateMonkeyLearnCategoryID();
			}
			
			$postData = array('backupVehicleSentToMonkeyLearn' => 1);	
			$this->db->update('inventorybackupsystem', $postData, array('inventoryBackupID' => $backupItemSingle['inventoryBackupID']));	
		}
		
		$ml->classifiers->train($module_id);
		//$ml->classifiers->deploy($module_id);
		
	}

	
	


}