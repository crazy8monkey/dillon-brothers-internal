<?php


class Brand extends BaseObject {
	
	private $_id;
	private $_currentExpiredDate;
	public $_currentSpecialImage;
	
	public $_brandName;
	public $brandID;
	public $StoreID;
	public $BrandImage;
	
	public $offerLinks;
	public $NewBrandName;
	
	public $brandDescription;
	public $NewBrandImage;
	public $NewAlternateLogo;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithBrand($name) {
        $instance = new self();
        $instance-> _brandName = $name;
        $instance-> loadRow();
        return $instance;
    }

	public static function WithID($brandID) {
        $instance = new self();
        $instance-> _id = $brandID;
        $instance->loadRow();
        return $instance;
    }
	
	public function GetID() {
		return $this -> _id;
	}
	
		
	protected function loadRow() {
		$sth = $this -> db -> prepare('SELECT * FROM brands WHERE BrandID = :BrandID');
        $sth->execute(array(':BrandID' => $this->_id));
		
        $record = $sth->fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> brandID = $row['BrandID'];
		$this -> offerLinks = $row['OfferPageRef'];
		$this -> _brandName = $row['BrandName'];
		$this -> brandDescription = $row['BrandDescription'];
		$this -> StoreID = $row['Store'];
		$this -> BrandImage = $row['BrandImage'];
		$this -> NewAlternateLogo = $row['AlternateBrandImage'];
    }
	
	
	public function GetNewSpecialBreadCrumbs() {
		return "<a href='" . $this -> pages -> specials() . "'>Types of Specials</a><span><i class='fa fa-caret-right' aria-hidden='true'></i></span>".
			   "<a href='" . $this -> pages -> specials() . "/category/OEM'>OEM Specials</a><span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   "<a href='". $this -> pages -> specials() . "/OEM/" . $this -> brandID . "'>" . $this -> _brandName . " Specials</a><span><i class='fa fa-caret-right' aria-hidden='true'></i></span>New " . $this -> _brandName . " Special";
	}
	
	
	public function Validate() {
		$validationErrors = array();
		

		$duplicateBrandNameContent = $this -> db -> prepare('SELECT * FROM (SELECT brands.BrandName, 
																				 (1) ISOEMBrand, 
																				 (1) BrandType, 
																				 brands.BrandImage, 
																				 brands.BrandID,
									               								 brands.brandNameSEOUrl FROM brands 
																		    UNION 
																		    SELECT partaccessorybrand.brandName, 
																		  		 (0) ISOEMBrand, (partaccessorybrand.BrandType) BrandType, 
																		  		 partaccessorybrand.partBrandImage, 
																		  		 partaccessorybrand.partAccessoryBrandID,
									               								 partaccessorybrand.brandSEOUrl FROM partaccessorybrand) TotalBrands WHERE brandNameSEOUrl <> :brandNameSEOUrl');
		$duplicateBrandNameContent -> execute(array(":brandNameSEOUrl" => parent::seoUrl($this -> NewBrandName)));																		 
																				 

		if($this -> validate -> emptyInput($this -> NewBrandName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if($duplicateBrandNameContent -> fetchAll()) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'There is a brand name associated with a different record'));
		}

		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	private function uploadPicture() {
		$ImagePathParts = array();
		
		Image::MoveAndRenameImage($_FILES['brandLogo']['tmp_name'], 
								  $ImagePathParts,
								  $this -> NewBrandImage,
								  "Dillon" . parent::seoUrl($this -> NewBrandName), 14);	
								  
		
		$fileExt = explode('.', $this -> NewBrandImage);
		$PartBrandFileDirectory = PHOTO_PATH . "brands/"; 
		
		$currentPhotoUploaded = $PartBrandFileDirectory . "Dillon" . parent::seoUrl($this -> NewBrandName) . '.' . $fileExt[1];
		
		list($origWidth, $origHeight) = getimagesize($currentPhotoUploaded);
		
		
		//small
		if(120 > $origWidth) {
			$resizeWidth = $origWidth;
			$resizeHeight = $origHeight;		
		} else {
			$resizeWidth = 120;
			$resizeHeight = 120;	
		}	
		
		Image::resizeImage($currentPhotoUploaded, 
						   $PartBrandFileDirectory . "Dillon" . parent::seoUrl($this -> NewBrandName) . '.' . $fileExt[1], 
						   $resizeWidth, 
						   $resizeHeight);
						   			  				  
	}
	
	private function uploadAlternatePicture() {
		$ImagePathParts = array();
		
		Image::MoveAndRenameImage($_FILES['alternateBrandLogo']['tmp_name'], 
								  $ImagePathParts,
								  $this -> NewAlternateLogo,
								  "DillonAlternate" . parent::seoUrl($this -> NewBrandName), 14);	
								  
		
		$fileExt = explode('.', $this -> NewAlternateLogo);
		$PartBrandFileDirectory = PHOTO_PATH . "brands/"; 
		
		$currentPhotoUploaded = $PartBrandFileDirectory . "DillonAlternate" . parent::seoUrl($this -> NewBrandName) . '.' . $fileExt[1];
		
		list($origWidth, $origHeight) = getimagesize($currentPhotoUploaded);
		
		
		//small
		if(120 > $origWidth) {
			$resizeWidth = $origWidth;
			$resizeHeight = $origHeight;		
		} else {
			$resizeWidth = 120;
			$resizeHeight = 120;	
		}	
		
		Image::resizeImage($currentPhotoUploaded, 
						   $PartBrandFileDirectory . "DillonAlternate" . parent::seoUrl($this -> NewBrandName) . '.' . $fileExt[1], 
						   $resizeWidth, 
						   $resizeHeight);
						   			  				  
	}
	
	
	public function Save() {
		try {
			$oemUpdateDBdata = array();
			$oemUpdateDBdata['BrandName'] = $this -> NewBrandName;
			$oemUpdateDBdata['brandNameSEOUrl'] = parent::seoUrl($this -> NewBrandName);
			$oemUpdateDBdata['BrandDescription'] = $this -> brandDescription;
			
			if(!empty($this -> NewBrandImage)) {
				$this -> uploadPicture();
				$fileExt = explode('.', $this -> NewBrandImage);
				$oemUpdateDBdata['BrandImage'] = "Dillon" . parent::seoUrl($this -> NewBrandName) . '.' . $fileExt[1];
				
			}
			
			if(isset($this -> NewAlternateLogo)) {
				if(!empty($this -> NewAlternateLogo)) {
					$this -> uploadAlternatePicture();
					$alternatefileExt = explode('.', $this -> NewAlternateLogo);
					$oemUpdateDBdata['AlternateBrandImage'] = "DillonAlternate" . parent::seoUrl($this -> NewBrandName) . '.' . $alternatefileExt[1];	
				}
				
			}
			
			$this->db->update('brands', $oemUpdateDBdata, array('BrandID' => $this -> brandID));
				
		
			
			$this -> json -> outputJqueryJSONObject('redirect', array('PATH' => PATH . 'brands/view/OEM/'. $this->_id));
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Event Save Error: " . $e->getMessage();
			$TrackError -> type = "EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	

	
	

		

}