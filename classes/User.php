<?php

class User extends BaseObject {

	public $_userId;
	public $FirstName;
	public $LastName;
	public $Username;
	public $currentUserID;
	public $Email;
	public $securityToken;
	public $IsProfileView;
	
	public $ProfileImageName;
	public $ProfileImageTempName;
	
	public $currentProfilePic;
	
	public $AssociatedStores = array();
	
	public $_permissions = array();
	private $_newPermissions = array();

    public function __sleep()
    {
        parent::__sleep();
        return array('_userId', 'FirstName', 'LastName', 'Email', '_permissions', '_password', 'currentProfilePic');
    }

    public function __wakeup()
    {
        parent::__wakeup();
		return array('_userId', 'FirstName', 'LastName', 'Email', '_permissions', '_password', 'currentProfilePic');
    }


    public function __construct(){
        parent::__construct();
    }

    public static function WithID($userId){
        $instance = new self();
        $instance->_userId = $userId;
        $instance->loadById();
        return $instance;
    }


	
	
    protected function loadByID()
    {
        $sth = $this -> db -> prepare('SELECT * FROM users WHERE userID = :userID');
        $sth->execute(array(':userID' => $this->_userId));
        $record = $sth -> fetch();
        $this->fill($record);
        $this->LoadPermissions();
    }

    protected function fill(array $row){
        $this->FirstName = $row['firstName'];
        $this->LastName = $row['lastName'];
        $this->Email = $row['UserEmail'];
        $this->Salt = $row['salt'];
        $this -> _password = $row['password'];
		$this -> currentUserID = $row['userID'];
		$this -> securityToken = $row['securityToken'];
		$this -> currentProfilePic = $row['userProfile'];
    }

    // USER ID is read-only, so it is private, and the function below is to get the value.
    public function GetUserId() {
        return $this->_userId;
    }

    
  	public $ConfirmPassword;
    public $Salt;
	
	
    public function FullName(){
        return $this->FirstName . ' ' . $this->LastName;
    }

    
	public $emailCheck;

    
    public function HasPermission($permissionName){
		$key = array_search($permissionName, array_column($this->_permissions, 'PermissionName'));
		
		if($key !== false) {
			return $this->_permissions[$key]['Enabled'];
		} else {
			return false;
		}
    }

    
    public function SetPermission($permissionName, $value) {
    	array_push($this->_newPermissions, array(
		    'PermissionName' => $permissionName,
			'Enabled' => $value
		));	
    }

    private function LoadPermissions()
    {
        $sth = $this -> db -> prepare('SELECT * FROM userpermissions WHERE relatedUserID = :userID');
        $sth->execute(array(':userID' => $this->_userId));
        $this->_permissions = $sth -> fetchAll();
    }

    public function Validate() {
        if(!isset($this->_userId)) {
            $emailCheck = $this->db->prepare('SELECT UserEmail FROM users WHERE UserEmail = ?');
            $emailCheck->execute(array($this->Email));

        }else{
            $emailCheck = $this->db->prepare('SELECT UserEmail FROM users WHERE UserEmail = :email and userID <> :userId');
            $emailCheck->execute(array(':userId' => $this->_userId, ':email' => $this->Email));
        }

			
		$validationErrors = array();
		

		if($this -> validate -> emptyInput($this -> FirstName)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> LastName)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		}
		
		
		if(!isset($this->_userId)) {
			if($this -> validate -> emptyInput($this -> _initialPassword)) {
				array_push($validationErrors, array("inputID" => 4,
													'errorMessage' => 'Required'));
			} else if($this -> validate -> passwordLength($this -> _initialPassword, 6)) {
				array_push($validationErrors, array("inputID" => 4,
													'errorMessage' => $this -> msg -> passwordLengthMessage("6")));
			}		
		}
		

		if($this -> validate -> emptyInput($this -> Email)) {
			array_push($validationErrors, array("inputID" => 5,
												'errorMessage' => 'Required'));
		} else if($this -> validate -> correctEmailFormat($this->Email)) {
			array_push($validationErrors, array("inputID" => 5,
												'errorMessage' => $this -> msg -> emailFormatRequiredMessage()));			
        } else if (count($emailCheck -> fetchAll())) {
            array_push($validationErrors, array("inputID" => 5,
												'errorMessage' => $this -> msg -> duplicateEmail()));
        }
		
		
		
		if($this -> IsProfileView == false) {
			if(count($this -> AssociatedStores) == 0) {
				array_push($validationErrors, array("inputID" => 6,
													'errorMessage' => "Required"));
			}	
		
			if (!in_array("EventsReadOnlyOrEdit", array_column($this->_newPermissions, 'PermissionName'))) {
				array_push($validationErrors, array("inputID" => 'eventPermission',
													'errorMessage' => "Please select permission for events"));
			}
			
			if (!in_array("InventoryViewing", array_column($this->_newPermissions, 'PermissionName'))) {
				array_push($validationErrors, array("inputID" => 'inventoryPermission',
													'errorMessage' => "Please select permission for inventory"));
			}
			
			if (!in_array("SocialMediaPermissionType", array_column($this->_newPermissions, 'PermissionName'))) {
				array_push($validationErrors, array("inputID" => 'socialMediaListPermission',
													'errorMessage' => "Please select permission for Social Media"));
			}
			
			if (in_array("SpecialsAddEditDelete", array_column($this->_newPermissions, 'PermissionName'))) {
				$selectedSpecialPermission = array("SpecialOEMCategoryVisible", "SpecialMotorclothesCategoryVisible", "SpecialPartsCategoryVisible", "SpecialServiceCategoryVisible", "SpecialStoreCategoryVisible");
				
				if (!array_intersect($selectedSpecialPermission, array_column($this->_newPermissions, 'PermissionName'))) {
						array_push($validationErrors, array("inputID" => 'subSpecialCategoryPermission',
															'errorMessage' => "Please select permission for special categories"));
						
				}	
			}
			
			if (!in_array("leadsViewing", array_column($this->_newPermissions, 'PermissionName'))) {
				array_push($validationErrors, array("inputID" => 'leadsViewingPermission',
													'errorMessage' => "Please select permission for Leads"));
			}
			
			
			
			if (in_array("SettingsViewEdit", array_column($this->_newPermissions, 'PermissionName'))) {
				$selectedSettingPermission = array("SettingsUsers", 
												   "SettingsDonations",
												   "SettingsSocialMedia", 
												   "SettingsSpecs", 
												   "SettingsInventoryCateories", 
												   "SettingsInventoryColors", 
												   "SettingsStoreSettings", 
												   "SettingsPhotoSettings", 
												   "SettingsConversionsManufacture", 
												   "SettingsConversionsColors", 
												   "SettingsSpecLabelMapping",
												   "SettingsShortUrls");
												   
				if (!array_intersect($selectedSettingPermission, array_column($this->_newPermissions, 'PermissionName'))) {
						array_push($validationErrors, array("inputID" => 'subSettingsPermission',
															'errorMessage' => "Please select permission for settings"));
						
					}									   
												   
				
			}
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}	

    }
    
    public function GetAssociatedStores() {
		$sth = $this -> db -> prepare('SELECT * FROM userassociatedstore LEFT JOIN stores ON userassociatedstore.associatedStore = stores.storeID WHERE UserID = :userID');
        $sth->execute(array(':userID' => $this->_userId));
        return $sth -> fetchAll();
	}

	public function GetAssociatedStore($id, $storeID) {
		$sth = $this -> db -> prepare('SELECT * FROM userassociatedstore WHERE UserID = :userID AND associatedStore = :associatedStore');
        $sth->execute(array(':userID' => $this->_userId, ':associatedStore' => $storeID));
        return $sth -> fetch();
	}

	private function ValidateAssociatedStores() {
		$ReturnValue = NULL;
		foreach ($this -> AssociatedStores as $key => $value) {
			if($value == '0') {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}

	private function UploadProfilePic($newName = NULL) {
		$newImageText = NULL;	
		$ImagePathParts = array();			
		
		if($newName != NULL) {
			$newImageText = $newName;
		} else {
			$newImageText = $this -> _userId;
		}
			
		Image::MoveAndRenameImage($this -> ProfileImageTempName, 
								  $ImagePathParts,
								  $this -> ProfileImageName,
								  $newImageText, 8);
		
		Image::resizeImage(PHOTO_PATH . 'Accounts' . '/' . $newImageText . '.' . Image::getFileExt($this -> ProfileImageName), 
						   PHOTO_PATH . 'Accounts' . '/' .$newImageText . '.' . Image::getFileExt($this -> ProfileImageName), 
						   250, 
						   250);						  
	}

    public function Save() {
    	try {
		
			$userSingleDB = array();
			$redirectID = NULL;
	        if(isset($this->_userId)) {
	        	
				if(!empty($this -> ProfileImageName)) {
					$this -> UploadProfilePic();
					$userSingleDB['userProfile'] = $this->_userId . '.' . Image::getFileExt($this -> ProfileImageName);
				}
				
				$userSingleDB['firstName'] = $this->FirstName;
				$userSingleDB['lastName'] = $this->LastName;
				$userSingleDB['UserEmail'] = $this->Email;
			
				if(!empty($this -> _initialPassword)) {
					$userSingleDB['password'] = $this -> _password;
					$userSingleDB['salt'] = $this -> Salt;
					
					if(LIVE_SITE == true) {
						$this -> SendUserCredentials();	
					}
				}
				
				
	        	
				
	            $this->db->update('users', $userSingleDB, array('userID' => $this->_userId));	
	        } else {
	            $this->_userId = $this->db->insert('users', array('firstName' => $this->FirstName,
														                'lastName' => $this->LastName,
														                'UserEmail' => $this->Email,
														                'password' => $this->_password,
														                'salt' => $this->Salt));
				
				if(LIVE_SITE == true) {
					$this -> SendUserCredentials();	
				}
	
	        }
			
			if($this -> IsProfileView == 1) {
				$this -> json -> outputJqueryJSONObject("redirect", PATH . "profile" );
			} else {
				$this->SavePermissions();
				$this -> json -> outputJqueryJSONObject("redirect", PATH . "settings/edit/user/" . $this->_userId );	
			}
			if($this -> IsProfileView == false) {
					
				$sth = $this -> db -> prepare("DELETE FROM userassociatedstore WHERE UserID = :UserID");
				$sth -> execute(array(':UserID' => $this->_userId));
				
				foreach ($this -> AssociatedStores as $key => $value) {
					$this->db->insert('userassociatedstore', array('UserID' => $this->_userId,
															                'associatedStore' => $value));
																			
				}
						
			}
			
			
	        
		} catch (Exception $e) {
			
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_GENERAL_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			if(isset($this->_userId)) {
				$this -> json -> outputJqueryJSONObject("MySqlError", $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} else {
				$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				$this -> json -> outputJqueryJSONObject("redirect", PATH . "cms/users");
			}
			
			
			
			//$this -> redirect -> redirectPage(PATH . 'cms/users');
		}

    }

	private function SendUserCredentials() {
		$userCredentials = array();
		$userCredentials['password'] = $this->_initialPassword;
					
		$this -> email -> to = $this->Email;	
		$this -> email -> subject = "Dillon Brothers Intranet | Credentials";			
		$this -> email -> Credentials($userCredentials);
	}

	public function validatePassword() {
		if($this -> validate -> emptyInput($this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject('errorMessage',$this -> msg -> isRequired("Password"));
			return false;
		} else if($this -> validate -> passwordLength($this->_initialPassword, 6)) {
			$this -> json -> outputJqueryJSONObject("errorMessage", $this -> msg -> passwordLengthMessage("6"));
			return false;
		} 
		return true;
	}
	
	public function validateEmail() {
			
		$getAdminCredentials = $this -> db -> prepare("SELECT * FROM users WHERE UserEmail = :Email");
		$getAdminCredentials -> execute(array(":Email" => $this -> emailCheck));
		$getCredentialObject = $getAdminCredentials -> fetch();
		
		$this->_userId = $getCredentialObject['userID'];
		
		if($this -> validate -> emptyInput($this -> emailCheck)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailRequiredRetriveCredentials());
			return false;
		} else if(!Email::verifyEmailAddress($this -> emailCheck)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
		} else if($this -> validate -> noEmailMatch($this -> emailCheck, $getCredentialObject['UserEmail'])) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailNotMatchingMessage());
			return false;
		} 
		return true;
		
	}
	
	public function SendEmail() {
		$securityToken = Hash::generateSecurityToken();
		
		if(LIVE_SITE == true) {
			$this -> email -> to = $this->emailCheck;	
			$this -> email -> subject = "Dillon Brothers Intranet | Reset your Password";			
			$this -> email -> passwordReset(PATH . "login/newpassword?token=" . $securityToken . "&userID=" . $this->_userId);
		}
			
		$this->db->update('users', array('securityToken' => $securityToken), array('userID' => $this->_userId));
		
		$this -> json -> multipleJSONOjbects(array("success" => "Success!, please check your email to reset your password",
                								   "redirect"	=> PATH . 'login'));
	}
	
	public function SaveNewPassword() {
	
		try {
			
			if(LIVE_SITE == true) {
				$this -> SendUserCredentials();	
			}
			
			$postData = array('password' => $this->_password, 'salt' => $this->Salt, 'securityToken' => '');
			$this->db->update('users', $postData, array('userID' => $this->_userId));
			$this -> json -> multipleJSONOjbects(array("success" => "Success!, You can now login with your new password",
                								       "redirect" => PATH . 'login'));
				
        } catch (Exception $e) {
			
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}		
			
	}

    private function SavePermissions() {
    	$sth = $this -> db -> prepare("DELETE FROM userpermissions WHERE relatedUserID = :relatedUserID");
		$sth -> execute(array(':relatedUserID' => $this->_userId));
		
		foreach($this->_newPermissions as $row) {
			$this->db->insert("userpermissions", array('PermissionName' => $row['PermissionName'],
                									   'relatedUserID' => $this->_userId,
													   'Enabled' => $row['Enabled']));	
			
        	
        }
    }

    public function ValidatePasswordAttempt($pass) {
        return  Hash::create('sha256', $pass, $this->Salt) == $this->_password;
    }

    private $_password;
    private $_initialPassword;
	
    public function SetPassword($password) {
        $this->Salt = Hash::generateSalt();
        $this->_initialPassword = $password;
        $this->_password = Hash::create('sha256', $password, $this->Salt);
    }

	public function Delete() {
		try {
				
			//dete permissions
			$permissionDelete = $this -> db -> prepare('DELETE FROM userpermissions WHERE relatedUserID = :relatedUserID');
			$permissionDelete -> execute(array(':relatedUserID' => $this->_userId));
		
			//delete user photo
			if(!empty($this -> currentProfilePic)) {
				unlink(PHOTO_PATH . 'Accounts/' . $this -> currentProfilePic);	
			}
			
			//delete user
			$sth = $this -> db -> prepare("DELETE FROM users WHERE userID = :id");
			$sth -> execute(array(':id' => $this->_userId));
						
			
			$this -> redirect -> redirectPage(PATH . 'settings/users');
		} catch (Exception $e) {
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
		
		}

	}

	public function userLogin() {
		$sth = $this->db->prepare("SELECT * FROM users WHERE UserEmail = :UserEmail");
		$sth->execute(array(':UserEmail' => $this -> Email));
		
		$data = $sth->fetch();							 
		$count =  $sth->rowCount();
		
		if ($count > 0) {
			
			$userLogin = User::WithID($data['userID']);
            if($userLogin -> ValidatePasswordAttempt($this -> password)) {
				//login	
				Session::init();
	            Session::set('UserLoggedIn', true);
	            Session::set('user', User::WithID($data['userID']));	
				//redirect to main app section
				
				
				if(isset($_SESSION['Redirect'])) {
					$this -> json -> outputJqueryJSONObject('redirect', PATH . $_SESSION['Redirect']);
					unset($_SESSION['Redirect']);
				} else {
					$this -> json -> outputJqueryJSONObject('redirect', PATH . "dashboard");	
				}
				
							
				
				
			} else {
				$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> wrongCredentialMatch());
			}
		} else {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> wrongCredentialMatch(). " No Username");			
		}
	}

}