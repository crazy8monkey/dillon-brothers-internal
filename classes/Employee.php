<?php

class Employee extends BaseObject {
	
	
	private $_id;
	
	public $photo;
	public $firstName;
	public $lastName;
	public $Title;
	public $email;
	public $Phone;
	public $Store;
	public $department;
	public $CurrentPhoto;
	
    public function __sleep() {
        parent::__sleep();
		 return array('_id');
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($employeeID) {
        $instance = new self();
        $instance->_id = $employeeID;
        $instance->loadById();
        return $instance;
    }
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM employees WHERE EmployeeID = :EmployeeID');
        $sth->execute(array(':EmployeeID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
    protected function fill(array $row){
    	$this -> firstName = $row['EmployeeFirstName'];
		$this -> lastName = $row['EmployeeLastName'];
 		$this -> Title = $row['Title'];
		$this -> email = $row['Email'];
		$this -> Phone = $row['Phone'];
		$this -> department = $row['Department'];
		$this -> CurrentPhoto = $row['Photo'];
    }	

	public function GetID() {
		return $this -> _id;
	}

	public function GetStores() {
		return $this -> db -> select("SELECT * FROM employeestorerelation WHERE EmployeeID = " . $this -> _id);
	}

	public function Validate() {
		
		if(isset($this -> _id)) {
		//	$emailCheck = $this->db->prepare('SELECT memberEmail FROM referralmembers WHERE memberEmail = :email AND referralMemberID <> :referralMemberID');
		//	$emailCheck->execute(array(':email' => $this -> email, ':referralMemberID' => $this -> _id));
			
		//	$userNameCheck = $this->db->prepare('SELECT userName FROM referralmembers WHERE userName = :userName AND referralMemberID <> :referralMemberID');
		//    $userNameCheck->execute(array(':userName' => $this -> userName, ':referralMemberID' => $this -> _id));	
				
				

		} else {
		//	$emailCheck = $this->db->prepare('SELECT memberEmail FROM referralmembers WHERE memberEmail = ?');
		//	$emailCheck->execute(array($this -> email));
			
		//	$userNameCheck = $this->db->prepare('SELECT userName FROM referralmembers WHERE userName = ?');
		//    $userNameCheck->execute(array($this -> userName));			
		}
		
		$validationErrors = array();
		

		//empty first name
		if($this -> validate -> emptyInput($this -> firstName)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> lastName)) {
			array_push($validationErrors, array("inputID" => 6,
												'errorMessage' => 'Required'));
		}
		
		//empty last name
		if($this -> validate -> emptyInput($this -> Title)) {
			array_push($validationErrors, array("inputID" => 2,
												"errorMessage" => "Required"));
		}
		
		if(!isset($this -> email)) {
			if($this -> validate -> correctEmailFormat($this -> email)) {
				array_push($validationErrors, array("inputID" => 3,
													"errorMessage" => "Incorrect Email Format"));					
			//duplicate emails										
			}// else if(count($emailCheck -> fetchAll())) {
			//	array_push($validationErrors, array("inputID" => 4,
			//										"inputType" => "Email",
			//										"errorMessage" => "Email associated with a different account"));
			//}
		} 
		
		if($this -> validate -> emptyInput($this -> Store)) {
			array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> department)) {
			array_push($validationErrors, array("inputID" => 5,
												'errorMessage' => 'Required'));
		}
		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
		
	}
	
	
	private function uploadProfilePic($storeIDS) {
		$ImagePathParts = array();
		$ImagePathParts['Employees'] = "employees";
		
		Image::MoveAndRenameImage($_FILES['employeePhoto']['tmp_name'], 
								  $ImagePathParts,
								  $this -> photo,
								  parent::seoUrl($storeIDS . '-' . $this -> firstName . '-' . $this -> lastName. '-'. $this -> department), 3);	
				
	}
	
	
	
	public function Save() {
		try {
			
			
			$photoThumbNail = NULL;
			$storeString = NULL;
			$storeIDS = NULL;
			$employeeIDSave = NULL;
			
			foreach($this -> Store as $storeID) {
				$storeIDS .= $storeID . ',';
			}
			$storeIDS = rtrim($storeIDS, ',');
			
			if(isset($this -> _id)) {
				$employeeIDSave = $this -> _id;
				
				$sth = $this -> db -> prepare("DELETE FROM employeestorerelation WHERE EmployeeID = :EmployeeID");
				$sth -> execute(array(':EmployeeID' => $employeeIDSave));
				
				if(!empty($this -> photo)) {
					$photoThumbNail = parent::seoUrl($storeIDS . '-' . $this -> firstName . '-' . $this -> lastName. '-'. $this -> department) . '.' .Image::getFileExt($this -> photo);
					$this -> uploadProfilePic($storeIDS);
					
					$postData = array('EmployeeFirstName' => $this -> firstName,
									  'EmployeeLastName' => $this -> lastName, 
									  'Photo' => $photoThumbNail,
									  'Title' => $this -> Title,
									  "Email" => $this -> email,
									  'Phone' => preg_replace("/[^0-9]/", "", $this -> Phone),
									  "Department" => $this -> department);
					
				} else {
					$postData = array('EmployeeFirstName' => $this -> firstName,
									  'EmployeeLastName' => $this -> lastName,
									  'Title' => $this -> Title,
									  "Email" => $this -> email,
									  'Phone' => preg_replace("/[^0-9]/", "", $this -> Phone),
									  "Department" => $this -> department);
				}
				
				
				
				
					
				
				$this->db->update('employees', $postData, array('EmployeeID' => $this -> _id));
				
				
			} else {
				if(empty($this -> photo)) {
					$photoThumbNail = "no-picture.png";
				} else {
					$photoThumbNail = parent::seoUrl($storeIDS . '-' . $this -> firstName . '-' . $this -> lastName. '-'. $this -> department) . '.' .Image::getFileExt($this -> photo);
					$this -> uploadProfilePic($storeIDS);
				}
				
				
				$employeeID = $this -> db -> insert('employees', array('EmployeeFirstName' => $this -> firstName,
														 'EmployeeLastName' => $this -> lastName, 
														  'Title' => $this -> Title,
														  "Email" => $this -> email,
														  'Phone' => preg_replace("/[^0-9]/", "", $this -> Phone),
														  'Photo' => $photoThumbNail,
														  "Department" => $this -> department));
				$employeeIDSave	= $employeeID;									  
			}
			
			//update employee related store
			foreach($this -> Store as $storeID) {
				$this -> db -> insert('employeestorerelation', array('EmployeeID' => $employeeIDSave,
																	 'RelatedStoreID' => $storeID));
			}
			
			
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'employees');
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Employee Save Error: " . $e->getMessage();
			$TrackError -> type = "EMPLOYEE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	
	public function delete() {
		unlink(PHOTO_PATH . 'employees/' . $this -> CurrentPhoto);	
			
		$sth = $this -> db -> prepare("DELETE FROM employees WHERE EmployeeID = :EmployeeID");
		$sth -> execute(array('EmployeeID' => $this -> _id));	
		
		$permissionDelete = $this -> db -> prepare("DELETE FROM employeestorerelation WHERE EmployeeID = :EmployeeID");
		$permissionDelete -> execute(array(':EmployeeID' => $this -> _id));
		
		$this -> redirect -> redirectPage(PATH. 'employees');
	}



}