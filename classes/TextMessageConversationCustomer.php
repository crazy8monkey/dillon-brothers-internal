<?php

class TextMessageConversationCustomer extends BaseObject {
		
	private $_id;	
	private $_phoneNumber;
	
	public $phoneNumber;
	public $NewMessage;
	public $firstName;
	public $lastname;
	
	public $StoreDept;
	
	public $StoreDeptNumber;
	
	public $TextMessageID;
	
	public $CustomerName;
	public $recieveResponseMessage = false;
	
	
	public $CurrentMessages = array();
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($employeeID) {
        $instance = new self();
        $instance->_id = $employeeID;
        $instance->loadById();
        return $instance;
    }
	
	public static function WithNumber($phoneNumber) {
		$instance = new self();
        $instance->_phoneNumber = $phoneNumber;
        $instance->loadByPhone();
        return $instance;
	}
	
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM textmessageconversationcustomers WHERE textmessageconversationID = :textmessageconversationID');
        $sth->execute(array(':textmessageconversationID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
		$this -> _textMessages();
    }
	
	protected function loadByPhone() {
		$sth = $this -> db -> prepare('SELECT * FROM textmessageconversationcustomers WHERE phonenumber = :phonenumber');
        $sth->execute(array(':phonenumber' => substr($this->_phoneNumber, 2)));	
        $record = $sth -> fetch();
        $this->fill($record);
	}
	
    protected function fill(array $row){
    	$this -> TextMessageID = $row['textmessageconversationID'];
    	$this -> CustomerName = $row['CustomerName'];
    	$this -> phoneNumber = $row['phonenumber'];
    }	

	public function GetID() {
		return $this -> _id;
	}

	private function _textMessages() {
		$textMessages = new OutboundTextsList();	
		$this -> CurrentMessages = $textMessages -> ByCustomer($this->_id);
	}

	public function Validate() {
		
		$validationErrors = array();
		
		if(!isset($this -> _id)) {
			
			$phoneNumberCheck = $this->db->prepare('SELECT phonenumber FROM textmessageconversationcustomers WHERE phonenumber = ?');
            $phoneNumberCheck -> execute(array(preg_replace("/[^0-9]/", "", $this -> phoneNumber)));
			
			
			//empty first name
			if($this -> validate -> emptyInput($this -> firstName)) {
				array_push($validationErrors, array("inputID" => 1,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> lastname)) {
				array_push($validationErrors, array("inputID" => 2,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> phoneNumber)) {
				array_push($validationErrors, array("inputID" => 3,
													'errorMessage' => 'Required'));
			} else if($phoneNumberCheck -> fetchAll()) {
				array_push($validationErrors, array("inputID" => 3,
													'errorMessage' => 'There is already a phone number associated with a different customer, please enter a new number'));
			}	
			
			
			
		} else {
			
			if(count($this -> CurrentMessages) > 0) {
				if($this -> validate -> emptyInput($this -> NewMessage)) {
					array_push($validationErrors, array("inputID" => 4,
														'errorMessage' => 'Required'));
				}	
			}
			
		}
		
		
		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
		
	}
	
	
	public function SaveCustomerResponse() {
		try {
			$outboundMesage = new OutBoundTextMessage();
			$outboundMesage -> textMessage = $this -> NewMessage;
			$outboundMesage -> RelatedPhoneNumber = $this -> phoneNumber;
			$outboundMesage -> RelatedCustomerID = $this -> TextMessageID;	
			$outboundMesage -> catchResponse = true;
			$outboundMesage -> Save();	
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Customer Save Message Error: " . $e->getMessage();
			$TrackError -> type = "EMPLOYEE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	private function generateTextDisclaimer(array $replace, $subject) { 
	   return str_replace(array_keys($replace), array_values($replace), $subject);    
	} 
	
	
	public function SaveCustomer() {
		if(isset($this -> _id)) {
			$redirectID = $this -> _id;
		} else {
			$newCustomer = $this -> db -> insert('textmessageconversationcustomers', array('CustomerName' => $this -> firstName . ' ' . $this -> lastname,
																						   'phonenumber' => preg_replace("/[^0-9]/", "", $this -> phoneNumber)));
			$redirectID = $newCustomer;																			   
		}
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'communication/view/customer/' . $redirectID);
	}
	
	public function SendMessage() {
		try {
			
			$outboundMesage = new OutBoundTextMessage();
			
			$storeDeptSelect = explode('-', $this -> StoreDept);	
			
			if(count($this -> CurrentMessages) == 0) {
				Session::init();	
				$settings = SettingsObject::init();	
				
				$replaceStrings = array(
					"[EmployeeName]" => $_SESSION['user'] -> FirstName . ' ' . $_SESSION['user'] -> LastName,
					"[StoreName]" => $storeDeptSelect[0],
					"[DepartmentName]" => $storeDeptSelect[1]
				);
				
				$outboundMesage -> textMessage = $this ->generateTextDisclaimer($replaceStrings, $settings -> SMSDisclosure);
				
			} else {
				$outboundMesage -> textMessage = $this -> NewMessage;				
			}
			
			$outboundMesage -> RelatedCustomerID = $this->_id;
			$outboundMesage -> DepartmentNumber = $storeDeptSelect[2];
			$outboundMesage -> RelatedPhoneNumber = $this -> phoneNumber;			
			
			$outboundMesage -> Save();
			
															
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'communication/view/customer/' . $this->_id);
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Employee Save Error: " . $e->getMessage();
			$TrackError -> type = "EMPLOYEE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	



}