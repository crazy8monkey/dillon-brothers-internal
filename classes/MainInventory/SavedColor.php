<?php


class SavedColor extends BaseObject {
	
	public $MainUnitInventoryID;
	public $RelatedMainUnitColorID;
	public $CurrentInventoryColorCheck;
	public $KeyPhrase;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public function Save() {
		try {
			
			$savedColor = $this -> db -> prepare("INSERT IGNORE INTO muirelatedcolors(relatedMUInventoryID, relatedMUIColorID) VALUES(:relatedMUInventoryID, :relatedMUIColorID)");
			$savedColor -> execute(array(":relatedMUInventoryID" => $this -> MainUnitInventoryID, ":relatedMUIColorID" => $this -> RelatedMainUnitColorID));
			
			
			
			
			//$colorConverted = new ColorConversionsList();
			//$colors = $this -> multiexplode(array('/', '.'), $this -> CurrentInventoryColorCheck);
			//$FinalColors = array();
			//foreach($colors as $colorSingle) {
			//	$key = array_search($colorSingle, array_column($colorConverted -> ConvertedColors(), 'keyPhrase'));	
			//	if($key !== false) {
					
			//	}
			//}
			//$saveColor -> CurrentInventoryColorCheck
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Edit History Log Save Error: " . $e->getMessage();
			$TrackError -> type = "EDIT HISTORY LOG SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	
	private	function multiexplode ($delimiters,$string) {

	    $ready = str_replace($delimiters, $delimiters[0], $string);
	    $launch = explode($delimiters[0], $ready);
	    return  $launch;
	}


}