<?php


class GenericColorObject extends BaseObject {
	
	private $_id;
	public $NewColorText;
	public $ColorText;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($genericColorID) {
        $instance = new self();
        $instance->_id = $genericColorID;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM genericmuicolors WHERE genericColorID = :genericColorID');
        $sth->execute(array(':genericColorID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> ColorText = $row['GenericColorText'];
    }
	
	public function GetID() {
		return $this->_id;
	}

	public function Validate() {
		if(!isset($this->_id)) {
            $colorCheck = $this->db->prepare('SELECT GenericColorText FROM genericmuicolors WHERE GenericColorText = ?');
            $colorCheck->execute(array($this->NewColorText));
        } else {
            $colorCheck = $this->db->prepare('SELECT * FROM genericmuicolors WHERE GenericColorText = :Text AND genericColorID <> :genericColorID');
            $colorCheck->execute(array(':genericColorID' => $this->_id, 
            						   ':Text' => $this -> NewColorText));
        }
		
		
		$validationErrors = array();
		

		//empty first name
		if($this -> validate -> emptyInput($this -> NewColorText)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		} else if(count($colorCheck -> fetchAll())) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'There is already that color entered into the database, please type in a different color'));
		}
				
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Save() {
		try {

			
			if(isset($this -> _id)) {
				$colorList = new ColorsList();
				
				foreach($colorList -> ByFilteredColor($this -> ColorText) as $colorSingle) {
					$color = ColorObject::WithID($colorSingle['colorID']);
					$color -> FilterText = $this -> NewColorText;	
					$color -> UpdateFilterText();
				}
				
				$postData = array('GenericColorText' => $this -> NewColorText);	
									
				$this->db->update('genericmuicolors', $postData, array('genericColorID' => $this -> _id));
				
				
				
			} else {
				$this -> db -> insert('genericmuicolors', array('GenericColorText' => $this -> NewColorText));
			}
			
			$this -> json -> outputJqueryJSONObject("redirect", PATH. 'settings/inventorycolors');
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Event Save Error: " . $e->getMessage();
			$TrackError -> type = "EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	
	public function Delete() {
		$colorList = new ColorsList();
		
		
		foreach($colorList -> ByFilteredColor($this -> ColorText) as $colorSingle) {
			$color = ColorObject::WithID($colorSingle['colorID']);
			$color -> FilterText = '';	
			$color -> UpdateFilterText();
		}
		
		
		//delete color	
		$colorDelete = $this -> db -> prepare('DELETE FROM genericmuicolors WHERE genericColorID = :genericColorID');
		$colorDelete -> execute(array(":genericColorID" => $this->_id));
		
		
		$this -> redirect -> redirectPage(PATH . 'settings/inventorycolors');
	}
	

		

}