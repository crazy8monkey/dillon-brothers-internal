<?php


class ColorObject extends BaseObject {
	
	private $_id;
	public $ColorText;
	public $FilterText;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($colorID) {
        $instance = new self();
        $instance->_id = $colorID;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM colors WHERE colorID = :colorID');
        $sth->execute(array(':colorID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> ColorText = $row['ColorText'];
		$this -> FilterText = $row['FilterColor'];
    }
	
	public function GetID() {
		return $this->_id;
	}

	public function Validate() {
		if(!isset($this->_id)) {
            $colorCheck = $this->db->prepare('SELECT ColorText FROM colors WHERE ColorText = ?');
            $colorCheck->execute(array($this->ColorText));
        } else {
            $colorCheck = $this->db->prepare('SELECT * FROM colors WHERE ColorText = :Text AND colorID <> :colorID');
            $colorCheck->execute(array(':colorID' => $this->_id, 
            						   ':Text' => $this -> ColorText));
        }
		
		
		$validationErrors = array();
		

		//empty first name
		if($this -> validate -> emptyInput($this -> ColorText)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		} else if(count($colorCheck -> fetchAll())) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'There is already that color entered into the database, please type in a different color'));
		}
		
		if($this -> validate -> emptyInput($this -> FilterText)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		} 
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Save() {
		try {

			
			if(isset($this -> _id)) {
				$redirectID = $this -> _id;
				$postData = array('ColorText' => $this -> ColorText,
								  'FilterColor' => $this -> FilterText);	
				
									
				$this->db->update('colors', $postData, array('colorID' => $this -> _id));
				
				
				
			} else {
				$NewColor = $this -> db -> insert('colors', array('ColorText' => $this -> ColorText, 'FilterColor' => $this -> FilterText));
				$redirectID = $NewColor;
			}
			
			$this -> json -> outputJqueryJSONObject("redirect", PATH. 'settings/edit/color/'. $redirectID);
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Event Save Error: " . $e->getMessage();
			$TrackError -> type = "EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	
	public function UpdateFilterText() {
		$postData = array('FilterColor' => $this -> FilterText);	
											
		$this->db->update('colors', $postData, array('colorID' => $this -> _id));
	}
	
	
	public function Delete() {
		//delete color	
		$colorDelete = $this -> db -> prepare('DELETE FROM colors WHERE colorID = :colorID');
		$colorDelete -> execute(array(":colorID" => $this->_id));
			
		//delete color conversions
		$conversionsDelete = $this -> db -> prepare('DELETE FROM colorconversions WHERE relatedColorID = :relatedColorID');
		$conversionsDelete -> execute(array(":relatedColorID" => $this -> _id));
		
		//delete main inventory colors
		$mainInventoryColorDelete = $this -> db -> prepare('DELETE FROM muirelatedcolors WHERE relatedMUIColorID = :relatedMUIColorID');
		$mainInventoryColorDelete -> execute(array(":relatedMUIColorID" => $this -> _id));
		
		$this -> redirect -> redirectPage(PATH . 'settings/inventorycolors');
	}
	

		

}