<?php


class EditHistoryLog extends BaseObject {
	
	public $itemID;
	public $userID;
	//1 = blog
	//2 = specials
	//3 = events
	//4 = backup inventory
	//5 = ecommerce product
	//6 = ecommerce order
	public $itemType;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	
	
	public function Save() {
		try {
			
			$this -> db -> insert('edithistorylog', array('relatedItemID' => $this -> itemID,
														  'edittedByUserID' => $this -> userID,
														  'edittedTimeAndDate' => parent::TimeStamp(),
														  'editHistoryType' => $this -> itemType));								
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Edit History Log Save Error: " . $e->getMessage();
			$TrackError -> type = "EDIT HISTORY LOG SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}


}