<?php

class FeedBackObject extends BaseObject {
	
	
	private $_id;
	
	public $FeedBackText;
	public $itemID;
	//1 = current inventory item
	public $itemType;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($employeeID) {
        $instance = new self();
        $instance->_id = $employeeID;
        $instance->loadById();
        return $instance;
    }
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM employees WHERE EmployeeID = :EmployeeID');
        $sth->execute(array(':EmployeeID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
    protected function fill(array $row){
    	
    }	

	public function GetID() {
		return $this -> _id;
	}

	public function Validate() {
		
		$validationErrors = array();
		

		//empty first name
		if($this -> validate -> emptyInput($this -> FeedBackText)) {
			array_push($validationErrors, array("inputID" => "FeedBackID1",
												'errorMessage' => 'Required'));
		}
		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
		
	}
	
	
	
	
	public function Save() {
		try {
			
			Session::init();

			$this -> db -> insert('internalfeedback', array('FeedbackText' => $this -> FeedBackText,
														 	'feedbackBy' => $_SESSION['user'] -> _userId, 
															'associatedFeedbackID' => $this -> itemID,
															'itemType' => $this -> itemType,
															"feedBackSubmitted" => date("Y-m-d", $this -> time -> NebraskaTime()) . 'T' . date("H:i:s", $this -> time -> NebraskaTime())));
															
			$submittedByText = $_SESSION['user'] -> FirstName . ' ' . $_SESSION['user'] -> LastName . ' | ' . $this -> time -> formatDate(date("Y-m-d", $this -> time -> NebraskaTime())) . ' - ' . $this -> time -> formatTime(date("H:i:s", $this -> time -> NebraskaTime()));		
			
			$this -> json -> outputJqueryJSONObject('newFeedBack', array("Text" => $this -> FeedBackText,
																		 "SubmittedByDetails" => $submittedByText));
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Employee Save Error: " . $e->getMessage();
			$TrackError -> type = "EMPLOYEE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	



}