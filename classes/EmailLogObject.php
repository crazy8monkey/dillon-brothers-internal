<?php


class EmailLogObject extends BaseObject {
	
	private $_id;
	public $VehicleInfo;
	public $LeadStoreID;
	public $SourceofLead;
	//1 = Send To Friend
	//2 = Online Offer
	//3 = Trade in Value
	//4 = Schedule Test Ride
	//5 = Contact Us
	//6 = Service Department
	public $TypeOfLead;
	public $ContentOfLead;
	public $EnteredTime;
	public $isActive;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($emailID) {
        $instance = new self();
        $instance->_id = $emailID;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM emaillogs WHERE emailLogID = :emailLogID');
        $sth->execute(array(':emailLogID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	

    protected function fill(array $row){
	    $this -> VehicleInfo = $row['VehiclelInfo'];
		$this -> LeadStoreID = $row['LeadStoreID'];
		$this -> SourceofLead = $row['SourceOfLead'];
		$this -> TypeOfLead = $row['TypeOfLead'];
		$this -> ContentOfLead = $row['ContentOfLead'];
		$this -> EnteredTime = $row['EnteredTime'];
		$this -> isActive = $row['isActive'];
    }

	
	public function GetEmailID() {
		return $this -> _id;
	}
	
	public function GenerateEmailTypeHeader() {
		$text = '';	
		switch($this -> TypeOfLead) {
			case 1;
				$text = "Inventory Lead - Send To a Friend";
				break;
			case 2:
				$text = "Inventory Lead - Online Offer";
				break;
			case 3:
				$text = "Inventory Lead - Trade In Value";
				break;
			case 4:
				$text = "Inventory Lead - Schedule Test Ride";
				break;
			case 5:
				$text = "Inventory Lead - Contact Us";
				break;
			case 6:
				$text = "Service Department Request";
				break;			
			case 7:
				$text = "Parts Department Request";
				break;			
			case 8:
				$text = "Donation Request";
				break;				
			case 9:
				$text = "Apparel Department Request";
				break;					
		}
		return $text;
	}
	
	public function GenerateEmailImage() {
		$image = '';	
		switch($this -> TypeOfLead) {
			case 1:
				$image = "<img src='". PATH . "public/images/SendToFriend.png' />";
				break;
			case 2:
				$image = "<img src='". PATH . "public/images/OnlineOffer.png' />";
				break;
			case 3:
				$image = "<img src='". PATH . "public/images/TradeInValue.png' />";
				break;
			case 4:
				$image = "<img src='". PATH . "public/images/ScheduleTestRide.png' />";
				break;
			case 5:
				$image = "<img src='". PATH . "public/images/InventoryContactUs.png' />";
				break;
			case 6:
				$image = "<img src='". PATH . "public/images/ServiceEmail.png' />";
				break;			
			case 7:
				$image = "<img src='". PATH . "public/images/PartsEmail.png' />";
				break;			
			case 8:
				$image = "<img src='". PATH . "public/images/DonationRequest.png' />";
				break;			
			case 9:
				$image = "<img src='". PATH . "public/images/ApparelEmail.png' />";
				break;				
		}
		return $image;
	}
	
	public function GenerateSource() {
		$text = '';
		switch($this -> SourceofLead) {
			case 1;
				$text = "dillon-brothers.com";
				break;
		}
		return $text;
	}
	
	public function GenerateStoreName() {
		$storeNameText = '';	
		switch($this -> LeadStoreID) {
			case 2:
				$storeNameText = "Dillon Brothers Motorsports";
				break;
			case 3:
				$storeNameText = "Dillon Brothers Harley-Davidson";
				break;
			case 4:
				$storeNameText = "Dillon Brothers Indian Motorcycle";
				break;
		}
		return $storeNameText;
	}
	
	
	public function GenerateTime() {
		$timeEntered = explode('T', $this -> EnteredTime);
		return $this -> time -> formatDate($timeEntered[0]) . ' / ' . $this -> time -> formatTime($timeEntered[1]);
	}
	
	public function Delete() {
		$postData = array('isActive' => 0);
		$this->db->update('emaillogs', $postData, array('emailLogID' => $this -> _id));
		$this -> redirect -> redirectPage(PATH. 'emaillogs');
	}
	
	

		

}