<?php


class ShortLinks extends BaseObject {
	
	public $_shortUrlID;
	private $_shortLinkSEOName;
	private $_blogID;
	private $_eventID;
	private $_ShortLinkID;
	private $_replaceShortLink = false;
	public $UpdateShortLinkInfo = false;
	
	public $BlogID;
	public $EventID;
	public $ShortLinkValue;
	public $NewShortLink;
	public $ShortCodeLinkActive;
	public $OriginatedSource;
	
	public $CurrentShortCode;
	public $CurrentYear;
	
	public $SettingIsActive;
	public $SettingShortURL;
	public $SettingRedirectAction;
	public $RedirectIfInactive;
	public $SettingsRedirectedContent;
	
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithBlogID($blogID) {
		$instance = new self();
        $instance -> _blogID = $blogID;
        $instance -> loadRowByBlogID();
        return $instance;
	}
	
	public static function WithEventID($blogID) {
		$instance = new self();
        $instance -> _eventID = $blogID;
        $instance -> loadRowByEventID();
        return $instance;
	}
	
	
	public static function WithSEOName($name) {
        $instance = new self();
        $instance -> _shortLinkSEOName = $name;
        $instance -> loadRow();
        return $instance;
    }
	
	
	public static function WithID($shortURLID) {
		$instance = new self();
        $instance -> _shortUrlID = $shortURLID;
        $instance -> loadByID();
        return $instance;
	}
	
		
	protected function loadRow() {
		$sth = $this -> db -> prepare('SELECT * FROM shortlinks WHERE shortUrl = :shortUrl');
        $sth->execute(array(':shortUrl' => $this-> _shortLinkSEOName));	
		
		if($sth->rowCount() > 0) {
			$record = $sth->fetch();
        	$this->fill($record);
			$this -> _updateShortLinkInfo = true;
		} 
    }
	
	public function loadRowByBlogID() {
		$sth = $this -> db -> prepare('SELECT * FROM shortlinks WHERE relatedBlogID = :relatedBlogID');
        $sth->execute(array(':relatedBlogID' => $this-> _blogID));	
		
		//if exist replace current short url
		if($sth->rowCount() > 0) {
			$record = $sth->fetch();
        	$this->fill($record);
			$this -> _updateShortLinkInfo = true;
		}
        
	}
	
	public function loadRowByEventID() {
		$sth = $this -> db -> prepare('SELECT * FROM shortlinks WHERE relatedEventID = :relatedEventID');
        $sth->execute(array(':relatedEventID' => $this-> _eventID));	
		
		//if exist replace current short url
		if($sth->rowCount() > 0) {
			$record = $sth->fetch();
        	$this->fill($record);
			$this -> _replaceShortLink = true;
		}
        
	}

	public function loadByID() {
		$sth = $this -> db -> prepare('SELECT * FROM shortlinks WHERE shortLinkID = :shortLinkID');
        $sth->execute(array(':shortLinkID' => $this-> _shortUrlID));	
		$record = $sth->fetch();
        $this->fill($record);
	}
	

    protected function fill(array $row){
    	$this -> _ShortLinkID = $row['shortLinkID'];
    	$this -> BlogID = $row['relatedBlogID'];
		$this -> EventID = $row['relatedEventID'];
		$this -> ShortCodeLinkActive = $row['shortLinkActive'];
		$this -> OriginatedSource = $row['OriginatedSource'];
    }
	
	
	public function SaveBlogShortLink() {
		$shortLinkBlogDB = array();
		
		//if($this -> _replaceShortLink == true) {
		//	$this -> DeleteBlogShortUrl();
		//}
		
		$shortLinkBlogDB['shortUrl'] = $this -> NewShortLink;
		$shortLinkBlogDB['relatedEventID'] = 0;
		$shortLinkBlogDB['relatedBlogID'] = $this -> _blogID;
		$shortLinkBlogDB['shortLinkActive'] = $this -> ShortCodeLinkActive;			
		$shortLinkBlogDB['OriginatedSource'] = $this -> OriginatedSource;			
							
		$this -> db -> insert('shortlinks', $shortLinkBlogDB);	
		
		
		
	}

	public function SaveEventShortLink() {
		$currentShortCode = $this -> db -> prepare('SELECT * FROM shortlinks WHERE shortUrl = :shortUrl');
		$currentShortCode -> execute(array(':shortUrl' => $this -> CurrentShortCode));
		
		$getCurrentShortUrl = $currentShortCode -> fetch();
		if($getCurrentShortUrl) {
			$updateShortUrlDB = array();
			
			$updateShortUrlDB['shortUrl'] = $this -> NewShortLink;
			
			$updateShortUrlDB['relatedBlogID'] = 0;
			$updateShortUrlDB['relatedEventID'] = $this -> EventID;	
								
			$updateShortUrlDB['CurrentYear'] = $this -> CurrentYear;	
								
			$updateShortUrlDB['OriginatedSource'] = $this -> NewShortLink;
					
			$this->db->update('shortlinks', $updateShortUrlDB, array('shortLinkID' => $getCurrentShortUrl['shortLinkID']));
		} else {
			$shortUrlDBInsert = array();
			$shortUrlDBInsert['shortUrl'] = $this -> NewShortLink;
					
			$shortUrlDBInsert['relatedBlogID'] = 0;
			$shortUrlDBInsert['relatedEventID'] = $this -> EventID;	
			$shortUrlDBInsert['Action'] = 'CurrentContent';
			
			$shortUrlDBInsert['CurrentYear'] = $this -> CurrentYear;		
			$shortUrlDBInsert['shortLinkActive'] = 1;
			$shortUrlDBInsert['OriginatedSource'] = $this -> NewShortLink;
					
			$this -> db -> insert('shortlinks', $shortUrlDBInsert);	
		}
		
	}

	public function SaveSingleShortLink() {
		$removeOld = $this -> db -> prepare('DELETE FROM shortlinks WHERE shortUrl LIKE :shortUrl');
		$removeOld -> execute(array(":shortUrl" => '%' . $this -> NewShortLink . '%'));
		
		
		$shortUrlDBInsert = array();
		$shortUrlDBInsert['shortUrl'] = $this -> NewShortLink;
		$shortUrlDBInsert['CurrentYear'] = $this -> CurrentYear;
			
		$shortUrlDBInsert['relatedBlogID'] = 0;
		$shortUrlDBInsert['relatedEventID'] = $this -> EventID;	
			
		$shortUrlDBInsert['shortLinkActive'] = 1;
		$shortUrlDBInsert['OriginatedSource'] = $this -> OriginatedSource;
						
		$this -> db -> insert('shortlinks', $shortUrlDBInsert);	
	}
	
	public function DeleteShortUrl($value, $year) {
		$removeOld = $this -> db -> prepare('DELETE FROM shortlinks WHERE shortUrl = :shortUrl AND CurrentYear <> :CurrentYear');
		$removeOld -> execute(array(":shortUrl" => $value,
									":CurrentYear" => $year));
	}

	public function DeleteEventShortUrl($value) {
		$removeOld = $this -> db -> prepare('DELETE FROM shortlinks WHERE relatedEventID = :relatedEventID');
		$removeOld -> execute(array(":relatedEventID" => $value));
	}

	public function UpdateBlogShortLink() {
		if(!empty($this -> ShortLinkValue)) {
			$currentShortCodeCheck = $this -> db -> prepare('SELECT * FROM shortlinks WHERE shortUrl = :shortUrl');
			$currentShortCodeCheck -> execute(array(":shortUrl" => $this -> ShortLinkValue));	
			$getCurrentShortUrl = $currentShortCodeCheck -> fetch();
			
			if($getCurrentShortUrl) {
				
				$updateShortUrlDB = array();	
			
				$updateShortUrlDB['relatedBlogID'] = $this-> BlogID;
				$updateShortUrlDB['relatedEventID'] = 0;	
					
				$updateShortUrlDB['shortLinkActive'] = $this -> ShortCodeLinkActive;
				
				if($this -> ShortCodeLinkActive == 0) {
					$updateShortUrlDB['Action'] = '301Redirect';
				} else {
					$updateShortUrlDB['Action'] = 'CurrentContent';
				}
				
				$updateShortUrlDB['OriginatedSource'] = $this -> OriginatedSource;
					
				$this->db->update('shortlinks', $updateShortUrlDB, array('shortLinkID' => $getCurrentShortUrl['shortLinkID']));
			} else {
				$shortUrlDBInsert = array();
				$shortUrlDBInsert['shortUrl'] = $this -> ShortLinkValue;
					
				$shortUrlDBInsert['relatedBlogID'] = $this -> BlogID;
				$shortUrlDBInsert['relatedEventID'] = 0;	
				$shortUrlDBInsert['Action'] = 'CurrentContent';
					
				$shortUrlDBInsert['shortLinkActive'] = $this -> ShortCodeLinkActive;
				$shortUrlDBInsert['OriginatedSource'] = $this -> OriginatedSource;
					
								
				$this -> db -> insert('shortlinks', $shortUrlDBInsert);			
			}
			
			
		}
		
		//if($this -> _updateShortLinkInfo == true) {

		//} else {
		
			
		//}
	}
	
	public function RemoveOldShortLink() {
		$remove = $this -> db -> prepare('DELETE FROM shortlinks WHERE shortlinks = :shortlinks');
		$remove -> execute(array(":shortlinks" => $this -> _eventID));
	}

	public function UpdateShortLink() {
		
		
		if($this -> _updateShortLinkInfo == true) {
			$updateShortUrlDB = array();	
			
			if(isset($this-> _blogID)) {
				$updateShortUrlDB['relatedBlogID'] = $this-> _blogID;
				$updateShortUrlDB['relatedEventID'] = 0;	
			}
			
			//if(isset($this -> EventID)) {
			//	$updateShortUrlDB['relatedBlogID'] = 0;
		//		$updateShortUrlDB['relatedEventID'] = $this -> EventID;	
		//	}
			
			$updateShortUrlDB['shortLinkActive'] = $this -> ShortCodeLinkActive;
			$updateShortUrlDB['OriginatedSource'] = $this -> OriginatedSource;
			
			$this->db->update('shortlinks', $updateShortUrlDB, array('shortLinkID' => $this -> _ShortLinkID));
		} else {
			$shortUrlDBInsert = array();
			$shortUrlDBInsert['shortUrl'] = $this -> NewShortLink;
			
			if(isset($this -> _blogID)) {
				$shortUrlDBInsert['relatedBlogID'] = $this -> _blogID;
				$shortUrlDBInsert['relatedEventID'] = 0;	
			}
			
			if(isset($this -> EventID)) {
				$shortUrlDBInsert['relatedBlogID'] = 0;
				$shortUrlDBInsert['relatedEventID'] = $this -> EventID;	
			}
			
			$shortUrlDBInsert['shortLinkActive'] = $this -> ShortCodeLinkActive;
			$shortUrlDBInsert['OriginatedSource'] = $this -> OriginatedSource;
			
			
								
			$this -> db -> insert('shortlinks', $shortUrlDBInsert);	
			
		}
		
	}

	public function ValidateSettingsSide() {
		$validationErrors = array();

		if(isset($this -> _shortUrlID)) {
			$shortUrlCheck = $this->db->prepare('SELECT shortUrl FROM shortlinks WHERE shortUrl = :shortUrl AND shortLinkID <> :shortLinkID');
            $shortUrlCheck->execute(array(":shortUrl" => $this -> SettingShortURL, ":shortLinkID" => $this -> _shortUrlID));	
		} else {
			$shortUrlCheck = $this->db->prepare('SELECT shortUrl FROM shortlinks WHERE shortUrl = ?');
            $shortUrlCheck->execute(array($this -> SettingShortURL));
		}

		if($this -> validate -> emptyInput($this -> SettingShortURL)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Short URL text is Required'));
		} else if($shortUrlCheck -> fetchAll()) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => "There is a short url associated with a different item, please type in a different short url"));
		}
		
		if($this -> SettingRedirectAction == '') {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => "Please Select 301/410/Active Content option in 'Redirect Action' option(s)"));
		}
		
		if($this -> SettingsRedirectedContent == '0') {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => "Please Select Redirected Content"));
		}
		
		
				
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function SettingsSaveShortURL() {
		$redirectedContent = explode('--', $this -> SettingsRedirectedContent);	
			
		if(isset($this -> _shortUrlID)) {
			$updateDBData = array();
			$updateDBData['shortUrl'] = $this -> SettingShortURL;
			$updateDBData['Action'] = $this -> SettingRedirectAction;
			switch($redirectedContent[0]) {
				case 1:
					$updateDBData['relatedEventID'] = $redirectedContent[2];
					$updateDBData['relatedBlogID'] = 0;
					$updateDBData['isSubDomain'] = 0;
					break;
				case 2:
					$updateDBData['relatedEventID'] = 0;
					$updateDBData['relatedBlogID'] = $redirectedContent[2];
					$updateDBData['isSubDomain'] = 0;
					break;
				case 3:
					$updateDBData['relatedEventID'] = 0;
					$updateDBData['relatedBlogID'] = 0;
					$updateDBData['isSubDomain'] = 0;
					break;		
				case 4:
					$insertDBData['relatedEventID'] = 0;
					$insertDBData['relatedBlogID'] = 0;
					$updateDBData['isSubDomain'] = 1;
					break;
			}
			
			
			$updateDBData['OriginatedSource'] = $redirectedContent[1];
			$updateDBData['shortLinkActive'] = $this -> SettingIsActive;
			
			$this->db->update('shortlinks', $updateDBData, array('shortLinkID' => $this-> _shortUrlID));
		} else {
			$insertDBData = array();
			$insertDBData['shortUrl'] = $this -> SettingShortURL;
			$insertDBData['Action'] = $this -> SettingRedirectAction;
			switch($redirectedContent[0]) {
				case 1:
					$insertDBData['relatedEventID'] = $redirectedContent[2];
					$insertDBData['relatedBlogID'] = 0;
					$updateDBData['isSubDomain'] = 0;
					break;
				case 2:
					$insertDBData['relatedEventID'] = 0;
					$insertDBData['relatedBlogID'] = $redirectedContent[2];
					$updateDBData['isSubDomain'] = 0;
					break;
				case 3:
					$insertDBData['relatedEventID'] = 0;
					$insertDBData['relatedBlogID'] = 0;
					$updateDBData['isSubDomain'] = 0;
					break;	
				case 4:
					$insertDBData['relatedEventID'] = 0;
					$insertDBData['relatedBlogID'] = 0;
					$updateDBData['isSubDomain'] = 1;
					break;		
					
			}
			
			
			
			$insertDBData['OriginatedSource'] = $redirectedContent[1];
			$insertDBData['shortLinkActive'] = $this -> SettingIsActive;
				
			$NewEvent = $this -> db -> insert('shortlinks', $insertDBData);
		}
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'settings/redirects');
		
	}

	public function UpdateActiveStatusBlogShortUrl() {
		$updateBlogDB = array();
		
		if(isset($this -> ShortCodeLinkActive)) {
			$updateBlogDB['shortLinkActive'] = $this -> ShortCodeLinkActive;	
		}
				
		$this->db->update('shortlinks', $updateBlogDB, array('shortLinkID' => $this -> _ShortLinkID));
	}
		

}