<?php

class RelatedSpecialItem extends BaseObject {
	
	private $_id;
	
	public $RelatedSpecialID;
	public $RelatedInventoryID;
	public $RelatedSpecialItemName;
	public $RelatedSpecialType;
	public $RelatedSpecialTypeValue;
	public $RelatedSpecialExpiredDate;
	public $RelatedDescription;
	public $RelatedDisclaimer;
	public $RelatedInventoryIDS;
	
	
	public $RelatedSpecialTypeSingle;
	public $RelatedSpecialTypeValueSingle;
	public $RelatedSpecialExpiredDateSingle;
	public $RelatedDescriptionSingle;
	public $RelatedDisclaimerSingle;
	
	
	public $LinkedRelatedInventoryID;
	public $SpecialID;
	
	public $SpecialCategory;
	
	public $ItemExpiredDate;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($relatedItemID) {
        $instance = new self();
        $instance->_id = $relatedItemID;
        $instance->loadById();
        return $instance;
    }
		
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM relatedspecialitems LEFT JOIN specials ON relatedspecialitems.relatedSpecialID = specials.SpecialID WHERE RelatedSpecialItemID = :RelatedSpecialItemID');
	    $sth->execute(array(':RelatedSpecialItemID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	

	protected function fill(array $row){
		$this -> SpecialID = $row['relatedSpecialID'];
		$this -> RelatedSpecialTypeValueSingle = $row['Value'];
		$this -> RelatedSpecialTypeSingle = $row['RelatedItemSpecialType'];
		$this -> SpecialCategory = $row['specialType'];
		//$this -> ItemExpiredDate
	}	
		
	
	public function GetID() {
		return $this -> _id;
	}




	private function GenerateEditCategory() {
		$categoryString = NULL;
		
		switch($this -> SpecialCategory) {
			case "VehicleSpecial":			
				break;
							
			case "PartsSpecial":
				break;
					
			case "ServiceSpecial":
				break;
						
			case 5:
				$categoryString = "StoreSpecial";
				break;	
		}
		return $categoryString;
	}
	
	
	public function ValidateRelatedItem() {
		$validationErrors = array();
		
		$multipleSpecialType = false;
		
		//if(!isset($this -> _id)) {
		//	 $specialType = $this -> db -> prepare("SELECT * FROM relatedspecialitems WHERE relatedSpecialID = :relatedSpecialID AND RelatedItemSpecialType = :RelatedItemSpecialType");
		//	 $specialType -> execute(array(":relatedSpecialID" => $this -> RelatedSpecialID,
		//								  ":RelatedItemSpecialType" => $value));
			
            

            
        //}else{
            $specialType = $this -> db -> prepare("SELECT * FROM relatedspecialitems WHERE relatedSpecialID = :relatedSpecialID AND RelatedItemSpecialType = :RelatedItemSpecialType AND Value = :Value");
			$specialType -> execute(array(":relatedSpecialID" => $this -> RelatedSpecialID,
										  ":RelatedItemSpecialType" => $this -> RelatedSpecialType,
										  ":Value" => $this -> RelatedSpecialTypeValue));
        //}
		
		
										  
		if($specialType -> rowCount() > 0) {
			$multipleSpecialType = true;
		}							  
		

		if($this -> validate -> emptyInput($this -> RelatedSpecialType)) {
			array_push($validationErrors, array('errorType' => 'specialType',
												'errorMessage' => 'There are special types that has not been selected'));
		} else if($multipleSpecialType == true) {
			array_push($validationErrors, array('errorType' => 'specialType',
												'errorMessage' => 'There are already special type selected in this special, please select unique special types'));
		}

		if($this -> validate -> emptyInput($this -> RelatedSpecialTypeValue)) {
			array_push($validationErrors, array('errorType' => 'specialTypeValue',
												'errorMessage' => 'There are value that has not been Filled in'));
		}
		
		if($this -> validate -> emptyInput($this -> RelatedSpecialExpiredDate)) {
			array_push($validationErrors, array('errorType' => 'ExpiredDate',
												'errorMessage' => 'There are Expired Date that has not been selected'));
		}

		if($this -> validate -> emptyInput($this -> RelatedDescription)) {
			array_push($validationErrors, array('errorType' => 'Description',
												'errorMessage' => 'There are descriptions that has not been selected'));
		}
		
		if($this -> validate -> emptyInput($this -> RelatedDisclaimer)) {
			array_push($validationErrors, array('errorType' => 'Disclaimer',
												'errorMessage' => 'There are disclaimer that has not been selected'));
		}
		
		//if($multipleSpecialType == true) {
		//	array_push($validationErrors, array('inputID' => 11,
		//										'errorMessage' => 'There are already special type selected in this special, please select unique special types'));
		//}	
		
	
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function RefreshPage() {
		$this -> json -> outputJqueryJSONObject('incentiveSaved', true);
	}
	
	
	public function Save() {
		try {
		
			$incentiveSaveData = array();
			
			
			if(isset($this -> _id)) {
				$specialID = $this -> SpecialID;	

				$incentiveSaveData['RelatedItemSpecialType'] = $this -> RelatedSpecialType;
				$incentiveSaveData['Value'] = $this -> RelatedSpecialTypeValue;
				$incentiveSaveData['ExpiredDate'] = $this -> RelatedSpecialExpiredDate;
				$incentiveSaveData['Description'] = parent::DescriptionFormat($this -> RelatedDescription);
				$incentiveSaveData['Disclaimer'] = parent::DescriptionFormat($this -> RelatedDisclaimer);
				
				if(isset($this -> RelatedInventoryID)) {
					$incentiveSaveData['relatedInventoryID'] = $this -> RelatedInventoryID;
				} else {
					$incentiveSaveData['relatedInventoryID'] = 0;
				}
				
					
				$this->db->update('relatedspecialitems', $incentiveSaveData, array('RelatedSpecialItemID' => $this -> _id));
				
			} else {
				
				$incentiveSaveData['relatedSpecialID'] = $this -> RelatedSpecialID;
				$incentiveSaveData['RelatedItemSpecialType'] = $this -> RelatedSpecialType;
				$incentiveSaveData['Value'] = $this -> RelatedSpecialTypeValue;
				$incentiveSaveData['ExpiredDate'] = $this -> RelatedSpecialExpiredDate;
				$incentiveSaveData['Description'] = parent::DescriptionFormat($this -> RelatedDescription);
				$incentiveSaveData['Disclaimer'] = parent::DescriptionFormat($this -> RelatedDisclaimer);
				
				$this -> db -> insert('relatedspecialitems', $incentiveSaveData);	
								
				$specialID = $this -> RelatedSpecialID;																									  				
			}
			
			$this -> UpdateSpecialExpiredDate($specialID);
			
			$log = new EditHistoryLog();
			$log -> itemID = $specialID;
			$log -> userID = $_SESSION['user'] -> _userId;
			$log -> itemType = 2;		
			$log -> Save();	
			
			
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Related Item Save Error: " . $e->getMessage();
			$TrackError -> type = "RELATED ITEM SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}

	private function UpdateSpecialExpiredDate($id) {
		$prevExpiredDate = NULL;
		$specialExpiredDate = NULL;
		$relatedItems = new RelatedSpecialItemsList();
		
		foreach($relatedItems -> ItemsBySpecial($id) as $itemSingle) {
			if(new DateTime($itemSingle['ExpiredDate']) > $prevExpiredDate) {
				$specialExpiredDate = $itemSingle['ExpiredDate'];	
			}
			
			$prevExpiredDate = new DateTime($itemSingle['ExpiredDate']);
		}
		
		$special = Special::WithID($id);
		$special -> UpdateStartEndDates($specialExpiredDate);
	}

	public function SaveLinkedInventory() {
		try {
		
		if(isset($this -> _id)) {
			$specialID = $this -> SpecialID;
			$relatedIncentiveID = $this -> _id;	
			
			$deleteRelatedInventory = $this -> db -> prepare("DELETE FROM inventoryrelatedspecials WHERE RelatedIncentiveID = :RelatedIncentiveID");
			$deleteRelatedInventory -> execute(array(":RelatedIncentiveID" => $this -> _id));
			
							
			$postData = array('RelatedItemSpecialType' => $this -> RelatedSpecialType,
							  'Value' => $this -> RelatedSpecialTypeValue,
							  'ExpiredDate' => $this -> RelatedSpecialExpiredDate,
							  'Description' => $this -> RelatedDescription,
							  'Disclaimer' => $this -> RelatedDisclaimer);
				
				
					
			$this->db->update('relatedspecialitems', $postData, array('RelatedSpecialItemID' => $this -> _id));
				
		} else {					
			$specialID = $this -> RelatedSpecialID;
			
			$relatedItemID = $this -> db -> insert('relatedspecialitems', array('relatedSpecialID' => $this -> RelatedSpecialID,
																				'RelatedItemSpecialType' => $this -> RelatedSpecialType,
																				'Value' => $this -> RelatedSpecialTypeValue,
																				'ExpiredDate' => $this -> RelatedSpecialExpiredDate,
																				'Description' => $this -> RelatedDescription,
																				'Disclaimer' => $this -> RelatedDisclaimer));	
																				
			$relatedIncentiveID = $relatedItemID;																		
			
			
		}
		
																													  				
		
		
		$inventoryIDS = explode(",", $this -> RelatedInventoryIDS);
		$previousID = NULL;
		foreach($inventoryIDS as $inventorySingle) {
			if($previousID != $inventorySingle) {
				$this -> db -> insert('inventoryrelatedspecials', array('RelatedSpecialID' => $specialID,
																		'RelatedIncentiveID' => $relatedIncentiveID,
																		'RelatedInventoryID' => $inventorySingle));
			}
				
																		
			$previousID	= $inventorySingle;														
		}

		$log = new EditHistoryLog();
		$log -> itemID = $specialID;
		$log -> userID = $_SESSION['user'] -> _userId;
		$log -> itemType = 2;		
		$log -> Save();	

		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Related Item Save Error: " . $e->getMessage();
			$TrackError -> type = "RELATED ITEM SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}

	public function ValidateLinkedInventory() {
		$validationErrors = array();
		
		
		if(!isset($this -> LinkedRelatedInventoryID)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Please select inventory to link to this incentive'));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function LinkInventoryToIncentive() {
		$sth = $this -> db -> prepare("DELETE FROM inventoryrelatedspecials WHERE RelatedIncentiveID = :RelatedIncentiveID");
		$sth -> execute(array(':RelatedIncentiveID' => $this -> _id));
		
		foreach($this -> LinkedRelatedInventoryID as $inventorySingle) {
				
				
			$this -> db -> insert('inventoryrelatedspecials', array('RelatedSpecialID' => $this -> SpecialID,
																	'RelatedIncentiveID' => $this -> _id,
																	'RelatedInventoryID' => $inventorySingle));
		}
		
		
		$this -> json -> outputJqueryJSONObject('incentiveSaved', true);	
	}
	
	
	public function delete() {
		try {
			
			$inventoryLinked = $this -> db -> prepare("DELETE FROM inventoryrelatedspecials WHERE RelatedIncentiveID = :RelatedIncentiveID");
			$inventoryLinked -> execute(array(':RelatedIncentiveID' => $this -> _id));
			
			//delete special from table
			$relatedItem = $this -> db -> prepare("DELETE FROM relatedspecialitems WHERE RelatedSpecialItemID = :RelatedSpecialItemID");
			$relatedItem -> execute(array(':RelatedSpecialItemID' => $this->_id));
			
			
			$relatedItems = new RelatedSpecialItemsList();
			
			$specialExpiredDate = NULL;
			$prevExpiredDate = NULL;
			
			foreach($relatedItems -> ItemsBySpecial($this -> SpecialID) as $item) {
				if(new DateTime($item['ExpiredDate']) > $prevExpiredDate) {
					$specialExpiredDate = $item['ExpiredDate'];	
				}
				$prevExpiredDate = new DateTime($item['ExpiredDate']);
			}
			
			$specialItem = Special::WithID($this -> SpecialID);
			$specialItem -> UpdateStartEndDates($specialExpiredDate);
			
			$redirectString = NULL;
			switch($this -> SpecialCategory) {
				case '4':
					$redirectString = PATH . 'specials/edit/' . $this -> GenerateEditCategory() . '/' . $this -> SpecialID;
					break;		
				case '5':
					$redirectString = PATH . 'specials/addinginventorytoincentive/' . $this -> SpecialID;
					break;	
			}
			
			$this -> redirect -> redirectPage($redirectString);
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Incentive Error: " . $e->getMessage();
			$TrackError -> type = "DELETE SPECIAL ERROR";
			$TrackError -> SendMessage();
		}
	}



}