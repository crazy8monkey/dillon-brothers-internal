<?php

class DataConversion extends BaseObject {
	
	private $_id;
	
	public $_IfKeyWordExists = false;
	
	public $CurrentConversionType;
	
	public $NewConversionType;
	public $ItemType;

	public $specLabelText;
	
	public $ConversionActions;
	public $KeyWordSearch;
	
	public $CurrentColorTexts;
	public $NewColorTexts;
	
	public $CurrentManufactureTexts;
	public $NewManufactureTexts;
	
	public $RuleIDS;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($conversionID) {
        $instance = new self();
        $instance->_id = $conversionID;
        $instance->loadById();
        return $instance;
    }
	
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM dataconversions LEFT JOIN speclabels ON dataconversions.AssociatedItemID = speclabels.specLabelID WHERE dataConversionID = :dataConversionID');
        $sth->execute(array(':dataConversionID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	
	
    protected function fill(array $row){
		$this -> CurrentConversionType = $row['Type'];
		$this -> specLabelText = $row['labelText'];
    }	
	
	public function GetID() {
		return $this -> _id;
	}
	
	public function Validate() {
		$validationErrors = array();
		
		$ifSpecRuleExists = $this->db->prepare('SELECT AssociatedItemID FROM dataconversions WHERE AssociatedItemID = ? AND Type = 1');
        $ifSpecRuleExists -> execute(array($this -> ItemType));

		$colorConversionsExist = $this->db->prepare('SELECT AssociatedItemID FROM dataconversions WHERE Type = ? AND AssociatedItemID = 0');
		$colorConversionsExist -> execute(array($this -> NewConversionType));
		
		if(isset($this->_id)) {
			
			if($this -> CurrentConversionType == 1) {
				if($this -> ValidateConversionRules($this -> ConversionActions)) {
					array_push($validationErrors, array("inputID" => 2,
													    'errorMessage' => 'There are Empty Conversion Action(s)'));
				}
				
				if($this -> ValidateConversionRules($this -> KeyWordSearch)) {
					array_push($validationErrors, array("inputID" => 3,
													    'errorMessage' => 'There are Empty Keyword searche(s)'));
				}	
			}
			
			if($this -> CurrentConversionType == 2) {
				if($this -> ValidateConversionRules($this -> CurrentColorTexts)) {
					array_push($validationErrors, array("inputID" => 2,
													    'errorMessage' => 'There are Empty Current Text field(s)'));
				}
			}
			
			if($this -> CurrentConversionType == 3) {
				
				if($this -> ValidateConversionRules($this -> CurrentManufactureTexts)) {
					array_push($validationErrors, array("inputID" => 2,
													    'errorMessage' => 'There are Empty Current Text field(s)'));
				}
				
				if($this -> ValidateConversionRules($this -> NewManufactureTexts)) {
					array_push($validationErrors, array("inputID" => 3,
													    'errorMessage' => 'There are Empty Switch To field(s)'));
				}
			}	
			
			
			
		} else {
			if($this -> NewConversionType == 0) {
				array_push($validationErrors, array("inputID" => 1,
													'errorMessage' => 'Required'));
			} else {
				if($colorConversionsExist -> fetchAll()) {
					array_push($validationErrors, array("inputID" => 1,
																'errorMessage' => 'There is already a rule set for this particular conversion type'));
				}
				
				
				if($this -> NewConversionType == 1) {
					if($this -> ItemType == 0) {
						array_push($validationErrors, array("inputID" => 2,
															'errorMessage' => 'Required'));
					} else if($ifSpecRuleExists -> fetchAll()) {
						array_push($validationErrors, array("inputID" => 1,
															'errorMessage' => 'There is already a rule set for this particular specification label'));
					}		
				}
			} 
			
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function NewSpecLabelConversion() {
		$newSpecMappingConversion = $this -> db -> insert('dataconversions', array('Type' => 1,
																			   'AssociatedItemID' => $this -> ItemType));
																			   
		$rule = new ConversionRule();
		$rule -> dataConversionID = $newSpecMappingConversion;
																			   
		$rule -> Action = 1;
		$rule -> keyWord = $this -> KeyWordSearch;		
		
		$rule -> Save();															   
	}
	
	public function Save() {
		$redirectID = NULL;
		if(isset($this->_id)) {
			foreach($this -> RuleIDS as $key => $rule) {
				if($rule == 0) {
					$rule = new ConversionRule();
					$rule -> dataConversionID = $this->_id;
				} else {
					$rule = ConversionRule::WithID($rule);
				}
				
				if($this -> CurrentConversionType == 1) {
					$rule -> Action = $this -> ConversionActions[$key];
					$rule -> keyWord = $this -> KeyWordSearch[$key];	
				}
				
				if($this -> CurrentConversionType == 3) {
					$rule -> CurrentManufacture = $this -> CurrentManufactureTexts[$key];
					$rule -> NewManufacture = $this -> NewManufactureTexts[$key];
				}
				
				$rule -> Save();
				
				$redirectID = $this->_id;
			}
			
		} else {
			$DataConversionID = $this -> db -> insert('dataconversions', array('Type' => $this -> NewConversionType,
																			   'AssociatedItemID' => $this -> ItemType));
			$redirectID = $DataConversionID;
		}
		if($this -> CurrentConversionType == 3) {
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'settings/manufactureconversions');
		} else if($this -> CurrentConversionType == 2) {
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'settings/colorconversions');
		} else {
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'settings/specmapping');	
		}
		
	}
	
	
	private function ValidateConversionRules($type) {
		$ReturnValue = NULL;
		foreach ($type as $key => $value) {
			if(empty($value)) {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}
	
	
	public function Delete() {
		$sth = $this -> db -> prepare("DELETE FROM dataconversions WHERE dataConversionID = :dataConversionID");
		$sth -> execute(array(':dataConversionID' => $this -> _id));	
		
		$permissionDelete = $this -> db -> prepare("DELETE FROM conversionrules WHERE relatedDataConversionID = :relatedDataConversionID");
		$permissionDelete -> execute(array(':relatedDataConversionID' => $this -> _id));
		
		$this -> redirect -> redirectPage(PATH. 'settings/type/dataconversions');
	}

	
	

	


}