<?php

require_once TWILIO_API_PATH. 'autoload.php'; // Loads the library
use Twilio\Rest\Client;	

use Twilio\Twiml;

//http://c2cd82e6.ngrok.io/dillonbrothers/updates/twiliocatchresponse?Twiml=%3CResponse%3E%3C%2FResponse%3E


//?Twiml=%3CResponse%3E%3C%2FResponse%3E
class TwilioHandler {
	
	private $TwilioClient;
	
	public $CustomerPhoneNumber;
	public $Message;
	public $CustomerName;

	public $DeptNumber;


	public $MessageServiceID;
	
	public $TextConversationDBID;
	
    public function __construct() {
		$this -> TwilioClient = new Client('AC23289f8d8a0e5a87ae60349eb24ee1b8', 'fe80215bdc30955d0be7a06d18351fbb');
    }
	
	public function InitiateTwilioClient() {
		
	}
	
	
	public function SendOutgoingNumber() {
		$validationRequest = $this -> TwilioClient -> validationRequests->create(
		    $this -> CustomerPhoneNumber,
		    array(
		        "friendlyName" => $this -> CustomerName
		    )
		);
		
		echo $validationRequest->validationCode;
	}
	
	public function SendMessage() {
		$this -> TwilioClient->messages->create(
		    $this -> CustomerPhoneNumber,
		    array(
		    	'from' => '+1' . $this -> DeptNumber,
		        'messagingServiceSid' => "MGa3a1dc71d1967dc7174f89436c503516",
		        'body' => $this -> Message,
		        'ProvideFeedback' => true
		    )
		);
		
	}

}