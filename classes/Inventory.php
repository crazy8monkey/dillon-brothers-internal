<?php

class InventoryObject extends BaseObject {
	
	private $_id;
	private $_vinCheckSQL;
	
	public $_lockedColumns;
	
	private $_MotorCycleSearch;
	private $_BikezContinueSearch = false;
	private $_stopMotorCycleSearch = true;
	
	
	private $_NewBackUpID;
	
	
	
	private $_MotorCycleDBManufacturerWebsite;
	private $_MotorCycleYears = array();
	private $_MotorCycleDBLink;
	private $_MotorCyleBackupCheck = array();
	private $_backUpJSONresponse;
	
	
	private $_NewBackupResponse = array();
	public $CurrentSpecialsOnInventory = array();
	
	
	
	private $_bikezYearLink;
	private $_bikezBikeLink;
	
	
	public $VinNumber;
	public $Year;
	public $Manufacture;
	public $CurrentModel;
	public $NewModel;
	public $ModelFriendlyName;
	public $SpecJSON;
	
	public $StatusCode;
	public $Stock;
	public $Color;
	
	public $CurrentMileage;
	public $NewMileage;
	
	public $CurrentMSRP;
	public $NewMSRP;
	
	public $Conditions;
	public $StoreID;
	public $IsVehicleInfoCatched;
	public $StoreLocation;
	
	public $NewCleanColor;
	public $NewCleanManufacture;
	
	public $UnlockColumn;
	
	public $InventoryActive;
	
	public $markAsSold;
	public $Category;
	public $NewCategory;
	
	public $NewBackedUpCategory;
	
	public $InventoryDescription;
	
	public $IsCurrentInventory = true;
	public $OverlayText;
	public $IsFeatureProduct;
	public $IsPowerEquipment;
	public $IsInService;
	
	public $YoutubeURL;
	
	public $PublishedTime;
	
	
	public $filterType;
	public $filterValue;
	
	public $EngineSizeCC;
	
	public $CategoryName;
	
	//backup inventory list fitler variables
	public $Text;
	public $category;
	public $BackupEmptyEngie;
	
	public $IsDummyInventoryFlag;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($inventoryID) {
        $instance = new self();
        $instance->_id = $inventoryID;
        $instance->loadById();
        return $instance;
    }
	
	public static function WithVIN($vin) {
        $instance = new self();
        $instance->_vinCheckSQL = $vin;
        $instance->loadByVIN();
        return $instance;
    }
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) Colors FROM inventory LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) WHERE inventory.inventoryID = :inventoryID');
        $sth->execute(array(':inventoryID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }

	
	protected function loadByVIN() {
		$sth = $this -> db -> prepare('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) Colors FROM inventory LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) WHERE inventory.VinNumber = :VinNumber');
        $sth->execute(array(':VinNumber' => $this->_vinCheckSQL));	
        
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
        	$this->fill($record);
		} else {
			$this -> IsCurrentInventory = false;
		}
		
	}

	
	
    protected function fill(array $row){
    	$this -> _id = $row['inventoryID'];
		$this -> VinNumber = $row['VinNumber'];
		$this -> IsVehicleInfoCatched = $row['VehicleInfoChecked'];
		$this -> Year = $row['Year'];
		$this -> Manufacture = $row['Manufacturer'];
		$this -> CurrentModel = $row['ModelName'];
		$this -> ModelFriendlyName = $row['FriendlyModelName'];
		$this -> Stock = $row['Stock'];
		$this -> CurrentMileage = $row['Mileage'];
		$this -> CurrentMSRP = $row['MSRP'];
		$this -> Conditions = $row['Conditions'];
		$this -> SpecJSON = $row['SpecJSON'];
		$this -> Color = $row['Colors'];
		$this -> StoreID = $row['InventoryStoreID'];
		$this -> StoreLocation = $row['StoreLocation'];
		$this -> _lockedColumns = $row['LastUpdatedLocks'];
		$this -> InventoryActive = $row['IsInventoryActive'];
		$this -> markAsSold = $row['MarkAsSoldDate'];
		$this -> Category = $row['Category'];
		$this -> InventoryDescription = $row['InventoryDescription'];
		$this -> OverlayText = $row['OverlayText'];
		$this -> IsFeatureProduct = $row['isFeaturedProduct'];
		$this -> IsInService = $row['isInService'];
		$this -> YoutubeURL = $row['youtubeURL'];
		$this -> PublishedTime = $row['inventoryPublishedDate'];
		$this -> EngineSizeCC = $row['EngineSizeCC'];
		$this -> IsDummyInventoryFlag = $row['IsDummyInventory'];
		$this -> CategoryName = $row['inventoryCategoryName'];
    }	
	
	public function GetID() {
		return $this -> _id;
	}
	
	public function GetSpecs() {
		return file_get_contents(INVENTORY_VEHICLE_INFO_URL . $this -> SpecJSON);
	}
	
	public function SaveStaging() {
		$this -> db -> insert('inventorystaging', array('VinNumber' => $this -> VinNumber,
													    'Year' => $this -> Year,
													    'Manufacturer' => $this -> Manufacture,
													    'ModelName' => $this -> Model,
														'StatusCode' => $this -> StatusCode,
														'Stock' => $this -> Stock,
														'Color' => $this -> Color,
														'Mileage' => $this -> Mileage,
														'MSRP' => $this -> MSRP,
														'Conditions' => $this -> Conditions,
														'StoreLocation' => $this -> StoreLocation,
														'Category' => $this -> Category,
														'TimeInserted' => date("Y-m-d", $this -> time -> NebraskaTime()) . "T" . date("H:i:s", $this -> time -> NebraskaTime()),
														'InventoryStoreID' => $this -> StoreID));
															   													   
	}
	
	public function InsertNewInventory() {
		$BulkAdd = $this -> db -> prepare('INSERT INTO inventory (VinNumber, Year, Manufacturer, ModelName, StatusCode, Stock, Color, Mileage, MSRP, Conditions, StoreLocation, TimeInserted, InventoryStoreID)
										   SELECT s.VinNumber, s.Year, s.Manufacturer, s.ModelName, s.StatusCode, s.Stock, s.Color, s.Mileage, s.MSRP, s.Conditions, s.StoreLocation, s.TimeInserted, s.InventoryStoreID FROM inventorystaging s
										   LEFT OUTER JOIN inventory p ON (s.VinNumber = p.VinNumber) WHERE p.VinNumber IS NULL');
		$BulkAdd -> execute();		
	}

	public function UpdateInventory() {
		$tableColumn = ""; 
		$BulkUpdate = $this -> db -> prepare('UPDATE inventory LEFT JOIN inventorystaging ON inventory.VinNumber = inventorystaging.VinNumber 
																	SET inventory.MSRP = (
																		CASE
																			WHEN inventory.LastUpdatedLocks LIKE "%MSRP%" THEN inventory.MSRP
																			WHEN inventorystaging.VinNumber IS NULL THEN inventory.MSRP
																			ELSE inventorystaging.MSRP
																		END
																	),
																	inventory.Mileage = (
																		CASE
																			WHEN inventory.LastUpdatedLocks LIKE "%Mileage%" THEN inventory.Mileage
																			WHEN inventorystaging.VinNumber IS NULL THEN inventory.Mileage
																			ELSE inventorystaging.Mileage
																		END
																	),
																	inventory.StoreLocation = (
																		CASE
																			WHEN inventorystaging.VinNumber IS NOT NULL THEN inventorystaging.StoreLocation
																			WHEN inventorystaging.VinNumber IS NULL THEN inventory.StoreLocation
																			ELSE inventory.StoreLocation
																		END
																	),
																	inventory.InventoryStoreID = (
																		CASE
																			WHEN inventorystaging.VinNumber IS NOT NULL THEN inventorystaging.InventoryStoreID
																			WHEN inventorystaging.VinNumber IS NULL THEN inventory.InventoryStoreID
																			ELSE inventory.InventoryStoreID
																		END
																	),
																	inventory.ModelName = (
																		CASE
																			WHEN inventory.LastUpdatedLocks LIKE "%ModelNumber%" THEN inventory.ModelName
																			WHEN inventorystaging.VinNumber IS NOT NULL THEN inventorystaging.ModelName
																			WHEN inventorystaging.VinNumber IS NULL THEN inventory.ModelName
																			ELSE inventory.ModelName
																		END
																	),
																	inventory.Stock = (
																		CASE
																			WHEN inventorystaging.VinNumber IS NOT NULL THEN inventorystaging.Stock
																			WHEN inventorystaging.VinNumber IS NULL THEN inventory.Stock
																			ELSE inventory.Stock
																		END
																	),
																	inventory.IsDummyInventory = 0');
																	
																	
																	
        $BulkUpdate -> execute();
	}

	public function MarkInventoryAsSold() {
		//updating inventory
		$BulkUpdate = $this -> db -> prepare('UPDATE inventory LEFT JOIN inventorystaging ON inventory.VinNumber = inventorystaging.VinNumber 
																	SET inventory.MarkAsSoldDate = (
																		CASE
																			WHEN inventory.MarkAsSoldDate IS NULL THEN :soldDate
                                                                        	ELSE inventory.MarkAsSoldDate
																		END
																	)
																	WHERE inventorystaging.VinNumber IS NULL');
        $BulkUpdate -> execute(array(":soldDate" => date("Y-m-d", $this -> time -> NebraskaTime())));
		
	}

	public function RemoveItems() {
		$sth = $this -> db -> prepare('DELETE FROM inventory WHERE MarkAsSoldDate < DATE_SUB(:todaysDate, INTERVAL 4 DAY)');
        $sth -> execute(array(":todaysDate" => date("Y-m-d", $this -> time -> NebraskaTime())));
	}

	public function ClearInventory() {
		$sth = $this -> db -> prepare('TRUNCATE TABLE inventorystaging');
        $sth -> execute();
	}
	
	public function Validate($type = NULL) {
		$validationErrors = array();
		
		if($type != NULL) {
			if($this -> NewCategory == 0) {
				array_push($validationErrors, array("inputID" => 5,
													'errorMessage' => 'Required'));
			}		

			if($this -> StoreID == 0) {
				array_push($validationErrors, array("inputID" => 6,
													'errorMessage' => 'Required'));
			}

			if($this -> EngineSizeCC == 0) {
				array_push($validationErrors, array("inputID" => 7,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> NewCleanColor)) {
				array_push($validationErrors, array("inputID" => 8,
													'errorMessage' => 'Required'));
			}

			if($this -> Conditions == 'none') {
				array_push($validationErrors, array("inputID" => 9,
													'errorMessage' => 'Required'));
			}
			
			


		}

		if($this -> validate -> emptyInput($this -> NewMSRP)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> Year)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		}		
		
		if($this -> validate -> emptyInput($this -> Manufacture)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> NewModel)) {
			array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => 'Required'));
		} 
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function ValidateFilters($type) {
		$validationErrors = array();
		
		switch($type) {
			case "Current":
				
				if($this -> validate -> emptyInput($this -> filterValue)) {
					array_push($validationErrors, array("inputID" => 2,
														'errorMessage' => 'Enter Value'));
				}
				
				if(isset($this -> filterType) && isset($this -> filterValue)) {
					$inventory = new CurrentInventoryList();
					$fileredInventory = $inventory -> FilteredInventory($this -> filterType, $this -> filterValue);
					
					if(count($fileredInventory['inventory-list']) == 0) {
						array_push($validationErrors, array("inputID" => 3,
															'errorMessage' => 'This does not return any results, please try another combination'));
					}	
				}
				break;
			
			case "Backup":
				if($this -> validate -> emptyInput($this -> Text)) {
					array_push($validationErrors, array("inputID" => 1,
														'errorMessage' => 'Please type in what you want to filter'));
				}
				break;
		}


		
				
		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Save() {
		
		$seoURL = NULL;
		if($this -> InventoryActive == 1) {
			if($this -> ModelFriendlyName != NULL) {
				$seoURL = parent::seoUrl($this -> Year . '-' . $this -> Manufacture . '-' . $this -> ModelFriendlyName . '-' . $this -> VinNumber);
			} else {
				$seoURL = parent::seoUrl($this -> Year . '-' . $this -> Manufacture . '-' . $this -> NewModel . '-' . $this -> VinNumber);
			}
										
			
		} 
		
		$publishedDate = NULL;
		if($this -> PublishedTime == NULL) {
			if($this -> InventoryActive == 1) {
				$publishedDate = date("Y-m-d", $this -> time -> NebraskaTime()) . "T" . date("H:i:s", $this -> time -> NebraskaTime());	
			}
		} else {
			$publishedDate = $this -> PublishedTime;
		}
		
		
		
		$postData = array('Year' => $this -> Year,
						  'MSRP' => $this -> NewMSRP,
						  'Mileage' => $this -> NewMileage,
						  'Manufacturer' => $this -> Manufacture,
						  'ModelName' => filter_var($this -> NewModel, FILTER_SANITIZE_STRING),
						  'Category' => $this -> NewCategory,
						  'IsInventoryActive' => $this -> InventoryActive,
						  'FriendlyModelName' => filter_var($this -> ModelFriendlyName, FILTER_SANITIZE_STRING),
						  'InventoryDescription' => $this -> InventoryDescription,
						  'OverlayText' => $this -> OverlayText,
						  'isFeaturedProduct' => $this -> IsFeatureProduct,
						  'isInService' => $this -> IsInService,
						  'youtubeURL' => $this -> YoutubeURL,
						  'inventorySeoURL' => $seoURL,
						  'inventoryPublishedDate' => $publishedDate,
						  'inventoryModifiedTime' => date("Y-m-d", $this -> time -> NebraskaTime()) . "T" . date("H:i:s", $this -> time -> NebraskaTime()));	
						  
		//save backup
		$inventoryBackup = InventoryBackup::WithVIN($this -> VinNumber);
		$inventoryBackup -> inventoryBackupYear = $this -> Year;
		$inventoryBackup -> inventoryBackupManufactur = $this -> Manufacture;
		$inventoryBackup -> inventoryBackupModelName = $this -> NewModel;
		$inventoryBackup -> inventoryBackupModelFriendlyName = $this -> ModelFriendlyName;
		$inventoryBackup -> backupNewSpecs = $this -> SpecJSON;
		$inventoryBackup -> NewCategory = $this -> Category;
		$inventoryBackup -> NewEngizeCC = $this -> EngineSizeCC;
		
		
		$inventoryBackup -> UpdateBackupItem();
						  	
		$this -> GenerateSpecs($this -> VinNumber, $this -> SpecJSON);					
							
		$this->db->update('inventory', $postData, array('inventoryID' => $this->_id));
		
		$this -> UpdateLocksColumn();
	}

	public function SaveDummy() {
		
		if(isset($this->_id)) {
			$publishedDate = NULL;
			if($this -> PublishedTime == NULL) {
				if($this -> InventoryActive == 1) {
					$publishedDate = date("Y-m-d", $this -> time -> NebraskaTime()) . "T" . date("H:i:s", $this -> time -> NebraskaTime());	
				}
			} else {
				$publishedDate = $this -> PublishedTime;
			}
			
			
			if($this -> ModelFriendlyName != NULL) {
				$seoURL = parent::seoUrl($this -> Year . '-' . $this -> Manufacture . '-' . $this -> ModelFriendlyName . '-' . $this -> VinNumber);
			} else {
				$seoURL = parent::seoUrl($this -> Year . '-' . $this -> Manufacture . '-' . $this -> Model . '-' . $this -> VinNumber);
			}
			
			$SpecFileName = $this -> SpecJSON;
			
			
			
			$postData = array('Year' => $this -> Year,
						  'MSRP' => $this -> NewMSRP,
						  'Mileage' => $this -> NewMileage,
						  'Manufacturer' => $this -> Manufacture,
						  'ModelName' => filter_var($this -> Model, FILTER_SANITIZE_STRING),
						  'Category' => $this -> NewCategory,
						  'IsInventoryActive' => $this -> InventoryActive,
						  'FriendlyModelName' => filter_var($this -> ModelFriendlyName, FILTER_SANITIZE_STRING),
						  'InventoryDescription' => $this -> InventoryDescription,
						  'OverlayText' => $this -> OverlayText,
						  'isFeaturedProduct' => $this -> IsFeatureProduct,
						  'InventoryStoreID' => $this -> StoreID,
						  'youtubeURL' => $this -> YoutubeURL,
						  'inventorySeoURL' => $seoURL,
						  'inventoryPublishedDate' => $publishedDate,
						  'inventoryModifiedTime' => date("Y-m-d", $this -> time -> NebraskaTime()) . "T" . date("H:i:s", $this -> time -> NebraskaTime()));
			
			
			
			
						  
			$this->db->update('inventory', $postData, array('inventoryID' => $this->_id));	
			
			$this -> GenerateSpecs($this -> VinNumber, $this -> SpecJSON);	
			
			$redirectID	= $this->_id;
		} else {
			
			$VehicleVinText = parent::seoUrl($this -> Year . str_replace('-', '',$this -> Manufacture) . $this -> NewCleanColor . mt_rand(1, 9999));
			
			$newInventory = $this -> db -> insert('inventory', array('VinNumber' => $VehicleVinText,
													    'Year' => $this -> Year,
													    'Manufacturer' => $this -> Manufacture,
													    'ModelName' => $this -> Model,
														'StatusCode' => $this -> StatusCode,
														'Stock' => $this -> Stock,
														'Color' => $this -> NewCleanColor,
														'Mileage' => $this -> NewMileage,
														'MSRP' => $this -> NewMSRP,
														'Conditions' => $this -> Conditions,
														'StoreLocation' => '',
														'StatusCode' => 'A',
														'Stock' => '',
														'SpecJSON' => $VehicleVinText . 'Specs.json', 
														'Category' => $this -> NewCategory,
														'TimeInserted' => date("Y-m-d", $this -> time -> NebraskaTime()) . "T" . date("H:i:s", $this -> time -> NebraskaTime()),
														'InventoryStoreID' => $this -> StoreID,
														'InventoryDescription' => $this -> InventoryDescription,
														'OverlayText' => $this -> OverlayText,
														'youtubeURL' => $this -> YoutubeURL,
														'EngineSizeCC' => $this -> EngineSizeCC,
														'Conditions' => $this -> Conditions,
														'IsDummyInventory' => 1,
														'VehicleInfoChecked' => 1));
														
			
			$redirectID	= $newInventory;
			$labels = new SpecList();
			
			$emptySpecString = array();
								
			foreach($labels -> SpecLabelsByGroup() as $label) {
				array_push($emptySpecString, array('LabelID' => $label['specLabelID'],
												   'Content' => ''));
					
			}
			$this -> GenerateSpecs($VehicleVinText, json_encode($emptySpecString));	
			
			mkdir(PHOTO_PATH . 'inventory/' . $VehicleVinText);
										
		}

		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'inventory/edit/' . $redirectID);
	}  
	
	public function Delete() {
		unlink(INVENTORY_VEHICLE_INFO_PATH . $this -> SpecJSON);
		
		Folder::DeleteDirectory(PHOTO_PATH . 'inventory/' . $this -> VinNumber);
		
		switch($this -> StoreID) {
			case 2:
				$storestring = 'motorsports';
				break;
			case 3:
				$storestring = 'harleydavidson';
				break;
			case 4:
				$storestring = 'indian';
				break;
		}
		$inventoryDelete = $this -> db -> prepare("DELETE FROM inventory WHERE inventoryID = :inventoryID");
		$inventoryDelete -> execute(array(':inventoryID' => $this -> _id));
		
		$inventoryPhotos = $this -> db -> prepare("DELETE FROM inventoryphotos WHERE relatedInventoryID = :relatedInventoryID");
		$inventoryPhotos -> execute(array(':relatedInventoryID' => $this -> _id));
		
		$this -> redirect -> redirectPage(PATH. 'inventory/currentinventory/' . $storestring. '/1');
	} 
	
	private function UpdateLocksColumn() {
		$UpdateLockFields = array();
		
		$totalLockedFields = json_decode($this -> _lockedColumns, true);
		
		if(count($totalLockedFields) > 0) {
			$MSRPKey = array_search('MSRP', array_column($totalLockedFields, 'ColumnLocked'));
			$Mileagekey = array_search('Mileage', array_column($totalLockedFields, 'ColumnLocked'));	
			$ModelKey = array_search('ModelNumber', array_column($totalLockedFields, 'ColumnLocked'));
		} else {
			$MSRPKey = false;
			$Mileagekey = false;
			$ModelKey = false;
		}
		
		if(isset($this -> NewMSRP)) {
			if($MSRPKey !== false) {
				array_push($UpdateLockFields, array("ColumnLocked" => "MSRP",
														"Value" => $totalLockedFields[$MSRPKey]["Value"]));
			} else if($this -> NewMSRP != $this -> CurrentMSRP) {
				array_push($UpdateLockFields, array("ColumnLocked" => "MSRP",
													"Value" => $this -> NewMSRP));
			}	
		} 
		
		
		if(isset($this -> NewMileage)) {
			if($Mileagekey !== false) {
				array_push($UpdateLockFields, array("ColumnLocked" => "Mileage",
													"Value" => $totalLockedFields[$Mileagekey]["Value"]));
			} else if($this -> NewMileage != $this -> CurrentMileage) {
				array_push($UpdateLockFields, array("ColumnLocked" => "Mileage",
													"Value" => $this -> NewMileage));
			}	
		}
		
		
		
		if($ModelKey !== false) {
			array_push($UpdateLockFields, array("ColumnLocked" => "ModelNumber",
												"Value" => $totalLockedFields[$ModelKey]["Value"]));
		} else if($this -> NewModel != $this -> CurrentModel) {
			array_push($UpdateLockFields, array("ColumnLocked" => "ModelNumber",
												"Value" => $this -> NewModel));
		}
		
		
				
			
		if(count($UpdateLockFields) > 0) {
			$postData = array('LastUpdatedLocks' => json_encode($UpdateLockFields));	
			//$postData = array('LastUpdatedLocks' => count($totalLockedFields));	
		
			$this->db->update('inventory', $postData, array('inventoryID' => $this->_id));	
		}
			
			
		
		
	}
	
	
	public function UpdateColor() {
		$postData = array('Color' => $this -> NewCleanColor);
		$this->db->update('inventory', $postData, array('inventoryID' => $this->_id));
	}
	
	public function UpdateManufacture() {
		$postData = array('Manufacturer' => $this -> NewCleanManufacture);
		$this->db->update('inventory', $postData, array('inventoryID' => $this->_id));
	}
	
	public function UnLockColumn() {
		$newLockedArray = array();
		$lockedFields = json_decode($this -> _lockedColumns, true);	
		
		$key = array_search($this -> UnlockColumn, array_column($lockedFields, 'ColumnLocked'));
		
		
		unset($lockedFields[$key]);
		sort($lockedFields);

		$postData = array('LastUpdatedLocks' => json_encode($lockedFields));			
		$this->db->update('inventory', $postData, array('inventoryID' => $this->_id));	
		$this -> redirect -> redirectPage(PATH. 'inventory/edit/' . $this->_id);
	}
	
	public function FilteredInventoryPage() {
		$storeString = '';
		switch($this -> StoreID) {
			case 2:
				$storeString = "motorsports";
				break;
			case 3:
				$storeString = "harleydavidson";
				break;
			case 4:
				$storeString = "indian";
				break;
		}
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'inventory/filteredinventory/' . $storeString. '?value=' . $this -> filterValue);	
	}
	
	public function FilteredBackupList() {
		$queryResult = NULL;	
		//public $Text;
		//public $category;
		if(!empty($this -> Text)) {
			$queryResult .= 'SearchQuery=' .urldecode($this -> Text) . '&';	
		}
		
		if($this -> category != 0) {
			$queryResult .= 'category=' . $this -> category . '&';
		}
		
		if($this -> BackupEmptyEngie != '0') {
			$queryResult .= 'FilteredBy=' . $this -> BackupEmptyEngie . '&';
		}
		
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'inventory/backup/1'. ($queryResult!= NULL ? '?' . rtrim($queryResult, '&') : ''));	
	}
	
	public function SavedFromBackup() {
		$postData = array('Year' => $this -> Year,
						  'Manufacturer' => $this -> Manufacture,
						  'ModelName' => $this -> NewModel,
						  'FriendlyModelName' => $this -> ModelFriendlyName,
						  'EngineSizeCC' => $this -> EngineSizeCC,
						  'Category' => $this -> Category);	
		
		$this->db->update('inventory', $postData, array('inventoryID' => $this->_id));	
		
		
		$this -> GenerateSpecs($this -> VinNumber, $this -> SpecJSON);
		$this -> UpdateLocksColumn();
					  
	}
	
	
	
	private function GenerateStoreInventoryLink() {
		$store = Store::WithID($this -> StoreID);
		
		$storeNameLink = NULL;
		
		switch($this -> StoreID) {
			case 2:
				$storeNameLink = "<a href='" . $this -> pages -> inventory() . "/currentinventory/motorsports/1'>Motor Sport Inventory</a>";
				break;
			case 3:
				$storeNameLink = "<a href='" . $this -> pages -> inventory() . "/currentinventory/harleydavidson/1'>Harley-Davidson Inventory</a>";
				break;
			case 4:
				$storeNameLink = "<a href='" . $this -> pages -> inventory() . "/currentinventory/indian/1'>Indian Inventory</a>";
				break;		
		}
		
		return $storeNameLink;
		
	}
	
	
	public function GetInventorySpecBreadCrumbs() {
		return "<a href='" . $this -> pages -> inventory() ."'>Inventory Portal</a><span><i class='fa fa-caret-right' aria-hidden='true'></i></span><a href='" . $this -> pages -> inventory() ."/currentinventory'>Select Store</a><span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . $this -> GenerateStoreInventoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>View/Edit Inventory";
	
	}
	
	
	public function GetVehicleInfo() {
		ini_set("allow_url_fopen", 1);
		foreach($this -> db -> select('SELECT * FROM inventory WHERE VehicleInfoChecked = 0 LIMIT 20') as $vehicleInfo) {
			
			$this -> DecodeSingleVin($vehicleInfo['VinNumber'], $vehicleInfo['Year'], $vehicleInfo['ModelName'], $vehicleInfo['Manufacturer'], $vehicleInfo['inventoryID']);
		}
		
	}
	
	public function RemoveInventoryFromSpecials() {
		foreach($this -> db -> select('SELECT * FROM inventory WHERE MarkAsSoldDate IS NOT NULL') as $vehicleInfo) {
			//delete realted inventory
			$relatedInventoryRemove = $this -> db -> prepare('DELETE FROM inventoryrelatedspecials WHERE RelatedInventoryID = :RelatedInventoryID');
			$relatedInventoryRemove -> execute(array(':RelatedInventoryID' => $vehicleInfo['inventoryID']));
			
		}
	}

	public function UpdateURLS() {
		foreach($this -> db -> select('SELECT * FROM inventory WHERE IsInventoryActive = 1') as $ActiveInfo) {
			
			
			if($ActiveInfo['FriendlyModelName'] != NULL) {
				$seoURL = parent::seoUrl($ActiveInfo['Year'] . '-' . $ActiveInfo['Manufacturer'] . '-' . $ActiveInfo['FriendlyModelName'] . '-' . $ActiveInfo['VinNumber']);
			} else {
				$seoURL = parent::seoUrl($ActiveInfo['Year'] . '-' . $ActiveInfo['Manufacturer'] . '-' . $ActiveInfo['ModelName'] . '-' . $ActiveInfo['VinNumber']);
			}
			
			$postData = array('inventorySeoURL' => $seoURL);	
		
			$this->db->update('inventory', $postData, array('inventoryID' => $ActiveInfo['inventoryID']));	
			
		}
	}
	
	
	public function UpdateEngineSizeCC() {
		$backup = new BackupInventoryList();
		foreach($backup -> BackuplinkedInventory() as $currentVin) {
			$postData = array('EngineSizeCC' => $currentVin['backupEngineSizeCC']);	
		
			$this->db->update('inventory', $postData, array('inventoryID' => $currentVin['inventoryID']));	
		}
	}

	
	private function DecodeSingleVin($vinNumber = NULL, $year = NULL, $model = NULL, $make = NULL, $inventoryID = NULL) {
		try{
          
			$emailText = array();	
				
			$VinNumberCheckString = NULL;
			$YearCheck = NULL;
			$makeCheckString = NULL;
			$modelArray = array();
			$inventoryIDCheck = NULL;
			
			//vin number 
			if($vinNumber != NULL) {
				$VinNumberCheckString = $vinNumber;
			} else {
				$VinNumberCheckString = $this -> VinNumber;
			}
			
			mkdir(PHOTO_PATH . 'inventory/' . $VinNumberCheckString);
			
			
			//year
			if($year != NULL) {
				$YearCheck = $year;
			} else {
				$YearCheck = $this -> Year;
			}
			
			$apiURL = "https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/" . $VinNumberCheckString . "*BA?format=json&modelyear=" . $YearCheck;
				
			$jsonInfo = file_get_contents($apiURL);
			$responses = json_decode($jsonInfo, true);			
			
			
			//make
			if($make != NULL) {
				$makeCheckString = $make;
			} else {
				$makeCheckString = $this -> Manufacture;
			}
			
		
			//model
			if($model != NULL) {
				$modelArray['DatabaseModel'] = $model;
			} else {
				$modelArray['DatabaseModel'] = $this -> Model;
			}
			
			$modelArray['APIModel'] = $responses['Results']['0']['Model'];
			$modelArray['Series'] = $responses['Results']['0']['Series'];
			
			//inventory ID
			if($inventoryID != NULL) {
				$inventoryIDCheck = $inventoryID;
			} else {
				$inventoryIDCheck = $this -> _id;
			}
			
			
		
			$vinNumberCheckObj = array();
			//vin number check
			$vinNumberCheck = $this -> db -> prepare('SELECT * FROM vinnumberbackup LEFT JOIN inventorybackupsystem ON vinnumberbackup.inventoryBackupBikeID = inventorybackupsystem.inventoryBackupID WHERE backedupVinNumber = :backedupVinNumber');
			$vinNumberCheck -> execute(array(":backedupVinNumber" => $VinNumberCheckString));
			$vinNumberCheckObj = $vinNumberCheck -> fetch();
			
			$inventoryNameCheck = $this -> db -> prepare('SELECT * FROM inventorybackupsystem WHERE backupYearCheck = :backupYearCheck AND backupManufactureCheck = :backupManufactureCheck AND backupModelCheck = :backupModelCheck');
			$inventoryNameCheck -> execute(array(":backupYearCheck" => parent::CleanStringNoSpaces($YearCheck),
												 ":backupManufactureCheck" => parent::CleanStringNoSpaces(strtolower($makeCheckString)),
												 ":backupModelCheck" => parent::CleanStringNoSpaces(strtolower($modelArray['DatabaseModel']))));
			$getSpecs = $inventoryNameCheck -> fetch();
			
			
			//echo "Backup Manufacturer Check: " . parent::CleanStringNoSpaces(strtolower($makeCheckString)) . "<br />";
			//echo "Backup Model Check: " . parent::CleanStringNoSpaces(strtolower($modelArray['DatabaseModel'])) . "<br />";
			
			if($vinNumberCheck->rowCount() > 0) {
								  
	          $postData = array('VehicleInfoChecked' => 1,
	                            'Manufacturer' => $vinNumberCheckObj['inventoryBackupManufactur'],
								'ModelName' => $vinNumberCheckObj['inventoryBackupModelName'],
								'FriendlyModelName' => $vinNumberCheckObj['inventoryBackupModelFriendlyName'],
								'SpecJSON' => $VinNumberCheckString. 'Specs.json',
								'Category' => $vinNumberCheckObj['inventoryBackupCategory'],
								'EngineSizeCC' => $vinNumberCheckObj['backupEngineSizeCC'],
								'LastUpdatedLocks' => json_encode(array("ColumnLocked" => "ModelNumber", "Value" => $vinNumberCheckObj['inventoryBackupModelName'])));									
								
				$specJSON = file_get_contents(INVENTORY_VEHICLE_INFO_URL . 'backup/' . $vinNumberCheckObj['inventoryBackupSpecJSON']);
				
				
	           $emailText['RequireValidation'] = false;
			   $this -> GenerateSpecs($VinNumberCheckString, $specJSON);				  
				
				$emailText['ResultType'] = "Vin Number Check Successfull";
							  
			} else if($inventoryNameCheck->rowCount() > 0) {
			  	$postData = array('VehicleInfoChecked' => 1,
	              				  'Manufacturer' => $getSpecs['inventoryBackupManufactur'],
								  'ModelName' => $getSpecs['inventoryBackupModelName'],
								  'FriendlyModelName' => $getSpecs['inventoryBackupModelFriendlyName'],
								  'SpecJSON' => $VinNumberCheckString . 'Specs.json',
								  'Category' => $getSpecs['inventoryBackupCategory'],
								  'EngineSizeCC' => $getSpecs['backupEngineSizeCC'],
									'LastUpdatedLocks' => json_encode(array("ColumnLocked" => "ModelNumber", "Value" => $getSpecs['inventoryBackupModelName'])));
				
				
				$this -> GenerateSpecs($VinNumberCheckString, file_get_contents(INVENTORY_VEHICLE_INFO_PATH . 'backup/' . $getSpecs['inventoryBackupSpecJSON']));
				
				
				$backupVin = new BackupVinNumber();
				$backupVin -> BackupVinNumber = $VinNumberCheckString;
				$backupVin -> inventoryBackupInventoryID = $getSpecs['inventoryBackupID'];													 						
				$backupVin -> Save();
				$emailText['RequireValidation'] = false;
				$emailText['ResultType'] = "Inventory backup check Successfull";
				
			} else {
				
				$totalManufactureChangeRules = new ConversionRulesList();	
				
				$cleanMake = NULL;
				
				//monkey learn
				require 'libs/MonkeyLearn/autoload.php';	
				$module_id = 'cl_4LLnXuzp';
				
				$ml = new MonkeyLearn\Client('c6046ac2da646a10e42c70be661ecf6fbe565c21');	
				
				$key = array_search($makeCheckString, array_column($totalManufactureChangeRules -> ManufactureChangesRules(), 'CurrentManufactureText'));
				if($key !== false) {
					$cleanMake = $totalManufactureChangeRules -> ManufactureChangesRules()[$key]['NewManufacturerText'];
				} else {
					$cleanMake = $modelArray['DatabaseModel'];
				}
						
				$text_list = [$cleanMake . ' '. $modelArray['DatabaseModel']];
				
				$res = $ml->classifiers->classify($module_id, $text_list, true);
				
				$results = end($res-> result);
				$selectedVehicleCategory = end($results);
				
				
				$vehicleCategory = InventoryCategoryObject::MonkeyLearnID($selectedVehicleCategory['category_id']);
			
			
				$this -> NewBackedUpCategory = $vehicleCategory -> GetID();
				
				
				$postData = array('VehicleInfoChecked' => 1,
								  'Manufacturer' => $makeCheckString,
								  'SpecJSON' => $VinNumberCheckString . 'Specs.json',
								  'Category' => $vehicleCategory -> GetID());	
				
				//$responses['Results']['0']['Model']
				
				
				$this -> GrabMotorCycleInfo($VinNumberCheckString, $makeCheckString, $modelArray, $YearCheck);	
			//	
				$this -> CreateBackup($VinNumberCheckString, $YearCheck, $modelArray['DatabaseModel'], $makeCheckString);
				
				
				
				if($this -> _MotorCycleSearch == "MotorCycleDBSearchEnd") {
					$emailText['RequireValidation'] = true;
					$emailText['BikeLookup'] = "https://www.google.com/search?q=" . $YearCheck . "+" . $responses['Results']['0']['Make'] . "+" . $modelArray['DatabaseModel'];
					$emailText['ResultType'] = "Bike Found on motorcycledb.com / New Duplicate";
					$emailText['BackupInventoryLink'] = PATH . "updates/redirectlogin?val=inventory/editbackup/" . $this -> _NewBackUpID;
					
					$emailText['saved-response'] = $this -> _NewBackupResponse;
				} 
				if($this -> _MotorCycleSearch == "BikezSearchEnd") {
					$emailText['RequireValidation'] = true;
					$emailText['BikeLookup'] = "https://www.google.com/search?q=" . $YearCheck . "+" . $responses['Results']['0']['Make'] . "+" . $modelArray['DatabaseModel'];
					$emailText['ResultType'] = "Bike Found on bikez.com / New Duplicate";
					$emailText['BackupInventoryLink'] = PATH . "updates/redirectlogin?val=inventory/editbackup/" . $this -> _NewBackUpID;
					
					$emailText['saved-response'] = $this -> _NewBackupResponse;
				} 
				if($this -> _MotorCycleSearch == "StopSearching") {
					$emailText['RequireValidation'] = true;
					$emailText['BikeLookup'] = "https://www.google.com/search?q=" . $YearCheck . "+" . $responses['Results']['0']['Make'] . "+" . $modelArray['DatabaseModel'];
					$emailText['ResultType'] = "Could not find bike on outsite sources / New Duplicate";
				}
		
				
			}
			
			
			if(LIVE_SITE == true) {
				
				$emailText['vinNumber'] = $VinNumberCheckString;
				$emailText['year'] = $YearCheck;
				$emailText['make'] = $responses['Results']['0']['Make'];
				$emailText['model'] = $modelArray['DatabaseModel'];
				$emailText['inventoryID'] = "<a href='" . PATH . "updates/redirectlogin?val=inventory/edit/" . $inventoryIDCheck . "'>". $inventoryIDCheck . "</a>";
				
				$setting = SettingsObject::Init();
				$this -> email -> to = $setting -> InventorySpecNotifications;	
				$this -> email -> subject = $VinNumberCheckString . " Spec Lookup";			
				$this -> email -> InventorySpecLookup($emailText);
			}
			
			
			$this->db->update('inventory', $postData, array('inventoryID' => $inventoryIDCheck));
		
		} catch (Exception $e) {
        	echo $e->getMessage();
        }  
					
	}

	private function GrabMotorCycleInfo($vin, $make, $model = array(), $year) {
			
		$this -> MotorCyleDBGetManufactureWebsite($make);
		
		if($this -> _MotorCycleSearch == "ContinueMotorCycleDBYear") {
			$this -> MotorCycleDBGetMotorCycleYear($year);
		}
		
		if($this -> _MotorCycleSearch == "ContinueMotorCycleDBGetBike") {
			$this -> MotorCycleDBGetBike($make, $model);
		}
		
		if($this -> _MotorCycleSearch == "ContinueMotorCycleDBGetBikeSpecs") {
			$this -> MotoryCycleDBGetBikeSpecs($vin);
		}
		
		if($this -> _MotorCycleSearch == "StartBikezGetYear") {
			$this -> BikezGetYear($year);
		}
		
		if($this -> _MotorCycleSearch == "BikezGetBikeWithinYear") {
			$this -> BikezGetBike($make, $model);
		}
		
		if($this -> _MotorCycleSearch == "BikezGetBikeBikeSpecs") {
			$this -> BikezGetSpecs($vin);
		}
		
		if($this -> _MotorCycleSearch == "StopSearching") {
			$labels = new SpecList();
			$emptySpecString = array();
								
			foreach($labels -> SpecLabelsByGroup() as $label) {
				array_push($emptySpecString, array('LabelID' => $label['specLabelID'],
												   'Content' => ''));
					
			}
					
			$this -> GenerateSpecs($vin, json_encode($emptySpecString));
		}
		
		

		
		
		
				
	}

	
	
	private function BikezGetYear($year) {
		if(LIVE_SITE == false) {
			echo "<br /><br />Bikez.com year search<br /><br />"; 	
		}
		
		libxml_use_internal_errors(true);
		$Year = $this -> file_get_contents_curl("http://www.bikez.com/years/index.php");
		
		$doc = new DOMDocument();
		$doc->loadHTML(mb_convert_encoding($Year, 'HTML-ENTITIES', 'UTF-8'));
		libxml_use_internal_errors(false);
		
		$xpath = new DOMXpath($doc);
		$result = $xpath->query('//table[@class="zebra"]');
		
		$yearListTable = $doc -> saveXML($result->item(0));
		//echo $doc -> saveXML($result->item(0));
		$yearListHTML = new DOMDocument();
		$yearListHTML -> loadHTML(mb_convert_encoding($yearListTable, 'HTML-ENTITIES', 'UTF-8'));
		
		$yearLinks = $yearListHTML->getElementsByTagName('a');
			
		foreach($yearLinks as $link) {
			//echo $link->getAttribute('href'). "<br />";
			
			if(strpos($link->getAttribute('href'), $year) !== false ) {
				if(LIVE_SITE == false) {
					echo str_replace("..", "", $link->getAttribute('href')) . '<br />';
				}
				
				$this -> _bikezYearLink = str_replace("..", "", $link->getAttribute('href'));
				break;
			}
			
		}
		
		if(!empty($this -> _bikezYearLink)) {
			$this -> _MotorCycleSearch = "BikezGetBikeWithinYear";
		} else {
			$this -> _MotorCycleSearch = "StopSearching";
		}
		
		//$_BikezContinueSearch
        if(LIVE_SITE == false) {
          echo "Selected Bikez Year Link: " . $this -> _bikezYearLink . "<br />";
        }
		
	}
	
	private function BikezGetBike($make, $model) {
		//$model['DatabaseModel'];
		//$model['APIModel'];
		libxml_use_internal_errors(true);
		
		$BikeYear = $this -> file_get_contents_curl("http://www.bikez.com" . $this -> _bikezYearLink);
		
		$doc = new DOMDocument();
		$doc->loadHTML(mb_convert_encoding($BikeYear, 'HTML-ENTITIES', 'UTF-8'));
		$internalErrors = libxml_use_internal_errors(true);
		libxml_use_internal_errors($internalErrors);
		
		$xpath = new DOMXpath($doc);
		$result = $xpath->query('//table[@class="zebra"]');
		
		$bikeListTable = $doc -> saveXML($result->item(0));
		//echo $doc -> saveXML($result->item(0));
		$bikeList = new DOMDocument();
		$bikeList -> loadHTML(mb_convert_encoding($bikeListTable, 'HTML-ENTITIES', 'UTF-8'));
		
		$bikeLinks = $bikeList -> getElementsByTagName('a');
		$stringPostionArray = array();
		libxml_use_internal_errors(false);
			
		foreach($bikeLinks as $link) {
			if(LIVE_SITE == false) {
				echo "Link Url Foreach value: ". $link -> nodeValue. "<br />";
				echo "Passed Make Variable: " . $make. "<br />";
			}
			
			
			if(strpos(strtolower($link -> nodeValue), strtolower($make)) !== false ) {
				if(strpos($link->getAttribute('href'), "/motorcycles/")) {
					$currentURLText = $link->nodeValue;
					
					if(LIVE_SITE == false) {
						echo "Current URL text: " . str_replace(" ", "", $currentURLText) . "<br />";
						echo "Database Model: "  . $model['DatabaseModel'] . "<br />";
						echo "API Model: "  . $model['APIModel']. "<br />";
						echo "Series: "  . $model['Series']. "<br />";
					}
							
					
					$stringPositionValue = similar_text(str_replace(" ", "", $currentURLText), $model['DatabaseModel'] . $model['APIModel'] . $model['Series']);
			
					array_push($stringPostionArray, array("StringPosition" => $stringPositionValue,
														  "Link" => $link->getAttribute('href')));
				}
				
			}
			
		}	
		
		usort($stringPostionArray, function($a, $b) {
			return $a['StringPosition'] - $b['StringPosition'];
		});
			
		if(LIVE_SITE == false) {
			echo "<pre>";
			print_r($stringPostionArray);
			echo "</pre>";	
		}	
		
	
		
		$LastArray = end($stringPostionArray);
		
		if($LastArray['StringPosition'] != 0) {
			$this -> _MotorCycleSearch = "BikezGetBikeBikeSpecs";
			$this -> _bikezBikeLink = str_replace("..", "",$LastArray['Link']);	
		} else {
			$this -> _MotorCycleSearch = "StopSearching";
		}	
		
		if(LIVE_SITE == false) {
			echo "Final Bike URL: " . $this -> _bikezBikeLink;	
		}
			
		
	}
	
	
	
	public function GetSpecificationsSingle() {
		$this -> DecodeSingleVin();
	}
	
	public function RedirectToInventoryEdit() {
		$this -> redirect -> redirectPage(PATH. 'inventory/edit/' . $this -> _id);
	}
	
	public function JSONRedirect() {
		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'inventory/edit/' . $this -> _id);
	}
	
	private function GenerateSpecs($vin, $specContent) {
		$fileName = INVENTORY_VEHICLE_INFO_PATH . $vin . 'Specs.json';
		$JSONVinInfo = fopen($fileName, "w");
		fwrite($JSONVinInfo, $specContent);
		fclose($JSONVinInfo);
	}
	
	//get specs section
	
	
	//get manufacutre site
	private function MotorCyleDBGetManufactureWebsite($manufacturer) {
		$totalManufactureChangeRules = new ConversionRulesList();	
			
		if(LIVE_SITE == false) {	
			echo "MotorycleDB manufacture Find<br /><br />";	
		}
		
		libxml_use_internal_errors(true);
		$manufactureLinkCheck = NULL;
		
		$key = array_search($manufacturer, array_column($totalManufactureChangeRules -> ManufactureChangesRules(), 'CurrentManufactureText'));
		if($key !== false) {
			$manufactureLinkCheck = $totalManufactureChangeRules -> ManufactureChangesRules()[$key]['NewManufacturerText'];
		} else {
			$manufactureLinkCheck = $manufacturer;
		}
		
		
		$manufactureURL = $this -> file_get_contents_curl("http://www.motorcycledb.com/manufacturers.php");
		
		if ($manufactureURL === TRUE) {
			//parsing begins here:
			$doc = new DOMDocument();
			$doc->loadHTML($manufactureURL);
			libxml_use_internal_errors(false);
			$hyperLinks = $doc->getElementsByTagName('a');	
				
			foreach ($hyperLinks as $link) {
				if(strpos($link->getAttribute('href'), '/Motorcycle_Manufacturer/') !== false ) {
					
					$manufacturerLinkArray = explode('/', $link->getAttribute('href'));	
					if(strpos(strtolower($manufactureLinkCheck), strtolower($manufacturerLinkArray[2])) !== false ) {
						$this -> _MotorCycleDBManufacturerWebsite = $link->getAttribute('href');
						$this -> _MotorCycleSearch = "ContinueMotorCycleDBYear";
						break;
					} else {
						$this -> _MotorCycleSearch = "StartBikezGetYear";
					}
					
					if(LIVE_SITE == false) {
						echo $link->getAttribute('href') . "<br />";	
						echo $link -> getAttribute('href') . "<br />";
						echo "Manufacture Passed Variable: " . $manufacturer . "<br />";
						echo "<br />";	
					}
					
				}	
				
			}
	
			
			if(LIVE_SITE == false) {
				echo "MotorycleDB manufacture hyperlink manufacture website: ". $this -> _MotorCycleDBManufacturerWebsite;	
			}	
		} else {
			$this -> _MotorCycleSearch = "StartBikezGetYear";
		}
		
	}

	//get year
	private function MotorCycleDBGetMotorCycleYear($year) {
		libxml_use_internal_errors(true);
		$manufactureURL = $this -> file_get_contents_curl("http://www.motorcycledb.com" . $this -> _MotorCycleDBManufacturerWebsite);
		
		
		//parsing begins here:
		$doc = new DOMDocument();
		$doc->loadHTML($manufactureURL);
		libxml_use_internal_errors(false);
		
		$yearsLinks = $doc->getElementsByTagName('a');
		
		foreach ($yearsLinks as $aTag) {
			if(strpos($aTag->getAttribute('href'), $year) !== false ) {
				
				array_push($this -> _MotorCycleYears, $aTag->getAttribute('href'));
				//echo  "<pre>";
				//print_r($tableTag);
				//echo  "</pre>";
				if(LIVE_SITE == false) {
					echo $aTag->getAttribute('href'). "<br />";	
				}
				
			}	
			
		}
		
		if(LIVE_SITE == false) {
			echo "<br />Motorcycle DB Years";
			echo "<pre>";
			print_r($this -> _MotorCycleYears);
			echo "</pre>";	
		}
		
		
		if(count($this -> _MotorCycleYears) > 0) {
			$this -> _MotorCycleSearch = "ContinueMotorCycleDBGetBike";
		} else {
			$this -> _MotorCycleSearch = "StartBikezGetYear";
		}
	}


	
	//get year
	private function MotorCycleDBGetBike($manufactureText, $Model = array()) {
		
		$previousMatch = NULL;
		$stringPostionArray = array();
		foreach($this -> _MotorCycleYears as $bike) {
			$manufactureName = strtolower($manufactureText);
			$urlLowercase = strtolower($bike);
			$scrapedManufactureURL = str_replace('/'. $manufactureName . '_', "", $urlLowercase);
			
			if(LIVE_SITE == false) {
				echo "DatabaseModel: ". $Model['DatabaseModel'] . "<br />";
				echo "APIModel: ". $Model['APIModel'] . "<br />";
				echo "Series: ". $Model['Series'] . "<br />";
			
				//echo "Manufacture Name: " . str_replace("_", "", $manufactureName) . "<br />";
				echo "Lowercase URL: " . str_replace("_", "", $urlLowercase) . "<br />";
				echo "URL manufacutre scrapped: " . $scrapedManufactureURL . "<br />";	
			}
			
			
			
			$stringPositionValue = similar_text(str_replace("_", "", $scrapedManufactureURL), strtolower($Model['DatabaseModel'] . $Model['APIModel'].$Model['Series']));
			//$stringPositionValue = 0;
			
			array_push($stringPostionArray, array("StringPosition" => $stringPositionValue,
												  "Link" => $bike,
												  "ManipulatedValue" => str_replace("_", "", $scrapedManufactureURL),
												  "PassedThroughContent" => $Model['DatabaseModel'] . '/' . $Model['APIModel'] . '/' .$Model['Series']));
				
		}
		
		usort($stringPostionArray, function($a, $b) {
			return $a['StringPosition'] - $b['StringPosition'];
		});
		
		$LastArray = end($stringPostionArray);
		if(LIVE_SITE == false) {
			echo "Link Array";	
			echo "<pre>";
			print_r($stringPostionArray);
			echo "</pre>";	
			
			//echo "testing to see what returns: ".  $LastArray['StringPosition'];
		}
				
		
		
		
		if((int)$LastArray['StringPosition'] == 0) {
			$this -> _MotorCycleSearch = "StartBikezGetYear";			
		} else {
			$this -> _MotorCycleSearch = "ContinueMotorCycleDBGetBikeSpecs";
			$this -> _MotorCycleDBLink = $LastArray['Link'];
		}	
		
	}

	//get bike specs
	private function MotoryCycleDBGetBikeSpecs($vin) {
		echo "Getting Bike Specs On MotorCycle DB<br /><br />";	
			
		libxml_use_internal_errors(true);	
		$bikeUrlData = $this -> file_get_contents_curl("http://www.motorcycledb.com" . $this -> _MotorCycleDBLink);
		
		//parsing begins here:
		$doc = new DOMDocument();
		$doc->loadHTML(mb_convert_encoding($bikeUrlData, 'HTML-ENTITIES', 'UTF-8'));
		
		
		$xpath = new DOMXpath($doc);
		$result = $xpath->query('//table[@width="600"]');
		
		//specs table
		$specs = $doc -> saveXML($result->item(1));
		//echo $doc -> saveXML($result->item(1));
		
		$specHTML = new DOMDocument();
		$specHTML -> loadHTML(mb_convert_encoding($specs, 'HTML-ENTITIES', 'UTF-8'));
		
		//echo $specHTML -> saveHTML();
		
		$TRData = $specHTML->getElementsByTagName('tr');
		
		$SpecInfoFinal = array();
		//echo count($TRData);
		
		$FinalSpecsArray = array();
		$rulesList = new ConversionRulesList();
		libxml_use_internal_errors(false);
		
		foreach ($TRData as $key => $trSingle) {
			$trContent = explode(":", $trSingle -> textContent);
			
			if(count($trContent) > 1) {
				
				
				
				
				
				$LabelContent = NULL;
				
				$key = array_search($trContent[0], array_column($rulesList -> SpecLabelRules(), 'KeyWordFind'));
				
				if(LIVE_SITE == false) {
					echo "if Key Exists: " . $key. "<br />";	
				}
				
				
				$isMapped = NULL;
				if($key !== false) {
					$LabelContent = $rulesList -> SpecLabelRules()[$key]['labelText'];	
					echo "returned Value: " . $rulesList -> SpecLabelRules()[$key]['labelText'] . "<br />";	
					$isMapped = true;
				} else {
					$isMapped = false;
					$LabelContent = $trContent[0];	
					$specLabel = new SpecLabel();
					$specLabel -> labelText = $trContent[0];
					$specLabel -> SaveFromInventoryCheck();
					
					
					//$conversionType = new DataConversion();	
					//$conversionType -> ItemType = $specLabel -> GetNewSpecLabelID();
					
					//$conversionType -> KeyWordSearch = $trContent[0];
					
					
					//$conversionType -> NewSpecLabelConversion();
				}
				
				
				
					
				array_push($FinalSpecsArray, array("Label" => $LabelContent,
												   "Content" =>  $trContent[1],
												   "MappingLabelStats" => $isMapped));
			}
			
		}
		
		$this -> _NewBackupResponse = $FinalSpecsArray;
		
		$labelList = new SpecList();
		
		
		$specLabelContentString = array();
		
		foreach($labelList -> SpecLabelsByGroup() as $label) {
			$label = SpecLabel::WithID($label['specLabelID']);
			
			$finalArraykey = array_search($label -> labelText, array_column($FinalSpecsArray, 'Label'));
			
			if(LIVE_SITE == false) {
				//$key = array_search($label -> labelText, array_column($FinalSpecsArray, 'Label'));
				echo "database label text: " . $label -> labelText . "<br />";
				//echo "Returned Key: " . $key . "<br />";	
			}
				
			
			$ContentValue = '';
			
			if($finalArraykey !== false) {
				if(LIVE_SITE == false) {
					echo "<strong>it founded its key!!!!</strong><br />";	
				}
				
				$ContentValue = $FinalSpecsArray[$finalArraykey]['Content'];
			}
			
			
			
			array_push($specLabelContentString, array('LabelID' => $label-> labelID,
													  'Content' => $ContentValue));	
			
			
		}
		
		if(LIVE_SITE == false) {
			echo "Final Specs Array<pre>";
			print_r($FinalSpecsArray);
			echo "</pre>";			
		}

		$this -> _MotorCycleSearch = "MotorCycleDBSearchEnd";
		$this -> GenerateSpecs($vin, json_encode($specLabelContentString));		
		$this -> _backUpJSONresponse = json_encode($specLabelContentString);
	}



	private function BikezGetSpecs($vin) {
		libxml_use_internal_errors(true);	
		$bikeUrlData = $this -> file_get_contents_curl("http://www.bikez.com" . $this -> _bikezBikeLink);
		
		//parsing begins here:
		$doc = new DOMDocument();
		$doc->loadHTML(mb_convert_encoding($bikeUrlData, 'HTML-ENTITIES', 'UTF-8'));
		
		
		$xpath = new DOMXpath($doc);
		$result = $xpath->query('//table[@width="600"]');
		
		//specs table
		$specs = $doc -> saveXML($result->item(1));
		//echo $doc -> saveXML($result->item(1));
		
		$specHTML = new DOMDocument();
		$specHTML -> loadHTML(mb_convert_encoding($specs, 'HTML-ENTITIES', 'UTF-8'));
		
		//echo $specHTML -> saveHTML();
		
		$TRData = $specHTML->getElementsByTagName('tr');
		
		$SpecInfoFinal = array();
		
		libxml_use_internal_errors(false);
		
		$FinalSpecsArray = array();
		$rulesList = new ConversionRulesList();
		
		
		foreach ($TRData as $key => $trSingle) {
			$trContent = explode(":", $trSingle -> textContent);
			
			if(count($trContent) > 1) {
				
				$LabelContent = NULL;
				
				$key = array_search($trContent[0], array_column($rulesList -> SpecLabelRules(), 'KeyWordFind'));
				
				if(LIVE_SITE == false) {
					echo "if Key Exists: " . $key. "<br />";	
				}
				
				
				$isMapped = NULL;
				if($key !== false) {
					$isMapped = true;
					$LabelContent = $rulesList -> SpecLabelRules()[$key]['labelText'];	
					echo "returned Value: " . $rulesList -> SpecLabelRules()[$key]['labelText'] . "<br />";	
				} else {
					$isMapped = false;	
					$LabelContent = $trContent[0];	
					
					$specLabel = new SpecLabel();
					$specLabel -> labelText = $trContent[0];
					$specLabel -> SaveFromInventoryCheck();
					
					
					//$conversionType = new DataConversion();	
					//$conversionType -> ItemType = $specLabel -> GetNewSpecLabelID();
					
					//$conversionType -> KeyWordSearch = $trContent[0];
					
					
					//$conversionType -> NewSpecLabelConversion();
					
				}
				
				
				
					
				array_push($FinalSpecsArray, array("Label" => $LabelContent,
												   "Content" =>  $trContent[1],
												   "MappingLabelStats" => $isMapped));
			}
			
			
			
			
			
		}
		
		$this -> _NewBackupResponse = $FinalSpecsArray;
		
		$labelList = new SpecList();
		
		$specLabelContentString = array();
		
		foreach($labelList -> SpecLabelsByGroup() as $label) {
			$label = SpecLabel::WithID($label['specLabelID']);
			
			$finalArraykey = array_search($label -> labelText, array_column($FinalSpecsArray, 'Label'));
			
			if(LIVE_SITE == false) {
				echo "database label text: " . $label -> labelText . "<br />";	
			}
			
			$ContentValue = '';
			
			if($finalArraykey !== false) {
				if(LIVE_SITE == false) {
					echo "<strong>it founded its key!!!!</strong><br />";	
				}
				
				$ContentValue = $FinalSpecsArray[$finalArraykey]['Content'];
			}
			
			
			
			array_push($specLabelContentString, array('LabelID' => $label-> labelID,
													  'Content' => $ContentValue));	
			
			
		}
		
		if(LIVE_SITE == false) {
			echo "Final Bikez specs<br />";
			echo "<pre>";
			print_r($FinalSpecsArray);
			echo "</pre>";	
		}
		
		
		//echo "Successfull method call";
		
		$this -> _MotorCycleSearch = "BikezSearchEnd";
		$this -> GenerateSpecs($vin, json_encode($specLabelContentString));		
		$this -> _backUpJSONresponse = json_encode($specLabelContentString);
	}

	private function file_get_contents_curl($url) {
	    $ch = curl_init();
	
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	
	    $data = curl_exec($ch);
	    curl_close($ch);
	
	    return $data;
	}
	

	
	private function CreateBackup($vin, $year, $model, $manufacture) {
		$backup = new InventoryBackup();
		
		$backup -> inventoryBackupYear = $year;
		$backup -> inventoryBackupManufactur = $manufacture;
		$backup -> inventoryBackupModelName = $model;
		$backup -> inventoryBackupModelFriendlyName = NULL;
		
		if(!empty($this -> _backUpJSONresponse)) {
			$backup -> inventoryBackupSpecJSON = $this -> _backUpJSONresponse;	
		} else {
			
			
			$labels = new SpecList();
			$emptySpecString = array();
								
			foreach($labels -> SpecLabelsByGroup() as $label) {
				array_push($emptySpecString, array('LabelID' => $label['specLabelID'],
												   'Content' => ''));
					
			}
			
			$backup -> inventoryBackupSpecJSON = json_encode($emptySpecString);
		}
		
		
		$backup -> inventoryBackupVinNumber = $vin;
		
		$backup -> Category = $this -> NewBackedUpCategory;
		
		$backup -> SaveBackup();
		
		$this -> _NewBackUpID = $backup -> GetNewBackupID();
	}


}