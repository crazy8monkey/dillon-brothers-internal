<?php

class SpecGroup extends BaseObject {
	
	private $_id;
	
	public $groupID;
	public $SpecGroupText;
	
	public $labelIDS;
	public $Visible;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($groupID) {
        $instance = new self();
        $instance->_id = $groupID;
        $instance->loadById();
        return $instance;
    }
	
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM specgroups WHERE specGroupID = :specGroupID');
        $sth->execute(array(':specGroupID' => $this->_id));	
        $record = $sth -> fetch();
		$this->fill($record);
	}
	
	
    protected function fill(array $row){
    	$this -> SpecGroupText = $row['specGroupText'];
		$this -> Visible = $row['GroupVisible'];
    }	
	
	public function GetID() {
		return $this -> _id;
	}
	
	
	public function Validate() {
		$validationErrors = array();
		
		
		
		if(!isset($this->_id)) {
            $groupTextCheck = $this->db->prepare('SELECT specGroupText FROM specgroups WHERE specGroupText = ?');
            $groupTextCheck->execute(array($this->SpecGroupText));
        } else {
            $groupTextCheck = $this->db->prepare('SELECT * FROM specgroups WHERE specGroupText = :specGroupText and specGroupID <> :specGroupID');
            $groupTextCheck->execute(array(':specGroupID' => $this->_id, ':specGroupText' => $this->SpecGroupText));
        }
		

		if($this -> validate -> emptyInput($this -> SpecGroupText)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		} else if(count($groupTextCheck -> fetchAll())) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'There is a group entered with this same text, please enter a new group'));
		}

		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	
	
	public function Save() {
		$groupID = NULL;
		$isNewGroup = False;	
		if(isset($this -> _id)) {
			$groupID = $this -> _id;
			
			$postData = array('GroupVisible' => $this -> Visible,
							  'specGroupText' => $this -> SpecGroupText);
				
			$this->db->update('specgroups', $postData, array('specGroupID' => $this->_id));
			
			if(!empty($this -> labelIDS)) {
				$count = 1;
				foreach($this -> labelIDS as $labelID) {
					$label = SpecLabel::WithID($labelID);	
					$label -> newOrder = $count;
					$label -> UpdateOrder();
					$count++;	
				}				
			}

			
			
		} else {
			$groupID = $this -> db -> insert('specgroups', array('specGroupText' => $this -> SpecGroupText,
																 'GroupVisible' => $this -> Visible));
		}
					
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'settings/specifications');
										   					
	}
	
	public function Delete() {
		$sth = $this -> db -> prepare("DELETE FROM specgroups WHERE specGroupID = :specGroupID");
		$sth -> execute(array(':specGroupID' => $this -> _id));	
		
		$labels = $this -> db -> prepare("DELETE FROM speclabels WHERE relatedSpecGroupID = :relatedSpecGroupID");
		$labels -> execute(array(':relatedSpecGroupID' => $this -> _id));		
			
		$this -> redirect -> redirectPage(PATH. 'settings/specifications');
	}
	
	public function reorderGroups() {
		
		$count = 1;
		foreach ($this -> groupID as $value) {
			$postData = array('GroupOrder' => $count);
				
			$this->db->update('specgroups', $postData, array('specGroupID' => $value));			
			$count++;
		}
		
		$this -> json -> outputJqueryJSONObject('groupOrderSaved', true);
		
	}
	


}