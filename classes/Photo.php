<?php


class Photo extends BaseObject {
	
	private $_id;
	
	public $photoName;
	public $fileExt;

	public $visibility;
	public $AlbumID;
	public $albumName;
	public $parentDirectory;
	private $_uploadedByFirstName;
	private $_uploadedByLastName;
	public $updatedDate;
	public $updatedTime;
	
	public $altText;
	public $titleText;
	
	public $AlbumType;
	
	public $CurrentImageName;
	
	public $_albumFolderName;
	
	public $uploadedImage;
	public $uploadedTempImage;
	public $BlogPostID;
	
	public $UploadedByID;
	
	public $CurrentUploadedCount;
	
	private $PhotoAlbumObject = array();
	
	private $latestPhotoUpload;
	
	public $rotation;
	public $PhotoWaterMark;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($photoID) {
        $instance = new self();
        $instance->_id = $photoID;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM photos INNER JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID 
    														INNER JOIN users ON photos.uploadedBy = users.userID 
    														LEFT JOIN blogposts ON photoalbums.albumBlogID = blogposts.blogPostID WHERE photoID = :photoID');
        $sth->execute(array(':photoID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> photoName = $row['photoName'];
		$this -> CurrentImageName = $row['photoName'];
		$this -> fileExt = $row['ext'];
		$this -> AlbumID = $row['photoAlbumID'];
		$this -> albumName = $row['albumName'];
		$this -> parentDirectory = $row['ParentDirectory'];
		$this -> _uploadedByFirstName = $row['firstName'];
		$this -> _uploadedByLastName = $row['lastName'];
		$this -> updatedDate = $row['date'];
		$this -> updatedTime = $row['time'];
		$this -> altText = $row['altText'];
		$this -> titleText = $row['title'];
		$this -> AlbumType = $row['AlbumType'];
		$this -> _albumFolderName = $row['albumFolderName'];
		$this -> BlogPostID = $row['blogPostID'];
		$this -> PhotoWaterMark = $row['photowatermark'];
    }
	
	public function GetPhotoID() {
		return $this->_id;
	}
	
	public function GetImageName() {
		return $this -> CurrentImageName . '.' . $this-> fileExt;
	}

	public function GetImageNameSmall() {
		return $this -> CurrentImageName . '-s.' . $this-> fileExt;
	}

	public function GetImageNameMedium() {
		return $this -> CurrentImageName . '-m.' . $this-> fileExt;
	}	

	public function GetImageNameLarge() {
		return $this -> CurrentImageName . '-l.' . $this-> fileExt;
	}
	
	public function GetUploadedBy() {
		return $this -> _uploadedByFirstName . ' ' . $this -> _uploadedByLastName;
	}

	private function GetAlbumParentPath() {
		switch($this -> AlbumType) {
			case 1:
				$path = "/events/";
				break;
			case 2:
				$path = "/blog/";
				break;
			case 3:
				$path = "/misc/";
				break;
		}
		
		return $path;
		
	}

	public function GetPhotoAlbum() {
		$currentAlbum = $this -> db -> prepare("SELECT * FROM photoalbums WHERE albumName = :albumName AND albumBlogID <> 0");
		$currentAlbum -> execute(array(":albumName" => $this -> albumName));
		$getCurrentAlbum = $currentAlbum -> fetch();		
		
		$this -> AlbumID = $getCurrentAlbum['albumID'];
		$this -> PhotoAlbumObject = $getCurrentAlbum;
	}
	
	public function ChangeVisiblity() {
		$postData = array('visible' => $this -> visibility);		
		$this->db->update('photos', $postData, array('photoID' => $this -> _id));		
	}
	
	public function SavePhoto() {
		$Directory = NULL;
		
		$ImagePathParts = array();
		$uploadLink = NULL;
		
		
		
		switch($this -> AlbumType) {
			case 1:
				$uploadLink = PHOTO_PATH . $this -> parentDirectory . '/events/'. $this -> _albumFolderName . '/';	
				$Directory = '/events/';
				break;	
			case 2:
				$uploadLink = PHOTO_PATH . $this -> parentDirectory . '/blog/'. $this -> _albumFolderName . '/';		
				$Directory = "/blog/";
				break;
			case 3:
				$uploadLink = PHOTO_PATH . $this -> parentDirectory . '/misc/'. $this -> _albumFolderName . '/';		
				$Directory = "/misc/";
				break;
		}
		
		
		
		$uploadedPATH = $uploadLink . $this -> uploadedImage;
		
		
		
		
		$ImagePathParts['Year'] = $this -> parentDirectory;
		$ImagePathParts['type'] = $Directory;
		$ImagePathParts['AlbumName'] = $this -> _albumFolderName;
		
		Image::MoveAndRenameImage($this -> uploadedTempImage, 
								  $ImagePathParts,
								  $this -> uploadedImage,
								  parent::seoUrl($this -> photoName), 
								  1);
		
		
		
		$newImageNameResizedSmall = parent::seoUrl($this -> photoName) . '-s.' . $this -> fileExt;
		$newImageNameResizedMedium = parent::seoUrl($this -> photoName) . '-m.' . $this -> fileExt;
		$newImageNameResizedLarge = parent::seoUrl($this -> photoName) . '-l.' . $this -> fileExt;
		
		$currentPhotoUploaded = $uploadLink . parent::seoUrl($this -> photoName) . '.' . $this -> fileExt;
		
		list($origWidth, $origHeight) = getimagesize($currentPhotoUploaded);
		
		//small
		if(500 > $origWidth) {
			Image::resizeImage($currentPhotoUploaded, $uploadLink . $newImageNameResizedSmall, $origWidth, $origHeight);
		} else {
			Image::resizeImage($currentPhotoUploaded, $uploadLink . $newImageNameResizedSmall, 500, 500);	
		}
		
		
		//medium
		if(1000 > $origWidth) {
			Image::resizeImage($currentPhotoUploaded, $uploadLink . $newImageNameResizedMedium, $origWidth, $origHeight);			
		} else {
			Image::resizeImage($currentPhotoUploaded, $uploadLink . $newImageNameResizedMedium, 1000, 1000);	
		}
			
		//large
		if(2000 > $origWidth) {
			Image::resizeImage($currentPhotoUploaded, $uploadLink . $newImageNameResizedLarge, $origWidth, $origHeight);
		} else {
			Image::resizeImage($currentPhotoUploaded, $uploadLink . $newImageNameResizedLarge, 2000, 2000);	
		}
		
				
		unlink($currentPhotoUploaded);
		
		$this -> latestPhotoUpload = $this -> db -> insert('photos', array('photoName' => parent::seoUrl($this -> photoName), 
											  'ext' => $this -> fileExt,
											  'altText' => $this -> altText,
											  'title' => $this -> titleText,
											  'photoAlbumID' => $this -> AlbumID, 
											  'date' => date("Y-m-d", $this -> time -> NebraskaTime()),
											  'time' => date("H:i:s", $this -> time -> NebraskaTime()),
											  'uploadedBy' => $this -> UploadedByID));						  
													  
	}
	
	
	public function GetLatestPhotoID() {
		return $this -> latestPhotoUpload;
	}
	
	public function Validate() {
		$validationErrors = array();
		
		
		//$this -> AlbumID
		$photoNameCheck = $this->db->prepare('SELECT * FROM photos INNER JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE photoName = :photoName AND photoAlbumID = :photoAlbumID AND photoID <> :photoID');
        $photoNameCheck->execute(array(":photoName" => $this -> photoName, ":photoAlbumID" => $this -> AlbumID, ":photoID" => $this->_id));
		
		
		if($this -> validate -> emptyInput($this -> photoName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if(count($photoNameCheck -> fetchAll())) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Photo name already exists in album'));
		}
		
		if($this -> validate -> emptyInput($this -> altText)) {
			array_push($validationErrors, array('inputID' => 2,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> titleText)) {
			array_push($validationErrors, array('inputID' => 3,
												'errorMessage' => 'Required'));
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Save() {
		switch($this -> AlbumType) {
			case 1:
				$path = "/events/";
				break;
			case 2:
				$path = "/blog/";
				break;
			case 3:
				$path = "/misc/";
				break;
		}
		
		$uploadedAlbumPATH = PHOTO_PATH . $this -> parentDirectory. $path . $this -> _albumFolderName . '/';
		
		$uploadedPATHNormal = $uploadedAlbumPATH . $this -> GetImageName();
		$uploadedPATHSmall = $uploadedAlbumPATH . $this -> GetImageNameSmall();
		$uploadedPATHMedium = $uploadedAlbumPATH . $this -> GetImageNameMedium();
		$uploadedPATHLarge = $uploadedAlbumPATH . $this -> GetImageNameLarge();
		
		//small
		Image::renameImage($uploadedAlbumPATH, $uploadedPATHSmall, parent::seoUrl($this -> photoName). '-s');
		
		//medium
		Image::renameImage($uploadedAlbumPATH, $uploadedPATHMedium, parent::seoUrl($this -> photoName). '-m');
		
		//large
		Image::renameImage($uploadedAlbumPATH, $uploadedPATHLarge, parent::seoUrl($this -> photoName). '-l');
		
		
		$postData = array('photoName' => parent::seoUrl($this -> photoName),
						  'altText' => $this -> altText,
						  'title' => $this -> titleText);		
		$this->db->update('photos', $postData, array('photoID' => $this -> _id));
		
		$this -> json -> outputJqueryJSONObject('success', array("msg" => "Photo Saved",
																 "NewName" => parent::seoUrl($this -> photoName),
																 "fileExt" => $this -> fileExt));	
																 
		if($this -> AlbumType == 2) {
			$blogPost = BlogPost::WithID($this -> BlogPostID);
			$blogPost -> CurrentViewedPhoto = $this -> CurrentImageName . '-l.' .$this -> fileExt;
			$blogPost -> NewPhotoName = parent::seoUrl($this -> photoName) . '-l.' . $this -> fileExt;
			
			$blogPost -> NewPhotoAltText = $this -> altText;
			$blogPost -> NewPhotoTitleText = $this -> titleText;
			
			$blogPost -> UpdateBlogPostContentImages();
		}														 
	}
	
	
	
	
	public function CreateSinglePhotoBreadCrumbs() {
		$album = PhotoAlbum::WithID($this -> AlbumID);
		
		return $album -> PhotosLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $album -> YearLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $album -> TypePhotosCategory() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $album -> AlbumLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>View Photo:<span id='photoSingleName'>" . $this -> photoName . "</span>";
	}
	
	public function RotateImage() {
		
		switch($this -> AlbumType) {
			case 1:
				$path = "/events/";
				break;
			case 2:
				$path = "/blog/";
				break;
			case 3:
				$path = "/misc/";
				break;
		}
		
		$imageRotatedLarge = PHOTO_PATH . $this -> parentDirectory . $path . $this -> _albumFolderName . '/' . $this -> CurrentImageName . '-l.' . $this -> fileExt;
		$imageRotatedMedium = PHOTO_PATH . $this -> parentDirectory . $path . $this -> _albumFolderName . '/' . $this -> CurrentImageName . '-m.' . $this -> fileExt;
		$imageRotatedSmall = PHOTO_PATH . $this -> parentDirectory . $path . $this -> _albumFolderName . '/' . $this -> CurrentImageName . '-s.' . $this -> fileExt;
		
		
		if(strtolower($this -> fileExt) == "gif") { 
    		$sourceLarge = imagecreatefromgif($imageRotatedLarge);
			$sourceMedium = imagecreatefromgif($imageRotatedLarge);
			$sourceSmall = imagecreatefromgif($imageRotatedSmall);
			
    	} else if(strtolower($this -> fileExt) == "png") { 
    		$sourceLarge = imagecreatefrompng($imageRotatedLarge);
			$sourceMedium = imagecreatefrompng($imageRotatedLarge);
			$sourceSmall = imagecreatefrompng($imageRotatedSmall);
    	
		} else { 
	    	$sourceLarge = imagecreatefromjpeg($imageRotatedLarge);
			$sourceMedium = imagecreatefromjpeg($imageRotatedLarge);
			$sourceSmall = imagecreatefromjpeg($imageRotatedSmall);
    	}
		
		
		
		
		
		switch($this -> rotation) {
			case "right":
				$rotateLarge = imagerotate($sourceLarge, -90, 0);
				$rotateMedium = imagerotate($sourceMedium, -90, 0);		
				$rotateSmall = imagerotate($sourceSmall, -90, 0);	
				break;
			case "left":
				$rotateLarge = imagerotate($sourceLarge, 90, 0);	
				$rotateMedium = imagerotate($sourceMedium, 90, 0);	
				$rotateSmall = imagerotate($sourceSmall, -90, 0);	
				break;
		}
		
		
		
    	
		
		if(strtolower($this -> fileExt) == "gif") {
			imagegif($rotateSmall, $imageRotatedLarge);	
			imagegif($rotateSmall, $imageRotatedMedium); 
			imagegif($rotateSmall, $imageRotatedSmall);
		} else if(strtolower($this -> fileExt) == "png") {
			imagepng($rotateSmall, $imageRotatedLarge);	
			imagepng($rotateSmall, $imageRotatedMedium);  
    	    imagepng($rotateMedium, $imageRotatedSmall);
    	} else {
    		imagejpeg($rotateSmall, $imageRotatedLarge, 100);	
			imagejpeg($rotateSmall, $imageRotatedMedium, 100); 
			imagejpeg($rotateSmall, $imageRotatedSmall, 100); 
    	}	
		
		imagedestroy($sourceLarge);
		imagedestroy($rotateLarge);
		
		imagedestroy($sourceMedium);
		imagedestroy($rotateMedium);
		
		imagedestroy($sourceSmall);
		imagedestroy($rotateSmall);
			
		
		$this -> redirect -> redirectPage(PATH . 'photos/view/photo/' . $this -> _id);
	}

	public function WaterMarkPhoto() {
		//large
		$this -> SingleWaterMarkPhoto(PHOTO_PATH . $this -> parentDirectory . $this -> GetAlbumParentPath() . $this -> _albumFolderName . '/' . $this -> CurrentImageName . '-l.' . $this -> fileExt, $this -> fileExt, $this -> CurrentImageName . '-l.' . $this -> fileExt);
		//medium
		$this -> SingleWaterMarkPhoto(PHOTO_PATH . $this -> parentDirectory . $this -> GetAlbumParentPath() . $this -> _albumFolderName . '/' . $this -> CurrentImageName . '-m.' . $this -> fileExt, $this -> fileExt, $this -> CurrentImageName . '-m.' . $this -> fileExt);
		//small
		$this -> SingleWaterMarkPhoto(PHOTO_PATH . $this -> parentDirectory . $this -> GetAlbumParentPath() . $this -> _albumFolderName . '/' . $this -> CurrentImageName . '-s.' . $this -> fileExt, $this -> fileExt, $this -> CurrentImageName . '-s.' . $this -> fileExt);
		
		$postData = array('photowatermark' => 1);		
		$this->db->update('photos', $postData, array('photoID' => $this -> _id));
		
		$this -> redirect -> redirectPage(PATH . 'photos/view/photo/' . $this -> _id);
	}

	private function SingleWaterMarkPhoto($currentImage, $ext, $newImage) {
		
		//echo $settingsObj -> WaterMarkImage;
		$waterMark = imagecreatefrompng(PHOTO_PATH . 'WaterMark/OveralWaterMark.png');
		
		if(strtolower($ext) == "gif") { 
			$image = imagecreatefromgif($currentImage);
    	} else if(strtolower($ext) == "png") { 
    		$image = imagecreatefrompng($currentImage);
		} else {
			$image = imagecreatefromjpeg($currentImage); 
    	}
		
		
		
		$marge_right = 0;
		$marge_bottom = 0;
		$sx = imagesx($waterMark);
		$sy = imagesy($waterMark);
		$sximg = imagesx($image);
		
		$percent = $sximg * 0.15;
		
		//Create the final resized watermark stamp
	    $dest_image = imagecreatetruecolor($percent, $percent);
		
	    //keeps the transparency of the picture
	    imagealphablending( $dest_image, false );
	    imagesavealpha( $dest_image, true );
	    
	    //resizes the stamp
	    imagecopyresampled($dest_image, $waterMark, 0, 0, 0, 0, $percent, $percent, $sx, $sy);
		
		imagecopy($image, 
				  $dest_image, 
				  imagesx($image) - imagesx($dest_image) - $marge_right, 
				  imagesy($image) - imagesy($dest_image) - $marge_bottom, 
				  0, 
				  0, 
				  $percent, 
				  $percent);
		
		if(strtolower($ext) == "gif") {
			imagegif($image, PHOTO_PATH . $this -> parentDirectory . $this -> GetAlbumParentPath(). $this -> _albumFolderName . '/' . $newImage);	
		} else if(strtolower($ext) == "png") {
			imagepng($image, PHOTO_PATH . $this -> parentDirectory . $this -> GetAlbumParentPath(). $this -> _albumFolderName . '/' . $newImage);	
    	} else {
    		imagejpeg($image, PHOTO_PATH . $this -> parentDirectory . $this -> GetAlbumParentPath(). $this -> _albumFolderName . '/' . $newImage); 
    	}	
		
		
		
	}


	
	public function delete() {
		$path = NULL;	
		switch($this -> AlbumType) {
			case 1:
				$path = "/events/";
				break;
			case 2:
				$path = "/blog/";
				break;
			case 3:
				$path = "/misc/";
				break;
		}
		
		
		//small
		unlink(PHOTO_PATH . $this -> parentDirectory . $path . $this -> _albumFolderName . '/'. $this -> GetImageNameSmall());
		//medium
		unlink(PHOTO_PATH . $this -> parentDirectory . $path . $this -> _albumFolderName . '/'. $this -> GetImageNameMedium());
		//large		
		unlink(PHOTO_PATH . $this -> parentDirectory . $path . $this -> _albumFolderName . '/'. $this -> GetImageNameLarge());
				
		$sth = $this -> db -> prepare("DELETE FROM photos WHERE photoID = :photoID");
		$sth -> execute(array(':photoID' => $this->_id));
		
		$this -> redirect -> redirectPage($this -> pages -> photos() . '/view/album/' . $this->AlbumID);	
	}
	
	

}