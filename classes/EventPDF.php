<?php


class EventPDF extends BaseObject {
	
	private $_id;
	private $year;
	public $PDF;
	private $NotificationUploadText;
	private $UploadLabel;

	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithCurrentYear($year) {
        $instance = new self();
        $instance-> year = $year;
        $instance->loadByYear();
        return $instance;
    }
		
	protected function loadByYear() {
    	$sth = $this -> db -> prepare('SELECT * FROM eventpdflist WHERE eventYearList = :eventYearList');
        $sth->execute(array(':eventYearList' => $this->year));
        if($sth->rowCount() > 0) {
			$this -> NotificationUploadText = "Current Event PDF Year is uploaded, upload new to replace curren year";
			$this -> UploadLabel = "Upload / Replace Event list PDF";
		} else {
			$this -> NotificationUploadText = "You need to Upload this years events list pdf";
			$this -> UploadLabel = "Upload New Event list PDF";
		}
		
		
    }

    public function GetUploadNotificaitonText() {
    	return $this -> NotificationUploadText;
    }

	public function GetUploadLabel() {
		return $this -> UploadLabel;
	}	
	
	
	public function Validate() {
		$validationErrors = array();
		
		
		if($this -> validate -> emptyInput($this -> PDF)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		}
		
						
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	
	
	
	private function uploadPDF($newImageName) {
		$ImagePathParts = array();			
		$imageUploadType = NULL;	
			
		$imageUploadType = 9;
					
		Image::MoveAndRenameImage($_FILES['eventYearPDF']['tmp_name'], 
								  $ImagePathParts,
								  $this -> PDF,
								  $newImageName, $imageUploadType);
	}
	
	
	public function Save() {
		try {
			
			//Dillon-Brothers-Events-2017-Print
			$NewPDFSave = true;
			$newFileName = "Dillon-Brothers-Events-" . date("Y", $this -> time -> NebraskaTime()) . "-Print";
			
			$this -> uploadPDF($newFileName);
			
			$yearCheck = $this -> db -> prepare('SELECT * FROM eventpdflist WHERE eventYearList = :eventYearList');
			$yearCheck -> execute(array(':eventYearList' => date("Y", $this -> time -> NebraskaTime())));
			
			
			if($yearCheck->rowCount() > 0) {
				$NewPDFSave = false;
			} 
			
			
			if($NewPDFSave == true) {
				$this -> db -> insert('eventpdflist', array('eventYearList' => date("Y", $this -> time -> NebraskaTime()),
													  		'eventYearListName' => $newFileName . '.' . Image::getFileExt($this -> PDF)));	
			}
		
			
			
			
			$this -> json -> outputJqueryJSONObject('success', date("Y", $this -> time -> NebraskaTime()));
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Save Event PDF Error: " . $e->getMessage();
			$TrackError -> type = "SAVE EVENT PDF SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}
	
	


		

}