<?php

class VehicleSpecial extends BaseObject {
	
	
	private $_id;
	
	public $category;
	public $type;
	public $TypeValue;
	public $VehicleName;
	public $endDate;
	public $description;
	public $disclaimer;
	
	
	public $categorySingle;
	public $typeSingle;
	public $TypeValueSingle;
	public $VehicleNameSingle;
	public $endDateSingle;
	public $descriptionSingle;
	public $disclaimerSingle;
	
	
	public $specialID;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($specialID) {
        $instance = new self();
        $instance->_id = $specialID;
        $instance->loadById();
        return $instance;
    }
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM vehiclespecials WHERE vehicleID = :vehicleID');
        $sth->execute(array(':vehicleID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
    protected function fill(array $row){
		$this -> categorySingle = $row['VehicleCategory'];
		$this -> typeSingle = $row['SpecialType'];
		$this -> TypeValueSingle = $row['SpecialTypeValue'];
		$this -> VehicleNameSingle = $row['VehicleName'];
		$this -> endDateSingle = $row['ExpiredDate'];
		$this -> descriptionSingle = $row['Description'];
		$this -> disclaimerSingle = $row['Disclaimer'];
		$this -> specialID = $row['MainSpecialID'];
    }	
	
	public function GetID() {
		return $this -> _id;
	}
	
	public function GetEditVehicleIncentiveBreadCrumbs() {
		$special = Special::WithID($this -> specialID);
		return $special -> GetEditVehicleIncentiveBreadCrumbs(true);
	}

	public function Validate() {
		$validationErrors = array();
		
		if(isset($this -> _id)) {
			
			if($this -> validate -> emptyInput($this -> categorySingle)) {
				array_push($validationErrors, array("inputID" => 1,
													'errorMessage' => 'Required'));
			}

			if($this -> validate -> emptyInput($this -> typeSingle)) {
				array_push($validationErrors, array("inputID" => 2,
													'errorMessage' => 'Required'));
			}

			if($this -> validate -> emptyInput($this -> TypeValueSingle)) {
				array_push($validationErrors, array("inputID" => 3,
													'errorMessage' => 'Required'));
			}

			if($this -> validate -> emptyInput($this -> VehicleNameSingle)) {
				array_push($validationErrors, array("inputID" => 4,
													'errorMessage' => 'Required'));
			}

			if($this -> validate -> emptyInput($this -> endDateSingle)) {
				array_push($validationErrors, array("inputID" => 5,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> descriptionSingle)) {
				array_push($validationErrors, array("inputID" => 6,
													'errorMessage' => 'Required'));
			}			

			if($this -> validate -> emptyInput($this -> disclaimerSingle)) {
				array_push($validationErrors, array("inputID" => 7,
													'errorMessage' => 'Required'));
			}			
				
		} else {
			
			
			if($this -> ValidateEmptyInputs($this -> category)) {
				array_push($validationErrors, array('inputID' => 1,
													'errorMessage' => 'There are empty Categories fields that need to be filled out'));
			}
	
			if($this -> ValidateEmptyInputs($this -> type)) {
				array_push($validationErrors, array('inputID' => 2,
													'errorMessage' => 'There are empty Type fields that need to be filled out'));
			}
	
			if($this -> ValidateEmptyInputs($this -> TypeValue)) {
				array_push($validationErrors, array('inputID' => 3,
													'errorMessage' => 'There are empty Type values that need to be filled out'));
			}
	
			if($this -> ValidateEmptyInputs($this -> VehicleName)) {
				array_push($validationErrors, array('inputID' => 4,
													'errorMessage' => 'There are empty Vehicle Names that need to be filled out'));
			}
			
			if($this -> ValidateEmptyInputs($this -> endDate)) {
				array_push($validationErrors, array('inputID' => 5,
													'errorMessage' => 'There are empty Expired Dates that need to be filled out'));
			}
	
			if($this -> ValidateEmptyInputs($this -> description)) {
				array_push($validationErrors, array('inputID' => 6,
													'errorMessage' => 'There are empty Description(s) that need to be filled out'));
			}
	
			if($this -> ValidateEmptyInputs($this -> disclaimer)) {
				array_push($validationErrors, array('inputID' => 7,
													'errorMessage' => 'There are empty Disclaimer(s) that need to be filled out'));
			}		
			
			
					
		}
		
		
		if (empty($validationErrors)) {
				return true;
			} else {
				$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
				return false;
			}

		
		
		
		
		
	}
	
	
	
	
	private function ValidateEmptyInputs($inputs) {
		$ReturnValue = NULL;
		foreach ($inputs as $key => $value) {
			if(empty($inputs[$key])) {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}
	
	
	
	
	

	
	public function Save() {
		try {
			$specialIDRedirect = NULL;
			$brandValue = NULL;
			
			if(isset($this -> _id)) {				
				
				$postData = array('VehicleCategory' => $this -> categorySingle,
									  'SpecialType' => $this -> typeSingle,
									  'SpecialTypeValue' => $this -> TypeValueSingle,
									  'VehicleName' => $this -> VehicleNameSingle,
									  'ExpiredDate' => $this -> endDateSingle,
									  'Description' => $this -> descriptionSingle,
									  'Disclaimer' => $this -> disclaimerSingle);
									  
				$this->db->update('vehiclespecials', $postData, array('vehicleID' => $this -> _id));	
				
				$currentSpecial = Special::WithID($this -> specialID);
				$currentSpecial -> NewExpiredDate = $this -> endDateSingle;
				$currentSpecial -> UpdateExpiredDate();					  
				
			} else {
					
				foreach ($this -> category as $key => $value) {
					$this -> db -> insert('vehiclespecials', array('VehicleCategory' => $this -> category[$key],
														    'SpecialType' => $this -> type[$key],
														    'SpecialTypeValue' => $this -> TypeValue[$key],
														    'VehicleName' => $this -> VehicleName[$key],
														    'ExpiredDate' => $this -> endDate[$key],
														    'Description' => $this -> description[$key],
														    'Disclaimer' => $this -> disclaimer[$key],
														    'MainSpecialID' => $this -> specialID));
															
					$currentSpecial = Special::WithID($this -> specialID);
					$currentSpecial -> NewExpiredDate = $this -> endDate[$key];
					$currentSpecial -> UpdateExpiredDate();										
				}				  				
			}
			
			
			
			$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> specials() . '/edit/VehicleSpecial/'. $this -> specialID);
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Employee Save Error: " . $e->getMessage();
			$TrackError -> type = "EMPLOYEE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}

	public function delete() {
		try {
			
			$currentSpecial = Special::WithID($this -> specialID);
			$currentSpecial -> ResetExpiredDate();
			
			
			$sth = $this -> db -> prepare("DELETE FROM vehiclespecials WHERE vehicleID = :vehicleID");
			$sth -> execute(array(':vehicleID' => $this->_id));
				
			$currentVehicleIncentivesList = new VehicleSpecialsList();
			if(count($currentVehicleIncentivesList -> VehiclesBySpecial($this -> specialID)) > 0) {
				foreach ($currentVehicleIncentivesList -> VehiclesBySpecial($this -> specialID) as $value) {
					$currentSpecial = Special::WithID($this -> specialID);
					$currentSpecial -> NewExpiredDate = $value['ExpiredDate'];
					$currentSpecial -> UpdateExpiredDate();
				}
			}
				
			$this -> redirect -> redirectPage($this -> pages -> specials() . '/edit/VehicleSpecial/' . $this -> specialID);
			
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Vehicle Incentive Delete Error: " . $e->getMessage();
			$TrackError -> type = "VEHICLE INCENTIVE DELETE ERROR";
			$TrackError -> SendMessage();
		}	
	}
	
	public function DeleteIncentives() {
		$sth = $this -> db -> prepare("DELETE FROM vehiclespecials WHERE MainSpecialID = :MainSpecialID");
		$sth -> execute(array(':MainSpecialID' => $this -> specialID));
	}



}