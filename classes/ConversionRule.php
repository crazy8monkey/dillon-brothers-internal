<?php

class ConversionRule extends BaseObject {
	
	private $_id;
	
	public $dataConversionID;
	public $Action;
	public $keyWord;	
	
	public $CurrentColorText;
	public $colorIDS;
	
	public $CurrentManufacture;
	public $NewManufacture;
	
	public $NewColorText;
		
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($ruleID) {
        $instance = new self();
        $instance->_id = $ruleID;
        $instance->loadById();
        return $instance;
    }

	
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM conversionrules WHERE ruleID = :ruleID');
        $sth->execute(array(':ruleID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	
	
    protected function fill(array $row){
    	$this -> dataConversionID = $row['relatedDataConversionID'];
		$this -> CurrentColorText = $row['CurrentColorText'];
		$this -> NewColorText = $row['NewColorText'];
    }	
	
	public function GetID() {
		return $this -> _id;
	}
	
	
	public function Validate() {
		$validationErrors = array();
		
		

		if(!isset($this -> colorIDS)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Please Select a Color'));
		}
		
		

		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	
	
	private function UpdateManufactureInventory($currentText, $newText) {
		$currentInventory = new CurrentInventoryList();		
		foreach($currentInventory -> CheckInventoryManufacture($currentText) as $cleanManufactureText) {			
			$currentInventoryObject = InventoryObject::WithID($cleanManufactureText['inventoryID']);	
			$currentInventoryObject -> NewCleanManufacture = $newText;
			$currentInventoryObject -> UpdateManufacture();
		}
	}
	
	private function UpdateManufactureBackup($currentText, $newText) {
		$backupList = new BackupInventoryList();
		foreach($backupList -> CheckInventoryManufacture($currentText) as $cleanManufactureText) {			
			$backupInventory = InventoryBackup::WithID($cleanManufactureText['inventoryBackupID']);	
			$backupInventory -> NewManufactureText = $newText;
			$backupInventory -> UpdateManufacture();
		}
		
	}
	
	
	public function Save() {
		$redirectID = NULL;
		if(isset($this->_id)) {
				
			if(!empty($this -> CurrentManufacture)) {
				$postData = array('CurrentManufactureText' => $this -> CurrentManufacture, 
								  'NewManufacturerText' => $this -> NewManufacture);
								  
				$this -> UpdateManufactureInventory($this -> CurrentManufacture, $this -> NewManufacture);
				$this -> UpdateManufactureBackup($this -> CurrentManufacture, $this -> NewManufacture);
								  	
			} else {
				$postData = array('ActionMethod' => $this -> Action, 
								  'KeyWordFind' => $this -> keyWord,
								  'CurrentColorText' => $this -> CurrentColorText);	
			}
								 
			
			
			$this->db->update('conversionrules', $postData, array('ruleID' => $this -> _id));
			
			
		} else {
			if(!empty($this -> CurrentManufacture)) {
				$this -> db -> insert('conversionrules', array('relatedDataConversionID' => $this -> dataConversionID,
															   'CurrentManufactureText' => $this -> CurrentManufacture,
															   'NewManufacturerText' => $this -> NewManufacture));
				
				$this -> UpdateManufactureInventory($this -> CurrentManufacture, $this -> NewManufacture); 
				$this -> UpdateManufactureBackup($this -> CurrentManufacture, $this -> NewManufacture);													   
															   
			} else {
				$this -> db -> insert('conversionrules', array('relatedDataConversionID' => $this -> dataConversionID,
															   'ActionMethod' => $this -> Action,
															   'KeyWordFind' => $this -> keyWord,
															   'CurrentColorText' => $this -> CurrentColorText));	
			}
			
												   
		}
		
		
	}


	
	
	private function ValidateConversionRules($type) {
		$ReturnValue = NULL;
		foreach ($type as $key => $value) {
			if($value != '0') {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}
	
	
	
	public function CleanManufactureText() {
		$currentInventory = new CurrentInventoryList();
		$totalManufactureChangeRules = new ConversionRulesList();
		
		foreach($currentInventory -> VehicleInfoFoundInventory() as $cleanManufactureText) {
			
			$key = array_search($cleanManufactureText['Manufacturer'], array_column($totalManufactureChangeRules -> ManufactureChangesRules(), 'CurrentManufactureText'));
			if($key !== false) {
				$currentInventoryObject = InventoryObject::WithID($cleanManufactureText['inventoryID']);	
				$currentInventoryObject -> NewCleanManufacture = $totalManufactureChangeRules -> ManufactureChangesRules()[$key]['NewManufacturerText'];
				$currentInventoryObject -> UpdateManufacture();
				
			}
		}
	}
	
	public function CleanManufactureTextBackup() {
		$backupList = new BackupInventoryList();
		$totalManufactureChangeRules = new ConversionRulesList();
		
		
		foreach($backupList -> BackupList() as $cleanManufactureText) {
			
			$key = array_search($cleanManufactureText['inventoryBackupManufactur'], array_column($totalManufactureChangeRules -> ManufactureChangesRules(), 'CurrentManufactureText'));			
			if($key !== false) {
				$backupInventory = InventoryBackup::WithID($cleanManufactureText['inventoryBackupID']);	
				$backupInventory -> NewManufactureText = $totalManufactureChangeRules -> ManufactureChangesRules()[$key]['NewManufacturerText'];
				$backupInventory -> UpdateManufacture();	
			}
			
			
		}
	}

	
	

	


}