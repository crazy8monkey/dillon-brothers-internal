<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:58 PM
 */

class BaseObject {
    function __construct()
    {
        $this -> msg = new Message();
        $this -> json = new JSONparse();
        $this -> validate = new Validation();
		$this -> pages = new Pages();
        $this -> redirect = new Redirect();
		$this -> time = new Time();
		$this -> security = new Security();
		$this -> email = new Email();
		$this -> form = new Form();
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }

    public function __sleep()
    {
        return array();
    }

    public function __wakeup()
    {
        $this -> msg = new Message();
        $this -> json = new JSONparse();
        $this -> validate = new Validation();
        $this -> redirect = new Redirect();
		$this -> currentTime = new Time();
		$this -> email = new Email();
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }
	
	
	protected function seoUrl($string) {
	    //Lower case everything
	    $string = strtolower($string);
	    //Make alphanumeric (removes all other characters)
	    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
	    //Clean up multiple dashes or whitespaces
	    $string = preg_replace("/[\s-]+/", " ", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace("/[\s_]/", "-", $string);
	    return $string;
	}
	
	protected function seoUrlSpaces($string) {
	    //Lower case everything
	    $string = strtolower($string);
	    //Make alphanumeric (removes all other characters)
	    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
	    //Clean up multiple dashes or whitespaces
	    $string = preg_replace("/[\s-]+/", "", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace("/[\s_]/", "", $string);
	    return $string;
	}
	
	protected function CleanString($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string = str_replace('/', '_', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	
	   return preg_replace('/-+/', '_', $string); // Replaces multiple hyphens with single one.
	}
	
	protected function CleanJSONString($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}
	
	protected function CleanStringNoSpaces($string) {
	   $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
	   $string = str_replace('/', '', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	
	   return preg_replace('/-+/', '', $string); // Replaces multiple hyphens with single one.
	}
	
	protected function TimeStamp() {
		return date("Y-m-d", $this -> time -> NebraskaTime()) . "T" . date("H:i:s", $this -> time -> NebraskaTime());
	}
	
	protected function DescriptionFormat($string) {
		return str_replace("\n", '<br />', $string);
	}
	
}