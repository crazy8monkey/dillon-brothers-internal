<?php

class InventoryPhotos extends BaseObject {
	
	private $_id;
	
	public $inventoryID;
	public $photosTemp;
	public $photoName;
	public $vin;
	
	
	public $currentPhotoName;
	public $currentPhotoExt;
	public $RelatedInventoryID;
	
	public $altTag;
	public $titleTag;
	public $newPhotoName;
	public $mainPhotoCheck;
	public $WaterMarkApplied;
	
	
	public $rotation;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($inventoryPhotoID) {
        $instance = new self();
        $instance->_id = $inventoryPhotoID;
        $instance->loadById();
        return $instance;
    }
	
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM inventoryphotos LEFT JOIN inventory ON inventoryphotos.relatedInventoryID = inventory.inventoryID WHERE inventoryPhotoID = :inventoryPhotoID');
        $sth->execute(array(':inventoryPhotoID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	
	
	
    protected function fill(array $row){
    	$this -> currentPhotoName = $row['inventoryPhotoName'];
		$this -> currentPhotoExt = $row['inventoryPhotoExt'];
		$this -> vin = $row['VinNumber'];
		$this -> RelatedInventoryID = $row['relatedInventoryID'];
		$this -> altTag = $row['inventoryAltTag'];
		$this -> titleTag = $row['inventoryTitleTag'];
		$this -> mainPhotoCheck = $row['MainProductPhoto'];
		$this -> WaterMarkApplied = $row['WaterMarkApplied'];
    }	
	
	public function GetPhotoID() {
		return $this->_id;
	} 


	
	public function Validate() {
		$validationErrors = array();
		
		$similiarImageHame = $this -> db -> prepare('SELECT * FROM inventoryphotos WHERE inventoryPhotoID <> :inventoryPhotoID AND inventoryPhotoName = :inventoryPhotoName AND relatedInventoryID = :relatedInventoryID ');
		$similiarImageHame -> execute(array(":inventoryPhotoID" => $this->_id, ":inventoryPhotoName" => parent::seoUrl($this -> newPhotoName), ":relatedInventoryID" => $this -> RelatedInventoryID));
		
		

		if(isset($this->_id)) {
			if($similiarImageHame -> fetchAll()) {
				array_push($validationErrors, array("inputID" => 2,
													'errorMessage' => 'This name is associated with a different photo, please type another one'));
			}	
		} else {
			if($this -> ValidateEmptyPhotos()) {
				array_push($validationErrors, array("inputID" => 1,
													'errorMessage' => 'There are Empty file uploads'));
			}
		}		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Upload() {
		$currentInventoryPhotos = new InventoryPhotosList();

		$currentInventoryObject = InventoryObject::WithID($this -> inventoryID);	
		$currentInventoryPhotoCount = count($currentInventoryPhotos -> ByID($this -> inventoryID));
		
		$count = NULL;
		$ImagePathParts = array();
		$ImagePathParts['vin'] = '/'. $currentInventoryObject -> VinNumber;		
		
		$newImageNameBike = $currentInventoryObject -> Year . ' ' . $currentInventoryObject -> Manufacture  . ' ' . $currentInventoryObject -> CurrentModel  . ' ' . $currentInventoryObject -> ModelFriendlyName; 
		
		$uploadedInventoryPATH = PHOTO_PATH . 'inventory/'. $currentInventoryObject -> VinNumber . '/';
		
		$latestCount = $this -> db -> prepare("SELECT (max(latestCount)) LatestCountUploaded FROM inventoryphotos WHERE relatedInventoryID = :relatedInventoryID");
		$latestCount -> execute(array(":relatedInventoryID" => $this -> inventoryID));
		$getLatestCount = $latestCount -> fetch();
		
		if($getLatestCount['LatestCountUploaded'] != NULL) {
			$count = $getLatestCount['LatestCountUploaded'] + 1;
		} else {
			$count = 1;
		}
		
		
		Image::MoveAndRenameImage($this -> photosTemp, 
								  $ImagePathParts,
								  $this -> photoName,
								  parent::seoUrl($newImageNameBike) . $count, 
								  7);
			
			$employeeID = $this -> db -> insert('inventoryphotos', array('relatedInventoryID' => $this -> inventoryID,
																		 'inventoryPhotoName' => parent::seoUrl($newImageNameBike) . $count,
																		 'inventoryPhotoExt' => Image::getFileExt($this -> photoName),
																		 'inventoryAltTag' => $newImageNameBike,
																		 'inventoryTitleTag' => $newImageNameBike,
																		 'MainProductPhoto' => ($currentInventoryPhotoCount == 0 ? 1 : 0),
																		 'latestCount' => $count));
			
			
			$newImageNameResizedMedium = parent::seoUrl($newImageNameBike) . $count . '-s.' . Image::getFileExt($this -> photoName);
			$newImageNameResizedLarge = parent::seoUrl($newImageNameBike) . $count . '-l.' . Image::getFileExt($this -> photoName);
			
																		 
			$currentUploadedPhoto = $uploadedInventoryPATH . parent::seoUrl($newImageNameBike) . $count . '.' . Image::getFileExt($this -> photoName);
																		 
			list($origWidth, $origHeight) = getimagesize($currentUploadedPhoto);
																		 
			//small
			if(300 > $origWidth) {
				Image::resizeImage($currentUploadedPhoto, $uploadedInventoryPATH . $newImageNameResizedMedium, $origWidth, $origHeight);			
			} else {
				Image::resizeImage($currentUploadedPhoto, $uploadedInventoryPATH . $newImageNameResizedMedium, 300, 300);	
			}
				
			//large
			if(1500 > $origWidth) {
				Image::resizeImage($currentUploadedPhoto, $uploadedInventoryPATH . $newImageNameResizedLarge, $origWidth, $origHeight);
			} else {
				Image::resizeImage($currentUploadedPhoto, $uploadedInventoryPATH . $newImageNameResizedLarge, 1200, 1200);	
			}															 
																		 
			unlink($uploadedInventoryPATH . parent::seoUrl($newImageNameBike) . $count . '.' . Image::getFileExt($this -> photoName));															
			
			$count++;
		
		
		
		
		$this -> json -> outputJqueryJSONObject('finishedUpload', true);	
	}

	public function MarkMainPhoto() {
		
	}

	public function RotateImage() {
		
		$imageRotatedLarge = PHOTO_PATH . 'inventory/' . $this -> vin . '/' . $this -> currentPhotoName . '-l.' . $this -> currentPhotoExt;
		$imageRotatedMedium = PHOTO_PATH . 'inventory/' . $this -> vin . '/' . $this -> currentPhotoName . '-s.' . $this -> currentPhotoExt;
		
		$sourceLarge = imagecreatefromjpeg($imageRotatedLarge);
		$sourceMedium = imagecreatefromjpeg($imageRotatedLarge);
		
		switch($this -> rotation) {
			case "right":
				$rotateLarge = imagerotate($sourceLarge, -90, 0);
				$rotateMedium = imagerotate($sourceMedium, -90, 0);		
				break;
			case "left":
				$rotateLarge = imagerotate($sourceLarge, 90, 0);	
				$rotateMedium = imagerotate($sourceMedium, 90, 0);		
				break;
			
		}
		
		
		
		
		imagejpeg($rotateLarge, $imageRotatedLarge);
		
		imagedestroy($sourceLarge);
		imagedestroy($rotateLarge);
		
		imagejpeg($rotateMedium, $imageRotatedMedium);
		
		imagedestroy($sourceMedium);
		imagedestroy($rotateMedium);
		
		$this -> redirect -> redirectPage(PATH. 'inventory/editphoto/' . $this -> _id);
	}

	public function WaterMarkSingle() {
		$currentInventoryObject = InventoryObject::WithID($this -> RelatedInventoryID);
		
		$this -> WaterMarkPhoto(PHOTO_PATH . 'inventory/' . $this -> vin . '/' . $this -> currentPhotoName . '-l.' . $this -> currentPhotoExt, 
								$currentInventoryObject -> StoreID, 
								$this -> vin, 
								$this -> currentPhotoName . '-l.' . $this -> currentPhotoExt);
								
								
		$this -> WaterMarkPhoto(PHOTO_PATH . 'inventory/' . $this -> vin . '/' . $this -> currentPhotoName . '-s.' . $this -> currentPhotoExt, 
								$currentInventoryObject -> StoreID, 
								$this -> vin, 
								$this -> currentPhotoName . '-s.' . $this -> currentPhotoExt);	
								
		$postData = array('WaterMarkApplied' => 1);	
		$this->db->update('inventoryphotos', $postData, array('inventoryPhotoID' => $this -> _id));											
	}

	public function RedirectToPhotoSingle() {
		$this -> redirect -> redirectPage($this -> pages -> inventory() . '/editphoto/' . $this -> _id);	
	}

	
	public function WaterMarkAllPhotosByInventory() {
		$photos = new InventoryPhotosList();
		
		$currentInventoryObject = InventoryObject::WithID($this -> RelatedInventoryID);
		foreach($photos -> ByID($this -> RelatedInventoryID) as $photoSingle) {
			if($photoSingle['WaterMarkApplied'] == 0) {
				$this -> WaterMarkPhoto(PHOTO_PATH . 'inventory/' . $currentInventoryObject -> VinNumber . '/' . $photoSingle['inventoryPhotoName'] . '-l.' . $photoSingle['inventoryPhotoExt'], 
								$currentInventoryObject -> StoreID, 
								$currentInventoryObject -> VinNumber, 
								$photoSingle['inventoryPhotoName'] . '-l.' . $photoSingle['inventoryPhotoExt']);
								
								
				$this -> WaterMarkPhoto(PHOTO_PATH . 'inventory/' . $currentInventoryObject -> VinNumber . '/' . $photoSingle['inventoryPhotoName'] . '-m.' . $photoSingle['inventoryPhotoExt'], 
										$currentInventoryObject -> StoreID, 
										$currentInventoryObject -> VinNumber, 
										$photoSingle['inventoryPhotoName']. '-s.' . $photoSingle['inventoryPhotoExt']);
										
										
				$postData = array('WaterMarkApplied' => 1);	
				$this->db->update('inventoryphotos', $postData, array('inventoryPhotoID' => $photoSingle['inventoryPhotoID']));						
			}
			
		}
		
		$this -> redirect -> redirectPage($this -> pages -> inventory() . '/edit/' . $this -> RelatedInventoryID);	
		
	}



	private function WaterMarkPhoto($img, $storeID, $vin, $newImage) {
		$store = Store::WithID($storeID);
		
		$waterMark = imagecreatefrompng(PHOTO_PATH . 'WaterMark/' . $store -> WaterMarkImage);
		$image = imagecreatefromjpeg($img);
		
		$marge_right = 0;
		$marge_bottom = 0;
		$sx = imagesx($waterMark);
		$sy = imagesy($waterMark);
		$sximg = imagesx($image);
		
		$percent = $sximg * 0.15;
		
		
		//Create the final resized watermark stamp
	    $dest_image = imagecreatetruecolor($percent, $percent);
		
		
	    //keeps the transparency of the picture
	    imagealphablending( $dest_image, false );
	    imagesavealpha( $dest_image, true );
	    
	    //resizes the stamp
	    imagecopyresampled($dest_image, $waterMark, 0, 0, 0, 0, $percent, $percent, $sx, $sy);
		  
		
		imagecopy($image, 
				  $dest_image, 
				  imagesx($image) - imagesx($dest_image) - $marge_right, 
				  imagesy($image) - imagesy($dest_image) - $marge_bottom, 
				  0, 
				  0, 
				  $percent, 
				  $percent);
		
		
		imagepng($image, PHOTO_PATH . 'inventory/' . $vin . '/' . $newImage);

		
	}
	
	
	
	public function Save() {
		$postData = array('inventoryPhotoName' => parent::seoUrl($this -> newPhotoName),
						  'inventoryAltTag' => $this -> altTag,
						  'inventoryTitleTag' => $this -> titleTag,
						  'MainProductPhoto' => $this -> mainPhotoCheck);	
						  
						  
						  
		$this->db->update('inventoryphotos', $postData, array('inventoryPhotoID' => $this -> _id));
		
		
		$updateAllMainPhoto = $this -> db -> prepare('UPDATE inventoryphotos SET MainProductPhoto = 0 WHERE inventoryPhotoID <> :inventoryPhotoID AND relatedInventoryID = :relatedInventoryID');
		$updateAllMainPhoto -> execute(array(":inventoryPhotoID" => $this -> _id, ":relatedInventoryID" => $this -> RelatedInventoryID));
		
		
		$uploadedAlbumPATH = PHOTO_PATH . 'inventory/'. $this -> vin . '/';
		
		$uploadedPATHMedium = $uploadedAlbumPATH . $this -> currentPhotoName . '-s.' . $this -> currentPhotoExt;
		$uploadedPATHLarge = $uploadedAlbumPATH . $this -> currentPhotoName . '-l.' . $this -> currentPhotoExt;
		
		
		
		//medium
		Image::renameImage($uploadedAlbumPATH, $uploadedPATHMedium, parent::seoUrl($this -> newPhotoName). '-s');
		
		//large
		Image::renameImage($uploadedAlbumPATH, $uploadedPATHLarge, parent::seoUrl($this -> newPhotoName). '-l');
		
		//check if inventory object is active
		$currentInventory = InventoryObject::WithID($this -> RelatedInventoryID);		
		if($currentInventory -> InventoryActive == 1) {
			if($this -> mainPhotoCheck == 1) {
				$AlgoliaClient = new \AlgoliaSearch\Client("943L0R18H9", "eba4ec01de1347a0bff85f56d79ad18d");
				
				if(LIVE_SITE == TRUE) {
					$index = $AlgoliaClient->initIndex('MainUnitInventory');	
				} else {
					$index = $AlgoliaClient->initIndex('MainUnitInventoryTest');	
				}
	
				$index->partialUpdateObject(array("Image" => PHOTO_URL . 'inventory/'. $this -> vin . '/' . $this -> currentPhotoName . '-s.' . $this -> currentPhotoExt,
										 		  "objectID" => $this -> RelatedInventoryID));	
	
			}
			
		}
		
		 
		$this -> json -> outputJqueryJSONObject('saved', true);
	}
	
	
	public function Delete() {
		//medium
		unlink(PHOTO_PATH . 'inventory/'. $this -> vin . '/' . $this -> currentPhotoName . '-s.' . $this -> currentPhotoExt);
		//large		
		unlink(PHOTO_PATH . 'inventory/'. $this -> vin . '/' . $this -> currentPhotoName . '-l.' . $this -> currentPhotoExt);
				
		$sth = $this -> db -> prepare("DELETE FROM inventoryphotos WHERE inventoryPhotoID = :inventoryPhotoID");
		$sth -> execute(array(':inventoryPhotoID' => $this->_id));
		
		$this -> redirect -> redirectPage($this -> pages -> inventory() . '/edit/' . $this -> RelatedInventoryID);	
	}
	
	public function CompressImages() {
		foreach($this -> db -> select('SELECT * FROM inventoryphotos LEFT JOIN inventory ON inventoryphotos.relatedInventoryID = inventory.inventoryID') as $photoSingle) {
			$smallimage = PHOTO_PATH . 'inventory/' . $photoSingle['VinNumber'] . '/'. $photoSingle['inventoryPhotoName'] . '-s' . $photoSingle['inventoryPhotoExt'];
			$largeimage = PHOTO_PATH . 'inventory/' . $photoSingle['VinNumber'] . '/'. $photoSingle['inventoryPhotoName'] . '-l' . $photoSingle['inventoryPhotoExt'];
			
			Image::compress($smallimage, $smallimage, 80);
			Image::compress($largeimage, $largeimage, 80);
		}
	}

	
	private function ValidateEmptyPhotos() {
		$ReturnValue = NULL;
		foreach ($this -> photosTemp as $key => $value) {
			if(empty($value)) {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}

	
	
	





}