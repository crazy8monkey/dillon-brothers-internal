<?php


class PartsAccessoryBrand extends BaseObject {
	
	private $_id;
	
	public $PartsAccessoryBrandName;
	public $PartsBrandImage;
	public $ApparelDescription;
	public $PartsDescription;	
	public $BrandType;
	
	public $ViewType;
	
	public $CurrentBrandImage;
	public $NoValidate = false;
	
	
	public $currentBrandType;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithBrand($name) {
        $instance = new self();
        $instance->_brandName = $name;
        $instance->loadRow();
        return $instance;
    }

	public static function WithID($brandID) {
        $instance = new self();
        $instance-> _id = $brandID;
        $instance->loadRow();
        return $instance;
    }
	
	
		
	protected function loadRow() {
		if(isset($this -> _brandName)) {
			$sth = $this -> db -> prepare('SELECT * FROM partaccessorybrand WHERE brandSEOUrl = :brandSEOUrl');
        	$sth->execute(array(':brandSEOUrl' => $this->_brandName));
		}
		
		if(isset($this -> _id)) {
			$sth = $this -> db -> prepare('SELECT * FROM partaccessorybrand WHERE partAccessoryBrandID = :partAccessoryBrandID');
        	$sth->execute(array(':partAccessoryBrandID' => $this->_id));	
		}
		
		
        $record = $sth->fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> _id = $row['partAccessoryBrandID'];
		$this -> PartsAccessoryBrandName = $row['brandName'];
		$this -> PartsDescription = $row['partDescription'];
		$this -> ApparelDescription = $row['apparelDescription'];
		$this -> CurrentBrandImage = $row['partBrandImage'];
		$this -> currentBrandType = $row['BrandType'];
    }
	
	public function GetNewSpecialBreadCrumbs() {
		return "<a href='" . $this -> pages -> specials() . "'>Types of Specials</a><span><i class='fa fa-caret-right' aria-hidden='true'></i></span>".
			   "<a href='" . $this -> pages -> specials() . "/category/Parts'>Parts Specials</a><span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   "<a href='". $this -> pages -> specials() . "/brand/" . $this -> _id . "/Parts'>" . $this -> PartsAccessoryBrandName . " Specials</a><span><i class='fa fa-caret-right' aria-hidden='true'></i></span>New " . $this -> PartsAccessoryBrandName . " Special";
	}
	
	
	public function GetID() {
		return $this -> _id;
	}
	
	
	
	public function Validate() {
		$validationErrors = array();
		
		
		if(!isset($this -> _id)) {
			$duplicateBrand = $this->db->prepare('SELECT brandName FROM partaccessorybrand WHERE brandName = ?');
            $duplicateBrand -> execute(array($this -> PartsAccessoryBrandName));
			
			if($this -> BrandType == 0) {
					array_push($validationErrors, array('inputID' => 4,
														'errorMessage' => 'Required'));
			} else if($this -> BrandType == 1) {
				if($this -> validate -> emptyInput($this -> PartsDescription)) {
						array_push($validationErrors, array('inputID' => 3,
															'errorMessage' => 'Required'));
				}
			} else if($this -> BrandType == 2) {
				if($this -> validate -> emptyInput($this -> ApparelDescription)) {
					array_push($validationErrors, array('inputID' => 5,
														'errorMessage' => 'Required'));
				}	
				
					
			} else if($this -> BrandType == 3) {
					
				if($this -> ViewType == "PartsCategory") {
					if($this -> validate -> emptyInput($this -> PartsDescription)) {
						array_push($validationErrors, array('inputID' => 3,
															'errorMessage' => 'Required'));
					}	
				}
							
				if($this -> ViewType == "ApparelCategory") {
					if($this -> validate -> emptyInput($this -> ApparelDescription)) {
						array_push($validationErrors, array('inputID' => 5,
															'errorMessage' => 'Required'));
					}	
				}
					
			}
			
			
			
		} else {
			$duplicateBrand = $this->db->prepare('SELECT brandName FROM partaccessorybrand WHERE brandName = :brandName and partAccessoryBrandID <> :partAccessoryBrandID');
            $duplicateBrand -> execute(array(':partAccessoryBrandID' => $this -> _id, ':brandName' => $this -> PartsAccessoryBrandName));
			
			
			if($this -> currentBrandType == $this -> BrandType) {
				if($this -> BrandType == 0) {
					array_push($validationErrors, array('inputID' => 4,
														'errorMessage' => 'Required'));
				} else {
					if($this -> BrandType == 1) {
						if($this -> validate -> emptyInput($this -> PartsDescription)) {
							array_push($validationErrors, array('inputID' => 3,
																'errorMessage' => 'Required'));
						}
					} else if($this -> BrandType == 2) {
						if($this -> validate -> emptyInput($this -> ApparelDescription)) {
							array_push($validationErrors, array('inputID' => 5,
																'errorMessage' => 'Required'));
						}	
					} else if($this -> BrandType == 3) {
					
						if($this -> ViewType == "PartsCategory") {
							if($this -> validate -> emptyInput($this -> PartsDescription)) {
								array_push($validationErrors, array('inputID' => 3,
																	'errorMessage' => 'Required'));
							}	
						}
									
						if($this -> ViewType == "ApparelCategory") {
							if($this -> validate -> emptyInput($this -> ApparelDescription)) {
								array_push($validationErrors, array('inputID' => 5,
																	'errorMessage' => 'Required'));
							}	
						}
							
					}
						
					
						
				}
			}	
			
			
		}
		
		
			
		if($this -> validate -> emptyInput($this -> PartsAccessoryBrandName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if($duplicateBrand -> fetch()) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'This brand is already entered in our system, please type in a different brand'));
		}
		
		
			
	
		if(empty($this -> CurrentBrandImage)) {
			if($this -> validate -> emptyInput($this -> PartsBrandImage)) {
				array_push($validationErrors, array('inputID' => 2,
													'errorMessage' => 'Required'));
			}	
		}	
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	
	private function uploadPicture() {
		$ImagePathParts = array();
		$ImagePathParts['PartsBrand'] = "PartsBrand";
		
		Image::MoveAndRenameImage($_FILES['brandLogo']['tmp_name'], 
								  $ImagePathParts,
								  $this -> PartsBrandImage,
								  "Dillon" . parent::seoUrl($this -> PartsAccessoryBrandName), 2);	
								  
		
		$fileExt = explode('.', $this -> PartsBrandImage);
		$PartBrandFileDirectory = PHOTO_PATH . "PartsBrand/"; 
		
		$currentPhotoUploaded = $PartBrandFileDirectory . "Dillon" . parent::seoUrl($this -> PartsAccessoryBrandName) . '.' . $fileExt[1];
		
		list($origWidth, $origHeight) = getimagesize($currentPhotoUploaded);
		
		
		//small
		if(120 > $origWidth) {
			$resizeWidth = $origWidth;
			$resizeHeight = $origHeight;		
		} else {
			$resizeWidth = 120;
			$resizeHeight = 120;	
		}	
		
		Image::resizeImage($currentPhotoUploaded, 
						   $PartBrandFileDirectory . "Dillon" . parent::seoUrl($this -> PartsAccessoryBrandName) . '.' . $fileExt[1], 
						   $resizeWidth, 
						   $resizeHeight);
						   			  				  
	}
	
	public function Save() {
		try {
			$PartAccessoryBrandData = array();
			
			if(isset($this -> _id)) {
				$PartAccessoryBrandData['brandName'] = $this -> PartsAccessoryBrandName;	
				$PartAccessoryBrandData['brandSEOUrl'] = parent::seoUrl($this -> PartsAccessoryBrandName);		
				
				if(isset($this -> PartsDescription)) {
					$PartAccessoryBrandData['partDescription'] = parent::DescriptionFormat($this -> PartsDescription);		
				}

				if(isset($this -> ApparelDescription)) {
					$PartAccessoryBrandData['apparelDescription'] = parent::DescriptionFormat($this -> ApparelDescription);	
				}
				
				$PartAccessoryBrandData['BrandType'] = $this -> BrandType;	
					
				if(!empty($this -> PartsBrandImage)) {
					$this -> uploadPicture();
					$PartAccessoryBrandData['partBrandImage'] = "Dillon" . parent::seoUrl($this -> PartsAccessoryBrandName) . '.' . Image::getFileExt($this -> PartsBrandImage);		
				}
				
				$type = NULL;
				$specialTypeValue = NULL;
				switch($this -> BrandType) {
					case 1:
						$type = '/Parts';
						$specialTypeValue = 3;
						break;
					case 2:
						$type = '/Apparel';
						$specialTypeValue = 2;
						break;
				}
				
				if($this -> currentBrandType != $this -> BrandType) {
					
					if($this -> BrandType != 3) {
						$specialsList = new SpecialsList();	
						foreach($specialsList -> SpecialsByPartsApparelBrand($this -> _id) as $item) {
							$specialSingle = Special::WithID($item['CreatedSpecialID']);
							$specialSingle -> SpecialCategory = $specialTypeValue;
							$specialSingle -> UpdateSpecialType();
						}
					}
				}
				
				$this->db->update('partaccessorybrand', $PartAccessoryBrandData, array('partAccessoryBrandID' => $this -> _id));		
				
				$this -> json -> outputJqueryJSONObject('redirect', array('PATH' => PATH . 'brands/view/partapparel/' .$this -> _id,
																		  'NewCategoryValue' => $this -> BrandType));			
			} else {
				$this -> uploadPicture();
				
				$PartAccessoryBrandData['brandName'] = $this -> PartsAccessoryBrandName;	
				$PartAccessoryBrandData['brandSEOUrl'] = parent::seoUrl($this -> PartsAccessoryBrandName);		
				$PartAccessoryBrandData['partBrandImage'] = "Dillon" . parent::seoUrl($this -> PartsAccessoryBrandName) . '.' . Image::getFileExt($this -> PartsBrandImage);
				$PartAccessoryBrandData['partDescription'] = parent::DescriptionFormat($this -> PartsDescription);	
				$PartAccessoryBrandData['apparelDescription'] = parent::DescriptionFormat($this -> ApparelDescription);	
				$PartAccessoryBrandData['BrandType'] = $this -> BrandType;	
				
				$newBrand = $this -> db -> insert('partaccessorybrand', $PartAccessoryBrandData);	
				
				$redirectLink = PATH . 'brands/view/partapparel/' . $newBrand;
				
				
				$this -> json -> outputJqueryJSONObject('redirect', array("PATH" => $redirectLink));
			}

					
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Event Save Error: " . $e->getMessage();
			$TrackError -> type = "EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}

	public function delete() {
		unlink(PHOTO_PATH . 'PartsBrand/' . $this -> CurrentBrandImage);	
				
		$specials = new SpecialsList();
		
		foreach($specials -> SpecialsByPartApparelBrand($this -> _id) as $specialSingle) {
			$special = Special::WithID($specialSingle['SpecialID']);
			$special -> delete();
		}
		
		$sth = $this -> db -> prepare("DELETE FROM partaccessorybrand WHERE partAccessoryBrandID = :partAccessoryBrandID");
		$sth -> execute(array('partAccessoryBrandID' => $this -> _id));	
		
		
		$this -> redirect -> redirectPage(PATH. 'specials');
		
	}
	

	
	

		

}