<?php

class EcommerceSettingCategory extends BaseObject {
	
	private $_id;
	public $categoryName;
	public $SaveSizes;
	public $CurrentSavedSizes;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }

    public static function WithID($categoryID) {
        $instance = new self();
        $instance -> _id = $categoryID;
        $instance -> loadById();
        return $instance;
    }
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommercesettingcategories WHERE settingCategoryID = :settingCategoryID');
	    $sth->execute(array(':settingCategoryID' => $this -> _id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	public function GetID() {
		return $this -> _id;
	}

	protected function fill(array $row){
		$this -> categoryName = $row['settingCategoryName'];
		$this -> CurrentSavedSizes = $row['relatedSizeIDs'];
    }	
	
	public function Validate() {
		$validationErrors = array();
		
		if(!isset($this -> _id)) {
			$duplicateName = $this -> db -> prepare('SELECT settingCategoryName FROM ecommercesettingcategories WHERE settingCategoryName = ?');
			$duplicateName -> execute(array($this -> categoryName));
		} else {
			$duplicateName = $this -> db -> prepare('SELECT settingCategoryName FROM ecommercesettingcategories WHERE settingCategoryName = :settingCategoryName AND settingCategoryID <> :settingCategoryID');
			$duplicateName -> execute(array(":settingCategoryName" => $this -> categoryName, ":settingCategoryID" => $this -> _id));
		}
		
		if($this -> validate -> emptyInput($this -> categoryName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if($duplicateName -> fetch()) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'There is already a category associated with a different item, please type in a different category'));
		}
		
		//if(!isset($this -> SaveSizes)) {
		//	array_push($validationErrors, array('inputID' => 2,
		//										'errorMessage' => 'Please Select a Size related to this product category'));
		//}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
	}

	


	

	

	
	public function Save() {
		try {
		
			
			if(isset($this -> _id)) {
				$redirectID = $this -> _id;
				$savedSizeID = '';
				
				$updatedData = array();
				$updatedData['settingCategoryName'] = $this -> categoryName;
				$updatedData['settingCategorySEOName'] = parent::seoUrl($this -> categoryName);
				
				if(count($this -> SaveSizes)> 0) {
					foreach($this -> SaveSizes as $sizeSelected) {
						$savedSizeID .= $sizeSelected . ',';	
					}	
					
					$updatedData['relatedSizeIDs'] = rtrim($savedSizeID, ',');
				}
				
				$this->db->update('ecommercesettingcategories', $updatedData, array('settingCategoryID' => $this -> _id));
				
			} else {
				$newCategory = $this -> db -> insert('ecommercesettingcategories', array('settingCategoryName' => $this -> categoryName,
						 															  	 'settingCategorySEOName' => parent::seoUrl($this -> categoryName)));	
																		  
				$redirectID = $newCategory;														  
															  							  				
			}			
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'shop/edit/category/'. $redirectID);
		
			
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Ecommerce Setting Category Save Error: " . $e->getMessage();
			$TrackError -> type = "ECOMMERCE SETTING CATEGORY SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
		}
	}
	
	
	
	public function delete() {
		try {
		
			$products = new EcommerceProductList();
			
			if(count($products -> ByCategory($this->_id))) {
				foreach($products -> ByCategory($this->_id) as $productSingle) {
					$product = EcommerceProduct::WithID($productSingle['productID']);
					$product -> UpdateCategory();
				}
			}
			
			//delete special from table
			$settingRemove = $this -> db -> prepare("DELETE FROM ecommercesettingcategories WHERE settingCategoryID = :settingCategoryID");
			$settingRemove -> execute(array(':settingCategoryID' => $this->_id));
			
			$this -> redirect -> redirectPage(PATH . 'shop/settings');
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Ecommerce Setting Category Error: " . $e->getMessage();
			$TrackError -> type = "DELETE ECOMMERCE SETTING CATEGORY ERROR";
			$TrackError -> SendMessage();
		}
	}


}