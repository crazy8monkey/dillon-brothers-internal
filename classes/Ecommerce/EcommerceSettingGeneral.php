<?php

class EcommerceSettingGeneral extends BaseObject {
	
	private $_id;
	public $ShippingRates;	
	public $EmailNotifications;

	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }

    public static function Init() {
        $instance = new self();
        $instance -> loadById();
        return $instance;
    }
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommercesettings WHERE ecommerceSettingsID = :ecommerceSettingsID');
	    $sth->execute(array(':ecommerceSettingsID' => 1));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	public function GetID() {
		return $this -> _id;
	}

	protected function fill(array $row){
		$this -> ShippingRates = $row['shippingRates'];
		$this -> EmailNotifications = $row['emailnotifications'];
    }	
	
	public function Validate() {
		$validationErrors = array();
		
		
		if($this -> validate -> emptyInput($this -> ShippingRates)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} 
		
		if($this -> validate -> emptyInput($this -> EmailNotifications)) {
			array_push($validationErrors, array('inputID' => 2,
												'errorMessage' => 'Required'));
		} 
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
	}

	
	public function Save() {
		try {
			
			$postData = array('shippingRates' => $this -> ShippingRates,
							  'emailnotifications' => $this -> EmailNotifications);										
					
			$this->db->update('ecommercesettings', $postData, array('ecommerceSettingsID' => 1));
			
				
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'shop/settings');
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Ecommerce Setting General Save Error: " . $e->getMessage();
			$TrackError -> type = "ECOMMERCE SETTING GENERAL SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
		}
	}
	
	
	
	public function delete() {
		try {
			$savedProductSized = new EcommerceSavedSizes();
			$savedProductSized -> SizeID = $this->_id;
			$savedProductSized -> DeleteBySize();
		
			$sizeRemove = $this -> db -> prepare("DELETE FROM ecommercesettingsizes WHERE settingSizeID = :settingSizeID");
			$sizeRemove -> execute(array(':settingSizeID' => $this->_id));
			
			$this -> redirect -> redirectPage(PATH . 'shop/settings');
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Ecommerce Setting Size Error: " . $e->getMessage();
			$TrackError -> type = "DELETE ECOMMERCE SETTING SIZE ERROR";
			$TrackError -> SendMessage();
		}
	}


}