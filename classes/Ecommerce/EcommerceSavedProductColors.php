<?php

class EcommerceSavedProductColors extends BaseObject {
	
	public $productID;
	public $ColorID;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }

	public function Save() {
		try {
			$this -> db -> insert('ecommercesavedproductcolors', array('relatedProductID' => $this -> productID,
															   		   'relatedColorID' => $this -> ColorID));	
		
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Product Saved Product Color Error: " . $e->getMessage();
			$TrackError -> type = " PRODUCT SAVED PRODUCT COLOR ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	
	
	public function delete() {
		try {
			//delete special from table
			$saveColorRemove = $this -> db -> prepare("DELETE FROM ecommercesavedproductcolors WHERE relatedProductID = :relatedProductID");
			$saveColorRemove -> execute(array(':relatedProductID' => $this-> productID));
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Saved Product Color Error: " . $e->getMessage();
			$TrackError -> type = "DELETE SAVED PRODUCT COLOR ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	public function DeleteByColor() {
		try {
			//delete special from table
			$saveColorRemove = $this -> db -> prepare("DELETE FROM ecommercesavedproductcolors WHERE relatedColorID = :relatedColorID");
			$saveColorRemove -> execute(array(':relatedColorID' => $this-> ColorID));
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Saved Product Color Error: " . $e->getMessage();
			$TrackError -> type = "DELETE SAVED PRODUCT COLOR ERROR";
			$TrackError -> SendMessage();
		}
	}



}