<?php

class EcommerceSettingColor extends BaseObject {
	
	private $_id;
	public $colorName;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }

    public static function WithID($categoryID) {
        $instance = new self();
        $instance -> _id = $categoryID;
        $instance -> loadById();
        return $instance;
    }
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommercesettingcolors WHERE settingColorID = :settingColorID');
	    $sth->execute(array(':settingColorID' => $this -> _id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	public function GetID() {
		return $this -> _id;
	}

	protected function fill(array $row){
		$this -> colorName = $row['settingColorText'];
    }	
	
	public function Validate() {
		$validationErrors = array();
		
		if(!isset($this -> _id)) {
			$duplicateName = $this -> db -> prepare('SELECT settingColorText FROM ecommercesettingcolors WHERE settingColorText = ?');
			$duplicateName -> execute(array($this -> colorName));
		} else {
			$duplicateName = $this -> db -> prepare('SELECT settingColorText FROM ecommercesettingcolors WHERE settingColorText = :settingColorText AND settingColorID <> :settingColorID');
			$duplicateName -> execute(array(":settingColorText" => $this -> colorName, ":settingColorID" => $this -> _id));
		}
		
		if($this -> validate -> emptyInput($this -> colorName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if($duplicateName -> fetch()) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'This color has already been entered, please type in a different color'));
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
	}

	


	

	

	
	public function Save() {
		try {
			if(isset($this -> _id)) {
				$redirectID = $this -> _id;	
				$postData = array('apparelcolortext' => $this -> colorName);										
				$this->db->update('ecommercesettingcolors', $postData, array('settingColorID' => $this -> _id));
			} else {
				$newColor = $this -> db -> insert('ecommercesettingcolors', array('apparelcolortext' => $this -> colorName));	
				$redirectID = $newColor;	
			}
			
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'shop/edit/color/'. $redirectID);	
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Ecommerce Setting Color Save Error: " . $e->getMessage();
			$TrackError -> type = "ECOMMERCE SETTING COLOR SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
		}
	}
	
	
	
	public function delete() {
		try {
			
			$savedProductColors = new EcommerceSavedProductColors();
			$savedProductColors -> ColorID = $this->_id;
			$savedProductColors -> DeleteByColor();
			
			$colorRemove = $this -> db -> prepare("DELETE FROM ecommercesettingcolors WHERE settingColorID = :settingColorID");
			$colorRemove -> execute(array(':settingColorID' => $this->_id));
			
			$this -> redirect -> redirectPage(PATH . 'shop/settings');
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Ecommerce Setting Color Error: " . $e->getMessage();
			$TrackError -> type = "DELETE ECOMMERCE SETTING COLOR ERROR";
			$TrackError -> SendMessage();
		}
	}



}