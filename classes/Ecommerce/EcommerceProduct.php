<?php

class EcommerceProduct extends BaseObject {
	
	private $_id;
	public $productName;
	public $StockNumber;
	public $Description;
	public $MSRPPrice;
	public $Price;
	public $Category;
	public $Sizes;
	public $Colors;
	public $Folder;
	public $publishedDate;
	public $Visible;
	public $StoreID;
	public $PartNumberSelected;
	public $FeaturedProduct;
	
	public $NewFeaturedProduct;
	
	public $FolderName;
	public $ProductSEOName;
	
	public $FilteredSubmittedContent;
	
	public $SizesSelected = array();
	public $ColorsSelected = array();
	
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }

    public static function WithID($categoryID) {
        $instance = new self();
        $instance -> _id = $categoryID;
        $instance -> loadById();
        return $instance;
    }
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommerceproducts WHERE productID = :productID');
	    $sth->execute(array(':productID' => $this -> _id));	
        $record = $sth -> fetch();
        $this->fill($record);
		$this -> GetSelectedSizes();
		$this -> GetSelectedColors();
    }
	
	private function GetSelectedSizes() {
		$savedSizes = new EcommerceProductSavedSizesList();
		$this -> SizesSelected = $savedSizes -> ByProduct($this -> _id);
	}
	
	private function GetSelectedColors() {
		$savedColors = new EcommerceProductSavedColorList();
		$this -> ColorsSelected = $savedColors -> ByProduct($this -> _id);
	}
	
	public function GetID() {
		return $this -> _id;
	}

	protected function fill(array $row){
		$this -> productName = $row['productName'];
		$this -> StockNumber = $row['SKUNumber'];
		$this -> MSRPPrice = $row['productMSRP'];
		$this -> Price = $row['productPrice'];
		$this -> Description = $row['productDescription'];
		$this -> Category = $row['productCategory'];
		$this -> Folder = $row['productFolderName'];
		$this -> publishedDate = $row['productPublishedDate'];
		$this -> Visible = $row['productVisible'];
		$this -> StoreID = $row['productRelatedStore'];
		$this -> FolderName = $row['productFolderName'];
		$this -> ProductSEOName = $row['productSEOName'];
		$this -> FeaturedProduct = $row['featuredProduct'];
    }	
	
	
	
	public function Validate() {
		$validationErrors = array();
		
		//if(!isset($this -> _id)) {
		//	$duplicateName = $this -> db -> prepare('SELECT apparelCategoryName FROM ecommerceapparelcategories WHERE apparelCategoryName = ?');
		//	$duplicateName -> execute(array($this -> categoryName));
		//} else {
		//	$duplicateName = $this -> db -> prepare('SELECT apparelCategoryName FROM ecommerceapparelcategories WHERE apparelCategoryName = :apparelCategoryName AND apparelCategoryID <> :apparelCategoryID');
		//	$duplicateName -> execute(array(":apparelCategoryName" => $this -> categoryName, ":apparelCategoryID" => $this -> _id));
		//}
		
		if($this -> validate -> emptyInput($this -> productName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} 
		
		
		if($this -> validate -> emptyInput($this -> Price)) {
			array_push($validationErrors, array('inputID' => 3,
												'errorMessage' => 'Required'));
		}
 
		if(isset($this -> _id)) {
			if(count($this-> Sizes) > 0) {
				foreach($this-> Sizes as $key => $sizeSingle) {
					if(empty($this -> PartNumberSelected[$key])) {
						array_push($validationErrors, array('inputID' => 6,
															'errorMessage' => 'Please type in the Part # for the selected colors'));
						break;									
															
					}
				}	
			} else {
				if($this -> validate -> emptyInput($this -> StockNumber)) {
					array_push($validationErrors, array('inputID' => 2,
														'errorMessage' => 'Required'));
				}				
			}	
		}
		

		if($this -> Category == 0) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => 'Required'));
		} 
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
	}


	public function ValidateFilter() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> FilteredSubmittedContent)) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => 'Required'));
		} 
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	


	

	

	
	public function Save() {
		try {
			
			
		
			if(isset($this -> _id)) {
				
				$editIDRedirect = $this -> _id;
				$updatedProductData = array();
				$updatedProductData['productName'] = $this -> productName;
				$updatedProductData['productSEOName'] = parent::seoUrl($this -> productName);
				
				if(count($this-> Sizes) > 0) {
					$updatedProductData['SKUNumber'] = '';
				} else {
					$updatedProductData['SKUNumber'] = $this -> StockNumber;
				}
				$updatedProductData['productDescription'] = $this -> Description;
				$updatedProductData['productMSRP'] = $this -> MSRPPrice;
				$updatedProductData['productPrice'] = $this -> Price;
				$updatedProductData['productCategory'] = $this -> Category;
				$updatedProductData['featuredProduct'] = $this -> NewFeaturedProduct;
												
				$this->db->update('ecommerceproducts', $updatedProductData, array('productID' => $this -> _id));
				
				
				$saveSize = new EcommerceSavedSizes();
				$saveSize -> productID = $editIDRedirect;
				//remove previous saved sizes
				$saveSize -> delete();
				
				//$savedColors = new EcommerceSavedProductColors();
				//$savedColors -> productID = $editIDRedirect;
				//remove previous saved colors
				//$savedColors -> delete();
				
					
				//add sizes if exist	
				if(count($this-> Sizes) > 0) {
					$order = 1;
					foreach($this-> Sizes as $key => $sizeSingle) {
						$saveSize -> SizeID = $sizeSingle;
						$saveSize -> PartNumber = $this -> PartNumberSelected[$key];
						
						$saveSize -> OrderNumber = $order;
						$saveSize -> Save();
						
						$order++;
					}
				}
				
				//add colors if exist
				//if(count($this -> Colors) > 0) {
				//	foreach($this-> Colors as $colorSingle) {
				//		$savedColors -> ColorID  = $colorSingle;
				//		$savedColors -> Save();
				//	}
				//}
				
				
			} else {
				$folderName = parent::seoUrl($this -> productName);
				$newProduct = $this -> db -> insert('ecommerceproducts', array('productName' => $this -> productName,
																			   'productSEOName' => parent::seoUrl(rtrim($this -> productName)),
																			   'productDescription' => $this -> Description,
																			   'productFolderName' => $folderName,
																			   'productPrice' => $this -> Price,
																			   'productCategory' => $this -> Category));	
																					  
																					  
				mkdir(PHOTO_PATH . 'ecommerce/' . $folderName);																	  
																		
				$editIDRedirect = $newProduct;		
				$algoliaObj -> ecommerceProductID = $newProduct;						
										
															  							  				
			}
			
			
			$log = new EditHistoryLog();
			$log -> itemID = $editIDRedirect;
			$log -> userID = $_SESSION['user'] -> _userId;
			$log -> itemType = 5;		
			$log -> Save();	
			
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'shop/edit/product/' . $editIDRedirect);
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Ecommerce Product Save Error: " . $e->getMessage();
			$TrackError -> type = "ECOMMERCE PRODUCT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
		}
	}
	
	public function UpdateCategory() {
		try {
		
			$postData = array('productCategory' => 0,
							  'productVisible' => 0,
							  'productModifiedTime' => parent::TimeStamp());										
				
			$this->db->update('ecommerceproducts', $postData, array('productID' => $this -> _id));
				
			
			$log = new EditHistoryLog();
			$log -> itemID = $this -> _id;
			$log -> userID = $_SESSION['user'] -> _userId;
			$log -> itemType = 5;		
			$log -> Save();	
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Update Product Category Error: " . $e->getMessage();
			$TrackError -> type = "UPDATE PRODUCT CATEGORY ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
		}
	}
	
	
	public function FilterProducts() {
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'shop/filteredproducts/' . $this -> FilteredSubmittedContent);	
	}
	
	public function delete() {
		try {
			Folder::DeleteDirectory(PHOTO_PATH . 'ecommerce/' . $this -> Folder);
			
			$ecommercePhoto = new EcommerceProductPhoto();
			$ecommercePhoto -> ProductID = $this->_id;
			$ecommercePhoto -> DeleteByProduct();
			
			$this -> db -> insert('removedecommerceproducts', array('previousProductID' => $this->_id,
																	'previousProductName' => $this -> productName));	
			
			
			//remove history logs
			$logRemove = $this -> db -> prepare("DELETE FROM edithistorylog WHERE relatedItemID = :relatedItemID AND editHistoryType = 5");
			$logRemove -> execute(array(':relatedItemID' => $this->_id));
			
			//delete special from table
			$productRemove = $this -> db -> prepare("DELETE FROM ecommerceproducts WHERE productID = :productID");
			$productRemove -> execute(array(':productID' => $this->_id));
			
			$this -> redirect -> redirectPage(PATH . 'shop/products');
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Product Error: " . $e->getMessage();
			$TrackError -> type = "DELETE PRODUCT ERROR";
			$TrackError -> SendMessage();
		}
	}

	public function publish() {
		if($this -> publishedDate == NULL) {
			$publishedDate = parent::TimeStamp();
		} else {
			$publishedDate = $this -> publishedDate;
		}
		
		$photos = new EcommerceProductPhotosList();
		$mainProductPhoto = $photos -> GetProductMainPhoto($this -> _id);
		
		//this could be used for later
		//$algoliaObj = new AlgoliaHandler();
		//$algoliaObj -> ecommerceProductID = $this -> _id;
		//$algoliaObj -> ecommerceProductPrice = $this -> Price;
		//$algoliaObj -> ecommerceProductName = $this -> productName;
		//$algoliaObj -> ecommerceProductSEOName = $this -> ProductSEOName;
		//$algoliaObj -> ecommerceProductStoreID = $this -> StoreID;
		//$algoliaObj -> ecommerceProductImage = PHOTO_URL . 'ecommerce/' . $this -> FolderName . '/'  . $mainProductPhoto['PhotoName'] . '-l.' . $mainProductPhoto['PhotoExt'];
		//$algoliaObj -> AddUpdateShoppinInventory();
		
		
		$postData = array('productVisible' => 1,
					      'productModifiedTime' => parent::TimeStamp(),
						  'productPublishedDate' => $publishedDate);										
				
					
		$this->db->update('ecommerceproducts', $postData, array('productID' => $this -> _id));
	}
	
	public function unpublish() {
		//this could be used for later
		//$algoliaObj = new AlgoliaHandler();
		//$algoliaObj -> ecommerceProductID = $this -> _id;
		//$algoliaObj -> RemoveSHoppingInventory();
		
		
		$postData = array('productVisible' => 0,
					      'productModifiedTime' => parent::TimeStamp());										
					
		$this->db->update('ecommerceproducts', $postData, array('productID' => $this -> _id));
		
	}

	public function RedirectToProduct() {
		$this -> redirect -> redirectPage(PATH . 'shop/edit/product/'. $this -> _id);
	}

}