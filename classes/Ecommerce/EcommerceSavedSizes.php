<?php

class EcommerceSavedSizes extends BaseObject {
	
	public $productID;
	public $SizeID;
	public $PartNumber;
	public $OrderNumber;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }

	public function GetID() {
		return $this -> _id;
	}
	
	public function Save() {
		try {
			$this -> db -> insert('ecommercesavedsizes', array('relatedProductID' => $this -> productID,
															   'relatedSizeID' => $this -> SizeID,
															   'PartNumber' => $this -> PartNumber,
															   'SizeOrder' => $this -> OrderNumber));	
															   
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Ecommerce Saved Sizes Error: " . $e->getMessage();
			$TrackError -> type = " ECOMMERCE SAVED SIZES ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	
	
	public function delete() {
		try {
			
			//delete special from table
			$saveSizeRemove = $this -> db -> prepare("DELETE FROM ecommercesavedsizes WHERE relatedProductID = :relatedProductID");
			$saveSizeRemove -> execute(array(':relatedProductID' => $this-> productID));
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Product Saved Size Error: " . $e->getMessage();
			$TrackError -> type = "DELETE PRODUCT SAVED SIZE ERROR";
			$TrackError -> SendMessage();
		}
	}

	public function DeleteBySize() {
		try {
			//delete special from table
			$saveSizeRemove = $this -> db -> prepare("DELETE FROM ecommercesavedsizes WHERE relatedSizeID = :relatedSizeID");
			$saveSizeRemove -> execute(array(':relatedSizeID' => $this-> SizeID));
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Product Size Error: " . $e->getMessage();
			$TrackError -> type = "DELETE PRODUCT SAVED SIZE ERROR";
			$TrackError -> SendMessage();
		}
	}



}