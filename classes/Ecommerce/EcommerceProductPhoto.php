<?php

class EcommerceProductPhoto extends BaseObject {
	
	private $_id;
	
	public $ProductID;
	
	public $PhotoName;
	public $PhotoExt;
	public $AltTag;
	public $TitleTag;
	
	public $newPhotoName;
	public $MainProductPhoto;
	
	public $FileUploadName;
	public $FileUploadTemp;
	
	public $WaterMarkApplied;
	
	public $FolderName;
	public $RelatedProductID;
	
	public $rotation;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }

    public static function WithID($photoID) {
        $instance = new self();
        $instance -> _id = $photoID;
        $instance -> loadById();
        return $instance;
    }
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommercephotos LEFT JOIN 	ecommerceproducts ON ecommercephotos.relatedProductID = ecommerceproducts.productID WHERE ecommercephotos.productPhotoID = :productPhotoID');
	    $sth->execute(array(':productPhotoID' => $this -> _id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	public function GetID() {
		return $this -> _id;
	}

	protected function fill(array $row){
		$this -> PhotoName = $row['PhotoName'];
		$this -> PhotoExt = $row['PhotoExt'];
		$this -> AltTag = $row['AltTag'];
		$this -> TitleTag = $row['TitleTag'];
		$this -> MainProductPhoto = $row['productMainProductPhoto'];
		$this -> FolderName = $row['productFolderName'];
		$this -> WaterMarkApplied = $row['photoWarterMarked'];
		$this -> RelatedProductID = $row['productID'];
    }	
	
	private function SmallImageName() {
		return $this -> PhotoName . '-s.' . $this -> PhotoExt;
	}
	
	private function LargeImageName() {
		return $this -> PhotoName . '-l.' . $this -> PhotoExt;
	}
	
	public function Validate() {
		$validationErrors = array();
		
		$similiarImageHame = $this -> db -> prepare('SELECT * FROM ecommercephotos WHERE productPhotoID <> :productPhotoID AND PhotoName = :PhotoName AND relatedProductID = :relatedProductID');
		$similiarImageHame -> execute(array(":productPhotoID" => $this->_id, ":PhotoName" => parent::seoUrl($this -> newPhotoName), ":relatedProductID" => $this -> ProductID));
		

		if(isset($this->_id)) {
			if($similiarImageHame -> fetchAll()) {
				array_push($validationErrors, array("inputID" => 2,
													'errorMessage' => 'This name is associated with a different photo, please type another one'));
			}	
		} else {
			if($this -> ValidateEmptyPhotos()) {
				array_push($validationErrors, array("inputID" => 1,
													'errorMessage' => 'There are Empty file uploads'));
			}
		}		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
	}

	
	public function Upload() {
		$currentPhotos = new EcommerceProductPhotosList();

		$currentProduct = EcommerceProduct::WithID($this -> ProductID);	
		$currentInventoryPhotoCount = count($currentPhotos -> ByProduct($this -> ProductID));
		
		$count = NULL;
		$ImagePathParts = array();
		$ImagePathParts['EcommerceProductFolder'] = $currentProduct -> Folder;		
		
		
		
		$uploadedInventoryPATH = PHOTO_PATH . 'ecommerce/'. $currentProduct -> Folder . '/';
		
		$latestCount = $this -> db -> prepare("SELECT (max(photoUploadLatestCount)) LatestCountUploaded FROM ecommercephotos WHERE relatedProductID = :relatedProductID");
		$latestCount -> execute(array(":relatedProductID" => $this -> ProductID));
		$getLatestCount = $latestCount -> fetch();
		
		if($getLatestCount['LatestCountUploaded'] != NULL) {
			$count = $getLatestCount['LatestCountUploaded'] + 1;
		} else {
			$count = 1;
		}
		
		
		Image::MoveAndRenameImage($this -> FileUploadTemp, 
								  $ImagePathParts,
								  $this -> FileUploadName,
								  parent::seoUrl($currentProduct -> productName . '-'. $count), 
								  12);
			
		$this -> db -> insert('ecommercephotos', array('relatedProductID' => $this -> ProductID,
															  'PhotoName' => parent::seoUrl($currentProduct -> productName . '-'. $count),
															  'PhotoExt' => Image::getFileExt($this -> FileUploadName),
															  'AltTag' => $currentProduct -> productName,
															  'TitleTag' => $currentProduct -> productName,
															  'productMainProductPhoto' => ($currentInventoryPhotoCount == 0 ? 1 : 0),
															  'photoUploadLatestCount' => $count));
			
			
			$newImageNameResizedMedium = parent::seoUrl($currentProduct -> productName . '-'. $count) . '-s.' . Image::getFileExt($this -> FileUploadName);
			$newImageNameResizedLarge = parent::seoUrl($currentProduct -> productName . '-'. $count) . '-l.' . Image::getFileExt($this -> FileUploadName);
			
																		 
			$currentUploadedPhoto = $uploadedInventoryPATH . parent::seoUrl($currentProduct -> productName . '-'. $count) . '.' . Image::getFileExt($this -> FileUploadName);
																		 
			list($origWidth, $origHeight) = getimagesize($currentUploadedPhoto);
																		 
			//small
			if(300 > $origWidth) {
				Image::resizeImage($currentUploadedPhoto, $uploadedInventoryPATH . $newImageNameResizedMedium, $origWidth, $origHeight);			
			} else {
				Image::resizeImage($currentUploadedPhoto, $uploadedInventoryPATH . $newImageNameResizedMedium, 300, 300);	
			}
				
			//large
			if(1200 > $origWidth) {
				Image::resizeImage($currentUploadedPhoto, $uploadedInventoryPATH . $newImageNameResizedLarge, $origWidth, $origHeight);
			} else {
				Image::resizeImage($currentUploadedPhoto, $uploadedInventoryPATH . $newImageNameResizedLarge, 1200, 1200);	
			}															 
																		 
			unlink($uploadedInventoryPATH . parent::seoUrl($currentProduct -> productName . '-'. $count) . '.' . Image::getFileExt($this -> FileUploadName));															
			
			$count++;
		
		$log = new EditHistoryLog();
		$log -> itemID = $this -> ProductID;
		$log -> userID = $_SESSION['user'] -> _userId;
		$log -> itemType = 5;		
		$log -> Save();	
		
		
		$this -> json -> outputJqueryJSONObject('finishedUpload', true);	
	}

	

	

	
	public function Save() {
		
		$postData = array('PhotoName' => parent::seoUrl($this -> newPhotoName),
						  'AltTag' => $this -> AltTag,
						  'TitleTag' => $this -> TitleTag,
						  'productMainProductPhoto' => $this -> MainProductPhoto);	
						
						  
		$this->db->update('ecommercephotos', $postData, array('productPhotoID' => $this -> _id));
		
		
		$updateAllMainPhoto = $this -> db -> prepare('UPDATE ecommercephotos SET productMainProductPhoto = 0 WHERE productPhotoID <> :productPhotoID AND relatedProductID = :relatedProductID');
		$updateAllMainPhoto -> execute(array(":productPhotoID" => $this -> _id, ":relatedProductID" => $this -> RelatedProductID));
		
		
		$uploadedAlbumPATH = PHOTO_PATH . 'ecommerce/'. $this -> FolderName . '/';
		
		$uploadedPATHMedium = $uploadedAlbumPATH . $this -> PhotoName . '-s.' . $this -> PhotoExt;
		$uploadedPATHLarge = $uploadedAlbumPATH . $this -> PhotoName . '-l.' . $this -> PhotoExt;
		
		
		//medium
		Image::renameImage($uploadedAlbumPATH, $uploadedPATHMedium, parent::seoUrl($this -> newPhotoName). '-s');
		
		//large
		Image::renameImage($uploadedAlbumPATH, $uploadedPATHLarge, parent::seoUrl($this -> newPhotoName). '-l');
		
		 
		$this -> json -> outputJqueryJSONObject('saved', true);
	}

	private function GetSmallImageName() {
		return $this -> PhotoName . '-s.' . $this -> PhotoExt;
	}
	
	private function GetLargeImageName() {
		return $this -> PhotoName . '-l.' . $this -> PhotoExt;
	}

	private function SmallImagePATH() {
		return PHOTO_PATH . 'ecommerce/' . $this -> FolderName . '/' . $this -> GetSmallImageName();
	}
	
	private function LargeImagePATH() {
		return PHOTO_PATH . 'ecommerce/' . $this -> FolderName . '/' . $this -> GetLargeImageName();
	}
	
	public function WaterMarkSingle() {
		$currentProduct = EcommerceProduct::WithID($this -> RelatedProductID);
		
		$this -> WaterMarkPhoto($this -> LargeImagePATH(), 
								$currentProduct -> StoreID, 
								$this -> FolderName, 
								$this -> GetLargeImageName());
								
								
		$this -> WaterMarkPhoto($this -> SmallImagePATH(), 
								$currentProduct -> StoreID, 
								$this -> FolderName, 
								$this -> GetSmallImageName());	
								
		$postData = array('photoWarterMarked' => 1);	
		$this->db->update('ecommercephotos', $postData, array('productPhotoID' => $this -> _id));	
		
		$this -> redirect -> redirectPage(PATH . 'shop/editphoto/'. $this -> _id);										
	}
	
	public function RedirectToProduct($id) {
		$this -> redirect -> redirectPage(PATH . 'shop/edit/product/'. $id);
	}
	
	public function WaterMarkAllPhotosByInventory() {
		$photos = new EcommerceProductPhotosList();
		
		$currentProduct = EcommerceProduct::WithID($this -> ProductID);
		foreach($photos -> ByProduct($this -> ProductID) as $photoSingle) {
			if($photoSingle['photoWarterMarked'] == 0) {
					
				$largeName = $photoSingle['PhotoName'] . '-l.' . $photoSingle['PhotoExt'];
				$smallName = $photoSingle['PhotoName'] . '-s.' . $photoSingle['PhotoExt'];
					
				$largeImagePATH = PHOTO_PATH . 'ecommerce/' . $currentProduct -> Folder . '/' . $largeName;
				$smallImagePATH = PHOTO_PATH . 'ecommerce/' . $currentProduct -> Folder . '/' . $smallName;
				
				$this -> WaterMarkPhoto($largeImagePATH, 
										$currentProduct -> StoreID, 
										$currentProduct -> Folder, 
										$largeName);
								
								
				$this -> WaterMarkPhoto($smallImagePATH, 
										$currentProduct -> StoreID, 
										$currentProduct -> Folder, 
										$smallName);
										
										
				$postData = array('photoWarterMarked' => 1);	
				$this->db->update('ecommercephotos', $postData, array('productPhotoID' => $photoSingle['productPhotoID']));						
			}
			
		}
		
		$this -> redirect -> redirectPage(PATH . 'shop/edit/product/' . $this -> RelatedProductID);	
		
	}
	
	
	private function WaterMarkPhoto($img, $storeID, $folder, $newImage) {
		$store = Store::WithID($storeID);
		
		$waterMark = imagecreatefrompng(PHOTO_PATH . 'WaterMark/' . $store -> WaterMarkImage);
		
		$ext = strtolower(Image::getFileExt($img));
    	if ($ext == "gif"){ 
			$image = imagecreatefromgif($img);
    	} else if($ext =="png"){ 
			$image = imagecreatefrompng($img);
    	} else { 
			$image = imagecreatefromjpeg($img);
    	}
		
		
		
		
		$marge_right = 0;
		$marge_bottom = 0;
		$sx = imagesx($waterMark);
		$sy = imagesy($waterMark);
		$sximg = imagesx($image);
		
		$percent = $sximg * 0.15;
		
		
		//Create the final resized watermark stamp
	    $dest_image = imagecreatetruecolor($percent, $percent);
		
		
	    //keeps the transparency of the picture
	    imagealphablending( $dest_image, false );
	    imagesavealpha( $dest_image, true );
	    
	    //resizes the stamp
	    imagecopyresampled($dest_image, $waterMark, 0, 0, 0, 0, $percent, $percent, $sx, $sy);
		  
		
		imagecopy($image, 
				  $dest_image, 
				  imagesx($image) - imagesx($dest_image) - $marge_right, 
				  imagesy($image) - imagesy($dest_image) - $marge_bottom, 
				  0, 
				  0, 
				  $percent, 
				  $percent);
		
		
		imagepng($image, PHOTO_PATH . 'ecommerce/' . $folder . '/' . $newImage);

		
	}
	
	
	Public function DeleteByProduct() {
		$sth = $this -> db -> prepare("DELETE FROM ecommercephotos WHERE relatedProductID = :relatedProductID");
		$sth -> execute(array(':relatedProductID' => $this -> ProductID));
	}
	
	
	public function RotateImage() {
				
		$imageRotatedLarge = PHOTO_PATH . 'ecommerce/' . $this -> FolderName . '/' . $this -> LargeImageName();
		$imageRotatedMedium = PHOTO_PATH . 'ecommerce/' . $this -> FolderName . '/' . $this -> SmallImageName();
		
		$ext = strtolower(Image::getFileExt($this -> PhotoExt));
    	
    	if ($ext == "gif"){
			$sourceLarge = imagecreatefromgif($imageRotatedLarge);
			$sourceMedium = imagecreatefromgif($imageRotatedLarge);	 	 
    	} else if($ext =="png"){
    		$sourceLarge = imagecreatefrompng($imageRotatedLarge);
			$sourceMedium = imagecreatefrompng($imageRotatedLarge);	 
    	} else {
			$sourceLarge = imagecreatefromjpeg($imageRotatedLarge);
			$sourceMedium = imagecreatefromjpeg($imageRotatedLarge);	 
    	}
		
		
		
		switch($this -> rotation) {
			case "right":
				$rotateLarge = imagerotate($sourceLarge, -90, 0);
				$rotateMedium = imagerotate($sourceMedium, -90, 0);		
				break;
			case "left":
				$rotateLarge = imagerotate($sourceLarge, 90, 0);	
				$rotateMedium = imagerotate($sourceMedium, 90, 0);		
				break;
			
		}
		
		
		
		
		imagejpeg($rotateLarge, $imageRotatedLarge);
		
		imagedestroy($sourceLarge);
		imagedestroy($rotateLarge);
		
		imagejpeg($rotateMedium, $imageRotatedMedium);
		
		imagedestroy($sourceMedium);
		imagedestroy($rotateMedium);
		
		$this -> redirect -> redirectPage(PATH. 'shop/editphoto/' . $this -> _id);
	}
	
	
	
	public function Delete() {
		$currentPhotos = new EcommerceProductPhotosList();	
			
		//medium
		unlink(PHOTO_PATH . 'ecommerce/'. $this -> FolderName . '/' . $this -> PhotoName . '-s.' . $this -> PhotoExt);
		//large		
		unlink(PHOTO_PATH . 'ecommerce/'. $this -> FolderName . '/' . $this -> PhotoName . '-l.' . $this -> PhotoExt);
				
				
				
		$sth = $this -> db -> prepare("DELETE FROM ecommercephotos WHERE productPhotoID = :productPhotoID");
		$sth -> execute(array(':productPhotoID' => $this->_id));
		
		if(count($currentPhotos -> ByProduct($this -> RelatedProductID)) == 0) {
			$product = EcommerceProduct::WithID($this -> RelatedProductID);
			$product -> unpublish();
		}
		
		
		$this -> redirect -> redirectPage(PATH . 'shop/edit/product/' . $this -> RelatedProductID);	
	}



}