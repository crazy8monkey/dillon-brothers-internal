<?php

class EcommerceSettingSize extends BaseObject {
	
	private $_id;
	public $SizeAbbr;
	public $SizeText;
	
	public $categoryName;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }

    public static function WithID($categoryID) {
        $instance = new self();
        $instance -> _id = $categoryID;
        $instance -> loadById();
        return $instance;
    }
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommercesettingsizes WHERE settingSizeID = :settingSizeID');
	    $sth->execute(array(':settingSizeID' => $this -> _id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	public function GetID() {
		return $this -> _id;
	}

	protected function fill(array $row){
		$this -> SizeAbbr = $row['settingSizeAbbr'];
		$this -> SizeText = $row['settingSizeText'];
    }	
	
	public function Validate() {
		$validationErrors = array();
		
		if(!isset($this -> _id)) {
			$duplicateAbbr = $this -> db -> prepare('SELECT settingSizeAbbr FROM ecommercesettingsizes WHERE settingSizeAbbr = ?');
			$duplicateAbbr -> execute(array($this -> SizeAbbr));
			
			$duplicateSize = $this -> db -> prepare('SELECT settingSizeText FROM ecommercesettingsizes WHERE settingSizeText = ?');
			$duplicateSize -> execute(array($this -> SizeText));
		} else {
			$duplicateAbbr = $this -> db -> prepare('SELECT settingSizeAbbr FROM ecommercesettingsizes WHERE settingSizeAbbr = :settingSizeAbbr AND settingSizeID <> :settingSizeID');
			$duplicateAbbr -> execute(array(":settingSizeAbbr" => $this -> SizeAbbr, ":settingSizeID" => $this -> _id));
			
			$duplicateSize = $this -> db -> prepare('SELECT settingSizeText FROM ecommercesettingsizes WHERE settingSizeText = :settingSizeText AND settingSizeID <> :settingSizeID');
			$duplicateSize -> execute(array(":settingSizeText" => $this -> SizeText, ":settingSizeID" => $this -> _id));			
		}
		
		if($this -> validate -> emptyInput($this -> SizeAbbr)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		} else if($duplicateAbbr -> fetch()) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Duplicate Abbreviation'));
		}
		
		if($this -> validate -> emptyInput($this -> SizeText)) {
			array_push($validationErrors, array('inputID' => 2,
												'errorMessage' => 'Required'));
		} else if($duplicateSize -> fetch()) {
			array_push($validationErrors, array('inputID' => 2,
												'errorMessage' => 'Duplicate Size'));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
	}

	
	public function Save() {
		try {
			
			if(isset($this -> _id)) {
				$redirectID = $this -> _id;
				$postData = array('settingSizeAbbr' => $this -> SizeAbbr,
								  'settingSizeText' => $this -> SizeText);										
					
				$this->db->update('ecommercesettingsizes', $postData, array('settingSizeID' => $this -> _id));
				
			} else {
				$newSize = $this -> db -> insert('ecommercesettingsizes', array('settingSizeAbbr' => $this -> SizeAbbr,
																	 			'settingSizeText' => $this -> SizeText));	
				$redirectID = $newSize;													 
			}
				
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'shop/edit/size/'. $redirectID);
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Setting Size Save Error: " . $e->getMessage();
			$TrackError -> type = "SETTING SIZE SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
		}
	}
	
	
	
	public function delete() {
		try {
			$savedProductSized = new EcommerceSavedSizes();
			$savedProductSized -> SizeID = $this->_id;
			$savedProductSized -> DeleteBySize();
		
			$sizeRemove = $this -> db -> prepare("DELETE FROM ecommercesettingsizes WHERE settingSizeID = :settingSizeID");
			$sizeRemove -> execute(array(':settingSizeID' => $this->_id));
			
			$this -> redirect -> redirectPage(PATH . 'shop/settings');
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Ecommerce Setting Size Error: " . $e->getMessage();
			$TrackError -> type = "DELETE ECOMMERCE SETTING SIZE ERROR";
			$TrackError -> SendMessage();
		}
	}


}