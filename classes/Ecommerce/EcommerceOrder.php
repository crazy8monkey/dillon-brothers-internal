<?php

class EcommerceOrder extends BaseObject {

	private $_id;
	public $firstName;
	public $LastName;
	public $PhoneNumber;
	public $Email;
	public $ShippingAddress;
	public $ShippingCity;
	public $ShippingState;
	public $ShippingZip;
	
	public $StorePickup;
	
	public $TaxRate;
	public $ShippingRate;
	public $GrandTotal;
	
	public $orderItems;
	
	public $CustomerStripeID;
	public $PaymentStripeID;
	
	public $TrackingNumber;
	public $ShippingCompany;
	
	public $ShipStatus;
	
	public $orders = array();
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }
	
	public function GetID() {
		return $this -> _id;
	}
	
	 public static function WithID($orderID) {
        $instance = new self();
        $instance -> _id = $orderID;
        $instance -> loadById();
        return $instance;
    }
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommerceorders WHERE orderID = :orderID');
	    $sth->execute(array(':orderID' => $this -> _id));	
        $record = $sth -> fetch();
        $this->fill($record);
		$this->GenerateOrderList();
    }
	
	protected function fill(array $row){
		$this -> firstName = $row['firstName'];
		$this -> LastName = $row['lastName'];
		$this -> PhoneNumber = $row['phoneNumber'];
		$this -> Email = $row['orderEmail'];
		$this -> StorePickup = $row['storePickup'];
		$this -> orderItems = $row['orderSummary'];
		$this -> PaymentStripeID = $row['stripeChargeID'];
		$this -> CustomerStripeID = $row['stripeCustomerID'];
		$this -> ShippingAddress = $row['shippingAddress'];
		$this -> ShippingCity = $row['shippingCity'];
		$this -> ShippingState = $row['shippingState'];
		$this -> ShippingZip = $row['shippingZip'];
		$this -> ShipStatus = $row['shippedStatus'];
		$this -> TrackingNumber = $row['ShippingNumber'];
		$this -> ShippingCompany = $row['ShippingCompany'];
		$this -> TaxRate = $row['taxRates'];
		$this -> ShippingRate = $row['shippingRate'];
		$this -> GrandTotal = $row['grandTotal'];
    }	
	
	private function GenerateOrderList() {
		$tempTable = $this -> db -> prepare('CREATE TEMPORARY TABLE OrderItems (
													orderItemID INT NOT NULL AUTO_INCREMENT, 
													productID INT NOT NULL,
													quantity INT NOT NULL,
													price DECIMAL(10,2) NOT NULL,
													color INT NOT NULL,
													size INT NOT NULL,
													PartNumber VARCHAR(500),
													PRIMARY KEY (orderItemID)
																				);');
		$tempTable -> execute();
		
		foreach(json_decode($this -> orderItems, true) as $orderItemSingle) {
			$this -> db -> insert('OrderItems', array('productID' => $orderItemSingle['product_id'],
													  'quantity' => $orderItemSingle['quantity'],
													  'price' => $orderItemSingle['price'],
													  'color' => $orderItemSingle['colorSelected'],
													  'size' => $orderItemSingle['sizeSelected'],
													  'PartNumber' => $orderItemSingle['PartNumber']));
		}
		
		$this -> orders = $this -> db -> select('SELECT ecommerceproducts.productName, 
														OrderItems.quantity, 
														ecommercesettingcolors.settingColorText, 
														OrderItems.price,
														ecommercesettingsizes.settingSizeText,
														OrderItems.PartNumber,
														removedecommerceproducts.previousProductName FROM OrderItems 
														LEFT OUTER JOIN ecommerceproducts ON OrderItems.productID = ecommerceproducts.productID
														LEFT OUTER JOIN removedecommerceproducts ON OrderItems.productID = removedecommerceproducts.previousProductID
														LEFT OUTER JOIN ecommercesettingcolors ON OrderItems.color = ecommercesettingcolors.settingColorID
														LEFT OUTER JOIN ecommercesettingsizes ON OrderItems.size = ecommercesettingsizes.settingSizeID');
	}
	
	public function ValidateShipStatus() {
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> TrackingNumber)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		}
		
		if($this -> ShippingCompany == 0) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function MarkAsShipped() {
		$postData = array('shippedStatus' => 1,
						  'ShippingNumber' => $this -> TrackingNumber,
						  'ShippingCompany' => $this -> ShippingCompany);		
		$this->db->update('ecommerceorders', $postData, array('orderID' => $this->_id));
		
		$log = new EditHistoryLog();
		$log -> itemID = $this->_id;
		$log -> userID = $_SESSION['user'] -> _userId;
		$log -> itemType = 6;		
		$log -> Save();	
		
		if(LIVE_SITE == true) {
			$email = new Email();
			
			if($this -> ShippingCompany == 1) {
				$url = "https://wwwapps.ups.com/tracking/tracking.cgi?tracknum=" . $this -> TrackingNumber;
			} else if($this -> ShippingCompany == 2) {
				$url = "https://tools.usps.com/go/TrackConfirmAction_input?strOrigTrackNum=" . $this -> TrackingNumber;
			}
			$email -> to = $this -> Email;
			$email ->subject = "Track Your Order";
			$email -> TrackOrder($url);
		}
		
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'shop/vieworder/'. $this->_id);
		
	}






}