<?php

require CLASSES . 'BaseObject.php';
require CLASSES . 'EmailServerErrors.php';
require CLASSES . 'PhotoAlbum.php';
require CLASSES . 'Photo.php';
require CLASSES . 'User.php';
require CLASSES . 'Event.php';
require CLASSES . 'Newsletter.php';
require CLASSES . 'Employee.php';
require CLASSES . 'Special.php';
require CLASSES . 'Brand.php';
require CLASSES . 'VehicleSpecial.php';
require CLASSES . 'PartsAccessoryBrand.php';
require CLASSES . 'Store.php';
require CLASSES . 'BlogPost.php';
require CLASSES . 'BlogCategory.php';
require CLASSES . 'BlogPostComment.php';
require CLASSES . 'Settings.php';
require CLASSES . 'SocialMedia.php';

require CLASSES . 'InventoryBackup.php';
require CLASSES . 'BackupVinNumbers.php';
require CLASSES . 'SpecGroup.php';
require CLASSES . 'SpecLabel.php';
require CLASSES . 'DataConversion.php';
require CLASSES . 'ConversionRule.php';
require CLASSES . 'InventoryPhotos.php';

require CLASSES . 'InventoryCategoryObject.php';
require CLASSES . 'FeedBackObject.php';
require CLASSES . 'RelatedSpecialItem.php';
require CLASSES . 'EventPDF.php';
require CLASSES . 'YearPhotoDirectory.php';
require CLASSES . 'EditHistoryLog.php';
require CLASSES . 'GoogleAnalytics.php';
require CLASSES . 'ReviewSingle.php';
require CLASSES . 'ColorConversion.php';
require CLASSES . 'ShortLinks.php';
require CLASSES . 'EmailLogObject.php';
require CLASSES . 'BrandContent.php';
require CLASSES . 'SettingSMSNumber.php';
require CLASSES . 'AlgoliaHandler.php';

//ecommerce
require CLASSES . 'Ecommerce/EcommerceProduct.php';
require CLASSES . 'Ecommerce/EcommerceSavedSizes.php';
require CLASSES . 'Ecommerce/EcommerceSavedProductColors.php';
require CLASSES . 'Ecommerce/EcommerceProductPhoto.php';
require CLASSES . 'Ecommerce/EcommerceSettingColor.php';
require CLASSES . 'Ecommerce/EcommerceSettingSize.php';
require CLASSES . 'Ecommerce/EcommerceSettingCategory.php';
require CLASSES . 'Ecommerce/EcommerceSettingGeneral.php';
require CLASSES . 'Ecommerce/EcommerceOrder.php';
//main inventory
require CLASSES . 'MainInventory/Inventory.php';
require CLASSES . 'MainInventory/SavedColor.php';
require CLASSES . 'MainInventory/ColorObject.php';
require CLASSES . 'MainInventory/GenericColorText.php';
//twilio logic
require CLASSES . 'Twilio/TextMessageConversationCustomer.php';
require CLASSES . 'Twilio/TwilioHandler.php';
require CLASSES . 'Twilio/OutBoundTextMessage.php';
require CLASSES . 'Twilio/TwilioChatChannel.php';
require CLASSES . 'Twilio/TwilioChatChannelMember.php';
require CLASSES . 'Twilio/ChatConversation.php';
require CLASSES . 'Twilio/ChatConversationMessage.php';
