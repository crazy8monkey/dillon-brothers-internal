<?php


class GoogleAnalytics extends BaseObject {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	


    protected function fill(array $row){
    
    }
	
	
	
	public function GetAnalytics() {
		if(LIVE_SITE == true) {
			require '/var/www/vhosts/dillon-brothers.com/GoogleAPI/autoload.php';	
		} else {
			require 'libs/GoogleAPI/autoload.php';	
		}
				
		$googleClient = new Google_Client();
		
		
		$googleClient->setApplicationName("Dillon Brothers Analytics");
		$googleClient->setAuthConfig('client_secret_analytics.json');
		$googleClient->setDeveloperKey('AIzaSyDferrOQ_amLV4_jMveTWhCpM5lwOzEmNg');
  		$googleClient->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
  		$googleClient->setClientId('941371420468-cv79omb9lte6obg7c26a8r6smhbto56j.apps.googleusercontent.com');
		$analytics = new Google_Service_Analytics($googleClient);
	
		
	
		return $analytics;
		
	}
	
	public function GetToken() {
		if(LIVE_SITE == true) {
			require '/var/www/vhosts/dillon-brothers.com/GoogleAPI/autoload.php';	
		} else {
			require 'libs/GoogleAPI/autoload.php';	
		}
				
		$googleClient = new Google_Client();
		
		
		$googleClient->setApplicationName("Dillon Brothers Analytics");
		$googleClient->setClientId('941371420468-cv79omb9lte6obg7c26a8r6smhbto56j.apps.googleusercontent.com');
		$googleClient->setAuthConfig('client_secret_analytics.json');
		$googleClient->setRedirectUri(PATH . 'dashboard/getAnalytics');
  		$googleClient->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
  		$googleClient->setAccessType('offline');

		$googleClient->refreshTokenWithAssertion();
  		$token = $googleClient->getAccessToken();
  		$accessToken = $token['access_token'];
	
		
	
		return $accessToken;
	}

	

}