<?php


class Newsletter extends BaseObject {
	
	private $_id;
	
	public $thumbNail;
	public $name;
	public $date;
	public $folderName;
	public $Visible;

    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($newsletterID) {
        $instance = new self();
        $instance->_id = $newsletterID;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM newsletter WHERE NewsLetterID = :NewsLetterID');
        $sth->execute(array(':NewsLetterID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
 		$this -> thumbNail = $row['ThumbNail'];
		$this -> name = $row['Name'];
		$this -> date = $row['DateSent'];
		$this -> folderName = $row['folderName'];
		$this -> Visible = $row['newsletterVisible'];
    }
	
	

	
	public function Validate() {
		$validationErrors = array();
		
		if(!isset($this -> _id)) {			
			if($this -> validate -> emptyInput($this -> thumbNail)) {
				array_push($validationErrors, array('inputID' => 1,
													'errorMessage' => 'Required'));
			}
			
			
			if($this -> validate -> emptyInput($this -> name)) {
				array_push($validationErrors, array('inputID' => 3,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> date)) {
				array_push($validationErrors, array('inputID' => 4,
													'errorMessage' => 'Required'));
			}
		
		}			
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function GetID() {
		return $this -> _id;
	}
	
	public function GetThumbNail() {
		return PATH . NEWSLETTER_PATH . $this -> folderName . '/' .$this -> thumbNail;
	}
	
	public function Save() {
		try {
			
			if(isset($this -> _id)) {
				
				
				
				//$postData = array('link' => $this -> link);	
				//$this->db->update('newsletter', $postData, array('NewsLetterID' => $this -> _id));
				
				
			} else {
				$newFolder = NEWSLETTER_PATH . parent::seoUrl($this -> date . '-' . $this -> name);
				mkdir($newFolder);
				
				$uploadedThumb = $newFolder . '/' . $this -> thumbNail . '/' . $this -> thumbNail;
				
				move_uploaded_file($_FILES['newsletterThumb']['tmp_name'], $newFolder . '/' . $this -> thumbNail);
				
				
				//renameImage($albumName, $imageUploaded, $newName)
				
				$newImageName = Image::renameImage($newFolder. '/', $newFolder. '/' . $this -> thumbNail, parent::seoUrl($this -> name));
				
				$this -> db -> insert('newsletter', array('Name' => $this -> name,
														  'DateSent' => $this -> date,
														  "folderName" => parent::seoUrl($this -> date . '-' . $this -> name),
														  'ThumbNail' => parent::seoUrl($this -> name) . '.' .Image::getFileExt($this -> thumbNail)));
			}
			
			
			
			$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> newsletters());
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Newsletter Save Error: " . $e->getMessage();
			$TrackError -> type = "NEWSLETTER SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}

	public function Publish() {
		$postData = array('newsletterVisible' => 1);	
		$this->db->update('newsletter', $postData, array('NewsLetterID' => $this -> _id));
		$this -> redirect -> redirectPage(PATH. 'newsletters/edit/' . $this -> _id);
	}
	
	public function UnPublish() {
		$postData = array('newsletterVisible' => 0);	
		$this->db->update('newsletter', $postData, array('NewsLetterID' => $this -> _id));
		$this -> redirect -> redirectPage(PATH. 'newsletters/edit/' . $this -> _id);
	}
	
	

		

}