<?php

class Special extends BaseObject {
	
	
	private $_id;
	public $_currentImageBig;
	public $_currentImageSmall;
	
	public $SpecialImageBig;
	public $SpecialImageSmall;
	public $SpecialTitle;
	public $SpecialDescription;
	public $SpecialDescriptionFooter;
	
	public $SpecialCategory;
	
	public $SpecialStartDate;
	public $MetaData;
	
	public $_currentStartDate;
	public $_currentExpiredDate;
	
	public $NewExpiredDate;
	
	//1 = Honda
	//2 = KTM
	//3 = Suzuki
	//4 = Triumph
	//5 = Yamaha
	//6 = Kawasaki
	//7 = Harley
	//8 = Indian
	public $OEMBrandID;
	
	private $VehicleSpecialBrand; 
	private $PartsSpecialBrand;
	private $IsServiceSpecial;
	
	public $BrandName;
	public $BrandNameSEOurl;
	
	public $PartsBrandName;
	public $PartsBrandSEOUrl;
	
	
	public $VehicleSpecial = false;
	public $PartsSpecial = false;
	public $ServiceSpecial = false;
	
	public $currentCategory;
	public $RelatedStores;
	
	public $StoreName;
	
	public $FolderName;
	//1 = vehicle
	public $RelatedInventoryType;
	
	public $RelatedSpecialID;
	public $RelatedInventoryID;
	public $RelatedSpecialItemName;
	public $RelatedSpecialType;
	public $RelatedSpecialTypeValue;
	public $RelatedSpecialExpiredDate;
	public $RelatedDescription;
	public $RelatedDisclaimer;
	//1 = oem
	//2 = clothes
	//3 = parts
	//4 = service
	//5 = store
	public $SpecialType;
	
	public $SpecialIsActive;
	
	public $PublishedDate;
	
	public $NewEndDate;
	
	public $PartAcessoryBrandID;
	public $PartAccessoryBrandName;
	
	public $LinkedInventoryIDS;
	public $OEMBrandName;
	public $OEMBrandStoreID;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($specialID) {
        $instance = new self();
        $instance->_id = $specialID;
        $instance->loadById();
        return $instance;
    }
	
	
	public function GetStores() {
		return $this -> db -> select("SELECT * FROM specialrelatedstores WHERE CreatedSpecialID = " . $this -> _id);
	}
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM specials LEFT JOIN partaccessorybrand ON specials.PartsApparelID = partaccessorybrand.partAccessoryBrandID LEFT JOIN brands ON specials.OEMBrandID = brands.BrandID WHERE SpecialID = :SpecialID');
	    $sth->execute(array(':SpecialID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	

	protected function fill(array $row){
		
		$this -> SpecialTitle = $row['SpecialTitle'];
		$this -> SpecialDescription = $row['SpecialDescription'];
		$this -> SpecialDescriptionFooter = $row['SpecialDescriptionFooter'];
		$this -> _currentImageBig = $row['SpecialMainImage'];
		$this -> _currentImageSmall = $row['specialThumbImage'];
		$this -> _currentExpiredDate = $row['SpecialEndDate'];
		$this -> FolderName = $row['FolderName'];
		$this -> RelatedInventoryType = $row['RelatedInventoryType'];
		$this -> SpecialCategory = $row['specialType'];
		$this -> _currentStartDate = $row['SpecialStartDate'];
		$this -> _currentExpiredDate = $row['SpecialEndDate'];
		$this -> SpecialIsActive = $row['isSpecialsActive'];
		$this -> PublishedDate = $row['specialPublishedDate'];
		$this -> PartAcessoryBrandID = $row['partAccessoryBrandID'];
		$this -> PartAccessoryBrandName = $row['brandName'];
		$this -> OEMBrandName = $row['BrandName'];
		$this -> OEMBrandID = $row['BrandID'];
		$this -> OEMBrandStoreID = $row['Store'];
    }	
		
	public function RelatedStores() {
		$string = NULL;
		foreach($this -> GetStores() as $Store) {
			$string .= $Store['RelatedStoreID'] . ',';	
		}
		return rtrim($string, ',');
	}
	
	
	private function GenerateSpecialCategoryLink() {
		return "<a href='" . $this -> pages -> specials() . "'>Types of Specials</a>";
	} 
	
	private function GenerateCategoryLink() {
		$categoryLink = NULL;
		
		
		switch($this -> SpecialCategory) {
			case 1:
				$categoryLink = "<a href='". $this -> pages -> specials() . "/category/OEM'>OEM Specials</a>";	
				break;		
			case 2:
				if($this -> PartAcessoryBrandID == 0) {
					$categoryLink = "<a href='". $this -> pages -> specials() . "/category/Apparel'>Apparel Specials</a>";	
				} else {
					$categoryLink = "<a href='". $this -> pages -> specials() . "/category/Apparel'>Apparel Brands</a>";	
				}
				
					
				break;	
			case 3:
				if($this -> PartAcessoryBrandID == 0) {
					$categoryLink = "<a href='". $this -> pages -> specials() . "/category/Parts'>Parts Specials</a>";
				} else {
					$categoryLink = "<a href='". $this -> pages -> specials() . "/category/Parts'>Parts Brands</a>";	
				}
					
				break;
					
			case 4:
				$categoryLink = "<a href='". $this -> pages -> specials() . "/category/Service'>Service Specials</a>";
				break;
						
			case '5':
				$categoryLink = "<a href='". $this -> pages -> specials() . "/category/Store'>Store Specials</a>";
				break;	
		}
		
		
		return $categoryLink;
	}
	
	
	private function GetBrandListBreadCrumbs() {
		$BrandName = NULL;
		
		switch($this -> SpecialCategory) {
			case 1:
				$BrandName = "<a href='". $this -> pages -> specials() . "/OEM/" . $this -> OEMBrandID . "'>" . $this -> OEMBrandName . " Specials</a>";	
				break;	
			case 2:
				$BrandName = "<a href='". $this -> pages -> specials() . "/brand/" . $this -> PartAcessoryBrandID . "/Apparel'>" . $this -> PartAccessoryBrandName . " Specials</a>";	
				break;	
			case 3:
				$BrandName = "<a href='". $this -> pages -> specials() . "/brand/" . $this -> PartAcessoryBrandID . "/Parts'>" . $this -> PartAccessoryBrandName . " Specials</a>";	
				break;
					
		}
		
		
		return $BrandName;
	}
	
	private function GetSpecialLink() {
		$specialLink = NULL;
		if($this -> VehicleSpecialBrand != NULL) {
			$specialLink = "<a href='". $this -> pages -> specials() . "/edit/VehicleSpecial/" . $this -> _id . "'>" . $this -> SpecialTitle . "</a>";
		}
		
		if($this -> PartsSpecialBrand != NULL) {
			$specialLink = "<a href='". $this -> pages -> specials() . "/edit/PartsSpecial/" . $this -> _id . "'>" . $this -> SpecialTitle . "</a>";
		}
		
		if($this -> IsServiceSpecial != NULL) {
			$specialLink = "<a href='". $this -> pages -> specials() . "/edit/ServiceSpecial/" . $this -> _id . "'>" . $this -> SpecialTitle . "</a>";
		}
		return $specialLink;
	}
	
	private function AddEditInventive($edit = false) {
		$addVehicleOrPartText = NULL;	
		if($this -> VehicleSpecialBrand != NULL) {
			if($edit == true) {
				$addVehicleOrPartText = "Edit Vehicle";	
			} else {
				$addVehicleOrPartText = "Add Vehicles";
			}
			
		}
		
		if($this -> PartsSpecialBrand != NULL) {
			if($edit == true) {
				$addVehicleOrPartText = "Edit Part/Accessory";	
			} else {
				$addVehicleOrPartText = "Add Parts/Accessories";
			}
		}
		
		if($this -> IsServiceSpecial != NULL) {
			if($edit == true) {
				$addVehicleOrPartText = "Edit Service Incentive";	
			} else {
				$addVehicleOrPartText = "Add Service Incentives";
			}
		}
		return $addVehicleOrPartText;
	}
	
	public function GetEditSpecialsBreadcrumbs() {
		return $this -> GenerateSpecialCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GenerateCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetBrandListBreadCrumbs() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" .$this -> SpecialTitle;
	}
	
	public function GetEditServiceSpecialBreadCrumb() {
		return $this -> GenerateSpecialCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetBrandListBreadCrumbs() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . $this -> SpecialTitle;
	}
	
	
	public function GetAddPartsToSpecialBreadCrumbs($edit = false) {
		return $this -> GenerateSpecialCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GenerateCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetBrandListBreadCrumbs() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetSpecialLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> AddEditInventive(); 
	}

	public function GetEditVehicleIncentiveBreadCrumbs($edit = false) {
		return $this -> GenerateSpecialCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GenerateCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetBrandListBreadCrumbs()  . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetSpecialLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> AddEditInventive($edit);
	}


	public function GetAddServiceIncentiveBreadCrumbs() {
		return $this -> GenerateSpecialCategoryLink()  . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetBrandListBreadCrumbs()  . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetSpecialLink()  . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>Add Service Incentives";
	}

	public function GetEditPartsSpecialBreadCrumbs() {
		if($this -> PartAcessoryBrandID == 0) {
			return $this -> GenerateSpecialCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
				   $this -> GenerateCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . $this -> SpecialTitle;	
		} else {
			return $this -> GenerateSpecialCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
				   $this -> GenerateCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
				   $this -> GetBrandListBreadCrumbs() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . $this -> SpecialTitle;	
		}
		
		
	}
	
	public function GetEditPartIncentiveToSpecialBreadCrumbs($edit = false) {
		return $this -> GenerateSpecialCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GenerateCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetBrandListBreadCrumbs()  . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetSpecialLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> AddEditInventive($edit);
	}

	public function GetEditServiceIncentiveToSpecialBreadCrumbs($edit = false) {
		return $this -> GenerateSpecialCategoryLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" .
			   $this -> GetBrandListBreadCrumbs() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" . 
			   $this -> GetSpecialLink() . "<span><i class='fa fa-caret-right' aria-hidden='true'></i></span>" .  
			   $this -> AddEditInventive($edit);
	}



	public function GetID() {
		return $this -> _id;
	}



	public function Validate() {
		
		$validationErrors = array();
		if(!isset($this -> _id)) {
			$duplicateName = $this -> db -> prepare('SELECT * FROM specials WHERE SpecialTitle = ?');
			$duplicateName -> execute(array($this -> SpecialTitle));
		} else {
			$duplicateName = $this -> db -> prepare('SELECT * FROM specials WHERE SpecialTitle = :SpecialTitle AND SpecialID <> :SpecialID');
			$duplicateName -> execute(array(":SpecialTitle" => $this -> SpecialTitle, ":SpecialID" => $this -> _id));
		}
			
		
		
		
		if($this -> validate -> emptyInput($this -> SpecialTitle)) {
			array_push($validationErrors, array('inputID' => 2,
												'errorMessage' => 'Required'));
		} else if($duplicateName -> fetch()) {
			array_push($validationErrors, array('inputID' => 2,
												'errorMessage' => 'There is already this name associated to a different special, please type in another name'));
		}
		
		if($this -> validate -> emptyInput($this -> SpecialDescription)) {
			array_push($validationErrors, array('inputID' => 3,
												'errorMessage' => 'Required'));
		}
		
		
		
		if($this -> SpecialCategory != 1) {
			if(!isset($this -> Stores)) {
				array_push($validationErrors, array("inputID" => 4,
													'errorMessage' => 'Required'));
			}	
		}
		
		if($this -> RelatedInventoryType == 0) {
			array_push($validationErrors, array('inputID' => 5,
												'errorMessage' => 'Required'));
		}	
		
		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
		
	}

	public function ValidateRelatedItems() {
		$validationErrors = array();
		
		$multipleSpecialType = false;
		
		foreach($this -> RelatedSpecialType as $key => $value) {
			$specialType = $this -> db -> prepare("SELECT * FROM relatedspecialitems WHERE relatedSpecialID = :relatedSpecialID AND RelatedSpecialItemID <> :RelatedSpecialItemID AND RelatedItemSpecialType = :RelatedItemSpecialType");
			$specialType -> execute(array(":relatedSpecialID" => $this -> _id,
										  ":RelatedSpecialItemID" => $this -> RelatedSpecialID[$key],
										  ":RelatedItemSpecialType" => $value));
										  
			if($specialType -> rowCount() > 0) {
				$multipleSpecialType = true;
				break;
			}							  
										  	
		}
		
	
		if($this -> ValidateMultiple($this -> RelatedSpecialType)) {
			
			array_push($validationErrors, array('inputID' => 11,
												'errorMessage' => 'There are special types that has not been selected'));
					
		} else if($multipleSpecialType == true) {
			array_push($validationErrors, array('inputID' => 11,
												'errorMessage' => 'There are already special type selected in this special, please select unique special types'));
		}	
		
		if($this -> ValidateMultiple($this -> RelatedSpecialTypeValue)) {
				array_push($validationErrors, array('inputID' => 12,
													'errorMessage' => 'There are special types that has not been selected'));
					
		} 	

		if($this -> ValidateMultiple($this -> RelatedSpecialExpiredDate)) {
				array_push($validationErrors, array('inputID' => 13,
													'errorMessage' => 'There are Expired Date that has not been selected'));
					
		} 
		
		if($this -> ValidateMultiple($this -> RelatedDescription)) {
				array_push($validationErrors, array('inputID' => 14,
													'errorMessage' => 'There are descriptions that has not been selected'));
					
		} 
		
		if($this -> ValidateMultiple($this -> RelatedDisclaimer)) {
				array_push($validationErrors, array('inputID' => 15,
													'errorMessage' => 'There are disclaimer that has not been selected'));
					
		} 
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function ValidateRelatedItemsInventory() {
		$validationErrors = array();
		
		//value messages
		$errorValueMessages = array();	
		$values = array();
		
		$showValueErrors = false;
		foreach($this -> RelatedSpecialTypeValue as $key => $valueSingle) {
			if(empty($valueSingle)) {
				$valueMsg = "Please type in a value";
				$showValueErrors = true;
			} else {
				$valueMsg = "";
			}
				
				
			array_push($errorValueMessages, array("key" => $key,
												  "ValueMessage" => $valueMsg));	
			
			array_push($values, $key);
			
		}		
		
		if($showValueErrors == true) {
			array_push($validationErrors, array('ValueErrors' => $errorValueMessages));	
		}			
					
		//type errors 	
		$errorTypeMessages = array();
		$types = array();
		$showTypeErrors = false;
		foreach($this -> RelatedSpecialType as $specialTypekey => $typeSingle) {
			array_push($types, $this -> RelatedSpecialTypeValue[$specialTypekey] . ' ' . $typeSingle);
			
			if(empty($typeSingle)) {
				$typeMsg = "Please select percentage off/dollar off/special financing";
				$showTypeErrors = true;
			} else {
				$typeMsg = "";	
			}
				
			array_push($errorTypeMessages, array("key" => $specialTypekey,
												 "TypeMessage" => $typeMsg));	
		}
		
		if($showTypeErrors == true) {
			array_push($validationErrors, array('TypeErrors' => $errorTypeMessages));	
		}	
		
		//expired date messages
		$errorExpiredDate = array();
		$showExpiredDateErrors = false;
		foreach($this -> RelatedSpecialExpiredDate as $key => $dateSingle) {
			if(empty($dateSingle)) {
				$msg = "Please select an expired date for this type of special";
				$showExpiredDateErrors = true;
			} else if(new DateTime($dateSingle) < new DateTime(date("Y-m-d", $this -> time -> NebraskaTime()))) {
				$msg = "Expired Date needs to be in the future";
				$showExpiredDateErrors = true;
			} else {
				$msg = "";
			}
				
				
			array_push($errorExpiredDate, array("key" => $key,
													"ExpiredMessage" => $msg));	
		}
		if($showExpiredDateErrors == true) {
			array_push($validationErrors, array('ExpiredErrors' => $errorExpiredDate));	
		}	
		
		
		//error descriptions
		$errorDescriptions = array();
		$showDescriptionErrors = false;
		foreach($this -> RelatedDescription as $key => $descriptionSingle) {
			if(empty($descriptionSingle)) {
				$descripMsg = "Please enter the secondar offer description";
				$showDescriptionErrors = true;
			} else {
				$descripMsg = "";
			}
		
			array_push($errorDescriptions, array("key" => $key,
												 "DescriptionMessage" => $descripMsg));	
		}
		
		if($showDescriptionErrors == true) {
			array_push($validationErrors, array('DescriptionErrors' => $errorDescriptions));	
		}	
		
					
		//erro disclaimer			
		$errorDisclaimer = array();
		$showDisclaimerErrors = false;
		foreach($this -> RelatedDisclaimer as $key => $disclaimerSingle) {
			if(empty($disclaimerSingle)) {
				$disclamerMsg = "Please enter the discliamer";
				$showDisclaimerErrors = true;
			} else {
				$disclamerMsg = "";
			}
				
			array_push($errorDisclaimer, array("key" => $key,
											   "DisclaimerMessage" => $disclamerMsg));	
		}
		
		if($showDisclaimerErrors == true) {
			array_push($validationErrors, array('DisclaimerErrors' => $errorDisclaimer));	
		}	
		
			
					
		//$errorLinkedInventory = array();
		//$showLinkedErrors = false;
		
		//if(isset($this -> LinkedInventoryIDS)) {
		//	foreach($this -> LinkedInventoryIDS as $linkedSingle) {
		//		if(empty($linkedSingle)) {
		//			$linkedMsg = "Please drag inventory to this particular special";	
		//			$showLinkedErrors = true;
		//		} else {
		//			$linkedMsg = "";
		//		}	
		///		
		//		array_push($errorLinkedInventory, array("key" => $key,
		//										   "inventoryMessage" => $linkedMsg));
		//	}
		//	
		//	if($showLinkedErrors == true) {
		//		array_push($validationErrors, array('LinkedInventoryErrors' => $errorLinkedInventory));	
		//	}			
		//}
				
		
		
		$incentiveCombinations = array_combine($values, $types);
		$errorDuplicates = array();
		$prevCombination = NULL;
		$showPreviousCombinationErrors = false;
		foreach($incentiveCombinations as $combinationKey => $value) {
			if($prevCombination == $value) {
				$msg = "This combination already exists in a previous incentive, please type in a unique incentive";
				$showPreviousCombinationErrors = true;
			} else {
				$msg = "";
			}
			
			$prevCombination = 	$value;
			array_push($errorDuplicates, array("key" => $combinationKey,
											   "DuplicateMessage" => $msg));
		}
		
		if($showPreviousCombinationErrors == true) {
			array_push($validationErrors, array('DuplicateCombinationErrors' => $errorDuplicates));	
		}
		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function UpdateSpecialType() {
		$postData = array('specialType' => $this -> SpecialCategory);										
		$this->db->update('specials', $postData, array('SpecialID' => $this -> _id));
	}
	
	private function GenerateEditCategory() {
		$categoryString = NULL;
		
		switch($this -> SpecialCategory) {
			case 1:
				$categoryString = "OEMSpecial";			
				break;	
			case 2:
				$categoryString = "ApparelSpecial";			
				break;
							
			case 3:
				$categoryString = "PartsSpecial";	
				break;
					
			case 4:
				$categoryString = "ServiceSpecial";	
				break;
						
			case 5:
				$categoryString = "StoreSpecial";
				break;	
		}
		return $categoryString;
	}

	public function SaveLinkedInventory() {
		$specialExpiredDate = NULL;
		$prevStartDate = NULL;
		$prevExpiredDate = NULL;
		
		foreach($this -> RelatedSpecialID as $key => $newItem) {
			if($newItem == '0') {
				$item = new RelatedSpecialItem();	
				$item -> RelatedSpecialID = $this -> _id;
			} else {
				$item = RelatedSpecialItem::WithID($newItem);
			}
			
			
			$item -> RelatedSpecialType = $this -> RelatedSpecialType[$key];			
			$item -> RelatedSpecialTypeValue = $this -> RelatedSpecialTypeValue[$key];
			$item -> RelatedSpecialExpiredDate = $this -> RelatedSpecialExpiredDate[$key];
			$item -> RelatedDescription = $this -> RelatedDescription[$key];
			$item -> RelatedDisclaimer = $this -> RelatedDisclaimer[$key];
			$item -> RelatedInventoryIDS = $this -> LinkedInventoryIDS[$key];
			$item -> SaveLinkedInventory();
			
			if(new DateTime($this -> RelatedSpecialExpiredDate[$key]) > $prevExpiredDate) {
				$specialExpiredDate = $this -> RelatedSpecialExpiredDate[$key];	
			}
					
			$prevExpiredDate = new DateTime($this -> RelatedSpecialExpiredDate[$key]);
		}
		
		$this -> UpdateStartEndDates($specialExpiredDate);	
		$this -> json -> outputJqueryJSONObject('incentiveSaved', true);		
		
	}

	public function SaveItems() {
		
		$specialExpiredDate = NULL;
		$prevStartDate = NULL;
		$prevExpiredDate = NULL;
		
		foreach($this -> RelatedSpecialID as $key => $newItem) {
			if($newItem == '0') {
				$item = new RelatedSpecialItem();	
				$item -> RelatedSpecialID = $this -> _id;
			} else {
				$item = RelatedSpecialItem::WithID($newItem);
			}
			
			
			$item -> RelatedSpecialType = $this -> RelatedSpecialType[$key];			
			$item -> RelatedSpecialTypeValue = $this -> RelatedSpecialTypeValue[$key];
			$item -> RelatedSpecialExpiredDate = $this -> RelatedSpecialExpiredDate[$key];
			$item -> RelatedDescription = $this -> RelatedDescription[$key];
			$item -> RelatedDisclaimer = $this -> RelatedDisclaimer[$key];
			$item -> Save();
			
			
			
			if(new DateTime($this -> RelatedSpecialExpiredDate[$key]) > $prevExpiredDate) {
				$specialExpiredDate = $this -> RelatedSpecialExpiredDate[$key];	
			}
			
			
			$prevExpiredDate = new DateTime($this -> RelatedSpecialExpiredDate[$key]);
		}
		
	
		
		$this -> UpdateStartEndDates($specialExpiredDate);
		
		$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> specials() . '/edit/'. $this -> GenerateEditCategory() . '/'. $this -> _id);
	}

	public function ValidateMultiple($value) {
		$ReturnValue = NULL;
		foreach ($value as $key => $value) {
			if(empty($value)) {
				$ReturnValue = true;
				break;
			}
		}
		return $ReturnValue;
	}
	
	public function UpdateStartEndDates($endDate) {
		$postData = array('SpecialEndDate' => $endDate);											
		$this->db->update('specials', $postData, array('SpecialID' => $this -> _id));
	}
	
	
	
	private function uploadPicture($newImageName, $folderName, $imageTemp, $imageCurrent) {
		$ImagePathParts = array();
		$ImagePathParts['SpecialsParent'] = "specials";
		$ImagePathParts['FolderName'] = $folderName;
		Image::MoveAndRenameImage($imageTemp, 
								  $ImagePathParts,
								  $imageCurrent,
								  parent::seoUrl($newImageName), 4);	
		
		$currentPhotoUploaded = PHOTO_PATH . 'specials/' . $folderName . '/' . parent::seoUrl($newImageName) . '.' . Image::getFileExt($imageCurrent);
								  
		list($origWidth, $origHeight) = getimagesize($currentPhotoUploaded);
		
		if(1000 > $origWidth) {
			Image::resizeImage($currentPhotoUploaded, $currentPhotoUploaded, $origWidth, $origHeight);
		} else {
			Image::resizeImage($currentPhotoUploaded, $currentPhotoUploaded, 1000, 1000);	
		}						  
	}
	
	
	
	private function SaveRelatedInventory($specialID) {
		if($this -> RelatedInventoryType == '99') {
			$relatedItems = new RelatedSpecialItemsList();	
						
			foreach($relatedItems -> ItemsBySpecial($specialID) as $relatedItemSingle) {
				$inventoryLinked = $this -> db -> prepare("DELETE FROM inventoryrelatedspecials WHERE RelatedIncentiveID = :RelatedIncentiveID");
				$inventoryLinked -> execute(array(':RelatedIncentiveID' => $relatedItemSingle['RelatedSpecialItemID']));
			}				
		}
	}
	
	private function SaveRelatedStores($specialID) {
		if($this -> SpecialCategory == 1) {
			$VehicleBrandID = Brand::WithID($this -> OEMBrandID);
				
			$this -> db -> insert('specialrelatedstores', array("CreatedSpecialID" => $specialID,
																"RelatedStoreID" => $VehicleBrandID -> StoreID));
		} else {
			if(isset($this -> Stores)) {
				foreach($this -> Stores as $storeID) {
					$this -> db -> insert('specialrelatedstores', array("CreatedSpecialID" => $specialID,
																		"RelatedStoreID" => $storeID));	
				}	
			}	
		}
	}
	
	public function Save() {
		try {
			$specialDataArray = array();
			$specialIDRedirect = NULL;
			
			if(isset($this -> _id)) {
				//remove special related stores	
				$sth = $this -> db -> prepare("DELETE FROM specialrelatedstores WHERE CreatedSpecialID = :SpecialID");
				$sth -> execute(array(':SpecialID' => $this -> _id));
				
				$ModifiedDate = parent::TimeStamp();
				
				if($this -> SpecialIsActive == '1') {
					if($this -> PublishedDate == "") {
						$specialDataArray['SpecialStartDate'] = date("Y-m-d", $this -> time -> NebraskaTime());
						$specialDataArray['specialPublishedDate'] = parent::TimeStamp();
					}
				}
				
				
				$specialDataArray['SpecialTitle'] = $this -> SpecialTitle;
				$specialDataArray['specialSEOurl'] = parent::seoUrl($this -> SpecialTitle);
				$specialDataArray['SpecialDescription'] = parent::DescriptionFormat($this -> SpecialDescription);
				$specialDataArray['SpecialDescriptionFooter'] = parent::DescriptionFormat($this -> SpecialDescriptionFooter);
				
				$specialDataArray['isSpecialsActive'] = $this -> SpecialIsActive;
				$specialDataArray['RelatedInventoryType'] = $this -> RelatedInventoryType;
				$specialDataArray['specialModified'] = $ModifiedDate;
				
				
				if(!empty($this -> SpecialImageBig)) {
					$newImageName = date("Y-m-d", $this -> time -> NebraskaTime()) . '-' . $this -> SpecialTitle; 
					$this -> uploadPicture($newImageName, $this -> FolderName, $_FILES['specialPhotoBig']['tmp_name'], $this -> SpecialImageBig);
					
					$specialDataArray['SpecialMainImage'] = parent::seoUrl($newImageName) . '.' . Image::getFileExt($this -> SpecialImageBig);
				}
				
				if(!empty($this -> SpecialImageSmall)) {
					$newImageNameSmall = date("Y-m-d", $this -> time -> NebraskaTime()) . '-' . $this -> SpecialTitle . '-small'; 
					$this -> uploadPicture($newImageNameSmall, $this -> FolderName, $_FILES['specialPhotoSmall']['tmp_name'], $this -> SpecialImageSmall);
					
					$specialDataArray['specialThumbImage'] = parent::seoUrl($newImageNameSmall) . '.' . Image::getFileExt($this -> SpecialImageSmall);
				}
				
				$this->db->update('specials', $specialDataArray, array('SpecialID' => $this -> _id));
				
				$specialIDRedirect = $this -> _id;
			} else {
				$folderName	= parent::seoUrl(date("Y-m-d", $this -> time -> NebraskaTime()) . '-' . $this -> SpecialTitle);
				mkdir(PHOTO_PATH . 'specials/'. $folderName);
				
				$specialDataArray['specialSEOurl'] = parent::seoUrl($this -> SpecialTitle);
				$specialDataArray['SpecialTitle'] = $this -> SpecialTitle;
				$specialDataArray['SpecialStartDate'] = date("Y-m-d", $this -> time -> NebraskaTime());
				$specialDataArray['SpecialDescription'] = parent::DescriptionFormat($this -> SpecialDescription);
				$specialDataArray['SpecialDescriptionFooter'] = parent::DescriptionFormat($this -> SpecialDescriptionFooter);
				$specialDataArray['specialType'] = $this -> SpecialCategory;
				$specialDataArray['RelatedInventoryType'] = $this -> RelatedInventoryType;
				$specialDataArray['FolderName'] = $folderName;
				
				if(isset($this -> PartAcessoryBrandID)) {
					$specialDataArray['PartsApparelID'] = $this -> PartAcessoryBrandID;
				} else {
					$specialDataArray['PartsApparelID'] = 0;
				}
				
				if(isset($this -> OEMBrandID)) {
					$specialDataArray['OEMBrandID'] = $this -> OEMBrandID;
				} else {
					$specialDataArray['OEMBrandID'] = 0;
				}
				
				if(!empty($this -> SpecialImageBig)) {
					$newImageName = date("Y-m-d", $this -> time -> NebraskaTime()) . '-' . $this -> SpecialTitle; 
					$this -> uploadPicture($newImageName, $folderName, $_FILES['specialPhotoBig']['tmp_name'], $this -> SpecialImageBig);
					
					$specialDataArray['SpecialMainImage'] = parent::seoUrl($newImageName) . '.' . Image::getFileExt($this -> SpecialImageBig);
				}
				
				if(!empty($this -> SpecialImageSmall)) {
					$newImageNameSmall = date("Y-m-d", $this -> time -> NebraskaTime()) . '-' . $this -> SpecialTitle . '-small'; 
					$this -> uploadPicture($newImageNameSmall, $folderName, $_FILES['specialPhotoSmall']['tmp_name'], $this -> SpecialImageSmall);
					
					$specialDataArray['specialThumbImage'] = parent::seoUrl($newImageNameSmall) . '.' . Image::getFileExt($this -> SpecialImageSmall);
				}
				
				
				$specialID = $this -> db -> insert('specials', $specialDataArray);	
				$specialIDRedirect = $specialID;		
			}
					
			$this -> SaveRelatedInventory($specialIDRedirect);	
			$this -> SaveRelatedStores($specialIDRedirect);	
			//save log edit		
			$log = new EditHistoryLog();
			$log -> itemID = $specialIDRedirect;
			$log -> userID = $_SESSION['user'] -> _userId;
			$log -> itemType = 2;		
			$log -> Save();	
			
			$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> specials() . '/edit/'. $this -> GenerateEditCategory() . '/'. $specialIDRedirect);
		
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Special Save Error: " . $e->getMessage();
			$TrackError -> type = "SPECIAL SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
		}
	}
	
	public function ResetExpiredDate() {
		$postData = array('SpecialEndDate' => NULL);
		$this->db->update('specials', $postData, array('SpecialID' => $this -> _id));
	}
	
	public function UpdateExpiredDate() {
		$currentExpiredDate = new DateTime($this -> _currentExpiredDate);
		$NewExpiredDate = new DateTime($this -> NewExpiredDate);	
		
		if($NewExpiredDate > $currentExpiredDate) {
			$postData = array('SpecialEndDate' => $this -> NewExpiredDate);
			$this->db->update('specials', $postData, array('SpecialID' => $this -> _id));
		}	
	}
	
	public function delete() {
		try {
			$redirectedPath = NULL;
			Folder::DeleteDirectory(PHOTO_PATH . 'specials/' . $this -> FolderName);
			
			switch($this -> SpecialCategory) {
				case 1:
					$redirectedPath = $this -> pages -> specials() . '/OEM/' . $this -> OEMBrandID;					
					break;	
				case 2:
					
					if($this -> PartAcessoryBrandID == 0) {
						$redirectedPath = $this -> pages -> specials() . '/category/Apparel';
					} else {
						$redirectedPath = $this -> pages -> specials() . '/brand/' . $this -> PartAcessoryBrandID . '/Apparel';
					}
					
					break;
								
				case 3:
					$categoryString = "PartsSpecial";	
					break;
						
				case 4:
					$categoryString = "ServiceSpecial";	
					break;
							
				case 5:
					$redirectedPath = $this -> pages -> specials() . '/category/Store';
					break;	
			}
			
		
			
			//delete special from table
			$specialRemove = $this -> db -> prepare("DELETE FROM specials WHERE SpecialID = :SpecialID");
			$specialRemove -> execute(array(':SpecialID' => $this->_id));
			
			
			//delete from related special table
			$specialRelationRemove = $this -> db -> prepare("DELETE FROM specialrelatedstores WHERE CreatedSpecialID = :CreatedSpecialID");
			$specialRelationRemove -> execute(array(':CreatedSpecialID' => $this->_id));
			
			//delete related items
			$relatedItemsRemove = $this -> db -> prepare('DELETE FROM relatedspecialitems WHERE relatedSpecialID = :relatedSpecialID');
			$relatedItemsRemove -> execute(array(':relatedSpecialID' => $this->_id));
			
			//delete realted inventory
			$relatedInventoryRemove = $this -> db -> prepare('DELETE FROM inventoryrelatedspecials WHERE RelatedSpecialID = :RelatedSpecialID');
			$relatedInventoryRemove -> execute(array(':RelatedSpecialID' => $this->_id));
			
			$this -> redirect -> redirectPage($redirectedPath);
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Delete Special Error: " . $e->getMessage();
			$TrackError -> type = "DELETE SPECIAL ERROR";
			$TrackError -> SendMessage();
		}
	}

	public function ShowActiveOption() {
		$imageUploaded = false;
		$relateItemsEntered = false;
		
		$relatedItems = new RelatedSpecialItemsList();
		
		if(!empty($this -> _currentImageBig) && !empty($this -> _currentImageSmall)) {
			$imageUploaded = true;
		}	
		
		
		//if($this -> RelatedInventoryType == 1) {
		//	foreach($relatedItems -> ItemsBySpecial($this->_id) as $relatedItemSingle) {
		//		$sth = $this -> db -> prepare('SELECT * FROM inventoryrelatedspecials WHERE RelatedIncentiveID = :RelatedIncentiveID');
		//		$sth->execute(array(':RelatedIncentiveID' => $relatedItemSingle['RelatedSpecialItemID'])); 
				
		//		if($sth->rowCount() > 0) {
		//			$relateItemsEntered = true;
		//		} else {
		//			$relateItemsEntered = false;
		//		}
		//	}
			
			
		//} else if($this -> RelatedInventoryType == 99) {
			if(count($relatedItems -> ItemsBySpecial($this->_id)) > 0) {
				$relateItemsEntered = true;
			}
		///}
		
		if($imageUploaded == true && $relateItemsEntered == true) {
			return true;
		}		
		
	}


}