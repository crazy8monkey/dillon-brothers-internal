<?php

class TwilioChatChannel extends BaseObject {
		
	private $_id;	
	private $_twilioChannelID;
	
	public $twilioChannelID;
	public $FriendlyName;
	public $UniqueName;	
	
	public $UserID;
	
	public $CurrentStoreID;
	
	public $RelatedStoreID;
	
	public $UserIDS;
	public $UserNames;
	public $CurrentChatUsers = array();
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($id) {
		$instance = new self();
        $instance -> _id = $id;
        $instance -> loadByID();
        return $instance;
	}

    public static function TwilioChannelID($twilioChannelID) {
        $instance = new self();
        $instance -> _twilioChannelID = $twilioChannelID;
        $instance -> loadByTwilioChannelID();
        return $instance;
    }
	
	protected function loadByTwilioChannelID() {
		$sth = $this -> db -> prepare('SELECT * FROM twiliochatchannels WHERE twilioChannelID = :channelID');
        $sth -> execute(array(':channelID' => $this -> _twilioChannelID));	
        $record = $sth -> fetch();
        $this -> fill($record);
	}
		
	protected function loadByID() {
		$sth = $this -> db -> prepare('SELECT * FROM twiliochatchannels WHERE chatChannelID = :chatChannelID');
        $sth->execute(array(':chatChannelID' => $this -> _id));	
        $record = $sth -> fetch();
        $this->fill($record);
		$this -> _LoadCurrentUsers();
	}	
		
    protected function fill(array $row){
		$this -> _id = $row['chatChannelID'];	
		$this -> twilioChannelID = $row['twilioChannelID'];
		$this -> FriendlyName = $row['friendlyName'];
		$this -> CurrentStoreID = $row['relatedStoreID'];
    }	
	
	private function _LoadCurrentUsers() {
		$chatusers = new ChatUsersList();
		$this -> CurrentChatUsers = $chatusers -> ByChannel($this -> _id);
	}
	

	public function GetID() {
		return $this -> _id;
	}


	public function Validate() {
		$validationErrors = array();
		
		if(!isset($this -> _id)) {
			$channelCheck = $this->db->prepare('SELECT uniqueName FROM twiliochatchannels WHERE uniqueName = ?');
	        $channelCheck->execute(array(parent::CleanStringNoSpaces($this -> FriendlyName)));
			
			
			if($this -> validate -> emptyInput($this -> FriendlyName)) {
				array_push($validationErrors, array("inputID" => 1,
													'errorMessage' => 'Required'));
			} else if($channelCheck -> fetchAll()) {
				array_push($validationErrors, array("inputID" => 1,
													'errorMessage' => 'There is already a channel name entered, please type in a different one'));
			}

		}

		if(!isset($this -> RelatedStoreID)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Please Select a related store'));
		}		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function CreateChannelMember() {
		$chatMember = new TwilioChatChannelMember();
		$chatMember -> RelatedTwilioChatID = $this -> _id;
		$chatMember -> TwilioMemberID = $this -> _twilioChannelID;
		$chatMember -> RelatedUserID = '0';
		$chatMember -> Save();
	}

	public function Save() {
		try {
			
			$twilioHandler = new TwilioHandler();
			
			$twilioHandler -> ChannelFriendlyName = $this -> FriendlyName;
			$twilioHandler -> ChannelUniqueName = parent::CleanStringNoSpaces($this -> FriendlyName);
			
			$twilioHandler -> CreateChatChannel();
			$twilioHandler -> SetChannelID();
			
			
			$newTwilioChannelDB = array();
			
			$newTwilioChannelDB['twilioChannelID'] = $twilioHandler -> GetChannelID();
			$newTwilioChannelDB['friendlyName'] = $this -> FriendlyName;
			$newTwilioChannelDB['relatedStoreID'] = $this -> RelatedStoreID[0];
			$newTwilioChannelDB['uniqueName'] = parent::CleanStringNoSpaces($this -> FriendlyName);
			
			$this -> db -> insert('twiliochatchannels', $newTwilioChannelDB);
			
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'settings/chat');
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Save Twilio Channel Error: " . $e->getMessage();
			$TrackError -> type = "TWILIO CHANNEL SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	
	public function SaveUsers() {
		//remove previous users
		//$this -> CurrentChatUsers
		$currentUsers = array_column($this -> CurrentChatUsers, 'relatedUserID');	
		
		if(isset($this -> UserIDS)) {
			$differences = array_diff($currentUsers, $this -> UserIDS);	
			
			
			foreach($this -> UserIDS as $key => $userSingle) {
				if(!in_array($userSingle, $currentUsers)) {
					
					$channelUser = new TwilioChatChannelMember();
					$channelUser -> RelatedUserID = $userSingle;
					$channelUser -> TwilioMemberID = $this -> twilioChannelID;
					$channelUser -> MemberName = $this -> UserNames[$key];
					$channelUser -> RelatedTwilioChatID = $this -> _id;
					$channelUser -> Save();
				} 
			}	
			foreach($differences as $differentUser) {
				$this -> _RemoveUser($differentUser);
			}	
				
			
		} else {
			foreach($this -> CurrentChatUsers as $currentUserSingle) {
				$this -> _RemoveUser($currentUserSingle['relatedUserID']);	
			}	
		}
		
		$channelUpdateDB = array();
		$channelUpdateDB['relatedStoreID'] = $this -> RelatedStoreID[0];
		
		
		$this->db->update('twiliochatchannels', $channelUpdateDB, array('chatChannelID' => $this -> _id));
		
		//print_r($differences);
		$this -> json -> outputJqueryJSONObject('redirect', PATH. 'settings/adduserstochat/' . $this -> _id);
	}
	
	private function _RemoveUser($userID) {
		$channelUser = TwilioChatChannelMember::WithUserID($userID);
		$channelUser -> RelatedTwilioChatID = $this -> twilioChannelID;
		$channelUser -> Delete();	
	}
	
	public function Delete() {
		$twilio = new TwilioHandler();
		$twilio -> CurrentChannelID = $this -> twilioChannelID;
		$twilio -> DeleteChannel();	
		
		foreach($this -> CurrentChatUsers as $userSingle) {
			$userChannel = TwilioChatChannelMember::WithUserID($userSingle['relatedUserID']);
			$userChannel -> Delete();
		} 
		
			
		$channelDelete = $this -> db -> prepare('DELETE FROM twiliochatchannels WHERE chatChannelID = :chatChannelID');
		$channelDelete -> execute(array(':chatChannelID' => $this -> _id));
		
		$this -> redirect -> redirectPage(PATH . 'settings/chat');
	}
	
	



}