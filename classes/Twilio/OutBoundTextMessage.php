<?php

class OutBoundTextMessage extends BaseObject {
	
	public $textMessage;
	public $RelatedCustomerID;
	public $RelatedConversationID;
	public $RelatedPhoneNumber;
	
	public $DepartmentNumber;
	public $DepartmentNumberID;
	
	public $catchResponse = false;
	
	private $_toReply;
	private $_fromReply;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($employeeID) {
        $instance = new self();
        $instance->_id = $employeeID;
        $instance->loadById();
        return $instance;
    }
	
	public static function WithPhones($to, $from) {
		$instance = new self();
        $instance -> _toReply = $to;
		$instance -> _fromReply = $from;
	    $instance->loadByReplyMessage();
        return $instance;
	}
	
	
	//SELECT * FROM outboundtextmessages LEFT JOIN textmessageconversation ON outboundtextmessages.relatedConversationID = textmessageconversation.textmessageconversationID
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM employees WHERE EmployeeID = :EmployeeID');
        $sth->execute(array(':EmployeeID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	
	protected function loadByReplyMessage() {
		$sth = $this -> db -> prepare('SELECT * FROM outboundtextmessages 
											LEFT JOIN textmessageconversation ON outboundtextmessages.relatedConversationID = textmessageconversation.textmessageconversationID 
											WHERE textmessageconversation.phonenumber = :toNumber AND textmessageconversation.relatedDeptNumber = :fromNumber');
        $sth->execute(array(':toNumber' => $this -> _toReply,
							':fromNumber' => $this -> _fromReply));	
        $record = $sth -> fetch();
        $this->fill($record);
	}
	
    protected function fill(array $row){
    	
    }	

	public function GetID() {
		return $this -> _id;
	}	
	
	public function Save() {
		try {
			
			Session::init();
			
			$NewMessageSave = array();
			
			$NewMessageSave['textMessage'] = $this -> textMessage;
			$NewMessageSave['textmessagesent'] = parent::TimeStamp();
			$NewMessageSave['relatedCustomerID'] = $this -> RelatedCustomerID;
			$NewMessageSave['twilioNumberUsed'] = $this -> DepartmentNumberID;
			//preg_replace("/[^0-9]/", "", $this -> DepartmentNumber)
			
			if($this -> catchResponse == true) {
				$NewMessageSave['fromCustomer'] = 1;
			} else {
				$NewMessageSave['textsentbyUserID'] = $_SESSION['user'] -> _userId;
				
				$twilioHandler = new TwilioHandler();
				$twilioHandler -> CustomerPhoneNumber = preg_replace("/[^0-9]/", "", $this -> RelatedPhoneNumber);
				$twilioHandler -> Message = $this -> textMessage;
				$twilioHandler -> DeptNumber = preg_replace("/[^0-9]/", "", $this -> DepartmentNumber);
				$twilioHandler -> SendMessage();	
			}
			
			$this -> db -> insert('outboundtextmessages', $NewMessageSave);			
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "OutBountText Save Error: " . $e->getMessage();
			$TrackError -> type = "EMPLOYEE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	



}