<?php

class ChatConversationMessage extends BaseObject {
		
	public $FromCustomer;
	public $RelatedConversationID;
	public $Message;
	public $RelatedWorker;
	public $TimeStamp;
	public $EndOfConversation;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

	public function Save() {
		try {

			$newChatMessageDB = array();
			
			$newChatMessageDB['fromCustomer'] = $this -> FromCustomer;
			$newChatMessageDB['relatedConversationID'] = $this -> RelatedConversationID;
			$newChatMessageDB['message'] = $this -> Message;
			$newChatMessageDB['relatedWorder'] = $this -> RelatedWorker;
			$newChatMessageDB['dateentered'] = str_replace(' ', 'T', $this -> TimeStamp);
			$newChatMessageDB['endofConversation'] = $this -> EndOfConversation;
			
			$this -> db -> insert('chatconversationmessages', $newChatMessageDB);
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Save Twilio Chat Message Error: " . $e->getMessage();
			$TrackError -> type = "TWILIO CHAT MESSAGE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	




}