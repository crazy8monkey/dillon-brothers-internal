<?php

require_once TWILIO_API_PATH. 'autoload.php'; // Loads the library
use Twilio\Rest\Client;	
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\IpMessagingGrant;

use Twilio\Twiml;

//http://c2cd82e6.ngrok.io/dillonbrothers/updates/twiliocatchresponse?Twiml=%3CResponse%3E%3C%2FResponse%3E


//?Twiml=%3CResponse%3E%3C%2FResponse%3E
class TwilioHandler {
	
	private $TwilioClient;
	
	public $CustomerPhoneNumber;
	public $Message;
	public $CustomerName;

	public $DeptNumber;


	public $MessageServiceID;
	
	public $TextConversationDBID;
	
	public $ChannelFriendlyName;
	public $ChannelUniqueName;
	
	
	public $CurrentChannelID;
	public $CurrentTaskID;
	public $CurrentMemberID;
	public $CurrentWorkerID;
	
	public $NewMemberName;
	
	
	private $NewChannel;
	private $NewMemberClient;
	private $NewWorkerClient;
	
	private $_ChannelID;
	private $_MemberID;
	private $_WorkderID;
	
	public $ChatIdentityToken;
	
	public $NewWorkerCreated = false;
	
    public function __construct() {
		$this -> TwilioClient = new Client('AC23289f8d8a0e5a87ae60349eb24ee1b8', 'fe80215bdc30955d0be7a06d18351fbb');
    }
	
	public function SendOutgoingNumber() {
		$validationRequest = $this -> TwilioClient -> validationRequests->create(
		    $this -> CustomerPhoneNumber,
		    array(
		        "friendlyName" => $this -> CustomerName
		    )
		);
		
		echo $validationRequest->validationCode;
	}
	
	public function SendMessage() {
		$this -> TwilioClient -> messages -> create(
		    $this -> CustomerPhoneNumber,
		    array(
		    	'from' => '+1' . $this -> DeptNumber,
		        'messagingServiceSid' => "MGa3a1dc71d1967dc7174f89436c503516",
		        'body' => $this -> Message,
		        'ProvideFeedback' => true
		    )
		);
		
	}
	
	//create new chat channel
	public function CreateChatChannel() {
		$this -> NewChannel = $this -> TwilioClient -> chat -> services("IScef31cfda2374e4aa4f3f9eb4678a377") -> channels ->create(array(
																																    'friendlyName' => $this -> ChannelFriendlyName,
																																  	'uniqueName' => $this -> ChannelUniqueName
																																  ));
	}
	
	public function SetChannelID() {
		$this -> _ChannelID = $this -> NewChannel -> sid;
	}
	
	public function GetChannelID() {
		return $this -> _ChannelID;
	}
	
	// Delete the channel
	public function DeleteChannel() {
		$this -> TwilioClient -> chat -> services("IScef31cfda2374e4aa4f3f9eb4678a377") -> channels($this -> CurrentChannelID) -> delete();
	}
	
	//create new member
	public function CreateChannelMember() {
		$this -> NewMemberClient = $this -> TwilioClient -> chat -> services("IScef31cfda2374e4aa4f3f9eb4678a377") -> channels($this -> CurrentChannelID) -> members -> create($this -> NewMemberName);
	}
	
	public function RemoveChannelMember() {
 		$this -> TwilioClient -> chat -> services("IScef31cfda2374e4aa4f3f9eb4678a377") -> channels($this -> CurrentChannelID) -> members($this -> CurrentMemberID) -> delete();
 	}
		
	public function SetNewMemberID() {
		$this -> _MemberID = $this -> NewMemberClient -> sid;
	}
	
	public function GetNewMemberID() {
		return $this -> _MemberID;
	}
	
	public function WorkerToInactive() {
		$this -> TwilioClient -> taskrouter
							  -> v1
							  -> workspaces("WS47103271c50b41b49342a83c28ec6f4a") 
		  					  -> workers($this -> CurrentWorkerID)->update(array( 
											'ActivitySid' => "WAff3354c82270bfec8c8acb94897d9da4",   
										));
	}
	
	public function DeleteWorker() {
		$this -> TwilioClient -> taskrouter
							  ->workspaces("WS47103271c50b41b49342a83c28ec6f4a")
							  ->workers($this -> CurrentWorkerID)
							  ->delete();
	}
	
	//create new worker
	public function CreateNewWorker() {
		$currentWorkers = $this -> TwilioClient -> taskrouter -> workspaces("WS47103271c50b41b49342a83c28ec6f4a") -> workers -> read();
		
		$currentWorkerArray = array();
		
		foreach($currentWorkers as $workerSingle) {
			array_push($currentWorkerArray, $workerSingle -> friendlyName);	
		}
		
		if(!in_array($this -> NewMemberName, $currentWorkerArray)) {
			$this -> NewWorkerClient = $this -> TwilioClient -> taskrouter
							    ->workspaces("WS47103271c50b41b49342a83c28ec6f4a")
							    ->workers
							    ->create($this -> NewMemberName);
								
		 
			$this -> TwilioClient -> taskrouter
								  -> v1
								  -> workspaces("WS47103271c50b41b49342a83c28ec6f4a") 
			  					  -> workers($this -> NewWorkerClient -> sid)->update(array( 
												'ActivitySid' => "WA8d16e845bd96d6f22bdc78efccaaf6d4",   
											)); 	
											
											
			$this -> NewWorkerCreated = true;														
		}
										
	}
	
	
	public function SetWorkderID() {
		$this -> _WorkderID = $this -> NewWorkerClient -> sid;
	}
	
	public function GetNewWorkderID() {
		return $this -> _WorkderID;
	}
	
	public function UpdateWorkerStatus($statusID) {
		$this -> TwilioClient -> taskrouter
							  -> v1
							  -> workspaces("WS47103271c50b41b49342a83c28ec6f4a") 
		  					  -> workers($this -> CurrentWorkerID)->update(array( 
											'ActivitySid' => $statusID,   
										));
	}
	
	
	public function GrabMessagesByTaskID() {
		$MessagesByTask = array();
		//Retrieve the messages
		$messages = $this -> TwilioClient -> chat
										  -> services("IScef31cfda2374e4aa4f3f9eb4678a377")
										  -> channels($this -> CurrentChannelID)
										  -> messages
										  -> read();
		
		//
		
		foreach($messages as $msgSingle) {
			$attributes = json_decode($msgSingle -> attributes, true);
			if(isset($attributes['taskID'])) {
				if($attributes['taskID'] == $this -> CurrentTaskID) {
					array_push($MessagesByTask, $msgSingle);
				}	
			}
			
		}
		
		return $MessagesByTask;
	}
	
	
	public function GenerateAccessToken() {
		// Required for IP messaging grant
		$ipmServiceSid = 'IScef31cfda2374e4aa4f3f9eb4678a377';
		// An identifier for your app - can be anything you'd like
		$appName = 'DillonBrothersSalesChatService';
		// choose a random username for the connecting user
		$identity = $this -> ChatIdentityToken;
		// A device ID should be passed as a query string parameter to this script
		$deviceId = $this -> DeviceID;
		$endpointId = $appName . ':' . $identity . ':' . $deviceId;
		
		// Create access token, which we will serialize and send to the client
		$token = new AccessToken(
		    'AC23289f8d8a0e5a87ae60349eb24ee1b8',
		    'SK193190932553a8e3ba71ff149b58cc64',
		    'A2YYwbu0Jjvv1vlb0VAB0tHosifQNsj6',
		    3600,
		    $identity
		);
		
		// Create IP Messaging grant
		$ipmGrant = new IpMessagingGrant();
		$ipmGrant->setServiceSid($ipmServiceSid);
		$ipmGrant->setEndpointId($endpointId);
		
		// Add grant to token
		$token->addGrant($ipmGrant);
		
		// render token to string
		return $token->toJWT();	
	}

	public function CompleteTask() {
		$taskSingle = $this -> TwilioClient -> taskrouter -> workspaces("WS47103271c50b41b49342a83c28ec6f4a") -> tasks($this -> CurrentTaskID);
		
		// cancel a task
		$taskSingle-> update(
		    array('assignmentStatus' => 'completed', 'reason' => 'done talking to customer')
		);
		
		//$this -> TwilioClient->taskrouter
		//    ->workspaces("WS47103271c50b41b49342a83c28ec6f4a")
		//    ->tasks($this -> CurrentTaskID)
		//    ->delete();
	}

	public function RemoveTask() {
		$this -> TwilioClient->taskrouter
		    ->workspaces("WS47103271c50b41b49342a83c28ec6f4a")
		    ->tasks($this -> CurrentTaskID)
		    ->delete();
	}
	
	
	
}