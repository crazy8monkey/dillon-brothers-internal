<?php

class ChatConversation extends BaseObject {
		
	private $_id;	
	private $_taskID;
	
	public $Message;
	public $FromCustomerValue;
	public $UserID;
	public $TwilioChannelID;
	public $TwilioTaskID;
	public $taskStatus;
	
	public $TwilioMemberID;
	
	public $UniqueChannelName;
	
	public $WorkerEmail;
	
	
	public $CurrentMessages = array();
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

	//by id search
	public static function WithID($id) {
		$instance = new self();
        $instance -> _id = $id;
        $instance -> loadByID();
        return $instance;
	}

	protected function loadByID() {
		$sth = $this -> db -> prepare('SELECT * FROM chatconversations 
											LEFT JOIN twiliochatchannels ON chatconversations.twilioChannelID = twiliochatchannels.twilioChannelID 
											LEFT JOIN twiliochatmembers ON chatconversations.WorkerName = twiliochatmembers.relatedUserID 
											LEFT JOIN users ON chatconversations.WorkerName = users.userID
											WHERE chatConversationID = :chatConversationID');
        $sth->execute(array(':chatConversationID' => $this -> _id));	
        $record = $sth -> fetch();
        $this->fill($record);
		$this -> _grabMessages();	
	
		
	}

	//task id search
	public static function WithTaskID($taskID) {
		$instance = new self();
        $instance -> _taskID = $taskID;
        $instance -> loadByTaskID();
        return $instance;
	}
	
	protected function loadByTaskID() {
		$sth = $this -> db -> prepare('SELECT * FROM chatconversations LEFT JOIN users ON chatconversations.WorkerName = users.userID WHERE twilioTaskID = :twilioTaskID');
        $sth->execute(array(':twilioTaskID' => $this -> _taskID));	
        $record = $sth -> fetch();
        $this->fill($record);
	}
	
	protected function fill(array $row){
		$this -> _id = $row['chatConversationID'];
		$this -> TwilioChannelID = $row['twilioChannelID'];
		$this -> TwilioTaskID = $row['twilioTaskID'];
		$this -> UniqueChannelName = $row['uniqueName'];
		$this -> taskStatus = $row['taskStatus'];
		$this -> TwilioMemberID = $row['twilioMemberID'];
		$this -> WorkerEmail = $row['UserEmail'];
    }	

	public function GetID() {
		return $this -> _id;
	}
	
	
	private function _grabMessages() {
		if($this -> taskStatus == 1) {
			$twilio = new TwilioHandler();
			$twilio -> CurrentChannelID = $this -> TwilioChannelID;
			$twilio -> CurrentTaskID = $this -> TwilioTaskID;
			$this -> CurrentMessages = $twilio -> GrabMessagesByTaskID();	
		}
	}


	//public function Save() {
	//	try {
			//for testing purposes
			//$uniqueID = uniqid();
			
			//$twilioHandler = new TwilioHandler();
			//$twilioHandler -> CurrentChannelID = $this -> TwilioMemberID;
			//$twilioHandler -> NewMemberName = $this -> MemberName;
			//$twilioHandler -> CreateChannelMember();
			//$twilioHandler -> SetNewMemberID();
			
			
			//$newTwilioMemberDB = array();
			
			//$newTwilioMemberDB['relatedUserID'] = $this -> RelatedUserID;
			//$newTwilioMemberDB['twilioMemberID'] = $twilioHandler -> GetNewMemberID();
			//$newTwilioMemberDB['twilioMemberID'] = 'hi';
			//$newTwilioMemberDB['relatedTwilioChatID'] = $this -> RelatedTwilioChatID;
			
			//$this -> db -> insert('twiliochatmembers', $newTwilioMemberDB);
			
	//	} catch (Exception $e) {
	//		$TrackError = new EmailServerError();
	//		$TrackError -> message = "Save Twilio Channel Member Error: " . $e->getMessage();
	//		$TrackError -> type = "TWILIO CHANNEL MEMBER SAVE ERROR";
	//		$TrackError -> SendMessage();
	//	}
	//}
	
	public function NotifyAssignedUser() {
		if(LIVE_SITE == true) {
			$channelUsers = new ChatUsersList();
			$userEmails = '';
			
			foreach($channelUsers -> All() as $userSingle) {
				$userEmails	.= $userSingle['UserEmail'] . ', ';
			}
						
			$email = new Email();
			$email -> to = rtrim($userEmails, ', ');
			$email -> subject = "New Chat Request";
			$email -> ChatNotification();	
		}
	}
	
	
	public function CloseConversation() {
		foreach($this -> CurrentMessages as $msgSingle) {
			$attributes = json_decode($msgSingle -> attributes, true);
			$msgSingle -> dateCreated -> setTimezone(new DateTimeZone('America/Chicago'));
			
			$message = new ChatConversationMessage();
			$message -> FromCustomer = $attributes['fromCustomer'];	
			$message -> RelatedConversationID = $this -> _id;
			$message -> Message = $msgSingle -> body;
			if($this -> FromCustomerValue == 1) {
				$message -> RelatedWorker = 0;	
			} else {
				$message -> RelatedWorker = 1;	
			}
			
			$message -> TimeStamp = $msgSingle -> dateCreated -> format('Y-m-d H:i:s');
			if(isset($attributes['taskComplete'])) {
				$message -> EndOfConversation = 1;	
			} else {
				$message -> EndOfConversation = 0;
			}
			
			$message -> Save();			
		}
		$updateConversationDB = array();
		$updateConversationDB['taskStatus'] = 0;
		
		$this->db->update('chatconversations', $updateConversationDB, array('chatConversationID' => $this->_id));
		
		$twilio = new TwilioHandler();
		$twilio -> CurrentTaskID = $this -> TwilioTaskID;
		$twilio -> CurrentWorkerID = $this -> TwilioMemberID;
		//$twilio -> UpdateWorkerStatus('WAf893d2e76d2308561bfd7a5e6fbd629a');
		//$twilio -> CompleteTask();
		$twilio -> RemoveTask();
		
		//$this -> redirect -> redirectPage(PATH. 'communication');
		
		
	}

}