<?php

class TwilioChatChannelMember extends BaseObject {
		
	private $_id;	
	
	
	public $RelatedUserID;
	public $TwilioMemberID;
	public $RelatedTwilioChatID;
	public $MemberName;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }


	public static function WithUserID($id) {
		$instance = new self();
        $instance -> RelatedUserID = $id;
        $instance -> loadByUserID();
        return $instance;
	}
	
	protected function loadByUserID() {
		$sth = $this -> db -> prepare('SELECT * FROM twiliochatmembers WHERE relatedUserID = :relatedUserID');
        $sth->execute(array(':relatedUserID' => $this -> RelatedUserID));	
        $record = $sth -> fetch();
        $this->fill($record);
	}	
	
	protected function fill(array $row){
		$this -> TwilioMemberID = $row['twilioMemberID'];
    }	

	public function Save() {
		try {
			//for testing purposes
			//$uniqueID = uniqid();
			$newTwilioMemberDB = array();
			$newTwilioMemberDB['relatedUserID'] = $this -> RelatedUserID;
			
			$twilioHandler = new TwilioHandler();
			$twilioHandler -> NewMemberName = $this -> MemberName;
			$twilioHandler -> CreateNewWorker();
			
			if($twilioHandler -> NewWorkerCreated == true) {
				$twilioHandler -> SetWorkderID();	
				$newTwilioMemberDB['twilioMemberID'] = $twilioHandler -> GetNewWorkderID();
			} else {
				$currentMember = TwilioChatChannelMember::WithUserID($this -> RelatedUserID);
				$newTwilioMemberDB['twilioMemberID'] = $currentMember -> TwilioMemberID;
			}
			
			$newTwilioMemberDB['relatedTwilioChatID'] = $this -> RelatedTwilioChatID;
			
			$this -> db -> insert('twiliochatmembers', $newTwilioMemberDB);
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Save Twilio Channel Member Error: " . $e->getMessage();
			$TrackError -> type = "TWILIO CHANNEL MEMBER SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	
	
	public function Delete() {
		$twilioHandler = new TwilioHandler();
		
		$twilioHandler -> CurrentWorkerID = $this -> TwilioMemberID;	
		$twilioHandler -> WorkerToInactive();
		
		$twilioHandler -> DeleteWorker();
		
		
		$memeberDelete = $this -> db -> prepare('DELETE FROM twiliochatmembers WHERE relatedUserID = :relatedUserID');
		$memeberDelete -> execute(array(":relatedUserID" => $this -> RelatedUserID));
	}



}