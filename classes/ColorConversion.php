<?php


class ColorConversion extends BaseObject {
	
	private $_id;
	private $_keyPhrase;
	
	public $ColorID;
	public $colorConversionID;
	public $Text;	
	public $CurrentKeyPhrase;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($conversionID) {
        $instance = new self();
        $instance->_id = $conversionID;
        $instance->loadByID();
        return $instance;
    }
	
	public static function WithKeyPhrase($keyPhrase) {
        $instance = new self();
        $instance->_keyPhrase = $keyPhrase;
        $instance->loadByKeyPhrase();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM colorconversions WHERE conversionID = :conversionID');
        $sth->execute(array(':conversionID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	protected function loadByKeyPhrase() {
    	$sth = $this -> db -> prepare('SELECT * FROM colorconversions WHERE keyPhrase = :keyPhrase');
        $sth->execute(array(':keyPhrase' => $this->_keyPhrase));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	

    protected function fill(array $row){
    	$this -> _id = $row['conversionID'];
    	$this -> CurrentKeyPhrase = $row['keyPhrase'];
    	$this -> ColorID = $row['relatedColorID'];
    }
	
	public function GetID() {
		return $this->_id;
	}

	public function Validate() {
		$validationErrors = array();
		
		if(!isset($this -> colorConversionID)) {
			array_push($validationErrors, array("inputID" => "99",
												'errorMessage' => 'Please select new color conversions'));
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	
	public function Save() {
		try {
			if(isset($this->_id)) {
				$colorConversionUpdateArray = array();
				
				//$colorConversionUpdateArray[]
				//$superEventDBData['eventName'] = $this -> eventName;
				//$superEventDBData['StructuredDataInfo'] = $StructuredData;
				//$superEventDBData['seoUrl'] = parent::seoUrlSpaces($this -> eventName);
				//$superEventDBData['SuperEventDescription'] = parent::DescriptionFormat($this -> SuperEventDescription);
				//$superEventDBData['modifiedDateFull'] = parent::TimeStamp();
				
				
				
				//$eventID = $this -> _id;
				
					
				//$this->db->update('colorconversions', $colorConversionUpdateArray, array('eventID' => $this -> _id));
				
			} else {
				$newColorConversionArray = array();
				$newColorConversionArray['keyPhrase'] = $this -> Text;
				
				if(isset($this -> ColorID)) {
					$newColorConversionArray['relatedColorID'] = $this -> ColorID;	
				}
				
				$this -> db -> insert('colorconversions', $newColorConversionArray);
			}
			
			

			//foreach($this ->Text as $key => $ruleSingle) {
			//	$checkDuplicate = $this->db->prepare('SELECT keyPhrase FROM colorconversions WHERE keyPhrase = :keyPhrase AND relatedColorID = :relatedColorID');
            //	$checkDuplicate->execute(array(":keyPhrase" => $ruleSingle, ":relatedColorID" => $this -> ColorID));
					
			//	if(!empty($ruleSingle) && $checkDuplicate -> rowCount() == 0) {
			//		$newColorConversionArray = array();
			//		$newColorConversionArray['keyPhrase'] = $ruleSingle;
			//		$newColorConversionArray['relatedColorID'] = $this -> ColorID;
			//		$this -> db -> insert('colorconversions', array('keyPhrase' => $ruleSingle,
			//														'relatedColorID' => $this -> ColorID));
			//		$this -> UpdateMainInventoryColors($ruleSingle);
			//	}					
			//}
			
			//$this -> json -> outputJqueryJSONObject("redirect", PATH . 'settings/edit/color/' . $this -> ColorID);				
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Event Save Error: " . $e->getMessage();
			$TrackError -> type = "EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
	}
	
	
	public function UpdateConversionRelatedColor() {
		$colorInfo = explode(":", $this -> ColorID);
		
		//switch(true) {
			
		//}
		
		if(strtolower($this -> CurrentKeyPhrase) == strtolower($colorInfo[1])) {
			$ColorConversionArray = array();	
			$ColorConversionArray['relatedColorID'] = $colorInfo[0];	
			$this->db->update('colorconversions', $ColorConversionArray, array('conversionID' => $this -> _id));		
		}
			
	}
	
	
	public function UpdateMainInventoryColors($colorText) {
		$currentInventory = new CurrentInventoryList();
		foreach($currentInventory -> CheckInventoryColor($colorText) as $cleanColorText) {
			
			//$inventoryObject = InventoryObject::WithID($cleanColorText['inventoryID']);
			//$inventoryObject -> UpdateColor();
						
			$saveColor = new SavedColor();
			$saveColor -> MainUnitInventoryID = $cleanColorText['inventoryID'];
			$saveColor -> RelatedMainUnitColorID = $this -> ColorID;
			$saveColor -> CurrentInventoryColorCheck = $cleanColorText['Color'];
			$saveColor -> Save();
		}
	}

	public function Delete() {
		$sth = $this -> db -> prepare("DELETE FROM colorconversions WHERE conversionID = :conversionID");
		$sth -> execute(array(':conversionID' => $this->_id));	
		
		$this -> redirect -> redirectPage(PATH. 'settings/edit/color/' . $this -> ColorID);
	}
	

		

}