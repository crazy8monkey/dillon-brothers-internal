<?php

class YearPhotoDirectory extends BaseObject {

	public $yearID;	
	public $photo;
	public $yearText;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($id) {
        $instance = new self();
        $instance-> yearID = $id;
        $instance->loadByID();
        return $instance;
    }
    
    public static function WithYear($year) {
        $instance = new self();
        $instance-> yearText = $year;
        $instance->loadByYear();
        return $instance;
    }
	
	protected function loadByYear() {
    	$sth = $this -> db -> prepare('SELECT * FROM yearphotos WHERE YearText = :YearText');
        $sth->execute(array(':YearText' => $this-> yearText));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM yearphotos WHERE yearPhotoID = :yearPhotoID');
        $sth->execute(array(':yearPhotoID' => $this -> yearID));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	
	
    protected function fill(array $row){
    	$this-> yearID = $row['yearPhotoID'];
    	$this -> yearText = $row['YearText'];
    	$this -> photo = $row['YearPhotoThumb'];
    }	

	private function uploadPicture($newImageName) {
		$ImagePathParts = array();			
		$imageUploadType = NULL;	
		
		$ImagePathParts['YearDirectory'] = $this -> yearText;	
		$imageUploadType = 10;
					
		Image::MoveAndRenameImage($_FILES['yearPhotoUpload']['tmp_name'], 
								  $ImagePathParts,
								  $this -> photo,
								  parent::seoUrl($newImageName), $imageUploadType);
		
	}

	public function Validate() {
		$validationErrors = array();
		
		
		if($this -> validate -> emptyInput($this -> photo)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	
	public function Save() {
		$photoName = $this -> yearText. '-photos';
		
		$this -> uploadPicture($photoName);
		$postData = array('YearPhotoThumb' => parent::seoUrl($photoName) . '.' . Image::getFileExt($this -> photo));	
						  
		$this->db->update('yearphotos', $postData, array('yearPhotoID' => $this -> yearID));	
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'photos/year/' . $this -> yearText);			  
		
	}
	

	
	





}