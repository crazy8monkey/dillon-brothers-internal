<?php

class ReviewSingle extends BaseObject {
	
	private $_id;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($eventID) {
        $instance = new self();
        $instance->_id = $eventID;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM reviews WHERE reviewID = :reviewID');
        $sth->execute(array(':reviewID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    
    }
	
	public function Approve() {
		$postData = array('reviewActive' => 1);	
		$this->db->update('reviews', $postData, array('reviewID' => $this -> _id));
		$this -> redirect -> redirectPage(PATH. 'shop/reviews');
	}
	
	public function Delete() {
		$delete = $this -> db -> prepare('DELETE FROM reviews WHERE reviewID = :reviewID');
		$delete -> execute(array(":reviewID" => $this -> _id));
		$this -> redirect -> redirectPage(PATH. 'shop/reviews');
	}


}