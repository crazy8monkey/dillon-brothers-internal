<?php


class BrandContent extends BaseObject {
	
	private $_id;
	
	public $Brand;
	public $LandingPageName;
	public $Content;
	public $IsPublished;
	
	public $CurrentPageName;
	public $CurrentContent;
	public $ISOEMPost;
	
	public $OEMBrandID;
	public $PartApparelBrandID;
	
	private $_CurrentPublisheDate;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($contentID) {
        $instance = new self();
        $instance-> _id = $contentID;
        $instance->loadRow();
        return $instance;
    }
		
	protected function loadRow() {
		$sth = $this -> db -> prepare('SELECT * FROM brandpostcontent WHERE brandpostID = :brandpostID');
        $sth->execute(array(':brandpostID' => $this->_id));
        $record = $sth->fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> CurrentPageName = $row['ContentTitle'];
		$this -> CurrentContent = $row['Content'];
		$this -> _CurrentPublisheDate = $row['publishDateFull'];
		$this -> IsPublished = $row['isPageActive'];
		$this -> ISOEMPost = $row['IsOEMPost'];
		$this -> OEMBrandID = $row['OEMBrandID'];
		$this -> PartApparelBrandID = $row['PartBrandID'];
    }
	
	public function GetID() {
		return $this -> _id;
	}
	
	public function GenerateBreadCrumbs() {
		if($this -> ISOEMPost == 1) {
			$brandSingle = Brand::WithID($this -> OEMBrandID);
			$brandName = '<a href="' . PATH .'brands/view/OEM/' . $this -> OEMBrandID . '">'. $brandSingle -> _brandName . ' Information</a>';
		} else {
			$partBrandSingle = PartsAccessoryBrand::WithID($this -> PartApparelBrandID);
			$brandName = '<a href="' . PATH .'brands/view/partapparel/' . $this -> PartApparelBrandID . '">'. $partBrandSingle -> PartsAccessoryBrandName . ' Information</a>';
		}
		
		//Create Quick Brand Article
		return '<a href="' . PATH  .'brands">Brands List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>' . $brandName . '<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Landing Page Content';
	}
	
	public function GenerateEditPageTitle() {
		if($this -> ISOEMPost == 1) {
			$brandSingle = Brand::WithID($this -> OEMBrandID);
			$title = 'Edit '. $brandSingle -> _brandName . ' Landing Content';
		} else {
			$partBrandSingle = PartsAccessoryBrand::WithID($this -> PartApparelBrandID);
			$title = 'Edit '. $partBrandSingle -> PartsAccessoryBrandName . ' Landing Content';
		}
		
		//Create Quick Brand Article
		return $title;
		
	}
	
	public function Validate() {
		$validationErrors = array();
		
		
			
		if(isset($this -> _id)) {
			if($this -> ISOEMPost == 1) {
				$duplicateContentCheck = $this->db->prepare('SELECT contentUrl FROM brandpostcontent WHERE contentUrl = :contentUrl AND brandpostID <> :brandpostID AND OEMBrandID = :OEMBrandID');
		        $duplicateContentCheck -> execute(array(":contentUrl" => parent::seoUrl($this -> LandingPageName),
														":brandpostID" => $this -> _id,
														":OEMBrandID" => $this -> OEMBrandID));	
			} else {
				$duplicateContentCheck = $this->db->prepare('SELECT contentUrl FROM brandpostcontent WHERE contentUrl = :contentUrl AND brandpostID <> :brandpostID AND PartBrandID = :PartBrandID');
		        $duplicateContentCheck -> execute(array(":contentUrl" => parent::seoUrl($this -> LandingPageName),
														":brandpostID" => $this -> _id,
														":PartBrandID" => $this -> PartApparelBrandID));
			}
		} else {
			$brandDetails = explode(':', $this -> Brand);
			if($brandDetails[0] == 1) {
				$duplicateContentCheck = $this->db->prepare('SELECT contentUrl FROM brandpostcontent WHERE contentUrl = :contentUrl AND OEMBrandID = :OEMBrandID');
		        $duplicateContentCheck -> execute(array(":contentUrl" => parent::seoUrl($this -> LandingPageName),
														":OEMBrandID" => $brandDetails[1]));	
			} else if($brandDetails[0] == 0) {
				$duplicateContentCheck = $this->db->prepare('SELECT contentUrl FROM brandpostcontent WHERE contentUrl = :contentUrl AND PartBrandID = :PartBrandID');
		        $duplicateContentCheck -> execute(array(":contentUrl" => parent::seoUrl($this -> LandingPageName),
														":PartBrandID" => $brandDetails[1]));
			}	
			
		}
				
		
		
		
		if($this -> validate -> emptyInput($this -> LandingPageName)) {
			array_push($validationErrors, array('inputID' => 2,
												'errorMessage' => 'Required'));
		} else if($duplicateContentCheck -> fetchAll()) {
			
				array_push($validationErrors, array("inputID" => 2,
													'errorMessage' => 'There is a page name associated with a different page within this brand, please type another one'));
			
		}
		
		
				
				
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Save() {
		try {
			
			
			
			if(isset($this -> _id)) {
				$redirectString = $this -> _id;
				
				$updateContentDBData = array();
				$updateContentDBData['modifiedDateFull'] = parent::TimeStamp();
				$updateContentDBData['ContentTitle'] = $this -> LandingPageName;
				$updateContentDBData['contentUrl'] = parent::seoUrl($this -> LandingPageName);
				
				
				$this->db->update('brandpostcontent', $updateContentDBData, array('brandpostID' => $this -> _id));
			} else {
				//0 = is OEM brand
				//1 = OEM/Part/Apparel Brand ID
				$brandDetails = explode(':', $this -> Brand);
				
				$newContentDBData = array();
				$newContentDBData['Content'] = $this -> Content;
				$newContentDBData['ContentTitle'] = $this -> LandingPageName;
				$newContentDBData['contentUrl'] = parent::seoUrl($this -> LandingPageName);
				$newContentDBData['IsOEMPost'] = $brandDetails[0];
				if($brandDetails[0] == 1) {
					$newContentDBData['OEMBrandID'] = $brandDetails[1];	
					$newContentDBData['PartBrandID'] = 0;
				} else {
					$newContentDBData['OEMBrandID'] = 0;	
					$newContentDBData['PartBrandID'] = $brandDetails[1];	
					$newContentDBData['PartApparelBrandType'] = $brandDetails[2];	
				}
				
				$NewContent = $this -> db -> insert('brandpostcontent', $newContentDBData);
				$redirectString = $NewContent;
			}
				
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'brands/view/pagecontent/' . $redirectString);
			
			
			
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Event Save Error: " . $e->getMessage();
			$TrackError -> type = "EVENT SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		
		}
		
	}

	public function Publish() {
		$publishDBData = array();
		$publishDBData['isPageActive'] = 1;
		$publishDBData['modifiedDateFull'] = parent::TimeStamp();
		
		if(empty($this -> _CurrentPublisheDate)) {
			$publishDBData['publishDateFull'] = parent::TimeStamp();
		}
					
		$this->db->update('brandpostcontent', $publishDBData, array('brandpostID' => $this -> _id));
		$this -> redirect -> redirectPage(PATH. 'brands/view/pagecontent/' . $this -> _id);	
	}

	public function Unpublish() {
		$publishDBData = array();
		$publishDBData['isPageActive'] = 0;
		$publishDBData['modifiedDateFull'] = parent::TimeStamp();
							
		$this->db->update('brandpostcontent', $publishDBData, array('brandpostID' => $this -> _id));
		$this -> redirect -> redirectPage(PATH. 'brands/view/pagecontent/' . $this -> _id);	
	}
	

	
	

		

}