<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>communication">Communication Portal</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create New Customer				
				</div>
			</div>
		</div>
	</div>
</div>


<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="sectionHeader">Create New Customer</div>		
								</div>
							</div>
							<form method='post' id="OutbountText" action="<?php echo PATH ?>communication/save/customer">
								
								<div class="row">
									<div class="col-md-4">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Customers First Name</div>
												<input type='text' name='customerFirstName' onchange="Globals.removeValidationMessage(1)" />	
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div id="inputID2">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Customers Last Name</div>
												<input type='text' name='customerLastName' onchange="Globals.removeValidationMessage(2)" />	
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div id="inputID3">
											<div class="errorMessage"></div>										
											<div class="input">
												<div class="inputLabel">Customers Phone Number</div>
												<input type='text' name='customerPhoneNumber' id="customerPhoneNumber" onchange="Globals.removeValidationMessage(3)" />	
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" class="greenButton" value="Save Customer" />
									</div>
								</div>	
							</form>
							
						</div>
					</div>
				</div>
			</div>				
		</div>
	</div>
</div>


