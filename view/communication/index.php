<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Communication
				</div>
			</div>
		</div>
	</div>
</div>


<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">Customers</div>		
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo PATH ?>communication/create/customer">
									<div class="greenButton" style="float:left">
										Create New Customer
									</div>
								</a>
							</div>
						</div>
						<?php foreach($this -> textMessages as $msgSingle): ?>
							<div class="row">
								<div class="col-md-12">
									<a href='<?php echo PATH?>communication/view/customer/<?php echo $msgSingle['textmessageconversationID']; ?>'>
										<?php echo $msgSingle['CustomerName'] ?>
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>				
			<div class="col-md-9">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">Chat Messages</div>		
							</div>
						</div>
						
						<?php 
							$activeChats = 1;
														
							$filteredActiveChats = array_filter($this -> chatconversations, function ($var) use($activeChats) {
								return $var['taskStatus'] == $activeChats; 
							});
						?>
						<?php if(count($filteredActiveChats)): ?>
							<div class="row">
								<div class="col-md-12">
									<strong>Current Active Chats</strong>
								</div>
							</div>
							<div style='margin:10px 0px;'>
								<?php foreach(array_chunk($filteredActiveChats, 4, true) as $activeChatFour) : ?>
									<div class="row">
										<?php foreach($activeChatFour as $activeChatSingle): ?>
											<div class="col-md-3">
												<div class="activeChat" data-toggle="tooltip" data-placement="bottom" title="Chat Active"></div>
												<div style='margin-left: 20px;'>
													<div style='margin-bottom: 10px;'>
														<a href='<?php echo PATH ?>communication/view/chatconversation/<?php echo $activeChatSingle['chatConversationID'] ?>'>View</a>
													</div>
													<table>
														<tr>
															<td style='padding-right:20px;'>Customer:</td>
															<td><?php echo $activeChatSingle['CustomerName'] ?></td>
														</tr>
														<tr>
															<td>Salesman:</td>
															<td><?php echo $activeChatSingle['firstName'] ?> <?php echo $activeChatSingle['lastName'] ?></td>
														</tr>
													</table>
												</div>
											</div>
										<?php endforeach; ?>	
									</div>								
								<?php endforeach; ?>								
							</div>
						<?php endif; ?>
						<?php 
							$inactiveChats = 0;
														
							$filteredInactiveChats = array_filter($this -> chatconversations, function ($var) use($inactiveChats) {
								return $var['taskStatus'] == $inactiveChats; 
							});
						?>
						
						<div class="row">
							<div class="col-md-12">
								<strong>Inactive Chats</strong>
							</div>
						</div>
						<div style='margin:10px 0px 0px 0px;'>
							<?php foreach(array_chunk($filteredInactiveChats, 4, true) as $InactiveChatFour) : ?>
								<div class="row">
									<?php foreach($InactiveChatFour as $InactiveChatSingle): ?>
										<div class="col-md-3" style='margin-bottom:10px;'>
											<div class="closedChat" data-toggle="tooltip" data-placement="bottom" title="Chat Inactive"></div>
											<div style='margin-left: 20px;'>
												<div style='margin-bottom: 10px;'>
													<a href='javascript:void(0);' onclick='CommunicationController.openChatConversation(<?php echo $InactiveChatSingle['chatConversationID'] ?>)'>View</a>	
												</div>
												<table>
													<tr>
														<td style='padding-right:20px;'>Customer:</td>
														<td><?php echo $InactiveChatSingle['CustomerName'] ?></td>
													</tr>
													<tr>
														<td>Salesman:</td>
														<td><?php echo $InactiveChatSingle['firstName'] ?> <?php echo $InactiveChatSingle['lastName'] ?></td>
													</tr>
												</table>
											</div>
										</div>
									<?php endforeach; ?>	
								</div>								
							<?php endforeach; ?>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="ViewingSingleConversation" style='display:none;'>
	<a href="javascript:void(0);" onclick="CommunicationController.CloseSingleConversation()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">Title Here
			<a href="javascript:void(0);" onclick="CommunicationController.CloseSingleConversation()">
				<div class="close" style="margin-top: 4px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>	
		</div>
		<div class="FormContent" id="ConversationSingleView" style="padding-left: 0px; overflow-y: auto; height: 370px;">
			
		</div>
		
		<div class="row" id="SalesManBubble" style="margin:0px; display:none;">
			<div class="col-md-12">
				<div style="clear:both">
					<div class="msgBubbleDillonUser">
						<span class='msgText'></span>
						<div class="bubbleArrow"></div>
					</div>
				</div>
				<div class="dateEntered dillonUser"></div>
			</div>
		</div>
		<div class="row" id="NewCustomerBubble" style="margin:0px; display:none">
			<div class="col-md-12">
				<div style="clear:both">
					<div class="msgBubbleCustomer">											
						<span class='msgText'></span>	
						<div class="bubbleArrow"></div>
					</div>			
				</div>
				<div class="dateEntered dateCustomer">														
				</div>
			</div>									
		</div>
		<div class="row" id="EndConversationIndicator" style="margin:0px; display:none">
			<div class="col-md-12">
				<div style="clear:both">
					<div class="endConversation">											
						<span class='msgText'></span>	
					</div>			
				</div>
			</div>									
		</div>
		
		
	</div>
	
</div>


