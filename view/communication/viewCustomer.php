<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>communication">Communication Portal</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>View Customer Information				
				</div>
			</div>
		</div>
	</div>
</div>


<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class='row'>
							<div class="col-md-12">
								<div class="sectionHeader">Viewing Customer Info</div>		
							</div>	
						</div>
						<div class='row'>
							<div class="col-md-3">
								<div class="subHeader">
									Customer Information
								</div>
								<?php echo $this -> TextMessage -> CustomerName ?><br />
								<?php echo $this -> PhoneFormat($this -> TextMessage -> phoneNumber) ?>
								
							</div>
							<div class="col-md-9">
								<?php if(count($this -> TextMessage -> CurrentMessages) > 0): ?>
								
									<div class="subHeader">
										Text Messages Sent to Customer
									</div>
									<div id="TextMessageContent">
										<?php 
											//$userMessages = array_search('0', array_column($this -> TextMessage -> CurrentMessages, 'fromCustomer'));
											$fromCustomer = '0';
											
											$filteredUserMessages = array_filter($this -> TextMessage -> CurrentMessages, function ($var) use($fromCustomer) {
												return $var['fromCustomer'] == $fromCustomer; 
											});
											
											$filteredUserMessages = array_values($filteredUserMessages);
											
											//echo '<pre>';
											//print_r($filteredUserMessages);
											//echo '</pre>'
											
											$prevDate = null;
										?>
										<?php foreach($this -> TextMessage -> CurrentMessages as $msgSingle): ?>
											<?php 
												$date = explode('T', $msgSingle['textmessagesent']);
												$dateEnteredText  = $this -> recordedTime -> formatTime($date[1]);
												$showDateHeader = false;
												
												if($msgSingle['fromCustomer'] == 0) {
													$cssClass = "msgBubbleDillonUser";
													$dateClass = "dillonUser";
													$dateEnteredText .= ' / '. $this -> PhoneFormat($msgSingle['deptNumber']) . ' / ' . $msgSingle['firstName'] . ' ' . $msgSingle['lastName'];
												} else {
													$cssClass = "msgBubbleCustomer";
													$dateClass = "dateCustomer";
												}
												
												if($prevDate != $date[0]) {
									            	$showDateHeader = true;
									            }
												
												$prevDate = $date[0];
											?>
											<?php if($showDateHeader == true): ?>											
												<div class="row">
													<div class="col-md-12" style='padding-left: 0px; padding-right: 0px;'>
														<div class="dateHeader">
															<div style='background:white; padding-right:5px; float:left; position:relative; z-index:1'>
																<?php echo $this -> recordedTime -> formatDate($date[0]); ?>		
															</div>
															<div class="divider"></div>
															<div style='clear:both'></div>
														</div>
													</div>
												</div>
											<?php endif; ?>
											
											<div class="row">
												<div class="col-md-12" style='padding-left: 0px; padding-right: 0px;'>
													<div style='clear:both'>
														<div class='<?php echo $cssClass ?>'>
															<?php echo $msgSingle['textMessage'] ?>
															<div class="bubbleArrow"></div>
														</div>			
													</div>
													
													<div class='dateEntered <?php echo $dateClass ?>'>
														<?php echo $dateEnteredText ?>
													</div>
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
								<div class='row'>
									<div class="col-md-12" style='margin-bottom:10px;'>
										<?php if(count($this -> TextMessage -> CurrentMessages) > 0): ?>
											<strong>Send Text Message to Customer</strong>
										<?php else: ?>
											<strong>Send Text Disclaimer</strong>
										<?php endif; ?>
									</div>
								</div>
								<div class='row'>
									<div class="col-md-12">
										<form method='post' id="OutbountText" action="<?php echo PATH ?>communication/save/outboundtext/<?php echo $this -> TextMessage -> GetID() ?>">
											<div class='row'>
												<div class="col-md-6">
													<div id="inputID5">
														<div class="errorMessage"></div>										
														<div class="input">
															<?php 
																
															
															
																$stores = array_column($this -> smsnumbers, 'StoreName');
																$storeArray = array_unique($stores, SORT_REGULAR);
																sort($storeArray);
															?>
															<select name='storeDeptSelection'>
																<option value=''>Please Select Department</option>
																<?php foreach($storeArray as $storeSingle): ?>
																	<?php 
																		$filteredSMSStoreNumbers = array_filter($this -> smsnumbers, function ($var) use($storeSingle) {
																			return $var['StoreName'] == $storeSingle; 
																		});
																	?>
																	<optgroup label='<?php echo $storeSingle ?>'>
																		<?php foreach($filteredSMSStoreNumbers as $number): ?>
																			<option <?php if($number['smsdeptID'] == $filteredUserMessages[count($filteredUserMessages) - 1]['twilioNumberUsed']): ?> selected <?php endif; ?> value='<?php echo $storeSingle ?> / <?php echo $number['deptName'] ?> / <?php echo $number['deptNumber'] ?> / <?php echo $number['smsdeptID'] ?>'><?php echo $storeSingle ?> - <?php echo $number['deptName'] ?></option>
																		<?php endforeach; ?>
																	</optgroup>
																<?php endforeach; ?>
															</select>					
															</div>
														</div>
													</div>
												</div>
												
												<?php if(count($this -> TextMessage -> CurrentMessages) > 0): ?>
													<div class="row">
														<div class="col-md-12">
															<div id="inputID4">
																<div class="errorMessage"></div>										
																<div class="input">
																	<div class="inputLabel">Send New Text Message</div>
																	<textarea name='TextMessageValue' onchange="Globals.removeValidationMessage(4)"></textarea>	
																</div>
															</div>
														</div>
													</div>
												<?php endif; ?>
												
												
												<div class="row">
													<div class="col-md-12">
														<input type="submit" class="greenButton" value="Send Message" />
													</div>
												</div>	
											</form>
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>				
		</div>
	</div>
</div>


