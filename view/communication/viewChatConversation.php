<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>communication">Communication Portal</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>View Chat Conversation
				</div>
			</div>
		</div>
	</div>
</div>


<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class='row'>
							<div class="col-md-12">
								<div class="sectionHeader">Viewing Chat Conversation</div>		
							</div>	
						</div>
						<div class='row'>
							<div class="col-md-3">
								<div class="row">
									<div class="col-md-12">
										<div class="subHeader">
											Customer Information
										</div>				
									</div>
								</div>
								<?php if($this -> conversationDetail -> taskStatus == 1): ?>								
									<div class="row buttonContainer">
										<div class="col-md-12">
											<a href="javascript:void(0);" onclick='CommunicationController.SendCloseChatMessage("<?php echo $this -> conversationDetail -> GetID()?>")'>
												<div class="greenButton" style="float:left">
													Close Conversation
												</div>
											</a>
										</div>
									</div>
								<?php endif; ?>
							</div>
							
							<div class="col-md-9">	
								<div class='row'>
									<div class="col-md-12">
										<div id="messagesLogged" style='overflow-y: scroll; max-height: 400px; overflow-x: hidden;'>
											<?php foreach($this -> conversationDetail -> CurrentMessages as $msgSingle): ?>
												<?php 
												$attributes = json_decode($msgSingle -> attributes, true);
												//print_r($msgSingle);
												if($attributes['fromCustomer'] == 1) {
													$cssClass = "msgBubbleCustomer";
													$dateClass = "dateCustomer";
												} else {
													$cssClass = "msgBubbleDillonUser";
													$dateClass = "dillonUser";
												}
												
												?>
												<div class="row" style='margin:0px'>
													<div class="col-md-12">
														<div style='clear:both'>
															<div class='<?php echo $cssClass ?>'>
																<?php echo $msgSingle -> body ?>	
																<div class="bubbleArrow"></div>
															</div>			
														</div>
														<div class="dateEntered <?php echo $dateClass ?>">
															<?php 
																$msgSingle -> dateCreated -> setTimezone(new DateTimeZone('America/Chicago'));
																echo $msgSingle -> dateCreated -> format('F j, Y / g:i a'); 
															?>
														</div>
													</div>
													
												</div>
											<?php endforeach; ?>									
										</div>
									</div>
								</div>
								
								<div class='row'>
									<div class="col-md-12">
										<textarea id='NewMessage' style='display:none; margin-top:20px;'></textarea>
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>				
		</div>
	</div>
</div>


