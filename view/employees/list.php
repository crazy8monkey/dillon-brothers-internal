<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Employees
				</div>
			</div>
			<div class="row buttonContainer">
				<div class="col-md-12">
					<a href="<?php echo $this -> pages -> employees() ?>/create">
						<div class="greenButton" style="float:left">
							New Employee
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
			<?php if(count($this -> Employees) > 0): ?>
	
		<?php $prevStore = null ?>
		<?php
			$PrevDepartments = NULL;  
			$employeesByStore = array(); 
		?>
		<?php foreach($this -> Employees as $key => $store) : ?>	
			<?php 
				$NewStore = $store['StoreName'];
				$newDepartment = $store['Department'];
	
				if($prevStore != $NewStore) {
					//echo "<strong>Array Push</strong><br />";
					array_push($employeesByStore, array("Store" => $NewStore,
														"Employees" => $this -> Employees));	
				}
				
				$prevStore = $NewStore; 
				$PrevDepartments = $newDepartment;
			?>		
		<?php endforeach; ?>
		
		
		<!-- clear wrong date newsletters-->
		<?php 
		foreach($employeesByStore as $key => $value) {
			$Store = $value['Store']; 
			foreach($value['Employees'] as $employeeKey => $employee) {
				$employeeStore = $employee['StoreName'];
				if($Store != $employeeStore) {
					unset($employeesByStore[$key]['Employees'][$employeeKey]);
				}		
			}
		 } 
		 ?>
	
		<?php foreach($employeesByStore as $storeInfo) : ?>		
			<div class="row">
				<div class="col-md-12">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="subHeader"><?php echo $storeInfo['Store'] ?></div>	
				
			
			<?php foreach(array_chunk($storeInfo['Employees'], 6, true) as $employeeSets) : ?>
				
				<div class="row" style='margin-bottom:10px;'>
					<?php foreach($employeeSets as $employeeSingle) : ?>	
						<div class="col-md-2">
							<a href="<?php echo $this -> pages -> employees() ?>/edit/<?php echo $employeeSingle['EmployeeID'] ?>">
								<div style='text-align:center; width:100%'>								
									<img src="<?php echo PHOTO_URL . 'employees/'.  $employeeSingle['Photo'] ?>" style='max-width: 100%;' />
									<div class="EmployeeName">
										<?php echo $employeeSingle['EmployeeFirstName'] . ' ' . $employeeSingle['EmployeeLastName'] ?>
									</div>
									<div class="jobTitle">
										<?php echo $employeeSingle['Title'] ?>
									</div>
								</div>
							</a>
							
							<div class="deleteButton">
								<a href="<?php echo $this -> pages -> employees() ?>/delete/<?php echo $employeeSingle['EmployeeID'] ?>">
									<div class="redButton" data-toggle="tooltip" data-placement="bottom" title="Delete">
										<i class="fa fa-times" aria-hidden="true"></i>
									</div>		
								</a>
								
							</div>
								
							
							
						</div>
					<?php endforeach; ?>
				</div> 
					
			<?php endforeach; ?>
								
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
	
		
	</div>
</div>

<div class="container">
	
	
	

	
</div>
