<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo $this -> pages -> employees() ?>">Employees</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Employee
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Edit Employee
		</div>
	</div>
	
	<form method="post" action="<?php echo $this -> pages -> employees() ?>/save/<?php echo $this -> EmployeeSingle -> GetID() ?>" id="EmployeeAddForm">
		<div class="row">
			<div class="col-md-12">
				<strong>Recommended: 165x165</strong>
				<?php echo $this -> form -> FileInput("Employee Photo", "employeePhoto") ?>
			</div>
		</div>
		<div class="row" style='margin-bottom:10px;'>
			<div class="col-md-12">
				<div><strong>Current Photo</strong></div>
				<img src='<?php echo PHOTO_URL ?>employees/<?php echo $this -> EmployeeSingle -> CurrentPhoto?>' width='165' />
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "First Name", "employeeFirstName", 1, $this -> EmployeeSingle -> firstName) ?>
				</div>
			</div>
			<div class="col-md-3">
				<div id="inputID6">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Last Name", "employeeLastName", 6, $this -> EmployeeSingle -> lastName) ?>
				</div>
			</div>
			<div class="col-md-6">
				<div id="inputID2">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Title", "employeeTitle", 2, $this -> EmployeeSingle -> Title) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID3">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Email", "employeeEmail", 3, $this -> EmployeeSingle -> email) ?>
				</div>
			</div>
			<div class="col-md-6">
				<?php echo $this -> form -> Input("text", "Phone", "employeePhone", NULL, $this -> EmployeeSingle -> Phone) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID4"><div class="errorMessage"></div></div>
				<?php echo $this -> form -> checkboxes("Store", "store[]", $this -> Stores(), 4, $this -> EmployeeSingle -> GetStores()) ?>
			</div>
			<div class="col-md-6">
				<div id="inputID5">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> select("Department", "employeeDepartment", $this -> Departments(), 5, $this -> EmployeeSingle -> department) ?>
				</div>						
			</div>			
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit("Save", "greenButton") ?>
			</div>
		</div>
	</form>
</div>



