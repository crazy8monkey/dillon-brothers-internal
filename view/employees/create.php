<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo $this -> pages -> employees() ?>">Employees</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New Employee
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			New Employee
		</div>
	</div>
	<form method="post" action="<?php echo PATH ?>employees/save" id="EmployeeAddForm" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-12">
				<strong>Recommended: 165x165</strong>
				<?php echo $this -> form -> FileInput("Employee Photo", "employeePhoto") ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "First Name", "employeeFirstName", 1, NULL) ?>
				</div>
			</div>
			<div class="col-md-3">
				<div id="inputID6">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Last Name", "employeeLastName", 6, NULL) ?>
				</div>
			</div>
			<div class="col-md-6">
				<div id="inputID2">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Title", "employeeTitle", 2, NULL) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID3">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Email", "employeeEmail", 3, NULL) ?>
				</div>
			</div>
			<div class="col-md-6">
				<?php echo $this -> form -> Input("text", "Phone", "employeePhone") ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID4"><div class="errorMessage"></div></div>
				<?php echo $this -> form -> checkboxes("Store", "store[]", $this -> Stores(), 4) ?>
			</div>
			<div class="col-md-6">
				<div id="inputID5">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> select("Department", "employeeDepartment", $this -> Departments(), 5) ?>
				</div>						
			</div>			
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit("Save", "greenButton") ?>
			</div>
		</div>
	</form>
</div>



