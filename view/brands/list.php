<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Brand Content
				</div>
			</div>
			<div class="row buttonContainer">
				<div class="col-md-12">
					<a href="<?php echo PATH ?>brands/create/brand">
						<div class="greenButton" style="float:left">
							Create New Brand
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								All Brands List
							</div>
						</div>
						<?php foreach(array_chunk($this -> brandsList, 6, true) as $brandSix) : ?>
							<div class="row">
								<?php foreach($brandSix as $brandSingle): ?>
									<div class="col-md-2">
										<?php 
											if($brandSingle['ISOEMBrand'] == 1) {
												$url = PATH . 'brands/view/OEM/'. $brandSingle['BrandID'];
											} else {
												$url = PATH . 'brands/view/partapparel/'. $brandSingle['BrandID'];
											}
										?>
										
										
										<a href='<?php echo $url ?>'>
											<div class="GrayBox BrandSingleElement">
												<?php if($brandSingle['ISOEMBrand'] == 1): ?>											
													<div class="bradType">
														OEM Brand
													</div>
													<div class="brandImage">
														<img src='<?php echo PHOTO_URL ?>brands/<?php echo $brandSingle['BrandImage'] ?>' />
													</div>
												<?php endif; ?>	
												<?php if($brandSingle['ISOEMBrand'] == 0): ?>											
													<div class="bradType">
														<?php
															switch($brandSingle['BrandType']) {
																case 1:
																	echo "Part Brand";
																	break;
																case 2:
																	echo "Apparel Brand";
																	break;
																case 3:
																	echo "Part/Apparel Brand";
																	break;
															}
														?>
													</div>
													<div class="brandImage">
														<img src='<?php echo PHOTO_URL ?>PartsBrand/<?php echo $brandSingle['BrandImage'] ?>' />
													</div>
												<?php endif; ?>	
											</div>	
										</a>
										
										
									</div>
								<?php endforeach; ?>	
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

