<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>brands">Brands List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New Parts/Accessories Brand
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage">
	<div class="container-fluid">
		<div class="WhiteSectionDiv">
			<div class="content">
				<div class="row">
					<div class="col-md-12 sectionHeader">
						New Parts/Accessories Brand
					</div>
				</div>
				<form method="post" action="<?php echo PATH ?>brands/save/partapparel" id="SinlgeBrandForm">
					<div class="row">
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Brand Name", "PartApparelBrandName", 1) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div id="inputID5">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Brand Description (Apparel)</div>
											<div class="descriptiveInfo">This is required if this associated with an Apparel brand that we carry</div>
											<textarea id="ApparelBrandDescription" class='textareaMinHeight' name="apparelBrandDescription" onchange="Globals.removeValidationMessage(5)"></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div id="inputID3">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Brand Description (Parts)</div>
											<div class="descriptiveInfo">This is required if this associated with a Parts brand that we carry</div>
											<textarea id="PartsBrandDescription" class='textareaMinHeight' name="partBrandDescription" onchange="Globals.removeValidationMessage(3)"></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit("Save", "greenButton") ?>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-12">
											<div class="descriptiveInfo">120px (width) x 120 (height)</div>
										</div>
									</div>
									<div id="inputID2">
										<div class="errorMessage"></div>
									</div>
									<?php echo $this -> form -> FileInput("Brand Logo", "brandLogo") ?>
									<div class="emptyPhotoPlacement">
										<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
										<div class="required">Brand Image Is Required</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID4">
										<div class="errorMessage"></div>
									
										<div class="input">
											<div class="inputLabel">Is this brand an Apparel or Parts brand?</div>
											<select name="BrandType">
												<option value="0">Please Select brand category</option>
												<option value="1">Parts Brand</option>
												<option value="2">Apparel Brand</option>
												<option value="3">Both Parts/Apparel Brand</option>
											</select>
											
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</div>

<div class="container">

	
	
		
					
	

</div>
