<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php if(isset($this -> OEMCreate)): ?>
						<a href="<?php echo PATH ?>brands">Brands List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>brands/view/OEM/<?php echo $this -> OEMBrandSingle -> brandID ?>"><?php echo $this -> OEMBrandSingle -> _brandName ?> Information</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create <?php echo $this -> OEMBrandSingle -> _brandName ?> Landing Page
					<?php endif; ?>
					<?php if(isset($this -> PartBrandCreate)): ?>
						<a href="<?php echo PATH ?>brands">Brands List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>brands/view/partapparel/<?php echo $this -> PartBrandSingle -> GetID() ?>"><?php echo $this -> PartBrandSingle -> PartsAccessoryBrandName ?> Information</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create <?php echo $this -> PartBrandSingle -> PartsAccessoryBrandName ?> Landing Page
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="container">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									<?php if(isset($this -> OEMCreate)): ?>
										Create <?php echo $this -> OEMBrandSingle -> _brandName ?> Landing Page
									<?php endif; ?>
									<?php if(isset($this -> PartBrandCreate)): ?>
										Create <?php echo $this -> PartBrandSingle -> PartsAccessoryBrandName ?> Landing Page
									<?php endif; ?>
								</div>
							</div>
							<form action='<?php echo PATH ?>brands/save/landingpage' method='post' id="BrandLadingPageContent">
								<?php if(isset($this -> OEMCreate)): ?>
									<input type='hidden' name='brandContentSelect' value='1:<?php echo $this -> OEMBrandSingle -> brandID ?>' />
								<?php endif; ?>
								<?php if(isset($this -> PartBrandCreate)): ?>
									<input type='hidden' name='brandContentSelect' value='0:<?php echo $this -> PartBrandSingle -> GetID() ?>:<?php echo $this -> PartBrandSingle -> currentBrandType ?>' />
								<?php endif; ?>
								
								<div class="row">
									<div class="col-md-12">
										<div id="inputID2">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Landing Page Name</div>
												<input type='text' name='brandLandingPageName' />
											</div>
										</div>
										
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="input">
											<div class="inputLabel">Content</div>
											<textarea name='brandLandingPageContent' id="brandLandingPageContent"></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type='submit' value='Save' class='greenButton' />
									</div>
								</div>
							</form>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

