<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php echo $this -> brandContent -> GenerateBreadCrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="container">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									<?php echo $this -> brandContent -> GenerateEditPageTitle(); ?>
								</div>
							</div>
							<div class="row buttonContainer">
								<div class="col-md-12">
									<?php if($this -> brandContent -> IsPublished == 0): ?>
										<a href='<?php echo PATH ?>brands/publish/<?php echo $this -> brandContent -> GetID() ?>'>
											<div class="whiteButton" style='float:left'>
												Publish
											</div>
										</a>
									<?php endif; ?>
									<?php if($this -> brandContent -> IsPublished == 1): ?>
										<a href='<?php echo PATH ?>brands/unpublish/<?php echo $this -> brandContent -> GetID() ?>'>
											<div class="whiteButton" style='float:left'>
												Unpublish
											</div>
										</a>
									<?php endif; ?>
								</div>
							</div>
							<form action='<?php echo PATH ?>brands/save/landingpage/<?php echo $this -> brandContent -> GetID() ?>' method='post' id="BrandLadingPageContent">
								<div class="row">
									<div class="col-md-12">
										<div id="inputID2">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Landing Page Name</div>
												<input type='text' name='brandLandingPageName' value='<?php echo $this -> brandContent -> CurrentPageName?>' />
											</div>
										</div>
										
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="input">
											<div class="inputLabel">Content</div>
											<textarea name='brandLandingPageContent' id="brandLandingPageContent">
												<?php echo $this -> brandContent -> CurrentContent?>
											</textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type='submit' value='Save' class='greenButton' />
									</div>
								</div>
							</form>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

