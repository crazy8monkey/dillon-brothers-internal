<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php if(isset($this -> OEMBrandSingle)) : ?>
						<a href="<?php echo PATH ?>brands">Brands List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> OEMBrandSingle -> _brandName; ?> Information
					<?php endif; ?>		
					<?php if(isset($this -> PartApparelContent)) : ?>
						<a href="<?php echo PATH ?>brands">Brands List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> PartApparelContent -> PartsAccessoryBrandName; ?> Information
					<?php endif; ?>		
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								<?php 
									if(isset($this -> OEMBrandSingle)) {
										echo $this -> OEMBrandSingle -> _brandName;		
									}
									
									if(isset($this -> PartApparelContent)) {
										echo $this -> PartApparelContent -> PartsAccessoryBrandName;		
									}
								?> Information
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12 subHeader">
										Basic Info.
									</div>
								</div>
								<?php 
									if(isset($this -> OEMBrandSingle)) {
										$url = PATH . 'brands/save/oembrand/' .  $this -> OEMBrandSingle -> GetID();
										$image = PHOTO_URL . 'brands/' . $this -> OEMBrandSingle -> BrandImage;
										if(!empty($this -> OEMBrandSingle -> NewAlternateLogo)) {
											$showAlternateLogo = true;
											$alternateImage = PHOTO_URL . 'brands/' . $this -> OEMBrandSingle -> NewAlternateLogo;
										} else {
											$showAlternateLogo = false;
										}
									}
									
									if(isset($this -> PartApparelContent)) {
										$url = PATH . 'brands/save/partapparel/' . $this -> PartApparelContent -> GetID();
										$image = PHOTO_URL . 'PartsBrand/' . $this -> PartApparelContent -> CurrentBrandImage;
									}
									
								?>
								<div class="row" style='margin-bottom:30px;'>
									<div class="col-md-6">
										<div><strong>Current Brand Image</strong></div>
										<img src='<?php echo $image ?>' />
									</div>
									<?php if(isset($this -> OEMBrandSingle)): ?>									
										<?php if($showAlternateLogo == true): ?>
											<div class="col-md-6">
												<div><strong>Alternate Brand Image</strong></div>
												<img src='<?php echo $alternateImage ?>' />
											</div>
										<?php endif; ?>
									<?php endif; ?>
								</div>
								<form action="<?php echo $url ?>" method="post" id="SinlgeBrandForm">
									<div class="row">
										<div class="col-md-12">
											<div class="descriptiveInfo">120px (width) x 120 (height)</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div id="inputID2">
												<div class="errorMessage"></div>
											</div>
											<?php echo $this -> form -> FileInput("Brand Logo", "brandLogo") ?>
										</div>
										<?php if(isset($this -> OEMBrandSingle)): ?>									
											<div class="col-md-6">
												<?php echo $this -> form -> FileInput("Alternate Logo", "alternateBrandLogo") ?>
											</div>
										<?php endif; ?>
									</div>
									
									<div class="row">
										<div class="col-md-12">
											<div id="inputID1">
												<div class="errorMessage"></div>
												<div class="input">
													<div class="inputLabel">Brand Name</div>
													<?php if(isset($this -> OEMBrandSingle)): ?>
														<input type="text" name="OEMbrandName" onchange="Globals.removeValidationMessage(1)" value='<?php echo $this -> OEMBrandSingle -> _brandName ?>' />
													<?php endif; ?>
													<?php if(isset($this -> PartApparelContent)): ?>
														<input type="text" name="PartApparelBrandName" onchange="Globals.removeValidationMessage(1)" value='<?php echo $this -> PartApparelContent -> PartsAccessoryBrandName ?>' />
													<?php endif; ?>
												</div>
											</div>
										</div>
									</div>
									<?php if(isset($this -> OEMBrandSingle)): ?>
										<div class="row">
											<div class="col-md-12">
												<?php echo $this -> form -> textarea("Brand Description", "brandDescription", NULL, $this -> OEMBrandSingle -> brandDescription) ?>
											</div>
										</div>
									<?php endif; ?>
									<?php if(isset($this -> PartApparelContent)): ?>
										<?php if($this -> PartApparelContent -> currentBrandType == 3 || $this -> PartApparelContent -> currentBrandType == 2): ?>
											<div class="row">
												<div class="col-md-12">
													<?php echo $this -> form -> textarea("Apparel Description", "apparelBrandDescription", NULL, $this -> PartApparelContent -> ApparelDescription) ?>	
												</div>
											</div>
										<?php endif; ?>
										<?php if($this -> PartApparelContent -> currentBrandType == 3 || $this -> PartApparelContent -> currentBrandType == 1): ?>
											<div class="row">
												<div class="col-md-12">
													<?php echo $this -> form -> textarea("Part Description", "partBrandDescription", NULL, $this -> PartApparelContent -> PartsDescription) ?>	
												</div>
											</div>
										<?php endif; ?>
										<div class="row">
											<div class="col-md-12">
												<div class="input">
													<div class="inputLabel">Is this brand an Apparel or Parts brand?</div>												
													<select name="BrandType">
														<option value='0'>Please Select brand Type</option>
														<option value='1'<?php if($this -> PartApparelContent -> currentBrandType == 1): ?> selected <?php endif; ?>>Parts Brand</option>
														<option value='2'<?php if($this -> PartApparelContent -> currentBrandType == 2): ?> selected <?php endif; ?>>Apparel Brand</option>
														<option value='3'<?php if($this -> PartApparelContent -> currentBrandType == 3): ?> selected <?php endif; ?>>Both Parts/Apparel Brand</option>
													</select>
												</div>
											</div>
										</div>
										
									<?php endif; ?>
									<div class="row">
										<div class="col-md-12">
											<?php echo $this -> form -> Submit("Save", "greenButton") ?>
										</div>
									</div>	
								</form>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12 subHeader">
										Landing Pages
									</div>
								</div>
								<?php if(isset($this -> OEMBrandSingle)) : ?>
									<div class="row">
										<div class="col-md-12 buttonContainer">
											<a href='<?php echo PATH ?>brands/create/oemlandingpage/<?php echo $this -> OEMBrandSingle -> brandID ?>'>
												<div class="greenButton" style='float:left'>
													Create Landing page
												</div>
											</a>
										</div>
									</div>
									<?php foreach($this -> OEMContent as $pageSingle): ?>
										<div class="row">
											<div class="col-md-12">
												<a href='<?php echo PATH ?>brands/view/pagecontent/<?php echo $pageSingle['brandpostID'] ?>'><?php echo $pageSingle['ContentTitle'] ?></a>
											</div>
										</div>
									<?php endforeach; ?>			
								<?php endif; ?>
								<?php if(isset($this -> PartApparelContent)): ?>
									<div class="row">
										<div class="col-md-12 buttonContainer">
											<a href='<?php echo PATH ?>brands/create/partapparellandingpage/<?php echo $this -> PartApparelContent -> GetID() ?>'>
												<div class="greenButton" style='float:left'>
													Create Landing page
												</div>
											</a>
										</div>
									</div>
									<?php foreach($this -> PartApparelContentList as $pageSingle): ?>
										<div class="row">
											<div class="col-md-12">
												<a href='<?php echo PATH ?>brands/view/pagecontent/<?php echo $pageSingle['brandpostID'] ?>'><?php echo $pageSingle['ContentTitle'] ?></a>
											</div>
										</div>
									<?php endforeach; ?>	
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

