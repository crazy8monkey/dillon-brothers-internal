<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Blog
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> blog() ?>">
									<div class="whiteButton" style="text-align:center; width:100%">
										Blog Posts
									</div>	
								</a>
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> blog() ?>/categories">
									<div class="whiteButton" style="text-align:center; width:100%">
										Blog Categories
									</div>				
								</a>
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<div class="whiteButton noButton" style="text-align:center; width:100%; border:1px solid white">
									Comments
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Blog Comments
							</div>
						</div>
						<?php if(count($this -> enteredBlogs) > 0): ?>
							<?php foreach($this -> enteredBlogs as $post): ?>
								<div class="row">
									<div class="col-md-12">
										<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 0px 5px 0px;"></div>
										<a href="javascript:void(0)" onclick="BlogController.ExpandBlogComments(this)">
											<div class="blogExpand">+</div><?php echo $post['blogPostName'] ?>
											<?php if(strpos($post['CommentVisibles'], '0')!== false): ?>
												<i class="fa fa-exclamation-circle" aria-hidden="true" style='color:#c30000; margin-left:5px;'></i>
											<?php endif; ?>	
										</a>
										<?php if(!empty($post['postID'])) : ?>
											<?php 
												$commentsArray = array();
												$commentIDS = explode(",", $post['CommentIDs']);
												$commentUserName = explode(",", $post['CommentFullNames']);
												$commentNotes = explode(",", $post['CommenNotes']);
												$commentEnteredDates = explode(",", $post['CommentEnteredDates']);
												$commentEnteredTimes = explode(",", $post['CommentEnteredTimes']);
												$commentApprovalStatus = explode(",", $post['CommentVisibles']);
												
												foreach($commentIDS as $key => $value) {
													array_push($commentsArray, array("ID" => $commentIDS[$key],
																					 "UserFullName" => $commentUserName[$key],
																					 "UserCommentNotes" => $commentNotes[$key],
																					 "Date" => $commentEnteredDates[$key],
																					 "Time" => $commentEnteredTimes[$key],
																					 "Approved" => $commentApprovalStatus[$key]));
												}	
												
										?>
										<div class="CommentList" style='display:none; margin-left:10px; margin-top:10px;'>
											<div class="row" style=" margin: 0px;">
												<div class="col-md-12">
													<?php foreach($commentsArray as $singleComment): ?>
														<?php 
														$showDeclineApproveButtons = false;
														$stylePadding = NULL;
														
														?>
														<div class="commentElementContainer">
															
															
															
															<div class="commentSingle">
																<?php if($singleComment['Approved'] == 0) {
																	$showDeclineApproveButtons = true;	
																	$stylePadding = "margin-left:20px;";
																} else {
																	$stylePadding = "clear:both;";
																}
																?>
																<?php if($showDeclineApproveButtons == true) :?>
																	<div style='float: left;'>
																		<div>
																			<a href="<?php echo $this -> pages -> blog() ?>/save/approvecomment/<?php echo $singleComment['ID'] ?>">
																				<i class="fa fa-check-circle" aria-hidden="true" style='color:#18ad00' data-toggle="tooltip" data-placement="bottom" title="Approve Comment"></i>															
																			</a>
																		</div>
																		<div>
																			<a href="<?php echo $this -> pages -> blog() ?>/delete/comment/<?php echo $singleComment['ID'] ?>">
																				<i class="fa fa-times-circle" aria-hidden="true" style='color:#c30000' data-toggle="tooltip" data-placement="bottom" title="Delete Comment"></i>
																			</a>
																			
																		</div>
																	</div>
																<?php endif?>
																<div style='<?php echo $stylePadding ?>'>
																	<?php echo $singleComment['UserCommentNotes'] ?>
																	<div class="Time">
																		<?php echo $this -> recordedTime -> formatShortDate($singleComment['Date']) ?>
																	</div>												
																</div>
																<?php if($showDeclineApproveButtons == false): ?>
																	<a href="<?php echo $this -> pages -> blog() ?>/delete/comment/<?php echo $singleComment['ID'] ?>" onclick="return confirm('Are you sure you want to delete this comment?');">
																		<i class="fa fa-times-circle" aria-hidden="true" style='position: absolute; top: -6px; right: -6px;' data-toggle="tooltip" data-placement="bottom" title="Delete Comment"></i>
																	</a>
																<?php endif; ?>
																
																<div class="caret"></div>
															</div>							
															<div class="commentName">
																<?php 
																	if(!empty($singleComment['UserFullName'])) {
																		$userNameFull = $singleComment['UserFullName'];
																	} else {
																		$userNameFull = "Anonymous";
																	}
																
																?>
																<?php echo $userNameFull ?>
															</div>			
														</div>
					
													<?php endforeach; ?>	
												</div>							
											</div>
					
											
										</div>
											
										<?php else: ?>
											<div class="CommentList" style='display:none; margin-left:10px; margin-top:10px;'>
												<div class="row" style=" margin: 0px;">
													<div class="col-md-12">
														There are no comments in this post
													</div>
												</div>
											</div>
											
										<?php endif; ?>
										
									
										
									</div>				
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class='col-md-12'>
									There are no blog posts entered
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	
	
	
	
	
</div>
