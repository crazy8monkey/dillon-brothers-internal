<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Blog
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<?php 
				$showLeftSide = false;
				$listViewClass = 'col-md-12';
				if((isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogAddEditCategories') == 1) && (isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogApproveDeclineComments') == 1)) {
					$showLeftSide = true;
					$listViewClass = 'col-md-9';
				} 
			
			?>
			<?php if($showLeftSide == true): ?>
			
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row buttonContainer">
							<div class="col-md-12">
								<div class="whiteButton noButton" style="margin-right:5px; border:1px solid white; text-align:center; width: 100%;">
									Blog Posts
								</div>
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogAddEditCategories') == 1) : ?>
									<a href="<?php echo $this -> pages -> blog() ?>/categories">
										<div class="whiteButton" style="text-align:center; width: 100%;">
											Blog Categories
										</div>				
									</a>
								<?php endif; ?>
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogApproveDeclineComments') == 1) : ?>
									<a href="<?php echo $this -> pages -> blog() ?>/comments">
										<div class="whiteButton" style="text-align:center; width: 100%;">
											Comments
										</div>				
									</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<div class="<?php echo $listViewClass ?>">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Blog Posts
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> blog() ?>/create/post">
									<div class="greenButton" style="float:left">
										New Post
									</div>
								</a>
							</div>
						</div>	
						<?php if(count($this -> blogpostlist) > 0): ?>
							<?php foreach($this -> blogpostlist as $post): ?>
								<div class="row">
									<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
									<div class="col-md-12">
										<a href="<?php echo $this -> pages -> blog() ?>/delete/post/<?php echo $post['blogPostID']; ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete" onclick="return confirm('Are you sure you want to delete this blog post? any images uploaded associated with this blog will be deleted')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</a>
										<a href="<?php echo $this -> pages -> blog() ?>/edit/post/<?php echo $post['blogPostID']; ?>"><?php echo $post['blogPostName']; ?></a>
									</div>				
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class='col-md-12'>
									There are no blog posts entered
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

