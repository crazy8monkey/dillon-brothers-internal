<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> blog() ?>">Blog Posts</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Post
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage">
	<div class="container-fluid">
		<form method="post" action="<?php echo $this -> pages -> blog() ?>/save/post/<?php echo $this -> blogPost -> GetID() ?>" id="BlogPostSingleForm">
			<div class="row">
				<div class="col-md-9">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									Edit Post
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-info">
										<strong>Congratulations!</strong> You have started a blog post. Just a few more steps (Required items will have a Red Button)...
										<ol style='margin-left:20px; margin-top:15px; margin-bottom: 0px;'>
											<li>Add photos from the panel on the right.</li>
											<li>Set a Default/Primary Image for the post.</li>
											<li>Write content (make sure to include revelant links and images)</li>
											<li>Save and Preview.</li>
										</ol>									 
									</div>	
								</div>
							</div>						
							<?php if(!empty($this -> blogPost -> blogPostContent) && !empty($this -> blogPost -> currentBlogPhoto)): ?>
								<div class="row buttonContainer">
									<div class="col-md-12">								
										<?php if($this -> blogPost -> visible == 0) :?>
											<a href="<?php echo $this -> pages -> blog() ?>/save/publish/<?php echo $this -> blogPost -> GetID() ?>">
												<div class="whiteButton" style="float:left">
													Publish Blog To Website
												</div>
											</a>
	
										<?php else: ?>
											<a href="<?php echo $this -> pages -> blog() ?>/save/unpublish/<?php echo $this -> blogPost -> GetID() ?>">
												<div class="whiteButton" style="float:left">
													Unpublish Blog To Website
												</div>
											</a>
										<?php endif; ?>
									</div>
								</div>		
							<?php endif; ?>
							<div class="row">
								<div class="col-md-12">
									<?php 
										$editedTime = explode('T', $this ->  logs[count($this ->  logs) - 1]['edittedTimeAndDate']);
									?>
									Last Edited by <?php echo $this ->  logs[count($this ->  logs) - 1]['firstName'] . ' ' . $this ->  logs[count($this ->  logs) - 1]['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1]) ?> <a href="javascript:void(0);" onclick="BlogController.getEditHistory(<?php echo $this -> blogPost -> GetID() ?>)">View History</a>
									<div style='border-bottom: 1px solid #f7f7f7; margin-bottom:10px; padding-bottom:10px;'></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style='margin-bottom:10px;'>
									<div style="background:#f7f7f7; padding:10px ">
										<div style='float:left; margin-right:5px;'><strong>Blog Creator: </strong><?php echo $this -> blogPost -> createdByFullName ?> | </div>
										<?php echo $this -> form -> checkbox('Show Blog Creator on website', 'showBlogCreator', $this -> blogPost -> showUserNameOnPublish, 1, NULL, true) ?>
									</div>									
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Post Name", "blogPostName", 1, $this -> blogPost -> blogPostName) ?>
									</div>
								</div>			
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="descriptiveInfo">
										<?php 
										if($this -> blogPost -> CurrentOptionalURLSlug == NULL) {
											echo WEBSITE_PATH . "blog/post/". $this -> blogPost -> SeoURL;
										} else {
											echo WEBSITE_PATH . $this -> blogPost -> CurrentOptionalURLSlug;
										}
										?>
									</div>
									<div id="inputID2">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Optional URL Slug", "urlSlug", 2, $this -> blogPost -> CurrentOptionalURLSlug) ?>
									</div>
								</div>			
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> textarea("Content", "blogPostContent", 2, $this -> blogPost -> blogPostContent) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID2">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Keywords # value (optional)</div>
											<div class="descriptiveInfo">I.E. #harleyomaha, #motorsportsomaha, #indianmotorcyclesomaha</div>
											<textarea name="blogkeyWords"><?php echo $this -> blogPost -> KeyWords ?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit("Save Changes", "greenButton") ?>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-md-3">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="row">
								<div class="col-md-12">
									<div id="SavedPhotoResponse" style='display:none'></div>
									
									<div class="descriptiveInfo">Recommended 500px (width) x 500 (height) / 240k file size</div>
									
									<a href="javascript:void(0);" onclick='BlogController.UploadMainPhoto(<?php echo $this -> blogPost -> photoAlbumID ?>)'>
										<?php if($this -> blogPost -> CurrentBlogImage == NULL): ?>										
											<div class="redUploadButton">
												Upload Blog Main Photo
											</div>	
										<?php else: ?>
											<div class="greyUploadButton">
												Upload Blog Main Photo
											</div>
										<?php endif; ?>
									</a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php if(!empty($this -> blogPost -> currentBlogPhoto)): ?>
										<div style='margin-bottom:10px;'><strong>Current Blog Main Photo</strong> <?php if(count($this -> photos) > 1): ?><a href='javascript:void(0);' onclick="BlogController.GetPhotos(<?php echo $this -> blogPost -> photoAlbumID?>, <?php echo $this -> blogPost -> GetID() ?>)">Change Photo</a><?php endif; ?> </div>
										<img src='<?php echo PHOTO_URL . $this -> blogPost -> photoAlbumYearDirectory?>/blog/<?php echo $this -> blogPost -> photoAlbumName . '/' . $this -> blogPost -> CurrentBlogPhotoImageName . '-s.' .$this -> blogPost -> CurrentBlogPhotoImageExt ?>' width='100%' />
									<?php else: ?>
										<a href="javascript:void(0);" onclick='BlogController.UploadMainPhoto(<?php echo $this -> blogPost -> photoAlbumID ?>)'><div class="emptyPhotoPlacement" style='margin-bottom:0px;'>
											<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
											<div class="required">Photo Is Required to show on website</div>
											</div>
										</a>
									<?php endif; ?>	
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="input" style="border-top: 1px solid #dedede; border-bottom: 1px solid #dedede; margin-top: 10px; margin-bottom: 10px;">
										<a href="<?php echo $this -> pages -> photos() ?>/view/album/<?php echo $this -> blogPost -> photoAlbumID ?>" target='_blank'>
											<div class="DirectoryLine" style='border-bottom: 0px;'>
												<div class="icon">
													<img style="width: 20px;" src="<?php echo PATH ?>public/images/FolderIcon.png">
												</div>
												<div class="text" style="margin-left: 35px; font-size: 16px;">
													View photos in this blog
												</div>
												<div style="clear:both"></div>
											</div>						
										</a>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> checkboxes("How is this post categorized? (optional)", "blogcategory[]", $this -> BlogCategories, NULL, $this -> blogPost -> GetCategories()) ?>
								</div>			
							</div>
						</div>
					</div>
				</div>
					
					
			</div>
		
		</form>
	</div>
</div>


<div id="BlogEditHistory" style="display:none;">
	<?php echo $this -> PopupReadOnly('Blog Edit History', 'BlogController.closeEditHistory()')?>
</div>

<div id="PhotosInBlog" style="display:none;">
	<a href="javascript:void(0);" onclick="BlogController.closeBlogPhotoPopup()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">Choose Blog Photo			
			<a href="javascript:void(0);" onclick="BlogController.closeBlogPhotoPopup()">
				<div class="close" style="margin-top: 4px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>	
		</div>
		<div class="PhotoThumbContent FormContent" style="margin-right: 15px;  overflow-y: auto; max-height: 350px;">
			
		</div>
	</div>
</div>

