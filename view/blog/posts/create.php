<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> blog() ?>">Blog Posts</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New Post
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage">
	<div class="container-fluid">
		<form method="post" action="<?php echo $this -> pages -> blog() ?>/save/post" id="BlogPostSingleForm" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-8">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="row">
								<div class="col-md-12 sectionHeader" style='margin-bottom:10px;'>
									New Post
								</div>	
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-info">
										Please type in the name of the blog you want to create
										<ul style='margin-left:20px; margin-top:15px;'>
											<li>On the right side, you can assiociate different categories to this post</li>
										</ul>									 
									</div>	
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Post Name", "blogPostName", 1) ?>
									</div>
								</div>			
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit("Go To Step 2", "greenButton") ?>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-md-4">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> checkboxes("How is this post categorized? (optional)", "blogcategory[]", $this -> BlogCategories) ?>
								</div>			
							</div>
						</div>
					</div>
				</div>
			</div>		
		</form>
	</div>
</div>





