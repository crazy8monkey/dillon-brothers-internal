<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Blog
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> blog() ?>">
									<div class="whiteButton" style="text-align:center; width:100%">
										Blog Posts
									</div>	
								</a>
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<div class="whiteButton noButton" style="text-align:center; width:100%; border:1px solid white">
									Blog Categories
								</div>
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">										
								<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogApproveDeclineComments') == 1) : ?>
									<a href="<?php echo $this -> pages -> blog() ?>/comments">
										<div class="whiteButton" style="text-align:center; width:100%;">
											Comments
										</div>				
									</a>	
								<?php endif; ?>		
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Blog Categories
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> blog() ?>/create/category">
									<div class="greenButton" style="float:left">
										New Category
									</div>
								</a>
							</div>
						</div>	
						<?php if(count($this -> blogcategories) > 0): ?>
							<?php foreach($this -> blogcategories as $post): ?>
								<div class="row">
									<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
									<div class="col-md-12">
										<a href="<?php echo $this -> pages -> blog() ?>/delete/category/<?php echo $post['blogPostCategoryID']; ?>" onclick="return confirm('Are you sure you want to delete this blog category?')">
											<i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete"></i>
										</a>
										<a href="<?php echo $this -> pages -> blog() ?>/edit/category/<?php echo $post['blogPostCategoryID']; ?>"><?php echo $post['blogCategoryName']; ?></a>
									</div>				
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class='col-md-12'>
									There are no blog categories entered
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

