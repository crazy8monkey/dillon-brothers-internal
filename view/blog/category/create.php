<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> blog() ?>/categories">Blog Categories</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New Category
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage">
	<div class="container-fluid">
		<div class="WhiteSectionDiv">
			<div class="content">
				<div class="container">
					<div class="row">
						<div class="col-md-12 sectionHeader">
							New Category
						</div>
					</div>
					<form method="post" action="<?php echo $this -> pages -> blog() ?>/save/category" id="BlogCategorySingleForm">
						<div class="row">
							<div class="col-md-12">
								<div id="inputID1">
									<div class="errorMessage"></div>
									<?php echo $this -> form -> Input("text", "Category Name", "blogCategoryName", 1) ?>
								</div>
							</div>			
						</div>
						<div class="row">
							<div class="col-md-12">
								<?php echo $this -> form -> Submit("Save", "greenButton") ?>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



