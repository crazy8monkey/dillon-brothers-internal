<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo PATH ?>settings/type/inventory">Inventory Settings</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Spec Label
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Edit Specification Label
		</div>
	</div>
	<form method="post" action="<?php echo PATH ?>settings/save/label/<?php echo $this -> singlelabel -> GetID(); ?>" id="SpecLabelForm">
		<div class="row">
			<div class="col-md-6">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Spec Label", "labelText", 1, $this -> singlelabel -> labelText) ?>	
				</div>	
				
			</div>
			<div class="col-md-6">
				<div class="input">
					<div class="inputLabel">Group Specs</div>
					<select name="GroupID">
						<?php foreach($this  -> specGroups as $groupSingle): ?>
							<option value="<?php echo $groupSingle['specGroupID'] ?>" <?php if($groupSingle['specGroupID'] == $this -> singlelabel -> RelatedGroupID): ?> selected <?php endif; ?>><?php echo $groupSingle['specGroupText'] ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='margin-bottom:10px;'>
				<div id="inputID2">
					<div class="errorMessage"></div>
				</div>
				<?php $checkedCategories = explode(',', $this -> singlelabel -> LabelCategories); ?>

				<?php foreach($this -> inventorycategories as $categorySingle): ?>
					<div style="margin-bottom:10px;">
						<div style='font-weight: bold; margin-bottom: 3px;'>
							<?php echo $categorySingle['ParentCategory']?>	
						</div>
						
						<?php 
							$subCategoriesText = explode(",", $categorySingle['SubCategories']); 
							$subCategoryIDS = explode(",", $categorySingle['SubCategoryIDS']); 
						?>
						<?php foreach($subCategoriesText as $key => $singleCategory): ?> 
							<div style="margin-bottom:3px; margin-left: 10px;">
								<input type="checkbox" name="specLabelCategory[]" value="<?php echo $subCategoryIDS[$key] ?>" style='float:left; margin:3px 3px 0px 0px' <?php if(in_array($subCategoryIDS[$key], $checkedCategories)): ?>checked<?php endif; ?> /><?php echo $subCategoriesText[$key] ?>							
							</div>
							
						<?php endforeach; ?>
							
						
						
						
						
					</div>
				<?php endforeach; ?>	
				
				
				
			</div>			
		</div>


		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit("Save", "greenButton") ?>
			</div>
		</div>
	</form>
</div>

