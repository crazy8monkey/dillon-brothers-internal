<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Inventory Categories
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo PATH ?>settings/create/inventorycategory">
									<div class="greenButton" style="float:left">
										New Category
									</div>
								</a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">	
								<?php foreach($this -> categories as $categorySingle): ?>
									<div style="margin-bottom:10px;">
										<div style='font-size:16px;'>
											<?php echo $categorySingle['ParentCategory']?>	
										</div>
											
										<?php 
												
											
											$subCategoriesText = explode(",", $categorySingle['SubCategories']); 
											$subCategoryIDS = explode(",", $categorySingle['SubCategoryIDS']); 
										?>
										<?php foreach($subCategoriesText as $key => $singleCategory): ?> 
											<div>
												<a href="<?php echo PATH ?>settings/edit/inventorycategory/<?php echo $subCategoryIDS[$key] ?>">
													<?php echo $subCategoriesText[$key] ?>	
												</a>	
											</div>
												
										<?php endforeach; ?>
											
									</div>
								<?php endforeach; ?>	
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




