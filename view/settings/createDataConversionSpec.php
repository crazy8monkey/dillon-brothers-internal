<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo PATH ?>settings/type/dataconversions">Data Conversions</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create Conversion Rule
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			New Data Conversion
		</div>
	</div>
	<form method="post" action="<?php echo PATH ?>settings/save/conversion" id="DataConversionForm">
		<div class="row">
			<div class="col-md-12">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<div class="input">
						<div class="inputLabel">Conversion Type</div>
						<select name="ConversionType" id="ConversionType" onchange="SettingsController.ShowOtherOptions(this)">
							<option value="0">Please Select Type</option>
							<option value="1">Spec Label Mapping</option>
							<option value="2">Inventory Data Conversion: Color</option>
							<option value="3">Inventory Data Conversion: Manufacturer</option>
							<option value="4">Inventory Data Conversion: Category</option>
						</select>
					</div>
				</div>
				
			</div>
		</div>
		<div class="row" id="specLabels" style="display:none;">
			<div class="col-md-12">
				<div class="input">
					<div id="inputID2">
						<div class="errorMessage"></div>
						<div class="inputLabel">Spec Label</div>
						<select name="SpecLabelItem">
							<option value='0'>Select Spec Label</option>
							<?php foreach($this -> SpecList as $label) : ?>
								<option value='<?php echo $label['specLabelID'] ?>'><?php echo $label['labelText'] ?></option>
							<?php endforeach; ?>
						</select>					
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit("Save", "greenButton") ?>
			</div>
		</div>
		
	
	
		
		
	</form>
</div>


