
<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>settings/type/stores">Store Settings</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Store Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="WhiteSectionDiv">
			<div class="content">
				<div class="row">
					<div class="col-md-12 sectionHeader">
						<?php echo $this -> storeDetails -> storeName ?>
					</div>
				</div>
				
				<div class="StoreTabs">
					<a href='javascript:void(0);' onclick="SettingsController.OpenStoreEmailSettings()">
						<div class="tab" id="emailTab">
							Email Notifications
							<div class="selectedTab" style='display:block;'></div>
						</div>	
					</a>
					<a href='javascript:void(0);' onclick='SettingsController.OpenStoreWaterMarkSettings()'>
						<div class="tab" id="waterMarkTab">
							Inventory Water Mark
							<div class="selectedTab"></div>
						</div>	
					</a>
					
					<div style='clear:both'></div>
				</div>
				
				<div id="EmailNotificationSettingsTab">
					<div class="row">
						<div class="col-md-12">
							<div class="descriptiveInfo">Enter multiple emails separated with a comma.</div>
						</div>
					</div>	
	
					<form method="post" action="<?php echo PATH ?>settings/save/storeNotifications/<?php echo $this -> storeDetails -> GetID() ?>" id="EmailNotificationSettings">
						<div class="row">
							<div class="col-md-6">
								<div id="inputID5">
									<div class="errorMessage"></div>
									<div class="input">
										<div class="inputLabel">Send To A Friend Notifications (BCC)</div>
										<textarea name="sendToFriendEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(5)"><?php echo $this -> storeDetails -> SendToFriendBcc ?></textarea>																		
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div id="inputID1">
									<div class="errorMessage"></div>
									<div class="input">
										<div class="inputLabel">Contact Us Notifications</div>
										<textarea name="contactUsEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(1)"><?php echo $this -> storeDetails -> ContactUsEmail ?></textarea>																		
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div id="inputID4">
									<div class="errorMessage"></div>
									<div class="input">
										<div class="inputLabel">Online Offer Notifications</div>
										<textarea name="OnlineOfferEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(3)"><?php echo $this -> storeDetails -> OnlineOffer ?></textarea>																		
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div id="inputID2">
									<div class="errorMessage"></div>
									<div class="input">
										<div class="inputLabel">Schedule Test Ride Notifications</div>
										<textarea name="ScheduleTestRideEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(2)"><?php echo $this -> storeDetails -> ScheduleTestRide ?></textarea>																		
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							
							<div class="col-md-6">
								<div id="inputID4">
									<div class="errorMessage"></div>
									<div class="input">
										<div class="inputLabel">Trade Value Notifications</div>
										<textarea name="TradeValueEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(4)"><?php echo $this -> storeDetails -> TradeValue ?></textarea>																		
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<input type="submit" value="Save Email Notification Settings" class="greenButton" />
							</div>
						</div>	
					</form>
					
				</div>
				<div id="WaterMarkSettingsTab" style='display:none;'>
					<form method="post" action="<?php echo PATH ?>settings/save/storeWaterMark/<?php echo $this -> storeDetails -> GetID() ?>" id="WaterMarkForm">
						<div class="row">
							
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="input">
									<input type="submit" value="Save" class="greenButton">
								</div>			
							</div>
						</div>
					</form>
				</div>
				
				
				
			</div>
			
		</div>
	</div>
</div>



