<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo PATH ?>settings/type/inventory">Inventory Settings</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Spec Group
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Edit Label Group
		</div>
	</div>
	<form method="post" action="<?php echo PATH ?>settings/save/specgroup/<?php echo $this -> group -> GetID(); ?>" id="SpecLabelForm">
		<div class="row">
			<div class="col-md-12" style='margin-bottom:20px;'>
				<?php echo $this -> form -> checkbox('Spec Group Visible', 'groupVisible', $this -> group -> Visible, 1, NULL, true) ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Group Text", "groupText", 1, $this -> group -> SpecGroupText) ?>	
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='margin-bottom:10px;' id="SpecLabelList">
				<?php foreach($this -> labels as $labelSingle): ?>
					
					<div class="LabelLine">
						<input type="hidden" value="<?php echo $labelSingle['specLabelID'] ?>" name='labelID[]' />
						<div style="float:left; padding: 5px;">
							<i class="fa fa-sort" aria-hidden="true"></i>	
						</div>
						<div style="padding:5px 20px;">
							<?php echo $labelSingle['labelText'] ?>
						</div>
						<div style="clear:both"></div>		
					</div>
				<?php endforeach; ?>
				
				
			</div>			
		</div>


		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit("Save", "greenButton") ?>
			</div>
		</div>
	</form>
</div>

