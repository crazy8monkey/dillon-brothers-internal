<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Social Media
							</div>
						</div>
						<form method="post" action="<?php echo PATH ?>settings/save/socialmedia" id="SocialMediaForm">
							<div class="row">
								<div class="col-md-6">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Facebook", "settingsFacebook", 1, $this -> socialMediaSettings -> FacebookList) ?>
									</div>
								</div>
								<div class="col-md-6">
									<div id="inputID2">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Twitter", "settingsTwitter", 2, $this -> socialMediaSettings -> TwitterList) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID3">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Email Summary", "settingsSocialMediaEmail", 3, $this -> socialMediaSettings -> EmailSummary) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID4">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Urgent Social Media Notificaitons", "settingsUrgentPosts", 4, $this -> socialMediaSettings -> UrgentEmail) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit('Save', 'greenButton') ?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container" style="padding-bottom: 120px;">
	
	
</div>


