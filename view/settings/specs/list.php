<?php 

	
function search($array, $key, $value) {
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, search($subarray, $key, $value));
        }
    }

    return $results;
}
	

?>

<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Specifications
							</div>
						</div>
						<form method="post" action="<?php echo PATH ?>settings/save/inventory" id="InventorySettingsForm">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Inventory Spec Notifications", "settingsInventory", 1, $this -> inventorySettings -> InventorySpecNotifications) ?>
									</div>
								</div>
							</div>
								
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit('Save Email Settings', 'greenButton') ?>
								</div>
							</div>
						</form>	
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo PATH ?>settings/create/specgroup">
									<div class="whiteButton" style="float:left; margin-right:5px;">
										Add Group
									</div>				
								</a>
								<a href="<?php echo PATH ?>settings/create/speclabel">
									<div class="whiteButton" style="float:left">
										Add Label
									</div>				
								</a>
							</div>
						</div>
						<style type='text/css'>
							div.tooltip {word-wrap: break-word;}
						</style>
						<div style="height: 20px;"><div id="SpecificationGroupResponses" style='color: #1da906; font-weight:bold; text-align: center;'></div></div>
						
						<div id="SpecSettingsContainer">
							<?php foreach ($this -> specGroups as $groupSingle): ?>
								<div class="specGroupContainer" id="<?php echo $groupSingle['specGroupID'] ?>">
									<div class="GroupHeaderElement">
										<div class="DragItem">
											<i class="fa fa-sort" aria-hidden="true"></i>		
										</div>
										<div class="text">
											<?php echo $groupSingle['specGroupText'] ?>
										</div>
										<div class="edit">
											<a href="<?php echo PATH ?>settings/edit/specgroup/<?php echo $groupSingle['specGroupID'] ?>">
												<i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
										</div>
										<div class="delete">
											<a href="<?php echo PATH ?>settings/delete/specgroup/<?php echo $groupSingle['specGroupID'] ?>" onclick="return confirm('Are you sure you want to delete?')">
												<i class="fa fa-trash" aria-hidden="true"></i>
											</a>
										</div>
										<div class="divider"></div>
										<div style="clear:both"></div>
									</div>
									<div class="labels">
										<div class='labelListHeader'>
											<a href='javascript:void(0);' onclick='SettingsController.ExpandGroupLabels(this)'><div class='labelExpand'>+</div>Labels</a>
										</div>
										<div class="LabelContentList">
											<?php 
												$groupID = $groupSingle['specGroupID'];
														
												$filteredLabels = array_filter($this -> specLabels, function ($var) use($groupID) {
													return $var['relatedSpecGroupID'] == $groupID; 
												});
											?>
													
											<?php foreach($filteredLabels as $labelSingle): ?>
												<?php $categoryIDS = explode(',', $labelSingle['LinkedInventoryCategories']); ?>
												
												<?php 
													$categoryIDS = explode(',', $labelSingle['LinkedInventoryCategories']);
													
														
													$filteredCategories = array_filter($this -> inventorycategories, function ($var) use($categoryIDS) {
														$finalResult = array();	
														foreach($categoryIDS as $categoryIDSingle) {
															if($var['inventoryCategoryID'] == $categoryIDSingle) {
																array_push($finalResult, $var['inventoryCategoryName']);
															}
														}				
														return $finalResult; 
													});
												?>
												
												<div class="labelLine">
													<div style="clear:both">
														<a href="<?php echo PATH ?>settings/edit/label/<?php echo $labelSingle['specLabelID'] ?>">
															<div class="editLabel">
																<i class="fa fa-pencil" aria-hidden="true"></i>
															</div>												
														</a>
							
														<div class="labelText">
															<?php echo $labelSingle['labelText'] ?>	
														</div>
																		
														<div class="DeleteLabel">
															<a href="<?php echo PATH ?>settings/delete/speclabel/<?php echo $labelSingle['specLabelID'] ?>" onclick="return confirm('Are you sure you want to delete?')">
																<i class="fa fa-trash" aria-hidden="true"></i>
															</a>
														</div>
														<?php if(count($filteredCategories) > 0): ?>
															<?php 
															$categories = '';
															foreach($filteredCategories as $categorySingle) {
																$categories .= $categorySingle['inventoryCategoryName'] . ', ';
															} ?>
															<div style="float: left; margin-top: 4px; margin-left: 8px; color: #cecece;">
																<i class="fa fa-info-circle" aria-hidden="true" data-placement="right" data-toggle="tooltip" title="<?php echo rtrim($categories, ', ')?>"></i>	
															</div>
														<?php endif; ?>
																		
														<div style="clear:both"></div>												
													</div>
												</div>
											<?php endforeach; ?>
										</div>
									</div>		
								</div>		
							<?php endforeach; ?>							
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




