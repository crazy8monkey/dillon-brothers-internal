<div class="container">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo PATH ?>settings/type/inventory">Inventory Settings</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Color
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Edit Color
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="inputID1">
				<div class="errorMessage"></div>
			</div>
		</div>
	</div>
	
	
	
	<form method="post" action="<?php echo PATH ?>settings/save/color/<?php echo $this -> color -> GetID()?>" id="InventoryCategoryForm">
		<div class="row">
			<div class="col-md-6">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Color Text", "ColorText", 1, $this ->  color -> ColorText) ?>
				</div>
			</div>
			<div class="col-md-6">
				<div id="inputID2">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Filter Color", "filterColorText", 2, $this -> color -> FilterText) ?>
				</div>
			</div>
		</div>
	
				

				
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit("Save", "greenButton") ?>
			</div>
		</div>
	</form>
	
	

	
	
</div>
