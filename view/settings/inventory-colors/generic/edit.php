<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>settings/inventorycolors">Settings: Inventory Colors</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Generic Color
				</div>
			</div>	
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style="max-width: 1170px; margin: 0 auto;">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									Edit Generic Color
								</div>
							</div>
							
							
							<form method="post" action="<?php echo PATH ?>settings/save/genericcolor/<?php echo $this -> color -> GetID() ?>" id="GenericColorSingleForm">
								<div class="row">
									<div class="col-md-12">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Generic Color Text", "genericColorText", 1, $this -> color -> ColorText) ?>
										</div>
									</div>
								</div>
							
										
						
										
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> Submit("Save", "greenButton") ?>
									</div>
								</div>
							</form>
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	
	
	

	
	
</div>
