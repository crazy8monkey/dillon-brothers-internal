<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12">
										<div class="subHeader">
											Colors
										</div>
									</div>	
								</div>
								<div class="row buttonContainer">
									<div class="col-md-12">
										<a href="<?php echo PATH ?>settings/create/color">
											<div class="greenButton" style="float:left">
												Add Color
											</div>
										</a>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">	
										<?php foreach($this -> colors as $colorSingle): ?>
											<div style="margin-bottom:10px;">
												<div style='font-size:16px;'>
													<?php echo $colorSingle['FilterColor']?>	
												</div>
											
												<?php 
													$subColorText = explode(",", $colorSingle['ColorTexts']); 
													$subColorIDS = explode(",", $colorSingle['ColorIDS']); 
												?>
												<?php foreach($subColorText as $key => $singleColor): ?> 
													<div>
														<a href="<?php echo PATH ?>settings/delete/color/<?php echo $subColorIDS[$key] ?>" onclick="return confirm('Are you sure you want to delete this Color? this action cannot be undone')">
															<i class="fa fa-trash-o" aria-hidden="true"></i>
														</a>
														<a href="<?php echo PATH ?>settings/edit/color/<?php echo $subColorIDS[$key] ?>">
															<?php echo $subColorText[$key] ?>	
														</a>	
													</div>			
												<?php endforeach; ?>
														
											</div>
										<?php endforeach; ?>	
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12">
										<div class="subHeader">
											Generic Colors
										</div>
									</div>	
								</div>
								<div class="row buttonContainer">
									<div class="col-md-12">
										<a href="<?php echo PATH ?>settings/create/genericcolor">
											<div class="greenButton" style="float:left">
												Add Generic Color
											</div>
										</a>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<?php foreach($this -> genericColors as $genericSingle): ?>
											<div>
												<a href="<?php echo PATH ?>settings/delete/genericcolor/<?php echo $genericSingle['genericColorID'] ?>" onclick="return confirm('Are you sure you want to delete this Generic Color? this action will remove all colors related to this filtered color')">
													<i class="fa fa-trash-o" aria-hidden="true"></i>
												</a>
												<a href="<?php echo PATH ?>settings/edit/genericcolor/<?php echo $genericSingle['genericColorID'] ?>">
													<?php echo $genericSingle['GenericColorText'] ?>	
												</a>	
											</div>
										<?php endforeach; ?>	
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




