<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>settings/inventorycolors">Settings: Inventory Colors</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Color
				</div>
			</div>	
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style="max-width: 1170px; margin: 0 auto;">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									Create Color
								</div>
							</div>
							
							
							<form method="post" action="<?php echo PATH ?>settings/save/color" id="ColorSingleForm">
								<div class="row">
									<div class="col-md-6">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Descriptive Color Text", "ColorText", 1) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div id="inputID2">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Generic Color Text</div>
												<select name="filterColorText" onchange="Globals.removeValidationMessage(2)">
													<option value = "">Please Select Generic Color</option>
													<?php foreach($this -> genericColors as $genericSingle): ?>
														<option value='<?php echo $genericSingle['GenericColorText'] ?>'><?php echo $genericSingle['GenericColorText'] ?></option>
													<?php endforeach; ?>	
												</select>
											</div>
										</div>
									</div>
								</div>
							
										
						
										
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> Submit("Save", "greenButton") ?>
									</div>
								</div>
							</form>
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	
	
	

	
	
</div>
