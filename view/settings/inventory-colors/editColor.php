<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>settings/inventorycolors">Settings: Inventory Colors</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Color
				</div>
			</div>	
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style="max-width: 1170px; margin: 0 auto;">
							
							<div class="row">
								<div class="col-md-12 sectionHeader">
									Edit Color
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
									</div>
								</div>
							</div>
							
							
							
							<form method="post" action="<?php echo PATH ?>settings/save/color/<?php echo $this -> color -> GetID()?>" id="ColorSingleForm">
								<div class="row">
									<div class="col-md-6">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Descriptive Color Text", "ColorText", 1, $this ->  color -> ColorText) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div id="inputID2">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Generic Color Text</div>
												<select name="filterColorText" onchange="Globals.removeValidationMessage(2)">
													<option value = "">Please Select Generic Color</option>
													<?php foreach($this -> genericColors as $genericSingle): ?>
														<option <?php if($this -> color -> FilterText == $genericSingle['GenericColorText']): ?>selected <?php endif; ?>value='<?php echo $genericSingle['GenericColorText'] ?>'><?php echo $genericSingle['GenericColorText'] ?></option>
													<?php endforeach; ?>	
												</select>
											</div>
										</div>
									</div>
								</div>
							
										
						
										
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> Submit("Save", "greenButton") ?>
									</div>
								</div>
							</form>
							
							<div class="row">
								<div class="col-md-12">
									<div class="subHeader">
										Color Conversions
									</div>
								</div>
							</div>
							
							<div id="inputID99">
								<div class="errorMessage"></div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="descriptiveInfo">These are looking for key phrases to find and convert to this particular saved color</div>
								</div>
							</div>
							<?php if(count($this -> CurrentConversions) > 0): ?>
								<div style='margin-bottom:10px;'>
									<div class="row">
										<div class="col-md-12" style='font-size:16px'>
											Current Conversions for this color
										</div>
									</div>
									<?php foreach($this -> CurrentConversions as $conversionSingle): ?>
										<div class="row">
											<div class="col-md-12">
												<strong><?php echo $conversionSingle['keyPhrase'] ?></strong>
											</div>
										</div>
										<div style='border-bottom:1px solid #ececec; margin: 5px 0px;'></div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	
	
	

	
	
</div>
