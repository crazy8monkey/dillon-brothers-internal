<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<?php foreach($this -> storesSettings as $key => $storeSettingSingle) : ?>
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="row">
								<div class="col-md-12">
									<div class="descriptiveInfo">Enter multiple emails separated with a comma.</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 subHeader">
									<?php echo $storeSettingSingle['StoreName'] ?>
								</div>
							</div>
							<form method="post" action="<?php echo PATH ?>settings/save/storeNotifications/<?php echo $storeSettingSingle['storeID'] ?>" class="EmailNotificationSettings" id="<?php echo $key ?>">
								<div class="row">
									<div class="col-md-6">
										<div class="inputID5">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Send To A Friend Notifications (BCC)</div>
												<textarea name="sendToFriendEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(5)"><?php echo $storeSettingSingle['SendToFriendBcc'] ?></textarea>																		
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="inputID1">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Contact Us Notifications</div>
												<textarea name="contactUsEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(1)"><?php echo $storeSettingSingle['contactUsEmail'] ?></textarea>																		
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="inputID3">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Online Offer Notifications</div>
												<textarea name="OnlineOfferEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(3)"><?php echo $storeSettingSingle['OnlineOfferEmail'] ?></textarea>																		
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="inputID2">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Schedule Test Ride Notifications</div>
												<textarea name="ScheduleTestRideEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(2)"><?php echo $storeSettingSingle['ScheduleTestRideEmail'] ?></textarea>																		
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="inputID4">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Trade Value Notifications</div>
												<textarea name="TradeValueEmail" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(4)"><?php echo $storeSettingSingle['TradeValueEmail'] ?></textarea>																		
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="inputID6">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Service Dept. Notificaitons</div>
												<textarea name="ServiceDeptNotifications" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(6)"><?php echo $storeSettingSingle['ServiceEmailNotification'] ?></textarea>																		
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="inputID7">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Parts Dept. Notifications</div>
												<textarea name="PartsDeptNotifications" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(7)"><?php echo $storeSettingSingle['PartsEmailNotification'] ?></textarea>																		
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="inputID8">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Apparel Dept. Notifications</div>
												<textarea name="ApparelDeptNotifications" class="emailNotificationTextBox" onchange="Globals.removeValidationMessage(8)"><?php echo $storeSettingSingle['ApparelEmailNotification'] ?></textarea>																		
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div id="inputID6">
											<div><strong>Current Store Water Mark</strong></div>
											<div style="border: 1px dashed;padding: 10px;text-align: center;">
						    					<img style='width: 180px;' src="<?php echo PHOTO_URL ?>WaterMark/<?php echo $storeSettingSingle['WaterMarkImage'] ?>">
						    				</div>
											<div class="input" style="border: 1px dashed; padding: 10px;">
												<div class="errorMessage"></div>
												<div class="inputLabel">Upload New Water Mark</div>
												<input type="file" name="storeWaterMarkPhoto">
											</div>
										</div>	
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<input type="submit" value="Save Email Notification Settings" class="greenButton" />
									</div>
								</div>	
							</form>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
