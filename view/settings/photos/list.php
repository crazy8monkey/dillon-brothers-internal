<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Application Settings
							</div>
						</div>
						
						<form method="post" action="<?php echo PATH ?>settings/save/applicationsettings" id="ApplicationForm">
							<div class="row">
								<div class="col-md-6">
									<div id="inputID1">
										
										<div><strong>Current Main Water Mark (photots)</strong></div>
										<div style="border: 1px dashed;padding: 10px;text-align: center;">
					    					<img src="<?php echo PHOTO_URL . 'WaterMark/' . $this -> settings -> WaterMarkImage ?>" />
					    				</div>
										<div class="input" style="border: 1px dashed; padding: 10px;">
											<div class="errorMessage"></div>
											<div class="inputLabel">Upload New Water Mark</div>
											<input type="file" name="mainWaterMarkPhoto">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit('Save', 'greenButton') ?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


