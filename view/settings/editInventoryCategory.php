<div class="container">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo PATH ?>settings/type/inventory">Inventory Settings</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Inventory Category
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Edit Inventory Category
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="inputID1">
				<div class="errorMessage"></div>
			</div>
		</div>
	</div>
	
	
	
	<form method="post" action="<?php echo PATH ?>settings/save/inventorycategory/<?php echo $this -> categorySingle -> GetID()?>" id="InventoryCategoryForm">
		<div class="row">
			<div class="col-md-6">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Category Text", "InventoryCategoryText", 1, $this -> categorySingle -> CategoryText) ?>
				</div>
			</div>
			<div class="col-md-6">
				<div id="inputID2">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Parent Category", "InventoryParentCategoryText", 2, $this -> categorySingle -> ParentCategoryText) ?>
				</div>
			</div>
		</div>

				
				<div class="row">
					<div class="col-md-12">
						<?php echo $this -> form -> Submit("Select", "greenButton") ?>
					</div>
				</div>
			</form>
	
	

	
	
</div>
