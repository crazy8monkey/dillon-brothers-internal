<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Application Settings
		</div>
	</div>
	
	<form method="post" action="<?php echo PATH ?>settings/save/applicationsettings" id="ApplicationForm">
		<div class="row">
			<div class="col-md-6">
				<div id="inputID1">
					
					<div><strong>Current Main Water Mark (photots)</strong></div>
					<div style="border: 1px dashed;padding: 10px;text-align: center;">
    					<img src="<?php echo PHOTO_URL . 'WaterMark/' . $this -> settings -> WaterMarkImage ?>" />
    				</div>
					<div class="input" style="border: 1px dashed; padding: 10px;">
						<div class="errorMessage"></div>
						<div class="inputLabel">Upload New Water Mark</div>
						<input type="file" name="mainWaterMarkPhoto">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit('Save', 'greenButton') ?>
			</div>
		</div>
	</form>
</div>


