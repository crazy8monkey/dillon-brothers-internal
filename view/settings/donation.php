<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Donation Submission Settings
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="descriptiveInfo">Enter multiple emails separated with a comma.</div>
							</div>
						</div>
						<form id="DonationSubmission" method="post" action="<?php echo PATH ?>settings/save/donation">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>	
										<div class="input">
											<div class="inputLabel">Donations Email Notifications</div>
											<textarea name='DonationEmail' class="emailNotificationTextBox"><?php echo $this -> settings -> DonationEmail?></textarea>																		
										</div>				
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type='submit' value='Save' class='greenButton' />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



