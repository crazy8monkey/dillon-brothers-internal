<div class="content" style='padding:0px;'>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsUsers') == 1) : ?>
		<a href="<?php echo PATH ?>settings/users"><div class='settingsLink<?php if(isset($this -> usersSelected)):  ?> selected<?php endif; ?>'>Users</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsDonations') == 1) : ?>
		<a href="<?php echo PATH ?>settings/donation"><div class='settingsLink <?php if(isset($this -> donationSettingsSelected)): ?> selected<?php endif; ?>'>Donation Settings</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsSocialMedia') == 1) : ?>
		<a href="<?php echo PATH ?>settings/socialmedia"><div class='settingsLink<?php if(isset($this -> socialMediaSelected)):  ?> selected<?php endif; ?>'>Social Media</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsSpecs') == 1) : ?>
		<a href="<?php echo PATH ?>settings/specifications"><div class='settingsLink<?php if(isset($this -> specSelected)):  ?> selected<?php endif; ?>'>Spec Settings</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsInventoryCateories') == 1) : ?>
		<a href="<?php echo PATH ?>settings/inventorycategories"><div class='settingsLink<?php if(isset($this -> inventoryCategoriesSelected)):  ?> selected<?php endif; ?>'>Inventory Categories</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsInventoryColors') == 1) : ?>
		<a href="<?php echo PATH ?>settings/inventorycolors"><div class='settingsLink<?php if(isset($this -> inventoryColorsSelected)):  ?> selected<?php endif; ?>'>Inventory Colors</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsStoreSettings') == 1) : ?>
		<a href="<?php echo PATH ?>settings/stores"><div class='settingsLink<?php if(isset($this -> storesSelected)):  ?> selected<?php endif; ?>'>Store Settings</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsPhotoSettings') == 1) : ?>
		<a href="<?php echo PATH ?>settings/photos"><div class='settingsLink<?php if(isset($this -> photosSelected)):  ?> selected<?php endif; ?>'>Photo Settings</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsConversionsManufacture') == 1) : ?>
		<a href="<?php echo PATH ?>settings/manufactureconversions"><div class='settingsLink<?php if(isset($this -> manufactureConversionSelected)):  ?> selected<?php endif; ?>'>Data Conversions: Manufacture</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsSpecLabelMapping') == 1) : ?>
		<a href="<?php echo PATH ?>settings/specmapping"><div class='settingsLink <?php if(isset($this -> specMappingSelected)):  ?> selected<?php endif; ?>'>Spec Label Maping</div></a>
	<?php endif; ?>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsShortUrls') == 1) : ?>
		<a href="<?php echo PATH ?>settings/redirects"><div class='settingsLink<?php if(isset($this -> shortUrlSettingsSelected)):  ?> selected<?php endif; ?>'>Redirects</div></a>
	<?php endif; ?>
	<a href="<?php echo PATH ?>settings/sms"><div class='settingsLink<?php if(isset($this -> smsSettingsSelected)):  ?> selected<?php endif; ?>'>SMS</div></a>
	<a href="<?php echo PATH ?>settings/chat"><div class='settingsLink<?php if(isset($this -> chatSettingsSelected)):  ?> selected<?php endif; ?>'>Chat</div></a>
</div>