<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Data Conversions
		</div>
	</div>
	<div class="row buttonContainer">
		<div class="col-md-12">
			<a href="<?php echo PATH ?>settings/create/dataconversion">
				<div class="greenButton" style="float:left">
					Add New Rule
				</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<a href="<?php echo PATH ?>settings/dataconversions/spec">
				<div class="GrayBox">
					Spec Label Mapping
				</div>	
			</a>
			
		</div>
		<div class="col-md-3">
			<div class="GrayBox">
				<a href="<?php echo PATH ?>settings/manufactureconversions">Inventory Manufacturer</a>
			</div>
		</div>
		<div class="col-md-3">
			<div class="GrayBox">
				<?php foreach($this -> colorconversions as $conversion): ?>
					<a href="<?php echo PATH ?>settings/colorconversion/<?php echo $conversion['dataConversionID'] ?>">Colors</a>
				<?php endforeach; ?> 
			</div>
		</div>
		<div class="col-md-3">
			<div class="GrayBox">
				Inventory Category
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12"></div>
	</div>
	
	
</div>


