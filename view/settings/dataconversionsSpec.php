<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Data Conversions
		</div>
	</div>
	<div class="row buttonContainer">
		<div class="col-md-12">
			<a href="<?php echo PATH ?>settings/create/dataconversion">
				<div class="greenButton" style="float:left">
					Add New Rule
				</div>
			</a>
		</div>
	</div>
	<?php foreach(array_chunk($this -> SpecMappingRules, 3, true) as $groupSet): ?>
		<div class="row">
			<?php foreach($groupSet as $groupSingle) : ?>
				<div class="col-md-4" style="margin-bottom:20px;">
					<div class="subHeader"><?php echo $groupSingle['specGroupText'] ?></div>
					<?php 
					
						$associatedLabels = array();
						$specLabelsText = explode(",", $groupSingle['RelatedSpecLabels']);
						$specLabelsIDS  = explode(",", $groupSingle['RelatedDataConversionIDs']);
						
						//echo "<pre>";
						//print_r($specLabelsText);
						//echo "</pre>";
						
						//echo "<pre>";
						//print_r($specLabelsIDS);
						//echo "</pre>";
						
						
						
						
						if(count($specLabelsText) > 0) {
							foreach($specLabelsText as $key => $value) {
								array_push($associatedLabels, array("Text" => $specLabelsText[$key],
																	"ID" => $specLabelsIDS[$key]));	
								
							}
						}
						//echo "<pre>";
						//print_r($associatedLabels);
						//echo "</pre>";
						 
					?>
					
					<?php if(count($associatedLabels) > 0): ?>
						<?php foreach($associatedLabels as $label): ?>
							<div class="row">
								<div class="col-md-12">
									<a href="<?php echo PATH ?>settings/edit/dataconversion/<?php echo $label['ID'] ?>">
										<?php echo $label['Text'] ?>	
									</a>
									
								</div>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
					
				</div>
			<?php endforeach; ?>
		</div>
	<?php endforeach; ?>
	
</div>


