<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>settings/colorconversions">Settings: Color Conversions</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create Color Conversion
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style="max-width: 1170px; margin: 0 auto;">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									Convert Color
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-md-12">
									<form method="post" action="<?php echo PATH ?>settings/save/conversioncolor" id="ConversionColorSelect">
										<div class="input" style='margin-top:15px;'>
											<div class="inputLabel">Current Color Text</div>
											<input type="Text" name="CurrentText" />
										</div>		
										<input type="hidden" value="<?php echo $this -> dataconversionID ?>" name="DataConversionID" />
										
										<div id="CurrentColorsList">
											<?php foreach ($this -> colors as $color): ?>
												<div class="ColorLineOption">
													<div style="float:left;">
														<input type="checkbox" name="colorID[]" value='<?php echo $color['colorID'] ?>' />
													</div>
													<div style="margin-left:15px;">
														<?php echo $color['ColorText'] ?>
													</div>
													<div style="clear:both"></div>
												</div>
											<?php endforeach; ?>	
										</div>
										
										
										
										<div class="row">
											<div class="col-md-12" style='margin-top:10px;'>
												<?php echo $this -> form -> Submit("Select", "greenButton") ?>
											</div>
										</div>
									</form>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="input" style='margin-top:15px;'>
										<div class="inputLabel">Enter New Color</div>
										<input type="text" name="New Color" onkeyup="SettingsController.NewColor(this)" />
									</div>
								
								</div>
							</div>
							
												
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


	<div id="NewColorElement" class="ColorLineOption" style="clear:both; display:none;">
		<div style="float:left;">
			<input type="checkbox" name="colorID[]" />
		</div>
		<div style="margin-left:15px;" class="newColorText"></div>
		<div style="clear:both"></div>
	</div>
	
	
	
