<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Manufacture Conversions
							</div>
						</div>
						<form method="post" action="<?php echo PATH ?>settings/save/conversion/<?php echo $this -> dataconversion -> GetID(); ?>" id="DataConversionForm">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID2"><div class="errorMessage"></div></div>	
									<div id="inputID3"><div class="errorMessage"></div></div>	
								</div>
							</div>
							
							<div id="ConversionRules">
								<?php foreach($this -> rules as $ruleSingle) : ?>
					
					
									<?php if($this -> dataconversion -> CurrentConversionType == 3): ?>
										<div class="row">
											<div class="col-md-6">
												<input type="hidden" name="ConversionRuleID[]" value="<?php echo $ruleSingle['ruleID'] ?>" />
												<div class="input">
													<div class="inputLabel">Current Text</div>
													<input type="text" name="CurrentManufactureText[]" placeholder="i.e. KAWASA" value="<?php echo $ruleSingle['CurrentManufactureText'] ?>" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="input">
													<div class="inputLabel">Switch To</div>
													<input type="text" name="NewManufactureText[]" placeholder="i.e. Kawasaki" value="<?php echo $ruleSingle['NewManufacturerText'] ?>" />
												</div>
											</div>
										</div>
									<?php endif; ?>	
								<?php endforeach; ?>
							</div>
							
							<?php if($this -> dataconversion -> CurrentConversionType == 4): ?>
							<?php endif; ?>	
							
							<div class="row">
								<div class="col-md-12">
									<a href="javascript:void(0);" onclick="SettingsController.AddConversionRule(<?php echo $this -> dataconversion -> CurrentConversionType ?>, <?php echo $this -> dataconversion -> GetID() ?>)">
										<div class="AddSpec">Add Conversion Rules</div>	
									</a>
								</div>
							</div>
							
							
							
							
							<?php if($this -> dataconversion -> CurrentConversionType == 1 || $this -> dataconversion -> CurrentConversionType == 3): ?>				
								<div class="row" style='margin-top:10px;'>
									<div class="col-md-12">
										<?php echo $this -> form -> Submit("Save", "greenButton") ?>
									</div>
								</div>
							<?php endif; ?>
						
						
							
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row" id="NewConversionRuleElementInventoryManufacture" style='display:none;'>
	<div class="col-md-6">
		<input type="hidden" name="ConversionRuleID[]" value="0" />
		<div class="input">
			<div class="inputLabel">Current Text</div>
			<input type="text" name="CurrentManufactureText[]" placeholder="i.e. KAWASA" />
		</div>
	</div>
	<div class="col-md-6">
		<div class="input">
			<div class="inputLabel">Switch To</div>
			<input type="text" name="NewManufactureText[]" placeholder="i.e. Kawasaki" />
		</div>
	</div>
</div>
