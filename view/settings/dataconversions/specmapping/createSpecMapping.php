<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>settings/specmapping">Settings: Spec Mapping</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create Spec Mapping
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style="max-width: 1170px; margin: 0 auto;">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									New Spec Mapping Rule
								</div>
							</div>
							<form method="post" action="<?php echo PATH ?>settings/save/conversion" id="DataConversionForm">
								<div class="row">
									<div class="col-md-12">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Conversion Type</div>
												<select name="ConversionType" id="ConversionType" onchange="SettingsController.ShowOtherOptions(this)">
													<option value="0">Please Select Type</option>
													<option value="1">Spec Label Mapping</option>
												</select>
											</div>
										</div>
										
									</div>
								</div>
								<div class="row" id="specLabels" style="display:none;">
									<div class="col-md-12">
										<div class="input">
											<div id="inputID2">
												<div class="errorMessage"></div>
												<div class="inputLabel">Spec Label</div>
												<select name="SpecLabelItem">
													<option value='0'>Select Spec Label</option>
													<?php foreach($this -> SpecList as $label) : ?>
														<option value='<?php echo $label['specLabelID'] ?>'><?php echo $label['specGroupText'] . ': ' . $label['labelText'] ?></option>
													<?php endforeach; ?>
												</select>					
											</div>
											
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> Submit("Save", "greenButton") ?>
									</div>
								</div>
								
							
							
								
								
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container" style="padding-bottom: 120px;">
	
	
</div>


