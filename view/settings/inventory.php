<?php 

	
function search($array, $key, $value) {
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, search($subarray, $key, $value));
        }
    }

    return $results;
}
	

?>

<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Inventory Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="WhiteSectionDiv">
			<div class="content">
				<div class="InventoryTabs">
					<a href="javascript:void(0);" onclick="SettingsController.OpenSpecTab()">
						<div class="tab" id="specTab">
							Specifications
							<div class="selectedTab" style='display:block;'></div>
						</div>	
					</a>
					
					<a href="javascript:void(0);" onclick="SettingsController.OpenCategoryTab()">
						<div class="tab" id="categoryTab">
							Categories
							<div class="selectedTab" style='display:none;'></div>
						</div>
					</a>
					<a href="javascript:void(0);" onclick="SettingsController.OpenColorTab()">
						<div class="tab" id="colorTab">
							Colors
							<div class="selectedTab" style='display:none;'></div>
						</div>
					</a>
					<div style='clear:both'></div>
				</div>
				<div id="specSettingsContent">
					<div class="tabContent">
						<div class="container">
						<form method="post" action="<?php echo PATH ?>settings/save/inventory" id="InventorySettingsForm">
								<div class="row">
									<div class="col-md-12">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Inventory Spec Notifications", "settingsInventory", 1, $this -> inventorySettings -> InventorySpecNotifications) ?>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> Submit('Save Email Settings', 'greenButton') ?>
									</div>
								</div>
							</form>	
							<div class="row">
								<div class="col-md-12">
									<div class="subHeader">Specification Settings</div>	
								</div>
							</div>
							<div class="row buttonContainer">
								<div class="col-md-12">
									<a href="<?php echo PATH ?>settings/create/specgroup">
										<div class="whiteButton" style="float:left; margin-right:5px;">
											Add Group
										</div>				
									</a>
									<a href="<?php echo PATH ?>settings/create/speclabel">
										<div class="whiteButton" style="float:left">
											Add Label
										</div>				
									</a>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div style="height: 20px;">
										<div id="SpecificationGroupResponses" style='color: #1da906; font-weight:bold; text-align: center;'></div>	
									</div>
									<div id="SpecSettingsContainer">
										<?php foreach($this -> SpecSettingsRow as $groupSpec): ?>
											<div class="specGroupContainer" id="<?php echo $groupSpec['specGroupID'] ?>">
												<div class="GroupHeaderElement">
													<div class="DragItem">
														<i class="fa fa-sort" aria-hidden="true"></i>		
													</div>
													<div class="text">
														<?php echo $groupSpec['specGroupText'] ?>
													</div>
													<div class="edit">
														<a href="<?php echo PATH ?>settings/edit/specgroup/<?php echo $groupSpec['specGroupID'] ?>">
															<i class="fa fa-pencil" aria-hidden="true"></i>
														</a>
													</div>
													<div class="delete">
														<a href="<?php echo PATH ?>settings/delete/specgroup/<?php echo $groupSpec['specGroupID'] ?>" onclick="return confirm('Are you sure you want to delete?')">
															<i class="fa fa-trash" aria-hidden="true"></i>
														</a>
													</div>
													<div class="divider"></div>
													<div style="clear:both"></div>
												</div>
												
												<div class="labels">
													<div class="LabelContentList">
														<?php 
														$associatedLabels = array();
														
														if($groupSpec['LabelsByGroup'] != NULL) {
															$LabelIDS = explode(",", $groupSpec['LabelsByGroup']);
															$labelNames = explode(" / ", $groupSpec['GrabbedLabelNames']);
															$labelOrders = explode(",", $groupSpec['LabelOrders']);
															//print_r(search($specLabels, 'relatedSpecGroupID', $groupSpec['specGroupID']));
															
															foreach($LabelIDS as $key => $label) {
																if(isset($labelNames)) {
																	array_push($associatedLabels, array("specLabelID" => $LabelIDS[$key],
																										"labelText" => $labelNames[$key],
																										"labelOrder" => $labelOrders[$key]));											
																}
						
															}
															
															usort($associatedLabels, function($a, $b) {
																return $a['labelOrder'] - $b['labelOrder'];
															});									
														}
						
														
														?>
														
														<?php if(count($associatedLabels)> 0) :?>
															<?php foreach ($associatedLabels as $label) :?>	
																<?php $categoriesByLabel = explode(" - ", $label['labelText'])?>	
																<div class="labelLine">
																	<div style="clear:both">
																		<a href="<?php echo PATH ?>settings/edit/label/<?php echo $label['specLabelID'] ?>">
																			<div class="editLabel">
																				<i class="fa fa-pencil" aria-hidden="true"></i>
																			</div>												
																		</a>
							
																		<div class="labelText">
																			<?php echo $categoriesByLabel[0] ?>	
																		</div>
																		
																		<div class="DeleteLabel">
																			<a href="<?php echo PATH ?>settings/delete/speclabel/<?php echo $label['specLabelID'] ?>" onclick="return confirm('Are you sure you want to delete?')">
																				<i class="fa fa-trash" aria-hidden="true"></i>
																			</a>
																		</div>
																		
																		<div style="clear:both"></div>												
																	</div>
																	
																	<?php if(count($categoriesByLabel) > 1): ?>
																		<div style='font-size: 12px; margin-left: 35px; margin-top: 5px; margin-bottom: 15px;'>
																			<?php echo $categoriesByLabel[1];  ?>	
																		</div>
																	<?php endif; ?>
																</div>
															<?php endforeach; ?>
														<?php endif; ?>
														
													</div>
												</div>
						
											</div>
										<?php endforeach; ?>
										
									</div>		
									
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="CategorySettingsContent" style='display:none;'>
					<div class="tabContent">
						<div class="container">
							
							<div class="row buttonContainer">
								<div class="col-md-12">
									<a href="<?php echo PATH ?>settings/create/inventorycategory">
										<div class="greenButton" style="float:left">
											New Category
										</div>
									</a>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">	
									<?php foreach($this -> categories as $categorySingle): ?>
										<div style="margin-bottom:10px;">
											<div style='font-size:16px;'>
												<?php echo $categorySingle['ParentCategory']?>	
											</div>
											
											<?php 
												
											
												$subCategoriesText = explode(",", $categorySingle['SubCategories']); 
												$subCategoryIDS = explode(",", $categorySingle['SubCategoryIDS']); 
											?>
											<?php foreach($subCategoriesText as $key => $singleCategory): ?> 
												<div>
													<a href="<?php echo PATH ?>settings/edit/inventorycategory/<?php echo $subCategoryIDS[$key] ?>">
														<?php echo $subCategoriesText[$key] ?>	
													</a>	
												</div>
												
											<?php endforeach; ?>
												
											
											
											
											
										</div>
									<?php endforeach; ?>	
								</div>
							</div>
							
						</div>
					</div>
				</div>
				
				<div id="ColorSettingsContent" style='display:none;'>
					<div class="tabContent">
						<div class="container">
							<div class="row">
								<div class="col-md-12">	
									<?php foreach($this -> colors as $colorSingle): ?>
										<div style="margin-bottom:10px;">
											<div style='font-size:16px;'>
												<?php echo $colorSingle['FilterColor']?>	
											</div>
									
										<?php 
											$subColorText = explode(",", $colorSingle['ColorTexts']); 
											$subColorIDS = explode(",", $colorSingle['ColorIDS']); 
										?>
											<?php foreach($subColorText as $key => $singleColor): ?> 
												<div>
													<a href="<?php echo PATH ?>settings/edit/color/<?php echo $subColorIDS[$key] ?>">
														<?php echo $subColorText[$key] ?>	
													</a>	
												</div>
												
											<?php endforeach; ?>
												
										</div>
									<?php endforeach; ?>	
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
	</div>
</div>



