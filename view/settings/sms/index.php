<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Sending Text Disclaimer
							</div>
						</div>
						
						<form method="post" action="<?php echo PATH ?>settings/save/outbounddisclaimer" id="OutBoundTextDisclaimerForm">
							<div class="row">
								<div class="col-md-12">
									<div class="descriptiveInfo">
										This disclaimer text message that will be sent out to customers when you create new outbound text messages
										<br /><br />
										Ex.
										<br /><br />
										[EmployeeName] - The User logged in creating the message<br />
										[StoreName] - The Store Name (i.e. Dillon Brothers Motorsports, etc.)<br />
										[DepartmentName] - Sales,Service,Parts.<br /><br />
										<?php echo $this -> settingsInfo -> SMSDisclosure?>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div id="inputID4">
										<div class="errorMessage"></div>
										<div class="input">
											<textarea name='OutboundTextDisclaimer' class="outBoundTextUpdate"><?php echo $this -> settingsInfo -> SMSDisclosure?></textarea>	
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type='submit' value='Update Disclaimer' class="greenButton" />
								</div>
							</div>
						</form>
					</div>
				</div>
				<?php foreach($this -> stores as $key => $storeSingle): ?>
					<?php 	
						$salesDeptKey = '';
						$storeIDNumber = $storeSingle['storeID'];
														
						$filteredSMSStoreNumbers = array_filter($this -> smsnumbers, function ($var) use($storeIDNumber) {
							return $var['relatedStoreID'] == $storeIDNumber; 
						});
						
						$filteredSMSStoreNumbers = array_values($filteredSMSStoreNumbers);
						
						//echo '<pre>';
						//print_r($filteredSMSStoreNumbers);
						//echo '</pre>';
						
						//reset($filteredSMSStoreNumbers);
						
						$salesDeptKey = array_search('Sales', array_column($filteredSMSStoreNumbers, 'deptName'));
						$serviceDeptKey = array_search('Service', array_column($filteredSMSStoreNumbers, 'deptName'));
						$partsDeptKey = array_search('Parts', array_column($filteredSMSStoreNumbers, 'deptName'));
					?>
					
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="row">
								<div class="col-md-12 subHeader">
									<?php echo $storeSingle['StoreName'] ?>
								</div>
							</div>
							<form method='post' action='<?php echo PATH ?>settings/save/storesms' class='SMSStoreForm'>
								<div class="row">
									<div class="col-md-4">
										<div id="<?php echo $key ?>" class='inputID1'>
											<div class="errorMessage"></div>
											<input type='hidden' name='smsNumberID[]' value='<?php echo $filteredSMSStoreNumbers[$salesDeptKey]['smsdeptID'] ?>' />
											<?php echo $this -> form -> Input("text", "Sales Dept. SMS Number", "settingsSaleDeptNumber", 1, $filteredSMSStoreNumbers[$salesDeptKey]['deptNumber']) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div id="<?php echo $key ?>" class='inputID2'>
											<input type='hidden' name='smsNumberID[]' value='<?php echo $filteredSMSStoreNumbers[$serviceDeptKey]['smsdeptID'] ?>' />
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Service Dept. SMS Number", "settingsServiceDeptNumber", 1, $filteredSMSStoreNumbers[$serviceDeptKey]['deptNumber']) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div id="<?php echo $key ?>" class='inputID3'>
											<input type='hidden' name='smsNumberID[]' value='<?php echo $filteredSMSStoreNumbers[$partsDeptKey]['smsdeptID'] ?>' />
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Parts Dept. SMS Number", "settingsPartsDeptNumber", 1, $filteredSMSStoreNumbers[$partsDeptKey]['deptNumber']) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type='submit' value='Save Store Numbers' class='whiteButton' />
									</div>
								</div>	
							</form>
							
						</div>
					</div>
				<?php endforeach; ?>
				
			</div>
		</div>
	</div>
</div>

<div class="container" style="padding-bottom: 120px;">
	
	
</div>


