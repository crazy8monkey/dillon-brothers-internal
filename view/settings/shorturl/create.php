<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>settings/shorturls">Settings: Short URL</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create New Redirect Rule
				</div>
			</div>	
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style="max-width: 1170px; margin: 0 auto;">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									Create New Redirect Rule
								</div>
							</div>
							<form method="post" action="<?php echo PATH ?>settings/save/shorturl" id="ShortUrlSingleForm">
								<input type='hidden' name='ShortURLActive' value='1' />
								<div class="row">
									<div class="col-md-4">
										<div id="inputID1">
											<div class="errorMessage"></div>											
											<div class="input">
												<div class="inputLabel">Short URL</div>
												<input type='text' name='shortURLText' />	
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div id="inputID2">
											<div class="errorMessage"></div>											
											<div class="input">
												<div class="inputLabel">Redirect Action</div>
												<select name='RedirectString' onchange='SettingsController.RefreshRedirectActions(this, 0)'>
													<option value=''>N/A</option>
													<option value='301Redirect'>301 Redirect</option>
													<option value='410Redirect'>410 Redirect</option>
													<option value='CurrentContent'>Current Content</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div id="inputID2">
											<div class="errorMessage"></div>											
											<div class="input">
												<div class="inputLabel">Redirected Content</div>
												<select name='redirectedContent'>
													<option value=''>Please Select</option>
													<optgroup class='eventContentOptions0' label='Events'>
														<?php foreach($this -> events as $eventSingle): ?>
															<option value='1--<?php echo $eventSingle['seoUrl'] ?>--<?php echo $eventSingle['eventID'] ?>'><?php echo $eventSingle['eventName'] ?></option>
														<?php endforeach; ?>	
													</optgroup>
													<optgroup class='blogContentOptions0' label='Blog posts'>
														<?php foreach($this -> blogposts as $blogPostSingle): ?>
															<option class='blogContentOptions0' value='2--<?php echo $blogPostSingle['blogPostSEOurl'] ?>--<?php echo $blogPostSingle['blogPostID'] ?>'><?php echo $blogPostSingle['blogPostName'] ?></option>
														<?php endforeach; ?>
													</optgroup>
													<optgroup label='Static Pages' class='currentPageContent0'>
														<?php foreach($this -> CurrentWebsiteLinks() as $key => $pageContentSingle): ?>
															<option  value='3--<?php echo $key ?>'><?php echo $pageContentSingle ?></option>
														<?php endforeach; ?>
													</optgroup>
													<optgroup label='Sub Domains' class='subDomainLinks0'>
														<?php foreach($this -> SubDomains() as $key => $subDomainSingle): ?>
															<option  value='4--<?php echo $key ?>'><?php echo $subDomainSingle ?></option>
														<?php endforeach; ?>
													</optgroup>
													
													
												</select>
												
											</div>
										</div>
										
									</div>
								</div>
								
								
						
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> Submit("Save", "greenButton") ?>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container" style="padding-bottom: 120px;">
	
	
</div>

