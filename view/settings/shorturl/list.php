<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Redirect Settings
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo PATH ?>settings/create/shorturl">
									<div class="greenButton" style="float:left">
										New Redirect Rule
									</div>
								</a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id="inputID1"><div class="errorMessage"></div></div>
								<div id="inputID2"><div class="errorMessage"></div></div>
								<div id="inputID3"><div class="errorMessage"></div></div>
							</div>
						</div>
					</div>
				</div>
				<?php foreach($this -> shortURLS as $key => $shortURLSingle): ?>
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="ShortURLSingle">
								<form method='post' action='<?php echo PATH ?>settings/save/shorturl/<?php echo $shortURLSingle['shortLinkID'] ?>'>
									<div class="row">
										<div class="col-md-12">
											<div class="input">
												<input type='hidden' name='ShortURLActive' value='0' />
												<input type='checkbox' name='ShortURLActive' value='1' <?php if($shortURLSingle['shortLinkActive'] == 1): ?> checked <?php endif; ?>/> Active
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="input">
												<div class="inputLabel" style='font-weight:bold'>Short URL</div>
												<input type='text' name='shortURLText' onchange="Globals.removeValidationMessage(1)" value='<?php echo $shortURLSingle['shortUrl']; ?>' />	
											</div>
										</div>
										<div class="col-md-4">
											<div class="input">
												<div class="inputLabel">Redirect Action</div>
												<select name='RedirectString' onchange='SettingsController.RefreshRedirectActions(this, <?php echo $key ?>)'>
													<option value=''>N/A</option>
													<option <?php if($shortURLSingle['Action'] == "301Redirect"): ?>selected<?php endif; ?> value='301Redirect'>301 Redirect</option>
													<option <?php if($shortURLSingle['Action'] == "410Redirect"): ?>selected<?php endif; ?> value='410Redirect'>410 Redirect</option>
													<option <?php if($shortURLSingle['Action'] == "CurrentContent"): ?>selected<?php endif; ?> value='CurrentContent'>Current Content</option>
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="input">
												<div class="inputLabel">Redirected Content</div>
												<select name='redirectedContent'>
													<option value='0'>Please Select</option>
													<optgroup <?php if($shortURLSingle['Action'] == "410Redirect"): ?>style='display:none;'<?php endif; ?> class='eventContentOptions<?php echo $key ?>' label='Events'>
														<?php foreach($this -> events as $eventSingle): ?>
															<option <?php if($shortURLSingle['OriginatedSource'] == $eventSingle['seoUrl']): ?>selected<?php endif; ?> value='1--<?php echo $eventSingle['seoUrl'] ?>--<?php echo $eventSingle['eventID'] ?>'><?php echo $eventSingle['eventName'] ?></option>
														<?php endforeach; ?>		
													</optgroup>
													<optgroup <?php if($shortURLSingle['Action'] == "410Redirect"): ?>style='display:none;'<?php endif; ?> class='blogContentOptions<?php echo $key ?>' label='Blog posts'>
														<?php foreach($this -> blogposts as $blogPostSingle): ?>	
															<option  <?php if($shortURLSingle['OriginatedSource'] == $blogPostSingle['blogPostSEOurl']): ?>selected<?php endif; ?> value='2--<?php echo $blogPostSingle['blogPostSEOurl'] ?>--<?php echo $blogPostSingle['blogPostID'] ?>'><?php echo $blogPostSingle['blogPostName'] ?></option>
														<?php endforeach; ?>
													</optgroup>
													<optgroup label='Static Pages'>
														<?php foreach($this -> CurrentWebsiteLinks() as $key => $pageContentSingle): ?>
															<?php 
																$mainPageSelected = '';
																if($shortURLSingle['relatedEventID'] == 0 && $shortURLSingle['relatedBlogID'] == 0) {
																	if($shortURLSingle['OriginatedSource'] == $key) {
																		$mainPageSelected = 'selected';
																	}		
																}
															?>
															<option class='currentPageContent' <?php echo $mainPageSelected ?> value='3-<?php echo $key ?>'><?php echo $pageContentSingle ?></option>
														<?php endforeach; ?>
													</optgroup>
													<optgroup label='Sub Domains' class='subDomainLinks0'>
														<?php foreach($this -> SubDomains() as $key => $subDomainSingle): ?>
															<option <?php if($shortURLSingle['OriginatedSource'] == $key): ?>selected<?php endif; ?> value='4--<?php echo $key ?>'><?php echo $subDomainSingle ?></option>
														<?php endforeach; ?>
													</optgroup>
												</select>
											</div>
										</div>
										
										
									</div>	
									<div class="row">
										<div class="col-md-12">
											<div class="input">
												<input type='submit' class='whiteButton' value='Save' />
											</div>
										</div>
									</div>
									
								</form>								
							</div>
						</div>
					</div>			
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>



