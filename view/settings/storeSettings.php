<?php 

	
function search($array, $key, $value) {
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, search($subarray, $key, $value));
        }
    }

    return $results;
}
	

?>

<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Store Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="WhiteSectionDiv">
			<div class="content">
				<div class="row">
					<div class="col-md-4">
						<a href="<?php echo PATH ?>settings/edit/store/2">
							<div class="GrayBox" style="margin-bottom:0px; padding:26px 0px">
								<img src="<?php echo PATH ?>public/images/MotorSportLogo.png">
							</div>
						</a>
					</div>
					<div class="col-md-4">
						<a href="<?php echo PATH ?>settings/edit/store/3">
							<div class="GrayBox" style="margin-bottom:0px;">
								<img src="<?php echo PATH ?>public/images/DillonHarley.png">
							</div>
						</a>
					</div>
					
					<div class="col-md-4">
						<a href="<?php echo PATH ?>settings/edit/store/4">
							<div class="GrayBox" style="margin-bottom:0px;">
								<img src="<?php echo PATH ?>public/images/DillonIndian.png">
							</div>
						</a>
					
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>



