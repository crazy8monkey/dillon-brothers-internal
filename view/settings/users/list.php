
<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content" style='padding:0px;'>
						<div class="row">
							<div class="col-md-12 subHeader" style='margin:10px;'>
								Users
							</div>
						</div>
						<div class="row buttonContainer" style='margin:0px 0px 0px 10px;'>
							<div class="col-md-12" style='padding: 0px; padding-bottom: 15px;'>
								<a href="<?php echo PATH ?>settings/create/user">
									<div class="greenButton" style="float:left">
										Add User
									</div>
								</a>
							</div>
						</div>
						<?php foreach($this -> users as $userSingle) : ?>
							<div class="row">
								<div class="col-md-12">
									<div class="UserSingleLine">
										<div class="circle">
											<?php if($userSingle['userProfile'] != NULL): ?>
												<div class="profilePicObject" style="background:url(<?php echo PHOTO_URL ?>Accounts/<?php echo $userSingle['userProfile'] ?>)"></div>
											<?php else: ?>
												<div class="profilePicObject" style="background:url(<?php echo PATH ?>view/users/Images/no-picture.png)"></div>
											<?php endif; ?>
												
											<div class="text"><a href="<?php echo PATH ?>settings/edit/user/<?php echo $userSingle['userID'] ?>"><?php echo $userSingle['firstName'] ?> <?php echo $userSingle['lastName'] ?></a></div>
										</div>	
										<div style='float:right; margin-top: 5px; margin-right: 10px;'>														
											<div class="delete">
												<a href="<?php echo PATH ?>settings/delete/user/<?php echo $userSingle['userID'] ?>" onclick="return confirm('Are you sure you want to delete this user? this action cannot be done')">
													<div class="button">
														<i class="fa fa-trash-o" aria-hidden="true"></i>		
													</div>
												</a>
											</div>			
											<div class="view">
												<a href="<?php echo PATH ?>settings/edit/user/<?php echo $userSingle['userID'] ?>">
													<div class='button'>
														<i class="fa fa-eye" aria-hidden="true"></i>		
													</div>
												</a>
											</div>
										</div>
										
			
										<div style="clear:both"></div>		
									</div>
								</div>
							</div>	
						<?php endforeach; ?>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>






<div class="container">
	
	

			
	
	
</div>

