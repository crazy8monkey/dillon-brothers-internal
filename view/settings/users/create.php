<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>settings/users">Settings: Users</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit User
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style="max-width: 1170px; margin: 0 auto;">
						<div class="row">
								<div class="col-md-12 sectionHeader">
									Create User
								</div>
							</div>
							
							<form method="post" action="<?php echo PATH ?>settings/save/user" id="UserSubmitForm">
								<div class="row">
									<div class="col-md-12">
										<div class="ProfilePicContent">
											<div class="pic" style='background:url(<?php echo PATH ?>view/users/images/no-picture.png) no-repeat center'></div>
											<div class="fileInput">
												<?php echo $this -> form -> FileInput(NULL, "userProfilePic") ?>	
											</div>
											
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "First Name", "userFirstName", 1) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div id="inputID2">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Last Name", "userLastName", 2) ?>
										</div>
										
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div id="inputID5">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Email", "userEmail", 5) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div id="inputID4">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("password", "Password", "userPassword", 4) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div id="inputID6">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel" style='font-size:24px; font-weight:bold;'>Store Viewing Access</div>
												<?php echo $this -> form -> checkbox('Dillon Motor Sports', 'stores[]', NULL, 2, 6) ?>
												<?php echo $this -> form -> checkbox('Dillon Harley', 'stores[]', NULL, 3, 6) ?>
												<?php echo $this -> form -> checkbox('Dillon Indian', 'stores[]', NULL, 4, 6) ?>	
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 sectionHeader">
										Permissions
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div id="eventPermission" class='permissionErrorText'></div>
										<div class="permissionSection">
											<div class='permissionSectionHeader'>Events</div>
											<div class="inputRadioElement">
												<input type="radio" name="EventsViewing" value='0' /> Read Only	
											</div>
											<div class="inputRadioElement">
												<input type="radio" name="EventsViewing" value='1' /> Add/Edit/Delete
											</div>			
										</div>
									</div>
									<div class="col-md-3">
										<div class="permissionSection">
											<div class='permissionSectionHeader'>Photos</div>
											<div class="inputCheckboxElement">
												<input type='checkbox' value='1' name='PhotoAddEditDelete' /> Add/Edit/Delete		
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="permissionSection">
											<div class='permissionSectionHeader'>Newsletters</div>
											<div class="inputCheckboxElement">
												<input type='checkbox' value='1' name='NewsletterAddEditDelete' /> Add/Edit/Delete
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div id="subSpecialCategoryPermission" class='permissionErrorText'></div>
										<div class="permissionSection">
											<div class='permissionSectionHeader'>Specials</div>
											<div class="inputCheckboxElement">
												<input type='checkbox' value='1' name='SpecialsAddEditDelete' onchange="SettingsController.toggleSubCategories('specials', this)" /> Add/Edit/Delete
												<div class="subPermissions" id="specialsSubCategories" style="display:none;">
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SpecialOEMCategoryVisible' /> OEM	
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SpecialMotorclothesCategoryVisible' /> Motorclothes
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SpecialPartsCategoryVisible' /> Parts
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SpecialServiceCategoryVisible' /> Service
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SpecialStoreCategoryVisible' /> Store
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="permissionSection">
											<div class='permissionSectionHeader'>Employees</div>
											<div class="inputCheckboxElement">
												<input type='checkbox' value='1' name='EmployeesAddEditDelete' /> Add/Edit/Delete
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div class="permissionSection">
											<div class='permissionSectionHeader'>Blog</div>
											<div class="inputCheckboxElement">
												<input type='checkbox' value='1' name='BlogAddEditDelete' onchange="SettingsController.toggleSubCategories('blog', this)" /> Add/Edit/Delete													
												<div class="subPermissions" id="blogSubCategories" style="display:none;">
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='BlogAddEditCategories' /> Add/Edit Categories
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='BlogApproveDeclineComments' /> Approve/Decline Comments
													</div>
												</div>
											</div>
										</div>
									</div>	
									<div class="col-md-3">
										<div class="permissionSection">
											<div id="inventoryPermission" class='permissionErrorText'></div>
											<div class='permissionSectionHeader'>Inventory</div>
											<div class="inputRadioElement">
												<input type="radio" name="InventoryViewing" value='0' onclick="SettingsController.toggleRadioSubPermission('inventory', this)" /> Read Only	
											</div>
											<div class="inputRadioElement">
												<input type="radio" name="InventoryViewing" value='1' onclick="SettingsController.toggleRadioSubPermission('inventory', this)" /> Add/Edit/Delete
											</div>
											<div class="subPermissions" id="inventorySubPermissions" style='display:none;'>
												<div class="inputCheckboxElement">
													<input type='checkbox' value='1' name='InventorySpecificationManagement' /> Specification Management
												</div>
											</div>																							
										</div>
									</div>
									<div class="col-md-2">
										<div class="permissionSection">
											<div id="socialMediaListPermission" class='permissionErrorText'></div>
											<div class='permissionSectionHeader'>Social Media</div>
											<div class="inputRadioElement">
												<input type="radio" name="SocialMediaPermissionType" value='0' /> Administrator
											</div>
											<div class="inputRadioElement">
												<input type="radio" name="SocialMediaPermissionType" value='1' /> Submitter
											</div>				
											<div class="inputRadioElement">
												<input type="radio" name="SocialMediaPermissionType" value='2' /> Not Submitter
											</div>				
										</div>
									</div>
									<div class="col-md-2">
										<div class="permissionSection">
											<div class='permissionSectionHeader'>Shopping Cart</div>
											<div class="inputCheckboxElement">
												<input type='checkbox' value='1' name='ShoppingCartAccess' /> Viewing Access
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="permissionSection">
											<div id="leadsViewingPermission" class='permissionErrorText'></div>
											<div class="permissionSectionHeader">Leads</div>
											<div class="inputRadioElement">
												<input type="radio" name="leadsViewing" value="1"> Access	
											</div>
											<div class="inputRadioElement">
												<input type="radio" name="leadsViewing" value="0"> No Access
											</div>
										</div>
									</div>									
								</div>			
								<div class="row">
									<div class="col-md-3">
										<div class="permissionSection">
											<div id="subSettingsPermission" class='permissionErrorText'></div>
											<div class='permissionSectionHeader'>Settings</div>
											<div class="inputCheckboxElement">
												<input type='checkbox' value='1' name='SettingsViewEdit' onchange="SettingsController.toggleSubCategories('settings', this)" /> View/Edit													
												<div class="subPermissions" id="specialSubPermissions" style="display:none;">
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsUsers' /> Users
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsDonations' /> Donations
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsSocialMedia' /> Social Media
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsSpecs' /> Spec Settings
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsInventoryCateories' /> Inventory Categories
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsInventoryColors' /> Inventory Colors
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsStoreSettings' /> Store Settings
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsPhotoSettings' /> Photo Settings
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsConversionsManufacture' /> Data Conversions: Manufacture
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsSpecLabelMapping' /> Spec Label Mapping
													</div>
													<div class="inputCheckboxElement">
														<input type='checkbox' value='1' name='SettingsShortUrls' /> Redirects
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>				
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> Submit('Create', 'greenButton') ?>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



