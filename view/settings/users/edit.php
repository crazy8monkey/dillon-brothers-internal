<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<?php if(!isset($this -> profileView)) : ?>
				<div class="row">
					<div class="col-md-12 breadCrumbs">
						<a href="<?php echo PATH ?>settings/users">Settings: Users</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit User
					</div>
				</div>
			<?php else: ?>
				<div class="row">
					<div class="col-md-12 sectionHeader">
						<?php if(isset($this -> profileView)) {
							echo "Your Profile";
						} else {
							echo "Edit User";
						}
						?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style="max-width: 1170px; margin: 0 auto;">
							<?php
							$formURL = NULL; 
							if(isset($this -> profileView)) {
								$formURL = 'profile/save/';
							} else {
								$formURL = 'settings/save/user/';
							}
							?>
							<?php if(!isset($this -> profileView)): ?>							
								<div class="row">
									<div class="col-md-12 sectionHeader">
										Edit User
									</div>
								</div>
							<?php endif; ?>
							<form method="post" action="<?php echo PATH . $formURL . $this -> UserSingle -> GetUserId() ?>" id="UserSubmitForm">
								<div class="row">
									<div class="col-md-12">
										<div class="ProfilePicContent">
											<?php if($this -> UserSingle -> currentProfilePic != NULL): ?>
												<div class="pic" style='background:url(<?php echo PHOTO_URL ?>Accounts/<?php echo $this -> UserSingle -> currentProfilePic ?>) no-repeat center'></div>
											<?php else: ?>
												<div class="pic" style='background:url(<?php echo PATH ?>view/users/Images/no-picture.png) no-repeat center'></div>
											<?php endif; ?>
											
											<div class="fileInput">
												<?php echo $this -> form -> FileInput(NULL, "userProfilePic") ?>	
											</div>
											
										</div>
									</div>
								</div> 
						        <div class="row">
									<div class="col-md-6">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "First Name", "userFirstName", 1, $this -> UserSingle -> FirstName) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div id="inputID2">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Last Name", "userLastName", 2, $this -> UserSingle -> LastName) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div id="inputID5">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Email", "userEmail", 5, $this -> UserSingle -> Email) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div id="inputID4">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("password", "Password", "userPassword", 4) ?>
										</div>
									</div>
								</div>
								<?php if(!isset($this -> profileView)) :?>
									<div class="row">
										<div class="col-md-12">
											<div id="inputID6">
												<div class="errorMessage"></div>
												<div class="input">
													<div class="inputLabel" style='font-size:24px; font-weight:bold;'>Store Viewing Access</div>
													<?php 
														$userID = $this -> UserSingle -> GetUserId();
													?>
													<?php echo $this -> form -> checkbox('Dillon Motor Sports', 'stores[]', $this -> UserSingle -> GetAssociatedStore($userID, 2), 2, 6) ?>
													<?php echo $this -> form -> checkbox('Dillon Harley', 'stores[]', $this -> UserSingle -> GetAssociatedStore($userID, 3), 3, 6) ?>
													<?php echo $this -> form -> checkbox('Dillon Indian', 'stores[]', $this -> UserSingle -> GetAssociatedStore($userID, 4), 4, 6) ?>	
												</div>
											</div>
										</div>
									</div>
								
									<?php 
											
										$EventsViewingKey = array_search('EventsReadOnlyOrEdit', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$photoViewingKey = array_search('PhotoManagementAddEditDelete', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$newsletterViewingKey = array_search('NewsletterAddEditDelete', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$specialsViewingKey = array_search('SpecialsAddEditDelete', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$specialOEMKey = array_search('SpecialOEMCategoryVisible', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$specialMotorclothesKey = array_search('SpecialMotorclothesCategoryVisible', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$specialPartsKey = array_search('SpecialPartsCategoryVisible', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$specialServiceKey = array_search('SpecialServiceCategoryVisible', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$specialStoreKey = array_search('SpecialStoreCategoryVisible', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$EmployeeViewingKey = array_search('EmployeesAddEditDelete', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$BlogViewingKey = array_search('BlogAddEditDelete', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$BlogAddEditCategoriesKey = array_search('BlogAddEditCategories', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$BlogApproveDeclineKey = array_search('BlogApproveDeclineComments', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$inventoryViewingKey = array_search('InventoryViewing', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$inventorySpecificationsViewingKey = array_search('InventorySpecificationManagement', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$socialMediaPermissionTypeKey = array_search('SocialMediaPermissionType', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$shoppingPermissionKey = array_search('ShoppingCartAccess', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$leadsPermissionKey = array_search('leadsViewing', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsViewEditKey = array_search('SettingsViewEdit', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsUsersKey = array_search('SettingsUsers', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsSocialMediaKey = array_search('SettingsSocialMedia', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsSpecsKey = array_search('SettingsSpecs', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsInventoryCategoriesKey = array_search('SettingsInventoryCateories', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsInventoryColorsKey = array_search('SettingsInventoryColors', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsStoresKey = array_search('SettingsStoreSettings', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsPhotosKey = array_search('SettingsPhotoSettings', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsConversionsManufactureKey = array_search('SettingsConversionsManufacture', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsSpecLabelMappingKey = array_search('SettingsSpecLabelMapping', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsDonationsKey = array_search('SettingsDonations', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										$settingsShortUrlKey = array_search('SettingsShortUrls', array_column($this -> UserSingle -> _permissions, 'PermissionName'));
										
										
									?>
								
									<div class="row">
										<div class="col-md-12 sectionHeader">
											Permissions
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<div id="eventPermission" class='permissionErrorText'></div>
											<div class="permissionSection">
												<div class='permissionSectionHeader'>Events</div>
												<?php if($EventsViewingKey !== false): ?>
													<div class="inputRadioElement">
														<input type="radio" name="EventsViewing" <?php if($this -> UserSingle -> _permissions[$EventsViewingKey]['Enabled'] == 0): ?>checked<?php endif; ?> value='0' /> Read Only	
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="EventsViewing" <?php if($this -> UserSingle -> _permissions[$EventsViewingKey]['Enabled'] == 1): ?>checked<?php endif; ?> value='1' /> Add/Edit/Delete
													</div>
												<?php else: ?>
													<div class="inputRadioElement">
														<input type="radio" name="EventsViewing" value='0' /> Read Only	
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="EventsViewing" value='1' /> Add/Edit/Delete
													</div>
												<?php endif; ?>
												
											</div>
										</div>
										<div class="col-md-3">
											<div class="permissionSection">
												<div class='permissionSectionHeader'>Photos</div>
												<div class="inputCheckboxElement">
													<?php if($photoViewingKey !== false): ?>
														<input type='checkbox' value='1' name='PhotoAddEditDelete' checked /> Add/Edit/Delete
													<?php else: ?>
														<input type='checkbox' value='1' name='PhotoAddEditDelete' /> Add/Edit/Delete
													<?php endif; ?>	
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="permissionSection">
												<div class='permissionSectionHeader'>Newsletters</div>
												<div class="inputCheckboxElement">
													<?php if($newsletterViewingKey !== false): ?>
														<input type='checkbox' value='1' name='NewsletterAddEditDelete' checked /> Add/Edit/Delete
													<?php else: ?>
														<input type='checkbox' value='1' name='NewsletterAddEditDelete' /> Add/Edit/Delete
													<?php endif; ?>	
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div id="subSpecialCategoryPermission" class='permissionErrorText'></div>
											<div class="permissionSection">
												<div class='permissionSectionHeader'>Specials</div>
												<div class="inputCheckboxElement">
													<?php if($specialsViewingKey !== false): ?>
														<input type='checkbox' value='1' name='SpecialsAddEditDelete' onchange="SettingsController.toggleSubCategories('specials', this)" checked /> Add/Edit/Delete
													<?php else: ?>
														<input type='checkbox' value='1' name='SpecialsAddEditDelete' onchange="SettingsController.toggleSubCategories('specials', this)" /> Add/Edit/Delete
													<?php endif; ?>	
													<div class="subPermissions" id="specialsSubCategories" <?php if($specialsViewingKey === false): ?> style="display:none;" <?php endif; ?>>
														<div class="inputCheckboxElement">
															<?php if($specialOEMKey !== false): ?>
																<input type='checkbox' value='1' name='SpecialOEMCategoryVisible' <?php if($this -> UserSingle -> _permissions[$specialOEMKey]['Enabled'] == 1): ?> checked<?php endif; ?> /> OEM
															<?php else: ?>
																<input type='checkbox' value='1' name='SpecialOEMCategoryVisible' /> OEM
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($specialMotorclothesKey !== false): ?>
																<input type='checkbox' value='1' name='SpecialMotorclothesCategoryVisible'<?php if($this -> UserSingle -> _permissions[$specialMotorclothesKey]['Enabled'] == 1): ?> checked<?php endif; ?> /> Motorclothes
															<?php else: ?>
																<input type='checkbox' value='1' name='SpecialMotorclothesCategoryVisible' /> Motorclothes
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($specialPartsKey !== false): ?>
																<input type='checkbox' value='1' name='SpecialPartsCategoryVisible' checked /> Parts
															<?php else: ?>
																<input type='checkbox' value='1' name='SpecialPartsCategoryVisible' /> Parts
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($specialServiceKey !== false): ?>
																<input type='checkbox' value='1' name='SpecialServiceCategoryVisible' checked /> Service
															<?php else: ?>
																<input type='checkbox' value='1' name='SpecialServiceCategoryVisible' /> Service
															<?php endif; ?>															
														</div>
														<div class="inputCheckboxElement">
															<?php if($specialStoreKey !== false): ?>
																<input type='checkbox' value='1' name='SpecialStoreCategoryVisible' checked /> Store
															<?php else: ?>
																<input type='checkbox' value='1' name='SpecialStoreCategoryVisible' /> Store
															<?php endif; ?>	
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="permissionSection">
												<div class='permissionSectionHeader'>Employees</div>
												<div class="inputCheckboxElement">
													<?php if($EmployeeViewingKey !== false): ?>
														<input type='checkbox' value='1' name='EmployeesAddEditDelete' checked /> Add/Edit/Delete
													<?php else: ?>
														<input type='checkbox' value='1' name='EmployeesAddEditDelete' /> Add/Edit/Delete
													<?php endif; ?>	
													
												</div>
											</div>
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-3">
											<div class="permissionSection">
												<div class='permissionSectionHeader'>Blog</div>
												<div class="inputCheckboxElement">
													<?php if($BlogViewingKey !== false): ?>
														<input type='checkbox' value='1' name='BlogAddEditDelete' checked onchange="SettingsController.toggleSubCategories('blog', this)" /> Add/Edit/Delete
													<?php else: ?>
														<input type='checkbox' value='1' name='BlogAddEditDelete' onchange="SettingsController.toggleSubCategories('blog', this)" /> Add/Edit/Delete
													<?php endif; ?>	
													
													
													<div class="subPermissions" id="blogSubCategories" <?php if($BlogViewingKey === false): ?> style="display:none;" <?php endif; ?>>
														<div class="inputCheckboxElement">
															<?php if($BlogAddEditCategoriesKey !== false): ?>
																<input type='checkbox' value='1' name='BlogAddEditCategories' checked /> Add/Edit Categories
															<?php else: ?>
																<input type='checkbox' value='1' name='BlogAddEditCategories' /> Add/Edit Categories
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($BlogApproveDeclineKey !== false): ?>
																<input type='checkbox' value='1' name='BlogApproveDeclineComments' checked /> Approve/Decline Comments 
															<?php else: ?>
																<input type='checkbox' value='1' name='BlogApproveDeclineComments' /> Approve/Decline Comments
															<?php endif; ?>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="permissionSection">
												<div id="inventoryPermission" class='permissionErrorText'></div>
												<div class='permissionSectionHeader'>Inventory</div>
												<?php if($inventoryViewingKey !== false): ?>
													<div class="inputRadioElement">
														<input type="radio" name="InventoryViewing" <?php if($this -> UserSingle -> _permissions[$inventoryViewingKey]['Enabled'] == 0): ?>checked<?php endif; ?> value='0' onclick="SettingsController.toggleRadioSubPermission('inventory', this)" /> Read Only	
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="InventoryViewing" <?php if($this -> UserSingle -> _permissions[$inventoryViewingKey]['Enabled'] == 1): ?>checked<?php endif; ?> value='1' onclick="SettingsController.toggleRadioSubPermission('inventory', this)" /> Add/Edit/Delete
													</div>		
													<div class="subPermissions" id="inventorySubPermissions" <?php if($this -> UserSingle -> _permissions[$inventoryViewingKey]['Enabled'] == 0): ?> style='display:none;'<?php endif; ?>>
														<div class="inputCheckboxElement">
															<?php if($inventorySpecificationsViewingKey !== false): ?>
																<input type='checkbox' value='1' name='InventorySpecificationManagement' checked /> Specification Management
															<?php else: ?>
																<input type='checkbox' value='1' name='InventorySpecificationManagement' /> Specification Management
															<?php endif; ?>
														</div>
													</div>											
												<?php else: ?>
													<div class="inputRadioElement">
														<input type="radio" name="InventoryViewing" value='0' onclick="SettingsController.toggleRadioSubPermission('inventory', this)" /> Read Only	
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="InventoryViewing" value='1' onclick="SettingsController.toggleRadioSubPermission('inventory', this)" /> Add/Edit/Delete
													</div>
													<div class="subPermissions" id="inventorySubPermissions" style='display:none;'>
														<div class="inputCheckboxElement">
															<?php if($inventorySpecificationsViewingKey !== false): ?>
																<input type='checkbox' value='1' name='InventorySpecificationManagement' checked /> Specification Management
															<?php else: ?>
																<input type='checkbox' value='1' name='InventorySpecificationManagement' /> Specification Management
															<?php endif; ?>
														</div>
													</div>											
												<?php endif; ?>
												
												
											</div>
										</div>
										<div class="col-md-2">
											<div class="permissionSection">
												<div id="socialMediaListPermission" class='permissionErrorText'></div>
												<div class='permissionSectionHeader'>Social Media</div>
												<?php if($socialMediaPermissionTypeKey !== false): ?>
													<div class="inputRadioElement">
														<input type="radio" name="SocialMediaPermissionType" <?php if($this -> UserSingle -> _permissions[$socialMediaPermissionTypeKey]['Enabled'] == 0): ?>checked<?php endif; ?> value='0' /> Administrator
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="SocialMediaPermissionType" <?php if($this -> UserSingle -> _permissions[$socialMediaPermissionTypeKey]['Enabled'] == 1): ?>checked<?php endif; ?> value='1' /> Submitter
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="SocialMediaPermissionType" <?php if($this -> UserSingle -> _permissions[$socialMediaPermissionTypeKey]['Enabled'] == 2): ?>checked<?php endif; ?> value='2' /> Not Submitter
													</div>				
												<?php else: ?>
													<div class="inputRadioElement">
														<input type="radio" name="SocialMediaPermissionType" value='0' /> Administrator
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="SocialMediaPermissionType" value='1' /> Submitter
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="SocialMediaPermissionType" value='2' /> Not Submitter
													</div>				
												<?php endif; ?>
											</div>
										</div>
										<div class="col-md-2">
											<div class="permissionSection">
												
												
												<div class='permissionSectionHeader'>Shopping Cart</div>
												<div class="inputCheckboxElement">
													<?php if($shoppingPermissionKey !== false): ?>
														<input type='checkbox' value='1' name='ShoppingCartAccess' checked /> Viewing Access
													<?php else: ?>
														<input type='checkbox' value='1' name='ShoppingCartAccess' /> Viewing Access
													<?php endif; ?>
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="permissionSection">
												<div id="leadsViewingPermission" class='permissionErrorText'></div>
												<div class="permissionSectionHeader">Leads</div>
												<?php if($leadsPermissionKey !== false): ?>
													<div class="inputRadioElement">
														<input type="radio" name="leadsViewing" <?php if($this -> UserSingle -> _permissions[$leadsPermissionKey]['Enabled'] == 1): ?>checked<?php endif; ?> value="1"> Access	
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="leadsViewing" <?php if($this -> UserSingle -> _permissions[$leadsPermissionKey]['Enabled'] == 0): ?>checked<?php endif; ?> value="0"> No Access
													</div>
												<?php else: ?>
													<div class="inputRadioElement">
														<input type="radio" name="leadsViewing" value="0"> Access	
													</div>
													<div class="inputRadioElement">
														<input type="radio" name="leadsViewing" value="1"> No Access
													</div>
												<?php endif; ?>
												
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<div class="permissionSection">
												<div id="subSettingsPermission" class='permissionErrorText'></div>
												<div class='permissionSectionHeader'>Settings</div>
												<div class="inputCheckboxElement">
													<?php if($settingsViewEditKey !== false): ?>
														<input type='checkbox' value='1' name='SettingsViewEdit' onchange="SettingsController.toggleSubCategories('settings', this)" checked /> View/Edit
													<?php else: ?>
														<input type='checkbox' value='1' name='SettingsViewEdit' onchange="SettingsController.toggleSubCategories('settings', this)" /> View/Edit
													<?php endif; ?>
													
													<div class="subPermissions" id="specialSubPermissions" <?php if($settingsViewEditKey === false): ?> style="display:none;" <?php endif; ?>>
														<div class="inputCheckboxElement">
															<?php if($settingsUsersKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsUsers' checked /> Users
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsUsers' /> Users
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsDonationsKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsDonations' checked /> Donations
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsDonations' /> Donations
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsSocialMediaKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsSocialMedia' checked /> Social Media
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsSocialMedia' /> Social Media
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsSpecsKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsSpecs' checked /> Spec Settings
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsSpecs' /> Spec Settings
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsInventoryCategoriesKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsInventoryCateories' checked /> Inventory Categories
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsInventoryCateories' /> Inventory Categories
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsInventoryColorsKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsInventoryColors' checked /> Inventory Colors
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsInventoryColors' /> Inventory Colors
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsStoresKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsStoreSettings' checked /> Store Settings
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsStoreSettings' /> Store Settings
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsPhotosKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsPhotoSettings' checked /> Photo Settings
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsPhotoSettings' /> Photo Settings
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsConversionsManufactureKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsConversionsManufacture' checked /> Data Conversions: Manufacture
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsConversionsManufacture' /> Data Conversions: Manufacture
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsSpecLabelMappingKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsSpecLabelMapping' checked /> Spec Label Mapping
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsSpecLabelMapping' /> Spec Label Mapping
															<?php endif; ?>
														</div>
														<div class="inputCheckboxElement">
															<?php if($settingsShortUrlKey !== false): ?>
																<input type='checkbox' value='1' name='SettingsShortUrls' checked /> Redirects
															<?php else: ?>
																<input type='checkbox' value='1' name='SettingsShortUrls' /> Redirects
															<?php endif; ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php endif; ?>
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> Submit('Save', 'greenButton') ?>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



