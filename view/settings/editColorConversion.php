<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo PATH ?>settings/type/dataconversions">Data Conversions</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Conversion Rule
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Edit Data Conversion
		</div>
	</div>
	<form method="post" action="<?php echo PATH ?>settings/save/conversion/<?php echo $this -> dataconversion -> GetID(); ?>" id="DataConversionForm">
		<div style="margin-bottom:10px;">
			<?php if($this -> dataconversion -> CurrentConversionType == 1): ?>
				<div class="row">
					<div class="col-md-12">
						<div><strong>Selected Spec Label Conversion</strong></div>
						<div style='font-size:24px;'><?php echo $this -> dataconversion -> specLabelText; ?></div>
					</div>
				</div>
			<?php endif; ?>
			<?php if($this -> dataconversion -> CurrentConversionType == 2): ?>
				<div class="row">
					<div class="col-md-12">
						<div><strong>Inventory Data Conversion</strong></div>
						<div style='font-size:24px;'>Color</div>
					</div>
				</div>
			<?php endif; ?>	
			<?php if($this -> dataconversion -> CurrentConversionType == 3): ?>
				<div class="row">
					<div class="col-md-12">
						<div><strong>Inventory Data Conversion</strong></div>
						<div style='font-size:24px;'>Manufacturer</div>
					</div>
				</div>
			<?php endif; ?>	
			<?php if($this -> dataconversion -> CurrentConversionType == 4): ?>
				<div class="row">
					<div class="col-md-12">
						<div><strong>Inventory Data Conversion</strong></div>
						<div style='font-size:24px;'>Category</div>
					</div>
				</div>
			<?php endif; ?>				
		</div>

		
		<div class="row">
			<div class="col-md-12">
				<div id="inputID2"><div class="errorMessage"></div></div>	
				<div id="inputID3"><div class="errorMessage"></div></div>	
			</div>
		</div>
		
		<div id="ConversionRules">
			<?php foreach($this -> rules as $ruleSingle) : ?>
				<?php if($this -> dataconversion -> CurrentConversionType == 1): ?>				
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="ConversionRuleID[]" value="<?php echo $ruleSingle['ruleID'] ?>" />
							<div class="input">
								<div class="inputLabel">Conversion Action</div>
								<select name="ConversionAction[]">
									<option value='0'>Select</option>
									<option value='1' <?php if($ruleSingle['ActionMethod'] == 1): ?> selected <?php endif; ?>>If Text Contains</option>
								</select>					
							</div>
			
						</div>
						<div class="col-md-6">
							<div class="input">
								<div class="inputLabel">Find Key Words</div>
								<input type="text" name="keywordSearchFind[]" placeholder="i.e. Compression Ratio" value="<?php echo $ruleSingle['KeyWordFind'] ?>" />
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if($this -> dataconversion -> CurrentConversionType == 2): ?>
					<div class="row">
						<div style="border-bottom:1px solid #cecece; margin:15px; clear:both"></div>
						<div class="col-md-6">
							<div class="inputLabel" style='font-weight:bold'>Current Color</div>
							<a href="<?php echo PATH ?>settings/edit/colorrule/<?php echo $ruleSingle['ruleID'] ?>">
								<?php echo $ruleSingle['CurrentColorText'] ?>
							</a>
						</div>
						<div class="col-md-6">
							<div class="inputLabel" style='font-weight:bold'>Switch Color</div>
							<?php echo str_replace(",","/",$ruleSingle['SelectedColorText']) ?>
						</div>
						
					</div>
				<?php endif; ?>
				<?php if($this -> dataconversion -> CurrentConversionType == 3): ?>
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="ConversionRuleID[]" value="<?php echo $ruleSingle['ruleID'] ?>" />
							<div class="input">
								<div class="inputLabel">Current Text</div>
								<input type="text" name="CurrentManufactureText[]" placeholder="i.e. KAWASA" value="<?php echo $ruleSingle['CurrentManufactureText'] ?>" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="input">
								<div class="inputLabel">Switch To</div>
								<input type="text" name="NewManufactureText[]" placeholder="i.e. Kawasaki" value="<?php echo $ruleSingle['NewManufacturerText'] ?>" />
							</div>
						</div>
					</div>
				<?php endif; ?>	
			<?php endforeach; ?>
		</div>
		
		<?php if($this -> dataconversion -> CurrentConversionType == 4): ?>
		<?php endif; ?>	
		
		<div class="row">
			<div class="col-md-12">
				<a href="javascript:void(0);" onclick="SettingsController.AddConversionRule(<?php echo $this -> dataconversion -> CurrentConversionType ?>, <?php echo $this -> dataconversion -> GetID() ?>)">
					<div class="AddSpec">Add Conversion Rules</div>	
				</a>
			</div>
		</div>
		
		
		
		
		<?php if($this -> dataconversion -> CurrentConversionType == 1 || $this -> dataconversion -> CurrentConversionType == 3): ?>				
			<div class="row" style='margin-top:10px;'>
				<div class="col-md-12">
					<?php echo $this -> form -> Submit("Save", "greenButton") ?>
				</div>
			</div>
		<?php endif; ?>
	
	
		
		
	</form>
</div>


<div class="row" id="NewConversionRuleElementSpecMapping" style='display:none;'>
	<div class="col-md-6">
		<input type="hidden" name="ConversionRuleID[]" value="0" />
		<div class="input">
			<div class="inputLabel">Conversion Action</div>
			<select name="ConversionAction[]">
				<option value='0'>Select</option>
				<option value='1'>If Text Contains</option>
			</select>					
		</div>
	</div>
	<div class="col-md-6">
		<div class="input">
			<div class="inputLabel">Find Key Words</div>
			<input type="text" name="keywordSearchFind[]" placeholder="i.e. Compression Ratio" />
		</div>
	</div>
</div>

<div class="row" id="NewConversionRuleElementInventoryManufacture" style='display:none;'>
	<div class="col-md-6">
		<input type="hidden" name="ConversionRuleID[]" value="0" />
		<div class="input">
			<div class="inputLabel">Current Text</div>
			<input type="text" name="CurrentManufactureText[]" placeholder="i.e. KAWASA" />
		</div>
	</div>
	<div class="col-md-6">
		<div class="input">
			<div class="inputLabel">Switch To</div>
			<input type="text" name="NewManufactureText[]" placeholder="i.e. Kawasaki" />
		</div>
	</div>
</div>
