<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Settings
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="WhiteSectionDiv">
					<?php include 'view/settings/settingsSubLinks.php' ; ?>
				</div>
			</div>
			<div class="col-md-10">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Chat Channels
							</div>
						</div>
						
						<form method="post" action="<?php echo PATH ?>settings/savenewchannel" id="ChatChannelForm">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Add New Chat Channel", "newChatChannelName", 1) ?>
									</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-md-12 subHeader">
									Related Store
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style='margin-bottom:10px;'>
									<div id="inputID2">
										<div class="errorMessage"></div>
									</div>
									
									<?php foreach($this -> stores as $storeSingle): ?>
										<div class='inputCheckboxElement '>
											<input type='radio' onchange="Globals.removeValidationMessage(2)" value='<?php echo $storeSingle['storeID'] ?>' name='relatedStoreID[]' /> <?php echo $storeSingle['StoreName'] ?>	
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type='submit' value="Save New Chat Channel" class="greenButton" />
								</div>
							</div>
						</form>
					</div>
				</div>
				<?php foreach($this -> chatChannels as $channelSingle): ?>
					<div class="WhiteSectionDiv">
						<div class="content" style='position:relative'>
							<div class="deleteChatChannel">
								<a href='<?php echo PATH ?>settings/delete/chatchannel/<?php echo $channelSingle['chatChannelID'] ?>'>
									<i class="fa fa-trash" aria-hidden="true"></i>	
								</a>
							</div>
							<div class="row">
								<div class="col-md-12 subHeader">
									Channel: <?php echo $channelSingle['friendlyName'] ?>
								</div>
							</div>
							<?php 
								$channelID = $channelSingle['chatChannelID'];
								
								$filteredChatUsers = array_filter($this -> chatUsers, function ($var) use($channelID) {
									return $var['relatedTwilioChatID'] == $channelID; 
								});
							?>
							<?php if(count($filteredChatUsers) > 0): ?>
								<div style='margin-bottom:20px;'>
									<div><strong>Users on this chat Channel</strong></div>
									<?php foreach($filteredChatUsers as $userSingle): ?>
										<div>
											<?php echo $userSingle['firstName'] ?> <?php echo $userSingle['lastName'] ?>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							
							<div class="row">
								<div class="col-md-12">
									<a href='<?php echo PATH ?>settings/adduserstochat/<?php echo $channelSingle['chatChannelID'] ?>'>Add Users to channel >></a>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
