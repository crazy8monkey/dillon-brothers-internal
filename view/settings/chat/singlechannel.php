<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>settings/chat">Settings: Chat</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Viewing Channel
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="container">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									Viewing Chat Channel
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style='margin-bottom:10px;'>
									<div><strong>Channel Name</strong></div>
									<?php echo $this -> chatchanneldetails -> FriendlyName?>
								</div>
							</div>
							<form method='post' id="UserChannelForm" action="<?php echo PATH ?>settings/save/chatusers/<?php echo $this -> chatchanneldetails -> GetID()?>">
								<div class="row">
									<div class="col-md-12 subHeader">
										Related Store
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										
										<?php foreach($this -> stores as $storeSingle): ?>
											<div class='inputCheckboxElement '>
												<input type='radio' <?php if($this -> chatchanneldetails -> CurrentStoreID == $storeSingle['storeID']): ?>checked <?php endif; ?>value='<?php echo $storeSingle['storeID'] ?>' name='relatedStoreID[]' /> <?php echo $storeSingle['StoreName'] ?>	
											</div>
										<?php endforeach; ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 subHeader" style='margin-top:10px;'>
										Users on this channel
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<?php foreach($this -> usersList as $key => $userSingle): ?>										
											<div class="inputCheckboxElement">
												<input type='hidden' name='userName[<?php echo $key ?>]' value='<?php echo $userSingle['firstName'] ?> <?php echo $userSingle['lastName'] ?>' />
												<input type='checkbox' <?php if(in_array($userSingle['userID'], array_column($this -> chatchanneldetails -> CurrentChatUsers, 'relatedUserID'))):?> checked <?php endif; ?> name='channelUsers[<?php echo $key ?>]' value='<?php echo $userSingle['userID'] ?>' /> <?php echo $userSingle['firstName'] ?> <?php echo $userSingle['lastName'] ?>
											</div>
										<?php endforeach; ?>
									</div>
								</div>	
								<div class="row" style='margin-top:10px;'>
									<div class="col-md-12">
										<input type='submit' value="Save" class='greenButton' />
									</div>
								</div>
							</form>
							
						</div>
						
						
						

						
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
