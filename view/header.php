<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
	<head>
		<title><?php echo isset($this->title) ? $this->title : 'Dillon Brothers'; ?></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		
				
		
		
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
		
		
		
		<?php if(isset($this->css))  {
			foreach ($this -> css as $css)  {	?>
			<link rel="stylesheet" href="<?php echo $css; ?>" /> 
		<?php }
		}
		?>
		
		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
		
		
		
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
		
		<script src="<?php echo PATH ?>public/js/ckeditor/ckeditor.js"></script>
		 
		 <?php if(isset($this -> multipePhotoUpload)): ?>
			<link rel="stylesheet" href="<?php echo PATH ?>public/css/FormStone.css" />
			<script src="<?php echo PATH ?>/public/js/FormStone.js" type="text/javascript"></script>
		<?php endif; ?>		
 		
		<?php if(isset($this->js))  {
			foreach ($this -> js as $js) {	?>
				<script type="text/javascript" src="<?php echo $js; ?>"></script>
			<?php }
		}
		?>
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
		<script src="<?php echo PATH ?>public/js/maskedInput.js" type="text/javascript" type="text/javascript"></script>
		
		
		
		<script type="text/javascript"> 
			document.createElement("header");
			document.createElement("footer");

			$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip({html: true});
				
				<?php if(isset($this->startJsFunction)) : ?>
					<?php foreach ($this -> startJsFunction as $startJsFunction) : ?>	
						<?php echo $startJsFunction; ?>	
					<?php endforeach ?>
				<?php endif; ?>
			});
			Globals.InitializeApp();
			
			//var webSocketConnectionTest = new WebSocket('ws://localhost:80/DillonBrothers/updates/websocketconnect');
			
			//webSocketConnectionTest.onmessage = function(e) {
			//	console.log('hi');
			//}
			
			//var notification = new Notification('Notification title', {
			//	body: 'hi',
			//});
	        	
	        //notification.show();
			
			
		</script>
		
		
		
	</head>
<body <?php if(isset($this -> blueBackground)): ?> class='BlueBackground' <?php endif; ?>>

<?php if(isset($this-> adminLogin)) : ?>
	<?php if(!isset($this -> uploadSingleView)): ?>
	
<header>
	<div class="ContentPage">
		<div class="AdminTitleText">
			Dillon Brothers CMS
		</div>
		<div class="RightMenu">
			<ul>
				<li data-toggle='tooltip' data-placement='bottom' title='Your Profile' style='padding:13px 0px 11px 15px;'>
					
					<a href="<?php echo PATH ?>profile">
						<?php if($_SESSION['user'] -> currentProfilePic != NULL) : ?>
							<div class="profilePic" style='background:url(<?php echo PHOTO_URL ?>Accounts/<?php echo $_SESSION['user'] -> currentProfilePic ?>)'></div>
						<?php else: ?>
							<div class="profilePic" style='background:url(<?php echo PATH ?>view/users/Images/no-picture.png)'></div>
						<?php endif; ?>
						<div style="white-space: nowrap; margin-right: 35px; margin-top: 3px;"><?php echo $_SESSION['user'] -> FirstName ?> <?php echo $_SESSION['user'] -> LastName ?></div>	
					</a>
					
				</li>
				<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsViewEdit') == 1) : ?>
					<li>
						<div class="SettingsDropDown">
							<a href="<?php echo PATH ?>settings">
								<img src="<?php echo PATH ?>public/images/settings.png"  /> Settings
							</a>							
						</div>
					</li>
				<?php endif; ?>
				<li style='padding:13px 0px 11px 15px;'>
					<a href="<?php echo PATH ?>logout">
						<img src="<?php echo PATH ?>public/images/logout.png" data-toggle='tooltip' data-placement='bottom' title='Logout' />
					</a>
				</li>
			</ul>
			
		</div>
		<div style='clear:both'></div>
	</div>	
	
</header>

<div class="SecondaryLinks">
	<div class="linksContainer">
		<a href="<?php echo PATH ?>dashboard">
			<div class="link">
				<img src="<?php echo PATH ?>public/images/dashboard.png" /><span class="labelLink">Dashboard</span>
				<div style="clear:both"></div>
			</div>
		</a>
			
		
		<a href="<?php echo PATH ?>events">
			<div class="link">
				<img src="<?php echo PATH ?>public/images/eventsMenu.png" /><span class="labelLink">Events</span>
				<div style="clear:both"></div>
			</div>	
		</a> 
				
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('PhotoManagementAddEditDelete') == 1) : ?>				
			<a href="<?php echo $this -> pages -> photos() ?>">
				<div class="link">
					<img src="<?php echo PATH ?>public/images/PhotoManagement.png" /><span class="labelLink">Photos</span>
					<div style="clear:both"></div>
				</div>
			</a>
		<?php endif; ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('NewsletterAddEditDelete') == 1) : ?>
			<a href="<?php echo $this -> pages -> newsletters() ?>">
				<div class="link">
					<img src="<?php echo PATH ?>public/images/newsletter.png" /><span class="labelLink">Newsletters</span>
					<div style="clear:both"></div>
				</div>	
			</a>
		<?php endif; ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SpecialsAddEditDelete') == 1) : ?>				
			<a href="<?php echo $this -> pages -> specials() ?>">
				<div class="link">
					<img src="<?php echo PATH ?>public/images/specials.png" /><span class="labelLink">Specials</span>
					<div style="clear:both"></div>
				</div>	
			</a>
		<?php endif; ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('EmployeesAddEditDelete') == 1) : ?>
			<a href="<?php echo $this -> pages -> employees() ?>">
				<div class="link">
					<img src="<?php echo PATH ?>public/images/staff.png" /><span class="labelLink">Employees</span>
					<div style="clear:both"></div>
				</div>		
			</a>
		<?php endif; ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogAddEditDelete') == 1) : ?>
			<a href="<?php echo $this -> pages -> blog() ?>">
				<div class="link">
					<img src="<?php echo PATH ?>public/images/Blog.png" /><span class="labelLink">Blog</span>
					<div style="clear:both"></div>
				</div>	
			</a>
		<?php endif; ?>
		<a href="<?php echo PATH ?>brands">
			<div class="link SubLink">
				<img src="<?php echo PATH ?>public/images/SubLink.png" /><span class="labelLink">Brand<br />Content</span>
				<div style="clear:both"></div>
			</div>	
		</a>
		<a href="<?php echo $this -> pages -> inventory() ?>">
			<div class="link">
				<img src="<?php echo PATH ?>public/images/Inventory.png" /><span class="labelLink">Inventory</span>
				<div style="clear:both"></div>
			</div>	
		</a>
		<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory">
			<div class="link SubLink">
				<img src="<?php echo PATH ?>public/images/SubLink.png" /><span class="labelLink">Current</span>
				<div style="clear:both"></div>
			</div>	
		</a>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventorySpecificationManagement') == 1) : ?>
			<a href="<?php echo $this -> pages -> inventory() ?>/backup/1">
				<div class="link SubLink">
					<img src="<?php echo PATH ?>public/images/SubLink.png" /><span class="labelLink">Specs</span>
					<div style="clear:both"></div>
				</div>	
			</a>
		<?php endif; ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SocialMediaPermissionType') == 0) : ?>
			<a href="<?php echo $this -> pages -> socialmedia() ?>">
				<div class="link">
					<img src="<?php echo PATH ?>public/images/SocialMedia.png" /><span class="labelLink">Social Media</span>
					<div style="clear:both"></div>
				</div>	
			</a>
		<?php endif; ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('leadsViewing') == 0) : ?>
			<a href="<?php echo PATH ?>emaillogs">
				<div class="link">
					<img src="<?php echo PATH ?>public/images/Emails.png" /><span class="labelLink">Leads</span>
					<div style="clear:both"></div>
				</div>	
			</a>
		<?php endif; ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('ShoppingCartAccess') == 1) : ?>
			<a href="<?php echo PATH ?>shop">
				<div class="link">
					<img src="<?php echo PATH ?>public/images/shop.png" /><span class="labelLink">Shop</span>
					<div style="clear:both"></div>
				</div>	
			</a>
		<?php endif; ?>
		<a href="<?php echo PATH ?>communication">
			<div class="link">
				<img src="<?php echo PATH ?>public/images/SMS.png" /><span class="labelLink">Communicate</span>
				<div style="clear:both"></div>
			</div>	
		</a>
		
	</div>
</div>	

<div class="push"></div>
<?php endif; ?>
<?php endif ?>
