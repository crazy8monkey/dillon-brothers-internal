<div class="loginFormWrapper">
	<div class="logo">
		<img src="<?php echo PATH ?>public/images/DillonBrothers.png" />
	</div>
	<div class="header">
		Admininstration Login
	</div>
	<?php echo $this -> form -> ValiationMessage("LoginMessage", "LoginLoading", "LoadingMessage") ?>
	<form action="<?php echo $this -> pages -> login() ?>/userlogin" method="post" id="loginForm">
		<?php echo $this -> form -> Input("text", "Email", "userEmailLogin") ?>
		<?php echo $this -> form -> Input("password", "Password", "userNamePassword") ?>		
		<div class="submitButton">
			<?php echo $this -> form -> Submit("Login", "blueButton") ?>	
		</div>
		<div class='input' style="text-align:center">
			<a href="<?php echo $this -> pages -> login() ?>/forgotpassword">Forgot Password?</a>
		</div>
	</form>
</div>
