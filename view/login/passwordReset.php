<div class="loginFormWrapper">
	<?php if (isset($_GET['token']) && isset($_GET['userID'])) : ?>
		<?php if($_GET['userID'] == $this -> UserSingle -> _userId && $_GET['token'] == $this  -> UserSingle -> securityToken): ?>
		
		<div class="logo">
			<img src="<?php echo PATH ?>public/images/DillonBrothers.png" />
		</div>
		<div class="header">
			Reset Password
		</div>
		<?php echo $this -> form -> ValiationMessage("LoginMessage", "LoginLoading", "LoadingMessage") ?>
		<form action="<?php echo $this -> pages -> login() ?>/NewPasswordPost/<?php echo $_GET['userID'] ?>" id="PasswordResetForm">
			<?php echo $this -> form -> Input("password", "New Password", "userNamePassword") ?>
			<div class="submitButton">
				<?php echo $this -> form -> Submit("Submit", "blueButton") ?>	
			</div>
		</form>
		<div class='input' style="text-align:center">
			<a href="<?php echo $this -> pages -> login() ?>">Back to Login</a>
		</div>
		<?php else: ?>
			This page has been expired
		<?php endif; ?>
	<?php else: ?>
		This page has been expired
	<?php endif; ?>
	
	
</div>

