<div class="loginFormWrapper">
	<div class="logo">
		<img src="<?php echo PATH ?>public/images/DillonBrothers.png" />
	</div>
	<div class="header">
		Forgot Password Tool
	</div>
	<?php echo $this -> form -> ValiationMessage("LoginMessage", "LoginLoading", "LoadingMessage") ?>
	<form action="<?php echo $this -> pages -> login() ?>/emailCheck" id="EmailCheckForm">		
		<div class="input" style="padding-top:10px; padding-bottom:10px;">
			Enter your email address below to reset your password.
		</div>
		<?php echo $this -> form -> Input("text", "Your Email", "userEmail") ?>
		<div class="submitButton">
			<?php echo $this -> form -> Submit("Submit", "blueButton") ?>	
		</div>
	</form>
	<div class='input' style="text-align:center">
			<a href="<?php echo $this -> pages -> login() ?>">Back to Login</a>
	</div>
</div>
