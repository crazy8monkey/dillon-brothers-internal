<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo PATH ?>dashboard">DashBoard</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>socialmedia/store/<?php echo $this -> storeID ?>">Store Progress</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New <?php echo $this -> type ?> Post
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			New <?php echo $this -> type ?> Post
		</div>
	</div>
	<form method="post" action="<?php echo $this -> pages -> socialmedia() ?>/save" id="SocialMediaPostForm">
		<input type="hidden" name="SocialMediaType" value="<?php echo $this -> mediaType; ?>" />
		<input type="hidden" name="storeID" value="<?php echo $this -> storeID ?>" />
		
		<div class="row">
			<div class="col-md-4">
				<?php echo $this -> form -> Input("date", "Post Is Urgent?", "socialMediaUrgent") ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="inputID1">
					<div class="errorMessage"></div>
				</div>
				<?php echo $this -> form -> textarea("Post Content", "socialMediaContent") ?>
			</div>			
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> textarea("Keywords", "socialMediaKeywords") ?>
			</div>			
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit("Save", "greenButton") ?>
			</div>
		</div>
	</form>
</div>



