<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> socialmedia() ?>">Social Media Post List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>View Post
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					View Post
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">
									Post Details
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style='margin-bottom:20px;'>
								<div><strong>Submitted By:</strong> <?php echo $this -> PostSingle -> submittedByFirstName . ' ' . $this -> PostSingle -> submittedByLastName ?></div>
								<?php echo $this -> recordedTime -> formatShortDate($this -> PostSingle -> SubmittedDate) . ' / ' . $this -> recordedTime -> formatTime($this -> PostSingle -> SubmittedTime) ?>
							</div>
						</div>
						<?php if($this -> PostSingle -> IsApproved == 0 && $this -> PostSingle -> IsDeclined == 0 && $this -> PostSingle -> IsPosted == 0): ?>
							<div class="row buttonContainer">
								<div class="col-md-12">
									
									<a href="<?php echo $this -> pages -> socialmedia() ?>/approve/<?php echo $this -> PostSingle -> GetID() ?>">
										<div class="greenButton" style="float:left; margin-right:5px;">
											Approve
										</div>				
									</a>
									<a href="javascript:void(0);" onclick="SocialMediaController.ShowDeclinedPostForm()">
										<div class="redButton" style="float:left">
											Decline
										</div>				
									</a>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">
									Post Content
								</div>
							</div>
						</div>
						<?php if($this -> PostSingle -> IsApproved == 1): ?>
							<div class="row">
								<div class="col-md-12">
									<div class="Approved">Post Approved</div>
								</div>
							</div>
						<?php endif; ?>
						<?php if($this -> PostSingle -> IsDeclined == 1): ?>
							<div class="row">
								<div class="col-md-12">
									<div class="Declined">Post Declined</div>
								</div>
							</div>
						<?php endif; ?>
						<?php if($this -> PostSingle -> IsPosted == 1): ?>
							<div class="row">
								<div class="col-md-12">
									<div class="IsPosted">Marked As Posted</div>
								</div>
							</div>
						<?php endif; ?>
						
						<?php if($this -> PostSingle -> IsUrgent == 1): ?>
							<div class="row">
								<div class="col-md-12" style='color:#c30000; margin-bottom: 10px;'>
									<strong>Urgent:</strong> <?php echo $this -> recordedTime -> formatShortDate($this -> PostSingle -> UrgentDate) ?>
								</div>
							</div>
						<?php endif; ?>
						
						<?php if($this -> PostSingle -> isEditable == 1): ?>
							<form method="post" action="<?php echo $this -> pages -> socialmedia() ?>/save/<?php echo $this -> PostSingle -> GetID() ?>" id="SocialMediaPostForm">
								<div class="row">
									<div class="col-md-12">
										<div id="inputID1">
											<div class="errorMessage"></div>
										</div>
										<?php echo $this -> form -> textarea("Post Content", "socialMediaContent", 1, $this -> PostSingle -> PostContent) ?>
									</div>			
								</div>
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> textarea("Keywords", "socialMediaKeywords", NULL, $this -> PostSingle -> KeyWords) ?>
									</div>			
								</div>
								<div class="row">
									<div class="col-md-12">
										<?php echo $this -> form -> Submit("Save Draft", "whiteButton") ?>
									</div>
								</div>
							</form>
						<?php else: ?>
							<div style='border-top: 1px solid #dadada; margin-bottom: 10px;'></div>
							<div class="row">
								<div class="col-md-12">
									<div style="margin-bottom:10px;">
										<div style="font-size:16px; font-weight:bold">Post Content</div>
										<?php echo $this -> PostSingle -> PostContent ?>					
									</div>
									<?php if(!empty($this -> PostSingle -> KeyWords)) : ?>
										<div style="margin-bottom:10px;">
											<div style="font-size:16px; font-weight:bold">Keywords</div>
											<?php echo $this -> PostSingle -> KeyWords ?>					
										</div>
									<?php endif; ?>
									
									<div style="margin-bottom:10px;">
										<div style="font-size:16px; font-weight:bold"><?php echo $this -> PostSingle -> ApprovalDeclineStatus ?> By:</div>
										<?php echo $this -> PostSingle -> ApprovalDeclineByPerson ?><br /> 
										<?php echo $this -> recordedTime -> formatShortDate($this -> PostSingle -> ApprovalDeclineDate) ?> / <?php echo $this -> recordedTime -> formatTime($this -> PostSingle -> ApprovalDeclineTime) ?>
									</div>
									
									<?php if($this -> PostSingle -> IsDeclined == 0): ?>
										<?php if($this -> PostSingle -> IsPosted == 0) : ?>
											<div style="margin-bottom:10px;">
												<a href="<?php echo $this -> pages -> socialmedia() ?>/markasposted/<?php echo $this -> PostSingle -> GetID() ?>">
													<div class="blueButton" style="float:left;">
														Mark as Posted
													</div>
												</a>
											</div>
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="DeclinePostPopup" style='display:none;'>
	<a href="javascript:void(0);" onclick="SocialMediaController.CloseDeclinedPostForm()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">
			Decline Post			
			<a href="javascript:void(0);" onclick="SocialMediaController.CloseDeclinedPostForm()">
				<div class="close" style="margin-top: 4px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>	
		</div>
		<div class="FormContent" style='margin-right:15px;'>
			<div style='margin-bottom:10px;'>
				What is being entered will be emailed to the user who submitted this post
			</div>
			<form action="<?php echo $this -> pages -> socialmedia() ?>/decline/<?php echo $this -> PostSingle -> GetID() ?>" method="post" id="DeclinePostForm">
				<input type="hidden" name="socialMediaType" value="<?php echo $this -> PostSingle -> SocialMediaType ?>" />
				<div id="inputIDDecline">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> textarea("Decline Notes", "declineNotes", 1) ?>
				</div>
				
				<?php echo $this -> form -> Submit("Decline Post", "redButton") ?>
				
			</form>
						
		</div>
	</div>	
</div>



