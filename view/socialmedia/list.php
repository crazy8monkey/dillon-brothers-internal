<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Social Media Posts
				</div>
			</div>
		</div>
	</div>
</div>


<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='font-size:16px; font-weight:bold; margin-bottom:10px;'>New Posts</div>	
						<?php foreach($this -> newposts as $newpost) : ?>
							<a href='<?php echo $this -> pages -> socialmedia() ?>/view/post/<?php echo $newpost['socialID'] ?>'>
								<div class="postSingleElement">
									<?php if($newpost['isUrgent'] == 1): ?>
										<div class="UrgentIndicator">
											Urgent: <?php echo $this -> recordedTime -> formatShortDate($newpost['isUrgentDate']) ?>
										</div>
									<?php endif; ?>
									<?php 
									$icon = NULL;
									if($newpost['socialMediaType'] == 1) {
										$icon = "facebookIcon.png";
									} else if($newpost['socialMediaType'] == 2) {
										$icon = "twitter.png";
									} ?>
									<div class="row">
										<div class="col-md-2">
											<div class="socialMediaIcon">										
												<img src="<?php echo PATH ?>public/images/<?php echo $icon ?>" />	
											</div>
										</div>
										<div class="col-md-10">
											<div class="SubmittedByContent" style='margin-top:5px;'>
												<div><strong>Submitted By:</strong> <?php echo $newpost['firstName'] . ' ' . $newpost['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($newpost['submittedDate']) . ' / ' . $this -> recordedTime -> formatTime($newpost['time']) ?></div>
											</div>
											<div class="storeName">
												<?php echo $newpost['StoreName'] ?>	
											</div>
										</div>
									</div>				
									
								</div>
							</a>
						<?php endforeach; ?>	
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='font-size:16px; font-weight:bold; margin-bottom:10px;'>Approved</div>
						<?php foreach($this -> approvedposts as $approvedpost) : ?>
							<a href='<?php echo $this -> pages -> socialmedia() ?>/view/post/<?php echo $approvedpost['socialID'] ?>'>
								<div class="postSingleElement approvedPost">
									<?php if($approvedpost['isUrgent'] == 1): ?>
										<div class="UrgentIndicator">
											Urgent: <?php echo $this -> recordedTime -> formatShortDate($approvedpost['isUrgentDate']) ?>
										</div>
									<?php endif; ?>
									<?php 
									$icon = NULL;
									if($approvedpost['socialMediaType'] == 1) {
										$icon = "facebookIcon.png";
									} else if($approvedpost['socialMediaType'] == 2) {
										$icon = "twitter.png";
									} ?>
									<div class="row">
										<div class="col-md-2">
											<div class="socialMediaIcon">										
												<img src="<?php echo PATH ?>public/images/<?php echo $icon ?>" />	
											</div>
										</div>
										<div class="col-md-10">
											<div class="SubmittedByContent" style='margin-top:5px;'>
												<div><strong>Submitted By:</strong> <?php echo $approvedpost['firstName'] . ' ' . $approvedpost['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($approvedpost['submittedDate']) . ' / ' . $this -> recordedTime -> formatTime($approvedpost['time']) ?></div>
											</div>
											<div class="storeName">
												<?php echo $approvedpost['StoreName'] ?>	
											</div>
										</div>
									</div>				
									
								</div>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
				
			</div>
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='font-size:16px; font-weight:bold; margin-bottom:10px;'>Declined</div>
						<?php foreach($this -> declinedPosts as $declinedpost) : ?>
							<a href='<?php echo $this -> pages -> socialmedia() ?>/view/post/<?php echo $declinedpost['socialID'] ?>'>
								<div class="postSingleElement declinedPost">
									<?php if($newpost['isUrgent'] == 1): ?>
										<div class="UrgentIndicator">
											Urgent: <?php echo $this -> recordedTime -> formatShortDate($declinedpost['isUrgentDate']) ?>
										</div>
									<?php endif; ?>
									<?php 
									$icon = NULL;
									if($declinedpost['socialMediaType'] == 1) {
										$icon = "facebookIcon.png";
									} else if($declinedpost['socialMediaType'] == 2) {
										$icon = "twitter.png";
									} ?>
									<div class="row">
										<div class="col-md-2">
											<div class="socialMediaIcon">										
												<img src="<?php echo PATH ?>public/images/<?php echo $icon ?>" />	
											</div>
										</div>
										<div class="col-md-10">
											<div class="SubmittedByContent" style='margin-top:5px;'>
												<div><strong>Submitted By:</strong> <?php echo $declinedpost['firstName'] . ' ' . $declinedpost['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($declinedpost['submittedDate']) . ' / ' . $this -> recordedTime -> formatTime($declinedpost['time']) ?></div>
											</div>
											<div class="storeName">
												<?php echo $declinedpost['StoreName'] ?>	
											</div>
										</div>
									</div>				
									
								</div>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='font-size:16px; font-weight:bold; margin-bottom:10px;'>Posted</div>
						<?php foreach($this -> postedposts as $postedposts) : ?>
							<a href='<?php echo $this -> pages -> socialmedia() ?>/view/post/<?php echo $postedposts['socialID'] ?>'>
								<div class="postSingleElement">
									<?php if($newpost['isUrgent'] == 1): ?>
										<div class="UrgentIndicator">
											Urgent: <?php echo $this -> recordedTime -> formatShortDate($postedposts['isUrgentDate']) ?>
										</div>
									<?php endif; ?>
									<?php 
									$icon = NULL;
									if($postedposts['socialMediaType'] == 1) {
										$icon = "facebookIcon.png";
									} else if($declinedpost['socialMediaType'] == 2) {
										$icon = "twitter.png";
									} ?>
									<div class="row">
										<div class="col-md-2">
											<div class="socialMediaIcon">										
												<img src="<?php echo PATH ?>public/images/<?php echo $icon ?>" />	
											</div>
										</div>
										<div class="col-md-10">
											<div class="SubmittedByContent" style='margin-top:5px;'>
												<div><strong>Submitted By:</strong> <?php echo $postedposts['firstName'] . ' ' . $postedposts['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($postedposts['submittedDate']) . ' / ' . $this -> recordedTime -> formatTime($postedposts['time']) ?></div>
											</div>
											<div class="storeName">
												<?php echo $postedposts['StoreName'] ?>	
											</div>
										</div>
									</div>				
									
								</div>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
				
			</div>
		</div>

		
	</div>
</div>


