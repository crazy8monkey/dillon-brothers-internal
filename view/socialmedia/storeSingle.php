<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>dashboard">Dashboard</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Store Progress
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					<?php echo $this -> storeSingleInfo -> storeName ?>: Social Media Posts
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div style="font-size: 24px;  margin-bottom: 10px;">
									<strong><?php echo ceil($this -> storeSingleInfo -> weeklyProgress * 100) ?>%</strong>
								</div>
								<div style='width:100%; margin-top:0px; margin-bottom:10px;' class="progressBar">
									<div style='width:<?php echo ceil($this -> storeSingleInfo -> weeklyProgress * 100)  ?>%' class="progressContent"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='text-align:center'>
							<img src="<?php echo PATH ?>public/images/facebookIcon.png" />
							<div style='font-size:24px; margin-top:20px;'>
								<?php echo count($this -> totalSubmittedFacebook) ?>/<?php echo $this -> requiredFacebook ?>
							</div>
						</div>
						<a href="<?php echo PATH ?>socialmedia/addpost/<?php echo $this -> storeSingleInfo -> GetID() ?>/facebook">
							<div class="greenButton" style='text-align:center;'>
								Add Facebook Post
							</div>	
						</a>
						<?php if(count($this -> totalSubmittedFacebook) > 0) : ?>
							<div style='margin-top:30px;'>Submitted Facebook posts this week</div>
							<?php foreach($this -> totalSubmittedFacebook as $facebookSingle) : ?>
								<div class="row">
									<div style='border-top:1px solid #e4e4e4; padding: 3px; margin: 5px 15px 2px 15px;'></div>
									<div class="col-md-6">
										<?php echo $this -> tooltip('Submitted By ' . $facebookSingle['firstName'] . ' ' . $facebookSingle['lastName']) ?> <a href="javascript:void(0)" onclick="SocialMediaController.ShowSubmittedPost(<?php echo $facebookSingle['socialID'] ?>)">View</a>
									</div>
									<div class="col-md-6" style='text-align:right'>
										<?php echo $this -> recordedTime -> formatShortDate($facebookSingle['submittedDate']) . ' / ' . $this -> recordedTime -> formatTime($facebookSingle['time']) ?>
									</div>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='text-align:center'>
							<img src="<?php echo PATH ?>public/images/twitter.png" />
							<div style='font-size:24px; margin-top:20px;'>
								<?php echo count($this -> totalSubmittedTwitter) ?>/<?php echo $this -> requiredTwitter ?>
							</div>
						</div>
						<a href="<?php echo PATH ?>socialmedia/addpost/<?php echo $this -> storeSingleInfo -> GetID() ?>/twitter">
							<div class="greenButton" style='text-align:center;'>
								Add Twitter Post
							</div>
						</a>
						<?php if(count($this -> totalSubmittedTwitter) > 0) : ?>
							<div style='margin-top:30px;'>Submitted Twitter posts this week</div>
							<?php foreach($this -> totalSubmittedTwitter as $twitterSingle) : ?>
								<div class="row">
									<div style='border-top:1px solid #e4e4e4; padding: 3px; margin: 5px 15px 2px 15px;'></div>
									<div class="col-md-6">
										<?php echo $this -> tooltip('Submitted By ' . $twitterSingle['firstName'] . ' ' . $twitterSingle['lastName']) ?> <a href="javascript:void(0)" onclick="SocialMediaController.ShowSubmittedPost(<?php echo $twitterSingle['socialID'] ?>)">View</a>
									</div>
									<div class="col-md-6" style='text-align:right'>
										<?php echo $this -> recordedTime -> formatShortDate($twitterSingle['submittedDate']) . ' / ' . $this -> recordedTime -> formatTime($twitterSingle['time']) ?>
									</div>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="ShowSinglePost" style='display:none;'>
	<a href="javascript:void(0);" onclick="SocialMediaController.CloseSubmittedPost()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">
			View Submitted Post			
			<a href="javascript:void(0);" onclick="SocialMediaController.CloseSubmittedPost()">
				<div class="close" style="margin-top: 4px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>	
		</div>
		<div class="FormContent" style='overflow-y: auto; max-height: 300px;'>
			<div style='font-size:16px; font-weight:bold;'>Post Content</div>
			<div id="PostContent" style='margin-bottom:10px;'></div>
			<div id="ShowKeyWordContent" style='display:none; margin-bottom:10px'>
				<div style='font-size:16px; font-weight:bold;'>Key words</div>
				<div id="KeyWords" style='margin-bottom:10px;'></div>	
			</div>	
			<div style='font-size:16px; font-weight:bold;'>Submitted By</div>
			<div id="SubmittedByContent" style='margin-bottom:10px;'></div>					
		</div>
	</div>	
</div>


