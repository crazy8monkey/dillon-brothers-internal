<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>dashboard">Dashboard</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Store Progress
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					<?php echo $this -> storeSingleInfo -> storeName ?>: Social Media Posts
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='font-size: 24px;  margin-bottom: 10px;'>
							<strong><?php echo ceil($this -> storeSingleInfo -> weeklyProgress * 100) ?>%</strong> | <a href="<?php echo $this -> pages -> socialmedia() ?>">View All Submitted Posts</a>
						</div>
						<div style='width:100%; margin-top:0px;margin-bottom:10px;' class="progressBar">
							<div style='width:<?php echo ceil($this -> storeSingleInfo -> weeklyProgress * 100)  ?>%' class="progressContent"></div>
						</div>							
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='text-align:center'>
							<img src="<?php echo PATH ?>public/images/facebookIcon.png" />
							<div style='font-size:24px; margin-top:20px;'>
								<?php echo count($this -> totalSubmittedFacebook) ?>/<?php echo $this -> requiredFacebook ?>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div class="col-md-6">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='text-align:center'>
							<img src="<?php echo PATH ?>public/images/twitter.png" />
							<div style='font-size:24px; margin-top:20px;'>
								<?php echo count($this -> totalSubmittedTwitter) ?>/<?php echo $this -> requiredTwitter ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




