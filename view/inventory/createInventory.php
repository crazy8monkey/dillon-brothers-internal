<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> inventory() ?>">Inventory Portal</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create Inventory										
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
		<div class="row">
			<form method="post" action="<?php echo $this -> pages -> inventory() ?>/save/dummyinventory" id="SingleInventory">
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Create Vehicle Inventory
							</div>
						</div>
						
							<div class="row">	
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
											<div class="input">
												<div id="inputID5">		
													<div class="errorMessage"></div>										
													<div class="inputLabel">Category</div>
													<select name="InventoryCategoryID">
														<option value='0'>Select Category</option>
														<?php foreach($this -> inventorycategories as $categorySingle): ?>
															<optgroup label="<?php echo $categorySingle['ParentCategory']?>">
																<?php 
																	$subCategoriesText = explode(",", $categorySingle['SubCategories']); 
																	$subCategoryIDS = explode(",", $categorySingle['SubCategoryIDS']); 
																?>
																<?php foreach($subCategoriesText as $key => $singleCategory): ?> 
																	<option value="<?php echo $subCategoryIDS[$key] ?>"><?php echo $subCategoriesText[$key] ?></option>
																<?php endforeach; ?>
															</optgroup>	
														<?php endforeach; ?>	
													</select>
												</div>
											</div>
										</div>
									</div>
										
									<div class="row">
										<div class="col-md-6">
											<div id="inputID1">
												<div class="errorMessage"></div>
												<?php echo $this -> form -> Input("text", "MSRP", "inventoryMSRP", 1) ?>
												</div>
											</div>
											<div class="col-md-6">
												<div id="inputID5">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Mileage", "inventoryMileage", 5) ?>													
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-3">
												<div id="inputID2">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Year", "inventoryYear", 2) ?>
												</div>
											</div>
											<div class="col-md-3">
												<div id="inputID3">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Manufacturer", "inventoryManufactur", 3) ?>
												</div>
											</div>
											<div class="col-md-6">
												<div id="inputID4">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Model", "inventoryModel", 4) ?>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<?php echo $this -> form -> Input("text", "Friendly Model Name", "inventoryModelFriendlyName", NULL) ?>
											</div>
										</div>
										
										
									</div>
									
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<div class="errorMessage"></div>
												<?php echo $this -> form -> Input("text", "Overlay Text", "inventoryOverlayText") ?>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="errorMessage"></div>
												<div class="input">
													<div class="inputLabel">Youtube Video ID</div>
													<div class="descriptiveInfo">
														If you want to display this Youtube video on this URL -> https://www.youtube.com/watch?v=<strong>XsZKrctSDaw</strong><br />
														The value you need to enter put in this input is value -> <strong>XsZKrctSDaw</strong>
													</div>
													<input id="inventoryYoutubeURL" class="inventoryYoutubeURL" type="text" name="inventoryYoutubeURL" placeholder='i.e. vfWzyEWFu3I' onchange="Globals.removeValidationMessage(6)">
												</div>
												
											</div>
										</div>
								</div>
								
								</div>
								<div class="row">
									<div class="col-md-12">
										<?php echo $this-> form -> textarea("Inventory Description", "inventoryDescription", NULL) ?>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12 sectionHeader">
										Filter Options Information
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div id="inputID7">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Engine Size In CC", "inventoryEngineSizeCC", 7, 0) ?>
										</div>
										
									</div>
								</div>
																
								<div style='position: fixed; bottom: 0px; background: white; left: 0px; right:0px; box-shadow: 0px 1px 13px #b3b3b3;'>
									<div class="row">
										<div class="col-md-12">
											<div class="ContentPage">
												<div style='padding:15px 10px'>
													<input type="submit" value="Save Inventory" class="greenButton" style='font-size: 16px; font-weight: normal;'>	
												</div> 			
											</div>				
										</div>
									</div>				
								</div>			
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							
							<div class="col-md-6">
								<div id="inputID6">
									<div class="errorMessage"></div>
									<div class="input">
										<div class="inputLabel">Location</div>
										<select name='storeLocation'>
											<option value='0'>Select Store</option>
											<?php foreach($this -> stores as $storeSingle): ?>
												<option value='<?php echo $storeSingle['storeID'] ?>'><?php echo $storeSingle['StoreName'] ?></option>
											<?php endforeach; ?>
										</select>	
									</div>
									
								</div>
							</div>
							<div class="col-md-6">
								<div id="inputID8">
									<div class="errorMessage"></div>
									<?php echo $this -> form -> Input("text", "Color", "inventoryColorString", 8) ?>
								</div>
							</div>
						</div>
						<div class="row">
							
							<div class="col-md-6">
								<div id="inputID9">
									<div class="errorMessage"></div>
									<div class="input">
										<div class="inputLabel">Conditoin</div>
										<select name='condition'>
											<option value='none'>Select Condition</option>
											<option value='0'>New</option>
											<option value='1'>Used</option>
										</select>	
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="container">
	
	
	
</div>




