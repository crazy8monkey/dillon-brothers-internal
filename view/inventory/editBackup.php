<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> inventory() ?>">Inventory Portal</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> inventory() ?>/backup?page=1">Inventory Specificaitons Backup</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Backup
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid" style='padding-bottom: 80px;'>
		<div class="row">
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Edit Backup Vehicle/Inventory
							</div>
						</div>
						
						<?php if(count($this -> logs) > 0) : ?>
							<div class="row">
								<div class="col-md-12" style='margin-bottom:20px;'>
									<?php 
										$editedTime = explode('T', $this ->  logs[count($this -> logs) - 1]['edittedTimeAndDate']);
									?>
									Last Edited by <?php echo $this -> logs[count($this -> logs) - 1]['firstName'] . ' ' . $this -> logs[count($this -> logs) - 1]['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1]) ?> <a href="javascript:void(0);" onclick="InventoryController.ViewBackupInventoryHistory(<?php echo $this -> BackupInventorySingle -> GetID() ?>)">View History</a>
									
								</div>
							</div>
						<?php endif; ?>	
						
						<?php if(count($this -> BackupInventorySingle -> CurrentInventoryUsing) > 0) : ?>
							<div class="row">
								<div class="col-md-12">
									<strong>Current Related Inventory</strong>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style='margin-bottom:20px;'>
									<?php $stockNumTotal = NULL;
									foreach($this -> BackupInventorySingle -> CurrentInventoryUsing as $currentInventory) {
										$stockNumTotal .= "<a href='" . $this -> pages -> inventory() . "/edit/" . $currentInventory['inventoryID'] . "'>" . $currentInventory['Stock'] . '</a>, ';		
									}
									echo rtrim($stockNumTotal, ', ');
									?>
									
									
								</div>
							</div>
						<?php endif; ?>	
						
						<form method="post" action="<?php echo $this -> pages -> inventory() ?>/save/backup/<?php echo $this -> BackupInventorySingle -> GetID() ?>">
							<div class="row">
								<div class="col-md-6">
									<div class="input <?php if($this -> BackupInventorySingle -> Category == 0) : ?> needFilling <?php endif; ?>">
										<?php if($this -> BackupInventorySingle -> Category == 0) : ?> 
											<div style="color:#c30000; margin-bottom:10px;">
												This needs to be filled out
											</div>	
										<?php endif; ?>
										<div class="inputLabel">Category</div>
										<select name="InventoryCategoryID">
											<?php foreach($this -> inventorycategories as $categorySingle): ?>
												<optgroup label="<?php echo $categorySingle['ParentCategory']?>">
													<?php 
											
										
														$subCategoriesText = explode(",", $categorySingle['SubCategories']); 
														$subCategoryIDS = explode(",", $categorySingle['SubCategoryIDS']); 
													?>
													<?php foreach($subCategoriesText as $key => $singleCategory): ?> 
														<option value="<?php echo $subCategoryIDS[$key] ?>" <?php if($subCategoryIDS[$key] == $this -> BackupInventorySingle -> Category): ?> selected <?php endif; ?>><?php echo $subCategoriesText[$key] ?></option>
													<?php endforeach; ?>
												</optgroup>
												
												
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<?php echo $this -> form -> Input("text", "Engine Size CC (for related inventory purposes)", "inventoryEngineSizeCC", 1, $this -> BackupInventorySingle -> enginSizeCC) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<?php echo $this -> form -> Input("text", "Year", "inventoryYear", 1, $this -> BackupInventorySingle -> inventoryBackupYear) ?>
								</div>
								<div class="col-md-6">
									<?php echo $this -> form -> Input("text", "Manufacturer", "inventoryManufactur", 2, $this -> BackupInventorySingle -> inventoryBackupManufactur) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<?php echo $this -> form -> Input("text", "Model", "inventoryModel", 3, $this -> BackupInventorySingle -> inventoryBackupModelName) ?>
								</div>
								<div class="col-md-6">
									<?php echo $this -> form -> Input("text", "Friendly Model Name", "inventoryModelFriendlyName", 3, $this -> BackupInventorySingle -> inventoryBackupModelFriendlyName) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this-> form -> textarea("Inventory Description", "inventoryBackupDescription", NULL, $this -> BackupInventorySingle -> inventoryBackupDescription) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 sectionHeader">
									Specifications
								</div>
							</div>
							<?php $specDecoded = json_decode($this -> BackupInventorySingle -> GetSpecs(), true); ?>
							<input type="hidden" id="SpecJSON" name="SpecJSON" value='<?php echo $this -> BackupInventorySingle -> GetSpecs()?>' />
							
							<?php foreach($this -> SpecsList as $key => $spec): ?>
								
								<?php 
									$showHeader = false;
									$associatedLabels = array();
									
									if($spec['LabelsByGroup'] != NULL) {
										$LabelIDS = explode(",", $spec['LabelsByGroup']);
										$labelNames = explode(" / ", $spec['GrabbedLabelNames']);
										$labelOrders = explode(",", $spec['LabelOrders']);
														
										foreach($LabelIDS as $key => $label) {
											array_push($associatedLabels, array("specLabelID" => $LabelIDS[$key],
																				"labelText" => $labelNames[$key],
																				"labelOrder" => $labelOrders[$key]));
										}
														
														
														
										usort($associatedLabels, function($a, $b) {
											return $a['labelOrder'] - $b['labelOrder'];
										});									
									}
									
									$categoryArrayCheck = array_column($associatedLabels, 'labelText');
									
									$categoryIDS = array();
									
									foreach($categoryArrayCheck as $getCategoryIDS) {
										$getIDS = explode(" - ", $getCategoryIDS);	
										if(count($getIDS) > 1) {
											$specLabelsHeaderCheck = explode(",", $getIDS[2]);
											
											if(array_search($this -> BackupInventorySingle -> Category, $specLabelsHeaderCheck) !== false) {
												$showHeader = true;
												break;
											}				
										}
					
										
									}
					
									
									
								?>
								
								
								<?php if($showHeader == true): ?>
								<div class="row">
									<div class="col-md-12 SpecGroupHeader">
										<?php echo $spec['specGroupText'] ?>
									</div>
								</div>
								<?php endif; ?>
								
								
													
								<?php if(count($associatedLabels)> 0) :?>
									<?php foreach ($associatedLabels as $label) :?>	
										<?php 
											$relatedCategoryLabel = explode(" - ", $label['labelText']);
										?>
										
										<?php if(count($relatedCategoryLabel) > 1) :?>
					      					<?php $specLabels = explode(",", $relatedCategoryLabel[2]); ?>
					      					
					      					<?php if(array_search($this -> BackupInventorySingle -> Category, $specLabels) !== false): ?>								
											<div class="row" style="margin-bottom:15px;">
												<div class="col-md-2" style='margin-top: 5px;'>
													<?php echo $relatedCategoryLabel[0] ?>
												</div>					
												<div class="col-md-10">
													<?php 
														$inputValue = NULL;
														$key = array_search($label["specLabelID"], array_column($specDecoded, 'LabelID'));
													
														if($key !== false) {
															$inputValue = $specDecoded[$key]['Content'];
														}
													
													
													 ?>
													
													<input type="text" placeholder="<?php echo $relatedCategoryLabel[0] ?>" value="<?php echo $inputValue ?>"  onkeyup="InventoryController.EditLabelContent(this, <?php echo $label['specLabelID'] ?>)">
												</div>
												
											</div>
											<?php endif; ?>
										<?php endif; ?>
									<?php endforeach; ?>
								<?php endif; ?>
								
								
							<?php endforeach; ?>
							
							
							<div class="row">
								<div class="col-md-12 sectionHeader" style="margin-top:20px;">
									Associated VINS 
									<a href='javascript:void(0)' onclick='InventoryController.ManageVINNumbers()'>
										<div class="whiteButton" data-placement="bottom" title="Manage VIN(s)" data-toggle="tooltip">
											<i class="fa fa-pencil" aria-hidden="true"></i>
										</div>	
									</a>
									
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									
									<?php 
									$vinsTotal = NULL;
									foreach($this -> vins as $vin) {
										$vinsTotal .= $vin['backedupVinNumber'] . ', ';		
									}
									echo rtrim($vinsTotal, ', ');
									?>
										
									
					
								</div>
							</div>
					
							<div style='position: fixed; bottom: 0px; background: white; left: 0px; right:0px; box-shadow: 0px 1px 13px #b3b3b3;'>
										<div class="row">
											<div class="col-md-12">
												<div class="ContentPage">
													<div style='padding:15px 10px'>
														<input type="submit" value="Save" class="greenButton" style="font-size: 16px; font-weight: normal;">
													</div>
												</div>
											</div>
										</div>
								</div>
						
						</form>
						
						
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='float:left;'>
							<a href="javascript:void(0)" onclick="InventoryController.OpenDeleteBikePopup(<?php echo $this -> BackupInventorySingle -> GetID() ?>);"><i style='font-size: 26px;' class="fa fa-trash" aria-hidden="true"></i></a>	
						</div>
						<div style='margin-left:30px;'>
							<strong>Warning!!</strong><br />Deleting bike inventory specs will lose that mapping functionality to assist and maintain our current inventory	
						</div>
						
						<div style='clear:both'></div>
					</div>
				</div>
				<div class="WhiteSectionDiv">
					<div class="content">
						
						<div style='float:left'>
							<input id="VerifiedVehicleSpecs" type='checkbox' <?php if($this -> BackupInventorySingle -> vehicleVerified == 1): ?>checked<?php endif; ?> onclick="InventoryController.VehicleVerifiedToggle(this, <?php echo $this -> BackupInventorySingle -> GetID() ?>)" name="VerifiedVehicleSpecs" />
							<label for="VerifiedVehicleSpecs"></label>	
						</div>
						<?php 
						if($this -> BackupInventorySingle -> vehicleVerified == 1) {
							$displayText = 'display:block;';	
						} else {
							$displayText = 'display:none;';
						}
						
						?>
						<div id="vehicleVerifiedText" style='float:left; font-size: 18px; margin-left: 10px; <?php echo $displayText ?>'>
							Vehicle Has Been Verified
						</div>
						
						<div style='clear:both'></div>
					</div>
				</div>
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Internal Feedback
							</div>
						</div>
						<form method="post" id="SpecInventoryFeedBack" action="<?php echo PATH ?>settings/feedback/<?php echo $this -> BackupInventorySingle -> GetID() ?>">
							<div class="row">
								<div class="col-md-12">
									<div id="FeedBackID1">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Notes</div>
											<textarea id="internalFeedBackNotes" name="internalFeedBackNotes" onchange="Globals.removeFeedBackValidationMessage('FeedBackID1')"></textarea>
										</div>
																										
									</div>
									
									
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Save Notes" class="greenButtonRectangle">
								</div>
							</div>
						</form>
					</div>
					<div class="content" style='border-top: 1px solid #e0e0e0;'>
						<?php if(count($this -> feedBackList) > 0): ?>
							<div id="FeedBackList">
								<?php foreach($this -> feedBackList as $item): ?>
									<div class="FeedBackItem">
										<div class="bubble">
											<?php echo $item['FeedbackText'] ?>
											<div class="caret"></div>
										</div>
										<div class="detailsBy">
											<?php $submittedDetails = explode('T', $item['feedBackSubmitted']); ?>
											<?php echo $item['firstName'] ?> <?php echo $item['lastName'] ?> | <?php echo $this -> recordedTime -> formatDate($submittedDetails[0]) ?> - <?php echo $this -> recordedTime -> formatTime($submittedDetails[1]) ?>
										</div>
									</div>
								<?php endforeach; ?>
								
								
							</div>
						<?php else: ?>
							<div id="FeedBackList">There is no feed back on this item</div>
						<?php endif; ?>	
						
						<div class="FeedBackItem" id="newFeedBackItem" style='display:none;'>
							<div class="bubble"></div>
							<div class="detailsBy"></div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="BackupHistoryPopup" style="display:none">
	<?php echo $this -> PopupReadOnly('Inventory Backup History', 'InventoryController.closeBackupHistory()')?>
</div>

<div id="DeleteBackupPopup" style="display:none">
	<a href="javascript:void(0);" onclick="InventoryController.CloseDeleteBikePopup()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">Delete Bike Backup?
			<a href="javascript:void(0);" onclick="InventoryController.CloseDeleteBikePopup()">
				<div class="close" style="margin-top: 4px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>	
		</div>
		<div class="FormContent" style="margin-right: 15px; overflow-y: auto; height: 370px;">
			<form method="post" id="DeleteBackupForm" action="<?php echo PATH ?>inventory/delete/inventorybackup/<?php echo $this -> BackupInventorySingle -> GetID() ?>">
				<div style='margin-bottom:10px;'>Please Reassign (or remove) associated vins to this backup<br /> <strong>(This action cannot be undone!)</strong></div>
				<div id="inputID99">
					<div class="errorMessage"></div>
				</div>
				<?php foreach($this -> vins as $vin) : ?>
					<?php echo $vin['backedupVinNumber'] ?>
					<select name='reassignVin[]'>
						<option value='0'>Please Select Choice</option>
						<option value='-1'>Remove Vin</option>
						<option value='-2'>No Assigned Vehicle</option>
						<?php 
							$manufactures = array_column($this -> backupList, 'inventoryBackupManufactur');
							$ManufactureArray = array_unique($manufactures, SORT_REGULAR);
							sort($ManufactureArray);
						?>
						<?php foreach($ManufactureArray as $manufactureSingle): ?>
							<optgroup label='<?php echo $manufactureSingle ?>'>
								<?php 
									
									
									$filteredItems = array_filter($this -> backupList, function ($var) use($manufactureSingle) {
										return $var['inventoryBackupManufactur'] == $manufactureSingle; 
									});
								?>
								
								<?php foreach($filteredItems as $backupItemSingle): ?>
									<?php $bikeName = $backupItemSingle['inventoryBackupYear'] . ' '. $backupItemSingle['inventoryBackupManufactur'] . ' '. $backupItemSingle['inventoryBackupModelName'] . ' '. $backupItemSingle['inventoryBackupModelFriendlyName'] ?>
									<option value='<?php echo $backupItemSingle['inventoryBackupID'] ?>'><?php echo $bikeName ?></option>
								<?php endforeach; ?>		
							</optgroup>
						<?php endforeach; ?>
						
						
					</select>
				<?php endforeach; ?>
				<div style='margin-top:10px;'>
					<input type='submit' value='Delete Backup Vehicle' class="greenButtonRectangle" />	
				</div>
				
			</form>
		</div>
	</div>
</div>

<div id="ManageVinNumbers" style='display:none;'>
	<a href="javascript:void(0);" onclick="InventoryController.CloseManageVINNumbers()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">Manage VIN Numer(s)
			<a href="javascript:void(0);" onclick="InventoryController.CloseManageVINNumbers()">
				<div class="close" style="margin-top: 4px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>	
		</div>
		<div class="FormContent" style="margin-right: 15px; overflow-y: auto; height: 370px;">
			<form method="post" id="ReassignVinForm" action="<?php echo PATH ?>inventory/save/vinnumberassign/<?php echo $this -> BackupInventorySingle -> GetID() ?>">
				<div style='margin-bottom:10px;'>Please Reassign associated vins to this backup<br /> <strong>(This action cannot be undone!)</strong></div>
				<div id="inputID99">
					<div class="errorMessage"></div>
				</div>
				<?php foreach($this -> vins as $vin) : ?>
					<div  style='margin-bottom:10px;'>
						<?php echo $vin['backedupVinNumber'] ?>
						<select name='reassignVinDecision[]'>
							<option value='0'>Please Select Choice</option>
							<option value='-1'>Leave as is</option>
							<option value='-2'>No Assigned Vehicle</option>
							<?php 
								$manufactures = array_column($this -> backupList, 'inventoryBackupManufactur');
								$ManufactureArray = array_unique($manufactures, SORT_REGULAR);
								sort($ManufactureArray);
							?>
							<?php foreach($ManufactureArray as $manufactureSingle): ?>
								<optgroup label='<?php echo $manufactureSingle ?>'>
									<?php 
										
										
										$filteredItems = array_filter($this -> backupList, function ($var) use($manufactureSingle) {
											return $var['inventoryBackupManufactur'] == $manufactureSingle; 
										});
									?>
									
									<?php foreach($filteredItems as $backupItemSingle): ?>
										<?php $bikeName = $backupItemSingle['inventoryBackupYear'] . ' '. $backupItemSingle['inventoryBackupManufactur'] . ' '. $backupItemSingle['inventoryBackupModelName'] . ' '. $backupItemSingle['inventoryBackupModelFriendlyName'] ?>
										<option value='<?php echo $backupItemSingle['inventoryBackupID'] ?>'><?php echo $bikeName ?></option>
									<?php endforeach; ?>		
								</optgroup>
							<?php endforeach; ?>
						</select>	
					</div>
					
				<?php endforeach; ?>
				<div>
					<input type='submit' value='Save Vin Numbers' class="greenButtonRectangle" />	
				</div>
				
			</form>
		</div>
	</div>
</div>


