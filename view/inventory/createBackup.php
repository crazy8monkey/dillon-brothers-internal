<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> inventory() ?>">Inventory Portal</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> inventory() ?>/backup?page=1">Inventory Specificaitons Backup</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New Backup
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid" style='padding-bottom: 80px;'>
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="container">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									New Backup Vehicle/Inventory
								</div>
							</div>
							<form method="post" action="<?php echo $this -> pages -> inventory() ?>/save/backup">
								<div class="row">
									<div class="col-md-6">
										<div class="input">
											<div class="inputLabel">Category</div>
											<select name="InventoryCategoryID">
												<?php foreach($this -> inventorycategories as $categorySingle): ?>
													<optgroup label="<?php echo $categorySingle['ParentCategory']?>">
														<?php 					
															$subCategoriesText = explode(",", $categorySingle['SubCategories']); 
															$subCategoryIDS = explode(",", $categorySingle['SubCategoryIDS']); 
														?>
														<?php foreach($subCategoriesText as $key => $singleCategory): ?> 
															<option value="<?php echo $subCategoryIDS[$key] ?>"><?php echo $subCategoriesText[$key] ?></option>
														<?php endforeach; ?>
													</optgroup>												
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<?php echo $this -> form -> Input("text", "Engine Size CC (for related inventory purposes)", "inventoryEngineSizeCC", 1) ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<?php echo $this -> form -> Input("text", "Year", "inventoryYear", 1) ?>
									</div>
									<div class="col-md-6">
										<?php echo $this -> form -> Input("text", "Manufacturer", "inventoryManufactur", 2) ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<?php echo $this -> form -> Input("text", "Model", "inventoryModel", 3) ?>
									</div>
									<div class="col-md-6">
										<?php echo $this -> form -> Input("text", "Friendly Model Name", "inventoryModelFriendlyName", 3) ?>
									</div>
								</div>
								<div style='position: fixed; bottom: 0px; background: white; left: 0px; right:0px; box-shadow: 0px 1px 13px #b3b3b3;'>
										<div class="row">
											<div class="col-md-12">
												<div class="ContentPage">
													<div style='padding:15px 10px'>
														<input type="submit" value="Save" class="greenButton" style="font-size: 16px; font-weight: normal;">
													</div>
												</div>
											</div>
										</div>
								</div>
							</form>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	
	<form method="post" action="<?php echo $this -> pages -> inventory() ?>/save/backup">

		
		
					
	
	
	</form>
	
		
	
</div>

<div id="BackupHistoryPopup" style="display:none">
	<?php echo $this -> PopupReadOnly('Inventory Backup History', 'InventoryController.closeBackupHistory()')?>
</div>

