<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php echo $this -> InventorySingle -> GetInventorySpecBreadCrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Edit Vehicle/Inventory
							</div>
						</div>
						<?php if($this -> InventorySingle -> markAsSold != NULL): ?>
							<div class="row">
								<div class="col-md-12">
									This vehicle was marked as sold on <?php echo $this -> recordedTime -> formatDate($this -> InventorySingle -> markAsSold) ?>
								</div>
							</div>
						<?php else: ?>
							<?php if($this -> InventorySingle -> IsVehicleInfoCatched == 0): ?>
								<div class="row buttonContainer">
									<div class="col-md-12">
										<a href="<?php echo $this -> pages -> inventory() ?>/GetSpecifications/<?php echo $this -> InventorySingle -> GetID()?>">
											<div class="greenButton" style="float:left">
												Get Specifications
											</div>
										</a>
									</div>
								</div>
							<?php endif; ?>
						
						
						
							
							
							<?php if($this -> InventorySingle -> IsVehicleInfoCatched == 1): ?>
							<?php 
							$lockedFields = json_decode($this -> InventorySingle -> _lockedColumns, true); 
							
							if(count($lockedFields) > 0) {
								$MSRPLockedKey = array_search("MSRP", array_column($lockedFields, 'ColumnLocked'));
								$MileageLockedKey = array_search("Mileage", array_column($lockedFields, 'ColumnLocked'));	
							} else {
								$MSRPLockedKey = false;
								$MileageLockedKey = false;
							}
							
							
							?>
							
							<form method="post" action="<?php echo $this -> pages -> inventory() ?>/save/current/<?php echo $this -> InventorySingle -> GetID()?>" id="SingleInventory">
								<div class="row">
									<div class="col-md-12" style='margin-bottom:15px;'>
										<div style='margin-bottom:3px;'>
											<?php echo $this -> form -> checkbox('Inventory is Active', 'inventoryActive', $this -> InventorySingle -> InventoryActive, 1, NULL, true) ?>	
										</div>
										<div style='margin-bottom:3px;'>
											<?php echo $this -> form -> checkbox('Feature Product', 'inventoryFeature', $this -> InventorySingle -> IsFeatureProduct, 1, NULL, true) ?>	
										</div>
										<div style='margin-bottom:3px;'>
											<?php echo $this -> form -> checkbox('In Service', 'inventoryInService', $this -> InventorySingle -> IsInService, 1, NULL, true) ?>	
										</div>
										
									</div>
								</div>
								<div class="row">
									
								
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<div class="input">
													<div class="inputLabel">Category</div>
													<select name="InventoryCategoryID">
														<?php foreach($this -> inventorycategories as $categorySingle): ?>
															<optgroup label="<?php echo $categorySingle['ParentCategory']?>">
																<?php 
														
													
																	$subCategoriesText = explode(",", $categorySingle['SubCategories']); 
																	$subCategoryIDS = explode(",", $categorySingle['SubCategoryIDS']); 
																?>
																<?php foreach($subCategoriesText as $key => $singleCategory): ?> 
																	<option value="<?php echo $subCategoryIDS[$key] ?>" <?php if($subCategoryIDS[$key] == $this -> InventorySingle -> Category): ?> selected <?php endif; ?>><?php echo $subCategoriesText[$key] ?></option>
																<?php endforeach; ?>
															</optgroup>
														
														
														<?php endforeach; ?>
														
													</select>
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6">
												<div id="inputID1">
													<?php if($MSRPLockedKey !== false): ?>
														<div class="errorMessage"></div>
														<div style="margin-right: 25px;">
															<?php echo $this -> form -> Input("text", "MSRP", "inventoryMSRP", 1, $this -> InventorySingle -> CurrentMSRP) ?>	
														</div>
														<div style="float:right; margin-top: -45px; font-size: 20px;">
															<a href="<?php echo $this -> pages -> inventory() ?>/unlock/MSRP/<?php echo $this -> InventorySingle -> GetID()?>">
																<i class="fa fa-lock" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="MSRP is locked (this prevents from the system from daily auto updating). Please click the lock to allow auto updating"></i>	
															</a>
															
														</div>
													<?php else: ?>
														<div class="errorMessage"></div>
														<?php echo $this -> form -> Input("text", "MSRP", "inventoryMSRP", 1, $this -> InventorySingle -> CurrentMSRP) ?>
													<?php endif; ?>
												</div>
											</div>
											<div class="col-md-6">
												<div id="inputID5">
													<?php if($MileageLockedKey !== false): ?>
														<div class="errorMessage"></div>
														<div style="margin-right: 25px;">
															<?php echo $this -> form -> Input("text", "Mileage", "inventoryMileage", 5, $this -> InventorySingle -> CurrentMileage) ?>
														</div>
														<div style="float:right; margin-top: -45px; font-size: 20px;">
															<a href="<?php echo $this -> pages -> inventory() ?>/unlock/Mileage/<?php echo $this -> InventorySingle -> GetID()?>">
																<i class="fa fa-lock" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Mileage is locked (this prevents from the system from daily auto updating). Please click the lock to allow auto updating"></i>
															</a>
														</div>
													<?php else: ?>
														<div class="errorMessage"></div>
														<?php echo $this -> form -> Input("text", "Mileage", "inventoryMileage", 5, $this -> InventorySingle -> CurrentMileage) ?>
													<?php endif; ?>
													
													
												</div>
												
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-3">
												<div id="inputID2">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Year", "inventoryYear", 2, $this -> InventorySingle -> Year) ?>
												</div>
											</div>
											<div class="col-md-3">
												<div id="inputID3">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Manufacturer", "inventoryManufactur", 3, $this -> InventorySingle -> Manufacture) ?>
												</div>
											</div>
											<div class="col-md-6">
												<div id="inputID4">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Model", "inventoryModel", 4, $this -> InventorySingle -> CurrentModel) ?>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<?php echo $this -> form -> Input("text", "Friendly Model Name", "inventoryModelFriendlyName", NULL, $this -> InventorySingle -> ModelFriendlyName) ?>
											</div>
										</div>
										
										
									</div>
									
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<div id="inputID5">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Overlay Text", "inventoryOverlayText", 5, $this -> InventorySingle -> OverlayText) ?>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div id="inputID5">
													<div class="errorMessage"></div>
													<div class="input">
														<div class="inputLabel">Youtube Video ID</div>
														<div class="descriptiveInfo">
															If you want to display this Youtube video on this URL -> https://www.youtube.com/watch?v=<strong>XsZKrctSDaw</strong><br />
															The value you need to enter put in this input is value -> <strong>XsZKrctSDaw</strong>
														</div>
														<input id="inventoryYoutubeURL" class="inventoryYoutubeURL" type="text" name="inventoryYoutubeURL" value="<?php echo $this -> InventorySingle -> YoutubeURL ?>" placeholder='i.e. vfWzyEWFu3I' onchange="Globals.removeValidationMessage(6)">
													</div>
												</div>
											</div>
										</div>
									
								</div>
								
								</div>
								<div class="row">
									<div class="col-md-12">
										<?php echo $this-> form -> textarea("Inventory Description", "inventoryDescription", NULL, $this -> InventorySingle -> InventoryDescription) ?>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12 sectionHeader">
										Photos
									</div>
								</div>
								<?php 
									$needsWaterMarkArray = array();
								?>
								<?php if(count($this -> photos) > 0): ?>
									<div class="row">
										<div class="col-md-12" style='margin-bottom:10px'>
											<?php foreach($this -> photos as $photo): ?>
												<div style="width:228px; float:left">
													<div class="inventoryPhotoSingleElement" style='background:url(<?php echo PHOTO_URL ?>inventory/<?php echo $this -> InventorySingle -> VinNumber ?>/<?php echo $photo['inventoryPhotoName'] . '-s.' . $photo['inventoryPhotoExt'] ?>) no-repeat center'></div>
													<div class="photoOptions">
														<a onclick="InventoryController.ViewPhoto(<?php echo $photo['inventoryPhotoID'] ?>)" href="javascript:void(0);">
															<i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="view"></i>	
														</a>
														
														<a onclick="return confirm('Are you sure you want to delete this photo?')" href="<?php echo $this -> pages -> inventory() ?>/delete/inventoryphoto/<?php echo $photo['inventoryPhotoID'] ?>">
															<i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="delete"></i>	
														</a>
															
													
													</div>	
												</div>
												
												<?php array_push($needsWaterMarkArray, $photo['WaterMarkApplied']) ?>
											<?php endforeach; ?>
										</div>
									</div>
								<?php endif; ?>
								
								<div class="row buttonContainer">
									<div class="col-md-12">
										<a href="javascript:void(0);" onclick="InventoryController.AddPhotosToInventory(<?php echo $this -> InventorySingle -> GetID()?>)">
											<div class="whiteButton" style="float:left; margin-right:5px;">
												Add Photos
											</div>				
										</a>
										<?php if( in_array( 0 ,$needsWaterMarkArray ) ): ?>
										<a href="<?php echo $this -> pages -> inventory() ?>/applywatermark/all/<?php echo $this -> InventorySingle -> GetID()?>">
											<div class="blueButton" style="float:left; margin-right:5px;">
												Watermark Photos
											</div>				
										</a>
										<?php endif; ?>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12 sectionHeader">
										Specifications
									</div>
								</div>
								
								
								<?php $specDecoded = json_decode($this -> InventorySingle -> GetSpecs(), true); ?>
								<input type="hidden" id="SpecJSON" name="SpecJSON" value='<?php echo $this -> InventorySingle -> GetSpecs()?>' />
								
								<div style='clear:both'>
								<?php foreach($this -> SpecsList as $spec): ?>
									<?php 
									$showHeader = false;
									$associatedLabels = array();
									
									if($spec['LabelsByGroup'] != NULL) {
										$LabelIDS = explode(",", $spec['LabelsByGroup']);
										$labelNames = explode(" / ", $spec['GrabbedLabelNames']);
										$labelOrders = explode(",", $spec['LabelOrders']);
														
										foreach($LabelIDS as $key => $label) {
											array_push($associatedLabels, array("specLabelID" => $LabelIDS[$key],
																				"labelText" => $labelNames[$key],
																				"labelOrder" => $labelOrders[$key]));
										}
														
														
														
										usort($associatedLabels, function($a, $b) {
											return $a['labelOrder'] - $b['labelOrder'];
										});									
									}
									
									$categoryArrayCheck = array_column($associatedLabels, 'labelText');
									
									$categoryIDS = array();
									
									foreach($categoryArrayCheck as $getCategoryIDS) {
										$getIDS = explode(" - ", $getCategoryIDS);	
										if(count($getIDS) > 1) {
											$specLabelsHeaderCheck = explode(",", $getIDS[2]);
											
											if(array_search($this -> InventorySingle -> Category, $specLabelsHeaderCheck) !== false) {
												$showHeader = true;
												break;
											}				
										}
					
										
									}
					
									
									
								?>
									
								<?php if($showHeader == true): ?>
								<div class="row">
									<div class="col-md-12 SpecGroupHeader">
										<?php echo $spec['specGroupText'] ?>
									</div>
								</div>
								<?php endif; ?>
								
									<?php if(count($associatedLabels)> 0) :?>
										<?php foreach ($associatedLabels as $label) :?>	
											<?php 
												$relatedCategoryLabel = explode(" - ", $label['labelText']);
											?>
											
											<?php if(count($relatedCategoryLabel) > 1) :?>
												
												<?php $specLabels = explode(",", $relatedCategoryLabel[2]); ?>
					      					
					      						<?php if(array_search($this -> InventorySingle -> Category, $specLabels) !== false): ?>				
												<div class="row" style="margin-bottom:15px;">
													<div class="col-md-2" style='margin-top: 5px;'>
														<?php echo $relatedCategoryLabel[0] ?>
													</div>					
													<div class="col-md-10">
														<?php 
															$inputValue = NULL;
															$key = array_search($label["specLabelID"], array_column($specDecoded, 'LabelID'));
														
															if($key !== false) {
																$inputValue = $specDecoded[$key]['Content'];
															}
														
														
														 ?>
														
														<input type="text" placeholder="<?php echo $relatedCategoryLabel[0] ?>" value="<?php echo $inputValue ?>"  onkeyup="InventoryController.EditLabelContent(this, <?php echo $label['specLabelID'] ?>)">
													</div>
													
												</div>
												<?php endif; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
									
									
								<?php endforeach; ?>
								</div>
								<?php if($this -> InventorySingle -> IsVehicleInfoCatched == 1): ?>
									<div style='position: fixed; bottom: 0px; background: white; left: 0px; right:0px; box-shadow: 0px 1px 13px #b3b3b3;'>
										<div class="row">
											<div class="col-md-12">
												<div class="ContentPage">
													<div style='padding:15px 10px'>
														<input type="submit" value="Save Inventory" class="greenButton" style='font-size: 16px; font-weight: normal;'>	
													</div> 			
												</div>
												
											</div>
										</div>				
									</div>
									
								<?php endif; ?>
							
							
							</form>
							<?php endif; ?>
						<?php endif; ?>
						
						
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-6">
								<div style='margin-bottom:20px;'>
													<div><strong>Stock #</strong></div>
													<?php echo $this -> InventorySingle -> Stock ?>
								</div>
								<div style='margin-bottom:20px;'>
									<div><strong>Vin Number</strong></div>
									<?php echo $this -> InventorySingle -> VinNumber ?>
									<?php if($this -> InventorySingle -> Conditions == 0) : ?>
										<strong style='color:red'>(NEW)</strong>
									<?php else: ?>
										<strong style='color:red'>(USED)</strong>
									<?php endif; ?>	
								</div>
								<div>
									<div><strong>Color</strong></div>
									<?php echo str_replace(",","/",$this -> InventorySingle -> Color) ?>
								</div>
							</div>
							<div class="col-md-6">
								<div>
									<div><strong>Location</strong></div>
									<?php 
										switch($this -> InventorySingle -> StoreLocation) {
											case 'A':
												echo "Dillon Brothers Harley-Davidson<br />3838 N HWS Cleveland Blvd<br /> Omaha, NE 68116";
												break;
											case 'B':
												echo "Dillon Brothers Harley-Davidson<br />2440 East 23rd Street Fremont, NE 68025";
												break;		
											case 'C':
												echo "Dillon Brothers Motorsport<br />3848 N HWS Cleveland Blvd<br /> Omaha, NE 68116";
												break;
											case 'D':
												echo "Dillon Brothers Indian<br />3840 N 174th Ave.<br /> Omaha, NE 68116";
												break;	
										}			
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Internal Feedback
							</div>
						</div>
						<form method="post" id="CurrentInventoryFeedBack" action="<?php echo PATH ?>inventory/feedback/<?php echo $this -> InventorySingle -> GetID()?>">
							<div class="row">
								<div class="col-md-12">
									<div id="FeedBackID1">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Notes</div>
											<textarea id="internalFeedBackNotes" name="internalFeedBackNotes" onchange="Globals.removeFeedBackValidationMessage('FeedBackID1')"></textarea>
										</div>
																										
									</div>
									
									
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Save Notes" class="greenButtonRectangle">
								</div>
							</div>
						</form>
					</div>
					<div class="content" style='border-top: 1px solid #e0e0e0;'>
						<?php if(count($this -> feedBackList) > 0): ?>
							<div id="FeedBackList">
								<?php foreach($this -> feedBackList as $item): ?>
									<div class="FeedBackItem">
										<div class="bubble">
											<?php echo $item['FeedbackText'] ?>
											<div class="caret"></div>
										</div>
										<div class="detailsBy">
											<?php $submittedDetails = explode('T', $item['feedBackSubmitted']); ?>
											<?php echo $item['firstName'] ?> <?php echo $item['lastName'] ?> | <?php echo $this -> recordedTime -> formatDate($submittedDetails[0]) ?> - <?php echo $this -> recordedTime -> formatTime($submittedDetails[1]) ?>
										</div>
									</div>
								<?php endforeach; ?>
								
								
							</div>
						<?php else: ?>
							<div id="FeedBackList">There is no feed back on this item</div>
						<?php endif; ?>	
						
						<div class="FeedBackItem" id="newFeedBackItem" style='display:none;'>
									<div class="bubble"></div>
									<div class="detailsBy"></div>
								</div>
						
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>

<div class="container">
	
	
	
</div>




