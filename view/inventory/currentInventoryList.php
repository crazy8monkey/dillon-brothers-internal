<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> inventory() ?>">Inventory Portal</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Select Store
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Select Store
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid" style='padding-bottom: 80px;'>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventoryViewing') == 0) : ?>
			<div class="row">
				<div class="col-md-4">
					<div class="WhiteSectionDiv">
						<div class="content">
							<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/motorsports/1">
								<div class="ButtonBox" style='padding:25px 0px;'>
									<img src="<?php echo PATH ?>public/images/MotorSportLogo.png">
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="WhiteSectionDiv">
						<div class="content">
							<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/harleydavidson/1">
								<div class="ButtonBox">
									<img src="<?php echo PATH ?>public/images/DillonHarley.png">
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/indian/1">
						<div class="ButtonBox">
							<img src="<?php echo PATH ?>public/images/DillonIndian.png">
						</div>
					</a>
				</div>
			</div>
				
			
		<?php else: ?>
			<div class="row">
				<?php 
					$showMotorsport = false;
					$showHarley = false;
					$showIndian = false;
					
					$showStoreCount = 0;
				
					if(in_array('2', array_column($_SESSION["user"]->GetAssociatedStores(), 'associatedStore'))) {
						$showMotorsport = true;
						$showStoreCount++;
					}
					
					if(in_array('3', array_column($_SESSION["user"]->GetAssociatedStores(), 'associatedStore'))) {
						$showHarley = true;
						$showStoreCount++;
					}
					
					if(in_array('4', array_column($_SESSION["user"]->GetAssociatedStores(), 'associatedStore'))) {
						$showIndian = true;
						$showStoreCount++;
					}
					
					switch($showStoreCount) {
						case 1:
							$cssClass = "col-md-12";
							break;
						case 2:
							$cssClass = "col-md-6";
							break;
						case 3:
							$cssClass = "col-md-4";
							break;
					}
					
					
				?>
				
				<?php if($showMotorsport == true): ?>		
					<div class="<?php echo $cssClass ?>">
						<div class="WhiteSectionDiv">
							<div class="content">
								<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/motorsports/1">
									<div class="ButtonBox" style='padding:25px 0px;'>
										<img src="<?php echo PATH ?>public/images/MotorSportLogo.png">
									</div>
								</a>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if($showHarley == true): ?>		
				<div class="<?php echo $cssClass ?>">
					<div class="WhiteSectionDiv">
						<div class="content">
							<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/harleydavidson/1">
								<div class="ButtonBox">
									<img src="<?php echo PATH ?>public/images/DillonHarley.png">
								</div>
							</a>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if($showIndian == true): ?>		
				<div class="<?php echo $cssClass ?>">
					<div class="WhiteSectionDiv">
						<div class="content">
							<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/indian/1">
								<div class="ButtonBox">
									<img src="<?php echo PATH ?>public/images/DillonIndian.png">
								</div>
							</a>
						</div>
					</div>
					
				</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		
		
	</div>
</div>



