<?php 
	
	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventoryViewing') == 0) {
		$viewInventoryUrl = 'viewinventory';
	} else {
		$viewInventoryUrl = 'edit';
	}
	
	function multiexplode ($delimiters,$string) {

	    $ready = str_replace($delimiters, $delimiters[0], $string);
	    $launch = explode($delimiters[0], $ready);
	    return  $launch;
	}
	
?>

<style type='text/css'>
	body {
		background: #f6f7fb !important;
	}
	 
</style>

<div class="topSectionPage" style='margin-top: -20px; padding-top:20px;'>
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php if(isset($this  -> filteredResultsPage)): ?>
						<a href="<?php echo $this -> pages -> inventory() ?>">Inventory Portal</a><span><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> inventory() ?>/currentinventory">Select Store</a><span><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/<?php echo $this ->  currentinventorypage ?>/1"><?php echo $this -> currentInventoryList; ?></a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Filtered Results<span>
					<?php else: ?>
						<a href="<?php echo $this -> pages -> inventory() ?>">Inventory Portal</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> inventory() ?>/currentinventory">Select Store</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> currentInventoryList; ?>
					<?php endif; ?>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					<div style="float:left">
						<?php if(isset($this  -> filteredResultsPage)): ?>
							Filtered <?php echo $this -> currentInventoryList; ?> Inventory
						<?php else: ?>
							Current Inventory
						<?php endif; ?>
					</div>
					<?php if(!isset($this  -> filteredResultsPage)): ?>
						<div style="float:left; margin-top: 6px; margin-left: 10px;">
							<a href="javascript:void(0);">
								<div class="dropDownStores">
									<div class="arrow"></div>	
								</div>
							</a>
							<div class="StoreDropDownElement" style='width:auto'>
								<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventoryViewing') == 0) : ?>
									
									<?php if($this -> PageTitle == "Current Motorsport Inventory") : ?>
										<div class="Selected">
											Motorsports
										</div>
									<?php else: ?>
										<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/motorsports/1">
											<div class="elementLine">
												Motorsports
											</div>
										</a>
									<?php endif; ?>
									
									<?php if($this -> PageTitle == "Current Harley-Davidson Inventory") : ?>
										<div class="Selected">
											Harley-Davidson
										</div>
									<?php else: ?>
										<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/harleydavidson/1">
											<div class="elementLine">
												Harley-Davidson
											</div>
										</a>
									<?php endif; ?>
									
									<?php if($this -> PageTitle == "Current Indian Inventory") : ?>
										<div class="Selected">
											Indian
										</div>
									<?php else: ?>
										<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/indian/1">
											<div class="elementLine">
												Indian
											</div>
										</a>
									<?php endif; ?>
									
								<?php else: ?>
									
									<?php if(in_array('2', array_column($_SESSION["user"]->GetAssociatedStores(), 'associatedStore'))): ?>									
										<?php if($this -> PageTitle == "Current Motorsport Inventory") : ?>
											<div class="Selected">
												Motorsports
											</div>
										<?php else: ?>
											<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/motorsports/1">
												<div class="elementLine">
													Motorsports
												</div>
											</a>
										<?php endif; ?>
									<?php endif; ?>
									
									<?php if(in_array('3', array_column($_SESSION["user"]->GetAssociatedStores(), 'associatedStore'))): ?>
										<?php if($this -> PageTitle == "Current Harley-Davidson Inventory") : ?>
											<div class="Selected">
												Harley-Davidson
											</div>
										<?php else: ?>
											<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/harleydavidson/1">
												<div class="elementLine">
													Harley-Davidson
												</div>
											</a>
										<?php endif; ?>
									<?php endif; ?>
									
									<?php if(in_array('4', array_column($_SESSION["user"]->GetAssociatedStores(), 'associatedStore'))): ?>
										<?php if($this -> PageTitle == "Current Indian Inventory") : ?>
											<div class="Selected">
												Indian
											</div>
										<?php else: ?>
											<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory/indian/1">
												<div class="elementLine">
													Indian
												</div>
											</a>
										<?php endif; ?>
									<?php endif; ?>
								
								<?php endif; ?>
								
								
								
								
								
							</div>
						</div>
						<div style='margin-top: 5px; width: 300px; float: left; color: black; font-size: 12px;'>
							<select name='SortByOptions' onchange="InventoryController.ChangeSortingOrder(this)">
								<option>Sort Inventory</option>
								<option <?php if(isset($_GET['SortBy']) && $_GET['SortBy'] == 'YearDesc'): ?>selected <?php endif; ?>value='YearDesc'>Year Descending</option>
								<option <?php if(isset($_GET['SortBy']) && $_GET['SortBy'] == 'YearAsc'): ?>selected <?php endif; ?>value='YearAsc'>Year Ascending</option>
								<option <?php if(isset($_GET['SortBy']) && $_GET['SortBy'] == 'ManufactureDesc'): ?>selected <?php endif; ?>value='ManufactureDesc'>Manufacture Descending</option>
								<option <?php if(isset($_GET['SortBy']) && $_GET['SortBy'] == 'ManufactureAsc'): ?>selected <?php endif; ?>value='ManufactureAsc'>Manufacture Ascending</option>
								<option <?php if(isset($_GET['SortBy']) && $_GET['SortBy'] == 'PriceDesc'): ?>selected <?php endif; ?>value='PriceDesc'>Price Descending</option>
								<option <?php if(isset($_GET['SortBy']) && $_GET['SortBy'] == 'PriceAsc'): ?>selected <?php endif; ?>value='PriceAsc'>Price Ascending</option>
							</select>
						</div>
					<?php endif; ?>
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventoryViewing') == 1) : ?>
						<div style='margin-top:5px; float:left'>
							<a href="<?php echo $this -> pages -> inventory() ?>/create">
								<div class="greenButton">
									Create Inventory
								</div>
							</a>
						</div>
					<?php endif; ?>
					<div style="clear:both"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style="padding-bottom:10px;">
					<div><strong>Filter <?php echo $this -> currentInventoryList; ?></strong></div>	
					<div style='margin-top:10px; float:left'>
						<form action="<?php echo $this -> pages -> inventory() ?>/filterinventory" method="post" id="FilteredForm">
							<div style='float:left; width: 370px;'>
								<?php if(isset($this -> selectedValue)): ?>
									<input id="inventoryOverlayText" class="FilteredText" type="text" name="FilteredText" value="<?php echo $this -> selectedValue ?>" onchange="Globals.removeValidationMessage(5)">	
								<?php else: ?>
									<input id="inventoryOverlayText" class="FilteredText" type="text" name="FilteredText" onchange="Globals.removeValidationMessage(5)">
								<?php endif; ?>
							</div>
							<input type='hidden' id="storeID" name="storeID" value="<?php echo $this -> storeID ?>" />
							<button type="submit" class="filterSubmitButton" style='float:left'>
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
							<div id="FilterMessageErrors" style='color:#c30000; font-weight:bold; margin-left: 10px; float: left; margin-top: 5px;'></div>
							
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div style='margin-bottom:80px;'>
			<?php foreach(array_chunk($this -> InventoryList['inventory-list'], 3, true) as $inventorySix) : ?>
				<div class="row">
					<?php foreach($inventorySix as $inventorySingle) : ?>
					
					<div class="col-md-4">
						<a href="<?php echo $this -> pages -> inventory() ?>/<?php echo $viewInventoryUrl ?>/<?php echo $inventorySingle['inventoryID'] ?>">
							<div class="inventorySingleCard">
								<?php if($inventorySingle['MarkAsSoldDate'] != NULL): ?>
									<div class="soldOverlay">
										<div class="TextSold">
											SOLD	
										</div>
									</div>
									
								<?php endif; ?>	
								
								<div class="inventoryHeader">
									<div class="MSRP">
										$<?php echo number_format($inventorySingle['MSRP'], 2) ?>	
									</div>
									<div class="Stock">
										<?php echo $inventorySingle['Stock'] ?>	
									</div>
									<div style="clear:both"></div>							
								</div>
								<div class="row" style='margin-right:0px; margin-left:0px; position: relative;'>
									<div class="col-md-4" style='position: absolute; height: 100%;'>
										<?php if($inventorySingle['inventoryPhotoName'] != NULL) : ?>
											<div class="bikePhoto InventoryPhotoUploaded" style='background:url(<?php echo PHOTO_URL . 'inventory/'. $inventorySingle['VinNumber'] . '/' . $inventorySingle['inventoryPhotoName'] . '-s.' . $inventorySingle['inventoryPhotoExt'] ?>) no-repeat center;'></div>
												
										<?php else: ?>
											<div class="bikePhoto NoInventoryPhoto">
												<img src="<?php echo PATH ?>public/images/EmptyInventoryPlacement.png" />
											</div>
										<?php endif; ?>
										
									</div>
									<div class="col-md-8 BikeInfoElement">
										<div class="BikeInfo">
											<div style="margin-bottom:10px;">
												<strong><?php echo $inventorySingle['Year'] ?> <?php echo $inventorySingle['Manufacturer'] ?> <?php echo $inventorySingle['ModelName'] ?> <?php echo $inventorySingle['FriendlyModelName'] ?></strong><br />
														VIN/HIN - <?php echo $inventorySingle['VinNumber'] ?>
											</div>
											<div class="row">
												<div class="col-md-6">
													<strong>Mileage</strong><br />
													<?php echo number_format($inventorySingle['Mileage'], 0) ?>
												</div>
												<div class="col-md-6">
													<strong>Color</strong><br />
													<?php 
														echo str_replace(",", '/', $inventorySingle['SelectedColor']);
													?>
													
												</div>
											</div>
											<?php if($inventorySingle['IsInventoryActive'] == 1): ?>
												<div class="isActiveIndicator" data-toggle="tooltip" data-placement="left" title="Active"></div>
											<?php endif; ?>
											<?php if($inventorySingle['VehicleInfoChecked'] == 0): ?>
												<div class="needsSpecsIndicator" data-toggle="tooltip" data-placement="left" title="Needs Specifications"></div>
											<?php endif; ?>												
											
											
										</div>
										
									</div>
								</div>
		
							</div>
						</a>
					</div>
					<?php endforeach; ?>		
				</div>
			<?php endforeach; ?>			
		</div>

			
			
	</div>
</div>
<?php if(isset($this -> InventoryList['pagination'])) :?>
	<div class="InventoryPaginationObject">
		<div class="ContentPage">
		<?php echo $this -> InventoryList['pagination'] ?>
		</div>
	</div>
<?php endif; ?>
