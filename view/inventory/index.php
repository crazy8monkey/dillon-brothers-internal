<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Inventory Portal
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid" style='padding-bottom: 80px;'>
		<div class="row">
			<div class="col-md-6">
				<div class="WhiteSectionDiv">
					<div class="content">
						<a href="<?php echo $this -> pages -> inventory() ?>/currentinventory">
							<div class="ButtonBox">
								<div><img src="<?php echo PATH ?>public/images/VehicleSpecialCategory.png" /></div>
								<div class="ContentTypeText">
									Current Inventory
								</div>
							</div>	
						</a>	
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="WhiteSectionDiv">
					<div class="content">
						<a href="<?php echo $this -> pages -> inventory() ?>/backup/1">
							<div class="ButtonBox">
								<div><img src="<?php echo PATH ?>public/images/InventoryBackup.png"></div>
								<div class="ContentTypeText">
									Specifications Backup
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">

	<div class="row">
		<div class="col-md-6">
					
		</div>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventorySpecificationManagement') == 1) : ?>
		<div class="col-md-6">
			
		</div>
		<?php endif; ?>
	</div>
	
</div>

