<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
<head>
	<title>Edit Photo</title>
		
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
				
	<!-- Google Authorship and Publisher Markup --> 
	<a rel="publisher" href="https://plus.google.com/103831261532249725840/"></a>
	<a rel="author" href="https://plus.google.com/103831261532249725840/"></a>
		
		
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/InventoryController.css" /> 
	
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
		
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		
	<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo PATH ?>public/js/InventoryController.js"></script>	
	<script type="text/javascript">
		$(document).ready(function() {
			InventoryController.InitializePhotoEdit();
			$('[data-toggle="tooltip"]').tooltip({html: true});
		})
	</script>
		

</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php echo $this -> form -> ValiationMessage("PhotoSingleMessage", "PhotoSingleLoading", "PhotoSingleText") ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="photoContainer" style='position:relative'>
				<img src="<?php echo PHOTO_URL . 'inventory/' . $this -> photoSingle -> vin . '/' . $this -> photoSingle -> currentPhotoName . '-l.' . $this -> photoSingle -> currentPhotoExt?>" />
				<?php if($this -> photoSingle -> WaterMarkApplied == 0): ?>				
					<div style="position:absolute; bottom:10px; left:10px;">
						<a href="<?php echo $this -> pages -> inventory() ?>/applywatermark/single/<?php echo $this -> photoSingle -> GetPhotoID() ?>">
							<div class="whiteButton" style='float:left'>
								Apply Watermark
							</div>							
						</a>

					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<form method="post" id="editPhoto" action="<?php echo $this -> pages -> inventory() ?>/save/inventoryphoto/<?php echo $this -> photoSingle -> GetPhotoID() ?>">
				<div class="row">
					<div class="col-md-12" style='margin-bottom:15px;'>
						<div style="float:left; margin-right:10px;">
							<?php echo $this -> form -> checkbox('Main Photo', 'mainInventoryPhoto', $this -> photoSingle -> mainPhotoCheck, 1, NULL, true) ?>	
						</div>
						<div style="float:right">
							<div style="float:left; margin-right:10px;">
								<a href="<?php echo $this -> pages -> inventory() ?>/rotate/right/<?php echo $this -> photoSingle -> GetPhotoID() ?>">
									<img src="<?php echo PATH ?>public/images/RotateRight.png" data-toggle="tooltip" data-placement="bottom" title="Rotate Right" />
								</a>
							</div>
							<div style="float:left; margin-right:10px;">
								<a href="<?php echo $this -> pages -> inventory() ?>/rotate/left/<?php echo $this -> photoSingle -> GetPhotoID() ?>">
									<img src="<?php echo PATH ?>public/images/RotateLeft.png" data-toggle="tooltip" data-placement="bottom" title="Rotate Left" />
								</a>
							</div>							
						</div>

						<div style="border-bottom: 1px solid #eaeaea; clear: both; padding-top: 7px;"></div>
					</div>
					
				</div>
				
				<div id="inputID2">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Photo Name", "photoName", 2, $this -> photoSingle -> currentPhotoName) ?>	
				</div>
				<?php echo $this -> form -> Input("text", "Image Alt Text", "altText", 2, $this -> photoSingle -> altTag) ?>
				<div id="inputID3">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Image Title Text", "titleText", 3, $this -> photoSingle -> titleTag) ?>
				</div>
				<?php echo $this -> form -> Submit("Save", "greenButton") ?>
			</form>
		</div>
	</div>
	
</div>




</body>	
</html>
