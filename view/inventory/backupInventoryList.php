<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> inventory() ?>">Inventory Portal</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Inventory Specificaitons Backup
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Inventory Backup List
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> inventory() ?>/newbackup">
									<div class="greenButton" style="float:left">
										New Backup
									</div>
								</a>
							</div>
						</div>
						<div class="subHeader">Filter Backup list</div>
						<form method='post' action="<?php echo $this -> pages -> inventory() ?>/filter/backup" id="BackupFilterList">
							<div class="row">
								<div class="col-md-12">
									<div class="input">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php if(isset($_GET['SearchQuery'])): ?>
												<input type='text' name='BackupFilterResult' placeholder='i.e 2017 Kawasaki Blah' value='<?php echo $_GET['SearchQuery'] ?>' onchange="Globals.removeValidationMessage(1)" />
											<?php else: ?>
												<input type='text' name='BackupFilterResult' placeholder='i.e 2017 Kawasaki Blah' onchange="Globals.removeValidationMessage(1)" />	
											<?php endif; ?>
											
										</div>
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="input">
										<div class="inputLabel">Category</div>
										<select name="InventoryCategoryID">
											<option value='0'>Select Category (Optional)</option>
											<?php foreach($this -> filterCategories as $categorySingle): ?>
												<optgroup label="<?php echo $categorySingle['ParentCategory']?>">
													<?php 
														$subCategoriesText = explode(",", $categorySingle['SubCategories']); 
														$subCategoryIDS = explode(",", $categorySingle['SubCategoryIDS']); 
													?>
													<?php foreach($subCategoriesText as $key => $singleCategory): ?> 
														<?php if(isset($_GET['category'])): ?>
															<option value="<?php echo $subCategoryIDS[$key] ?>" <?php if($_GET['category'] == $subCategoryIDS[$key]): ?> selected <?php endif; ?>><?php echo $subCategoriesText[$key] ?></option>
														<?php else: ?>
															<option value="<?php echo $subCategoryIDS[$key] ?>"><?php echo $subCategoriesText[$key] ?></option>
														<?php endif; ?>
														
													<?php endforeach; ?>
												</optgroup>
															
															
											<?php endforeach; ?>					
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="input">
										<div class="inputLabel">Filter By</div>
										<select name="filterByOptions">
											<option value='0'>(Optional)</option>
											<option value='EmptyCC' <?php if(isset($_GET['FilteredBy']) && $_GET['FilteredBy'] == 'EmptyCC'): ?>selected<?php endif; ?>>Empty Engine Size CC</option>
											<option value='SpecVerified' <?php if(isset($_GET['FilteredBy']) && $_GET['FilteredBy'] == 'SpecVerified'): ?>selected<?php endif; ?>>Spec Verified</option>
											<option value='SpecNotVerified' <?php if(isset($_GET['FilteredBy']) && $_GET['FilteredBy'] == 'SpecNotVerified'): ?>selected<?php endif; ?>>Spec Not Verified</option>
											<option value='EmptyFriendlyName' <?php if(isset($_GET['FilteredBy']) && $_GET['FilteredBy'] == 'EmptyFriendlyName'): ?>selected<?php endif; ?>>Empty Friendly Name</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Filter Results" class='whiteButton'>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<?php if(count($this -> orphanedVins) > 0): ?>
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="subHeader">Orphaned Vin Numbers</div>
							<div>
								<?php $vins = ''; 
								foreach($this -> orphanedVins as $orphanedVin) {
									$onclick = "InventoryController.VinDetails('" . $orphanedVin['backedupVinNumber'] . "', '" . $orphanedVin['vinBackupID'] . "')";
									$vins .= '<a href="javascript:void(0)" onclick="' . $onclick . '">' . $orphanedVin['backedupVinNumber'] . '</a>, ';
								}
								echo rtrim($vins, ', '); 
								?>
							</div>
							
						</div>
					</div>
				<?php endif; ?>
				<div class="WhiteSectionDiv">
					<div class="content" style='padding:0px'>
						<?php if(count($this -> backupList['backup-list']) > 0): ?>
							<div class="row" style='margin: 0px;'>
								<div class="col-md-12">
									<?php echo $this -> backupList['pagination'] ?>
								</div>
							</div>
						

						<div class="row" style='margin: 0px;'>
							<div class="col-xs-6">
							</div>
							<div class="col-xs-6" style='text-align:right; font-size:16px; padding:10px'>
								<strong>Last Updated</strong>
							</div>
						</div>
						<?php foreach($this -> backupList['backup-list'] as $backupSingle) : ?>
							<div class="row" style='margin: 0px;'>
								<div class="divider" style='padding:0px; margin:0px'></div>
								<div class="col-xs-8 vehiclebackupColumn" style='padding-left:0px;'>
									<div style='width:30px; height: 20px; float:left' <?php if($backupSingle['BackupVehicleVerified'] == 1) : ?>data-toggle="tooltip" data-placement="bottom" title="Vehicle Is Verified"<?php endif; ?>>
										<?php if($backupSingle['BackupVehicleVerified'] == 1) : ?>
											<div class='verifiedSingle'></div>
										<?php endif; ?>
									</div>
									<div style="height:10px; width:20px; float:left; position: relative;">
										<?php if($backupSingle['IsNewBackup'] == 1) : ?> 
											<div class="NewBackupIndicator" data-toggle="tooltip" data-placement="bottom" title="New Backup"></div>	
										<?php endif; ?>	
									</div>
											
									<?php $specLabelSetup = explode(',', $backupSingle['AllSpecLabels']) ?>
									<?php 
										$count = 0;
										$totalLabelCount = 0;
										$jsonSpec = file_get_contents(INVENTORY_VEHICLE_INFO_PATH . 'backup/' . $backupSingle['inventoryBackupSpecJSON']);
										$specArray = json_decode($jsonSpec, true);
											
										foreach($specArray as $key => $specSingle) {					
											if(array_search($specSingle['LabelID'],$specLabelSetup) !== FALSE) {
												$totalLabelCount++;
												if(!empty($specSingle['Content'])) {
													$count++;
												}
											}			
										}
												
										if(!empty($backupSingle['inventoryBackupYear'])) {
											$count++;
										}
												
										if(!empty($backupSingle['backupEngineSizeCC'])) {
											$count++;
										}
												
										if(!empty($backupSingle['inventoryBackupManufactur'])) {
											$count++;
										}
												
										if(!empty($backupSingle['inventoryBackupModelName'])) {
											$count++;
										}
												
										if(!empty($backupSingle['inventoryBackupModelFriendlyName'])) {
											$count++;
										}
												
										if(!empty($backupSingle['inventoryBackupDecription'])) {
											$count++;
										}
												
										if($backupSingle['inventoryBackupCategory'] == NULL) {
											$count++;
										}
												
												
										$percentage = round(($count / ($totalLabelCount + 6)) * 100);
												
										$color = NULL;
												
										if($percentage >= 70) {
											$color = "color: #0cb703;";
										}
												
										if($percentage >= 49 && $percentage <= 69) {
											$color = "color: #ffa128;";
										}
												
										if(50 >= $percentage) {
											$color = "color: #d2053c;";
										}
										
									?>
								<div style='<?php echo $color ?>float:left; width:50px; border-right:2px solid #cecece; margin-right:5px;'><strong><?php echo $percentage ?>%</strong></div>
									<a href='javascript:void(0)' onclick='InventoryController.QuickBikeDetails(<?php echo $backupSingle['inventoryBackupID'] ?>)'><i style='color: #999;' class="fa fa-info-circle" aria-hidden="true"></i></a><a href="<?php echo $this -> pages -> inventory() ?>/editbackup/<?php echo $backupSingle['inventoryBackupID'] ?>">
										<?php echo $backupSingle['inventoryBackupYear'] ?> <?php echo $backupSingle['inventoryBackupManufactur'] ?> <?php echo $backupSingle['inventoryBackupModelName'] ?> <?php echo $backupSingle['inventoryBackupModelFriendlyName'] ?>	
									</a>		
								</div>
								<div class="col-xs-4 vehiclebackupColumn" style='text-align:right'>
									<?php if($backupSingle['edittedTimeAndDate'] != NULL) : ?>
										<?php $dateTimeSplit = explode('T', $backupSingle['edittedTimeAndDate']); ?>
										
										<?php echo $this -> recordedTime -> formatShortDate($dateTimeSplit[0]) . ' / ' . $this -> recordedTime -> formatTime($dateTimeSplit[1]); ?>
										<img data-toggle="tooltip" data-placement="bottom" title="<?php echo $backupSingle['LastEditedPerson'] ?>" src="<?php echo PATH ?>public/images/ToolTipHelp.png" style='width: 16px; margin-top: -2px;'>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class="col-md-12">
									<div style='padding: 15px;'>
										There are no results based on your filter results, please try a different result	
									</div>
									
								</div>
							</div>
						<?php endif; ?>		
								
						
				</div>
			</div>
		</div>
	</div>
</div>
<div id="BackupInventoryPopup" style="display:none">
	<a href="javascript:void(0);" onclick="InventoryController.CloseQuickBikeDetails()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">Bike Backup Details
			<a href="javascript:void(0);" onclick="InventoryController.CloseQuickBikeDetails()">
				<div class="close" style="margin-top: 4px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>	
		</div>
		<div class="FormContent" style="margin-right: 15px; overflow-y: auto; height: 370px;">
			<div style='font-size: 16px; margin-bottom: 10px;'>Saved Details</div>
			<table>
				<tr>
					<td style='font-size:12px; padding-right:10px;'><strong>YEAR:</strong></td>
					<td style='font-size:12px;' id="savedYear"></td>
				</tr>
				<tr>
					<td style='font-size:12px; padding-right:10px;'><strong>MANUFACTURE:</strong></td>
					<td style='font-size:12px;' id="savedMake"></td>
				</tr>
				<tr>
					<td style='font-size:12px; padding-right:10px;'><strong>MODEL NAME:</strong></td>
					<td style='font-size:12px;' id="savedModelName"></td>
				</tr>
				<tr>
					<td style='font-size:12px; padding-right:10px;'><strong>FRIENDLY MODEL NAME:</strong></td>
					<td style='font-size:12px;' id="savedFriendlyModelName"></td>
				</tr>
				<tr>
					<td style='font-size:12px; padding-right:10px;'><strong>CATEGORY:</strong></td>
					<td style='font-size:12px;' id="savedCategory"></td>
				</tr>
			</table>
			<div style='font-size: 16px; margin-top:10px; margin-bottom: 10px;'>Talon Saved Details</div>
			<table>
				<tr>
					<td style='font-size:12px; padding-right:10px;'><strong>YEAR:</strong></td>
					<td style='font-size:12px;' id="savedTalonYear"></td>
				</tr>
				<tr>
					<td style='font-size:12px; padding-right:10px;'><strong>MANUFACTURE:</strong></td>
					<td style='font-size:12px;' id="savedTalonMake"></td>
				</tr>
				<tr>
					<td style='font-size:12px; padding-right:10px;'><strong>MODEL NAME:</strong></td>
					<td style='font-size:12px;' id="savedTalonModelName"></td>
				</tr>
			</table>
			<div style='font-size: 16px; margin-top:10px; margin-bottom: 10px;'>VINS</div>
			<div id="Vins"></div>
		</div>
	</div>
</div>

<div id="VinNumberDetails" style="display:none">
	<a href="javascript:void(0);" onclick="InventoryController.CloseVinDetails()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader"><span id="VinNumberHeader"></span>
			<a href="javascript:void(0);" onclick="InventoryController.CloseVinDetails()">
				<div class="close" style="margin-top: 4px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>	
		</div>
		<div class="FormContent" style="margin-right: 15px; overflow-y: auto; height: 370px;">
			<div>
				Would you like to delete/reassign this orphaned VIN number?<br /> <strong>(This action cannot be undone!)</strong>
				
				
			</div>
			<div style='margin-top:10px;'>
				<div id="inputID99">
					<div class="errorMessage"></div>
				</div>
				
				<form id="ReassignVin" method="post" action="<?php echo PATH ?>inventory/save/reassignvin">
					<input type='hidden' name='vinIDObject' id='vinIDObject' />
					<select name='reassignVin'>
						<option value='0'>Please Select Choice</option>
						<option value='-1'>Remove Vin</option>
						
						<?php 
							$manufactures = array_column($this -> ReassignBackupList, 'inventoryBackupManufactur');
							$ManufactureArray = array_unique($manufactures, SORT_REGULAR);
							sort($ManufactureArray);
						?>
						<?php foreach($ManufactureArray as $manufactureSingle): ?>
							<optgroup label='<?php echo $manufactureSingle ?>'>
								<?php 
									
									
									$filteredItems = array_filter($this -> ReassignBackupList, function ($var) use($manufactureSingle) {
										return $var['inventoryBackupManufactur'] == $manufactureSingle; 
									});
								?>
									
								<?php foreach($filteredItems as $backupItemSingle): ?>
									<?php $bikeName = $backupItemSingle['inventoryBackupYear'] . ' '. $backupItemSingle['inventoryBackupManufactur'] . ' '. $backupItemSingle['inventoryBackupModelName'] . ' '. $backupItemSingle['inventoryBackupModelFriendlyName'] ?>
									<option value='<?php echo $backupItemSingle['inventoryBackupID'] ?>'><?php echo $bikeName ?></option>
								<?php endforeach; ?>		
							</optgroup>
						<?php endforeach; ?>
						
						
						
						
					</select>	
					<div style='margin-top:15px;'>
						<input type='submit' value='Save' class='greenButton' />	
					</div>
				</form>
				
				
				
			</div>
		</div>
	</div>
</div>

