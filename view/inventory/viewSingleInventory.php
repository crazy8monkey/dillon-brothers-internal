<?php 	

	function multiexplode ($delimiters,$string) {
	    $ready = str_replace($delimiters, $delimiters[0], $string);
	    $launch = explode($delimiters[0], $ready);
	    return  $launch;
	}

?>

<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php echo $this -> InventorySingle -> GetInventorySpecBreadCrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								View Vehicle/Inventory
							</div>
						</div>
						<div class="row" style='margin-bottom:10px;'>
							<div class="col-md-4">
								<div style='border:1px solid #18ad00; color:#18ad00; text-align:center; padding:10px;'>
									<?php if($this -> InventorySingle -> InventoryActive == 1): ?>
										Vehicle Inventory is Active on website
									<?php else: ?>
										Vehicle Inventory is not active on website
									<?php endif; ?>	
								</div>
								
							</div>
							<div class="col-md-4">
								<div style='border:1px solid #7fbef1; color:#7fbef1; text-align:center; padding:10px;'>
									<?php if($this -> InventorySingle -> IsFeatureProduct == 1): ?>
										Vehicle Inventory is a featured product
									<?php else: ?>
										Vehicle Inventory is not a featured product
									<?php endif; ?>
								</div>
							</div>
							<div class="col-md-4">
								<div style='border:1px solid #e20e0e; color:#e20e0e; text-align:center; padding:10px;'>
									<?php if($this -> InventorySingle -> IsInService == 1): ?>
										Vehicle Inventory is in service
									<?php else: ?>
										Vehicle Inventory is not in service
									<?php endif; ?>
								</div>
							</div>
						</div>
						<?php if($this -> InventorySingle -> markAsSold != NULL): ?>
							<div class="row">
								<div class="col-md-12">
									This vehicle was marked as sold on <?php echo $this -> recordedTime -> formatDate($this -> InventorySingle -> markAsSold) ?>
								</div>
							</div>
						<?php endif; ?>
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">
									Vehicle Info.
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<table>
									<tr>
										<td><strong>Year: </strong></td>
										<td><?php echo $this -> InventorySingle -> Year ?></td>
									</tr>
									<tr>
										<td><strong>Manufacture: </strong></td>
										<td><?php echo $this -> InventorySingle -> Manufacture ?></td>
									</tr>
									<tr>
										<td><strong>Model: </strong></td>
										<td><?php echo $this -> InventorySingle -> Model ?></td>
									</tr>
									<tr>
										<td style='padding-right:10px;'><strong>Friendly Model Name: </strong></td>
										<td><?php echo $this -> InventorySingle -> ModelFriendlyName ?></td>
									</tr>
									<tr>
										<td style='padding-top:10px;'><strong>Category: </strong></td>
										<td style='padding-top:10px;'><?php echo $this -> InventorySingle -> CategoryName ?></td>
									</tr>
									<tr>
										<td><strong>MSRP: </strong></td>
										<td>$<?php echo number_format($this -> InventorySingle -> CurrentMSRP, 2)  ?></td>
									</tr>
									<tr>
										<td><strong>Mileage: </strong></td>
										<td><?php echo number_format($this -> InventorySingle -> CurrentMileage, 0)  ?></td>
									</tr>
									<?php if(!empty($this -> InventorySingle -> OverlayText)): ?>
									<tr>
										<td style='padding-top:10px;'><strong>Overlay Text: </strong></td>
										<td style='padding-top:10px;'><?php echo $this -> InventorySingle -> OverlayText ?></td>
									</tr>
									<?php endif; ?>
								</table>
							</div>
							<div class="col-md-6">
								<?php if(!empty($this -> InventorySingle -> YoutubeURL)) : ?>
									<div class="embed-responsive embed-responsive-16by9" style='margin-top:10px;'>
									<iframe class="embed-responsive-item" frameborder="0" allowfullscreen src="https://www.youtube.com/embed/<?php echo $this -> InventorySingle -> YoutubeURL ?>"></iframe>
								</div>
								<?php endif; ?>
							</div>
						</div>
						
						<?php if(!empty($this -> InventorySingle -> InventoryDescription)): ?>
						<div class="row">
							<div class="col-md-12">
								<div><strong>Inventory Description</strong></div>
								<?php echo $this -> InventorySingle -> InventoryDescription; ?>
							</div>
						</div>
						<?php endif; ?>
						
						<?php if(count($this -> photos) > 0): ?>
							<div class="row">
								<div class="col-md-12" style='margin-top:15px;'>
									<div class="subHeader">
										Photos.
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<img src='<?php echo PHOTO_URL ?>inventory/<?php echo $this -> InventorySingle -> VinNumber ?>/<?php echo $this -> photos[0]['inventoryPhotoName'] . '-l.' . $this -> photos[0]['inventoryPhotoExt']?>' style='width:100%' />	
								</div>
								<div class="col-md-6">
									<?php foreach($this -> photos as $key => $photo): ?>
										<?php if($key != 0): ?>
											<img src='<?php echo PHOTO_URL ?>inventory/<?php echo $this -> InventorySingle -> VinNumber ?>/<?php echo $photo['inventoryPhotoName'] . '-s.' . $photo['inventoryPhotoExt'] ?>' style='width: 140px;' />
										<?php endif; ?>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
						
						<?php if($this -> InventorySingle -> IsVehicleInfoCatched == 1): ?>						
							<div class="row">
								<div class="col-md-12" style='margin-top:15px;'>
									<div class="subHeader">
										Specifications
									</div>
								</div>
							</div>
							
							
						
							<?php $specDecoded = json_decode($this -> InventorySingle -> GetSpecs(), true); ?>
							<?php foreach($this -> specgroups as $specgroupSingle): ?>
								
								<?php 
									$groupSpecID = $specgroupSingle['specGroupID'];
											
									$filteredSpecs = array_filter($this -> speclabels, function ($var) use($groupSpecID) {
										 return $var['relatedSpecGroupID'] == $groupSpecID; 
										}
									);	
								
								?>
								<?php if(count($filteredSpecs) > 0): ?>
									<div class="readOnySpecGroupHeader"><?php echo $specgroupSingle['specGroupText'] ?></div>
									<table style='margin-bottom:10px; width:100%'>
										<?php foreach($filteredSpecs as $specLabelSingle): ?>
											<tr>
												<td class='readOnlySpecLabel'><?php echo $specLabelSingle['labelText'] ?>:</td>
												<td>
													<?php 
														$key = array_search($specLabelSingle["specLabelID"], array_column($specDecoded, 'LabelID'));
															
														if($key !== false) {
														 	echo $specDecoded[$key]['Content'];
														}
													?>
													
													
												</td>
											</tr>
										<?php endforeach; ?>
									</table>
								<?php endif; ?>
							<?php endforeach; ?>
						<?php endif; ?>	
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-6">
								<div style='margin-bottom:20px;'>
													<div><strong>Stock #</strong></div>
													<?php echo $this -> InventorySingle -> Stock ?>
								</div>
								<div style='margin-bottom:20px;'>
									<div><strong>Vin Number</strong></div>
									<?php echo $this -> InventorySingle -> VinNumber ?>
									<?php if($this -> InventorySingle -> Conditions == 0) : ?>
										<strong style='color:red'>(NEW)</strong>
									<?php else: ?>
										<strong style='color:red'>(USED)</strong>
									<?php endif; ?>	
								</div>
								<div>
									<div><strong>Color</strong></div>
									<?php
										$colors = multiexplode(array('/', '.'), $this -> InventorySingle -> Color);
										$FinalColors = array();
										foreach($colors as $colorSingle) {
											$key = array_search($colorSingle, array_column($this -> convertedcolors, 'keyPhrase'));	
											if($key !== false) {
												array_push($FinalColors, str_replace($colorSingle, $this -> convertedcolors[$key]['ColorText'], $colorSingle));
											} else {
												array_push($FinalColors, $colorSingle);
											}
										}
														
										echo implode("/",$FinalColors);
									?>
								</div>
							</div>
							<div class="col-md-6">
								<div>
									<div><strong>Location</strong></div>
									<?php 
										switch($this -> InventorySingle -> StoreLocation) {
											case 'A':
												echo "Dillon Brothers Harley-Davidson<br />3838 N HWS Cleveland Blvd<br /> Omaha, NE 68116";
												break;
											case 'B':
												echo "Dillon Brothers Harley-Davidson<br />2440 East 23rd Street Fremont, NE 68025";
												break;		
											case 'C':
												echo "Dillon Brothers Motorsport<br />3848 N HWS Cleveland Blvd<br /> Omaha, NE 68116";
												break;
											case 'D':
												echo "Dillon Brothers Indian<br />3840 N 174th Ave.<br /> Omaha, NE 68116";
												break;	
										}			
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Internal Feedback
							</div>
						</div>
						<form method="post" id="CurrentInventoryFeedBack" action="<?php echo PATH ?>inventory/feedback/<?php echo $this -> InventorySingle -> GetID()?>">
							<div class="row">
								<div class="col-md-12">
									<div id="FeedBackID1">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Notes</div>
											<textarea id="internalFeedBackNotes" name="internalFeedBackNotes" onchange="Globals.removeFeedBackValidationMessage('FeedBackID1')"></textarea>
										</div>
																										
									</div>
									
									
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Save Notes" class="greenButtonRectangle">
								</div>
							</div>
						</form>
					</div>
					<div class="content" style='border-top: 1px solid #e0e0e0;'>
						<?php if(count($this -> feedBackList) > 0): ?>
							<div id="FeedBackList">
								<?php foreach($this -> feedBackList as $item): ?>
									<div class="FeedBackItem">
										<div class="bubble">
											<?php echo $item['FeedbackText'] ?>
											<div class="caret"></div>
										</div>
										<div class="detailsBy">
											<?php $submittedDetails = explode('T', $item['feedBackSubmitted']); ?>
											<?php echo $item['firstName'] ?> <?php echo $item['lastName'] ?> | <?php echo $this -> recordedTime -> formatDate($submittedDetails[0]) ?> - <?php echo $this -> recordedTime -> formatTime($submittedDetails[1]) ?>
										</div>
									</div>
								<?php endforeach; ?>
								
								
							</div>
						<?php else: ?>
							<div id="FeedBackList">There is no feed back on this item</div>
						<?php endif; ?>	
						
						<div class="FeedBackItem" id="newFeedBackItem" style='display:none;'>
									<div class="bubble"></div>
									<div class="detailsBy"></div>
								</div>
						
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>

<div class="container">
	
	
	
</div>




