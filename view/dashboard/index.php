<!--<div class="container-fluid" style='background: #f7f7f7; margin-bottom: 10px;'>
	
</div> -->


<script>
(function(w,d,s,g,js,fs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));


DashboardController.GenerateDillonBrothersMainAnalytics('<?php echo $this -> googleAnalytics ?>');

</script>


<div class="ContentPage">
	<div class="container-fluid">
		<div class="WhiteSectionDiv">
			<div class="content">
				<div class="row">
					<div class="col-md-12 sectionHeader">
						Website Traffic
					</div>
				</div>
				<div class="row" style="margin: 0px;">
					<div class="col-md-12" style='padding: 0px;'>
						<div id="chart-1-container">
							<div style='text-align:center;'>
								<img src='<?php echo PATH ?>public/images/ajax-loader.gif' />	
							</div>
						</div>
						<div id="chart-2-container"></div>
					</div>
				</div>
			</div>
		</div>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SocialMediaPermissionType') == 1) : ?>
			<div class="WhiteSectionDiv">
				<div class="content">
					<div class="row">
						<div class="col-md-12 sectionHeader">
							Your Stores: Social Media Submissions
						</div>
					</div>
					<div class="row">
						<?php foreach ($this -> SocialMediaList as $store): ?>
							<div class="col-md-12" style='margin-bottom: 15px;'>
								<div class="StoreName" style='font-size:16px;'>
									<strong><?php echo ceil($store['WeeklyPercentage']  * 100) ?>%</strong> | <a href="<?php echo PATH ?>socialmedia/yourstore/<?php echo $store['storeID'] ?>"><?php echo $store['StoreName'] ?></a> 	
								</div>
								<div class="row">
									<div class="col-md-12">
										<div style='width:100%; margin-top:0px;' class="progressBar">
											<div style='width:<?php echo ceil($store['WeeklyPercentage'] * 100) ?>%' class="progressContent"></div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php 
		
		if(count($this -> RightSideContent) > 0) {	
			$className = 'col-md-9';
		} else {
			$className = 'col-md-12';
		}
		?>
		
	
		
		<div class="row">
			<div class="<?php echo $className ?>">
				<div class="WhiteSectionDiv">
					<div class="content">
						
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Inventory
							</div>
						</div>
						<!--<div class="row">
							<div class="col-md-12" style='margin-bottom:10px;'>
								<div class="storeChip" style='background:#c30000'>
									Dillon Brothers MotorSports
								</div>
								<div class="storeChip" style='background:#ff6c00'>
									Dillon Brothers Harley-Davidson
								</div>
								<div class="storeChip" style='background:#850029'>
									Dillon Brothers Indian Motorcycle
								</div>
							</div>
						</div>-->
						<div class="row">
							<div class="col-md-12 subHeader">
								Need Photos
							</div>
						</div>
						
						<?php 
							$newCondition = 0;
							$usedCondition = 1;					
															
							$filteredNewPhotoInventory = array_filter($this -> InventoryNeedPhotos, function ($var) use($newCondition) {
								return $var['Conditions'] == $newCondition; 
							});
							
							$filteredUsedPhotoInventory = array_filter($this -> InventoryNeedPhotos, function ($var) use($usedCondition) {
								return $var['Conditions'] == $usedCondition; 
							});
						
						?>
						
						
						
						<div class="row" style='margin:0px;'>
							<div class="col-md-6" style='padding:0px 5px 0px 0px'>
								<div style='margin-bottom:10px;'><strong>New (<?php echo count($filteredNewPhotoInventory); ?>)</strong></div>
								<?php if(count($filteredNewPhotoInventory) > 0): ?>
									<div class="inventoryNeededList">								
										<?php foreach($filteredNewPhotoInventory as $inventorySingle): ?>
											<div class="inventoryLine">
												<div class="row" style='margin:0px;'>
													<div class="col-xs-2" style='padding: 0px;'>
														<a href="<?php echo $this -> pages -> inventory(); ?>/edit/<?php echo $inventorySingle['inventoryID']; ?>"><?php echo $inventorySingle['Stock'] ?></a> 
													</div>
													<div class="col-xs-10" style='padding: 0px;'>
														<?php 
															switch($inventorySingle['storeID']) {
																case 2:
																	$storeClass = 'motorsportIndicator';
																	break;
																case 3:
																	$storeClass = 'harleyIndicator';
																	break;
																case 4:
																	$storeClass = 'indianIndicator';
																	break;
															}
														
														?>
														<div class="<?php echo $storeClass ?>" data-placement="right" data-toggle="tooltip" title='<?php echo $inventorySingle['StoreName']; ?>'></div>
														<div class="vehicleNameList">
															<?php echo $inventorySingle['Year'] ?> <?php echo $inventorySingle['Manufacturer'] ?> <?php echo $inventorySingle['ModelName'] ?> <?php echo $inventorySingle['FriendlyModelName'] ?> 
															<?php if($inventorySingle['MarkAsSoldDate']): ?>
																<strong style="color:red">(SOLD)</strong>
															<?php endif; ?>
														</div>
														
														
													</div>
													
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
							</div>
							<div class="col-md-6" style='padding:0px 0px 0px 5px'>
								<div style='margin-bottom:10px;'><strong>Used (<?php echo count($filteredUsedPhotoInventory); ?>)</strong></div>
								<div class="inventoryNeededList">
									<?php foreach($filteredUsedPhotoInventory as $inventorySingle): ?>
										<div class="inventoryLine">
											<div class="row" style='margin:0px;'>
												<div class="col-xs-2" style='padding: 0px;'>
													<a href="<?php echo $this -> pages -> inventory(); ?>/edit/<?php echo $inventorySingle['inventoryID']; ?>"><?php echo $inventorySingle['Stock'] ?></a>
												</div>
												<div class="col-xs-10" style='padding: 0px;'>
													<?php 
															switch($inventorySingle['storeID']) {
																case 2:
																	$storeClass = 'motorsportIndicator';
																	break;
																case 3:
																	$storeClass = 'harleyIndicator';
																	break;
																case 4:
																	$storeClass = 'indianIndicator';
																	break;
															}
														
														?>
													<div class="<?php echo $storeClass ?>" data-placement="right" data-toggle="tooltip" title='<?php echo $inventorySingle['StoreName']; ?>'></div>
													<div class="vehicleNameList">
														<?php echo $inventorySingle['Year'] ?> <?php echo $inventorySingle['Manufacturer'] ?> <?php echo $inventorySingle['ModelName'] ?> <?php echo $inventorySingle['FriendlyModelName'] ?>
														<?php if($inventorySingle['MarkAsSoldDate']): ?>
															<strong style="color:red">(SOLD)</strong>
														<?php endif; ?>
													</div>
												</div>
												
											</div>
										</div>
									<?php endforeach; ?>								
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 subHeader" style='margin-top:15px;'>
								Need Pricing ($400 or less)
							</div>
						</div>
						
						<?php 
							
							$filteredNewPricingInventory = array_filter($this -> InventoryNeedsPricing, function ($var) use($newCondition) {
								return $var['Conditions'] == $newCondition; 
							});
							
							$filteredUsedPricingInventory = array_filter($this -> InventoryNeedsPricing, function ($var) use($usedCondition) {
								return $var['Conditions'] == $usedCondition; 
							});
						?>
						<div class="row" style='margin:0px;'>
							<div class="col-md-6" style='padding:0px 5px 0px 0px'>
								<div style='margin-bottom:10px;'><strong>New (<?php echo count($filteredNewPricingInventory); ?>)</strong></div>
								<?php if(count($filteredNewPricingInventory) > 0): ?>
									<div class="inventoryNeededList">								
										<?php foreach($filteredNewPricingInventory as $inventorySingle): ?>
											<div class="inventoryLine">
												<div class="row" style='margin:0px;'>
													<div class="col-xs-2" style='padding: 0px;'>
														<a href="<?php echo $this -> pages -> inventory(); ?>/edit/<?php echo $inventorySingle['inventoryID']; ?>"><?php echo $inventorySingle['Stock'] ?></a>
													</div>
													<div class="col-xs-10" style='padding: 0px;'>
														<?php 
															switch($inventorySingle['storeID']) {
																case 2:
																	$storeClass = 'motorsportIndicator';
																	break;
																case 3:
																	$storeClass = 'harleyIndicator';
																	break;
																case 4:
																	$storeClass = 'indianIndicator';
																	break;
															}
														
														?>
														<div class="<?php echo $storeClass ?>" data-placement="right" data-toggle="tooltip" title='<?php echo $inventorySingle['StoreName']; ?>'></div>
														<div class="vehicleNameList">
															<?php echo $inventorySingle['Year'] ?> <?php echo $inventorySingle['Manufacturer'] ?> <?php echo $inventorySingle['ModelName'] ?> <?php echo $inventorySingle['FriendlyModelName'] ?> 	
															<?php if($inventorySingle['MarkAsSoldDate']): ?>
																<strong style="color:red">(SOLD)</strong>
															<?php endif; ?>
														</div>
													</div>
													
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
							</div>
							<div class="col-md-6" style='padding:0px 0px 0px 5px'>
								<div style='margin-bottom:10px;'><strong>Used (<?php echo count($filteredUsedPricingInventory); ?>)</strong></div>
								<?php if(count($filteredUsedPricingInventory) > 0): ?>
									<div class="inventoryNeededList">								
										<?php foreach($filteredUsedPricingInventory as $inventorySingle): ?>
											<div class="inventoryLine">
												<div class="row" style='margin:0px;'>
													<div class="col-xs-2" style='padding: 0px;'>
														<a href="<?php echo $this -> pages -> inventory(); ?>/edit/<?php echo $inventorySingle['inventoryID']; ?>"><?php echo $inventorySingle['Stock'] ?></a>
													</div>
													<div class="col-xs-10" style='padding: 0px;'>
														<?php 
															switch($inventorySingle['storeID']) {
																case 2:
																	$storeClass = 'motorsportIndicator';
																	break;
																case 3:
																	$storeClass = 'harleyIndicator';
																	break;
																case 4:
																	$storeClass = 'indianIndicator';
																	break;
															}
														
														?>
														<div class="<?php echo $storeClass ?>" data-placement="right" data-toggle="tooltip" title='<?php echo $inventorySingle['StoreName']; ?>'></div>
														<div class="vehicleNameList">
															<?php echo $inventorySingle['Year'] ?> <?php echo $inventorySingle['Manufacturer'] ?> <?php echo $inventorySingle['ModelName'] ?> <?php echo $inventorySingle['FriendlyModelName'] ?> 
															<?php if($inventorySingle['MarkAsSoldDate']): ?>
																<strong style="color:red">(SOLD)</strong>
															<?php endif; ?>
														</div>
														
													</div>
													
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<?php if(count($this -> RightSideContent) > 0): ?>
				<div class="col-md-3">
					<?php if(in_array('ShowBlogComments', $this -> RightSideContent)): ?>	
						<div class="WhiteSectionDiv" id="BlogPostCommentContent">
							<div class="content">
								<div class="row">
									<div class="col-md-12 sectionHeader" style='margin-bottom: 10px;'>
										Blog
									</div>
								</div>
								<div class="row" style='margin: 0px;'>
									<div style='border-top:1px solid #e0e0e0; margin-left: -10px; margin-right: -10px; margin-bottom:10px;'></div>
									<div class="col-md-8" style='padding: 0px;'>
										<div style='font-size:20px;'>
											<?php if(count($this -> unapprovecomments) > 0) {
												echo "<span style='color:#c30000'>" . count($this -> unapprovecomments) . "</span>";
											} else {
												echo count($this -> unapprovecomments);	
											}
											?> New Comments
										</div>
									</div>
									<div class="col-md-4" style='padding: 0px;'>
										<div class="ViewCommentsLink">
											<a href="<?php echo PATH ?>blog/comments">
												<div class="viewCommentLink">
													View all Comments
												</div>
											</a>	
										</div>
										
									</div>
								</div>
							
							</div>
						</div>
					<?php endif; ?>
					
					<?php if(in_array('ShoppingCartAccess', $this -> RightSideContent)): ?>	
						<div class="WhiteSectionDiv" id="ShoppingReviewsContent">
							<div class="content">
								<div class="row">
									<div class="col-md-12 sectionHeader" style='margin-bottom: 10px;'>
										Shopping Reviews
									</div>
								</div>
								<div class="row" style='margin: 0px;'>
									<div style='border-top:1px solid #e0e0e0; margin-left: -10px; margin-right: -10px; margin-bottom:10px;'></div>
									<div class="col-md-8" style='padding: 0px;'>
										<div style='font-size:20px;'>
											<?php if(count($this -> NewReviews) > 0) {
												echo "<span style='color:#c30000'>" . count($this -> NewReviews) . "</span>";
											} else {
												echo count($this -> NewReviews);	
											}
											?> New Shopping Reivews
										</div>
									</div>
									<div class="col-md-4" style='padding: 0px;'>
										<div class="ViewCommentsLink">
											<a href="<?php echo PATH ?>shop/reviews">
												<div class="viewReviewLink">
													View all Reviews
												</div>
											</a>	
										</div>		
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>	
					
					
					<?php if(in_array('AdminSocialMediaList', $this -> RightSideContent)): ?>
						<div class="WhiteSectionDiv" id="SocialMediaSummary">
							<div class="content">
								<div class="row">
									<div class="col-md-12 sectionHeader" style='margin-bottom: 10px;'>
										Social Media List
									</div>
								</div>
								<div class="row" style='margin: 0px -10px;'>
									<div style='border-top:1px solid #e0e0e0; margin-left:0px; margin-right:0px;'></div>
									<div class="col-md-12" style='padding: 0px;'>
										<div class="CurrentWeek">
											Current Week: <strong><?php echo $this -> recordedTime -> formatShortDate($this -> StartOfWeek) ?> - <?php echo $this -> recordedTime -> formatShortDate($this -> EndOfWeek) ?></strong>
										</div>		
									</div>
								</div>
								<div class="row" style='margin: 0px;'>
									<div style='border-top:1px solid #e0e0e0; margin-left: -10px; margin-right: -10px;'></div>
									<div class="col-md-12" style='padding: 0px;'>
										<?php foreach($this -> AllSocialMediaList as $store): ?>
											<div style='margin-top:15px;'>
												<div class="StoreName">
													<strong><?php echo ceil($store['TotalSubmittedContent']  * 100) ?>%</strong> | <a href="<?php echo PATH ?>socialmedia/view/store/<?php echo $store['storeID'] ?>"><?php echo $store['StoreName'] ?></a> 	
												</div>
												<div class="row">
													<div class="col-md-12">
														<div style='width:100%; margin-top:0px;' class="progressBar">
															<div style='width:<?php echo ceil($store['TotalSubmittedContent'] * 100) ?>%' class="progressContent"></div>
														</div>
													</div>
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								</div>
							
							</div>
						</div>
					<?php endif; ?>
					
									
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

