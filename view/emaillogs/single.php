<?php 
	$contentOfEmail = json_decode($this -> emailSingle -> ContentOfLead, true);
	$vehicleInfo = json_decode($this -> emailSingle -> VehicleInfo, true);
?>
<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>emaillogs">Email Log Summary</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>View Email Log
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Viewing Email Log
							</div>
						</div>
						<?php if($this -> emailSingle -> TypeOfLead != 8): ?>
							<div class="row">
								<div class="col-md-4">
									<div class="row">
										<div class="col-md-12">
											<div style='margin-bottom:10px;'>
												<?php echo $this -> emailSingle -> GenerateEmailImage();?>	
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div>Email Type</div>
											<div class='EmailTypeHeader'>
												<?php echo $this -> emailSingle -> GenerateEmailTypeHeader();?> 
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-md-4" style='margin-bottom: 10px;'>
									<div style='margin-bottom:10px;'>
										<img src='<?php echo PATH ?>public/images/EmailSource.png' />
									</div>
									<div>Source</div>
									<div class='EmailTypeHeader'>
										<?php echo $this -> emailSingle -> GenerateSource(); ?>
									</div>
									<div style='font-size:10px;'><?php echo $this -> emailSingle -> GenerateTime() ?></div>
								</div>
								<div class="col-md-4" style='margin-bottom: 10px;'>
									<div style='margin-bottom:10px;'>
										<img src='<?php echo PATH ?>public/images/StoreInfo.png' />
									</div>
									<div>Store</div>
									<div class='EmailTypeHeader'>
										<?php echo $this -> emailSingle -> GenerateStoreName(); ?>
									</div>
								</div>
								<div style='border-bottom:1px solid #ececec; clear:both; margin-top:10px; margin-left:5px; margin-right:5px; margin-bottom:20px;'></div>
							</div>
						<?php else: ?>
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
											<div style='margin-bottom:10px;'>
												<?php echo $this -> emailSingle -> GenerateEmailImage();?>	
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div>Email Type</div>
											<div class='EmailTypeHeader'>
												<?php echo $this -> emailSingle -> GenerateEmailTypeHeader();?> 
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-md-6" style='margin-bottom: 10px;'>
									<div style='margin-bottom:10px;'>
										<img src='<?php echo PATH ?>public/images/EmailSource.png' />
									</div>
									<div>Source</div>
									<div class='EmailTypeHeader'>
										<?php echo $this -> emailSingle -> GenerateSource(); ?>
									</div>
									<div style='font-size:10px;'><?php echo $this -> emailSingle -> GenerateTime() ?></div>
								</div>
								<div style='border-bottom:1px solid #ececec; clear:both; margin-top:10px; margin-left:5px; margin-right:5px; margin-bottom:0px;'></div>
							</div>
						<?php endif; ?>
						
						
						<div class="row">
							<div class="col-md-6">
								<?php if($this -> emailSingle -> TypeOfLead != 8): ?>
									<div style='margin-bottom:10px;'>
										<img src='<?php echo PATH ?>public/images/PersonEntered.png' />
									</div>
								<?php endif; ?>	
								
								<?php if($this -> emailSingle -> TypeOfLead == 1): ?>
									<div class="emailSection">
										<div><strong>Person entering information</strong></div>
										<?php echo $contentOfEmail['ReferredPerson'] ?><br />
										<a href='mailto:<?php echo $contentOfEmail['ReferredPersonEmail'] ?>'><?php echo $contentOfEmail['ReferredPersonEmail'] ?></a>										
									</div>
									<div class="emailSection">
										<div><strong>Friends Email:</strong></div>
										<?php echo $contentOfEmail['FriendsEmail'] ?>
									</div>
									
									<div class="emailSection">
										<div><strong>Personal Message:</strong></div>
										<?php echo $contentOfEmail['PersonalMessage'] ?>
									</div>
								<?php endif; ?>
								
								<?php if($this -> emailSingle -> TypeOfLead == 2): ?>
									<div class="emailSection">
										<div><strong>Person entering information</strong></div>
										<div><?php echo $contentOfEmail['OnlineOfferPerson'] ?></div>
										<a href='mailto:<?php echo $contentOfEmail['OnlineOfferEmail'] ?>'><?php echo $contentOfEmail['OnlineOfferEmail'] ?></a>
									</div>
									<div><strong>Personal Message:</strong></div>
									<?php echo $contentOfEmail['Comments'] ?>
								<?php endif; ?>	
								
								<?php if($this -> emailSingle -> TypeOfLead == 3): ?>
									<div class="emailSection">
										<div><strong>Person entering information</strong></div>
										<div><?php echo $contentOfEmail['FullName'] ?></div>
										<?php echo $contentOfEmail['ContactInfo'] ?>
									</div>
									<div class="emailSection">
										<div class="row">
											<div class="col-md-6">
												<div><strong>Added Accessories:</strong></div>	
												<?php 
													if(!empty($contentOfEmail['AddedAccessoriesInfo'])) {
														echo $contentOfEmail['AddedAccessoriesInfo'];
													} else {
														echo "NA";
													}
												?>	
											</div>
											<div class="col-md-6">
												<div><strong>Trade Comments:</strong></div>	
												<?php
													if(!empty($contentOfEmail['TradeComments'])) {
														echo $contentOfEmail['TradeComments'];
													} else {
														echo "NA";
													}
												?>
											</div>
										</div>
									</div>
								<?php endif; ?>	
								
								<?php if($this -> emailSingle -> TypeOfLead == 4): ?>
									<div class="emailSection">
										<div><strong>Person entering information</strong></div>
										<div><?php echo $contentOfEmail['FullName'] ?></div>
										<?php echo $contentOfEmail['ContactInfo'] ?>
									</div>
									<div class="emailSection">
										<div><strong>Date of Ride</strong></div>
										<?php 
											$dateFormated = $this-> recordedTime -> FormatExpectedDate($contentOfEmail['DateOfRide']);
										
											echo $this-> recordedTime -> formatShortDate($dateFormated); 
										?>
									</div>
									
								<?php endif; ?>	
								
								<?php if($this -> emailSingle -> TypeOfLead == 5): ?>
									<div class="emailSection">
										<div><strong>Person entering information</strong></div>	
										<div><?php echo $contentOfEmail['FullName'] ?></div>
										<?php echo $contentOfEmail['ContactInfo'] ?>
									</div>
									<div class="emailSection">
										<div><strong>Comments</strong></div>
										<?php echo $contentOfEmail['CommentsConcerns']; ?>
									</div>
									
									
								<?php endif; ?>	
								
								<?php if($this -> emailSingle -> TypeOfLead == 6): ?>
									<div class="emailSection">
										<div><strong>Person entering information</strong></div>
										<div><?php echo $contentOfEmail['ContactInformation']['FullName'] ?></div>
									</div>
									<div class="emailSection">
										<div class="row">
											<div class="col-md-4">
												<div><strong>Home Phone:</strong></div>
												<?php echo $contentOfEmail['ContactInformation']['HomePhone'] ?>
											</div>
											<div class="col-md-4">
												<div><strong>Cell Phone:</strong></div>
												<?php echo $contentOfEmail['ContactInformation']['CellPhone'] ?>	
											</div>
											<div class="col-md-4">
												<div><strong>Email Address:</strong></div>
												<a href='mailto:<?php echo $contentOfEmail['ContactInformation']['EmailAddress'] ?>'><?php echo $contentOfEmail['ContactInformation']['EmailAddress'] ?></a>
											</div>
										</div>
									</div>
									<div class="emailSection">
										<div><strong>Address:</strong></div>
										<?php echo $contentOfEmail['ContactInformation']['Address'] ?>
									</div>
									<div style='margin-bottom:10px;'>
										<img src='<?php echo PATH ?>public/images/ServiceInfo.png' />
									</div> 
									<div class="emailSection">
										<div><strong>Previously Serviced:</strong></div>
										<?php 
											if($contentOfEmail['ServiceDetailInformation']['PreviousServiced'] == 1) {
												echo "Yes";	
											} else {
												echo "No";	
											} 
										?>
									</div>
									<div class="emailSection">
										<div class="row">
											<?php if($contentOfEmail['ServiceDetailInformation']['PreviousServiced'] == 1): ?>
												<div class="col-md-6">
													<div style='margin-bottom:10px;'>
														<div><strong>Last Service Date:</strong></div>
														<?php 
															$LastServiceDateFormated = $this-> recordedTime -> FormatExpectedDate($contentOfEmail['ServiceDetailInformation']['LastServicedDate']);														
															echo $this-> recordedTime -> formatShortDate($LastServiceDateFormated); 
														?>
													</div>
													<div style='margin-bottom:10px;'>
														<div><strong>Last Service Work Done:</strong></div>
														<?php echo $contentOfEmail['ServiceDetailInformation']['LastServiceWorkDone']; ?>
													</div>
												</div>
											<?php endif; ?>
											
											<div class="col-md-6">
												<div style='margin-bottom:10px;'>
													<div><strong>New Appointment Date</strong></div>
													<?php 
														$NewServiceDateFormated = $this-> recordedTime -> FormatExpectedDate($contentOfEmail['ServiceDetailInformation']['NewAppointmentDate']);														
														echo $this-> recordedTime -> formatShortDate($NewServiceDateFormated); 
													?>
												</div>
												<div style='margin-bottom:10px;'>
													<div><strong>New Work Done</strong></div>
													<?php echo $contentOfEmail['ServiceDetailInformation']['NewServiceWorkNeed']; ?>
												</div>
											</div>
										</div>
									</div>	
								<?php endif; ?>	
								
								<?php if($this -> emailSingle -> TypeOfLead == 7): ?>
									<div class="emailSection">
										<div><strong>Person entering information</strong></div>
										<div><?php echo $contentOfEmail['ContactInformation']['FullName'] ?></div>
									</div>
									<div class="emailSection">
										<div class="row">
											<div class="col-md-4">
												<div><strong>Home Phone:</strong></div>
												<?php echo $contentOfEmail['ContactInformation']['HomePhone'] ?>
											</div>
											<div class="col-md-4">
												<div><strong>Cell Phone:</strong></div>
												<?php echo $contentOfEmail['ContactInformation']['CellPhone'] ?>	
											</div>
											<div class="col-md-4">
												<div><strong>Email Address:</strong></div>
												<a href='mailto:<?php echo $contentOfEmail['ContactInformation']['EmailAddress'] ?>'><?php echo $contentOfEmail['ContactInformation']['EmailAddress'] ?></a>
											</div>
										</div>
									</div>
									<div class="emailSection">
										<div><strong>Address:</strong></div>
										<?php echo $contentOfEmail['ContactInformation']['Address'] ?>
									</div>
									<div style='margin-bottom:10px;'>
										<img src='<?php echo PATH ?>public/images/ServiceInfo.png' />
									</div> 
									<div class="emailSection">
										<div class="row">
											<div class="col-md-6">
												<div><strong>Parts Requested:</strong></div>
												<?php echo $contentOfEmail['PartRequestInformation']['PartsNeeded'] ?>
											</div>
											<div class="col-md-6">
												<div><strong>Part Numbers Requested:</strong></div>
												<?php echo $contentOfEmail['PartRequestInformation']['PartNumbers'] ?>	
											</div>
										</div>
									</div>
								<?php endif; ?>	
								<?php if($this -> emailSingle -> TypeOfLead == 9): ?>
									<div class="emailSection">
										<div><strong>Person entering information</strong></div>
										<div><?php echo $contentOfEmail['ContactInformation']['FullName'] ?></div>
									</div>
									<div class="emailSection">
										<div class="row">
											<div class="col-md-4">
												<div><strong>Home Phone:</strong></div>
												<?php echo $contentOfEmail['ContactInformation']['HomePhone'] ?>
											</div>
											<div class="col-md-4">
												<div><strong>Cell Phone:</strong></div>
												<?php echo $contentOfEmail['ContactInformation']['CellPhone'] ?>	
											</div>
											<div class="col-md-4">
												<div><strong>Email Address:</strong></div>
												<a href='mailto:<?php echo $contentOfEmail['ContactInformation']['EmailAddress'] ?>'><?php echo $contentOfEmail['ContactInformation']['EmailAddress'] ?></a>
											</div>
										</div>
									</div>
									<div class="emailSection">
										<div><strong>Address:</strong></div>
										<?php echo $contentOfEmail['ContactInformation']['Address'] ?>
									</div>
								<?php endif; ?>		
							</div>
							<div class="col-md-6">
								<?php if($this -> emailSingle -> TypeOfLead == 1 || 
										 $this -> emailSingle -> TypeOfLead == 2 ||
										 $this -> emailSingle -> TypeOfLead == 3 ||
										 $this -> emailSingle -> TypeOfLead == 4 ||
										 $this -> emailSingle -> TypeOfLead == 5): ?>
									<div style='margin-bottom:10px;'>
										<img src='<?php echo PATH ?>public/images/Inventory.png' />
									</div> 
									<div class="emailSection">
										<div style='margin-bottom:20px'>
											<div><strong>Vehicle Information</strong></div>	
											<?php echo $vehicleInfo['VehicleName'] ?>
										</div>
										<div style='margin-bottom:20px'>
											<table class='emailDetailsTable'>
												<tr>
													<td><strong>VIN:</strong></td>
													<td><?php echo $vehicleInfo['VIN'] ?></td>
												</tr>
												<tr>
													<td><strong>Color:</strong></td>
													<td><?php echo $vehicleInfo['Color'] ?></td>
												</tr>
												<tr>
													<td><strong>Stock #:</strong></td>
													<td><?php echo $vehicleInfo['StockNumber'] ?></td>
												</tr>
												<tr>
													<td><strong>Mileage:</strong></td>
													<td><?php echo $vehicleInfo['Mileage'] ?></td>
												</tr>
											</table>
										</div>
										<div><strong>Location</strong></div>
										<?php echo $vehicleInfo['AddressLocation'] ?>
									</div>
									
									
									
								<?php endif; ?>
								
								<?php if($this -> emailSingle -> TypeOfLead == 6): ?>
									<div style='margin-bottom:10px;'>
										<img src='<?php echo PATH ?>public/images/Inventory.png' />
									</div> 
									<div class="emailSection">
										<div class="row">
											<div class="col-md-4">
												<div><strong>Year:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['VehicleYear'] ?>
											</div>
											<div class="col-md-4">
												<div><strong>Make:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['VehicleMake'] ?>
											</div>
											<div class="col-md-4">
												<div><strong>Model:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['VehicleModel'] ?>
											</div>
										</div>
									</div>
									<div class="emailSection">
										<div class="row">
											<div class="col-md-4">
												<div><strong>Miles:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['Miles'] ?>
											</div>
											<div class="col-md-4">
												<div><strong>VIN:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['VinNumber'] ?>
											</div>
										</div>
									</div>									
								<?php endif; ?>
								
								<?php if($this -> emailSingle -> TypeOfLead == 7): ?>
									<div style='margin-bottom:10px;'>
										<img src='<?php echo PATH ?>public/images/Inventory.png' />
									</div> 
									<div class="emailSection">
										<div class="row">
											<div class="col-md-4">
												<div><strong>Year:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['VehicleYear'] ?>
											</div>
											<div class="col-md-4">
												<div><strong>Make:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['VehicleMake'] ?>
											</div>
											<div class="col-md-4">
												<div><strong>Model:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['VehicleModel'] ?>
											</div>
										</div>
									</div>
									<div class="emailSection">
										<div class="row">
											<div class="col-md-4">
												<div><strong>Miles:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['Miles'] ?>
											</div>
											<div class="col-md-4">
												<div><strong>VIN:</strong></div>
												<?php echo $contentOfEmail['VehicleInformation']['VinNumber'] ?>
											</div>
										</div>
									</div>									
								<?php endif; ?>
								<?php if($this -> emailSingle -> TypeOfLead == 9): ?>
									<div class="emailSection">
										<img src='<?php echo PATH ?>public/images/ServiceInfo.png' />
									</div> 
									<div class="emailSection">
										<div><strong>Comments</strong></div>
										<?php echo $contentOfEmail['AdditionalInformation'] ?>
									</div>
								<?php endif; ?>	
							</div>
						</div>
						
						<?php if($this -> emailSingle -> TypeOfLead == 8): ?>
							<div class="row" style='margin-top:10px;'>
								<div class="col-md-4">
									<div class="emailSection">
										<img src='<?php echo PATH ?>public/images/ServiceInfo.png' />
									</div> 
									<div class="emailSection">
										<div><strong>Dillon Brothers Customer Relation</strong></div>
										<?php echo $contentOfEmail['ContactInformation']['CustomerRelation'] ?>
									</div>
									<div class="emailSection">
										<div><strong>Contact Information</strong></div>
										<div><?php echo $contentOfEmail['ContactInformation']['EventContactName'] ?></div>
										<a href='mailto:<?php echo $contentOfEmail['ContactInformation']['EventContactEmail'] ?>'><?php echo $contentOfEmail['ContactInformation']['EventContactEmail'] ?></a> / <?php echo $contentOfEmail['ContactInformation']['EventContactPhone'] ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="emailSection">
										<img src='<?php echo PATH ?>public/images/eventsMenu.png' />
									</div> 
									<div class="emailSection">
										<div class="row">
											<div class="col-md-6">
												<div><strong>Event Name</strong></div>
												<div><?php echo $contentOfEmail['FundraisingEventInformation']['NameOfEvent'] ?></div>		
											</div>
											<div class="col-md-6">
												<div><strong>Type of Event</strong></div>
												<div><?php echo $contentOfEmail['FundraisingEventInformation']['TypeOfEvent'] ?></div>
											</div>
										</div>
									</div>
									<div class="emailSection">
										<div class="row">
											<div class="col-md-6">
												<div><strong>Benefiting</strong></div>
												<div><?php echo $contentOfEmail['FundraisingEventInformation']['EventBenefiting'] ?></div>
											</div>
											<div class="col-md-6">
												<div><strong>Tax ID Number</strong></div>
												<div><?php echo $contentOfEmail['FundraisingEventInformation']['TaxIDNumber'] ?></div>
											</div>											
										</div>
									</div>
									
									<div class="emailSection">
										<div><strong>Date Of Event</strong></div>
										<?php 
											$donationEventDate = $this-> recordedTime -> FormatExpectedDate($contentOfEmail['FundraisingEventInformation']['DateOfEvent']);														
											echo $this-> recordedTime -> formatShortDate($donationEventDate); 
										?>
									</div>
									<div class="emailSection">
										<div><strong>Anticipated Attendance</strong></div>
										<div><?php echo $contentOfEmail['FundraisingEventInformation']['AnticipatedAttendance'] ?></div>	
									</div>
								</div>
								<div class="col-md-4">
									<div class="emailSection">
										<img src='<?php echo PATH ?>public/images/Blog.png' />
									</div> 
									<div class="emailSection">
										<div><strong>Additional Comments</strong></div>
										<?php 
											if(!empty($contentOfEmail['AdditionalInformation']['Comments'])) {
												echo $contentOfEmail['AdditionalInformation']['Comments'];
											} else {
												echo "NA";
											}
										
										?>
									</div>
									<div class="emailSection">
										<div><strong>Attachments</strong></div>
										<?php if(isset($contentOfEmail['AdditionalInformation']['attachments'])): ?>
											<?php foreach($contentOfEmail['AdditionalInformation']['attachments'] as $fileSingle): ?>
												<a href='<?php echo PHOTO_URL ?>DonationSubmissions/<?php echo $this -> emailSingle -> GetEmailID() ?>/<?php echo $fileSingle ?>' target="_blank">
													<div class="fileSingleElement">
														<img src='<?php echo PATH ?>public/images/FileSingle.png' />
													</div>
												</a>
											<?php endforeach; ?>
										<?php else: ?>
											No Attachments
										<?php endif; ?>
									</div>
								</div>
							</div>
						
						<?php endif; ?>	
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content" style='font-size:26px;'>
						<a href="<?php echo PATH ?>emaillogs/delete/<?php echo $this -> emailSingle -> GetEmailID() ?>"><i class="fa fa-trash" aria-hidden="true" style='margin-right:10px;'></i></a>Delete Email Log
					</div>
				</div>
			</div>
		</div>
	</div>
</div>