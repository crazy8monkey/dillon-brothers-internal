<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Email Log Summary
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-8">
								<canvas id="myChart"></canvas>
								<script>
								
								var ctx = document.getElementById("myChart").getContext('2d');
								var myChart = new Chart(ctx, {
								    type: 'line',
								    data: {
								        labels: ["Jan", "Feb", "March", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec."],
								        datasets: [{
								            label: 'Harley-Davidson',
								            data: [
								            	<?php echo $this -> emailLogSummaryCount['HarleyJanuaryCount'] ?>, 
								            	<?php echo $this -> emailLogSummaryCount['HarleyFebrauryCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleyMarchCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleyAprilCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleyMayCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleyJuneCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleyJulyCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleyAugustCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleySeptCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleyOctoberCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleyNovemberCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['HarleyDecemberCount'] ?>
								            ],
								            backgroundColor: 'rgba(255, 108, 0, 0.2)',
								            borderColor: 'rgba(255, 108, 0, 1)',
								            borderWidth: 1,
								            fill: false
								        },
								        {
								            label: 'Motorsport',
								            data: [
								            	<?php echo $this -> emailLogSummaryCount['MotorSportJanuaryCount'] ?>, 
								            	<?php echo $this -> emailLogSummaryCount['MotorSportFebrauryCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportMarchCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportAprilCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportMayCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportJuneCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportJulyCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportAugustCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportSeptCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportOctoberCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportNovemberCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['MotorSportDecemberCount'] ?>
								            ],
								            backgroundColor: 'rgba(195, 0, 0, 0.2)',
								            borderColor: 'rgb(195, 0, 0)',
								            borderWidth: 1,
								            fill: false
								        },
								        {
								            label: 'Indian',
								            data: [
								            	<?php echo $this -> emailLogSummaryCount['IndianJanuaryCount'] ?>, 
								            	<?php echo $this -> emailLogSummaryCount['IndianFebrauryCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianMarchCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianAprilCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianMayCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianJuneCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianJulyCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianAugustCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianSeptCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianOctoberCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianNovemberCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['IndianDecemberCount'] ?>
								            ],
								            backgroundColor: 'rgba(133, 0, 41, 0.2)',
								            borderColor: 'rgb(133, 0, 41)',
								            borderWidth: 1,
								            fill: false
								        },
								        {
								            label: 'Donation Requests',
								            data: [
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsJanuaryCount'] ?>, 
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsFebrauryCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsAprilCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsMayCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsJuneCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsJulyCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsAugustCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsSeptCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsOctoberCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsNovemberCount'] ?>,
								            	<?php echo $this -> emailLogSummaryCount['DonationRequestsDecemberCount'] ?>
								            ],
								            backgroundColor: 'rgba(54, 162, 235, 0.2)',
								            borderColor: 'rgb(54, 162, 235)',
								            borderWidth: 1,
								            fill: false
								        }]
								    },options: {
						                responsive: true,
						                title:{
						                    display:true,
						                    text:'<?php echo date('Y'); ?> Email Logs'
						                },
						                tooltips: {
						                    mode: 'index',
						                    intersect: false,
						                },
						                hover: {
						                    mode: 'nearest',
						                    intersect: true
						                },
						                scales: {
						                    xAxes: [{
						                        display: true,
						                        scaleLabel: {
						                            display: true,
						                            labelString: 'Month'
						                        }
						                    }],
						                    yAxes: [{
						                        display: true,
						                        scaleLabel: {
						                            display: true,
						                            labelString: 'Value'
						                        }
						                    }]
						                }
						            }
								});
								</script>
							</div>
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-4">
										<a href='javascript:void(0)' onclick='EmailLogController.SummaryInventoryLeads()'>
											<div class="togglePieChartLink">
												<div class="image">
													<img src='<?php echo PATH ?>public/images/EmailLogTypeInventory.png' />
												</div>
												<div class="textLink">
													Inventory Leads
												</div>
											</div>	
										</a>
									</div>
									<div class="col-md-4">
										<a href='javascript:void(0)' onclick='EmailLogController.SummaryWebsiteSources()'>
											<div class="togglePieChartLink">
												<div class="image">
													<img src='<?php echo PATH ?>public/images/WebsiteSource.png' />
												</div>
												<div class="textLink">
													Website Source
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href='javascript:void(0)' onclick='EmailLogController.SummaryTypeOfEmails()'>
											<div class="togglePieChartLink">
												<div class="image">
													<img src='<?php echo PATH ?>public/images/TypeOfEmails.png' />
												</div>
												<div class="textLink">
													Type of Emails
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="PieChartContainer">
											<div id="InventoryLeads">
												<div class="pieChartHeader">Inventory Leads</div>	
												<div>Grand Total: <?php echo $this -> inventoryLeads['SendToFriend'] + $this -> inventoryLeads['OnlineOffer'] + $this -> inventoryLeads['TradeInValue'] + $this -> inventoryLeads['ScheduleTestRide'] + $this -> inventoryLeads['ContactUs']?></div>
												<div class="PieChartContent">
													<canvas id="InventoryLeadsPieChart" width='200' height='200'></canvas>
													<script>
													var pieChart = document.getElementById("InventoryLeadsPieChart").getContext('2d');
													var myChart = new Chart(pieChart, {
													    type: 'pie',
													    data: {
													    	datasets: [{
													        	data: [
													        		<?php echo $this -> inventoryLeads['SendToFriend'] ?>, 
													        		<?php echo $this -> inventoryLeads['OnlineOffer'] ?>, 
													        		<?php echo $this -> inventoryLeads['TradeInValue'] ?>, 
													        		<?php echo $this -> inventoryLeads['ScheduleTestRide'] ?>, 
													        		<?php echo $this -> inventoryLeads['ContactUs'] ?>
													        	],
													            backgroundColor: [
													            	'rgb(54, 162, 235)', 
													            	'rgb(255, 99, 132)', 
													            	'rgb(255, 159, 64)', 
													            	'rgb(255, 205, 86)', 
													            	'rgb(75, 192, 192)'
													            ],
													            label: 'Dataset 1'
													        }],
													        labels: ["Send To A Friend", "Online Offer", "Trade In value", "Schedule Test Ride", "Contact Us"]	
													    },
													    options: {
													    	responsive: true,
												        }
												    });
													</script>
													
												</div>
											</div>	
											<div id="WebsiteSource" style='display:none'>
												<div class="pieChartHeader">Website Source</div>
												<div>Grand Total: <?php echo $this -> websiteSourceLeads['DillonBrothersMain']; ?></div>
												<div class="PieChartContent">
													<canvas id="WebsiteSourcePieChart" width='200' height='200'></canvas>
													<script>
													var pieChart = document.getElementById("WebsiteSourcePieChart").getContext('2d');
													var myChart = new Chart(pieChart, {
													    type: 'pie',
													    data: {
													    	datasets: [{
													        	data: [
													        		<?php echo $this -> websiteSourceLeads['DillonBrothersMain'] ?>
													        	],
													            backgroundColor: [
													            	'rgb(54, 162, 235)'
													            ],
													            label: 'Dataset 1'
													        }],
													        labels: ["dillon-brothers.com"]	
													    },
													    options: {
													    	responsive: true,
												        }
												    });
													</script>
												</div>	
											</div>		
											<div id="TypeOfEmailSection" style='display:none'>
												<div class="pieChartHeader">Type of Emails</div>
												<div>Grand Total: <?php echo $this -> typeofemails['InventoryLeads'] + $this -> typeofemails['ServiceDept'] + $this -> typeofemails['PartsDept'] + $this -> typeofemails['DonationRequests']; ?></div>
												<div class="PieChartContent">
													<canvas id="TypeOfEmailsPieChart" width='200' height='200'></canvas>
													<script>
													var pieChart = document.getElementById("TypeOfEmailsPieChart").getContext('2d');
													var myChart = new Chart(pieChart, {
													    type: 'pie',
													    data: {
													    	datasets: [{
													        	data: [
													        		<?php echo $this -> typeofemails['InventoryLeads'] ?>,
													        		<?php echo $this -> typeofemails['ServiceDept'] ?>,
													        		<?php echo $this -> typeofemails['PartsDept'] ?>,
													        		<?php echo $this -> typeofemails['DonationRequests'] ?>
													        	],
													            backgroundColor: [
													            	'rgb(54, 162, 235)',
													            	'rgb(255, 99, 132)',
													            	'rgb(255, 159, 64)',
													            	'rgb(255, 205, 86)'
													            ],
													            label: 'Dataset 1'
													        }],
													        labels: ["Inventory Leads", "Service Department", "Parts Department", "Donation Requests"]	
													    },
													    options: {
													    	responsive: true,
												        }
												    });
													</script>
													
													
												</div>	
											</div>								
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content" style='padding:0px;'>
						<div class="subHeader" style='padding:10px;'>Dillon Brothers MotorSports</div>
						<?php 
							$storeID = '2';
							$motorSportEmails = array_filter($this -> TotalEmails, function ($var) use($storeID) {
								return $var['LeadStoreID'] == $storeID; 
							});
						?>
						<div class="StoreEmailList">
							<?php if(count($motorSportEmails) > 0): ?>							
								<?php foreach($motorSportEmails as $emailSingle): ?>
									<?php
										$datedEnteredTime = explode('T', $emailSingle['EnteredTime']);
									?>
									<div class="row" style='margin:0px'>
										<div class="col-xs-6" style='padding-left: 0px;'>
											<a href="<?php echo PATH ?>emaillogs/view/<?php echo $emailSingle['emailLogID'] ?>"><?php echo $this-> recordedTime -> formatShortDate($datedEnteredTime[0]);?></a>				
										</div>
										<div class="col-xs-6" style='text-align:right; padding-right: 0px;'>
											<?php switch($emailSingle['TypeOfLead']):
												case 1:?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Send To a Friend" />
											<?php break; ?>
											<?php case 2: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Online Offer" />
											<?php break; ?>
											<?php case 3: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Trade In Value" />
											<?php break; ?>
											<?php case 4: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Schedule Test Ride" />
											<?php break; ?>
											<?php case 5: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Contact Us" />
											 <?php break; ?>
											<?php case 6: ?>
												<img src='<?php echo PATH ?>public/images/ServiceEmail.png' style='width: 20px; margin-bottom: 8px;' data-toggle="tooltip" data-placement="left" title="Service Dept. Email" />
											<?php break; ?>
											<?php case 7: ?>
												<img src='<?php echo PATH ?>public/images/PartsEmail.png' style='width: 20px; margin-bottom: 8px;' data-toggle="tooltip" data-placement="left" title="Parts Dept. Email" />
											<?php break; ?>
											<?php case 9: ?>
												<img src='<?php echo PATH ?>public/images/ApparelEmail.png' style='width: 20px; margin-bottom: 8px;' data-toggle="tooltip" data-placement="left" title="Apparel Dept. Email" />
											<?php break; ?>
											<?php endswitch; ?>
										</div>
										<div style='border-bottom:1px solid #cecece; clear:both; margin:8px 0px 10px 0px;'></div>
									</div>
								<?php endforeach; ?>
							<?php else: ?>
								There are currently no emails
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content" style='padding:0px;'>
						<div class="subHeader" style='padding:10px;'>Dillon Brothers Harley-Davidson</div>
						<?php 
							$storeID = '3';
							$harleyEmails = array_filter($this -> TotalEmails, function ($var) use($storeID) {
								return $var['LeadStoreID'] == $storeID; 
							});
						?>
						<div class="StoreEmailList">
							<?php if(count($harleyEmails) > 0): ?>							
								<?php foreach($harleyEmails as $emailSingle): ?>
									<?php
										$datedEnteredTime = explode('T', $emailSingle['EnteredTime']);
									?>
									<div class="row" style='margin:0px'>
										<div class="col-xs-6" style='padding-left: 0px;'>
											<a href="<?php echo PATH ?>emaillogs/view/<?php echo $emailSingle['emailLogID'] ?>"><?php echo $this-> recordedTime -> formatShortDate($datedEnteredTime[0]);?></a>				
										</div>
										<div class="col-xs-6" style='text-align:right; padding-right: 0px;'>
											<?php switch($emailSingle['TypeOfLead']):
												case 1:?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Send To a Friend" />
											<?php break; ?>
											<?php case 2: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Online Offer" />
											<?php break; ?>
											<?php case 3: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Trade In Value" />
											<?php break; ?>
											<?php case 4: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Schedule Test Ride" />
											<?php break; ?>
											<?php case 5: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Contact Us" />
											 <?php break; ?>
											<?php case 6: ?>
												<img src='<?php echo PATH ?>public/images/ServiceEmail.png' style='width: 20px; margin-bottom: 8px;' data-toggle="tooltip" data-placement="left" title="Service Dept. Email" />
											<?php break; ?>
											<?php case 7: ?>
												<img src='<?php echo PATH ?>public/images/PartsEmail.png' style='width: 20px; margin-bottom: 8px;' data-toggle="tooltip" data-placement="left" title="Parts Dept. Email" />
											<?php break; ?>
											<?php case 9: ?>
												<img src='<?php echo PATH ?>public/images/ApparelEmail.png' style='width: 20px; margin-bottom: 8px;' data-toggle="tooltip" data-placement="left" title="Apparel Dept. Email" />
											<?php break; ?>
											<?php endswitch; ?>
										</div>
										<div style='border-bottom:1px solid #cecece; clear:both; margin:8px 0px 10px 0px;'></div>
									</div>
								<?php endforeach; ?>
							<?php else: ?>
								There are currently no emails
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content" style='padding:0px;'>
						<div class="subHeader" style='padding:10px;'>Dillon Brothers Indian</div>
						<?php 
							$storeID = '4';
							$indianEmails = array_filter($this -> TotalEmails, function ($var) use($storeID) {
								return $var['LeadStoreID'] == $storeID; 
							});
						?>
						<div class="StoreEmailList">
							<?php if(count($indianEmails) > 0): ?>							
								<?php foreach($indianEmails as $emailSingle): ?>
									<?php
										$datedEnteredTime = explode('T', $emailSingle['EnteredTime']);
									?>
									<div class="row" style='margin:0px'>
										<div class="col-xs-6" style='padding-left: 0px;'>
											<a href="<?php echo PATH ?>emaillogs/view/<?php echo $emailSingle['emailLogID'] ?>"><?php echo $this-> recordedTime -> formatShortDate($datedEnteredTime[0]);?></a>				
										</div>
										<div class="col-xs-6" style='text-align:right; padding-right: 0px;'>
											<?php switch($emailSingle['TypeOfLead']):
												case 1:?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Send To a Friend" />
											<?php break; ?>
											<?php case 2: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Online Offer" />
											<?php break; ?>
											<?php case 3: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Trade In Value" />
											<?php break; ?>
											<?php case 4: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Schedule Test Ride" />
											<?php break; ?>
											<?php case 5: ?>
												<img src='<?php echo PATH ?>public/images/Inventory.png' style='width: 25px;' data-toggle="tooltip" data-placement="left" title="Inventory Lead - Contact Us" />
											 <?php break; ?>
											<?php case 6: ?>
												<img src='<?php echo PATH ?>public/images/ServiceEmail.png' style='width: 20px; margin-bottom: 8px;' data-toggle="tooltip" data-placement="left" title="Service Dept. Email" />
											<?php break; ?>
											<?php case 7: ?>
												<img src='<?php echo PATH ?>public/images/PartsEmail.png' style='width: 20px; margin-bottom: 8px;' data-toggle="tooltip" data-placement="left" title="Parts Dept. Email" />
											<?php break; ?>
											<?php case 9: ?>
												<img src='<?php echo PATH ?>public/images/ApparelEmail.png' style='width: 20px; margin-bottom: 8px;' data-toggle="tooltip" data-placement="left" title="Apparel Dept. Email" />
											<?php break; ?>
											<?php endswitch; ?>
										</div>
										<div style='border-bottom:1px solid #cecece; clear:both; margin:8px 0px 10px 0px;'></div>
									</div>
								<?php endforeach; ?>
							<?php else: ?>
								There are currently no emails
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content" style='padding:0px;'>
						<div class="subHeader" style='padding:10px;'>Donation Requests</div>
						<?php 
							$donationRequests = '8';
							$donationRequestsEmails = array_filter($this -> TotalEmails, function ($var) use($donationRequests) {
								return $var['TypeOfLead'] == $donationRequests; 
							});
						?>
						<div class="StoreEmailList">
							<?php if(count($donationRequestsEmails) > 0): ?>
								<?php foreach($donationRequestsEmails as $emailSingle): ?>
									<?php
										$datedEnteredTime = explode('T', $emailSingle['EnteredTime']);
									?>
									<div class="row" style='margin:0px;'>
										<div class="col-md-12" style='margin-bottom: 9px; padding: 0px;'>
											<a href="<?php echo PATH ?>emaillogs/view/<?php echo $emailSingle['emailLogID'] ?>"><?php echo $this-> recordedTime -> formatShortDate($datedEnteredTime[0]);?></a>				
										</div>
										
										<div style='border-bottom:1px solid #cecece; clear:both; margin:8px 0px 10px 0px;'></div>
									</div>
								<?php endforeach; ?>
							<?php else: ?>
								There are currently no Donation Requests
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



