
<div class="container">
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Users
		</div>
	</div>
	<div class="row buttonContainer">
		<div class="col-md-12">
			<a href="<?php echo $this -> pages -> users() ?>/create">
				<div class="greenButton" style="float:left">
					Add User
				</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php foreach($this -> users as $userSingle) : ?>
				<div class="UserSingleLine">
					<div class="circle">
						<?php if($userSingle['userProfile'] != NULL): ?>
							<div class="profilePicObject" style="background:url(<?php echo PHOTO_URL ?>Accounts/<?php echo $userSingle['userProfile'] ?>)"></div>
						<?php else: ?>
							<div class="profilePicObject" style="background:url(<?php echo PATH ?>view/users/Images/no-picture.png)"></div>
						<?php endif; ?>
							
						<div class="text"><?php echo $userSingle['firstName'] ?> <?php echo $userSingle['lastName'] ?>	</div>
						<div class="delete">
							<i class="fa fa-trash-o" aria-hidden="true"></i>	
						</div>			
						<div class="view">
							<a href="<?php echo $this -> pages -> users() ?>/edit/<?php echo $userSingle['userID'] ?>">
								<i class="fa fa-eye" aria-hidden="true"></i>	
							</a>
						</div>
						
					</div>

					<div style="clear:both"></div>		
				</div>
				
				<div></div>
			<?php endforeach; ?>
		</div>
	</div>
	
</div>

