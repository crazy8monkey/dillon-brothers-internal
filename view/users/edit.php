<div class="container" style="padding-bottom: 120px;">
	<?php if(!isset($this -> profileView)) : ?>
		<div class="row">
			<div class="col-md-12 breadCrumbs">
				<a href="<?php echo $this -> pages -> users() ?>">Users</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit User
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			<?php if(isset($this -> profileView)) {
				echo "Your Profile";
			} else {
				echo "Edit User";
			}
			?>
		</div>
	</div>
	
	<?php
	$formURL = NULL; 
	if(isset($this -> profileView)) {
		$formURL = 'profile/save/';
	} else {
		$formURL = 'users/save/';
	}
	?>
	
	<form method="post" action="<?php echo PATH . $formURL . $this -> UserSingle -> GetUserId() ?>" id="UserSubmitForm">
		<div class="row">
			<div class="col-md-12">
				<div class="ProfilePicContent">
					<?php if($this -> UserSingle -> currentProfilePic != NULL): ?>
						<div class="pic" style='background:url(<?php echo PHOTO_URL ?>Accounts/<?php echo $this -> UserSingle -> currentProfilePic ?>) no-repeat center'></div>
					<?php else: ?>
						<div class="pic" style='background:url(<?php echo PATH ?>view/users/images/no-picture.png) no-repeat center'></div>
					<?php endif; ?>
					
					<div class="fileInput">
						<?php echo $this -> form -> FileInput(NULL, "userProfilePic") ?>	
					</div>
					
				</div>
			</div>
		</div> 
        <div class="row">
			<div class="col-md-6">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "First Name", "userFirstName", 1, $this -> UserSingle -> FirstName) ?>
				</div>
			</div>
			<div class="col-md-6">
				<div id="inputID2">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Last Name", "userLastName", 2, $this -> UserSingle -> LastName) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div id="inputID3">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Username", "userNameLogin", 3, $this -> UserSingle -> Username) ?>
				</div>
			</div>
			<div class="col-md-4">
				<div id="inputID4">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("password", "Password", "userPassword", 4) ?>
				</div>
				
			</div>
			<div class="col-md-4">
				<div id="inputID5">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Email", "userEmail", 5, $this -> UserSingle -> Email) ?>
				</div>
				
			</div>
		</div>
		<?php if(!isset($this -> profileView)) :?>
			<div class="row">
				<div class="col-md-12">
					<div id="inputID6">
						<div class="errorMessage"></div>
						<div class="input">
							<div class="inputLabel">Associated Store</div>
							<?php 
								$userID = $this -> UserSingle -> GetUserId();
							?>
							<?php echo $this -> form -> checkbox('Dillon Motor Sports', 'stores[]', $this -> UserSingle -> GetAssociatedStore($userID, 2), 2, 6) ?>
							<?php echo $this -> form -> checkbox('Dillon Harley', 'stores[]', $this -> UserSingle -> GetAssociatedStore($userID, 3), 3, 6) ?>
							<?php echo $this -> form -> checkbox('Dillon Indian', 'stores[]', $this -> UserSingle -> GetAssociatedStore($userID, 4), 4, 6) ?>	
						</div>
					</div>
				</div>
			</div>
		
		
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Permissions
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="permissionSection">
						<div class='permissionSectionHeader'>Social Media</div>
						<?php echo $this -> form -> checkbox('Social Media', 'permissionSocialMedia', $this -> UserSingle -> HasPermission("SocialMedia"), 1, NULL, true) ?>
						<?php echo $this -> form -> checkbox('Social Media Summary', 'permissionSocialMediaSummary', $this -> UserSingle -> HasPermission("SocialMediaSummary"), 1, NULL, true) ?>	
					</div>
					<div class="permissionSection">
						<div class='permissionSectionHeader'>Photos</div>
						<?php echo $this -> form -> checkbox('Photos', 'permissionPhotos', $this -> UserSingle -> HasPermission("PhotoManagement"), 1, NULL, true) ?>
						<?php echo $this -> form -> checkbox('Upload Photos Visible', 'permissionPhotoVisible', $this -> UserSingle -> HasPermission("PhotoUploadVisible"), 1, NULL, true) ?>
					</div><br />
				</div>
				<div class="col-md-8">
					<div class="permissionSection">
						<div class='permissionSectionHeader'>Misc</div>
					
						<div class="row">
							<div class="col-md-6">
								<?php echo $this -> form -> checkbox('Events', 'permissionEvents', $this -> UserSingle -> HasPermission("Events"), 1, NULL, true) ?>
								<?php echo $this -> form -> checkbox('Newsletters', 'permissionNewsletters', $this -> UserSingle -> HasPermission("Newsletters"), 1, NULL, true) ?>
								<?php echo $this -> form -> checkbox('Blog', 'permissionBlog', $this -> UserSingle -> HasPermission("Blog"), 1, NULL, true) ?>		
							</div>
							<div class="col-md-6">
								<?php echo $this -> form -> checkbox('Specials', 'permissionSpecials', $this -> UserSingle -> HasPermission("Specials"), 1, NULL, true) ?>
								<?php echo $this -> form -> checkbox('Employees', 'permissionEmployees', $this -> UserSingle -> HasPermission("Employees"), 1, NULL, true) ?>
								<?php echo $this -> form -> checkbox('Settings', 'permissionSettings', $this -> UserSingle -> HasPermission("Settings"), 1, NULL, true) ?>	
							</div>
						</div>
					</div>
					<br />
				</div>
			</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit('Save', 'greenButton') ?>
			</div>
		</div>
	</form>
</div>


