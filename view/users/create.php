<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo $this -> pages -> users() ?>">Users</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create User
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Create User
		</div>
	</div>
	
	<form method="post" action="<?php echo PATH ?>users/save" id="UserSubmitForm">
		<div class="row">
			<div class="col-md-12">
				<div class="ProfilePicContent">
					<div class="pic" style='background:url(<?php echo PATH ?>view/users/images/no-picture.png) no-repeat center'></div>
					<div class="fileInput">
						<?php echo $this -> form -> FileInput(NULL, "userProfilePic") ?>	
					</div>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "First Name", "userFirstName", 1) ?>
				</div>
			</div>
			<div class="col-md-6">
				<div id="inputID2">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Last Name", "userLastName", 2) ?>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div id="inputID3">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Username", "userNameLogin", 3) ?>
				</div>
				
			</div>
			<div class="col-md-4">
				<div id="inputID4">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("password", "Password", "userPassword", 4) ?>
				</div>
			</div>
			<div class="col-md-4">
				<div id="inputID5">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Email", "userEmail", 5) ?>
				</div>
				
			</div>
		</div>
		<div class="row">
				<div class="col-md-12">
					<div id="inputID6">
						<div class="errorMessage"></div>
						<div class="input">
							<div class="inputLabel">Associated Store</div>
							<?php echo $this -> form -> checkbox('Dillon Motor Sports', 'stores[]', NULL, 2, 6) ?>
							<?php echo $this -> form -> checkbox('Dillon Harley', 'stores[]', NULL, 3, 6) ?>
							<?php echo $this -> form -> checkbox('Dillon Indian', 'stores[]', NULL, 4, 6) ?>	
						</div>
					</div>
				</div>
			</div>
		<div class="row">
			<div class="col-md-12 sectionHeader">
				Permissions
			</div>
		</div>
		<div class="row">
				<div class="col-md-4">
					<div class="permissionSection">
						<div class='permissionSectionHeader'>Social Media</div>
						<?php echo $this -> form -> checkbox('Social Media', 'permissionSocialMedia', NULL, 1, NULL, true) ?>
						<?php echo $this -> form -> checkbox('Social Media Summary', 'permissionSocialMediaSummary', NULL, 1, NULL, true) ?>	
					</div>
					<div class="permissionSection">
						<div class='permissionSectionHeader'>Photos</div>
						<?php echo $this -> form -> checkbox('Photos', 'permissionPhotos', NULL, 1, NULL, true) ?>
						<?php echo $this -> form -> checkbox('Upload Photos Visible', 'permissionPhotoVisible', NULL, 1, NULL, true) ?>
					</div><br />
				</div>
				<div class="col-md-8">
					<div class="permissionSection">
						<div class='permissionSectionHeader'>Misc</div>
						<div class="row">
							<div class="col-md-6">
								<?php echo $this -> form -> checkbox('Events', 'permissionEvents', NULL, 1, NULL, true) ?>
								<?php echo $this -> form -> checkbox('Newsletters', 'permissionNewsletters', NULL, 1, NULL, true) ?>
								<?php echo $this -> form -> checkbox('Blog', 'permissionBlog', NULL, 1, NULL, true) ?>		
							</div>
							<div class="col-md-6">
								<?php echo $this -> form -> checkbox('Specials', 'permissionSpecials', NULL, 1, NULL, true) ?>
								<?php echo $this -> form -> checkbox('Employees', 'permissionEmployees', NULL, 1, NULL, true) ?>
								<?php echo $this -> form -> checkbox('Settings', 'permissionSettings', NULL, 1, NULL, true) ?>	
							</div>
						</div>
					</div>
					<br />
				</div>
			</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> Submit('Create', 'greenButton') ?>
			</div>
		</div>
	</form>
</div>


