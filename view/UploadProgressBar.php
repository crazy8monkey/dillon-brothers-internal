<?php

$url = PATH . 'view/UploadProgressBar.php';

//Get file upload progress information.
if(isset($_GET['progress_key'])) {
	$status = apc_fetch('upload_'.$_GET['progress_key']);
	echo $status['current']/$status['total']*100;
	die;
}
//

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
<style type="text/css">
	.progressBar {
    width: 300px;
    height: 30px;
    background: #d6d6d6;
    border-radius: 15px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    -ms-border-radius: 15px;
    -o-border-radius: 15px;
    overflow: hidden;
    margin-top: 10px;
}

.progressBar .progressContent {
    height: 30px;
    width: 0px;
    background: #3030bb;
    border-radius: 15px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    -ms-border-radius: 15px;
    -o-border-radius: 15px;
    -webkit-transition: all .5 ease;
    transition: all .5 ease;
}

</style>

<script>
$(document).ready(function() { 
//

	setInterval(function() 
		{
	$.get("<?php echo $url; ?>?progress_key=<?php echo $_GET['up_id']; ?>&randval="+ Math.random(), { 
		//get request to the current URL (upload_frame.php) which calls the code at the top of the page.  It checks the file's progress based on the file id "progress_key=" and returns the value with the function below:
	},
		function(data)	//return information back from jQuery's get request
			{
				$(".progressBar").show();
            	$(".progressBar .progressContent").width(data +"%");
            	$(".ProgressTotal").html("<strong>Current Progress: </strong>" + parseInt(data) + "%");				
			}
		)},500);	//Interval is set at 500 milliseconds (the progress bar will refresh every .5 seconds)

});


</script>

<body style="margin:0px">
<!--Progress bar divs-->
<div class="ProgressTotal"></div>
<div class="progressBar" style="display:none;">
	<div class="progressContent">
				
	</div>
</div>
<!---->
</body>