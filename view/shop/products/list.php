<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php if(isset($this -> filteredResultText)): ?>
						<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>shop/products">Products List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Filtered Results
					<?php else: ?>
						<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Products List
					<?php endif; ?>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Products
				</div>
			</div>
			<div class="row buttonContainer">
				<div class="col-md-12">
					<div><strong>Search Products</strong></div>
					<div style="margin-top:10px; float:left">
						<div id="inputID5">
							<div class="errorMessage"></div>
							<form method='post' id="FilterProductList"  action="<?php echo PATH ?>shop/save/filterproducts">
								<div style="float:left; width: 370px;">
									<?php if(isset($this -> filteredResultText)): ?>
										<input id="inventoryOverlayText" class="FilteredText" type="text" value='<?php echo $this -> filteredResultText ?>' name="FilteredText" onchange="Globals.removeValidationMessage(5)">
									<?php else: ?>
										<input id="inventoryOverlayText" class="FilteredText" type="text" name="FilteredText" onchange="Globals.removeValidationMessage(5)">
									<?php endif; ?>
								</div>
								
								<button type="submit" class="filterSubmitButton" style="float:left">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>	
							</form>
						</div>
					</div>
					<a href="<?php echo PATH ?>shop/create/product">
						<div class="greenButton" style="float:left; margin-top:10px; margin-left:10px;">
							New Product
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<?php if(count($this -> apparel) > 0):?>	
			<?php foreach(array_chunk($this -> apparel, 3, true) as $apparelThree) : ?>
				<div class="row">
					<?php foreach($apparelThree as $apparelSingle) : ?>
						<div class="col-md-4">
							<div class="apparelProductSingleElement">
								<div class="headerRow">
									<div class="delete">
										<a href='<?php echo PATH ?>shop/delete/product/<?php echo $apparelSingle['productID'] ?>' onclick="return confirm('Are you sure you want to delete this Product? any photos uploaded to this product will be deleted, and this action cannot be undone')">
											<i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete"></i>	
										</a>
										
									</div>
									<div class="productName">
										<a href='<?php echo PATH ?>shop/edit/product/<?php echo $apparelSingle['productID'] ?>'>
											<?php echo $apparelSingle['productName'] ?>
										</a>	
									</div>
									<?php if($apparelSingle['productVisible'] == 0): ?>
										<div class="VisibilityIndicator isNotVisible" data-toggle="tooltip" data-placement="left" title="Product Not Visible"></div>
									<?php else: ?>	
										<div class="VisibilityIndicator isVisible" data-toggle="tooltip" data-placement="left" title="Product Visible"></div>
									<?php endif; ?>
									
									<div style='clear:both'></div>	
								</div>
								<div class="Info">
									<div class="MSRP">
										$<?php echo number_format($apparelSingle['productPrice'], 2); ?>
									</div>
									<div><?php echo $apparelSingle['SKUNumber']; ?></div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endforeach; ?>
									
		<?php else: ?>
			<div class="row">
				<div class="col-md-12">
					<?php if(isset($this -> filteredResultText)): ?>
						There are no filter results
					<?php else: ?>
						There are no products entered
					<?php endif; ?>
					
				</div>
			</div>
		<?php endif; ?>	
	</div>
</div>
