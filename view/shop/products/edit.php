<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>shop/products">Products List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Product
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid" style='padding-bottom: 70px;'>
		<?php if(count($this -> apparelcategories) > 0) :?>
			
		
			<form method="post" action="<?php echo PATH ?>shop/save/product/<?php echo $this -> productSingle -> GetID() ?>" id="ProductForm">
				<div class="row">
					<div class="col-md-8">
						<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row">
									<div class="col-md-12 sectionHeader">
										Edit Product
									</div>
								</div>
								<?php if(count($this ->  logs) > 0): ?>								
									<div class="row">
										<div class="col-md-12">
											<?php 
												$editedTime = explode('T', $this ->  logs[count($this ->  logs) - 1]['edittedTimeAndDate']);
											?>
											Last Edited by <?php echo $this ->  logs[count($this ->  logs) - 1]['firstName'] . ' ' . $this ->  logs[count($this ->  logs) - 1]['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1]) ?> <a href="javascript:void(0);" onclick="ShopController.getEditHistory(<?php echo $this -> productSingle -> GetID() ?>)">View History</a>
											<div style='border-bottom: 1px solid #f7f7f7; margin-bottom:10px; padding-bottom:10px;'></div>
										</div>
									</div>
								<?php endif; ?>
								<?php if(count($this -> productPhotos) > 0 && $this -> productSingle -> Category != 0): ?>
									<?php if($this -> productSingle -> Visible == 1): ?>											
										<div class="row">
											<div class="col-md-12">
												<div style='margin-bottom:10px'>
													<?php echo $this -> form -> checkbox('Featured Product', 'featuredProduct', $this -> productSingle -> FeaturedProduct, 1, NULL, true) ?>		
												</div>
											</div>
										</div>
									<?php endif; ?>
									<div class="row">
										<div class="col-md-12 buttonContainer">
											<?php if($this -> productSingle -> Visible == 0): ?>											
												<a href="<?php echo PATH ?>shop/save/publish/<?php echo $this -> productSingle -> GetID() ?>">
													<div class="greenButton" style="float:left">
														Publish Product
													</div>
												</a>
											<?php endif; ?>
											
											<?php if($this -> productSingle -> Visible == 1): ?>											
												<a href="<?php echo PATH ?>shop/save/unpublish/<?php echo $this -> productSingle -> GetID() ?>">
													<div class="greenButton" style="float:left">
														Unpublish Product
													</div>
												</a>
											<?php endif; ?>
										</div>
									</div>					
								<?php endif; ?>	
								
								<div class="row">
									<div class="col-md-6">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Product Name", "productName", 1, $this -> productSingle -> productName) ?>	
										</div>		
									</div>
									<div class="col-md-3">
										<div id="inputID7">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "MSRP Price", "productMSRP", 7, $this -> productSingle -> MSRPPrice) ?>			
										</div>
									</div>
									<div class="col-md-3">
										<div id="inputID3">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Price", "productPrice", 3, $this -> productSingle -> Price) ?>			
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div id="inputID4">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Description</div>
												<textarea name="productDescription" onchange="Globals.removeValidationMessage(3)" class="textareaMinHeight"><?php echo $this -> productSingle -> Description?></textarea>
											</div>
										</div>	
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 sectionHeader">
										Photos
									</div>
								</div>
								<?php 
										$needsWaterMarkArray = array();
									?>
								<?php if(count($this -> productPhotos) > 0): ?>			
									
														
									<div class="row">
										<div class="col-md-12 sectionHeader">
											<?php foreach($this -> productPhotos as $photoSingle): ?>
												<div style="width:228px; float:left">
													<div class="inventoryPhotoSingleElement" style='background:url(<?php echo PHOTO_URL ?>ecommerce/<?php echo $this -> productSingle -> Folder ?>/<?php echo $photoSingle['PhotoName'] ?>-s.<?php echo $photoSingle['PhotoExt'] ?>) no-repeat center'></div>
													<div class="photoOptions">
														<a onclick="ShopController.ViewPhoto(<?php echo $photoSingle['productPhotoID'] ?>)" href="javascript:void(0);">
															<i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="view"></i>	
														</a>
																	
														<a onclick="return confirm('Are you sure you want to delete this photo?')" href="<?php echo PATH ?>shop/delete/photo/<?php echo $photoSingle['productPhotoID'] ?>">
															<i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="delete"></i>	
														</a>					
													</div>	
												</div>
												<?php array_push($needsWaterMarkArray, $photoSingle['photoWarterMarked']) ?>
											<?php endforeach; ?>
										</div>
									</div>
								<?php endif; ?>
								<div class="row buttonContainer">
									<div class="col-md-12">
										<a href="javascript:void(0);" onclick="ShopController.AddPhotosToInventory(<?php echo $this -> productSingle -> GetID() ?>)">
											<div class="whiteButton" style="float:left; margin-right:5px;">
												Add Photos
											</div>				
										</a>	
										<?php if( in_array( 0 ,$needsWaterMarkArray ) ): ?>
											<a href="<?php echo PATH ?>shop/watermark/all/<?php echo $this -> productSingle -> GetID()?>">
												<div class="blueButton" style="float:left; margin-right:5px;">
													Watermark Photos
												</div>				
											</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
						
						
					</div>
					<div class="col-md-4">
						
						<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row">
									<div class="col-md-12">
										<div id="inputID5">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Category</div>
												<select name="productCategory">
													<option value='0'>Select Category</option>
													<?php foreach($this -> apparelcategories as $categorySingle): ?>
														<option value='<?php echo $categorySingle['settingCategoryID'] ?>' <?php if($this -> productSingle -> Category == $categorySingle['settingCategoryID']): ?>selected<?php endif; ?>><?php echo $categorySingle['settingCategoryName'] ?></option>
													<?php endforeach; ?>	
												</select>	
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row">
									<div class="col-md-12">
										<div class="descriptiveInfo">Please select a Part number if you have no sizes selected for this particuar product</div>
										<div id="inputID2">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Part Number", "productStockNumber", 2, $this -> productSingle -> StockNumber) ?>			
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row">
									<div class="col-md-12">
										<?php if(count($this -> sizes) > 0): ?>
											<div class="input">
												<div class="inputLabel">Product Sizes</div>
												<div class="descriptiveInfo">Select sizes of product we are going to sell</div>
												<div id="inputID6">
													<div class='errorMessage'></div>
												</div>
												<?php foreach($this -> sizes as $key => $sizeSingle): ?>
													<div class="row" style='margin-top:10px;'>
														<div class="col-md-5">
															<div class="sizeCheckbox" style='margin-top:5px'>	
															<i class="fa fa-sort" aria-hidden="true"></i>	<input type="checkbox" name="productSizeOption[<?php echo $key ?>]" value='<?php echo $sizeSingle['settingSizeID'] ?>' <?php if(in_array($sizeSingle['settingSizeID'], array_column($this -> productSingle -> SizesSelected, 'relatedSizeID'))): ?>checked<?php endif; ?> /><?php echo $sizeSingle['settingSizeText'] ?>
															</div>
														</div>
														<div class="col-md-7">
															<?php 
																$key = array_search($sizeSingle['settingSizeID'], array_column($this -> productSingle -> SizesSelected, 'relatedSizeID'));
															?>
															<?php if($key !== false): ?>
																<input type='text' name='productPartNumberSize[]' placeholder='Part #' value='<?php echo $this -> productSingle -> SizesSelected[$key]['PartNumber'] ?>' />	
															<?php else: ?>
																<input type='text' name='productPartNumberSize[]' placeholder='Part #'  />
															<?php endif; ?>
															
														</div>	
													</div>														
												<?php endforeach; ?>
											</div>
											
										<?php else: ?>
											There are no product sizes entered in this category</br />
											Please go into settings to add sizes to this category in order to select sizes for this product<br />
											<a href='<?php echo PATH ?>shop/settings' target="_blank">Click Here</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row">
									<div class="col-md-12">
										<div class="input">
											<div class="inputLabel">Color Options</div>
											<div class="descriptiveInfo">Please select colors that apply to this product (not required)</div>
											<?php foreach(array_chunk($this -> coloroptions, 2, true) as $colorDuece) : ?>
												<div class="row">
													<?php foreach($colorDuece as $colorSingle): ?>
														<div class="col-md-6">
															<div class="sizeCheckbox">
																<input type="checkbox" name="colorOption[]" value='<?php echo $colorSingle['settingColorID'] ?>' <?php if(in_array($colorSingle['settingColorID'], array_column($this -> productSingle -> ColorsSelected, 'relatedColorID'))): ?>checked<?php endif; ?> /><?php echo $colorSingle['settingColorText'] ?>
															</div>
														</div>
													<?php endforeach; ?>	
												</div>
													
											<?php endforeach; ?>
										</div>
									</div>
								</div>
							</div>
						</div>-->
					</div>
						
				</div>
					
						
						
				<div class="SaveProductItem">
					<div class="row">
						<div class="col-md-12">
							<div class="ContentPage">
								<div style='padding:15px 10px'>
									<input type="submit" value="Save" class="greenButton" style="font-size: 16px; font-weight: normal;">		
								</div>
							</div>
						</div>
					</div>			
				</div>
			</form>
			
			
			
			
			
		<?php else: ?>
			<div class="row">
				<div class="col-md-12">
					<div class="WhiteSectionDiv">
						<div class="content">
							<?php if(count($this -> apparelcategories) == 0) :?>
								<div>There need to be Ecommerce Categories to select to create a product - <a href='<?php echo PATH ?>shop/settings'>Click here to create a category</a></div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			
		<?php endif; ?>
	</div>
</div>

<div id="ProductHistoryPopup" style="display:none">
	<?php echo $this -> PopupReadOnly('Ecommerce Product History', 'ShopController.closeInventoryHistory()')?>
</div>


