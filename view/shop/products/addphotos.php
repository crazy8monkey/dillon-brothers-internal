<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
<head>
	<title>Add Photos To Product: <?php echo $this -> ApparelProduct -> productName ?> | <?php echo $this -> ApparelProduct -> StockNumber ?></title>
		
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
				
	<!-- Google Authorship and Publisher Markup --> 
	<a rel="publisher" href="https://plus.google.com/103831261532249725840/"></a>
	<a rel="author" href="https://plus.google.com/103831261532249725840/"></a>
		
		
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/FormStone.css" />
		
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/InventoryController.css" /> 
	
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo PATH ?>/public/js/FormStone.js" type="text/javascript"></script>
		
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		
	<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".upload").upload({
								maxSize: 1073741824,
								beforeSend: onBeforeSend
							}).on("start.upload", onStart)
							.on("complete.upload", onComplete)
							.on("filestart.upload", onFileStart)
							.on("fileprogress.upload", onFileProgress)
							.on("filecomplete.upload", onFileComplete)
							.on("fileerror.upload", onFileError)
							.on("chunkstart.upload", onChunkStart)
							.on("chunkprogress.upload", onChunkProgress)
							.on("chunkcomplete.upload", onChunkComplete)
							.on("chunkerror.upload", onChunkError)
							.on("queued.upload", onQueued);
						$(".filelist.queue").on("click", ".cancel", onCancel);
						$(".cancel_all").on("click", onCancelAll);
			
			
		
		})
		
		//https://formstone.it/components/upload/
		function onCancel(e) {
						console.log("Cancel");
						var index = $(this).parents("li").data("index");
						$(this).parents("form").find(".upload").upload("abort", parseInt(index, 10));
					}
					function onCancelAll(e) {
						console.log("Cancel All");
						$(this).parents("form").find(".upload").upload("abort");
					}
					function onBeforeSend(formData, file) {
						console.log("Before Send");
						formData.append("test_field", "test_value");
						// return (file.name.indexOf(".jpg") < -1) ? false : formData; // cancel all jpgs
						return formData;
					}
					function onQueued(e, files) {
						console.log("Queued");
						var html = '';
						for (var i = 0; i < files.length; i++) {
							html += '<li data-index="' + files[i].index + '"><span class="content"><span class="file">' + files[i].name + '</span><span class="cancel">Cancel</span><span class="progress">Queued</span></span><span class="bar"></span></li>';
						}
						$(this).parents("form").find(".filelist.queue")
							.append(html);
					}
					function onStart(e, files) {
						console.log("Start");
						$(this).parents("form").find(".filelist.queue")
							.find("li")
							.find(".progress").text("Waiting");
					}
					function onComplete(e) {
						console.log("Complete");
						opener.location.reload();
						window.close();
						// All done!
					}
					function onFileStart(e, file) {
						console.log("File Start");
						$(this).parents("form").find(".filelist.queue")
							.find("li[data-index=" + file.index + "]")
							.find(".progress").text("0%");
					}
					function onFileProgress(e, file, percent) {
						console.log("File Progress");
						var $file = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");
						$file.find(".progress").text(percent + "%")
						$file.find(".bar").css("width", percent + "%");
					}
					function onFileComplete(e, file, response) {
						console.log("File Complete");
						if (response.trim() === "" || response.toLowerCase().indexOf("error") > -1) {
							$(this).parents("form").find(".filelist.queue")
								.find("li[data-index=" + file.index + "]").addClass("error")
								.find(".progress").text(response.trim());
						}
						else {
							var $target = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");
							$target.find(".file").text(file.name);
							$target.find(".progress").remove();
							$target.find(".cancel").remove();
							$target.appendTo($(this).parents("form").find(".filelist.complete"));
						}
					}
					function onFileError(e, file, error) {
						console.log("File Error");
						$(this).parents("form").find(".filelist.queue")
							.find("li[data-index=" + file.index + "]").addClass("error")
							.find(".progress").text("Error: " + error);
					}
					function onChunkStart(e, file) {
						console.log("Chunk Start");
					}
					function onChunkProgress(e, file, percent) {
						console.log("Chunk Progress");
					}
					function onChunkComplete(e, file, response) {
						console.log("Chunk Complete");
					}
					function onChunkError(e, file, error) {
						console.log("Chunk Error");
					}
		
		
	</script>
	<style type='text/css'>
			.filelists {
						margin: 20px 0;
					}
					
					.filelists h5 {
						margin: 10px 0 0;
					}
					
					.filelists .cancel_all {
						color: red;
						cursor: pointer;
						clear: both;
						font-size: 10px;
						margin: 0;
						text-transform: uppercase;
					}
					
					.filelist {
						margin: 0;
						padding: 10px 0;
					}
					
					.filelist li {
						background: #fff;
						border-bottom: 1px solid #ECEFF1;
						font-size: 14px;
						list-style: none;
						padding: 5px;
						position: relative;
					}
					
					.filelist li:before {
						display: none !important;
					}
					/* main site demos */
					
					.filelist li .bar {
						background: #eceff1;
						content: '';
						height: 100%;
						left: 0;
						position: absolute;
						top: 0;
						width: 0;
						z-index: 0;
						-webkit-transition: width 0.1s linear;
						transition: width 0.1s linear;
					}
					
					.filelist li .content {
						display: block;
						overflow: hidden;
						position: relative;
						z-index: 1;
					}
					
					.filelist li .file {
						color: #455A64;
						float: left;
						display: block;
						overflow: hidden;
						text-overflow: ellipsis;
						max-width: 50%;
						white-space: nowrap;
					}
					
					.filelist li .progress {
						color: #B0BEC5;
						display: block;
						float: right;
						font-size: 10px;
						text-transform: uppercase;
						background:transparent !important;
					}
					
					.filelist li .cancel {
						color: red;
						cursor: pointer;
						display: block;
						float: right;
						font-size: 10px;
						margin: 0 0 0 10px;
						text-transform: uppercase;
					}
					
					.filelist li.error .file {
						color: red;
					}
					
					.filelist li.error .progress {
						color: red;
					}
					
					.filelist li.error .cancel {
						display: none;
					}					
	</style>
		

</head>
<body>
	<div class="AddPhotoContainer ">
		<div class="AddPhotosHeader">
			Add Photos To Product
		</div>	
		<div id="inputID1">
			<div class="errorMessage"></div>
		</div>
		<form action="#" method="GET" class="form demo_form">
			<div class="upload" data-upload-options='{"action":"<?php echo PATH ?>shop/save/productphoto/<?php echo $this -> ApparelProduct -> GetID()?>"}'></div>
			
			<div class="filelists demo_content">
				<h5>Complete</h5>
				<ol class="filelist complete">
				</ol>
				<h5>Queued</h5>
				<ol class="filelist queue">
				</ol>
				<span class="cancel_all">Cancel All</span>
			</div>
			
		</form>
		
	</div>
	
	
	



</body>	
</html>
