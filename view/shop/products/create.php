<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>shop/products">Products List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New Product
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<?php if(count($this -> apparelcategories) > 0) :?>
		
		
			<form method="post" action="<?php echo PATH ?>shop/save/product" id="ProductForm">
				<div class="row">
					<div class="col-md-8">
						<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row">
									<div class="col-md-12 sectionHeader">
										New Product
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Product Name", "productName", 1) ?>	
										</div>		
									</div>
									<div class="col-md-3">
										<div id="inputID7">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "MSRP Price", "productMSRP", 7) ?>			
										</div>
									</div>
									<div class="col-md-3">
										<div id="inputID3">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Selling Price", "productPrice", 3) ?>			
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div id="inputID4">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Description</div>
												<textarea name="productDescription" onchange="Globals.removeValidationMessage(3)" class="textareaMinHeight"></textarea>
											</div>
										</div>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row">
									<div class="col-md-12">
										<div id="inputID5">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Category</div>
												<select name="productCategory">
													<option value='0'>Select Category</option>
													<?php foreach($this -> apparelcategories as $categorySingle): ?>
														<option value='<?php echo $categorySingle['settingCategoryID'] ?>'><?php echo $categorySingle['settingCategoryName'] ?></option>
													<?php endforeach; ?>	
												</select>	
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row">
									<div class="col-md-12">
										<?php if(count($this -> sizes) > 0): ?>
											<div id="inputID6">
												<div class="errorMessage"></div>
												<div class="input">
													<div class="inputLabel">Product Sizes</div>
													<div class="descriptiveInfo">Select sizes of product we are going to sell</div>
													<?php foreach($this -> sizes as $sizeSingle): ?>
														<div class="row">
															<div class="col-md-6">
																<div class="sizeCheckbox">	
																	<input type="checkbox" name="productSizeOption[]" value='<?php echo $sizeSingle['settingSizeID'] ?>' /><?php echo $sizeSingle['settingSizeText'] ?>
																</div>
															</div>
															<div class="col-md-6">
																<input type='productPartNumberSize[]' placeholder='Part #' />
															</div>	
														</div>
														
													<?php endforeach; ?>
												</div>
											</div>
										<?php else: ?>
											There are no product sizes entered in our settings</br />
											Please go into settings to add sizes in order to select sizes for this product<br />
											<a href='<?php echo PATH ?>shop/settings' target="_blank">Click Here</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>-->
						<!--<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row">
									<div class="col-md-12">
										<div class="input">
											<div class="inputLabel">Color Options</div>
											<div class="descriptiveInfo">Please select colors that apply to this product (not required)</div>
											<?php foreach(array_chunk($this -> coloroptions, 2, true) as $colorDuece) : ?>
												<div class="row">
													<?php foreach($colorDuece as $colorSingle): ?>
														<div class="col-md-6">
															<div class="sizeCheckbox">
																<input type="checkbox" name="colorOption[]" value='<?php echo $colorSingle['settingColorID'] ?>' /><?php echo $colorSingle['settingColorText'] ?>
															</div>
														</div>
													<?php endforeach; ?>	
												</div>
													
											<?php endforeach; ?>
										</div>
										
									</div>
								</div>
							</div>
						</div> -->
					</div>	
				</div>
						
						
						
				<div class="SaveProductItem">
					<div class="row">
						<div class="col-md-12">
							<div class="ContentPage">
								<div style='padding:15px 10px'>
									<input type="submit" value="Save" class="greenButton" style="font-size: 16px; font-weight: normal;">		
								</div>
							</div>
						</div>
					</div>			
				</div>
			</form>
		<?php else: ?>
			<div class="row">
				<div class="col-md-12">
					<div class="WhiteSectionDiv">
						<div class="content">
							<?php if(count($this -> apparelcategories) == 0) :?>
								<div>There need to be Apparel categories to select to create a product - <a href='<?php echo PATH ?>shop/settings'>Click here to create a category</a></div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			
		<?php endif; ?>
	</div>
</div>



