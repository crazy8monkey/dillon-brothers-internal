<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>shop/settings">Settings</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Category
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<form method="post" action="<?php echo PATH ?>shop/save/size/<?php echo $this -> sizeSingle -> GetID() ?>" id="SizeForm">
			<div class="row">
				<div class="col-md-12">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="container">
								<div class="row">
									<div class="col-md-12 sectionHeader">
										Edit Size
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Size Abbreviation</div>
												<input id="apparelSizeAbbr" class="apparelSizeAbbr" type="text" name="apparelSizeAbbr" maxlength="2" value="<?php echo $this -> sizeSingle -> SizeAbbr ?>" onchange="Globals.removeValidationMessage(1)">
											</div>
										</div>
									</div>
									<div class="col-md-10">
										<div id="inputID2">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Size Text", "apparelSizeText", 2, $this -> sizeSingle -> SizeText) ?>		
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" value="Save" class="greenButton">		
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>



