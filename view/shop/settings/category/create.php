<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>shop/settings">Settings</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New Category
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<form method="post" action="<?php echo PATH ?>shop/save/category" id="categoryForm">
			<div class="row">
				<div class="col-md-12">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="container">
								<div class="row">
									<div class="col-md-12 sectionHeader">
										New Category
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Category Name", "apparelCategoryname", 1) ?>		
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="descriptiveInfo">Please select sizes that will apply to products in this particular category</div>
										<div style='margin-bottom:10px;'>
											<div id="inputID2"><div class="errorMessage"></div></div>
											<?php foreach($this -> sizes as $sizeSingle): ?>
												<div class='categorySizeSelect'>
													
													<input type='checkbox' name='sizes[]' value='<?php echo $sizeSingle['settingSizeID'] ?>' /> <?php echo $sizeSingle['settingSizeText'] ?>
												</div>
											<?php endforeach; ?>	
										</div>
										
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" value="Save" class="greenButton">		
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>



