
<div class="topSectionPage" style='margin-top: -20px; padding-top:20px;'>
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Settings
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Shopping Settings
				</div>
			</div>
			
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								General
							</div>
						</div>
						<form id='EcommerceSettings' method='post' action="<?php echo PATH ?>shop/save/ecommercesettings">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Shipping Rate", "settingFlatRate", 1, $this -> ecommercesettings -> ShippingRates) ?>		
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="descriptiveInfo">Enter multiple emails separated with a comma.</div>
									<div id="inputID2">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Email Notifications</div>
											<textarea name='settingEmailNotificaitons' style='min-height:200px;'><?php echo $this -> ecommercesettings -> EmailNotifications; ?></textarea>	
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Save" class="whiteButton">		
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Categories
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="descriptiveInfo">Products need to have an associated category in order to be shown on the website, if you remove a category that is entered on this, those associated inventory will be removed on the website</div>
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo PATH ?>shop/create/category">
									<div class="greenButton" style="float:left">
										New Category
									</div>
								</a>
							</div>
						</div>
						<?php if(count($this -> apparelcategories) > 0): ?>						
							<?php foreach($this -> apparelcategories as $category): ?>
								<div class="row">
									<div class="col-xs-6">
										<a href="<?php echo PATH ?>shop/edit/category/<?php echo $category['settingCategoryID'] ?>"><?php echo $category['settingCategoryName'] ?></a>
									</div>
									<div class="col-xs-6" style='text-align:right'>
										<a href="<?php echo PATH ?>shop/delete/category/<?php echo $category['settingCategoryID'] ?>" onclick="return confirm('Are you sure you want to delete this Category? Any products associated with this category will be removed from the website, and this action cannot be undone')"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</div>
								</div>
								<div style='border-bottom: 1px solid #f1f1f1; margin-top: 5px;'></div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class="col-md-12">
									There are no categories Entered
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<!--<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Colors
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo PATH ?>shop/create/color">
									<div class="greenButton" style="float:left">
										New Color
									</div>
								</a>
							</div>
						</div>
						<?php if(count($this -> coloroptions) > 0): ?>						
							<?php foreach($this -> coloroptions as $colorSingle): ?>
								<div class="row">
									<div class="col-xs-6">
										<a href="<?php echo PATH ?>shop/edit/color/<?php echo $colorSingle['settingColorID'] ?>"><?php echo $colorSingle['settingColorText'] ?></a>
									</div>
									<div class="col-xs-6" style='text-align:right'>
										<a href="<?php echo PATH ?>shop/delete/color/<?php echo $colorSingle['settingColorID'] ?>" onclick="return confirm('Are you sure you want to delete this Color? This action cannot be undone')"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</div>
								</div>
								<div style='border-bottom: 1px solid #f1f1f1; margin-top: 5px;'></div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class="col-md-12">
									There are no colors Entered
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>-->
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 subHeader">
								Sizes
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo PATH ?>shop/create/size">
									<div class="greenButton" style="float:left">
										New Size Option
									</div>
								</a>
							</div>
						</div>
						<?php if(count($this -> sizes) > 0): ?>						
							<?php foreach($this -> sizes as $sizeSingle): ?>
								<div class="row">
									<div class="col-xs-6">
										<a href="<?php echo PATH ?>shop/edit/size/<?php echo $sizeSingle['settingSizeID'] ?>"><?php echo $sizeSingle['settingSizeAbbr'] ?> | <?php echo $sizeSingle['settingSizeText'] ?></a>
									</div>
									<div class="col-xs-6" style='text-align:right'>
										<a href="<?php echo PATH ?>shop/delete/size/<?php echo $sizeSingle['settingSizeID'] ?>" onclick="return confirm('Are you sure you want to delete this Size? This action cannot be undone')"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</div>
								</div>
								<div style='border-bottom: 1px solid #f1f1f1; margin-top: 5px;'></div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class="col-md-12">
									There are no Sizes Entered
								</div>
							</div>
						<?php endif; ?>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</diV>



