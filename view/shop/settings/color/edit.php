<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>shop/settings">Settings</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Category
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<form method="post" action="<?php echo PATH ?>shop/save/color/<?php echo $this -> colorSingle -> GetID() ?>" id="ColorForm">
			<div class="row">
				<div class="col-md-12">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="container">
								<div class="row">
									<div class="col-md-12 sectionHeader">
										Edit Color
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("text", "Color Name", "settingColorName", 1, $this -> colorSingle -> colorName) ?>		
										</div>
										
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" value="Save" class="greenButton">		
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>



