
<div class="topSectionPage" style='margin-top: -20px; padding-top:20px;'>
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Shopping Center
				</div>
			</div>
			
		</div>
	</div>
</div>
<div class="ContentPage ">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv SummaryOrderContent">
					<div class="content">
						<div class="Header">
							<?php echo date('Y'); ?> Orders
						</div>
						<div class="row">
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">January</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['JanuaryPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['JanuaryShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['JanuaryTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">February</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['FebruaryPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['FebruaryShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['FebruaryTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">March</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['MarchPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['MarchShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['MarchTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">April</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['AprilPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['AprilShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['AprilTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">May</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['MayPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['MayShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['MayTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">June</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['JunePendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['JuneShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['JuneTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">July</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['JulyPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['JulyShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['JulyTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">August</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['AugustPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['AugustShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['AugustTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">September</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['SeptemberPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['SeptemberShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['SeptemberTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">October</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['OctoberPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['OctoberShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['OctoberTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">November</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['NovemberPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['NovemberShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['NovemberTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="MonthlySummaryBox">
									<div class="monthHeader">December</div>
									<div class="MonthlyOrderContent">
										<div class="row" style='margin: 0px;'>
											<div class="col-md-6" style='border-right:1px solid #cdcdcd'>
												<div class="orderNumberHeader">Pending</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['DecemberPendingCount'] ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="orderNumberHeader">Shipped</div>
												<div class="orderNumberContent">
													<?php echo $this -> ThisYearSummary['DecemberShippedCount'] ?>
												</div>
											</div>
										</div>	
									</div>
									<div class="MonthlyTotalSalesContent">
										<div class='salesHeader'>Total Sales</div>
										<div class="salesNumber">
											$<?php echo $this -> ThisYearSummary['DecemberTotalSales'] ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<table class='yearSummary'>
									<tr>
										<td style='padding-bottom: 5px;'>
											Total Orders:
										</td>
										<td class='finalNumber'>
											<?php echo $this -> ThisYearSummary['TotalOrders'] ?>
										</td>
									</tr>
									<tr>
										<td style='padding-bottom: 5px;'>
											Total Sales:
										</td>
										<td class='finalNumber'>
											$<?php echo $this -> ThisYearSummary['TotalSales'] ?>
										</td>
									</tr>
								</table>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row MainLinks">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<a href="<?php echo PATH ?>shop/products">
						<div class="content">
							Products
						</div>	
					</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<a href="<?php echo PATH ?>shop/orders">
						<div class="content">
							Orders
						</div>
					</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<a href="<?php echo PATH ?>shop/reviews">
						<div class="content">
							Reviews
						</div>	
					</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<a href="<?php echo PATH ?>shop/settings">
						<div class="content">
							Settings
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</diV>



