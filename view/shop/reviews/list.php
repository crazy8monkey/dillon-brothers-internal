<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Submitted Reviews
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Product Reviews
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content" id="ProductReviewList">
						<?php if(count($this -> products) > 0): ?>
							<?php foreach($this -> products as $productSingle): ?>
								<?php 
									$productID = $productSingle['productID'];	
									$filteredInventory = array_filter($this -> reviewList, function ($var) use($productID) {
											 return $var['reviewItemID'] == $productID; 
										}
									);
								?>
								
								<?php if(count($filteredInventory) > 0): ?>
									<div class='productItemHeader'>
										<a href="javascript:void(0);" onclick='ShopController.ToggleReviewList(this)'><div class='productItemExpand'>+</div> <?php echo $productSingle['productName']; ?></a>	
									</div>
									<div class="productReviewList">
										<?php foreach(array_chunk($filteredInventory, 6, true) as $reviewSix) : ?>
											<div class="row">
												<?php foreach($reviewSix as $reviewSingle) : ?>
													<div class="col-md-2">
														<div class="reviewTextBox">
															<?php switch($reviewSingle['rating']): 
																case 5.0: ?>
																<div class="ratingElement">
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																</div>
																<?php break; ?>
																<?php case 4.5: ?>
																<div class="ratingElement">
																    <div class="starEmpty"></div>
																    <div class="starHalfFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																</div>
																<?php break; ?>
																<?php case 4.0: ?>
																<div class="ratingElement">
																    <div class="starEmpty"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																</div>
																<?php break; ?>
																<?php case 3.5: ?>
																<div class="ratingElement">
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starHalfFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																</div>
																<?php break; ?>
																<?php case 3.0: ?>
																<div class="ratingElement">
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																</div>
																<?php break; ?>
																<?php case 2.5: ?>
																<div class="ratingElement">
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starHalfFull"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																</div>
																<?php break; ?>
																<?php case 2.0: ?>
																<div class="ratingElement">
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starFull"></div>
																    <div class="starFull"></div>
																</div>
																<?php break; ?>
																<?php case 1.5: ?>
																<div class="ratingElement">
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starHalfFull"></div>
																    <div class="starFull"></div>
																</div>
																<?php break; ?>
																<?php case 1.0: ?>
																<div class="ratingElement">
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starFull"></div>
																</div>
																<?php break; ?>
																<?php case 0.5: ?>
																<div class="ratingElement">
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starEmpty"></div>
																    <div class="starHalfFull"></div>
																</div>
																<?php break; ?>
															<?php endswitch; ?>
															<div class="approveDeclineButtons">
																<?php if($reviewSingle['reviewActive'] == 0): ?>
																	<div style='float:left; margin-right:10px;'>
																		<a href="<?php echo PATH ?>shop/save/approvereview/<?php echo $reviewSingle['reviewID'] ?>" onclick="return confirm('Are you sure you want to Approve this review? this action cannot be undone')">
																			<i class="fa fa-check-circle" aria-hidden="true" style='color:#18ad00' data-toggle="tooltip" data-placement="bottom" title="Approve Review"></i>															
																		</a>
																	</div>
																<?php endif; ?>
																
																<div style='float:left;'>
																	<a href="<?php echo PATH ?>shop/delete/review/<?php echo $reviewSingle['reviewID'] ?>" onclick="return confirm('Are you sure you want to Delete this review? this action cannot be undone')">
																		<i class="fa fa-times-circle" aria-hidden="true" style='color:#c30000' data-toggle="tooltip" data-placement="bottom" title="Delete Comment"></i>
																	</a>
																	
																</div>
															</div>
															
															<div style='clear:both'>
																<?php echo $reviewSingle['reviewText'] ?>	
															</div>
															
														</div>
													</div>
												<?php endforeach; ?>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
								
								
							<?php endforeach; ?>	
						<?php else: ?>
							There are no products entered
						<?php endif; ?>							
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

