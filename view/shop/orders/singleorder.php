<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>shop/orders">Orders List</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>View Order
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid" style='padding-bottom: 70px;'>
		<div class="row">
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<?php if(count($this ->  logs) > 0): ?>						
							<div class="row">
								<div class="col-md-12">
									<?php 
										$editedTime = explode('T', $this ->  logs[count($this ->  logs) - 1]['edittedTimeAndDate']);
									?>
									Last Edited by <?php echo $this ->  logs[count($this ->  logs) - 1]['firstName'] . ' ' . $this ->  logs[count($this ->  logs) - 1]['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1]) ?>
									<div style='border-bottom: 1px solid #eaeaea; margin-bottom:10px; padding-bottom:10px;'></div>
								</div>
							</div>
						<?php endif; ?>
						<div class="row">
							<div class="col-md-12">
								<div style='font-size:24px; border-bottom: 1px solid #eaeaea; margin-bottom: 11px; padding-bottom: 10px;'>
									<?php if(!empty($this -> ordersingle -> CustomerStripeID)): ?>									
										<a href="<?php echo STRIPE_LINK ?>customers/<?php echo $this -> ordersingle -> CustomerStripeID ?>" target="_blank">
											<div class="blueButton" style="float:left; margin-right:5px;">
												Customer
											</div>				
										</a>
									<?php endif; ?>
									<a href="<?php echo STRIPE_LINK ?>payments/<?php echo $this -> ordersingle -> PaymentStripeID ?>" target="_blank">
										<div class="blueButton" style="float:left; margin-right:5px;">
											Payment Details
										</div>				
									</a>
									<div style='clear:both'></div>
								</div>
								
							</div>
						</div>
						<?php if($this -> ordersingle -> StorePickup == 0): ?>
							<div class="row">
								<div class="col-md-12">
									Customer will pick their order at the store
								</div>
							</div>
						<?php else: ?>
							<div class="row">
								<div class="col-md-12">
									<strong>Ship this order to the following address</strong>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style='margin:10px 0px;'>
									<div><?php echo $this -> ordersingle -> ShippingAddress; ?></div>
									<div><?php echo $this -> ordersingle -> ShippingCity; ?>, <?php echo $this -> ordersingle -> ShippingState; ?> <?php echo $this -> ordersingle -> ShippingZip; ?></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div style='border-top: 1px solid #eaeaea; padding-top: 10px;'>
										<?php if($this -> ordersingle -> ShipStatus == 0 && $this -> ordersingle -> StorePickup == 1): ?>
										
											<form method='post' id="MarkAsShippedForm" action='<?php echo PATH ?>shop/save/markedshipped/<?php echo $this -> ordersingle -> GetID()?>'>
												<div id="inputID1">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Tracking Number", "shipTrackingNumber", 1) ?>		
													
												</div>
												<div id="inputID2">
													<div class="errorMessage"></div>
													<select name='shippingCompany' onchange="Globals.removeValidationMessage(2)">
														<option value='0'>Please Select Shipping Company</option>
														<option value='1'>UPS</option>
														<option value='0'>USPS</option>
													</select>	
												</div>
												
												<div style='margin-top:15px;'>
													<input type='submit' value='Mark As Shipped' class="whiteButton" />
												</div>
											</form>
										<?php else: ?>
											<div class="row">
												<div class="col-md-12">
													<div style='font-size:24px; margin-bottom:10px;'>
														Order Shipment Details
													</div>
													<div><strong>This Order has been Marked As Shipped</strong></div>
													<table style='margin-top:10px;'>
														<tr>
															<td style='width:150px'>Tracking Number:</td>
															<td><?php echo $this -> ordersingle -> TrackingNumber; ?></td>
														</tr>
														<tr>
															<td style='width:150px'>Shipping Company:</td>
															<td>
																<?php if($this -> ordersingle -> ShippingCompany == 1): ?>
																	UPS
																<?php elseif($this -> ordersingle -> ShippingCompany == 2): ?>
																	USPS
																<?php endif; ?>
															</td>
														</tr>
													</table>
												</div>
											</div>
										<?php endif; ?>
									</div>
									
								</div>
							</div>
						<?php endif; ?>
						<div class="row">
							<div class="col-md-12">
								<div style='font-size:24px; border-top: 1px solid #eaeaea; margin-top: 11px; padding-top: 10px;'>
									Order Summary Details	
								</div>
							</div>	
						</div>
						<?php foreach($this -> ordersingle -> orders as $itemOrderSingle): ?>
							<div class="row">
								<div class="col-md-12" style='margin-top:15px;'>
									<?php if($itemOrderSingle['productName'] != NULL): ?>
										<div style='font-size:20px;'><strong><?php echo $itemOrderSingle['productName'] ?></strong></div>
									<?php else: ?>	
										<div style='font-size:20px;'><strong><?php echo $itemOrderSingle['previousProductName'] ?></strong></div>
									<?php endif; ?>
									
									<table>
										<tr>
											<td class='orderDetailLabel'>Part Number:</td>
											<td>#<?php echo $itemOrderSingle['PartNumber'] ?></td>
										</tr>
										<tr>
											<td class='orderDetailLabel'>Price:</td>
											<td>$<?php echo $itemOrderSingle['price'] ?></td>
										</tr>
										<?php if(!empty($itemOrderSingle['settingColorText'])): ?>
											<tr>
												<td class='orderDetailLabel'>Color:</td>
												<td><?php echo $itemOrderSingle['settingColorText'] ?></td>
											</tr>
										<?php endif; ?>
										<?php if(!empty($itemOrderSingle['settingSizeText'])): ?>
											<tr>
												<td class='orderDetailLabel'>Size:</td>
												<td><?php echo $itemOrderSingle['settingSizeText'] ?></td>
											</tr>
										<?php endif; ?>
									</table>
								</div>
							</div>
						<?php endforeach; ?>
						<div class="row">
							<div class="col-md-12">
								<div style='border-top: 1px solid #eaeaea; padding-top: 10px; margin-top:11px;'>
									<table>
										<tr>
											<td style='width:140px'>Shipping Rates:</td>
											<td style='text-align:right'>$<?php echo $this -> ordersingle -> ShippingRate ?></td>
										</tr>
										<tr>
											<td style='width:140px'>Taxes:</td>
											<td style='text-align:right'>$<?php echo $this -> ordersingle -> TaxRate ?></td>
										</tr>
										<tr>
											<td style='width:140px'>Grand Total:</td>
											<td style='text-align:right'>$<?php echo $this -> ordersingle -> GrandTotal ?></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					
								
				</div>
			</div>
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="subHeader">
							General Info
						</div>
						<div class="row">
							<div class="col-md-12">
								<div><?php echo $this -> ordersingle -> firstName?> <?php echo $this -> ordersingle -> LastName?></div>
								<div><?php echo $this -> PhoneFormat($this -> ordersingle -> PhoneNumber); ?></div>
								<div><?php echo $this -> ordersingle -> Email; ?></div>
							</div>	
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<div id="ProductHistoryPopup" style="display:none">
	<?php echo $this -> PopupReadOnly('Ecommerce Product History', 'ShopController.closeInventoryHistory()')?>
</div>


