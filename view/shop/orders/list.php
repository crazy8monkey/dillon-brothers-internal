<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo PATH ?>shop">Shop</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Orders
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Orders
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<?php if(count($this -> orders) > 0):?>	
			<?php foreach(array_chunk($this -> orders, 3, true) as $orderThree) : ?>
				<div class="row">
					<?php foreach($orderThree as $orderSingle) : ?>
						<div class="col-md-4">
							<a href='<?php echo PATH ?>shop/vieworder/<?php echo $orderSingle['orderID'] ?>'>
								<div class="orderSingle">
									<div class="OrderDate">
										<?php 
											$dateEnteredObj = explode('T', $orderSingle['DateEntered']);
										 	echo $this -> recordedTime -> formatDate($dateEnteredObj[0]) . ' / ' . $this -> recordedTime -> formatTime($dateEnteredObj[1]);
										 ?>
										<?php if($orderSingle['shippedStatus'] == 0): ?>
											<div class="shippingStatus OrderNotShipped" data-toggle="tooltip" data-placement="left" title="Order Not Shipped"></div>
										<?php else: ?>	
											<div class="shippingStatus OrderShipped" data-toggle="tooltip" data-placement="left" title="Order Shipped"></div>
										<?php endif; ?>
										
									</div>	
								</div>	
							</a>
							
						</div>
					<?php endforeach; ?>
				</div>
			<?php endforeach; ?>
									
		<?php else: ?>
			<div class="row">
				<div class="col-md-12">
					There are no products entered
				</div>
			</div>
		<?php endif; ?>	
	</div>
</div>
