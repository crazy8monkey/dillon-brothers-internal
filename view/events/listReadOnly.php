<?php 

function date_compare($a, $b) {
	$t1 = strtotime($a['EventDate']);
	$t2 = strtotime($b['EventDate']);
	return $t2 - $t1;
}  
			?>
			
<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Events
				</div>
			</div>
		</div>
	</div>
</div>
			
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">Event Caledar Upload</div>
							</div>
						</div>
						
						<?php foreach($this -> PDFS as $pdf): ?>
							<div class="row">
								<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
								<div class="col-md-12">
									<a href='<?php echo PHOTO_URL?>EventPDFs/<?php echo $pdf['eventYearListName'] ?>' target='_blank'><?php echo $pdf['eventYearList'] ?> Events Calendar</a>
								</div>
							</div>
						<?php endforeach; ?>
						
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">Event List</div>
							</div>
						</div>
						<?php foreach($this -> SuperEvents as $SuperEventSingle): ?>
							<div class="row">
								<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
								<div class="col-md-12">
									<div class="EventType SuperEvent" data-toggle="tooltip" data-placement="bottom" title="Super Event"></div>
									<a href="<?php echo $this -> pages -> events() ?>/view/SuperEvent/<?php echo $SuperEventSingle['eventID'] ?>"><?php echo $SuperEventSingle['eventName'] ?></a>
								</div>
							</div>
							<?php 
								$superEventID = $SuperEventSingle['eventID'];
														
								$filteredSubEvents = array_filter($this -> SubEvents, function ($var) use($superEventID) {
									return $var['SuperEventID'] == $superEventID; 
								});
							
							?>
							
							<?php if(count($filteredSubEvents) > 0): ?>
								<div style="margin-left:20px;">
									<?php foreach($filteredSubEvents as $subEventSingle): ?>
										<div class="row">
											<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
											<div class="col-md-6">
												<div class="EventType SubEvent" data-toggle="tooltip" data-placement="bottom" title="<?php echo $SuperEventSingle['eventName'] ?> Sub Event"></div>
												<a href="<?php echo $this -> pages -> events() ?>/view/SubEvent/<?php echo $subEventSingle['eventID'] ?>"><?php echo $subEventSingle['eventName']; ?></a>
											</div>
											<div class="col-md-6" style='text-align:right'>
												<?php echo $this -> recordedTime -> formatDate($subEventSingle['startDate']);?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							
						<?php endforeach; ?>
						<?php foreach($this -> StandaloneEvents as $StandaloneSingle): ?>
							<div class="row">
								<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
								<div class="col-md-6">
									<div class="EventType StandAlone" data-toggle="tooltip" data-placement="bottom" title="Stand Alone Event"></div>
									<a href="<?php echo $this -> pages -> events() ?>/view/Standalone/<?php echo $StandaloneSingle['eventID'] ?>"><?php echo $StandaloneSingle['eventName'] ?></a>
								</div>
								<div class="col-md-6" style='text-align:right'>
									<?php echo $this -> recordedTime -> formatDate($StandaloneSingle['startDate']);?>
								</div>
							</div>
						<?php endforeach; ?>	
						
						
						<?php if(count($this -> SuperEvents) == 0 && count($this -> StandaloneEvents) == 0): ?>
							<div class="row">
								<div class="col-md-12">
									There are no events entered
								</div>
							</div>
						<?php endif; ?>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


