<?php $structuredData = json_decode($this -> EventSingle -> StructuredData, true) ?>
<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> events() ?>">Events</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Super Event
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='padding-bottom:120px'>
	<div class="container-fluid">
		
			<form method="post" action="<?php echo PATH ?>events/save/superevent/<?php echo $this -> EventSingle -> GetEventID() ?>" id="EventForm" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-9">
					
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="row sectionHeader">
								<div class="col-md-12">
									Edit Super Event
								</div>
							</div>
							<div class="row">
									<div class="col-md-12">
										<?php 
											$editedTime = explode('T', $this ->  logs[count($this ->  logs) - 1]['edittedTimeAndDate']);
										?>
										Last Edited by <?php echo $this ->  logs[count($this ->  logs) - 1]['firstName'] . ' ' . $this ->  logs[count($this ->  logs) - 1]['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1]) ?> <a href="javascript:void(0);" onclick="EventsController.getEditHistory(<?php echo $this -> EventSingle -> GetEventID() ?>)">View History</a>
										<div style='border-bottom: 1px solid #f7f7f7; margin-bottom:10px; padding-bottom:10px;'></div>
									</div>
								</div>
							<div class="row">
								<div class="col-md-6">
									<div class="subHeader">Event Information</div>		
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="descriptiveInfo"><?php echo WEBSITE_PATH ?><strong><?php echo $this -> EventSingle -> CurrentSEOUrl?>/</strong></div>
									<div class="input">
										<div class="inputLabel">URL Slug</div>
										<div id="inputID20">
											<div class="errorMessage"></div>	
											<input id="eventName" class="eventName" type="text" name="urlSlug" value="<?php echo $this -> EventSingle -> CurrentSEOUrl?>" placeholder='i.e. Throttle Thursday' onchange="Globals.removeValidationMessage(20)">
										</div>
									</div>
									
									<div id="inputID1">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Super Event Name</div>
											<input id="eventName" class="eventName" type="text" name="eventName" value="<?php echo $this -> EventSingle -> eventName?>" placeholder='i.e. Throttle Thursday' onchange="Globals.removeValidationMessage(1)">
										</div>
									</div>
									<div id="inputID14">
										<div class="errorMessage"></div>
										<?php echo $this-> form -> textarea("Super Event Description", "superEventDescription", 14, $this -> EventSingle -> SuperEventDescription) ?>
									</div>	
								</div>
								<div class="col-md-6">
									<div id="inputID19">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="descriptiveInfo">i.e. bikenight, eurobikenight -> DO NOT PUT # in input</div>
											<div class="inputLabel">Super Event ID</div>
											<input type="text" name="superEventIDText" placeholder="i.e. bikenight" value="<?php echo $structuredData['SuperEventIDText'] ?>" onchange="Globals.removeValidationMessage(19)">
										</div>				
									</div>
								</div>
							</div>
						</div>
					</div>	
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="row sectionHeader">
								<div class="col-md-12">
									Misc. Information
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
											<div id="inputID15">
												<div class="errorMessage"></div>
												<div class="input">
													<div class="inputLabel">Is there any performers/bands at this event?</div>
													<div class="descriptiveInfo">if there is not any performers please type "NA" in this input</div>
													<input class="performerInfo" type="text" name="performerInfo" placeholder="i.e. Nickelback" value='<?php echo $structuredData['Performer'] ?>' onchange="Globals.removeValidationMessage(15)">
												</div>
											</div>	
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div id="inputID16">
												<div class="errorMessage"></div>
												<div class="input">
													<div class="inputLabel">Is there a price entrace fee? (in dollars)</div>
													<div class="descriptiveInfo">if There is no entrance fee type in "00.00"</div>
													<input class="priceInfo" type="text" name="priceInfo" placeholder="i.e. 30.00" value='<?php echo $structuredData['EventPrice'] ?>' onchange="Globals.removeValidationMessage(16)">
												</div>
											</div>	
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="subHeader">Geo Location</div>	
									<div class="descriptiveInfo">
										<strong>Dillon Harley Omaha</strong> -  Latitude: 41.292938 / Longitutde: -96.1883625<br />
										<strong>Dillon Indian</strong> - Latitude: 41.29352 / Longitutde: -96.1892845<br />
										<strong>Dillon Motorsports</strong> - Latitude: 41.293818 / Longitutde: -96.1884585<br />
										<strong>Dillon Harley Fremont</strong> - Latitude: 41.4519858 / Longitutde: -96.4654377<br />
										<strong>Partsfish</strong> - Latitude: 41.294056 / Longitutde: -96.1884655
									</div>
									<div class="row">
										<div class="col-md-6">
											<div id="inputID17">
												<div class="errorMessage"></div>
												<div class="input">
													<div class="inputLabel">Latitude</div>
													<input class="latitude" type="text" name="latitude" value='<?php echo $structuredData['Latitude'] ?>' onchange="Globals.removeValidationMessage(17)">
												</div>
											</div>	
										</div>
										<div class="col-md-6">
											<div id="inputID18">
												<div class="errorMessage"></div>
												<div class="input">
													<div class="inputLabel">Longitude</div>
													<input class="longitude" type="text" name="longitude" value='<?php echo $structuredData['Longitude'] ?>' onchange="Globals.removeValidationMessage(18)">
												</div>
											</div>	
										</div>
									</diV>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="input">
								<div class="inputLabel">Super Event Photo</div>
								<input type='file' name="SuperEventPhoto" />
							</div>
							
							<?php if(!empty($this -> EventSingle -> CurrentEventImage)): ?>
								<div style='margin-bottom:10px;'><strong>Current Blog Main Photo</strong> </div>
								<img src='<?php echo $this -> EventSingle -> GetMainPhoto(); ?>' width='100%' />
							<?php else: ?>
								<div class="emptyPhotoPlacement" style='margin-bottom:0px;'>
									<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
									<div class="required">Photo Is Required to show on website</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
				
				<div class="UploadPhotosSubmit" style="width: 100%; display: block;">
							<div class="ContentPage">
								<div class="container-fluid">
								<div class="row">
									<div class="col-md-8">
										<div style="float:left; margin-top: 2px;">
											<input type="submit" value="Edit Super Event" class="greenButton" style="font-size: 16px; font-weight: normal; padding: 8px 20px;">	
										</div>
										<div style="margin-top: 6px; margin-left: 125px;" id="uploadingPhotosText">
											<img src="<?php echo PATH ?>public/images/ajax-loader.gif"> Uploading Photos
										</div>
										
									</div>
								</div>
								
							</div>
							</div>
							
						</div>
						
			</form>	

<div id="EventEditHistory" style="display:none;">
	<?php echo $this -> PopupReadOnly('Event Edit History', 'EventsController.closeEditHistory()')?>
</div>

<div id="SuperEventPopup" style="display:none;">
	<a href="javascript:void(0);" onclick="EventsController.ClosePopup()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">
			<div class="text">Select Super Event</div>
			<a href="javascript:void(0);" onclick="EventsController.ClosePopup()">
				<div class="close" style="margin-top: -23px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>
		</div>
		<div class="FormContent"></div>
	</div>
</div>


