<?php $structuredData = json_decode($this -> EventSingle -> StructuredData, true) ?>
<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> events() ?>">Events</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>View Event
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" name="HtmlEventJSON" id="HtmlEventJSON" value='<?php echo ($this -> EventSingle -> SuperEvent != 1 ? $this -> EventSingle -> GetHtml() : '')  ?>' />

<div class="ContentPage" style='padding-bottom:120px'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Viewing Super Event
							</div>
						</div>	
						<div class="row">
							<div class="col-md-6">
								<div class="subHeader">Event Information</div>
								<div><strong><?php echo $this -> EventSingle -> eventName?></strong></div>
							</div>
							<div class="col-md-6">
								<div class="subHeader">Super Event Description</div>
								<?php echo$this -> EventSingle -> SuperEventDescription ?>
							</div>	
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader" style='margin-top:10px;'>Super Event ID</div>
								<?php echo $structuredData['SuperEventIDText'] ?>
							</div>
						</div>
						
					</div>
				</div>
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Misc. Information
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<table>
									<tr>
										<td><strong>Performing Bands: </strong></td>
										<td><?php echo $structuredData['Performer'] ?></td>
									</tr>
									<tr>
										<td><strong>Entrance Fee: </strong></td>
										<td><?php echo $structuredData['EventPrice'] ?></td>
									</tr>
									<tr>
										<td><strong>Latitude: </strong></td>
										<td><?php echo $structuredData['Latitude'] ?></td>
									</tr>
									<tr>
										<td><strong>Longitude: </strong></td>
										<td><?php echo $structuredData['Longitude'] ?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3" style='position:relative'>
				<div class="WhiteSectionDiv">
					<div class="content">
						<?php if($this -> EventSingle -> SuperEvent == 1): ?>
							<div class="input">
								<div class="inputLabel"><strong>Super Event Photo</strong></div>
							</div>	
						<?php endif; ?>
							
						<?php if(!empty($this -> EventSingle -> CurrentEventImage)): ?>
							<img src='<?php echo $this -> EventSingle -> GetMainPhoto(); ?>' width='100%' />
						<?php else: ?>
							<div class="emptyPhotoPlacement" style='margin-bottom:0px;'>
								<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
								<div class="required">Photo Is not uploaded</div>
							</div>
						<?php endif; ?>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>






