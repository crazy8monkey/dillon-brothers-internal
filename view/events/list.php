<?php 

function date_compare($a, $b) {
	$t1 = strtotime($a['EventDate']);
	$t2 = strtotime($b['EventDate']);
	return $t2 - $t1;
}  
?>
			
<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Events
				</div>
			</div>
		</div>
	</div>
</div>
			
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">Event Caledar Upload</div>
								<div class="descriptiveInfo">This section is where you upload the pdf of the current year events, and this will be available for people to access on the front end site</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<img src="<?php echo PATH ?>public/images/PDFIndicate.png" /> <?php echo $this -> uploadPDFText; ?>
							</div>
						</div>
						<form method="post" action="<?php echo PATH ?>events/save/uploadpdf" id="UploadPDFForm">						
							<div class="row">
								<div class="col-md-12" style='margin-top:10px;'>
									<div id="inputID1">
										<div class="errorMessage"></div>
										<div class="input" style='border: 1px dashed; padding: 10px;'>
											<div class="inputLabel"><?php echo $this -> uploadPDFLabel ?></div>
											<input type='file' name="eventYearPDF" />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">	
									<div class="input">
										<input type='submit' value="Upload PDF" class="whiteButton" />
									</div>
								</div>
							</div>
						</form>
						<?php foreach($this -> PDFS as $pdf): ?>
							<div class="row">
								<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
								<div class="col-md-12">
									<a href='<?php echo PHOTO_URL?>EventPDFs/<?php echo $pdf['eventYearListName'] ?>' target='_blank'><?php echo $pdf['eventYearList'] ?> Events Calendar</a>
								</div>
							</div>
						<?php endforeach; ?>
						
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">Event List</div>
								<div class="descriptiveInfo">This section is where you create dillon brothers related events</div>
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> events() ?>/create/SuperEvent">
									<div class="greenButton" style="float:left; margin-right:10px;">
										Create Super Event
									</div>
								</a>
								<?php if(count($this -> SuperEvents) > 0): ?>
									<a href="<?php echo $this -> pages -> events() ?>/create/SubEvent">
										<div class="greenButton" style="float:left; margin-right:10px;">
											Create Sub Event
										</div>
									</a>
								<?php endif; ?>
								
								<a href="<?php echo $this -> pages -> events() ?>/create/StandAloneEvent">
									<div class="greenButton" style="float:left">
										Create Stand alone Event
									</div>
								</a>
							</div>
						</div>
												<?php foreach($this -> SuperEvents as $SuperEventSingle): ?>
							<div class="row">
								<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
								<div class="col-md-12">
									<div class="EventType SuperEvent" data-toggle="tooltip" data-placement="bottom" title="Super Event"></div>
									<a href="<?php echo $this -> pages -> events() ?>/edit/SuperEvent/<?php echo $SuperEventSingle['eventID'] ?>"><?php echo $SuperEventSingle['eventName'] ?></a>
								</div>
							</div>
							<?php 
								$superEventID = $SuperEventSingle['eventID'];
														
								$filteredSubEvents = array_filter($this -> SubEvents, function ($var) use($superEventID) {
									return $var['SuperEventID'] == $superEventID; 
								});
							
							?>
							
							<?php if(count($filteredSubEvents) > 0): ?>
								<div style="margin-left:20px;">
									<?php foreach($filteredSubEvents as $subEventSingle): ?>
										<div class="row">
											<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
											<div class="col-md-6">
												<a href="<?php echo $this -> pages -> events() ?>/delete/<?php echo $subEventSingle['eventID'] ?>" onclick="return confirm('Are you sure you want to delete this event? any images uploaded associated with this event will be deleted as well')">
													<i data-toggle="tooltip" data-placement="bottom" title="Delete" class="fa fa-trash" aria-hidden="true" style='float:left; margin-top: 3px; margin-right: 10px;'></i>
												</a>
												<div class="EventType SubEvent" data-toggle="tooltip" data-placement="bottom" title="<?php echo $SuperEventSingle['eventName'] ?> Sub Event"></div>
												<a href="<?php echo $this -> pages -> events() ?>/edit/SubEvent/<?php echo $subEventSingle['eventID'] ?>"><?php echo $subEventSingle['eventName']; ?></a>
											</div>
											<div class="col-md-6" style='text-align:right'>
												<?php echo $this -> recordedTime -> formatDate($subEventSingle['startDate']);?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							
						<?php endforeach; ?>
						<?php foreach($this -> StandaloneEvents as $StandaloneSingle): ?>
							<div class="row">
								<div style="border-top: 1px solid #f1f1f1; clear: both; margin:5px 15px 5px 15px;"></div>
								<div class="col-md-6">
									<a href="<?php echo $this -> pages -> events() ?>/delete/<?php echo $StandaloneSingle['eventID'] ?>" onclick="return confirm('Are you sure you want to delete this event? any images uploaded associated with this event will be deleted as well')">
										<i data-toggle="tooltip" data-placement="bottom" title="Delete" class="fa fa-trash" aria-hidden="true" style='float:left; margin-top: 3px; margin-right: 10px;'></i>
									</a>
									<div class="EventType StandAlone" data-toggle="tooltip" data-placement="bottom" title="Stand Alone Event"></div>
									<a href="<?php echo $this -> pages -> events() ?>/edit/Standalone/<?php echo $StandaloneSingle['eventID'] ?>"><?php echo $StandaloneSingle['eventName'] ?></a>
								</div>
								<div class="col-md-6" style='text-align:right'>
									<?php echo $this -> recordedTime -> formatDate($StandaloneSingle['startDate']);?>
								</div>
							</div>
						<?php endforeach; ?>	
						
						
						<?php if(count($this -> SuperEvents) == 0 && count($this -> StandaloneEvents) == 0): ?>
							<div class="row">
								<div class="col-md-12">
									There are no events entered
								</div>
							</div>
						<?php endif; ?>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


