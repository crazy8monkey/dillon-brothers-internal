<?php $structuredData = json_decode($this -> EventSingle -> StructuredData, true) ?>
<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> events() ?>">Events</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>View Event
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" name="HtmlEventJSON" id="HtmlEventJSON" value='<?php echo $this -> EventSingle -> GetHtml() ?>' />

<div class="ContentPage" style='padding-bottom:120px'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Viewing Stand Alone Event
							</div>
						</div>	
						<div class="row buttonContainer">
							<div class="col-md-12">
								<?php if($this -> EventSingle -> visible == '0') : ?>
									<div style="border:1px solid #18ad00; color:#18ad00; padding:10px; text-align:center;">
										Event is Not Published
									</div>
								<?php else : ?>			
									<div style="border:1px solid #18ad00; color:#18ad00; padding:10px; text-align:center;">
										Event is Published
									</div>			
								<?php endif; ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="subHeader">Event Information</div>
								<div><strong><?php echo $this -> EventSingle -> SuperEventName ?></strong>: <?php echo $this -> EventSingle -> eventName?></div>
								<table style='margin-top:20px;'>
									<tr>
										<td><strong>Start Date:</strong></td>
										<td><?php echo $this-> recordedTime -> formatDate($this -> EventSingle -> startDate) ?> / <?php echo $this-> recordedTime -> formatTimeNoSections($this -> EventSingle -> startTime) ?></td>
									</tr>
									<tr>
										<td><strong>End Date:</strong></td>
										<td><?php echo $this-> recordedTime -> formatDate($this -> EventSingle -> endDate) ?> / <?php echo $this-> recordedTime -> formatTimeNoSections($this -> EventSingle -> endTime) ?></td>
									</tr>
								</table>
								
							</div>
							<div class="col-md-6">
								<div class="subHeader">Locaiton</div>
								<?php echo $this -> EventSingle -> address ?><br />
								<?php echo $this -> EventSingle -> city ?>, <?php echo $this -> EventSingle -> state ?> <?php echo $this -> EventSingle -> zip ?>
							</div>	
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="subHeader" style='margin-top:20px;'>Website Placement</div>
								
								<?php foreach($this -> stores as $store) : ?>		
								<?php 
									$selectedStoreID;
									$checked = NULL;
									$storeIDS = explode(',', $this -> EventSingle -> storeIDS);
												
									foreach($storeIDS as $id) {
										if($id == $store['storeID']) {
											$checked = 'checked="checked"';		
										}
									} 
								?>
											
									<div style="margin-bottom:3px;">
										<input type="checkbox" name='store[]' disabled readonly value='<?php echo $store['storeID'] ?>' <?php echo $checked ?> style='float:left; margin-right: 2px;' /><?php echo $store['StoreName'] ?>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="col-md-6">
								<div class="subHeader">Google Event Description</div>
								<?php echo $this -> EventSingle -> GoogleEventDescription ?>	
							</div>
						</div>
					</div>
				</div>
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Misc. Information
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<table>
									<tr>
										<td><strong>Performing Bands: </strong></td>
										<td><?php echo $structuredData['Performer'] ?></td>
									</tr>
									<tr>
										<td><strong>Entrance Fee: </strong></td>
										<td><?php echo $structuredData['EventPrice'] ?></td>
									</tr>
									<tr>
										<td><strong>Latitude: </strong></td>
										<td><?php echo $structuredData['Latitude'] ?></td>
									</tr>
									<tr>
										<td><strong>Longitude: </strong></td>
										<td><?php echo $structuredData['Longitude'] ?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row sectionHeader"><div class="col-md-12">Page Layout</div></div>
						<div class="row"><div class="col-md-12"><div id="inputID2"><div class="errorMessage"></div></div></div></div>
										
						<?php 
							$JsonDecoded = json_decode($this -> EventSingle -> GetHtml(), true);
							usort($JsonDecoded, function($a, $b) {
								return $a['Order'] - $b['Order'];
							});				
						?>
										
						<div class="PageBuilderContent">
							<div id="LayoutContent">
								<?php foreach($JsonDecoded as $key => $row) :?>
												 	<div class="ContentRow" id="<?php echo $row['rowID'] ?>">
												 		<div style="width:100%; clear:both">
													 		
													 		<div id="row<?php echo $row['rowID'] ?>" style="margin-bottom:1px; clear:both" class="row ColumnRowContent">
													 			<?php if(isset($row['Columns'])) : ?>
													 				
													 				<?php usort($row['Columns'], function($a, $b) {
																			return $a['Order'] - $b['Order'];
																	}); ?>
													 				
															 		<?php foreach($row['Columns'] as $column) : ?>
															 			<div class="ColumnElement col-md-<?php echo $column['ClassID'] ?>" id="<?php echo $column['columnID'] ?>" data-columnid="<?php echo $column['ClassID'] ?>">
															 				<div class="CurrentContent">
															 					<?php if(isset($column['Content'])) : ?>
															 						<?php foreach($column['Content'] as $content) : ?>
															 							
															 							<?php if($content['ContentType'] == "Text") : ?>
															 								<div style="margin-bottom:5px;">
															 									Text<br>
															 									<textarea disabled readonly  style="max-width:100%" onkeyup="EventsController.AddContentWidget('<?php echo $row['rowID'] ?>', '<?php echo $column['columnID'] ?>', '<?php echo $content['ContentID'] ?>', this)"><?php echo $content["Value"] ?></textarea>
															 								</div>
															 							<?php endif; ?>	
															 							
															 							<?php if($content['ContentType'] == "Header1") : ?>
															 								<div style="margin-bottom:5px;">Header 1<br><input type="text" disabled readonly value='<?php echo $content["Value"] ?>' onkeyup="EventsController.AddContentWidget('<?php echo $row['rowID'] ?>', '<?php echo $column['columnID'] ?>', '<?php echo $content['ContentID'] ?>', this)"></div>
															 							<?php endif; ?>
															 							
															 							<?php if($content['ContentType'] == "Header2") : ?>
															 								<div style="margin-bottom:5px;">Header 2<br><input type="text" disabled readonly value='<?php echo $content["Value"] ?>' onkeyup="EventsController.AddContentWidget('<?php echo $row['rowID'] ?>', '<?php echo $column['columnID'] ?>', '<?php echo $content['ContentID'] ?>', this)"></div>
															 							<?php endif; ?>
															 							
															 							<?php if($content['ContentType'] == "Header3") : ?>
															 								<div style="margin-bottom:5px;">Header 3<br><input type="text" disabled readonly  value='<?php echo $content["Value"] ?>' onkeyup="EventsController.AddContentWidget('<?php echo $row['rowID'] ?>', '<?php echo $column['columnID'] ?>', '<?php echo $content['ContentID'] ?>', this)"></div>
															 							<?php endif; ?>
															 							
															 							<?php if($content['ContentType'] == "Photo") : ?>
																 							<div class="ContentSection" id="<?php echo $content['ContentID'] ?>" style="margin-bottom:5px;">
																	 							Photo<br>
																	 							<?php 
																	 							
																	 							if($content['Value'] != "") {
																	 								$photoOnClick = "EventsController.SelectPhoto(this, " . $content['Value'] . ")";
																								} else {
																									$photoOnClick = "EventsController.SelectPhoto(this)";
																								}
																	 								
																								?>
																		 						<a href="javascript:void(0);" onclick="<?php echo $photoOnClick ?>">
																		 							<i class="fa fa-picture-o" aria-hidden="true"></i>
																		 							<span class="photoSelectedText">
																		 								<?php if(empty($content['Value'])) : ?>
																		 								 	Select Photo
																		 								<?php else : ?>
																		 									Change Photo
																		 								<?php endif; ?>
																		 							</span>
																		 						</a>
																	 						</div>
															 							<?php endif; ?>
															 							
															 							<?php if($content['ContentType'] == "PhotoAlbum") : ?>
																 							<div class="ContentSection" id="<?php echo $content['ContentID'] ?>" style="margin-bottom:5px;">
																	 							Photo Album<br>
																	 							<a href="javascript:void(0);" onclick="EventsController.SelectPhotoAlbum(this<?php echo (!empty($content['Value']) ? ',' . $content['Value'] : '') ?>)">
																		 							<i class="fa fa-picture-o" aria-hidden="true"></i>
																		 							<span class="photoSelectedText">
																		 								<?php if(empty($content['Value'])) : ?>
																		 								 	Select Photo Album
																		 								<?php else : ?>
																		 									Change Photo Album
																		 								<?php endif; ?>
																		 							</span>
																		 						</a>
																	 						</div>
															 							<?php endif; ?>											
															 							
															 						<?php endforeach; ?>
															 					<?php endif; ?>
															 				</div>
															 				
																 			<div class="Empty">
																 				<div class="AddWidget"></div>	
																 			</div>
															 			</div>
															 		<?php endforeach; ?>
															 	<?php endif; ?>			
													 		</div>
															
													 	</div>
												 	</div>
												 <?php endforeach; ?>	
										
							</div>				
						</div>						
					</div>
				</div>	
			</div>
			<div class="col-md-3" style='position:relative'>
				<div class="WhiteSectionDiv">
					<div class="content">
						<?php if($this -> EventSingle -> SuperEvent == 1): ?>
							<div class="input">
								<div class="inputLabel">Super Event Photo</div>
							</div>	
						<?php endif; ?>
							
						<?php if(!empty($this -> EventSingle -> CurrentEventImage)): ?>
							<div style='margin-bottom:10px;'><strong>Current Event Main Photo</strong> </div>
							<img src='<?php echo $this -> EventSingle -> GetMainPhoto(); ?>' width='100%' />
						<?php else: ?>
							<div class="emptyPhotoPlacement" style='margin-bottom:0px;'>
								<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
								<div class="required">Photo Is not uploaded</div>
							</div>
						<?php endif; ?>
						<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('PhotoManagementAddEditDelete') == 1) : ?>				
						
							<div class="row">
									<div class="col-md-12">
										<div class="input" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;">
											<div class="inputLabel">Photo Album</div>
											<a href="<?php echo $this -> pages -> photos() ?>/view/album/<?php echo $this -> EventSingle -> AlbumID ?>" target="_blank">
												<div class="DirectoryLine">
													<div class="icon">
														<img style="width: 20px;" src="<?php echo PATH ?>public/images/FolderIcon.png">
													</div>
													<div class="text" style="margin-left: 35px; font-size: 16px;">
														Upload photos to this event 
													</div>
													<div style="clear:both"></div>
												</div>						
											</a>
						
										</div>
									</div>
							</div>
						<?php endif; ?>									
						</div>
					</div>
			</div>
		</div>
	</div>
</div>






