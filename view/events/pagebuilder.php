<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
<head>
	<title>Create Sub Event Html</title>
	<link rel="stylesheet" href="<?php echo PATH ?>public/grapesjs/css/grapes.min.css" /> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo PATH ?>public/grapesjs/grapes.min.js"></script>
	<script type="text/javascript" src="<?php echo PATH ?>public/grapesjs/grapes-album-view.js"></script>
	<style type='text/css'>
		* {
			margin:0;
			padding:0;
		}
		body, html {
			height: 100%;
		    margin: 0;
		    overflow: hidden;
		}
	</style>
</head>
<body>

<div id="grapeJSEditor"></div>

<script type='text/javascript'>
	var images = [<?php echo $this -> savedPhotos ?>];

	var editor = grapesjs.init({
	  container : '#grapeJSEditor',
	  height: '100%',
	  plugins: ['album-photo-select'],
	  <?php if(isset($this -> htmlTemplate)): ?>
	  	components: '<?php echo $this -> htmlTemplate['html'] ?>',
	  	style: '<?php echo $this -> htmlTemplate['css'] ?>',
	  <?php endif; ?>
	  storageManager: {
	  	isAutosave: true,
      	type: 'remote',
      	urlStore: "<?php echo PATH ?>events/htmlpush/<?php echo $this -> eventID; ?>",
     },
	assetManager: {
		assets: images,
		upload: false
	},
     styleManager : {
     	 sectors: [{
          name: 'General',
          buildProps: ['float', 'display', 'position', 'top', 'right', 'left', 'bottom'],
          properties:[{
              name: 'Alignment',
              property: 'float',
              type: 'radio',
              defaults: 'none',
              list: [
                { value: 'none', className: 'fa fa-times'},
                { value: 'left', className: 'fa fa-align-left'},
                { value: 'right', className: 'fa fa-align-right'}
              ],
            },
            { property: 'position', type: 'select'}
          ],
        },
        {
            name: 'Dimension',
            open: false,
            buildProps: ['width', 'height', 'max-width', 'min-height', 'margin', 'padding'],
            properties:[{
              property: 'margin',
              properties:[
                { name: 'Top', property: 'margin-top'},
                { name: 'Right', property: 'margin-right'},
                { name: 'Bottom', property: 'margin-bottom'},
                { name: 'Left', property: 'margin-left'}
              ],
            },{
              property  : 'padding',
              properties:[
                { name: 'Top', property: 'padding-top'},
                { name: 'Right', property: 'padding-right'},
                { name: 'Bottom', property: 'padding-bottom'},
                { name: 'Left', property: 'padding-left'}
              ],
            }],
          },
          {
            name: 'Typography',
            open: false,
            buildProps: ['font-family', 'font-size', 'font-weight', 'letter-spacing', 'color', 'line-height', 'text-align', 'text-decoration', 'text-shadow'],
            properties:[
              { name: 'Font', property: 'font-family'},
              { name: 'Weight', property: 'font-weight'},
              { name:  'Font color', property: 'color'},
              {
                property: 'text-align',
                type: 'radio',
                defaults: 'left',
                list: [
                  { value : 'left',  name : 'Left',    className: 'fa fa-align-left'},
                  { value : 'center',  name : 'Center',  className: 'fa fa-align-center' },
                  { value : 'right',   name : 'Right',   className: 'fa fa-align-right'},
                  { value : 'justify', name : 'Justify',   className: 'fa fa-align-justify'}
                ],
              },{
                property: 'text-decoration',
                type: 'radio',
                defaults: 'none',
                list: [
                  { value: 'none', name: 'None', className: 'fa fa-times'},
                  { value: 'underline', name: 'underline', className: 'fa fa-underline' },
                  { value: 'line-through', name: 'Line-through', className: 'fa fa-strikethrough'}
                ],
              },{
                property: 'text-shadow',
                properties: [
                  { name: 'X position', property: 'text-shadow-h'},
                  { name: 'Y position', property: 'text-shadow-v'},
                  { name: 'Blur', property: 'text-shadow-blur'},
                  { name: 'Color', property: 'text-shadow-color'}
                ],
            }],
          },
          {
            name: 'Decorations',
            open: false,
            buildProps: [ 'background-color', 'border-radius', 'border', 'box-shadow', 'background'],
            properties: [{
              property: 'border-radius',
              properties  : [
                { name: 'Top', property: 'border-top-left-radius'},
                { name: 'Right', property: 'border-top-right-radius'},
                { name: 'Bottom', property: 'border-bottom-left-radius'},
                { name: 'Left', property: 'border-bottom-right-radius'}
              ],
            },{
              property: 'box-shadow',
              properties: [
                { name: 'X position', property: 'box-shadow-h'},
                { name: 'Y position', property: 'box-shadow-v'},
                { name: 'Blur', property: 'box-shadow-blur'},
                { name: 'Spread', property: 'box-shadow-spread'},
                { name: 'Color', property: 'box-shadow-color'},
                { name: 'Shadow type', property: 'box-shadow-type'}
              ],
            },{
              property: 'background',
              properties: [
                { name: 'Image', property: 'background-image'},
                { name: 'Repeat', property:   'background-repeat'},
                { name: 'Position', property: 'background-position'},
                { name: 'Attachment', property: 'background-attachment'},
                { name: 'Size', property: 'background-size'}
              ],
            },],
          },
          {
            name: 'Extra',
            open: false,
            buildProps: ['transition', 'perspective', 'transform'],
            properties: [{
              property: 'transition',
              properties:[
                { name: 'Property', property: 'transition-property'},
                { name: 'Duration', property: 'transition-duration'},
                { name: 'Easing', property: 'transition-timing-function'}
              ],
            },{
              property: 'transform',
              properties:[
                { name: 'Rotate X', property: 'transform-rotate-x'},
                { name: 'Rotate Y', property: 'transform-rotate-y'},
                { name: 'Rotate Z', property: 'transform-rotate-z'},
                { name: 'Scale X', property: 'transform-scale-x'},
                { name: 'Scale Y', property: 'transform-scale-y'},
                { name: 'Scale Z', property: 'transform-scale-z'}
              ],
            }]
          }]
	
      }
      //assetManager: {
      //	assets: images,
      //	upload: false
      //}
  });
</script>

</body>
</html>