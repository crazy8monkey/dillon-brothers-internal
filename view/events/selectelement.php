<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
<head>
	<title><?php echo $this -> title ?></title>
		
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
				
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
			
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/EventsController.css" /> 
	
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo PATH ?>/public/js/FormStone.js" type="text/javascript"></script>
		
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		
	<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo PATH ?>public/js/EventsController.js"></script>	
	<style type='text/css'>
			
	</style>
		

</head>
<body class='BlueBackground'>
	<input type="hidden" name="HtmlEventJSON" id="HtmlEventJSON" value='<?php echo $this -> EventSingle -> GetHtml() ?>' />
	
	<div class="topSectionPage">
		<div style="padding:10px;">
			<div class="container-fluid">
				<?php if(isset($this -> albumView)): ?>			
					<div class="row">	
						<div class="col-md-12 sectionHeader" style="margin-bottom: 0px;">
							Select Album
						</div>
					</div>
				<?php else: ?>
					<div class="row">	
						<div class="col-md-12 sectionHeader" style="margin-bottom: 0px;">
							Select Photo
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 breadCrumbs" style='margin-bottom:0px; margin-top:10px;'>
							<a href="<?php echo PATH ?>events/selectphoto/<?php echo $this -> EventSingle -> GetEventID() ?>?RowIndex=<?php $_GET['RowIndex']; ?>&ColumnIndex=<?php $_GET['ColumnIndex']; ?>&ContentIndex=<?php $_GET['ContentIndex']; ?>">Select Album</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Select Photo
						</div>
					</div>
				<?php endif; ?>				
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<?php if(isset($this -> albumView)): ?>						
							<?php foreach(array_chunk($this -> albumsList, 4, true) as $albumFour): ?>
								<div class="row">
									<?php foreach($albumFour as $albumSingle): ?>
										<div class="col-xs-3">
											
											<?php 
												if(isset($this -> selectAlbumLink)) {
													$LinkAttributes = "href='javascript:void(0)' onclick='EventsController.AlbumIDSelected(" . $albumSingle['albumID'] . ")'";
												} else  {
													$LinkAttributes = "href='" . PATH . "events/selectphoto/" . $this -> EventSingle -> GetEventID() . '?RowIndex=' . $_GET['RowIndex'] . '&ColumnIndex=' . $_GET['ColumnIndex'] . '&ContentIndex=' . $_GET['ContentIndex'] . '&albumID=' . $albumSingle['albumID'] . "'";
												}
											?>
													
																						
											<a <?php echo $LinkAttributes ?>>
												<div class="AlbumSingle">
													<?php if($albumSingle['photoName'] == NULL): ?>
														<div class='emptyThumb'><img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' /></div>
													<?php else: ?>
														<?php
															switch($albumSingle['AlbumType']) {
																case 1:
																	$albumType = "/events/";
																	break;
																case 2:
																	$albumType = "/blog/";
																	break;
																case 3:
																	$albumType = "/misc/";
																	break;
															}
														?>
														<div class='Thumb' style='background:url(<?php echo PHOTO_URL ?><?php echo $albumSingle['ParentDirectory'] ?><?php echo $albumType ?><?php echo $albumSingle['albumFolderName'] ?>/<?php echo $albumSingle['photoName'] ?>-m.<?php echo $albumSingle['ext'] ?>)'></div>
													<?php endif; ?>
													
													<div class='albumText' style='text-align:center'><?php echo $albumSingle['albumName'] ?> (<?php echo $albumSingle['AlbumCount'] ?>)</div>
												</div>	
											</a>
											
										</div>
										
									<?php endforeach; ?>	
								</div>
								
							<?php endforeach; ?>
						<?php else: ?>
							<?php foreach(array_chunk($this -> photosList, 4, true) as $photosFour): ?>
								<div class="row">
									<?php foreach($photosFour as $photoSingle): ?>
										<div class="col-xs-3">
											<a href="javascript:void(0)" onclick='EventsController.photoSelectValue(<?php echo $photoSingle['photoID'] ?>)'>
												<div class="photoSingleSelect">
													<?php
														switch($photoSingle['AlbumType']) {
															case 1:
																$albumType = "/events/";
																break;
															case 2:
																$albumType = "/blog/";
																break;
															case 3:
																$albumType = "/misc/";
																break;
														}
													?>
													<div class='thumbNailContent' style='background:url(<?php echo PHOTO_URL ?><?php echo $photoSingle['ParentDirectory'] ?><?php echo $albumType ?><?php echo $photoSingle['albumFolderName'] ?>/<?php echo $photoSingle['photoName'] ?>-m.<?php echo $photoSingle['ext'] ?>)'></div>
												</div>	
											</a>
										</div>
									<?php endforeach; ?>	
								</div>
								
							<?php endforeach; ?>
						<?php endif; ?>
						
					</div>
				</div>
			</div>
		</div>	
	</div>
</body>	
</html>
