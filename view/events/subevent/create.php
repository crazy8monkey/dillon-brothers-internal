<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> events() ?>">Events</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create Sub Event
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='padding-bottom:120px'>
	<div class="container-fluid">
		
			<form method="post" action="<?php echo PATH ?>events/save/subevent" id="EventForm">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row sectionHeader">
							<div class="col-md-12">
								Create New Sub Event
							</div>
						</div>
					
						<div class="row">
							<div class="col-md-6" style='margin-bottom:20px;'>
								<div id="inputID20">
									<div class="errorMessage"></div>
									<div class="input">
										<div class="inputLabel">Related Super Event</div>																				
										<select name="SuperEventID" onchange="Globals.removeValidationMessage(20)">
											<option value="0">Please Select Super Event</option>
											<?php foreach($this -> SuperEvents as $superEventSingle): ?>
												<option value="<?php echo $superEventSingle['eventID'] ?>"><?php echo $superEventSingle['eventName'] ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12">
										<div class="subHeader">Event Information</div>		
										<div class="descriptiveInfo">The event name and when the event is suppose to start and end</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div id="inputID1">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Sub Event Name</div>
												<input id="eventName" class="eventName" type="text" name="eventName" placeholder='i.e. Round 1' onchange="Globals.removeValidationMessage(1)">
											</div>
										</div>
									</div>
								</div>	
								<div class="row ShowForSuperEvent" style="display:none">
									<div class="col-md-12">
										<div id="inputID14">
											<div class="errorMessage"></div>
											<?php echo $this-> form -> textarea("Super Event Description", "superEventDescription", 14) ?>
										</div>	
									</div>
								</div>
								<div class="row ShowForSuperEvent" style="display:none">
										<div class="col-md-12">
											<div id="inputID19">
												<div class="errorMessage"></div>
												<div class="input">
													<div class="inputLabel">Super Event ID</div>
													<div class="descriptiveInfo">bikenight, eurobikenight, DO NOT PUT # in input</div>
													<input type="text" name="superEventIDText" placeholder="i.e. bikenight" onchange="Globals.removeValidationMessage(19)">
												</div>
												
											</div>
										</div>
									</div>
								<div class="row HideForSuperEvent">
									<div class="col-md-3">
										<div id="inputID3">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("date", "Start Date", "eventStartDate", 3) ?>	
										</div>
									</div>
									<div class="col-md-3">
										<div id="inputID4">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("time", "Start Time", "eventStartTime", 4, '19:00') ?>
										</div>
									</div>
									<div class="col-md-3">
										<div id="inputID5">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("date", "End Date", "eventEndDate", 5) ?>	
										</div>
										
									</div>
									<div class="col-md-3">
										<div id="inputID6">
											<div class="errorMessage"></div>
											<?php echo $this -> form -> Input("time", "End Time", "eventEndTime", 6, '21:00') ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="HideForSuperEvent">
									<div class="row">
										<div class="col-md-12">
											<div class="subHeader">Location of Event</div>	
											<div class="descriptiveInfo">Please type in what the location of where this event going to be at</div>
										</div>
									</div>
				
									<div class="row">
										<div class="col-md-12">
											<div id="inputID9">
												<div class="errorMessage"></div>
												<?php echo $this -> form -> Input("text", "Address", "eventAddress", 9, "17400 W Maple Street") ?>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-8">
											<div id="inputID10">
												<div class="errorMessage"></div>
												<?php echo $this -> form -> Input("text", "City", "eventCity", 10, "Omaha") ?>
											</div>
										</div>
										<div class="col-md-2">
											<div id="inputID11">
												<div class="errorMessage"></div>
												<?php echo $this -> form -> select("State", "eventState", $this -> states(), 11, 'NE') ?>
											</div>
										</div>
										<div class="col-md-2">
											<div id="inputID12">
												<div class="errorMessage"></div>
												<?php echo $this -> form -> Input("text", "Zip", "eventZip", 12, "68116") ?>
											</div>
										</div>
									</div>	
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div id="inputID7">
									<div class="subHeader">Website Placement</div>
									<div class="descriptiveInfo">This indicates where what website this event will show up</div>
									<div class="errorMessage"></div>
									<input type="hidden" name='store' value='0' />
									<?php foreach($this -> stores as $store) : ?>
										<div style="margin-bottom:3px;">
											<input type="checkbox" name='store[]' value='<?php echo $store['storeID'] ?>' style='float:left; margin-right: 2px;' /><?php echo $store['StoreName'] ?>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="col-md-6 HideForSuperEvent">
								<div id="inputID13">
									<div class="subHeader">Google Event Description</div>
									<div class="descriptiveInfo">When an event is created, it will show up on our google calendar, this description you type in here will show on our google calendar</div>
									<div class="errorMessage"></div>
									<textarea id="googleDescription" name="googleDescription" onchange="Globals.removeValidationMessage(13)"></textarea>
								</div>
							</div>
							
						</div>
						
						
					</div>
				</div>	
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row sectionHeader">
							<div class="col-md-12">
								Misc. Information
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-8">
										<div id="inputID15">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Is there any performers/bands at this event?</div>
												<div class="descriptiveInfo">if there is not any performers please type "NA" in this input</div>
												<input class="performerInfo" type="text" name="performerInfo" placeholder="i.e. Nickelback" onchange="Globals.removeValidationMessage(15)">
											</div>
										</div>	
									</div>
									<div class="col-md-4">
										<div id="inputID16">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Is there a price entrace fee? (in dollars)</div>
												<div class="descriptiveInfo">if There is no entrance fee type in "00.00"</div>
												<input class="priceInfo" type="text" name="priceInfo" placeholder="i.e. 30.00" onchange="Globals.removeValidationMessage(16)">
											</div>
										</div>	
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="subHeader">Geo Location</div>	
								<div class="descriptiveInfo">
									<strong>Dillon Harley Omaha</strong> -  Latitude: 41.292938 / Longitutde: -96.1883625<br />
									<strong>Dillon Indian</strong> - Latitude: 41.29352 / Longitutde: -96.1892845<br />
									<strong>Dillon Motorsports</strong> - Latitude: 41.293818 / Longitutde: -96.1884585<br />
									<strong>Dillon Harley Fremont</strong> - Latitude: 41.4519858 / Longitutde: -96.4654377<br />
									<strong>Partsfish</strong> - Latitude: 41.294056 / Longitutde: -96.1884655
								</div>
								<div class="row">
									<div class="col-md-6">
										<div id="inputID17">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Latitude</div>
												<input class="latitude" type="text" name="latitude" onchange="Globals.removeValidationMessage(17)">
											</div>
										</div>	
									</div>
									<div class="col-md-6">
										<div id="inputID18">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Longitude</div>
												<input class="longitude" type="text" name="longitude" onchange="Globals.removeValidationMessage(18)">
											</div>
										</div>	
									</div>
								</diV>
							</div>
						</div>
					</div>
				</div>
				
				<div class="UploadPhotosSubmit" style="width: 100%; display: block;">
							<div class="ContentPage">
								<div class="container-fluid">
								<div class="row">
									<div class="col-md-8">
										<div style="float:left; margin-top: 2px;">
											<input type="submit" value="Create Event: Go to Step 2" class="greenButton" style="font-size: 16px; font-weight: normal; padding: 8px 20px;">	
										</div>
										<div style="margin-top: 6px; margin-left: 125px;" id="uploadingPhotosText">
											<img src="<?php echo PATH ?>public/images/ajax-loader.gif"> Uploading Photos
										</div>
										
									</div>
								</div>
								
							</div>
							</div>
							
						</div>
						
			</form>	


<div id="SuperEventPopup" style="display:none;">
	<a href="javascript:void(0);" onclick="EventsController.ClosePopup()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">
			<div class="text">Select Super Event</div>
			<a href="javascript:void(0);" onclick="EventsController.ClosePopup()">
				<div class="close" style="margin-top: -23px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>
		</div>
		<div class="FormContent"></div>
	</div>
</div>


