<?php $structuredData = json_decode($this -> EventSingle -> StructuredData, true) ?>





<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> events() ?>">Events</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Sub Event
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='padding-bottom:120px'>
	<div class="container-fluid">
			
			<input type='hidden' value='<?php echo $this -> EventSingle -> GetEventID() ?>' id="eventID" />
		
			<form method="post" action="<?php echo PATH ?>events/save/subevent/<?php echo $this -> EventSingle -> GetEventID() ?>" id="EventForm" enctype="multipart/form-data">
				<input type="hidden" name="HtmlEventJSON" id="HtmlEventJSON" value='<?php echo $this -> EventSingle -> GetHtml()  ?>' />
				<div class="row">
					<div class="col-md-9">
					
						<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row sectionHeader">
									<div class="col-md-12">
										Edit Sub Event
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<?php 
											$editedTime = explode('T', $this ->  logs[count($this ->  logs) - 1]['edittedTimeAndDate']);
										?>
										Last Edited by <?php echo $this ->  logs[count($this ->  logs) - 1]['firstName'] . ' ' . $this ->  logs[count($this ->  logs) - 1]['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1]) ?> <a href="javascript:void(0);" onclick="EventsController.getEditHistory(<?php echo $this -> EventSingle -> GetEventID() ?>)">View History</a>
										<div style='border-bottom: 1px solid #f7f7f7; margin-bottom:10px; padding-bottom:10px;'></div>
									</div>
								</div>
								<?php if($this -> EventSingle -> CurrentEventImageBig != NULL && $this -> EventSingle -> CurrentEventImageSmall != NULL) : ?>				
									<div class="row buttonContainer">
										<div class="col-md-6">
											<?php if($this -> EventSingle -> visible == '0') : ?>
												<a href="<?php echo $this -> pages -> events() ?>/save/publish/<?php echo $this -> EventSingle -> GetEventID() ?>">
													<div class="greenButton" style="float:left">
														Publish Event
													</div>
												</a>
											<?php else: ?>			
												<a href="<?php echo $this -> pages -> events() ?>/save/unpublish/<?php echo $this -> EventSingle -> GetEventID() ?>">
													<div class="greenButton" style="float:left">
														Unpublish Event
													</div>
												</a>
											<?php endif; ?>
										</div>
										<div class="col-md-6">
											<a href="<?php echo EVENT_PREVIEW_URL . $this -> EventSingle -> GetEventID() ?>" target="_blank">
												<div style="float:right" class="previewButton">
													Preview Event
												</div>	
											</a>
										</div>
									</div>
								<?php endif; ?>
								<div class="row">
									<div class="col-md-12">
										<div id="inputID20">
											<div class="errorMessage"></div>
											<div class="input">
												<div class="inputLabel">Related Super Event</div>											
												<select name="SuperEventID" onchange="Globals.removeValidationMessage(20)">
													<option value="0">Please Select Super Event</option>
													<?php foreach($this -> SuperEvents as $superEventSingle): ?>
														<option <?php if($this -> EventSingle -> _CurrentSuperEventID == $superEventSingle['eventID']): ?>selected<?php endif; ?> value="<?php echo $superEventSingle['eventID'] ?>"><?php echo $superEventSingle['eventName'] ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
										
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<div class="subHeader">Event Information</div>		
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="descriptiveInfo"><?php echo WEBSITE_PATH ?><?php echo $this -> EventSingle -> SuperEventURL ?><strong>#<?php echo $this -> EventSingle -> CurrentSEOUrl?></strong></div>
												<div class="input">
													<div class="inputLabel">URL Slug</div>
													<div id="inputID21">
														<div class="errorMessage"></div>	
														<input id="eventName" class="eventName" type="text" name="urlSlug" value="<?php echo $this -> EventSingle -> CurrentSEOUrl?>" placeholder='i.e. Throttle Thursday' onchange="Globals.removeValidationMessage(20)">
													</div>
												</div>
												
												
												<div id="inputID1">
													<div class="errorMessage"></div>
													<div class="input">
														<div class="inputLabel">Sub Event Name</div>
														<input id="eventName" class="eventName" type="text" name="eventName" value="<?php echo $this -> EventSingle -> eventName?>" placeholder='i.e. Round 1' onchange="Globals.removeValidationMessage(1)">
													</div>
												</div>
											</div>
										</div>	
										<div class="row">
											<div class="col-md-12">
												<div class="descriptiveInfo">The event name and when the event is suppose to start and end</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div id="inputID3">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("date", "Start Date", "eventStartDate", 3, $this -> EventSingle -> startDate) ?>	
												</div>
											</div>
											<div class="col-md-6">
												<div id="inputID4">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("time", "Start Time", "eventStartTime", 4, $this -> EventSingle -> startTime) ?>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div id="inputID5">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("date", "End Date", "eventEndDate", 5, $this -> EventSingle -> endDate) ?>	
												</div>
											</div>
											<div class="col-md-6">
												<div id="inputID6">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("time", "End Time", "eventEndTime", 6, $this -> EventSingle -> endTime) ?>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<div class="subHeader">Location of Event</div>	
												<div class="descriptiveInfo">Please type in what the location of where this event going to be at</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div id="inputID9">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Address", "eventAddress", 9, $this -> EventSingle -> address) ?>
												</div>
											</div>
										</div>
											
										<div class="row">
											<div class="col-md-12">
												<div id="inputID10">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "City", "eventCity", 10, $this -> EventSingle -> city) ?>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-8">
												<div id="inputID11">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> select("State", "eventState", $this -> states(), 11, $this -> EventSingle -> state) ?>
												</div>
											</div>
											<div class="col-md-4">
												<div id="inputID12">
													<div class="errorMessage"></div>
													<?php echo $this -> form -> Input("text", "Zip", "eventZip", 12, $this -> EventSingle -> zip) ?>
												</div>
											</div>
										</div>	
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div id="inputID7">
											<div class="subHeader">Website Placement</div>
											<div class="descriptiveInfo">This indicates where what website this event will show up</div>
											<div class="errorMessage"></div>
											<input type="hidden" name='store' value='0' />
											<?php foreach($this -> stores as $store) : ?>
											
												<?php 
													$selectedStoreID;
													$checked = NULL;
													$storeIDS = explode(',', $this -> EventSingle -> storeIDS);
													
													foreach($storeIDS as $id) {
														if($id == $store['storeID']) {
															$checked = 'checked="checked"';		
														}
													} 
												?>
												
												<div style="margin-bottom:3px;">
													<input type="checkbox" name='store[]' value='<?php echo $store['storeID'] ?>' <?php echo $checked ?> style='float:left; margin-right: 2px;' /><?php echo $store['StoreName'] ?>
												</div>
											<?php endforeach; ?>
										</div>
									</div>
									<div class="col-md-6 HideForSuperEvent">
										<div id="inputID13">
											<div class="subHeader">Google Event Description</div>
											<div class="descriptiveInfo">When an event is created, it will show up on our google calendar, this description you type in here will show on our google calendar</div>
											<div class="errorMessage"></div>
											<textarea id="googleDescription" name="googleDescription" onchange="Globals.removeValidationMessage(13)"><?php echo $this -> EventSingle -> GoogleEventDescription ?></textarea>
										</div>
									</div>
									
								</div>
								
								
							</div>
						</div>	
						<div class="WhiteSectionDiv">
							<div class="content">
								<div class="row sectionHeader">
									<div class="col-md-12">
										Misc. Information
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<div id="inputID15">
													<div class="errorMessage"></div>
													<div class="input">
														<div class="inputLabel">Is there any performers/bands at this event?</div>
														<div class="descriptiveInfo">if there is not any performers please type "NA" in this input</div>
														<input class="performerInfo" type="text" name="performerInfo" placeholder="i.e. Nickelback" value='<?php echo $structuredData['Performer'] ?>' onchange="Globals.removeValidationMessage(15)">
													</div>
												</div>	
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div id="inputID16">
													<div class="errorMessage"></div>
													<div class="input">
														<div class="inputLabel">Is there a price entrace fee? (in dollars)</div>
														<div class="descriptiveInfo">if There is no entrance fee type in "00.00"</div>
														<input class="priceInfo" type="text" name="priceInfo" placeholder="i.e. 30.00" value='<?php echo $structuredData['EventPrice'] ?>' onchange="Globals.removeValidationMessage(16)">
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="subHeader">Geo Location</div>	
										<div class="descriptiveInfo">
											<strong>Dillon Harley Omaha</strong> -  Latitude: 41.292938 / Longitutde: -96.1883625<br />
											<strong>Dillon Indian</strong> - Latitude: 41.29352 / Longitutde: -96.1892845<br />
											<strong>Dillon Motorsports</strong> - Latitude: 41.293818 / Longitutde: -96.1884585<br />
											<strong>Dillon Harley Fremont</strong> - Latitude: 41.4519858 / Longitutde: -96.4654377<br />
											<strong>Partsfish</strong> - Latitude: 41.294056 / Longitutde: -96.1884655
										</div>
										<div class="row">
											<div class="col-md-6">
												<div id="inputID17">
													<div class="errorMessage"></div>
													<div class="input">
														<div class="inputLabel">Latitude</div>
														<input class="latitude" type="text" name="latitude" value='<?php echo $structuredData['Latitude'] ?>' onchange="Globals.removeValidationMessage(17)">
													</div>
												</div>	
											</div>
											<div class="col-md-6">
												<div id="inputID18">
													<div class="errorMessage"></div>
													<div class="input">
														<div class="inputLabel">Longitude</div>
														<input class="longitude" type="text" name="longitude" value='<?php echo $structuredData['Longitude'] ?>' onchange="Globals.removeValidationMessage(18)">
													</div>
												</div>	
											</div>
										</diV>
									</div>
								</div>
							</div>
						</div>
					
						<div class="WhiteSectionDiv">
							<div class="content">
								<a href='javascript:void();' onclick="window.open('<?php echo PATH ?>events/pagebuilder/subevent/<?php echo $this -> EventSingle -> GetEventID() ?>','sharer','height=1000,width=1024');">
									<div class="whiteButton" style='float:left'>
										Create Page
									</div>
								</a>
								<div style='clear:both'></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-3" style='position:relative'>
					<div class="WhiteSectionDiv">
						<div class="content">
							<div style='margin-bottom:15px; padding-bottom:15px; border-bottom: 1px solid #dedede;'>
								<div class='SavedPhotoResponse' id="SavedMainPhotoResponse" style='display:none'></div>
								<?php if(empty($this -> EventSingle -> CurrentEventImageBig))  {
									$classNameBig = "redUploadButton";
								} else {
									$classNameBig = "greyUploadButton";
								} 
								?>
								<a href="javascript:void(0);" onclick="EventsController.UploadMainPhoto(<?php echo $this -> EventSingle -> AlbumID ?>)">
									<div class="<?php echo $classNameBig ?>">
										Upload Main Event Photo	
									</div>	
								</a>
								<div style='margin-bottom:10px;'><strong>Current Event Main Photo (Big Photo)</strong></div>
								<?php if(!empty($this -> EventSingle -> CurrentEventImageBig)): ?>
									<img src='<?php echo $this -> EventSingle -> GetEventMainImage(); ?>' width='100%' />
								<?php else: ?>
									<div class="emptyPhotoPlacement" style='margin-bottom:0px;'>
										<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
										<div class="required">Photo Is Required to show on website<br />(Big Photo)</div>
									</div>
								<?php endif; ?>	
							</div>
							<div style='margin-bottom:15px;'>
								<div class='SavedPhotoResponse' id="SavedThumbPhotoResponse" style='display:none'></div>
								<?php if(empty($this -> EventSingle -> CurrentEventImageSmall))  {
									$classNameSmall = "redUploadButton";
								} else {
									$classNameSmall = "greyUploadButton";
								} 
								?>	
								<a href="javascript:void(0);" onclick="EventsController.UploadThumbnailPhoto(<?php echo $this -> EventSingle -> AlbumID ?>)">
									<div class="<?php echo $classNameSmall ?>">
										Upload Thumbnail Event Photo	
									</div>	
								</a>
								<div style='margin-bottom:10px;'><strong>Current Event Main Photo (Small Photo)</strong></div>
								<?php if(!empty($this -> EventSingle -> CurrentEventImageSmall)): ?>
									
									<img src='<?php echo $this -> EventSingle -> GetMainPhotoSmall(); ?>' />
								<?php else: ?>
									<div class="emptyPhotoPlacement" style='margin-bottom:0px;'>
										<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
										<div class="required">Photo Is Required to show on website<br />(Small Photo)</div>
									</div>
								<?php endif; ?>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="input" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;">
										<div class="inputLabel">Photo Album</div>
										<a href="<?php echo $this -> pages -> photos() ?>/view/album/<?php echo $this -> EventSingle -> AlbumID ?>" target="_blank">
											<div class="DirectoryLine">
												<div class="icon">
													<img style="width: 20px;" src="<?php echo PATH ?>public/images/FolderIcon.png">
												</div>
												<div class="text" style="margin-left: 35px; font-size: 16px;">
													Upload photos to this event 
												</div>
												<div style="clear:both"></div>
											</div>						
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
					
				</div>
				
				<div class="UploadPhotosSubmit" style="width: 100%; display: block;">
							<div class="ContentPage">
								<div class="container-fluid">
								<div class="row">
									<div class="col-md-8">
										<div style="float:left; margin-top: 2px;">
											<input type="submit" value="Save Sub Event" class="greenButton" style="font-size: 16px; font-weight: normal; padding: 8px 20px;">	
										</div>
										<div style="margin-top: 6px; margin-left: 125px;" id="uploadingPhotosText">
											<img src="<?php echo PATH ?>public/images/ajax-loader.gif"> Uploading Photos
										</div>
										
									</div>
								</div>
								
							</div>
							</div>
							
						</div>
						
			</form>	


<div id="EventEditHistory" style="display:none;">
	<?php echo $this -> PopupReadOnly('Event Edit History', 'EventsController.closeEditHistory()')?>
</div>


<div id="AlbumPhotoSelect" style="display:none;">
			<a href="javascript:void(0);" onclick="EventsController.ClosePopup()">
				<div class="Overlay"></div>
			</a>
			<div class="Popup">
				<div class="formHeader">
					<div class="text"></div>
					<a href="javascript:void(0);" onclick="EventsController.ClosePopup()">
						<div class="close" style="margin-top: -23px;">
							<i class="fa fa-times" aria-hidden="true"></i>
						</div>						
					</a>

				</div>
				<div class="FormContent">
					<div class="photoAlbumContent"></div>
				</div>
			</div>
	</div>
</div>


