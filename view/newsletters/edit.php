<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> newsletters() ?>">Newsletters</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Edit Newsletter
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Edit Newsletter
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 buttonContainer">
								<?php if($this -> NewsLetterSingle -> Visible == 0): ?>
									<a href='<?php echo $this -> pages -> newsletters() ?>/publish/<?php echo $this -> NewsLetterSingle -> GetID() ?>'>
										<div class="greenButton" style='float:left;'>
											Publish Newsletter	
										</div>
									</a>
								<?php else: ?>
									<a href='<?php echo $this -> pages -> newsletters() ?>/unpublish/<?php echo $this -> NewsLetterSingle -> GetID() ?>'>
										<div class="greenButton" style='float:left;'>
											Unpublish Newsletter	
										</div>
									</a>
								<?php endif; ?>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div style="font-weight:bold">Newsletter Name</div>
								<?php echo $this -> NewsLetterSingle -> name ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div style="font-weight:bold">Newsletter Sent</div>
								<?php echo $this -> recordedTime -> formatDate($this -> NewsLetterSingle -> date) ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style="margin-top:10px;">
								<div style="font-weight:bold">Newsletter Thumbnail</div>
								<img src="<?php echo $this -> NewsLetterSingle -> GetThumbNail() ?>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



