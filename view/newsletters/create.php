<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> newsletters() ?>">Newsletters</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create Archive
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					New Newsletter
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="container">
							<div class="row">
								<form method="post" action="<?php echo $this -> pages -> newsletters() ?>/save" id="NewsArchiveForm" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-12">
											<div id="inputID1">
												<div class="errorMessage"></div>
											</div>
											<?php echo $this -> form -> FileInput("Newsletter Thumbnail", "newsletterThumb") ?>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div id="inputID3">
												<div class="errorMessage"></div>
												<?php echo $this -> form -> Input("text", "Newsletter Name", "newsletterName") ?>
											</div>
										</div>
										<div class="col-md-6">
											<div id="inputID4">
												<div class="errorMessage"></div>
												<?php echo $this -> form -> Input("date", "Newsletter Sent", "newsletterDate") ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<?php echo $this -> form -> Submit("Create", "greenButton") ?>
										</div>
									</div>
								</form>
							</div>
						</div>				
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



