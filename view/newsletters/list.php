<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					NewsLetters
				</div>
			</div>
			<div class="row buttonContainer">
				<div class="col-md-12">
					<a href="<?php echo $this -> pages -> newsletters() ?>/create">
						<div class="greenButton" style="float:left">
							Create Archive
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage" style='margin-bottom:70px;'>
	<div class="container-fluid">
		<?php if(count($this -> newsLetters) > 0): ?>
	
		<?php $prevYear = null ?>
		<?php 
			$yearNewsletters = array(); 
			$newsLettersByYear = array(); 
		?>
		<?php foreach($this -> newsLetters as $newsLetter) : ?>	
			<?php 
				$NewYear = explode("-", $newsLetter['DateSent']);
	
				if($prevYear != $NewYear[0]) {
					//echo "<strong>Array Push</strong><br />";
					array_push($newsLettersByYear, array("Year" => $NewYear[0],
														 "NewsLetters" => $this -> newsLetters));	
				}
				$prevYear = $NewYear[0]; 
				
			?>		
		<?php endforeach; ?>
		
		
		<!-- clear wrong date newsletters-->
		<?php 
		foreach($newsLettersByYear as $key => $value) {
			$Year = $value['Year']; 
			foreach($value['NewsLetters'] as $newLetterKey => $newsLetter) {
				$DateSentYear = explode("-", $newsLetter['DateSent']);
				if($Year != $DateSentYear[0]) {
					unset($newsLettersByYear[$key]['NewsLetters'][$newLetterKey]);
				}		
			}
		 } 
		 ?>
		
			
	
		<?php foreach($newsLettersByYear as $yearData) : ?>		
			<div class="row">
				<div class="col-md-12">
					<div class="WhiteSectionDiv">
						<div class="content">
							<div class="subHeader"><?php echo $yearData['Year'] ?></div>		
			
			
			<?php foreach(array_chunk($yearData['NewsLetters'], 6, true) as $newsLetters) : ?>
				<div class="row" style='margin-bottom:15px;'>
				<?php foreach($newsLetters as $newsLetter) : ?>	
					<div class="col-md-2">	
						<a href="<?php echo $this -> pages -> newsletters() ?>/edit/<?php echo $newsLetter['NewsLetterID'] ?>">
								<div class="newsLetterLink">
									<img src="<?php echo NEWSLETTER_URL. $newsLetter['folderName'] . '/' . $newsLetter['ThumbNail'] ?> " />
									<div class="name">
										<?php echo $newsLetter['Name'] ?>
									</div>
								</div>
							</a>
					</div>		
						
					
					<?php endforeach; ?>
			</div>
			<?php endforeach; ?>
							</div>
					</div>
					
				</div>
			</div>				
	
		<?php endforeach; ?>
	<?php endif; ?>
	</div>
</div>

