
<div class="container">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo PATH ?>events">Events</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>View Event
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			<div style="float:left; margin-right: 10px; color:#ffa414">
				<i class="fa fa-calendar-o" aria-hidden="true"></i>	
			</div>
			Event Page Builder
		</div>
	</div>
	<div class="row buttonContainer">
		<div class="col-md-6">
			<?php if($this -> EventSingle -> visible == '0') : ?>
				<a href="<?php echo PATH ?>admin/save/eventpublish/<?php echo $this -> EventSingle -> GetEventID() ?>">
					<div class="greenButton" style="float:left">
						Publish Event
					</div>
				</a>
			<?php else : ?>			
				<a href="<?php echo PATH ?>admin/save/eventunpublish/<?php echo $this -> EventSingle -> GetEventID() ?>">
					<div class="greenButton" style="float:left">
						Unpublish Event
					</div>
				</a>
			<?php endif; ?>
		</div>
		<div class="col-md-6">
			<div style="float:right" class="previewButton">
				Preview
				<div class="arrow">
					
				</div>
			</div>
		</div>
	</div>
	
	<form method="post" action="<?php echo PATH ?>admin/save/event/<?php echo $this -> EventSingle -> GetEventID() ?>" id="EventForm">
		<input type="hidden" name="HtmlEventJSON" id="HtmlEventJSON" value='<?php echo $this -> EventSingle -> GetHtml() ?>' />
		<div class="row">
			<div class="col-md-12">
				<div style="margin-bottom:3px; <?php if($this -> EventSingle -> SubEvent == 1): ?> display:none; <?php endif; ?>">
					<input type="hidden" name="SuperEvent" value="0" />
					<input type="checkbox" name="SuperEvent"  style="float:left; margin-right: 2px;" <?php if($this -> EventSingle -> SuperEvent == 1): ?> checked <?php endif; ?>>Super Event							
				</div>
				<div style="margin-bottom:3px; <?php if($this -> EventSingle -> SuperEvent == 1): ?> display:none; <?php endif; ?>">
					<input type="hidden" name="SubEvent" value="0" />
					<input type="checkbox" name="SubEvent" onchange="AdminController.showSuperEvents(this)" <?php if($this -> EventSingle -> SubEvent == 1): ?> checked <?php endif; ?> style="float:left; margin-right: 2px;">Sub Event <span id="SuperEventSelected" style="color:red; font-weight:bold"></span>						
				</div>
			</div>
			<input type="hidden" name="SuperEventID"  id="SuperEventID" />
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="contentSections">
					<div id="inputID1">
						<div class="errorMessage"></div>
						<?php echo $this -> form -> Input("text", "Event Name", "eventName", 1, $this -> EventSingle -> eventName) ?>	
					</div>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="input">
					<div id="inputID8"><div class="errorMessage"></div></div>
					<div class="inputLabel">Event Photo</div>
					<div style="clear:both">
						<a href="javascript:void(0);" onclick="AdminController.SelectPhotoFromAlbum('<?php echo PATH ?>', '<?php echo $this -> EventSingle -> ParentDirectory ?>','<?php echo $this -> EventSingle -> AlbumID?>', '<?php echo $this -> EventSingle -> albumName?>')">						
							<span style="color: #369009; font-size:20px; float: left; margin-top: -4px; margin-right: 5px;">
								<i class="fa fa-plus-circle" aria-hidden="true"></i>	
							</span><span id="SelectPhotoText">
								<?php if(isset($this -> EventSingle -> EventPhoto)) :?>
									Change Photo
									
								<?php else :?>
									Select Photo
								<?php endif; ?>
							</span>
							<input type="hidden" id="EventPhoto" name="EventPhoto" <?php if (isset($this -> EventSingle -> EventPhoto)) : ?> value="<?php echo $this -> EventSingle -> EventPhoto ?>" <?php endif; ?> />
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div id="inputID3">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("date", "Start Date", "eventStartDate", 3, $this -> EventSingle -> startDate) ?>	
				</div>
			</div>
			<div class="col-md-3">
				<div id="inputID4">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("time", "Start Time", "eventStartTime", 4, $this -> EventSingle -> startTime) ?>
				</div>
			</div>
			<div class="col-md-3">
				<div id="inputID5">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("date", "End Date", "eventEndDate", 5, $this -> EventSingle -> endDate) ?>	
				</div>
				
			</div>
			<div class="col-md-3">
				<div id="inputID6">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("time", "End Time", "eventEndTime", 6, $this -> EventSingle -> endTime) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="contentSections">
					<div class="subHeader">Location of Event</div>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="inputID9">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Address", "eventAddress", 9, $this -> EventSingle -> address) ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-8">
				<div id="inputID10">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "City", "eventCity", 10, $this -> EventSingle -> city) ?>
				</div>
			</div>
			<div class="col-md-2">
				<div id="inputID11">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> select("State", "eventState", $this -> states(), 11, $this -> EventSingle -> state) ?>
				</div>
			</div>
			<div class="col-md-2">
				<div id="inputID12">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Zip", "eventZip", 12, $this -> EventSingle -> zip) ?>
				</div>
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-md-12">
				<div class="contentSections">
					<div id="inputID7">
						<div class="errorMessage"></div>
						<div class="subHeader">Website Placement</div>
						<input type="hidden" name='store' value='0' />
						<?php foreach($this -> stores as $store) : ?>
							
							<?php 
								$selectedStoreID;
								$checked = NULL;
								$storeIDS = explode(',', $this -> EventSingle -> storeIDS);
								
								foreach($storeIDS as $id) {
									if($id == $store['storeID']) {
										$checked = 'checked="checked"';		
									}
								} 
							?>
							
							<div style="margin-bottom:3px;">
								<input type="checkbox" name='store[]' value='<?php echo $store['storeID'] ?>' <?php echo $checked ?> style='float:left; margin-right: 2px;' /><?php echo $store['Name'] ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="contentSections">
					<div id="inputID13">
						<div class="errorMessage"></div>
						<?php echo $this-> form -> textarea("Google Event Description", "googleDescription", 13, $this -> EventSingle -> GoogleEventDescription) ?>
					</div>
				</div>
			</div>
		</div>
		<?php echo $this -> MetaDataForm($this -> EventSingle -> MetaData) ?>
		
		<div class="row">
			<div class="col-md-12">
				<div class="input" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;">
					<div class="inputLabel">Photo Album</div>
					<a href="<?php echo PATH ?>admin/photos/events/<?php echo $this -> EventSingle -> ParentDirectory ?>/<?php echo $this -> EventSingle -> albumName ?>">
						<div class="DirectoryLine">
							<div class="icon">
								<img style="width: 20px;" src="<?php echo PATH ?>public/images/FolderIcon.png">
							</div>
							<div class="text" style="margin-left: 35px; font-size: 16px;">
								Upload Photos 
							</div>
							<div style="clear:both"></div>
						</div>						
					</a>

				</div>
			</div>
		</div>
		
		<div id="inputID2"><div class="errorMessage"></div></div>
		<div class="subHeader">Page Layout</div>
		<div class="PageBuilderContent">
			<?php 
			$JsonDecoded = json_decode($this -> EventSingle -> GetHtml(), true);
			usort($JsonDecoded, function($a, $b) {
					return $a['Order'] - $b['Order'];
			});
			
			 ?>
			 
			 <script type="text/javascript">
			//http://www.bacubacu.com/colresizable/#samples
			$(document).ready(function() {			
				$("#LayoutContent").sortable({
					placeholder: "ui-state-highlight",
					items: '.ContentRow',
					stop: function( event, ui ) {
						AdminController.UpdateRowOrder();
					}
				});	
				
				$(".WidgetsList .item").draggable({
					connectToSortable: ".AddWidget",
      				helper: "clone",
      				revert: "invalid",
      				cursor: 'move'
				});	
				
				
			});
		</script>

			
			<div id="LayoutContent">
				<?php foreach($JsonDecoded as $key => $row) :?>
				 	<div class="ContentRow" id="<?php echo $row['rowID'] ?>">
				 		<div style="width:100%; clear:both">
					 		<div class="DragAdd">
					 			<div style="text-align:center">
					 				<i class='fa fa-arrows-v' aria-hidden='true'></i>	
					 			</div>
					 			<a href="javascript:void(0)" onclick="AdminController.AddColumnInRow('row<?php echo $row['rowID'] ?>', '<?php echo $row['rowID'] ?>')">
						 			<div style="text-align:center" id="<?php echo $row['rowID'] ?>" data-toggle="tooltip" data-placement="bottom" title="Add Column" class="AddColumn">
						 				<i class="fa fa-plus-circle" aria-hidden="true"></i>
						 			</div>
					 			</a>
					 			<div style="text-align:center">
					 				<a href="javascript:void(0);" onclick="AdminController.DeleteRow('<?php echo $row['rowID'] ?>')">
					 					<i class="fa fa-trash-o" aria-hidden="true"></i>	
					 				</a>
					 			</div>
					 		</div>	
					 		
					 		<div id="row<?php echo $row['rowID'] ?>" style="margin-bottom:1px; clear:both" class="row ColumnRowContent">
					 			<?php if(isset($row['Columns'])) : ?>
					 				
					 				<?php usort($row['Columns'], function($a, $b) {
											return $a['Order'] - $b['Order'];
									}); ?>
					 				
							 		<?php foreach($row['Columns'] as $column) : ?>
							 			<div class="ColumnElement col-md-<?php echo $column['ClassID'] ?>" id="<?php echo $column['columnID'] ?>" data-columnid="<?php echo $column['ClassID'] ?>">
							 				<div class="CurrentContent">
							 					<?php if(isset($column['Content'])) : ?>
							 						<?php foreach($column['Content'] as $content) : ?>
							 							
							 							<?php if($content['ContentType'] == "Text") : ?>
							 								<div style="margin-bottom:5px;">
							 									Text<br>
							 									<textarea style="max-width:100%" onkeyup="AdminController.AddContentWidget('<?php echo $row['rowID'] ?>', '<?php echo $column['columnID'] ?>', '<?php echo $content['ContentID'] ?>', this)"><?php echo $content["Value"] ?></textarea>
							 								</div>
							 							<?php endif; ?>	
							 							
							 							<?php if($content['ContentType'] == "Header1") : ?>
							 								<div style="margin-bottom:5px;">Header 1<br><input type="text" value='<?php echo $content["Value"] ?>' onkeyup="AdminController.AddContentWidget('<?php echo $row['rowID'] ?>', '<?php echo $column['columnID'] ?>', '<?php echo $content['ContentID'] ?>', this)"></div>
							 							<?php endif; ?>
							 							
							 							<?php if($content['ContentType'] == "Header2") : ?>
							 								<div style="margin-bottom:5px;">Header 2<br><input type="text" value='<?php echo $content["Value"] ?>' onkeyup="AdminController.AddContentWidget('<?php echo $row['rowID'] ?>', '<?php echo $column['columnID'] ?>', '<?php echo $content['ContentID'] ?>', this)"></div>
							 							<?php endif; ?>
							 							
							 							<?php if($content['ContentType'] == "Header3") : ?>
							 								<div style="margin-bottom:5px;">Header 3<br><input type="text" value='<?php echo $content["Value"] ?>' onkeyup="AdminController.AddContentWidget('<?php echo $row['rowID'] ?>', '<?php echo $column['columnID'] ?>', '<?php echo $content['ContentID'] ?>', this)"></div>
							 							<?php endif; ?>
							 							
							 							<?php if($content['ContentType'] == "Photo") : ?>
								 							<div class="ContentSection" id="<?php echo $content['ContentID'] ?>" style="margin-bottom:5px;">
									 							Photo<br>
										 						<a href="javascript:void(0);" onclick="AdminController.SelectPhoto(this, <?php echo $content['Value'] ?>)">
										 							<i class="fa fa-picture-o" aria-hidden="true"></i>
										 							<span class="photoSelectedText">
										 								<?php if(empty($content['Value'])) : ?>
										 								 	Select Photo
										 								<?php else : ?>
										 									Change Photo
										 								<?php endif; ?>
										 							</span>
										 						</a>
									 						</div>
							 							<?php endif; ?>
							 							
							 							<?php if($content['ContentType'] == "PhotoAlbum") : ?>
								 							<div class="ContentSection" id="<?php echo $content['ContentID'] ?>" style="margin-bottom:5px;">
									 							Photo Album<br>
									 							<?php 
									 							$PhotoAlbumOnClick = NULL;
																if(!empty($content['Value'])) {
																	$PhotoAlbumOnClick	= "AdminController.SelectPhotoAlbum(this, " + $content['Value'] + ")";
																} else {
																	$PhotoAlbumOnClick	= "AdminController.SelectPhotoAlbum(this)";
																}
									 							?>
									 							
										 						<a href="javascript:void(0);" onclick="<?php echo $PhotoAlbumOnClick ?>">
										 							<i class="fa fa-picture-o" aria-hidden="true"></i>
										 							<span class="photoSelectedText">
										 								<?php if(empty($content['Value'])) : ?>
										 								 	Select Photo Album
										 								<?php else : ?>
										 									Change Photo Album
										 								<?php endif; ?>
										 							</span>
										 						</a>
									 						</div>
							 							<?php endif; ?>											
							 							
							 						<?php endforeach; ?>
							 					<?php endif; ?>
							 				</div>
							 				<div class="delete">
							 					<div style="text-align:center">
								 					<a href="javascript:void(0);" onclick="AdminController.DeleteColumn('row<?php echo $row['rowID'] ?>', '<?php echo $column['columnID'] ?>', '<?php echo $row['rowID'] ?>')">
									 					<i class="fa fa-trash" aria-hidden="true"></i>						 						
								 					</a>							 						
							 					</div>
							 				</div>
								 			<div class="Empty">
								 				<div class="AddWidget"></div>	
								 			</div>
							 			</div>
							 		<?php endforeach; ?>
							 	<?php endif; ?>			
					 		</div>
					 		<?php if(isset($row['Columns'])) : ?>
						 		<div class="slider">
						 			<table id="rangeRow<?php echo $row['rowID'] ?>" width="100%" cellspacing="0" cellpadding="0">
										<tr>
									 		<?php foreach($row['Columns'] as $column) : ?>
									 			<td style='width:<?php echo ($column['ClassID'] / 12) * 100 ?>%'></td>
									 		<?php endforeach; ?>
								 		</tr>
								 	</table>	 			
						 		</div>
							<?php endif; ?>
					 		
				 		
						 	<script type="text/javascript">
						 		//http://www.bacubacu.com/colresizable/#samples
								$(document).ready(function() {
									EventsController.DroppableLines('<?php echo $row['rowID'] ?>');
									EventsController.SortableColumns('<?php echo $row['rowID'] ?>')
									EventsController.ColumnResizable('<?php echo $row['rowID'] ?>');
								});
							</script>
					 	</div>
				 	</div>
				 <?php endforeach; ?>	
			</div>
			
			
			
			<a href="javascript:void(0)" onclick="EventsController.AddLayoutRow()">
				<div class="row AddRow">
					Add Row
				</div>	
			</a>
		</div>
		
		<div class="widgetMenu">
			<div class="WidgetsList">
				<div class="item" id="Text">
					Text
				</div>
				<div class="item" id="Header1">
					Header 1
				</div>
				<div class="item" id="Header2">
					Header 2
				</div>
				<div class="item" id="Header3">
					Header 3
				</div>
				<div class="item" id="Photo">
					Photo
				</div>
				<div class="item" id="PhotoAlbum">
					Photo Album
				</div>
			</div>
		</div>
		
		<div id="AlbumPhotoSelect" style="display:none;">
			<a href="javascript:void(0);" onclick="AdminController.ClosePopup()">
				<div class="Overlay"></div>
			</a>
			<div class="Popup">
				<div class="formHeader">
					<div class="text"></div>
					<a href="javascript:void(0);" onclick="AdminController.ClosePopup()">
						<div class="close" style="margin-top: -23px;">
							<i class="fa fa-times" aria-hidden="true"></i>
						</div>						
					</a>

				</div>
				<div class="FormContent">
					<div class="photoAlbumContent"></div>
				</div>
			</div>
		</div>
		
		<div class="UploadPhotosSubmit" style="width: 100%; display: block;">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div style="float:left; margin-top: 2px;">
							<input type="submit" value="Save Event" class="greenButton" style="font-size: 16px; font-weight: normal; padding: 8px 20px;">	
						</div>
						<div style="margin-top: 6px; margin-left: 125px;" id="uploadingPhotosText">
							<img src="http://localhost/DillonBrothers/public/images/ajax-loader.gif"> Uploading Photos
						</div>
						
					</div>
				</div>
				
			</div>
			
		</div>
	</form>
</div>

