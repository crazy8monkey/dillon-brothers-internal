<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php echo $this -> photoSingle -> CreateSinglePhotoBreadCrumbs() ?>		
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Photo Details
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="uploadedInfo">
									<strong>Uploaded By:</strong> <?php echo $this -> photoSingle -> GetUploadedBy() ?><br />	
									<?php echo $this -> recordedTime -> formatDate($this -> photoSingle -> updatedDate) ?> / <?php echo $this -> recordedTime -> formatTime($this -> photoSingle -> updatedTime) ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<?php echo $this -> form -> ValiationMessage("PhotoSingleMessage", "PhotoSingleLoading", "PhotoSingleText") ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								
							</div>
						</div>
						
						<form method="post" id="editPhoto" action="<?php echo $this -> pages -> photos() ?>/save/photo/<?php echo $this -> photoSingle -> GetPhotoID() ?>">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Photo Name", "photoName", 1, $this -> photoSingle -> CurrentImageName) ?>	
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div id="inputID2">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Image Alt Text", "altText", 2, $this -> photoSingle -> altText) ?>
									</div>
								</div>
								<div class="col-md-6">
									<div id="inputID3">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Image Title Text", "titleText", 3, $this -> photoSingle -> titleText) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit("Save", "greenButton") ?>									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="photoContainer" style='margin: -10px; position:relative'>
									<?php if($this -> photoSingle -> PhotoWaterMark == 0): ?>									
										<div style='position:absolute; top:10px'>
											<a href="<?php echo $this -> pages -> photos() ?>/watermark/single/<?php echo $this -> photoSingle -> GetPhotoID() ?>">
												<div class="blueButton" style="float:left; margin-left:10px;">
													Water Mark photos
												</div>
											</a>
										</div>
									<?php endif; ?>
									<div style="position:absolute; bottom: 10px; left: 10px;">
										<?php 
										$currentPhoto = PHOTO_URL . $this -> photoSingle -> parentDirectory . $this -> photoSinglePath . $this -> photoSingle -> _albumFolderName . '/' . $this -> photoSingle -> GetImageNameLarge();
										?>
										<a href="javascript:void(0);" onclick="PhotosController.CopyUrl('<?php echo $currentPhoto?>')">
											<div class="whiteButton">
												Copy Url Image Path
											</div>
										</a>
										<input type="text" style="display:none;" value="<?php echo PHOTO_URL . $this -> photoSingle -> parentDirectory . $this -> photoSinglePath . $this -> photoSingle -> _albumFolderName . '/' . $this -> photoSingle -> GetImageNameLarge()?>" id="CopyTextElement" />
									</div>
									<img src="<?php echo PHOTO_URL . $this -> photoSingle -> parentDirectory . $this -> photoSinglePath . $this -> photoSingle -> _albumFolderName . '/' . $this -> photoSingle -> GetImageNameLarge()?>" />
									
									<div style="position:absolute; bottom: 10px; right: 10px;">
										<div style="float:right">
											<div style="float:left; margin-right:10px;">
												<a href="<?php echo $this -> pages -> photos() ?>/rotate/right/<?php echo $this -> photoSingle -> GetPhotoID() ?>">
													<img src="<?php echo PATH ?>public/images/RotateRight.png" data-toggle="tooltip" data-placement="bottom" title="Rotate Right" />
												</a>
											</div>
											<div style="float:left; margin-right:10px;">
												<a href="<?php echo $this -> pages -> photos() ?>/rotate/left/<?php echo $this -> photoSingle -> GetPhotoID() ?>">
													<img src="<?php echo PATH ?>public/images/RotateLeft.png" data-toggle="tooltip" data-placement="bottom" title="Rotate Left" />
												</a>
											</div>							
										</div>


									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	
	
	
	
	
	
	
	
</div>
