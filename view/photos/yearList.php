<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> photos() ?>">Photos</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> yearSelect ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="subHeader">Add/Edit Year Photo</div>
						<div style='margin-bottom:10px;'><strong>Current Year Photo</strong></div>
						<?php if(empty($this -> yearInfo -> photo)): ?>
							<div class="emptyPhotoPlacement" style='margin-bottom:10px;'>
								<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
								<div class="required">No Uploaded Photo For this year</div>
							</div>
						<?php else: ?>
							<div style="border: 1px dashed; padding: 10px; text-align:center;">
								<img src='<?php echo PHOTO_URL . $this -> yearInfo -> yearText . '/' .$this -> yearInfo -> photo?>' />
							</div>
						<?php endif; ?>
						
						<form method="post" action="<?php echo PATH ?>photos/save/yearphoto/<?php echo $this -> yearInfo -> yearID ?>" id="YearForm">
							<div class="row">
								<div class="col-md-12">	
									<div id="inputID1"><div class="errorMessage"></div></div>
									<div class="input" style="border: 1px dashed; padding: 10px;">
										<div class="inputLabel">Upload Year Photo</div>
										<input type="file" name="yearPhotoUpload">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">	
									<?php echo $this -> form -> Submit("Create", "greenButton") ?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div class="subHeader">Select your Category</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<a href="<?php echo PATH ?>photos/type/events/<?php echo $this -> yearSelect ?>" class="DirectoryLink">			
									<div class="PhotoElementBtn" style='margin-bottom:0px; border:1px solid #f9f9f9;'>
										Events	
									</div>
								</a>
							</div>
							<div class="col-md-4">
								<a href="<?php echo PATH ?>photos/type/blog/<?php echo $this -> yearSelect ?>" class="DirectoryLink">			
									<div class="PhotoElementBtn" style='margin-bottom:0px; border:1px solid #f9f9f9;'>
										Blog
									</div>
								</a>
							</div>
							<div class="col-md-4">
								<a href="<?php echo PATH ?>photos/type/misc/<?php echo $this -> yearSelect ?>" class="DirectoryLink">			
									<div class="PhotoElementBtn" style='margin-bottom:0px; border:1px solid #f9f9f9;'>
										Miscellaneous
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
