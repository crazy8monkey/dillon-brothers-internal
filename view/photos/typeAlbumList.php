<?php
$ShowCreateAlbumSection = false; 
if(isset($this -> eventsList) || isset($this -> MiscAlbums)) {
	$ShowCreateAlbumSection = true;
}


?>

<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> photos() ?>">Photos</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> photos() ?>/year/<?php echo $this -> yearSelect ?>"><?php echo $this -> yearSelect ?></a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> typeText ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<?php if($ShowCreateAlbumSection == true): ?>
							<div style='margin:-10px; padding: 10px; border-bottom: 1px solid #f9f9f9; margin-bottom: 10px;'>
								<a href="javascript:void(0)" onclick="PhotosController.AddAlbum()">
									<div class="greenButton" style="float:left">
										Create Album
									</div>
								</a>
								<div style="clear:both"></div>
							</div>
						<?php endif; ?>
						
						<?php if(count($this -> albumList) > 0): ?>
							<?php foreach(array_chunk($this -> albumList, 6, true) as $albumTwelve) : ?>
								<div class="row" style="margin-bottom:10px; margin-left:0px; margin-right:0px">
									<?php foreach($albumTwelve as $albumSingle): ?>
										<div class="col-md-2">
											<a href="<?php echo $this -> pages -> photos() ?>/view/album/<?php echo $albumSingle['albumID'] ?>">															
												<?php if($albumSingle['photoName'] != NULL): ?>
													<?php 
														$albumType = NULL;
														switch($albumSingle['AlbumType']) {
															case 1:
																$albumType = "events";
																break;
															case 2:
																$albumType = "blog";
																break;
															case 3:
																$albumType = "misc";
																break;
														}
													?>
													
													<div class="photoThumbNail" style="background:url(<?php echo PHOTO_URL . $albumSingle['ParentDirectory'] ?>/<?php echo $albumType ?>/<?php echo $albumSingle['albumFolderName'] ?>/<?php echo $albumSingle['photoName'] ?>-s.<?php echo $albumSingle['ext'] ?>) no-repeat center; margin-bottom:10px">
														<div class="deleteAlbum"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
													</div>
												<?php else: ?>
													<div class="PhotoAlbumElementEmpty" style="margin-bottom: 10px;">
														<div class="indicator">
															<img src="<?php echo PATH ?>public/images/PhotoAlbumIcon.png">	
														</div>
														<a href="<?php echo PATH ?>photos/delete/album/<?php echo $albumSingle['albumID'] ?>" onclick="return confirm('Are you sure you want to delete this Album? this action cannot be undone and any photos in this album will be removed permanently')"><div class="deleteAlbum"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
													</div>
												<?php endif; ?>
												
												<div class="albumName">
													<div><strong><?php echo $albumSingle['albumName'] ?></strong></div>
													<div style="font-size:12px"><?php echo $albumSingle['PhotoCount'] ?> Photo(s)</div>
												</div>
											</a>
										</div>
									<?php endforeach; ?>		
								</div>
								
							<?php endforeach; ?>
						<?php else: ?>
							There are no photo albums submitted in this year and this category
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php if($ShowCreateAlbumSection == true): ?>
	<div id="AddAlbumElement" style='display:none;'>
		<a href="javascript:void(0);" onclick="PhotosController.ClosePopup()">
			<div class="Overlay"></div>
		</a>
		<div class="Popup">
			<div class="formHeader">
				Create Album			
				<a href="javascript:void(0);" onclick="PhotosController.ClosePopup()">
					<div class="close" style="margin-top: 4px;">
						<i class="fa fa-times" aria-hidden="true"></i>
					</div>						
				</a>	
			</div>
			<div class="FormContent" style='margin-right: 15px;'>
				<div id="NewAlbumErrorMessages" style='color:#c30000'></div>
				<form action="<?php echo $this -> pages -> photos()?>/save/newalbum?year=<?php echo $this -> yearSelect ?>" method="post" id="ArchiveAlbumForm">
					<input type="hidden" name="albumType" value="<?php echo $this -> TypeInteger ?>" />
					
					<div class="input">
						<div id="inputID1"><div class="errorMessage"></div></div>
						<input id="albumName" type="text" name="albumName" onchange="Globals.removeValidationMessage(1)">
					</div>	
					<?php if(isset($this -> eventsList)) : ?>					
						<div class="input">
							<div class="inputLabel">Linked Event</div>
							<select name="albumEvent">
								<option value="0"></option>
								<?php foreach($this -> eventsList as $event): ?>
									<option value="<?php echo $event['UniqueIdentifier']?>"><?php echo $event['EventText']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					<?php endif; ?>
					<div class="input">
						<?php echo $this -> form -> Submit("Create", "greenButton") ?>
					</div>
				</form>
				
				
			</div>
		</div>	
	</div>
<?php endif; ?>
