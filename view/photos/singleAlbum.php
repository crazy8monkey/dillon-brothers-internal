<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php echo $this -> AlbumDetails -> GenerateAlbumViewBreadCrumbs() ?>		
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="subHeader">Album Details</div>
						<div class="row">
							<div class="col-md-12">
								<div class="uploadedInfo">
									<strong>Album Created By:</strong> <?php echo $this -> AlbumDetails -> GetCreatedByName() ?><br />	
									<?php echo $this -> recordedTime -> formatDate($this -> AlbumDetails -> createdDate) ?> / <?php echo $this -> recordedTime -> formatTime($this -> AlbumDetails -> createdTime) ?>
								</div>
							</div>
						</div>
						<form action="<?php echo $this -> pages -> photos() ?>/save/albumdetails/<?php echo $this -> AlbumDetails -> albumID ?>" method="post" id="PhotoAlbumDetailsForm">		
							<div class="row">
								<div class="col-md-12" style='margin-bottom:10px'>
									<input type="hidden" name="photoAlbumVisible" value="0" />
									<input type="checkbox" name="photoAlbumVisible" value='1' <?php if($this -> AlbumDetails -> Visible == 1): ?>checked<?php endif; ?> style="float:left; margin-right: 2px;">Photo Album Visible
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Album Name</div>
											<input type="text" name="albumName" value="<?php echo $this -> AlbumDetails -> GetAlbumName(); ?>" onchange="Globals.removeValidationMessage(1)">
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="input">
										<div class="inputLabel">Linked Event (Optional)</div>
										<select name="linkedAlbumEvent">
											<option value="0"></option>
											<?php foreach($this -> eventsList as $event): ?>
												<option <?php if($this -> AlbumDetails -> LinkedEvent == $event['UniqueIdentifier']): ?> selected <?php endif; ?>value="<?php echo $event['UniqueIdentifier']?>"><?php echo $event['EventText']?></option>
											<?php endforeach; ?>
										</select>
									</div>	
								</div>
							</div>
							
							
							
							<div class="row">
								<div class="col-md-12" style='margin-top:10px; margin-bottom:-10px'>
									<?php echo $this -> form -> Submit("Save Album", "greenButton") ?>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<div style='margin-bottom:5px;'><strong>Current Album Thumb</strong> <?php if($this -> AlbumDetails -> photoCount > 0) : ?><a href="javascript:void(0);" onclick="PhotosController.changeThumbNail(<?php echo $this -> AlbumDetails -> albumID ?>)">Change Thumbnail</a><?php endif; ?></div>
								<?php if(!empty($this -> AlbumDetails -> albumThumbNail)): ?>
									<div class='photoThumbNail' style='background:url(<?php echo PHOTO_URL . $this -> AlbumDetails -> year . $this -> photoPath . $this -> AlbumDetails -> AlbumFolderName . '/' . $this -> AlbumDetails ->  AlbumThumbNailName . '-s.' .$this -> AlbumDetails ->  AlbumThumbNailExt ?>) no-repeat center'></div>
								<?php else: ?>
									<div class="PhotoAlbumElementEmpty" style="width:250px">
										<div class="indicator">
											<img src="<?php echo PATH ?>public/images/PhotoAlbumIcon.png">	
										</div>		
									</div>
								<?php endif; ?>	
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div style='margin:-10px; padding:10px; border-bottom:1px solid #f7f7f7'>
							<a href="<?php echo $this -> pages -> photos() ?>/addphotos/<?php echo $this -> AlbumDetails -> AlbumFolderName ?>">
								<div class="whiteButton" style="float:left">
									Add Photos
								</div>
							</a>
							
							<?php if( in_array(0, array_column($this -> photos, 'photowatermark'))): ?>							
								<a href="<?php echo $this -> pages -> photos() ?>/watermark/all/<?php echo $this -> AlbumDetails -> albumID ?>">
									<div class="blueButton" style="float:left; margin-left:10px;">
										Water Mark photos
									</div>
								</a>
							<?php endif; ?>
							<div style="clear:both"></div>
						</div>
						<div style='margin-top:25px'>
							<?php if(count($this -> photos) > 0): ?>
								<?php foreach(array_chunk($this -> photos, 6, true) as $photoSix) : ?>
									<div class="row" style='margin: 0px -10px;'>
										<?php foreach($photoSix as $photo): ?>
											<div class="col-md-2">
												<div class="GrayBox" style="position:relative;height: 150px; margin-bottom: 10px;">
													<div class="thumbnailContainer" style="background: url(<?php echo PHOTO_URL . $this -> AlbumDetails-> year . $this -> photoPath . $this -> AlbumDetails-> AlbumFolderName . '/' . $photo['photoName'] . '-s.' . $photo['ext'] ?>)  center;"></div>
													<div class="photoOverlay">
														<div class="view">
															<a href="<?php echo $this -> pages -> photos() ?>/view/photo/<?php echo $photo['photoID'] ?>" style="color: white;">
																<i class="fa fa-eye" aria-hidden="true" data-toggle='tooltip' data-placement='bottom' title="view"></i>	
															</a>
														</div>
														<div class="delete">
															<a href="<?php echo $this -> pages -> photos() ?>/delete/photo/<?php echo $photo['photoID'] ?>" style="color: white;" onclick="return confirm('Are you sure you want to delete this photo? this action cannot be undone')">
																<i class="fa fa-trash-o" aria-hidden="true" data-toggle='tooltip' data-placement='bottom' title="delete"></i>	
															</a>
															
														</div>
														<div style="clear:both"></div>
													</div>
												</div>
												<div class="visibilitySwitch" style="margin-bottom: 20px;">
													<label class="switch">
													  <input type="checkbox" <?php echo $photo['visible'] == 1 ? "checked=''" : ''; ?> id="image<?php echo $photo['photoID'] ?>">
													  <div data-on="1" data-off="0" class="slider round" id="<?php echo $photo['photoID'] ?>" data-toggle='tooltip' data-placement='bottom' title='<?php echo $photo['visible'] == 1 ? "Photo visible" : 'Photo not visible'; ?>' onclick="PhotosController.ToggleVisibleImage(<?php echo $photo['photoID'] ?>, '#image<?php echo $photo['photoID'] ?>')"></div>
													</label>
												</div>
											</div>	
										<?php endforeach; ?>
									</div>
								<?php endforeach; ?>	
							<?php else: ?>
								There are no photos in this album
							<?php endif; ?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container">

	<div style="border-bottom:1px solid #f9f9f9; margin: 10px 0px 15px 0px;"></div>
	
	
				
	
	
	
</div>

<div id="PhotosInAlbum" style="display:none;">
	<?php echo $this -> PopupReadOnly('Select Album Thumbnail', 'PhotosController.closeSelectPhoto()')?>
</div>

