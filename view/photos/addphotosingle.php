<div class="topSectionPage">
	<div style='padding:10px;'>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader" style='margin-bottom: 0px;'>
					Add Main Photo: <?php echo $this -> AlbumSingle -> _albumName ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content" style='font-size:14px;'>
						Max Upload Size: <?php echo ini_get('upload_max_filesize') ?><br />
						Max File Upload per submission: <?php echo ini_get('max_file_uploads') ?><br />	
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12">
								<?php echo $this -> form -> ValiationMessage("AddPhotosMessage", "AddPhotosLoading", "AddPhotosText") ?>
							</div>
						</div>
						<form enctype="multipart/form-data" method="post" action="<?php echo $this -> pages -> photos() ?>/uploadtoalbum/<?php echo $this -> AlbumSingle -> albumID ?>" id="AddPhotos">		
							<div class="row" style="margin-bottom:10px;">
								<div class="col-md-12">
									<input type="file" name="file" id="filesToUpload" onchange="PhotosController.MakeFileList('<?php echo $this -> AlbumSingle -> _albumName ?>', '<?php echo $this -> AlbumSingle -> UploadCount ?>')" />
								</div>
							</div>
							<input type="hidden" name="AlbumType" value="<?php echo $this -> albumType ?>" />
							<input type="hidden" name="PhotoCount" id="PhotoCount" />
							
							<?php if(isset($_GET['blogMainPhoto'])): ?>
								<input type="hidden" name="BlogMainPhoto" value="1" />
							<?php endif; ?>
							<?php if(isset($_GET['EventMainPhoto'])): ?>
								<input type="hidden" name="EventMainPhoto" value="1" />
							<?php endif; ?>
										
							<?php if(isset($_GET['EventThumbNailPhoto'])): ?>
								<input type="hidden" name="EventThumbNailPhoto" value="1" />
							<?php endif; ?>
							
							
							<div class="ProgressTotal"></div>
							<div class="progressBar" style="display:none;">
								<div class="progressContent">
									
								</div>
							</div>
							<?php //echo $_SESSION['UploadProgress']?>
							<div id="fileList" style="margin-bottom: 110px;"></div>
							
							<div class="UploadPhotosSubmit" style="width:100%">
								<div class="container">
									<div class="row">
										<div class="col-md-12" style='margin-bottom:10px;'>
											<strong>Upload Size:</strong> <span id="UploadSizeTotal"></span><br />
											<strong>Current File Uploads:</strong> <span id="FileUploadTotal"></span>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div style="float:left; margin-top: 2px;">
													<input type="submit" value="Upload Photos" class="greenButton" style="font-size: 16px; font-weight: normal; padding: 8px 20px;" onclick="PhotosController.UploadPhotos()" />	
											</div>
											<div style="padding-left: 35px; padding-top:4px; margin-left: 125px;" id="uploadingPhotosText">
												<img src="<?php echo PATH ?>public/images/ajax-loader.gif"> Uploading Photos
											</div>
										
										</div>
									</div>
								</div>
							</div>
						</form>	
			
					</div>
				</div>
			</div>
		</div>
</div>

