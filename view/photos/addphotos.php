<script type="text/javascript">
		$(document).ready(function() {
			$(".upload").upload({
								maxSize: 1073741824,
								beforeSend: onBeforeSend
							}).on("start.upload", onStart)
							.on("complete.upload", onComplete)
							.on("filestart.upload", onFileStart)
							.on("fileprogress.upload", onFileProgress)
							.on("filecomplete.upload", onFileComplete)
							.on("fileerror.upload", onFileError)
							.on("chunkstart.upload", onChunkStart)
							.on("chunkprogress.upload", onChunkProgress)
							.on("chunkcomplete.upload", onChunkComplete)
							.on("chunkerror.upload", onChunkError)
							.on("queued.upload", onQueued);
						$(".filelist.queue").on("click", ".cancel", onCancel);
						$(".cancel_all").on("click", onCancelAll);
			
			
		
		})
		
		//https://formstone.it/components/upload/
		function onCancel(e) {
						console.log("Cancel");
						var index = $(this).parents("li").data("index");
						$(this).parents("form").find(".upload").upload("abort", parseInt(index, 10));
					}
					function onCancelAll(e) {
						console.log("Cancel All");
						$(this).parents("form").find(".upload").upload("abort");
					}
					function onBeforeSend(formData, file) {
						console.log("Before Send");
						formData.append("test_field", "test_value");
						// return (file.name.indexOf(".jpg") < -1) ? false : formData; // cancel all jpgs
						return formData;
					}
					function onQueued(e, files) {
						console.log("Queued");
						var html = '';
						for (var i = 0; i < files.length; i++) {
							html += '<li data-index="' + files[i].index + '"><span class="content"><span class="file">' + files[i].name + '</span><span class="cancel">Cancel</span><span class="progress">Queued</span></span><span class="bar"></span></li>';
						}
						$(this).parents("form").find(".filelist.queue")
							.append(html);
					}
					function onStart(e, files) {
						console.log("Start");
						$("#PleaseWaitElement").show();
						$(this).parents("form").find(".filelist.queue")
							.find("li")
							.find(".progress").text("Waiting");
					}
					function onComplete(e) {
						console.log("Complete");
						$("#PleaseWaitElement").fadeOut(function () {
							$("#UploadFinished").fadeIn();	
						});						
						// All done!
					}
					function onFileStart(e, file) {
						console.log("File Start");
						$(this).parents("form").find(".filelist.queue")
							.find("li[data-index=" + file.index + "]")
							.find(".progress").text("0%");
					}
					function onFileProgress(e, file, percent) {
						console.log("File Progress");
						var $file = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");
						$file.find(".progress").text(percent + "%")
						$file.find(".bar").css("width", percent + "%");
					}
					function onFileComplete(e, file, response) {
						console.log("File Complete");
						if (response.trim() === "" || response.toLowerCase().indexOf("error") > -1) {
							$(this).parents("form").find(".filelist.queue")
								.find("li[data-index=" + file.index + "]").addClass("error")
								.find(".progress").text(response.trim());
						}
						else {
							var $target = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");
							$target.find(".file").text(file.name);
							$target.find(".progress").remove();
							$target.find(".cancel").remove();
							$target.appendTo($(this).parents("form").find(".filelist.complete"));
						}
					}
					function onFileError(e, file, error) {
						console.log("File Error");
						$(this).parents("form").find(".filelist.queue")
							.find("li[data-index=" + file.index + "]").addClass("error")
							.find(".progress").text("Error: " + error);
					}
					function onChunkStart(e, file) {
						console.log("Chunk Start");
					}
					function onChunkProgress(e, file, percent) {
						console.log("Chunk Progress");
					}
					function onChunkComplete(e, file, response) {
						console.log("Chunk Complete");
					}
					function onChunkError(e, file, error) {
						console.log("Chunk Error");
					}
		
		
</script>
	<style type='text/css'>
			.filelists {
						margin: 20px 0;
					}
					
					.filelists h5 {
						margin: 10px 0 0;
					}
					
					.filelists .cancel_all {
						color: red;
						cursor: pointer;
						clear: both;
						font-size: 10px;
						margin: 0;
						text-transform: uppercase;
					}
					
					.filelist {
						margin: 0;
						padding: 10px 0;
					}
					
					.filelist li {
						background: #fff;
						border-bottom: 1px solid #ECEFF1;
						font-size: 14px;
						list-style: none;
						padding: 5px;
						position: relative;
					}
					
					.filelist li:before {
						display: none !important;
					}
					/* main site demos */
					
					.filelist li .bar {
						background: #eceff1;
						content: '';
						height: 100%;
						left: 0;
						position: absolute;
						top: 0;
						width: 0;
						z-index: 0;
						-webkit-transition: width 0.1s linear;
						transition: width 0.1s linear;
					}
					
					.filelist li .content {
						display: block;
						overflow: hidden;
						position: relative;
						z-index: 1;
						padding:0px;
					}
					
					.filelist li .file {
						color: #455A64;
						float: left;
						display: block;
						overflow: hidden;
						text-overflow: ellipsis;
						max-width: 50%;
						white-space: nowrap;
					}
					
					.filelist li .progress {
						color: #B0BEC5;
						display: block;
						float: right;
						font-size: 10px;
						text-transform: uppercase;
						background:transparent !important;
						margin-bottom:0px;
						border:none !important;
						webkit-box-shadow: inset 0 0px 0px rgba(0,0,0,.1) !important;
    					box-shadow: inset 0 0px 0px rgba(0,0,0,.1) !important;
					}
					
					.filelist li .cancel {
						color: red;
						cursor: pointer;
						display: block;
						float: right;
						font-size: 10px;
						margin: 0 0 0 10px;
						text-transform: uppercase;
					}
					
					.filelist li.error .file {
						color: red;
					}
					
					.filelist li.error .progress {
						color: red;
					}
					
					.filelist li.error .cancel {
						display: none;
					}					
	</style>


<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<?php if(!isset($this -> uploadSingleView)): ?>
				<div class="row">
					<div class="col-md-12 breadCrumbs" style='font-size:14px;'>
						<?php echo $this -> AlbumSingle -> GetAddPhotosBreadCrumbs() ?>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Add Photos: <?php echo $this -> AlbumSingle -> _albumName ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage" style='padding-bottom:120px'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content" style='font-size:14px;'>
						Max Upload Size: <?php echo ini_get('upload_max_filesize') ?><br />
						Max File Upload per submission: <?php echo ini_get('max_file_uploads') ?><br />	
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div id="PleaseWaitElement" style='padding-bottom:10px; display:none;'>
							Please wait until the upload progress is finished <strong>(refreshing the page or clicking back on your history will interrupt the progress)</strong>
						</div>
						<div id="UploadFinished" style='padding-bottom:10px; display:none;'>
							Photo upload has been complete, please click <a href="<?php echo PATH ?>photos/view/album/<?php echo $this -> AlbumSingle -> albumID ?>">here</a> to go back to the photo album
						</div>
						<form action="#" method="GET" class="form demo_form">
						<div class="upload" data-upload-options='{"action":"<?php echo $this -> pages -> photos() ?>/uploadtoalbum/<?php echo $this -> AlbumSingle -> albumID ?>"}'></div>
						
						<div class="filelists demo_content">
							<h5>Complete</h5>
							<ol class="filelist complete">
							</ol>
							<h5>Queued</h5>
							<ol class="filelist queue">
							</ol>
							<span class="cancel_all">Cancel All</span>
						</div>
						
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(!isset($this -> multipePhotoUpload)): ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> form -> ValiationMessage("AddPhotosMessage", "AddPhotosLoading", "AddPhotosText") ?>
			</div>
		</div>
		
		<form enctype="multipart/form-data" method="post" action="<?php echo $this -> pages -> photos() ?>/uploadtoalbum/<?php echo $this -> AlbumSingle -> albumID ?>" id="AddPhotos">		
			<div class="row" style="margin-bottom:10px;">
				<div class="col-md-12">
					<input type="file" name="photoUpload[]" <?php echo (isset($_GET['singlePhoto']) ? '' : "multiple='multiple'") ?>  id="filesToUpload" onchange="PhotosController.MakeFileList('<?php echo $this -> AlbumSingle -> _albumName ?>', '<?php echo $this -> AlbumSingle -> UploadCount ?>')" />
				</div>
			</div>
			<input type="hidden" name="AlbumType" value="<?php echo $this -> albumType ?>" />
			<input type="hidden" name="PhotoCount" id="PhotoCount" />
			
			<?php if(isset($_GET['blogMainPhoto'])): ?>
				<input type="hidden" name="BlogMainPhoto" value="1" />
			<?php endif; ?>
			<?php if(isset($_GET['EventMainPhoto'])): ?>
				<input type="hidden" name="EventMainPhoto" value="1" />
			<?php endif; ?>

			
			
			<div class="ProgressTotal"></div>
			<div class="progressBar" style="display:none;">
				<div class="progressContent">		
				</div>
			</div>
			<div id="fileList" style="margin-bottom: 110px;"></div>
			
			<div class="UploadPhotosSubmit" style="width:100%">
				<div class="container">
					<div class="row">
						<div class="col-md-12" style='margin-bottom:10px;'>
							<strong>Upload Size:</strong> <span id="UploadSizeTotal"></span><br />
							<strong>Current File Uploads:</strong> <span id="FileUploadTotal"></span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div style="float:left; margin-top: 2px;">
									<input type="submit" value="Upload Photos" class="greenButton" style="font-size: 16px; font-weight: normal; padding: 8px 20px;" onclick="PhotosController.UploadPhotos()" />	
							</div>
							<div style="padding-left: 35px; padding-top:4px; margin-left: 125px;" id="uploadingPhotosText">
								<img src="<?php echo PATH ?>public/images/ajax-loader.gif"> Uploading Photos
							</div>
						
						</div>
					</div>
				</div>
			</div>
		</form>	
	</div>
<?php endif; ?>
