<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 sectionHeader">
					Photo Management System
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="subHeader">Add New Year Directory</div>
						<form action="<?php echo $this -> pages -> photos()?>/save/year" method="post" id="YearDirectoryForm">
							<div class="row">
								<div class="col-md-12">	
									<div id="inputID1">
										<div class="errorMessage"></div>
										<input id="yearDirectory" type="text" name="yearDirectory" placeholder="Year" onchange="Globals.removeValidationMessage(1)" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style='margin-top:10px'>	
									<?php echo $this -> form -> Submit("Create", "greenButton") ?>
								</div>
							</div>
						</form>
						
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="WhiteSectionDiv">
					<div class="content">
						<?php foreach(array_chunk($this -> years, 6, true) as $years) : ?>
							<div class="row">
								<?php foreach($years as $yearSingle) : ?>										
									<div class="col-md-2">
										<a href="<?php echo $this -> pages -> photos() ?>/year/<?php echo $yearSingle['YearText'] ?>">
											
											<div class="PhotoElementBtn">
												<div class="icon">
													<img src="<?php echo PATH ?>public/images/FolderIcon.png" />	
												</div>
												<?php echo $yearSingle['YearText'] ?>
											</div>						
										</a>
					
									</div>
								<?php endforeach; ?>
							</div>			
						<?php endforeach; ?>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	
	
	
	
	
</div>
