<div class="container" style="padding-bottom: 120px;">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo PATH ?>events">Events</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Create Event
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			<div style="float:left; margin-right: 10px; color:#ffa414">
				<i class="fa fa-calendar-o" aria-hidden="true"></i>	
			</div>
			Event Page Builder
		</div>
	</div>
	<form method="post" action="<?php echo PATH ?>events/save" id="EventForm">
		<div class="row">
			<div class="col-md-12">
				<div style="margin-bottom:3px;">
					<input type="hidden" name="SuperEvent" value="0" />
					<input type="checkbox" name="SuperEvent"  style="float:left; margin-right: 2px;">Super Event							
				</div>
				<div style="margin-bottom:3px;">
					<input type="hidden" name="SubEvent" value="0" />
					<input type="checkbox" name="SubEvent" onchange="AdminController.showSuperEvents(this)" style="float:left; margin-right: 2px;">Sub Event	<span id="SuperEventSelected" style="color:red; font-weight:bold"></span>						
				</div>
			</div>
			<input type="hidden" name="SuperEventID"  id="SuperEventID" />
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="contentSections">				
					<div id="inputID1">
						<div class="errorMessage"></div>
						<?php echo $this -> form -> Input("text", "Event Name", "eventName", 1) ?>	
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div id="inputID3">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("date", "Start Date", "eventStartDate", 3) ?>	
				</div>
			</div>
			<div class="col-md-3">
				<div id="inputID4">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("time", "Start Time", "eventStartTime", 4) ?>
				</div>
			</div>
			<div class="col-md-3">
				<div id="inputID5">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("date", "End Date", "eventEndDate", 5) ?>	
				</div>
				
			</div>
			<div class="col-md-3">
				<div id="inputID6">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("time", "End Time", "eventEndTime", 6) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="contentSections">
					<div class="subHeader">Location of Event</div>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="inputID9">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Address", "eventAddress", 9) ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-8">
				<div id="inputID10">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "City", "eventCity", 10) ?>
				</div>
			</div>
			<div class="col-md-2">
				<div id="inputID11">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> select("State", "eventState", $this -> states(), 11) ?>
				</div>
			</div>
			<div class="col-md-2">
				<div id="inputID12">
					<div class="errorMessage"></div>
					<?php echo $this -> form -> Input("text", "Zip", "eventZip", 12) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="contentSections">
					<div id="inputID7">
						<div class="subHeader">Website Placement</div>
						<div class="errorMessage"></div>
						<input type="hidden" name='store' value='0' />
						<?php foreach($this -> stores as $store) : ?>
							<div style="margin-bottom:3px;">
								<input type="checkbox" name='store[]' value='<?php echo $store['storeID'] ?>' style='float:left; margin-right: 2px;' /><?php echo $store['Name'] ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="contentSections">
					<div id="inputID13">
						<div class="errorMessage"></div>
						<?php echo $this-> form -> textarea("Google Event Description", "googleDescription", 13) ?>
					</div>
				</div>
				
			</div>
		</div>
		<?php echo $this -> MetaDataForm() ?>
	</div>
		
		
		
		<div class="UploadPhotosSubmit" style="width: 100%; display: block;">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div style="float:left; margin-top: 2px;">
							<input type="submit" value="Create Event" class="greenButton" style="font-size: 16px; font-weight: normal; padding: 8px 20px;">	
						</div>
						<div style="margin-top: 6px; margin-left: 125px;" id="uploadingPhotosText">
							<img src="http://localhost/DillonBrothers/public/images/ajax-loader.gif"> Uploading Photos
						</div>
						
					</div>
				</div>
				
			</div>
			
		</div>
	</form>
</div>

<div id="SuperEventPopup" style="display:none;">
	<a href="javascript:void(0);" onclick="AdminController.ClosePopup()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">
			<div class="text">Select Super Event</div>
			<a href="javascript:void(0);" onclick="AdminController.ClosePopup()">
						<div class="close" style="margin-top: -23px;">
							<i class="fa fa-times" aria-hidden="true"></i>
						</div>						
					</a>

	</div>
	<div class="FormContent">
	
	</div>
	</div>
</div>


