<?php if(isset($this -> inventory)): ?>
	<div class="row" style='margin-bottom:10px;'>
		<div class="col-md-6">
			<a href="javascript:void(0);" onclick="SpecialsController.LinkInventoryToIncentive(<?php echo $this -> SpecialSingle -> GetID() ?>)">
				<div class="selectInventoryButton">
					<?php if(count($this -> items) > 0) : ?>		
						Edit Associated Inventory
					<?php else: ?>	
						Select Associated Inventory
					<?php endif; ?>		
				</div>
			</a>
		</div>
	</div>
	<?php function() {
		
	} ?>
	<?php if(count($this -> items) > 0) : ?>		
		<?php foreach(array_chunk($this -> items, 2, true) as $incentiveTwo) : ?>
			<div class="row">
				<?php foreach($incentiveTwo as $incentiveSingle) : ?>
					<div class="col-md-6">
						<div style='margin-bottom:20px;'>
							<div class="linkedInventoryIncentiveHeader">
								<div style='float:left'>
									<?php 
										$headerTitle = '';
										if($incentiveSingle['RelatedItemSpecialType'] == 'Percentage Off') {
											$headerTitle = 	$incentiveSingle['Value'] . '% Off';
										} else if($incentiveSingle['RelatedItemSpecialType'] == 'Dollar Off') {
											$headerTitle = 	'$'. $incentiveSingle['Value'] . ' Off';
										} else if($incentiveSingle['RelatedItemSpecialType'] == 'Special Financing') {
											$headerTitle = 	$incentiveSingle['Value'] . '% APR';
										}
										
										
										
										echo $headerTitle; 
									?>	
								</div>
								<div style='float:right; font-weight:normal; font-size:16px;'>
									Exp. <?php echo $this -> recordedTime -> formatDate($incentiveSingle['ExpiredDate']) ?>
								</div>
								<div style='clear:both'></div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div><strong>Additional/Secondary Offer Description</strong></div>
									<?php echo $incentiveSingle['Description'] ?>
								</div>
								<div class="col-md-6">
									<div><strong>Disclaimer</strong></div>
									<?php echo $incentiveSingle['Disclaimer'] ?>
								</div>
							</div>
							<div class="row" style='margin-top:10px;'>
								
									<?php 
									$incentiveID = $incentiveSingle['RelatedIncentiveID'];
										
									$filteredInventory = array_filter($this -> linkedInventory, function ($var) use($incentiveID) {
											 return $var['RelatedIncentiveID'] == $incentiveID; 
										}
									);
									
									?>
									<?php if(count($filteredInventory) > 0): ?>				
										<?php foreach(array_chunk($filteredInventory, 2, true) as $filteredInventoryDuece) : ?>
											<?php foreach($filteredInventoryDuece as $inventorySingle): ?>
												<div class="col-md-6">
													<?php 
														if($inventorySingle['FriendlyModelName'] != NULL) {
															$bikeName = $inventorySingle['Year'] . ' ' . $inventorySingle['Manufacturer'] . ' ' . $inventorySingle['FriendlyModelName'];
														} else {
															$bikeName = $inventorySingle['Year'] . ' ' . $inventorySingle['Manufacturer'] . ' ' . $inventorySingle['ModelName'];
														}
														echo $bikeName . ' - <strong>'. $inventorySingle['Stock'] . '</strong>';
													?>
													
												</div>
											<?php endforeach; ?>
										<?php endforeach; ?>					
										
									<?php else: ?>
										<div class="col-md-12">
											<a href="javascript:void(0);" onclick="SpecialsController.LinkInventoryToIncentive(<?php echo $this -> SpecialSingle -> GetID() ?>)">
												<div class="selectInventoryButton">
													Select Inventory
												</div>
											</a>
										</div>
										
									<?php endif; ?>
								
							</div>	
						</div>
						
					</div>
					
					
				<?php endforeach; ?>	
			</div>
		<?php endforeach; ?>
	<?php endif; ?>							
	
	
<?php else: ?>
	<div id="inputID10"><div class="errorMessage"></div></div>
	<div id="inputID11"><div class="errorMessage"></div></div>
	<div id="inputID12"><div class="errorMessage"></div></div>
	<div id="inputID13"><div class="errorMessage"></div></div>
	<div id="inputID14"><div class="errorMessage"></div></div>
	<div id="inputID15"><div class="errorMessage"></div></div>
	<form action="<?php echo $this -> pages -> specials() ?>/save/specialitems/<?php echo $this -> SpecialSingle -> GetID() ?>" method="post" id="SpecialItemForm">
		<div id="ContentList">
			<?php if(count($this -> items) > 0) : ?>								
				<?php foreach($this -> items as $key => $item): ?>
					<div class="incentiveItem" style='position:relative'>
						<a href="<?php echo $this -> pages -> specials() ?>/delete/incentive/<?php echo $item['RelatedSpecialItemID'] ?>" onclick="return confirm('Are you sure you want to delete this incentive? Related inventory associated with this incentive will be removed')">
							<div class="deleteIncentive" data-toggle="tooltip" data-placement="right" title="Delete Incentive"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
						</a>
						<input type="hidden" name="relatedSpecialItemID[]" value="<?php echo $item['RelatedSpecialItemID'] ?>" />					
						<div class='errors'>
							<div class="valueError"></div>
							<div class="typeError"></div>
							<div class="expiredError"></div>
							<div class="descriptionError"></div>
							<div class="disclaimerError"></div>
							<div class="linkedInvetoryError"></div>
							<div class="duplicateCombinationError"></div>
						</div>					
						<div class='row'>
							<div class="col-md-8">
								<div style='margin-bottom:5px;'>
									First give this special a Number Amount then choose the type of incentive from the drop down [%Off, $Savings, or %Financing].	
								</div>
								<div class="row">
									<div class="col-md-2">
										<input id="specialTypeValue[]" class="specialTypeValue[]" type="text" placeholder='i.e. 30' name="specialTypeValue[]" value="<?php echo $item['Value'] ?>">		
									</div>
									<div class="col-md-10">
										<div class="input">
											<select name="specialType[]" id="specialType[]">
												<option value="">Please Select Percentage Off/Dollar off/Special Financing Type</option>
												<option value="Percentage Off" <?php if($item['RelatedItemSpecialType'] == "Percentage Off") : ?> selected <?php endif; ?>>Percentage Off</option>
												<option value="Dollar Off" <?php if($item['RelatedItemSpecialType'] == "Dollar Off") : ?> selected <?php endif; ?>>Dollar Off</option>
												<option value="Special Financing" <?php if($item['RelatedItemSpecialType'] == "Special Financing") : ?> selected <?php endif; ?>>Special Financing</option>
											</select>
										</div>
									</div>
								</div>					
							</div>
							<div class="col-md-4">
								<?php echo $this -> form -> Input("date", "Expired Date of Incentive", "specialEndDate[]", NULL, $item['ExpiredDate']) ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<?php echo $this -> form -> textarea("Additional/Secondary Offer Description", "specialDescription[]", NULL, $item['Description']) ?>
							</div>
							<div class="col-md-6">
								<?php echo $this -> form -> textarea("Disclaimer <strong>(Required)</strong>", "specialDisclaimer[]", NULL, $item['Disclaimer']) ?>
							</div>
						</div>					
						<div class="row">
							<div class="col-md-12">
								
							</div>
						</div>						
					</div>					
				<?php endforeach; ?>
			<?php else: ?>
				<div class="incentiveItem">
					<input type="hidden" name="relatedSpecialItemID[]" value="0" />		
					<div class='errors'>
						<div class="valueError"></div>
						<div class="typeError"></div>
						<div class="expiredError"></div>
						<div class="descriptionError"></div>
						<div class="disclaimerError"></div>
						<div class="linkedInvetoryError"></div>
						<div class="duplicateCombinationError"></div>
					</div>				
					<div class='row'>
						<div class="col-md-8">
							<div style='margin-bottom:5px;'>
								First give this special a Number Amount then choose the type of incentive from the drop down [%Off, $Savings, or %Financing].	
							</div>
							<div class="row">
								<div class="col-md-2">
									<input id="specialTypeValue[]" class="specialTypeValue[]" type="text" placeholder='i.e. 30' name="specialTypeValue[]">		
								</div>
								<div class="col-md-10">
									<div class="input">
										<select name="specialType[]" id="specialType[]">
											<option value="">Please Select Percentage Off/Dollar off/Special Financing Type</option>
											<option value="Percentage Off">Percentage Off</option>
											<option value="Dollar Off">Dollar Off</option>
											<option value="Special Financing">Special Financing</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<?php echo $this -> form -> Input("date", "Expired Date of Special", "specialEndDate[]") ?>
						</div>
					</div>					
					<div class="row">
						<div class="col-md-6">
							<?php echo $this -> form -> textarea("Additional/Secondary Offer Description", "specialDescription[]") ?>
						</div>
						<div class="col-md-6">
							<?php echo $this -> form -> textarea("Disclaimer", "specialDisclaimer[]") ?>
						</div>
					</div>			
				</div>
			<?php endif; ?>					
		</div>	
		<a href='javascript:void(0)' onclick="SpecialsController.AddItem()">
			<div class="AddItem">
				Extra Incentive
			</div>							
		</a>
		<div class="row">
			<div class="col-md-12" style='margin-top:10px;'>
				<?php echo $this -> form -> Submit("Save Items", "greenButton") ?>
			</div>
		</div>	
	</form>
	<div id="NewRelatedItem" class="incentiveItem" style='display:none;'>
		<a href="javascript:void(0);"  onclick="SpecialsController.RemoveNewIncentive(this)">
			<div class="deleteIncentive" data-toggle="tooltip" data-placement="right" title="Delete Incentive"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
		</a>
		<input type="hidden" name="relatedSpecialItemID[]" value="0" />	
		<div class='errors'>
			<div class="valueError"></div>
			<div class="typeError"></div>
			<div class="expiredError"></div>
			<div class="descriptionError"></div>
			<div class="disclaimerError"></div>
			<div class="linkedInvetoryError"></div>
			<div class="duplicateCombinationError"></div>
		</div>				
		<div class='row'>
			<div class="col-md-12">
				<div style='font-size:20px; margin-bottom:10px;'>New Incentive</div>
			</div>
		</div>
		<div class='row'>
			<div class="col-md-8">
				<div style='margin-bottom:5px;'>
					First give this special a Number Amount then choose the type of incentive from the drop down [%Off, $Savings, or %Financing].	
				</div>
				<div class="row">
					<div class="col-md-2">
						<input id="specialTypeValue[]" class="specialTypeValue[]" type="text" placeholder='i.e. 30' name="specialTypeValue[]">		
					</div>
					<div class="col-md-10">
						<div class="input">
							<select name="specialType[]" id="specialType[]">
								<option value="">Please Select Percentage Off/Dollar off/Special Financing Type</option>
								<option value="Percentage Off">Percentage Off</option>
								<option value="Dollar Off">Dollar Off</option>
								<option value="Special Financing">Special Financing</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<?php echo $this -> form -> Input("date", "Expired Date of Special", "specialEndDate[]") ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this -> form -> textarea("Additional/Secondary Offer Description", "specialDescription[]") ?>
			</div>
			<div class="col-md-6">
				<?php echo $this -> form -> textarea("Disclaimer", "specialDisclaimer[]") ?>
			</div>
		</div>						
	</div>
<?php endif; ?>	