<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> specials() ?>">Types of Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Apparel Specials
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Choose brand to associate the special
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style='margin-bottom:10px;'>
								<ul style='margin-left:20px;'>
									<li><strong>Is the brand you are looking for on this page?</strong><br />Select the brand and create a special<br /><br /></li>
									<li><strong>is the brand you are looking for not on this page?</strong><br /><a href='<?php echo PATH ?>brands/create/brand'>Click Here</a>, save the new brand and create a special in that new brand</li>
								</ul>
							</div>
						</div>						
						<?php if(count($this -> brands) > 0) :?>
							<?php foreach(array_chunk($this -> brands, 4, true) as $brands) : ?>		
								<div class="row">
									<?php foreach($brands as $brand) : ?>	
										<div class="col-md-3">
											<a href="<?php echo $this -> pages -> specials() ?>/brand/<?php echo $brand['partAccessoryBrandID'] ?>/Apparel">
												<div class="GrayBox" data-toggle="tooltip" data-placement="bottom" title="" style='padding: 40px 40px 0px 40px;'>
													<img src="<?php echo PHOTO_URL . "PartsBrand/" . $brand['partBrandImage'] ?>" width='100%' />
													<div style='font-size:16px; padding:20px 0px;'><?php echo $brand['brandName'] ?></div>
												</div>
											</a>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endforeach; ?>
						<?php else:?>
							<div class="row">
								<div class="col-md-12">
									There are no brands entered yet into the parts and accessories seciton.
								</div>
							</div>
						<?php endif;?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Not Brand Specific?
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style='margin-bottom:10px;'>
								This section is where you are creating Apparel specials that are not related to any specific brand, or it a special could be applied to multiple brands
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> specials() ?>/create/Apparel">
									<div class="greenButton" style="float:left">
										Create Apparel Special
									</div>
								</a>
							</div>
						</div>
						<?php if(count($this -> apparelSpecials) > 0):?>		
							<div class="row">
								<div class="col-xs-8" style="font-weight:bold; font-size:20px;">
									Special Name					
								</div>
								<div class="col-xs-2" style="text-align:center; font-weight:bold; font-size:20px;">
									Stores
								</div>
								<div class="col-xs-2" style="text-align:right; font-weight:bold; font-size:20px;">
									Expires
								</div>
								<div class="specialDivider" style="margin:0px 15px; padding: 3px 0px;"></div>
							</div>
							<?php foreach($this -> apparelSpecials as $special) : ?>
								<div class="row">
									<div class="col-xs-8 SpecialDetailText">
										<div style="float:left; margin-right:10px; margin-left:5px;">
											<a href="<?php echo $this -> pages -> specials() ?>/delete/Special/<?php echo $special['CreatedSpecialID'] ?>" onclick="return confirm('Are you sure you want to delete this Special? this action will remove all of the work that has been done, and this action cannot be undone')">
												<i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete"></i>
											</a>
										</div>
				
										
										<a href="<?php echo PATH ?>specials/edit/ApparelSpecial/<?php echo $special['CreatedSpecialID'] ?>">
											<?php echo $special['SpecialTitle'] ?>	
										</a>
									</div>
									<div class="col-xs-2">
										<?php $stores = explode(",", $special['RelatedStores'])?>
										<div class="row">
											<div class="col-xs-4 specialDivderSide">
												<?php if (in_array("Dillon Brothers MotorSports", $stores, TRUE)) :?>
													<div class="StoreIndicatoryCirlce MotorSport" data-toggle="tooltip" data-placement="bottom" title="Motor Sports"></div>
												<?php endif; ?>
											</div>
											<div class="col-xs-4">
												<?php if (in_array("Dillon Brothers Harley-Davidson", $stores, TRUE)) :?>
													<div class="StoreIndicatoryCirlce Harley" data-toggle="tooltip" data-placement="bottom" title="Harley-Davidson"></div>
												<?php endif; ?>
											</div>
											<div class="col-xs-4 specialDivderSide">
												<?php if (in_array("Dillon Brothers Indian", $stores, TRUE)) :?>
													<div class="StoreIndicatoryCirlce Indian" data-toggle="tooltip" data-placement="bottom" title="Indian"></div>
												<?php endif; ?>
											</div>
										</div>
									</div>
									<div class="col-xs-2 SpecialDetailText" style="text-align:right">
										<?php 
										if($special['SpecialEndDate'] != NULL) {
											echo $this -> recordedTime -> formatDate($special['SpecialEndDate']);	
										}
										
										 ?>
									</div>
								
								
								<div class="specialDivider" style="margin:0px 15px;"></div>
							</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class="col-md-12">
									There are no specials entered
								</div>
							</div>
						<?php endif; ?>	
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
</div>


