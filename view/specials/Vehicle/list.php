<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> specials() ?>">Types of Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> specials() ?>/category/OEM">OEM Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> BrandSpecifics -> _brandName ?> Specials
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Brand Information: <?php echo $this -> BrandSpecifics -> _brandName ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style="margin-bottom:20px;">
								<div><strong>Dealer Offer link</strong></div>
								<a href="<?php echo $this -> BrandSpecifics -> offerLinks?>" target="_blank"><?php echo $this -> BrandSpecifics -> offerLinks?></a>
							</div>
						</div>
						<!--<form method="post" action="<?php echo $this -> pages -> specials() ?>/save/Brand/<?php echo $this -> BrandSpecifics -> brandID; ?>" id="BrandDetailsForm">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Brand Name</div>
											<input type="text" name="OEMbrandName" onchange="Globals.removeValidationMessage(1)" value='<?php echo $this -> BrandSpecifics -> _brandName ?>' />
											
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> textarea("Brand Description", "brandDescription", NULL, $this -> BrandSpecifics -> brandDescription) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit("Save", "greenButton") ?>
								</div>
							</div>	
						</form>-->
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Specials List
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> specials() ?>/create/OEMSpecial/<?php echo $this -> BrandSpecifics -> brandID ?>">
									<div class="greenButton" style="float:left">
										New <?php echo $this -> BrandSpecifics -> _brandName ?> OEM Special
									</div>
								</a>
							</div>
						</div>	
						<?php if(count($this -> SpecialsList) > 0):?>		
							<div class="row">
								<div class="col-xs-6" style="font-weight:bold; font-size:20px;">
									Special Name
								</div>
								<div class="col-xs-6" style="text-align:right; font-weight:bold; font-size:20px;">
									Expires
								</div>
								<div class="specialDivider" style="margin:0px 15px; padding: 3px 0px;"></div>
							</div>
							<?php foreach($this -> SpecialsList as $special) : ?>
								<div class="row">
									<div class="col-xs-6 SpecialDetailText">
										<div style="float:left; margin-right:10px; margin-left:5px;">
											<a href="<?php echo $this -> pages -> specials() ?>/delete/Special/<?php echo $special['SpecialID'] ?>" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="return confirm('Are you sure you want to delete this Special? this action will remove all of the work that has been done, and this action cannot be undone')">
												<i class="fa fa-trash" aria-hidden="true"></i>
											</a>
										</div>
										<div style="float:left">
											<?php if($special["Store"] == "2") : ?>
												<div class="StoreIndicatoryCirlce MotorSport" data-toggle="tooltip" data-placement="bottom" title="Dillon Motor Sports" style="margin-top: 2px; margin-bottom: 0px; margin-right: 5px;"></div>
											<?php endif; ?>
											<?php if($special["Store"] == "3") : ?>
												<div class="StoreIndicatoryCirlce Harley" data-toggle="tooltip" data-placement="bottom" title="Harley-Davidson" style="margin-top: 2px; margin-bottom: 0px; margin-right: 5px;"></div>
											<?php endif; ?>
											<?php if($special["Store"] == "4") : ?>
												<div class="StoreIndicatoryCirlce Indian" data-toggle="tooltip" data-placement="bottom" title="Indian" style="margin-top: 2px; margin-bottom: 0px; margin-right: 5px;"></div>
											<?php endif; ?>
										</div>
										<a href="<?php echo PATH ?>specials/edit/OEMSpecial/<?php echo $special['SpecialID'] ?>">
											 <?php echo $special['SpecialTitle'] ?>	
										</a>
									</div>
									<div class="col-xs-6 SpecialDetailText" style="text-align:right">
										<?php 
										if($special['SpecialEndDate'] != NULL) {
											echo $this -> recordedTime -> formatDate($special['SpecialEndDate']);	
										}
										
										 ?>
									</div>
									<div class="specialDivider" style="margin:0px 15px;"></div>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class="col-md-12">
									There are no specials entered
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="container">
	
	
	

	
	
	
	
	<div id="BrandVehicleSpecials" style="display:none;">
		
		>		
	</div>


</div>
