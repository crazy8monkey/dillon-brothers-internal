<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<?php echo $this -> brandBreadCrumbs; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ContentPage">
	<div class="container-fluid">
		<div class="WhiteSectionDiv">
			<div class="content">
				<form method="post" action="<?php echo $this -> pages -> specials() ?>/save/Special" id="SpecialForm" enctype="multipart/form-data">
					<div class="row">
						<input type="hidden" name="specialType" value="1" />
						<input type="hidden" name="OEMBrandName" value="<?php echo $this -> brandDetails -> brandID; ?>" />
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									New <?php echo $this -> brandDetails -> _brandName; ?> OEM Special
								</div>
							</div>	
							<div class="row">
								<div class="col-md-12">
									<div id="inputID2">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Special Title</div>
											<div class='descriptiveInfo'>Please give this special a title [Recommended minimum of 160 charaters]</div>
											<input id="specialTitle" class="specialTitle" type="text" name="specialTitle" onchange="Globals.removeValidationMessage(2)">
										</div>
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-md-6">
									<div id="inputID3">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Special Description</div>
											<div class='descriptiveInfo'>Provide a conversational description for this special. [Recommended minimum of 300 characters]<br />Provide more detailed information below after you save the special.</div>
											<textarea id="specialDescription" name="specialDescription" class='textareaMinHeight' onchange="Globals.removeValidationMessage(3)"></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input">
										<div class="inputLabel">Additional Information</div>
										<div class='descriptiveInfo'>IF there is a Disclaimer or Conditional text for this special please include it here. <br />(ex: Sale runs through [DATE/MONTH] and is good for in-stock items only.] </div>
										<textarea id="specialDescriptionFooter" name="specialDescriptionFooter" class='textareaMinHeight'></textarea>
									</div>
								</div>
							</div>
							
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
									</div>
									<div class="input">
										<div class="inputLabel">Special Photo (Big)</div>
										<div class="descriptiveInfo">Recommended 1024px (width) x 500 (height) / 100k file size</div>
										<input type="file" name="specialPhotoBig">
									</div>
									<div class="emptyPhotoPlacement">
										<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
										<div class="required">Photo Is Required to show on website (Big Image)</div>
									</div>
									<div class="input">
										<div class="inputLabel">Special Photo (Small)</div>
										<div class="descriptiveInfo">Recommended 200px (width) x 200 (height) / 100k file size</div>
										<input type="file" name="specialPhotoSmall">
									</div>
									<div class="emptyPhotoPlacement">
										<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
										<div class="required">Photo Is Required to show on website (Small Image)</div>
									</div>
									
									
									<input type="hidden" name="linkedToVehicleInventory" value="1" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php echo $this -> form -> Submit("Go To Step 2", "greenButton") ?>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div>
</div>




