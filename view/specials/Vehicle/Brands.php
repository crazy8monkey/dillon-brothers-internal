<div class="container">
	<div class="row">
		<div class="col-md-12 breadCrumbs">
			<a href="<?php echo $this -> pages -> specials() ?>">Types of Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>Vehicle Specials
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 sectionHeader">
			Select Brand
		</div>
	</div>
	<?php foreach($this -> brands as $brand)?>
	<?php ?>
	
	<?php foreach(array_chunk($this -> brands, 4, true) as $brands) : ?>		
		<div class="row">
			<?php foreach($brands as $brand) : ?>	
				<div class="col-md-3">
					<a href="<?php echo $this -> pages -> specials() ?>/OEM/<?php echo $brand['BrandID'] ?>">
						<div class="GrayBox">
							<img src="<?php echo PHOTO_URL . 'brands/' .  $brand['BrandImage'] ?>" />
						</div>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endforeach; ?>
</div>
