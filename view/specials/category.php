<?php 

	$finalCategoryView = array();
	
	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SpecialOEMCategoryVisible') == 1) {
		array_push($finalCategoryView, array("Text" => "OEM Specials",
											 "LinkText" => $this -> pages -> specials() . '/category/OEM',
											 "Image" => PATH . 'public/images/VehicleSpecialCategory.png'));
	}
	
	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SpecialMotorclothesCategoryVisible') == 1) {
		array_push($finalCategoryView, array("Text" => "Motorclothes Specials",
											 "LinkText" => $this -> pages -> specials() . '/category/Apparel',
											 "Image" => PATH . 'public/images/ClothingSpecialCategory.png'));
	}
	
	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SpecialPartsCategoryVisible') == 1) {
		array_push($finalCategoryView, array("Text" => "Parts Specials",
											 "LinkText" => $this -> pages -> specials() . '/category/Parts',
											 "Image" => PATH . 'public/images/PartsSpecialCategory.png'));
	}
	
	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SpecialServiceCategoryVisible') == 1) {
		array_push($finalCategoryView, array("Text" => "Service Specials",
											 "LinkText" => $this -> pages -> specials() . '/category/Service',
											 "Image" => PATH . 'public/images/ServiceSpecialCategory.png'));
	}
	
	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SpecialStoreCategoryVisible') == 1) {
		array_push($finalCategoryView, array("Text" => "Store Specials",
											 "LinkText" => $this -> pages -> specials() . '/category/Store',
											 "Image" => PATH . 'public/images/StoreSpecialCategory.png'));
	}
	
?>

<div class="ContentPage">
	<div class="container">
		<div class="row">
			<div class="col-md-12 sectionHeader">
				Please Select a type of special you want to create
			</div>
		</div>
		<?php foreach(array_chunk($finalCategoryView, 3, true) as $categoriesThree): ?>
			<div class="row">
				<?php foreach($categoriesThree as $categorySingle): ?>
					<div class="col-md-4">
						<a href="<?php echo $categorySingle['LinkText'] ?>">
							<div class="GrayBox">
								<img src="<?php echo $categorySingle['Image'] ?>" />
								<div class="ContentTypeText">
									<?php echo $categorySingle['Text'] ?>
								</div>
							</div>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endforeach; ?>
	</div>
</div>
