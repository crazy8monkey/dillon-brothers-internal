<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> specials() ?>">Types of Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> specials() ?>/category/Service">Service Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New Service Special
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="WhiteSectionDiv">
			<div class="content">
				<form method="post" action="<?php echo $this -> pages -> specials() ?>/save/Special" id="SpecialForm" enctype="multipart/form-data">
					<input type="hidden" name="specialType" value="4" />
					<div class="row">
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-12 sectionHeader">
									New Service Special
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div style='border-top: 1px solid #cecece; border-bottom: 1px solid #cecece; padding-top:10px; margin-bottom:10px;'>
										<div id="inputID4"><div class="errorMessage"></div></div>
										<div class="input">
											<div class="inputLabel">Which stores this special being applied to?</div>
											<?php foreach($this -> stores as $storeSingle): ?>
												
												<?php if(in_array($storeSingle['storeID'], array_column($_SESSION["user"]->GetAssociatedStores(), 'associatedStore'))): ?>
													<div style="margin-bottom:3px;"><input type="checkbox" name="store[]" value="<?php echo $storeSingle['storeID'] ?>" style="float:left; margin-right: 2px;" onchange="Globals.removeValidationMessage(4)"><?php echo $storeSingle['StoreName'] ?></div>
												<?php endif; ?>
												
												
											<?php endforeach; ?>	
										</div>
									</div>
									
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID2">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> Input("text", "Special Title", "specialTitle", 2) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div id="inputID3">
										<div class="errorMessage"></div>
										<?php echo $this -> form -> textarea("Special Description", "specialDescription", 3) ?>
									</div>
								</div>
								<div class="col-md-6">
									<?php echo $this -> form -> textarea("Special Description Footer", "specialDescriptionFooter") ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit("Create", "greenButton") ?>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
									</div>
									<div class="input">
										<div class="inputLabel">Special Photo (Big)</div>
										<div class="descriptiveInfo">Recommended 1024px (width) x 500 (height) / 100k file size</div>
										<input type="file" name="specialPhotoBig">
									</div>
									<div class="emptyPhotoPlacement">
										<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
										<div class="required">Photo Is Required to show on website (Big Image)</div>
									</div>
									<div class="input">
										<div class="inputLabel">Special Photo (Small)</div>
										<div class="descriptiveInfo">Recommended 200px (width) x 200 (height) / 100k file size</div>
										<input type="file" name="specialPhotoSmall">
									</div>
									<div class="emptyPhotoPlacement">
										<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
										<div class="required">Photo Is Required to show on website (Small Image)</div>
									</div>
								</div>
							</div>
							<input type="hidden" name="linkedToVehicleInventory" value="99" />
						</div>
					</div>
				</form>	
			</div>
		</div>
	</div>
</div>



