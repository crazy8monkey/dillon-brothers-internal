<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> specials() ?>">Types of Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> specials() ?>/category/Parts">Parts Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> BrandSpecifics -> PartsAccessoryBrandName ?> Specials
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader">
								Specials List
							</div>
						</div>
						<div class="row buttonContainer">
							<div class="col-md-12">
								<a href="<?php echo $this -> pages -> specials() ?>/create/BrandPartsSpecial/<?php echo $this -> BrandSpecifics -> GetID()?>">
									<div class="greenButton" style="float:left">
										New <?php echo $this -> BrandSpecifics -> PartsAccessoryBrandName ?> Parts Special
									</div>
								</a>
							</div>
						</div>
						<?php if(count($this -> SpecialsList) > 0):?>		
							<div class="row">
								<div class="col-xs-8" style="font-weight:bold; font-size:20px;">
									Special Name					
								</div>
								<div class="col-xs-2" style="text-align:center; font-weight:bold; font-size:20px;">
									Stores
								</div>
								<div class="col-xs-2" style="text-align:right; font-weight:bold; font-size:20px;">
									Expires
								</div>
								<div class="specialDivider" style="margin:0px 15px; padding: 3px 0px;"></div>
							</div>
							<?php foreach($this -> SpecialsList as $special) : ?>
								<div class="row">
									<div class="col-xs-8 SpecialDetailText">
										<div style="float:left; margin-right:10px; margin-left:5px;">
											<a href="<?php echo $this -> pages -> specials() ?>/delete/Special/<?php echo $special['CreatedSpecialID'] ?>" onclick="return confirm('Are you sure you want to delete this Special? this action will remove all of the work that has been done, and this action cannot be undone')">
												<i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete"></i>
											</a>
										</div>
				
										
										<a href="<?php echo PATH ?>specials/edit/PartsSpecial/<?php echo $special['CreatedSpecialID'] ?>">
											<?php echo $special['SpecialTitle'] ?>	
										</a>
									</div>
									<div class="col-xs-2">
										<?php $stores = explode(",", $special['RelatedStores'])?>
										<div class="row">
											<div class="col-xs-4 specialDivderSide">
												<?php if (in_array("Dillon Brothers MotorSports", $stores, TRUE)) :?>
													<div class="StoreIndicatoryCirlce MotorSport" data-toggle="tooltip" data-placement="bottom" title="Motor Sports"></div>
												<?php endif; ?>
											</div>
											<div class="col-xs-4">
												<?php if (in_array("Dillon Brothers Harley-Davidson", $stores, TRUE)) :?>
													<div class="StoreIndicatoryCirlce Harley" data-toggle="tooltip" data-placement="bottom" title="Harley-Davidson"></div>
												<?php endif; ?>
											</div>
											<div class="col-xs-4 specialDivderSide">
												<?php if (in_array("Dillon Brothers Indian", $stores, TRUE)) :?>
													<div class="StoreIndicatoryCirlce Indian" data-toggle="tooltip" data-placement="bottom" title="Indian"></div>
												<?php endif; ?>
											</div>
										</div>
									</div>
									<div class="col-xs-2 SpecialDetailText" style="text-align:right">
										<?php 
										if($special['SpecialEndDate'] != NULL) {
											echo $this -> recordedTime -> formatDate($special['SpecialEndDate']);	
										}
										
										 ?>
									</div>
								
								
								<div class="specialDivider" style="margin:0px 15px;"></div>
							</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="row">
								<div class="col-md-12">
									There are no specials entered
								</div>
							</div>
						<?php endif; ?>		
						
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
</div>
