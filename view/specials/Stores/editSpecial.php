<div class="topSectionPage">
	<div class="ContentPage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 breadCrumbs">
					<a href="<?php echo $this -> pages -> specials() ?>">Types of Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo $this -> pages -> specials() ?>/category/Store">Store Specials</a><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>New Store Special
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ContentPage">
	<div class="container-fluid">
		<div class="WhiteSectionDiv">
			<div class="content">
				<form method="post" action="<?php echo $this -> pages -> specials() ?>/save/Special/<?php echo $this -> SpecialSingle -> GetID() ?>" id="SpecialForm" enctype="multipart/form-data">			
					<div class="row">
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-12 sectionHeader" style='margin-bottom: 10px;'>
									Edit Store Special
								</div>
							</div>	
							<div class="row">
								<div class="col-md-12" style='margin-bottom:10px;'>
									<?php 
										$editedTime = explode('T', $this ->  logs[count($this ->  logs) - 1]['edittedTimeAndDate']);
									?>
									Last Edited by <?php echo $this ->  logs[count($this ->  logs) - 1]['firstName'] . ' ' . $this ->  logs[count($this ->  logs) - 1]['lastName'] ?> - <?php echo $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1]) ?> <a href="javascript:void(0);" onclick="SpecialsController.getEditHistory(<?php echo $this -> SpecialSingle -> GetID() ?>)">View History</a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-info">
										<div><strong>Congratulations!</strong></div>
										You have created a special, but it is not shown on the website yet.<br /><br />
										Start adding incentives in the <strong>"Related Items"</strong> section to describe what items are associated/applied to this particular special that you created!		
									</div>
								</div>
							</div>
							<?php if($this -> SpecialSingle -> ShowActiveOption() == true): ?>
								<div class="row">
									<div class="col-md-12" style='margin-bottom:10px;'>
										<?php echo $this -> form -> checkbox('Special Is Active', 'specialsActive', $this -> SpecialSingle -> SpecialIsActive, 1, NULL, true) ?>
									</div>
								</div>
							<?php endif; ?>
							<div class="row">
								<div class="col-md-12">
									<div style='border-top: 1px solid #cecece; border-bottom: 1px solid #cecece; padding-top:10px; margin-bottom:10px;'>
										<div class='descriptiveInfo'>Changing Store Options will lse previous linked inventory to related items</div>
										<div id="inputID4"><div class="errorMessage"></div></div>
										<div class="input">
											<div class="inputLabel">Which stores this special being applied to?</div>
											<?php foreach($this -> stores as $storeSingle): ?>
												
												<?php if(in_array($storeSingle['storeID'], array_column($_SESSION["user"]->GetAssociatedStores(), 'associatedStore'))): ?>
													<div style="margin-bottom:3px;">
														
														<input type="checkbox" <?php if(in_array($storeSingle['storeID'], array_column($this -> SpecialSingle -> GetStores(), 'RelatedStoreID'))): ?>checked<?php endif; ?> name="store[]" value="<?php echo $storeSingle['storeID'] ?>" style="float:left; margin-right: 2px;" onchange="Globals.removeValidationMessage(4)"><?php echo $storeSingle['StoreName'] ?>
													</div>
												<?php endif; ?>
												
												
											<?php endforeach; ?>	
										</div>
									</div>
									
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID2">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Special Title</div>
											<div class='descriptiveInfo'>Please give this special a title [Recommended minimum of 160 charaters]</div>
											<input id="specialTitle" class="specialTitle" type="text" name="specialTitle" value="<?php echo $this -> SpecialSingle -> SpecialTitle ?>" onchange="Globals.removeValidationMessage(2)">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div id="inputID3">
										<div class="errorMessage"></div>
										<div class="input">
											<div class="inputLabel">Special Description</div>
											<div class='descriptiveInfo'>Provide a conversational description for this special. [Recommended minimum of 300 characters]<br />Provide more detailed information below after you save the special.</div>
											<textarea id="specialDescription" name="specialDescription" onchange="Globals.removeValidationMessage(3)" class='textareaMinHeight'><?php echo $this -> SpecialSingle -> SpecialDescription?></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input">
										<div class="inputLabel">Additional Information</div>
										<div class='descriptiveInfo'>IF there is a Disclaimer or Conditional text for this special please include it here. <br />(ex: Sale runs through [DATE/MONTH] and is good for in-stock items only.] </div>
										<textarea id="specialDescriptionFooter" name="specialDescriptionFooter" class='textareaMinHeight'><?php echo $this -> SpecialSingle -> SpecialDescriptionFooter ?></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php echo $this -> form -> Submit("Save", "greenButton") ?>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
									</div>
									<div class="input">
										<div class="inputLabel">Special Photo (Big)</div>
										<div class="descriptiveInfo">Recommended 1024px (width) x 500 (height) / 100k file size</div>
										<input type="file" name="specialPhotoBig">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style='margin-bottom:10px;'>
									<?php if(!empty($this -> SpecialSingle -> _currentImageBig)): ?>							
										<div><strong>Current Special Image (Big)</strong></div>
										<img src="<?php echo PHOTO_URL . 'specials/' . $this -> SpecialSingle -> FolderName . '/' . $this -> SpecialSingle -> _currentImageBig ?>" width='100%' />
									<?php else: ?>
										<div class="emptyPhotoPlacement" style='margin-bottom:0px;'>
											<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
											<div class="required">Photo Is Required to show on website (Big Image)</div>
										</div>
									<?php endif; ?>		
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">
										<div class="errorMessage"></div>
									</div>
									<div class="input">
										<div class="inputLabel">Special Photo (Small)</div>
										<div class="descriptiveInfo">Recommended 200px (width) x 200 (height) / 100k file size</div>
										<input type="file" name="specialPhotoSmall">
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-md-12" style='margin-bottom:10px;'>
									<?php if(!empty($this -> SpecialSingle -> _currentImageSmall)): ?>							
										<div><strong>Current Special Image (Small)</strong></div>
										<img src="<?php echo PHOTO_URL . 'specials/' . $this -> SpecialSingle -> FolderName . '/' . $this -> SpecialSingle -> _currentImageSmall ?>" />
									<?php else: ?>
										<div class="emptyPhotoPlacement" style='margin-bottom:0px;'>
											<img src='<?php echo PATH ?>public/images/PhotoAlbumIcon.png' />
											<div class="required">Photo Is Required to show on website (Small Image)</div>
										</div>
									<?php endif; ?>		
								</div>
							</div>	
							<div class="row">
								<div class="col-md-12">
									<div class="input">
										<div class="inputLabel">Is there store inventory associated with this special?</div>
										<div class='descriptiveInfo'>Changing this setting will remove previous entered info.</div>
										<select name="linkedToVehicleInventory">
											<option value="0">Please Select </option>
											<option value="1" <?php if($this -> SpecialSingle ->RelatedInventoryType == 1): ?>selected<?php endif; ?>>Inventory: Vehicles</option>
											<option value="99" <?php if($this -> SpecialSingle ->RelatedInventoryType == 99): ?>selected<?php endif; ?>>No Inventory</option>
										</select>
										
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</form>	
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="WhiteSectionDiv">
					<div class="content">
						<div class="row">
							<div class="col-md-12 sectionHeader" style='margin-bottom:10px;'>
								Related Items
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style='margin-bottom:10px;'>
								<div class="alert alert-info" style='margin-bottom: 0px;'>
									The next step is to add incentives and/or associated inventory (if applicable).
									
									<ul style='margin-left:20px; margin-top:15px;'>
										<li>Based on the <strong>Associated Inventory selection</strong> above please use this form to choose the inventory item(s) this special is applies to.</li>
										<li>
											You have the option to select <strong>Percentage Off/Dollar Off/Special Financing</strong> for each related item		
										</li>
									</ul>									 
								</div>
								
							</div>
						</div>
						<?php require 'view/specials/relatedItemContent.php'?>
						
						
					</div>
				</div>
			</div>	
		</div>
	</div>
	
	
</div>

<div id="SpecialEditHistory" style="display:none;">
	<?php echo $this -> PopupReadOnly('Special Edit History', 'SpecialsController.closeEditHistory()')?>
</div>

<div id="LinkedInventoryPoup" style="display: none;">
	<a href="javascript:void(0);" onclick="SpecialsController.HideLinkedInventory()">
		<div class="Overlay"></div>
	</a>
	<div class="Popup">
		<div class="formHeader">Linked Inventory To Incentive		
			<a href="javascript:void(0);" onclick="SpecialsController.HideLinkedInventory()">
				<div class="close" style="margin-top: 4px;">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>						
			</a>	
		</div>
		<div class="FormContent" style="margin-right: 15px; overflow-y:auto; min-height:200px;"></div>
	</div>
</div>



