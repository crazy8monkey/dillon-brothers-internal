<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
<head>
	<title>Add Vehicles To Special <?php echo $this -> title ?></title>
		
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	
		
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/SpecialsController.css" /> 
	
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		
	<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo PATH ?>public/js/SpecialsController.js"></script>	
	<script type="text/javascript">
		$(document).ready(function() {
			SpecialsController.FilterInventory();
			$('[data-toggle="tooltip"]').tooltip({html: true});
			
		});
	</script>
	<style type="text/css">
		body {
			overflow-x:hidden;
		}
	</style>
	

</head>
<body onunload="RefreshParent()">

	
	<div class="container-fluid">
		<div style="margin:0px 10px; padding-bottom: 140px;">
			
				<div class="row">
				
					<div class="col-md-12">	
						<form method="post" action="<?php echo PATH ?>specials/save/speciallinkinventory/<?php echo $this -> specialID ?>" id="LinkInventoryForm">
							
							<div id="ContentList">
								<?php if(count($this -> items) > 0) : ?>
									<div class="row">
										<div class="col-md-12">	
											<div style='font-size:20px; margin-bottom: 20px; margin-top: 10px;'>Current Incentives</div>
										</div>
									</div>
									<div style='position: fixed; bottom: 0px; box-shadow: 0px -3px 7px #ccc; width: 100%; left: 0px; right: 0px; z-index: 99; background: white;'>
										<div style='padding:20px 25px'>
											<input type="submit" value="Save Selected Inventory" class="greenButton" style='font-size: 16px; font-weight: normal;' />	
										</div>
									</div>
									
									<?php foreach(array_chunk($this -> items, 2, true) as $itemDuece) : ?>
										<div class="row">
											<?php foreach($itemDuece as $itemSingle): ?>
												<div class="col-md-6">
													<div class="incentiveItem">
														<div class="deleteIncentive">
															<a href="<?php echo $this -> pages -> specials() ?>/delete/incentive/<?php echo $itemSingle['RelatedSpecialItemID'] ?>" onclick="return confirm('Are you sure you want to delete this incentive? Related inventory associated with this incentive will be removed'); SpecialsController.RefreshParent()">
																<i class="fa fa-times-circle" aria-hidden="true"></i>
															</a>
														</div>
														<input type="hidden" name="relatedSpecialItemID[]" class="relatedSpecialItemID" value="<?php echo $itemSingle['RelatedSpecialItemID'] ?>" />
														<div class='errors'>
															<div class="valueError"></div>
															<div class="typeError"></div>
															<div class="expiredError"></div>
															<div class="descriptionError"></div>
															<div class="disclaimerError"></div>
															<div class="linkedInvetoryError"></div>
															<div class="duplicateCombinationError"></div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class='row'>
																	<div class="col-md-12">
																		<div style='margin-bottom:5px;'>
																			First give this special a Number Amount then choose the type of incentive from the drop down [%Off, $Savings, or %Financing].	
																		</div>
																	</div>
																</div>
																
																<div class='row'>
																	<div class="col-md-3">
																		<input class="specialTypeValue" type="text" placeholder='i.e. 30' value="<?php echo $itemSingle['Value'] ?>" name="specialTypeValue[]">		
																	</div>
																	<div class="col-md-9">
																		<div class="input">
																			<select name="specialType[]" class="specialType">
																				<option value="">Please Select Percentage Off/Dollar off/Special Financing Type</option>
																				<option value="Percentage Off"<?php if($itemSingle['RelatedItemSpecialType'] == "Percentage Off"): ?> selected<?php endif; ?>>Percentage Off</option>
																				<option value="Dollar Off"<?php if($itemSingle['RelatedItemSpecialType'] == "Dollar Off"): ?> selected<?php endif; ?>>Dollar Off</option>
																				<option value="Special Financing"<?php if($itemSingle['RelatedItemSpecialType'] == "Special Financing"): ?> selected<?php endif; ?>>Special Financing</option>
																			</select>
																		</div>
																	</div>
																</div>
																
																<div class='row'>
																	<div class="col-md-12">
																		<?php echo $this -> form -> Input("date", "Expired Date of Special", "specialEndDate[]", NULL, $itemSingle['ExpiredDate']) ?>
																	</div>
																</div>
																
																<div class="row">
																	<div class="col-md-6">
																		<?php echo $this -> form -> textarea("Additional/Secondary Offer Description", "specialDescription[]", NULL, $itemSingle['Description']) ?>
																	</div>
																	<div class="col-md-6">
																		<?php echo $this -> form -> textarea("Disclaimer", "specialDisclaimer[]", NULL, $itemSingle['Disclaimer']) ?>
																	</div>
																</div>
																
																
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<?php 
																$inventoryIDS = NULL;
																
																$incentiveID = $itemSingle['RelatedIncentiveID'];
														
																$filteredInventory = array_filter($this -> linkedInventory, function ($var) use($incentiveID) {
																		 return $var['RelatedIncentiveID'] == $incentiveID; 
																	}
																);
																
																foreach($filteredInventory as $inventorySingle) {
																	$inventoryIDS .= $inventorySingle['inventoryID'] .',';	
																}
																
																 ?>
																
																<input type="hidden" name="linkedInventoryIDS[]" class="linkedInventoryIDS" value="<?php echo rtrim($inventoryIDS, ','); ?>" />
																
																<div class="row">
																	<div class="col-md-12">
																		<div class="DropInventorySection" style='margin-bottom: 10px;'>
																			<div class="inventoryList">
																				<a href="javascript:void(0)" onclick="SpecialsController.SelectInventory(this, <?php echo $itemSingle['RelatedSpecialItemID'] ?>)"><div class='SelectInventoryLink'>Select Inventory To Incentive</div></a>
																				<?php foreach($filteredInventory as $inventorySingle): ?>
																					<?php 
																					if($inventorySingle['FriendlyModelName'] != NULL) {
																						$bikeName = $inventorySingle['Year'] . ' ' . $inventorySingle['Manufacturer'] . ' ' . $inventorySingle['FriendlyModelName'];
																					} else {
																						$bikeName = $inventorySingle['Year'] . ' ' . $inventorySingle['Manufacturer'] . ' ' . $inventorySingle['ModelName'];
																					}
																					
																					?>
																					<div class="linkedInventoryItem" id="<?php echo $inventorySingle['inventoryID'] ?>">
																						<a href="javascript:void(0);" onclick="SpecialsController.RemoveLinkedInventory(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>										
																						<input type="hidden" name="inventoryID[]" class="inventoryID" value="<?php echo $inventorySingle['inventoryID'] ?>">
																						<?php echo $bikeName . ' - <strong>'. $inventorySingle['Stock'] . '</strong>'; ?>
																					</div>
																				<?php endforeach; ?>
																				
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>					
													</div>
												</div>
											<?php endforeach; ?>
										</div>
									<?php endforeach; ?>
									<div style='border-top: 1px solid #d6d6d6;'></div>
								<?php else: ?>
									<div class="row">
										<div class="col-md-12">
											<div style='font-weight: bold; text-align: center; padding: 10px; border: 1px solid #cecece; margin-top: 10px;'>
												Currently There are no incentives entered to this special, please enter new incentives	
											</div>
											
										</div>
									</div>
								<?php endif; ?>
							</div>
						</form>
						
						<div class="row">
							<div class="col-md-6">
								<div style='border: 1px solid #d6d6d6; padding: 10px; border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; margin-top: 15px;' id="NewIncentiveItem">
									<div class="row">
										<div class="col-md-12">
											<div style='font-size:20px; margin-bottom:20px;'>Add New Incentive</div>	
											<div class='errors'>
												<div class="valueError"></div>
												<div class="typeError"></div>
												<div class="expiredError"></div>
												<div class="descriptionError"></div>
												<div class="disclaimerError"></div>
												<div class="linkedInvetoryError"></div>
												<div class="duplicateCombinationError"></div>
											</div>											
										</div>				
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-12">
													<div style="margin-bottom:5px;">
														First give this special a Number Amount then choose the type of incentive from the drop down [%Off, $Savings, or %Financing].	
													</div>
												</div>
											</div>			
											<form method="post" id="NewIncentive" action="<?php echo PATH ?>specials/save/newspeciallinkinventory/<?php echo $this -> specialID ?>">		
												<div class="row">
													<div class="col-md-3">
														<input type="text" placeholder="i.e. 30"  name="newSpecialTypeValue">		
													</div>
													<div class="col-md-9">
														<div class="input">
															<select name="newSpecialType">
																<option value="">Please Select Percentage Off/Dollar off/Special Financing Type</option>
																<option value="Percentage Off" >Percentage Off</option>
																<option value="Dollar Off">Dollar Off</option>
																<option value="Special Financing">Special Financing</option>
															</select>
														</div>
													</div>
												</div>					
												<div class="row">
													<div class="col-md-12">
														<div class="input">
															<div class="inputLabel">Expired Date of Special</div>
															<input type="date" name="newSpecialEndDate">
														</div>																	
													</div>
												</div>					
												<div class="row">
													<div class="col-md-6">
														<div class="input">
															<div class="inputLabel">Additional/Secondary Offer Description</div>
															<textarea name="newSpecialDescription"></textarea>
														</div>																	
													</div>
													<div class="col-md-6">
														<div class="input">
															<div class="inputLabel">Disclaimer</div>
															<textarea name="newSpecialDisclaimer"></textarea>
														</div>																	
													</div>
												</div>	
												<div class="row">
													<div class="col-md-12">
														<input type='submit' class="AddItem" value="Save New Incentive" style='background:white;'/>
													</div>
												</div>
											</form>										
										</div>
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="incentiveItem" id="NewRelatedItem" style='display:none;'>
							<input type="hidden" name="relatedSpecialItemID[]" value="0" />
							<div class='errors'>
								<div class="valueError"></div>
								<div class="typeError"></div>
								<div class="expiredError"></div>
								<div class="descriptionError"></div>
								<div class="disclaimerError"></div>
								<div class="linkedInvetoryError"></div>
								<div class="duplicateCombinationError"></div>
							</div>					
							<div class='row'>
								<div class="col-md-12">
									<div style='margin-bottom:5px;'>
										First give this special a Number Amount then choose the type of incentive from the drop down [%Off, $Savings, or %Financing].	
									</div>
								</div>
							</div>
							
							<div class='row'>
								<div class="col-md-3">
									<input id="specialTypeValue[]" class="specialTypeValue[]" type="text" placeholder='i.e. 30' name="specialTypeValue[]">		
								</div>
								<div class="col-md-9">
									<div class="input">
										<select name="specialType[]" id="specialType[]">
											<option value="">Please Select Percentage Off/Dollar off/Special Financing Type</option>
											<option value="Percentage Off">Percentage Off</option>
											<option value="Dollar Off">Dollar Off</option>
											<option value="Special Financing">Special Financing</option>
										</select>
									</div>
								</div>
							</div>
							
							<div class='row'>
								<div class="col-md-12">
									<?php echo $this -> form -> Input("date", "Expired Date of Special", "specialEndDate[]") ?>
								</div>
							</div>
																		
							<div class="row">
								<div class="col-md-6">
									<?php echo $this -> form -> textarea("Additional/Secondary Offer Description", "specialDescription[]") ?>
								</div>
								<div class="col-md-6">
									<?php echo $this -> form -> textarea("Disclaimer", "specialDisclaimer[]") ?>
								</div>
							</div>
							
							<input type="hidden" name="linkedInventoryIDS[]" class="linkedInventoryIDS" value="" />
							
							<div class="row">
								<div class="col-md-12">
									<div class="DropInventorySection">
										Drag and Drop related inventory in this area
										<div class="inventoryList">
													
										</div>
									</div>
								</div>
							</div>
												
						</div>
							
						
					</div>
					
					
				</div>
				
			
		
	</div>
	
<div id="SelectInventoryWindow">
	

	
<div id="SpecialFilterInventory">
	<div class="Overlay" style='display:none;'></div>
	<div class="FilterSectionContainer">
		<a href='javascript:void(0)' onclick="SpecialsController.ToggleFilters()">
			<div class="buttonFlap">
				<i class="fa fa-filter" aria-hidden="true"></i> Filter
			</div>	
		</a>
		
		
		
		<form method="post" action="<?php echo PATH ?>specials/filter/inventory" id="filterForm">	
			<div style="margin-bottom:10px;padding: 10px; margin-right:10px;">
				<input type="submit" value="Filter Results" style="width:100%" class="greenButton" />				
			</div>
			
			<div style='overflow-y:auto; position: absolute; bottom: 0px; top: 50px; right: 0px; width: 100%;'>
				<div class="FilterBackground">
					<div class="filteredSection">
						<input type="hidden" id="stores" name="stores" value="<?php echo $this -> specialInfo -> RelatedStores() ?>" />
						<div class="filterHeader">Conditions</div>
							<?php 
								$conditions = array_column($this -> inventory, 'Conditions');
								$conditions = array_unique($conditions, SORT_REGULAR) ;
							?>
							<?php foreach($conditions as $conditionSingle) : ?>
								<?php
								 	switch($conditionSingle) {
									case 0:
										$conditionText = "New";
										break;	
									case 1:
										$conditionText = "Used";
										break;	
									
									
								} 
							?>
							<div>
								<input type="checkbox" name="condition[]" onclick="SpecialsController.FilterCondition()" value="<?php echo $conditionSingle ?>" /><?php echo $conditionText ?>	
							</div>		
						<?php endforeach; ?>
				</div>
				
				<div class="filteredSection">
					<a href='javascript:void(0);' onclick="SpecialsController.OpenUpFilterContent(this)">
						<div class="filterHeader">
							<div class="text">Year</div><div class="arrow"></div>
							<div style='clear:both'></div>
						</div>
					</a>
					<div class="content" id="YearContent">
						<?php 
							$Years = array_column($this -> inventory, 'Year');
							$Years = array_unique($Years);	
						?>
						<?php foreach($Years as $yearSingle): ?>
							<div classs='yearsCheckbox'>
								<input type="checkbox" name="Year[]" class='yearSelect' onclick="SpecialsController.FilterYear()" value="<?php echo $yearSingle ?>" style="float:left; margin-right: 2px;" value="" /> <?php echo $yearSingle ?>
							</div>
						<?php endforeach; ?>		 								
					</div>			
				</div>
				
				<div class="filteredSection">
					<a href='javascript:void(0);' onclick="SpecialsController.OpenUpFilterContent(this)">
						<div class="filterHeader">
							<div class="text">Category</div>
							<div class="arrow"></div>
							<div style='clear:both'></div>
						</div>
					</a>
					<div class="content" id="categoryContent">
						<?php 
							$category = array_column($this -> inventory, 'inventoryCategoryName');
							$category = array_unique($category);
									
							$categoryIDs = array_column($this -> inventory, 'inventoryCategoryID');
							$categoryIDs = array_unique($categoryIDs);
						?>
						<?php foreach($category as $key => $categorySingle): ?>
							<div>
								<input type="checkbox" name="category[]" class="categorySelect" onclick="SpecialsController.FilterCategory()" value="<?php echo $categoryIDs[$key] ?>" style="float:left; margin-right: 2px;" value="" /> <?php echo $categorySingle ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="filteredSection">
					<a href='javascript:void(0);' onclick="SpecialsController.OpenUpFilterContent(this)">
						<div class="filterHeader">
							<div class="text">Manufacturer</div>
							<div class="arrow"></div>
							<div style='clear:both'></div>
						</div>
					</a>
					<div class="content" id="ManufactureContent">
						<?php 
							$manufacture = array_column($this -> inventory, 'Manufacturer');
							$manufacture = array_unique($manufacture);
						?>
						<?php foreach($manufacture as $key => $manufactureSingle): ?>
							<div>
								<input type="checkbox" name="manufacture[]" class='manufactureSelect' onclick="SpecialsController.FilterManufacture()" value="<?php echo $manufactureSingle ?>" style="float:left; margin-right: 2px;" value="" /> <?php echo $manufactureSingle ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="filteredSection" id="ModelFilterShow">
					<a href='javascript:void(0)' onclick="SpecialsController.OpenUpFilterContent(this)">
						<div class="filterHeader">
							<div class="text">Model</div>
							<div class="arrow"></div>
							<div style='clear:both'></div>
						</div>
					</a>
					<div class="content" id="ModelNumberContent">
					</div>
				</div>
					
			</div>			
						
		</div>
	</form>
</div>

<div style='margin-left:70px; margin-right: 70px;'>
	<div class="container-fluid">
		<a href="javascript:void(0)" onclick='SpecialsController.CloseInventoryWindow()'>
			<div class="closeInventoryWindow">
				<i class="fa fa-times-circle" aria-hidden="true"></i>	
			</div>
		</a>
		<div class="row">
			<div class="col-md-12">
				<div>
					<div id="IncentiveHeader" style='font-size: 20px; float: left; margin-right: 20px;'></div>
					<div style='float:left; margin-top: 3px;'>
						<input type='checkbox' onclick='SpecialsController.CheckAllInventory(this)' /> Check All
					</div>	
				</div>
				
				<div id="LinkedInventoryError" style='color:#c30000'></div>
			</div>
		</div>
		<form method="post" id="AddInventoryToIncentive">
		<div style='position: absolute; bottom: 80px; top: 80px; overflow-x: hidden; overflow-y: scroll; padding-right: 15px; right: 70px; left: 85px;' id="CurrentActiveInventory">
			
				<?php foreach(array_chunk($this -> inventory, 6, true) as $inventorySix) : ?>
					<div class="row">
						<?php foreach($inventorySix as $inventorySingle): ?>
							<div class="col-md-2">
								<div class="bikeNameElement">
									<input type='checkbox' name="inventoryID[]" class='inventoryID' value='<?php echo $inventorySingle['inventoryID'] ?>' />
									<div class="image">
										<img src='<?php echo PHOTO_URL ?>inventory/<?php echo $inventorySingle['VinNumber'] ?>/<?php echo $inventorySingle['inventoryPhotoName'] ?>-s.<?php echo $inventorySingle['inventoryPhotoExt'] ?>' />
									</div>
									<div class='text'>
										<?php 
											if($inventorySingle['FriendlyModelName']) {
												echo $bikeName = $inventorySingle['Year'] . ' ' .  $inventorySingle['Manufacturer'] . ' ' . $inventorySingle['FriendlyModelName'] . ' | <strong>' . $inventorySingle['Stock'] . '</strong>';
											} else {
												echo  $bikeName = $inventorySingle['Year'] . ' ' .  $inventorySingle['Manufacturer'] . ' ' . $inventorySingle['ModelName'] . ' | <strong>' . $inventorySingle['Stock'] . '</strong>';
											}
																	
										?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
						
					</div>	
				<?php endforeach; ?>

		</div>
		<div style='position: absolute; bottom: 20px;'>
			<input type="submit" value="Apply Checked Inventory" class="greenButton">
		</div>
		</form>	
	</div>
		

</div>


</div>		
	
	
	<script type="text/javascript">
		function RefreshParent() {
	    	if (window.opener != null && !window.opener.closed) {
	        	window.opener.location.reload();
	    	}
	    }
		window.onbeforeunload = RefreshParent;
	</script>
	
</div>



</body>	
</html>
