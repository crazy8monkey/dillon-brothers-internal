<?php


class EmailLogs extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array("https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js", PATH . 'public/js/EmailLogController.js');
		$this -> view -> css = array(PATH . 'public/css/EmailLogController.css');
	}
	
	public function index() {
		$this -> view -> title = "Email Logs";
		$this -> view -> blueBackground = "";
		$emailLogs = new EmailLogList();
		$this -> view -> emailLogSummaryCount = $emailLogs -> LogSummary(date('Y'));
		$this -> view -> inventoryLeads = $emailLogs -> SummaryInventoryLeads(date('Y'));
		$this -> view -> websiteSourceLeads = $emailLogs -> SummaryWebsiteSource(date('Y'));
		$this -> view -> typeofemails = $emailLogs -> SummaryTypeOfEmails(date('Y'));
		$this -> view -> TotalEmails = $emailLogs -> EmaiListAll();
		//$this -> view -> startJsFunction = array('EmailLogController.InitializeSummaryPage(' . date('Y') . ');');
		$this -> view -> render('emaillogs/index');	
	}
	
	public function view($id) {
		$this -> view -> title = 'Viewing Email';
		$this -> view -> blueBackground = "";
		$this -> view -> emailSingle = EmailLogObject::WithID($id);
		$this -> view -> render('emaillogs/single');	
	}
	
	public function summary($type, $year) {
		$emailLogs = new EmailLogList();	
		switch($type) {
			case "inventory":
				echo json_encode($emailLogs -> SummaryInventoryLeads($year));
				break;	
			case "sources":
				echo json_encode($emailLogs -> SummaryWebsiteSource($year));
				break;	
		}
	}
	
	public function delete($id) {
		$email = EmailLogObject::WithID($id);
		$email -> Delete();
	}
	


}
?>