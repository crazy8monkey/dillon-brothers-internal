<?php


class Photos extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		if($_SESSION["user"]->HasPermission('PhotoManagementAddEditDelete') == false) {
			$this -> redirect -> redirectPage(PATH. 'dashboard');
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/PhotosController.js");
		$this -> view -> css = array(PATH . 'public/css/PhotosController.css');
	}
	
	public function index() {
		$years = new YearPhotosList();
		$this -> view -> blueBackground = "";
		$this -> view -> title = "Photo Management System";	
		$this -> view -> startJsFunction = array('PhotosController.InitializeYearDirectory();');
		$this -> view -> years = $years -> Years();
		$this -> view -> render('photos/index');
	}
	
	public function year($year) {
		$this -> view -> blueBackground = "";
		$this -> view -> yearSelect = $year;
		$this -> view -> yearInfo = YearPhotoDirectory::WithYear($year);
		$this -> view -> title = $year . " Photos";
		$this -> view -> startJsFunction = array('PhotosController.InitializeYearDirectory();');
		$this -> view -> render('photos/yearList');
	}
	
	public function type($type, $year) {
		$albums = new PhotoAlbums();
		$eventsList = new EventsList();
		$this -> view -> blueBackground = "";
		
		$typeInteger = NULL;
		switch($type) {
			case "events":
				$this -> view -> title = $year . " Event Photos";
				$this -> view -> startJsFunction = array('PhotosController.InitializeAlbumlist();');
				$this -> view -> eventsList = $eventsList -> NoSubEventsList();
				
				
				$typeInteger = 1;
				break;
			case "blog":
				$this -> view -> title = $year . " Blog Photos";
				$typeInteger = 2;
				break;
			case "misc":
				$this -> view -> title = $year . " Misc Photos";
				$this -> view -> startJsFunction = array('PhotosController.InitializeAlbumlist();');
				$this -> view -> MiscAlbums = "";
				$typeInteger = 3;
				break;
		}
		$this -> view -> yearSelect = $year;
		$this -> view -> typeText = $type;
		$this -> view -> TypeInteger = $typeInteger;
		$this -> view -> albumList = $albums -> AlbumsByYearAndType($year, $typeInteger);
		$this -> view -> render('photos/typeAlbumList');
	}
	
	
	public function view($type, $id) {
		switch($type) {
			case "album":
				$this -> view -> blueBackground = "";
				$eventsList = new EventsList();
				$album = PhotoAlbum::WithID($id);
				$photopathBase = NULL;
				
				switch($album -> AlbumType) {
					case 1:
						$photopathBase = '/events/';	
						break;
					case 2:
						$photopathBase = '/blog/';	
						break;
					case 3:
						$photopathBase = '/misc/';	
						break;
				}
				
				
				$albums = new PhotoAlbums();
				$this -> view -> title = "Viewing Album: " . $album -> GetAlbumName();
				$this -> view -> photos = $albums -> PhotosByAlbums($id);
				
				$this -> view -> eventsList = $eventsList -> NoSubEventsList();
				
				$this -> view -> photoPath = $photopathBase;
				$this -> view -> startJsFunction = array('PhotosController.InitializePhotoAlbumDetails();');
				$this -> view -> AlbumDetails = $album;
				$this -> view -> render('photos/singleAlbum');
				break;
			case "photo":
				$this -> view -> blueBackground = "";
				$photosinglepath = NULL;
				$photoSingle = Photo::WithID($id);
				
				$this -> view -> title = "View Photo: " . $photoSingle -> CurrentImageName;
				$this -> view -> photoSingle = $photoSingle;
				
				switch($photoSingle -> AlbumType) {
					case 1:
						$photosinglepath = '/events/';	
						break;	
					case 2:
						$photosinglepath = '/blog/';
						break;
					case 3:
						$photosinglepath = '/misc/';
						break;
				}
				
				$this -> view -> startJsFunction = array('PhotosController.InitializePhotoSingle();');
				$this -> view -> photoSinglePath = $photosinglepath;
				$this -> view -> render('photos/photoSingle');
				
				break;
		}
	}

	public function addphotos($albumName) {
		$albumSingle = PhotoAlbum::WithFolderName($albumName);
		$this -> view -> blueBackground = "";
		$this -> view -> title = "Add Photos To Album";
		$this -> view -> AlbumSingle = $albumSingle;
		$this -> view -> albumType = $albumSingle -> AlbumType;
		$this -> view -> multipePhotoUpload = '';
		$this -> view -> photoUploads = count(ini_get('max_file_uploads'));
		$this -> view -> startJsFunction = array('PhotosController.InitializeAddPhotos();');
		$this -> view -> render('photos/addphotos');
	}
	
	
	public function uploadSingle($albumName) {
		
		$albumSingle = PhotoAlbum::WithID($albumName);
		$this -> view -> title = "Add Photos To Album";
		$this -> view -> uploadSingleView = '';
		$this -> view -> blueBackground = "";
		$this -> view -> AlbumSingle = $albumSingle;
		$this -> view -> albumType = $albumSingle -> AlbumType;
		$this -> view -> photoUploads = count(ini_get('max_file_uploads'));
		$this -> view -> startJsFunction = array('PhotosController.InitializeAddPhotos();');
		$this -> view -> render('photos/addphotosingle');
	}

	
	

	
	
	public function delete($type, $id) {
		switch($type) {
			case "album":
				$album = PhotoAlbum::WithID($id);
				$album -> Delete();
				break;
			case "photo":
				$photo = Photo::WithID($id);
				$photo -> delete();
				break;
		}
		
		
		
	}
	
	public function save($type, $id = NULL) {
		switch($type) {
			case "album":
				$photoAlbum = new PhotoAlbum($id);
				$photoAlbum -> AlbumType = 2;
				$photoAlbum -> folderName = $_POST['albumName'];
				
				if($photoAlbum -> Validate()) {
					$photoAlbum -> NewDirectory();
				}
				
				break;
			case "photo":
				$photo = Photo::WithID($id);
				$photo -> photoName = $_POST['photoName'];
				$photo -> altText = $_POST['altText'];
				$photo -> titleText = $_POST['titleText'];
						
				if($photo -> Validate()) {
					$photo -> Save();
				}								
				break;
			case "year":
				$album = new PhotoAlbum();
				$album -> year = $_POST['yearDirectory'];
				
				if($album -> ValidateYear()) {
					$album -> EnterNewYearDirectory();
					$album -> RedirectToPhotosMainPage();
				}
				
				
				break;
			case "albumdetails":
				$photoAlbum = PhotoAlbum::WithID($id);
				$photoAlbum -> NewAlbumName = $_POST['albumName'];
				$photoAlbum -> Visible = $_POST['photoAlbumVisible'];				
				$photoAlbum -> LinkedEvent = $_POST['linkedAlbumEvent'];
							
				if($photoAlbum -> ValidateAlbum()) {
					$photoAlbum -> SaveAlbumDetails();	
				}			
								
				
				break;
			case "newalbum":
				$photoAlbum = new PhotoAlbum();
				$photoAlbum -> year = $_GET['year'];
				$photoAlbum -> folderName = $_POST['albumName'];
				
				if(isset($_POST['albumEvent'])) {
					$photoAlbum -> LinkedSuperEventID = $_POST['albumEvent'];	
				}
				
				$photoAlbum -> AlbumType = $_POST['albumType'];
				
				
				if($photoAlbum -> Validate()) {
					$photoAlbum -> NewDirectory($_GET['year']);
					$photoAlbum -> RedirectYearTypeDirectory();
				}
				
				break;
			case "yearphoto":
				$year = YearPhotoDirectory::WithID($id);
				$year -> photo = $_FILES['yearPhotoUpload']['name'];
				
				if($year -> Validate()) {
					$year -> Save();
				}
				
				break;
		}			
				
			

	}

	
	public function photovisible($id) {
		$photo = Photo::WithID($id);
		$photo -> visibility = $_POST['visible'];
		$photo -> ChangeVisiblity();
		
	}
	
	public function uploadtoalbum($id) {	
		$photoAlbum = PhotoAlbum::WithID($id);
		$photoAlbum -> fileUploads = $_FILES['file']['tmp_name'];
		
		if(isset($_POST['BlogMainPhoto'])) {
			$photoAlbum -> BlogMainPhoto = $_POST['BlogMainPhoto'];	
		}
		
		if(isset($_POST['EventMainPhoto'])) {
			$photoAlbum -> EventMainPhoto = $_POST['EventMainPhoto'];
		}
		
		if(isset($_POST['EventThumbNailPhoto'])) {
			$photoAlbum -> EventThumbNailPhoto = $_POST['EventThumbNailPhoto'];
		}
		
		
		//if($photoAlbum -> ValidateUpload()) {	
		$photoAlbum -> UploadPhotos();
		//}
	}
	
	public function GetPhotosOfCurrentAlbum($id) {
		$photoAlbum = PhotoAlbum::WithID($id);
		echo json_encode($photoAlbum -> GetPhotosOfAlbum());
	}
	
	public function savethumb($id) {
		$photoAlbum = PhotoAlbum::WithID($id);
		$photoAlbum -> albumThumbNail = $_POST['albumThumbNail'];
		$photoAlbum -> SaveThumbNail();
		
	}

	public function watermark($type, $id) {
		switch($type) {
			case "all";
				$album = PhotoAlbum::WithID($id);
				$album -> WaterMarkAlbumPhotos();
				break;
			case "single":
				$photoSingle = Photo::WithID($id);
				$photoSingle -> WaterMarkPhoto();
				break;
		}
		
		
	} 

	public function rotate($direction, $id) {
		$inventoryPhoto = Photo::WithID($id);
		$inventoryPhoto -> rotation = $direction;
		$inventoryPhoto -> RotateImage();
	}

}
?>