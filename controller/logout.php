<?php


class Logout extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		
	}
	
	public function index() {
		Session::destroy();
		$this -> redirect -> redirectPage("login");
		exit();
	}
	

}
?>