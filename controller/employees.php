<?php


class Employees extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		if($_SESSION["user"]->HasPermission('EmployeesAddEditDelete') == false) {
			$this -> redirect -> redirectPage(PATH. 'dashboard');
		}
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/EmployeeController.js");
		$this -> view -> css = array(PATH . 'public/css/EmployeeController.css');
	}
	
	public function index() {
		$this -> view -> blueBackground = "";
		$employees = new EmployeeList();
		$this -> view -> Employees = $employees -> Employees();
		$this -> view -> title = "Employees";
		$this -> view -> render('employees/list');
	}
	
	public function create() {
		$this -> view -> title = "New Employee";	
		$this -> view -> startJsFunction = array('EmployeeController.InitializeEmployeeForm();');
		$this -> view -> render('employees/create');
	}
	
	public function edit($id) {
		$this -> view -> title = "Edit Employee";	
		$this -> view -> EmployeeSingle = Employee::WithID($id);
		$this -> view -> startJsFunction = array('EmployeeController.InitializeEmployeeForm();');
		$this -> view -> render('employees/edit');
	}

	
	public function save($id = NULL) {
		if(isset($id)) {
			$employee = Employee::WithID($id);
		} else {
			$employee = New Employee();
		}
				
		$employee -> photo = $_FILES['employeePhoto']['name'];	
		$employee -> firstName = $_POST['employeeFirstName'];
		$employee -> lastName = $_POST['employeeLastName'];
		$employee -> Title = $_POST['employeeTitle'];
		$employee -> email = $_POST['employeeEmail'];
		$employee -> Phone = $_POST['employeePhone'];
		$employee -> department = $_POST['employeeDepartment'];	
				
		if(isset($_POST['store'])) {
			$employee -> Store = $_POST['store'];	
		}
				
		if($employee -> Validate()) {
			$employee -> Save();
		}
	}


	public function delete($id) {
		$employee = Employee::WithID($id);
		$employee -> delete();
	}

}
?>