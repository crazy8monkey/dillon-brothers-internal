<?php
require_once TWILIO_API_PATH. 'autoload.php'; // Loads the library
 
use Twilio\Rest\Client; 
use Twilio\Twiml;

class Updates extends Controller {
		
	private $MotorSportInventory  = array();	
	private $HarleyInventory = array();
	private $IndianInventory = array();
		
	public function __construct() {
		parent::__construct();
		$this -> mapCSVFile();
		
		set_time_limit(0);
		ini_set('memory_limit', '-1');
	}
	
	private function mapCSVFile() {
		//motor sport
		$MotorSportCSV = array_map('str_getcsv', file(MOTOR_SPORT_INVENTORY));
	    array_walk($MotorSportCSV, function(&$a) use ($MotorSportCSV) {
	      $a = array_combine($MotorSportCSV[0], $a);
	    });
		
	    array_shift($MotorSportCSV);
		
		
		$HarleyCSV = array_map('str_getcsv', file(HARLEY_INVENTORY));
	    array_walk($HarleyCSV, function(&$a) use ($HarleyCSV) {
	      $a = array_combine($HarleyCSV[0], $a);
	    });
		
	    array_shift($HarleyCSV);
		
		$IndianCSV = array_map('str_getcsv', file(INDIAN_INVENTORY));
	    array_walk($IndianCSV, function(&$a) use ($IndianCSV) {
	      $a = array_combine($IndianCSV[0], $a);
	    });
		
	    array_shift($IndianCSV);
		
		$this -> MotorSportInventory = $MotorSportCSV;
		$this -> HarleyInventory = $HarleyCSV;
		$this -> IndianInventory = $IndianCSV;
	}
	
	public function IncompleteList() {
		$storeList = new StoreList();
		$usersList = new UserList();
		
		$html = NULL;
		foreach($storeList -> SocialMediaProgress() as $storeProgress) {
			
			$html .= "<tr>
						<td style='padding:10px; border-bottom:1px solid #eaeaea'>
							<table width='100%' cellpadding='0' cellspacing='0'>
								<tr>
									<td style='vertical-align:top; font-size:20px; font-family:arial; font-weight:bold;'>" . $storeProgress['StoreName'] . "</td>
									<td style='text-align:right; font-size:20px; font-family:arial; font-weight:bold;'>" . ceil($storeProgress['TotalSubmittedContent'] * 100) . "%</td>
								</tr>
								<tr>
									<td colspan='2' style='font-family:arial; font-size:16px; padding:10px 0px 0px 0px'><img src='" . PATH . "public/images/facebookIcon.png' style='vertical-align: middle;' width='18' height='18' /> ". $storeProgress['FacebookCount'] ."/" . $storeProgress['RequiredFacebook'] . "</td>
								</tr>
								<tr>
									<td colspan='2' style='font-family:arial; font-size:16px; padding:5px 0px 0px 0px''><img src='" . PATH . "public/images/twitter.png' style='vertical-align: middle;' width='18' height='18' /> ". $storeProgress['TwitterCount'] ."/" . $storeProgress['RequiredTwitter'] . "</td>
								</tr>
							</table>
						</td>
					  </tr>";
				
		}
		
		foreach($usersList -> AllUsersStoreRelation() as $userSingle) {
			$userObject = User::WithID($userSingle['userID']);
			
			$htmlSingle = NULL;			
			if($userObject -> HasPermission("SocialMediaPermissionType") == 1) {
				$storeNames = explode(",", $userSingle['AssociatedStoreNames']);
				$progress = explode(",", $userSingle['StoresProgress']);
				$facebookSubmitted = explode(",", $userSingle['SubmittedFacebookPosts']);
				$twitterSubmitted = explode(",", $userSingle['SubmittedTwitterPosts']);	
				
				foreach($storeNames as $key => $value) {
					$htmlSingle .= "<tr>
										<td style='padding:10px; border-bottom:1px solid #eaeaea'>
											<table width='100%' cellpadding='0' cellspacing='0'>
												<tr>
													<td style='vertical-align:top; font-size:20px; font-family:arial; font-weight:bold;'>" . $value . "</td>
													<td style='text-align:right; font-size:20px; font-family:arial; font-weight:bold;'>" . ceil($progress[$key] * 100) . "%</td>
												</tr>
												<tr>
													<td colspan='2' style='font-family:arial; font-size:16px; padding:10px 0px 0px 0px'><img src='" . PATH . "public/images/facebookIcon.png' style='vertical-align: middle;' width='18' height='18' /> ". $facebookSubmitted[$key] ."/" . $userSingle['RequiredFacebook'] . "</td>
												</tr>
												<tr>
													<td colspan='2' style='font-family:arial; font-size:16px; padding:5px 0px 0px 0px''><img src='" . PATH . "public/images/twitter.png' style='vertical-align: middle;' width='18' height='18' /> ". $twitterSubmitted[$key] ."/" . $userSingle['RequiredTwitter'] . "</td>
												</tr>
											</table>
										</td>
									  </tr>";
				}	
					
				if(LIVE_SITE == true) {
										
					$this -> email -> to = $userSingle['UserEmail'];	
					$this -> email -> subject = "Store List Progress";
					$this -> email -> EmaiSummary($htmlSingle);	
				}
				
				echo "<table width='100%'>" . $htmlSingle . "</table>";
				
			}
						
			
		}
		
		if(LIVE_SITE == true) {
			$setting = SettingsObject::Init();	
								
			$this -> email -> to = $setting -> EmailSummary;	
			$this -> email -> subject = "Store List Progress";
			$this -> email -> EmaiSummary($html);	
		}
		
		
		//echo "<table width='100%'>" . $html . "</table>";
	}

	public function UpdateInventory() {
		$inventory = new InventoryObject();
		
		foreach($this -> MotorSportInventory as $MotorSportInventorySingle) {
			$store = Store::WithCSV("MUInventory_C.csv");
			
			
			$inventory -> VinNumber = $MotorSportInventorySingle['VIN'];
			$inventory -> Year = $MotorSportInventorySingle['Year'];
			$inventory -> Manufacture = $MotorSportInventorySingle['Make'];
			$inventory -> Model = $MotorSportInventorySingle['Model'];
			$inventory -> StatusCode = $MotorSportInventorySingle['StatusCode'];
			$inventory -> Stock = $MotorSportInventorySingle['Stock'];
			$inventory -> Color = $MotorSportInventorySingle['Color'];
			$inventory -> Mileage = $MotorSportInventorySingle['Mileage'];
			$inventory -> Conditions = $MotorSportInventorySingle['Conditions'];
			$inventory -> MSRP = $MotorSportInventorySingle['MSRP'];
			$inventory -> StoreLocation = $MotorSportInventorySingle['StoreLocation'];
			$inventory -> Category = $MotorSportInventorySingle['DealerCatName'];
			$inventory -> StoreID = $store -> GetID();
			$inventory -> SaveStaging();
			
		}
		
		foreach($this -> HarleyInventory as $HarleyInventorySingle) {
			$store = Store::WithCSV("MUInventory_AB.csv");
			
			$inventory -> VinNumber = $HarleyInventorySingle['VIN'];
			$inventory -> Year = $HarleyInventorySingle['Year'];
			$inventory -> Manufacture = $HarleyInventorySingle['Make'];
			$inventory -> Model = $HarleyInventorySingle['Model'];
			$inventory -> StatusCode = $HarleyInventorySingle['StatusCode'];
			$inventory -> Stock = $HarleyInventorySingle['Stock'];
			$inventory -> Color = $HarleyInventorySingle['Color'];
			$inventory -> Mileage = $HarleyInventorySingle['Mileage'];
			$inventory -> Conditions = $HarleyInventorySingle['Conditions'];
			$inventory -> MSRP = $HarleyInventorySingle['MSRP'];
			$inventory -> StoreLocation = $HarleyInventorySingle['StoreLocation'];
			$inventory -> Category = $HarleyInventorySingle['DealerCatName'];
			$inventory -> StoreID = $store -> GetID();
			$inventory -> SaveStaging();
			
		}
		
		foreach($this -> IndianInventory as $IndianInventorySingle) {
			$store = Store::WithCSV("MUInventory_D.csv");
			
			$inventory -> VinNumber = $IndianInventorySingle['VIN'];
			$inventory -> Year = $IndianInventorySingle['Year'];
			$inventory -> Manufacture = $IndianInventorySingle['Make'];
			$inventory -> Model = $IndianInventorySingle['Model'];
			$inventory -> StatusCode = $IndianInventorySingle['StatusCode'];
			$inventory -> Stock = $IndianInventorySingle['Stock'];
			$inventory -> Color = $IndianInventorySingle['Color'];
			$inventory -> Mileage = $IndianInventorySingle['Mileage'];
			$inventory -> Conditions = $IndianInventorySingle['Conditions'];
			$inventory -> MSRP = $IndianInventorySingle['MSRP'];
			$inventory -> StoreLocation = $IndianInventorySingle['StoreLocation'];
			$inventory -> Category = $IndianInventorySingle['DealerCatName'];
			$inventory -> StoreID = $store -> GetID();
			$inventory -> SaveStaging();
			
		}
		
		$inventory -> InsertNewInventory();
		$inventory -> UpdateInventory();
		$inventory -> MarkInventoryAsSold();
		$inventory -> RemoveItems();
	}

	public function ClearInventory() {
		$inventory = new InventoryObject();
		$inventory -> ClearInventory();
	}

	public function GetVehicleInfo() {
		$inventory = new InventoryObject();
		$inventory -> GetVehicleInfo();
		$inventory -> RemoveInventoryFromSpecials();
		$inventory -> UpdateURLS();
		$inventory -> BulkUpdateAlgoliaSearch();
		$dataConversion = new ConversionRule();
		$dataConversion -> CleanManufactureText();
		$dataConversion -> CleanManufactureTextBackup();
	}
	
	public function redirectlogin() {
		Session::init();
		$_SESSION['Redirect'] = $_GET['val'];
		
		$this -> redirect -> redirectPage(PATH. 'login');
		
	}
	
	public function AlgoliaPushTest() {
		$inventory = new InventoryObject();
		$inventory -> SendLiveInventoryToAlgolia();
	}
	
	public function SendToMonkeyLearn() {
		$backup = new InventoryBackup();
		$backup -> SendToMonkeyLearn();
	}
	
	private function file_get_contents_curl($url) {
	    $ch = curl_init();
	
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	
	    $data = curl_exec($ch);
	    curl_close($ch);
	
	    return $data;
	}
	
	
	public function GetManufactureName() {
			
		$manufactureURL = $this -> file_get_contents_curl("http://www.motorcycledb.com/manufacturers.php");
		
		
		if ($manufactureURL === TRUE) {
			//parsing begins here:
			$doc = new DOMDocument();
			$doc->loadHTML($manufactureURL);	
			$hyperLinks = $doc->getElementsByTagName('a');	
			foreach ($hyperLinks as $link) {
				if(strpos($link->getAttribute('href'), '/Motorcycle_Manufacturer/') !== false ) {
					$manufacturerLinkArray = explode('/', $link->getAttribute('href'));	
					//print_r($manufacturerLinkArray);
					if($manufacturerLinkArray[2] == "Honda") {
						echo $link->getAttribute('href');
						exit;
					}
					//echo $link->getAttribute('href') . "<br />";	
				}	
				
			}			
		} else {
			echo "it failed";
		}
		

	}
	
	public function GetManufacturerYear() {
			
		$manufactureURL = $this -> file_get_contents_curl("http://www.motorcycledb.com/Motorcycle_Manufacturer/Honda/3961");
		
		//parsing begins here:
		$doc = new DOMDocument();
		$doc->loadHTML($manufactureURL);
		$yearsLinks = $doc->getElementsByTagName('a');
		
		foreach ($yearsLinks as $aTag) {
			if(strpos($aTag->getAttribute('href'), '2005') !== false ) {
				echo $aTag->getAttribute('href'). "<br />";
				
				//echo  "<pre>";
				//print_r($tableTag);
				//echo  "</pre>";
			}	
			
		}
	}
	
	public function GetBike() {
		$yearBikes = array("/Honda_919__CB_900_F_2005/19585", "/Honda_CB_250_Nighthawk_2005/24941", "/Honda_CBF_600_N_2005/25117", 
						   "/Honda_CBF_600_S_2005/25119", "/Honda_CBR_1000_RR_2005/25139", "/Honda_CBR_125_R_2005/25149",
						   "/Honda_CBR_600_F4i_2005/25175", "/Honda_CBR_600_RR_2005/25181", "/Honda_CMX_250_Rebel_2005/25284",
						   "/Honda_Concept_1_2005/25293", "/Honda_CR_125_R_2005/25297", "/Honda_CR_250_R_2005/25302",
						   "/Honda_CR_85_R_2005/33954", "/Honda_CR_85_R_Expert_2005/25316", "/Honda_CRF_100_F_2005/25323",
						   "/Honda_CRF_150_F_2005/25328", "/Honda_CRF_230_F_2005/25332", "/Honda_CRF_250_R_2005/25339",
						   "/Honda_CRF_250_X_2005/25343", "/Honda_CRF_450_R_2005/33963", "/Honda_CRF_450_X_2005/25351",
						   "/Honda_CRF_50_F_2005/25355", "/Honda_CRF_70_F_2005/25360", "/Honda_CRF_80_F_2005/33967",
						   "/Honda_CT_110_2005/31214", "/Honda_FSC_600_S_2005/25449", "/Honda_GL_1800_Gold_Wing_2005/25486",
						   "/Honda_GL_1800_Gold_Wing_ABS_2005/31247", "/Honda_Hornet_250_2005/25517", "/Honda_RC_51_2005/25623",
						   "/Honda_Shadow_Aero_2005/25669", "/Honda_Shadow_Sabre_2005/25672", "/Honda_Shadow_Spirit_2005/25678",
						   "/Honda_Shadow_Spirit_750_2005/31318", "/Honda_Shadow_VLX_2005/25685", "/Honda_Shadow_VTX_Deluxe_2005/25688",
						   "/Honda_ST_1300_2005/25723", "/Honda_ST_1300_ABS_2005/25726", "/Honda_VFR_800_FI_Interceptor_2005/25825",
						   "/Honda_VFR_800_FI_Interceptor_ABS_2005/25828", "/Honda_VTR_1000_F_Super_Hawk_2005/31392", "/Honda_VTX_1300_C_2005/25924",
						   "/Honda_VTX_1300_R_2005/25926", "/Honda_VTX_1300_S_2005/25929", "/Honda_VTX_1800_2005/25931",
						   "/Honda_XR_250_Tornado_2005/26060", "/Honda_XR_650_L_2005/26072", "/Honda_XR_650_R_2005/35665", "/Rieju_RS1_Castrol_2005/21923");

		
		foreach($yearBikes as $bike) {
			//echo $bike . "<br />";
			$name = explode("/", "GL1800A/GOLD WING");
			$capitalizeWords =ucwords(strtolower(end($name)));
			//echo ucwords(strtolower(end($name)));
			
			//echo str_replace(" ", "_", $capitalizeWords); 
			
			if(strpos($bike, str_replace(" ", "_", $capitalizeWords)) !== false ) {
				echo $bike. "<br />";
				exit;
				//echo  "<pre>";
				//print_r($tableTag);
				//echo  "</pre>";
			}	
		}
		
		
		
		
	}

	
	public function GetBikeData() {
		$bikeUrlData = $this -> file_get_contents_curl("http://www.motorcycledb.com/Honda_GL_1800_Gold_Wing_2005/25486");
		
		//parsing begins here:
		$doc = new DOMDocument();
		$doc->loadHTML(mb_convert_encoding($bikeUrlData, 'HTML-ENTITIES', 'UTF-8'));
		$internalErrors = libxml_use_internal_errors(true);
		libxml_use_internal_errors($internalErrors);
		
		$xpath = new DOMXpath($doc);
		$result = $xpath->query('//table[@width="600"]');
		
		//specs table
		$specs = $doc -> saveXML($result->item(1));
		echo $doc -> saveXML($result->item(1));
		
		$specHTML = new DOMDocument();
		$specHTML -> loadHTML(mb_convert_encoding($specs, 'HTML-ENTITIES', 'UTF-8'));
		
		//echo $specHTML -> saveHTML();
		
		$TRData = $specHTML->getElementsByTagName('tr');
		
		$SpecInfoFinal = array();
		//echo count($TRData);
		
		$FinalSpecsArray = array();
		
		foreach ($TRData as $key => $trSingle) {
			$trContent = explode(":", $trSingle -> textContent);
			//echo "<pre>";
			//print_r($trSingle);
			//echo "</pre>";
			//echo count($trContent);
			//echo $trSingle -> textContent . "<br />";
			
			if(count($trContent) > 1) {
				array_push($FinalSpecsArray, array("Label" => $trContent[0],
												   "Content" => $trContent[1]));
			}
			
			//if($hasColSpan == false) {
			//	$trSpath = new DOMXpath($specHTML);
			//	$TRresult = $trSpath->query('//tr');
				
				//echo  "<div style='border:1px solid black'><table>";
				//echo $specHTML -> saveXML($TRresult->item($key));
				//echo  "</table></div>";
			//}
		}
		
		echo "<pre>";
		print_r($FinalSpecsArray);
		echo "</pre>";
		
		
		
		
	}

	public function BikezGetYear() {
		$Year = $this -> file_get_contents_curl("http://www.bikez.com/years/index.php");
		
		$doc = new DOMDocument();
		$doc->loadHTML(mb_convert_encoding($Year, 'HTML-ENTITIES', 'UTF-8'));
		$internalErrors = libxml_use_internal_errors(true);
		libxml_use_internal_errors($internalErrors);
		
		$xpath = new DOMXpath($doc);
		$result = $xpath->query('//table[@class="zebra"]');
		
		$yearListTable = $doc -> saveXML($result->item(0));
		//echo $doc -> saveXML($result->item(0));
		$yearListHTML = new DOMDocument();
		$yearListHTML -> loadHTML(mb_convert_encoding($yearListTable, 'HTML-ENTITIES', 'UTF-8'));
		
		$yearLinks = $yearListHTML->getElementsByTagName('a');
			
		foreach($yearLinks as $link) {
			//echo $link->getAttribute('href'). "<br />";
			
			if(strpos($link->getAttribute('href'), "2017") !== false ) {
				echo str_replace("..", "", $link->getAttribute('href')) . '<br />';
				break;
			}
			
		}	
			
	}

	public function BikezGetBike() {
		$BikeYear = $this -> file_get_contents_curl("http://www.bikez.com/year/2017-motorcycle-models.php");
		
		$doc = new DOMDocument();
		$doc->loadHTML(mb_convert_encoding($BikeYear, 'HTML-ENTITIES', 'UTF-8'));
		$internalErrors = libxml_use_internal_errors(true);
		libxml_use_internal_errors($internalErrors);
		
		$xpath = new DOMXpath($doc);
		$result = $xpath->query('//table[@class="zebra"]');
		
		$bikeListTable = $doc -> saveXML($result->item(0));
		//echo $doc -> saveXML($result->item(0));
		$bikeList = new DOMDocument();
		$bikeList -> loadHTML(mb_convert_encoding($bikeListTable, 'HTML-ENTITIES', 'UTF-8'));
		
		$bikeLinks = $bikeList -> getElementsByTagName('a');
			
		foreach($bikeLinks as $link) {
			//echo $link -> nodeValue. "<br />";
			//$link->getAttribute('href'). "<br />";
			
			if(strpos($link -> nodeValue, "Kawasaki") !== false ) {
				if(strpos($link->getAttribute('href'), "/motorcycles/")) {
					$simpliedURL = 	str_replace("kawasaki ", "", strtolower($link->nodeValue));
						
					echo str_replace(" ", "", $simpliedURL) . '<br />';	
					//break;
				}
				
		//		break;
			}
			
		}	
			
	}

	public function ChromeDataTest() {
		$mode = array ('soap_version' => 'SOAP_1_1',  // use soap 1.1 client
		               'trace' => 1,
					   'trace' => true, 
					   'exceptions' => true);	
				
			
		$chromeDataClient = new SoapClient("http://services.chromedata.com/Description/7b?wsdl", $mode);
			
		echo "<pre>";
		print_r($chromeDataClient->__getFunctions());
		echo "</pre>";	
			
		echo "<pre>";
		print_r($chromeDataClient->__getTypes());
		echo "</pre>";	
			
			
			
		
			
		$CredentialKeys = array("accountInfo" => array('number' => '306366',
													   'secret'  => '67014a561e134597',
													   'country' => 'US',
													   'language' => 'en'),
								'vin' => 'JY4AG04Y4CC022715',
								'ShowAvailableEquipment' => true,
								'ShowExtendedDescriptions' => true,
								'ShowExtendedTechnicalSpecifications' => true
								
		);	
			
		$describeVehicle = $chromeDataClient -> describeVehicle($CredentialKeys);
		
		echo "Describe Vehicle Reauest<pre>";
		print_r($describeVehicle);
		echo "</pre>";
		
		$JSONchromeDataDescribeVehicle = json_encode($describeVehicle);
		$describeVehicleDecoded = json_decode($JSONchromeDataDescribeVehicle, true);
		
		//echo $describeVehicleDecoded['vinDescription']['marketClass']['id'];
		
		$styles = array("accountInfo" => array('number' => '306366',
													   'secret'  => '67014a561e134597',
													   'country' => 'US',
													   'language' => 'en'),
				       $describeVehicle);
		
		
		$getStyleRequest = $chromeDataClient -> getTechnicalSpecificationDefinitions($styles);
		
		
		
		//echo "get Technicle Definitions <pre>";
		//print_r($getStyleRequest);
		//echo "</pre>";
		
		
		$JSONTechnicalSepc = json_encode($getStyleRequest);
		$JSONTechnicalSepcDecoded = json_decode($JSONTechnicalSepc, true);
		
		//definition = array('group' => $JSONTechnicalSepcDecoded['definition']['0']['group']['_'],
		//					 'header' => $JSONTechnicalSepcDecoded['definition']['0']['header']['_'],
		//					 'title' => $JSONTechnicalSepcDecoded['definition']['0']['title']['_'],
		//					 'measurementUnit' => $JSONTechnicalSepcDecoded['definition']['0']['measurementUnit']);
		
		//echo $JSONTechnicalSepcDecoded['definition']['0']['group']['_'];
		//echo $JSONTechnicalSepcDecoded['definition']['0']['header']['_'];
		//echo $JSONTechnicalSepcDecoded['definition']['0']['title']['_'];
		//echo $JSONTechnicalSepcDecoded['definition']['0']['measurementUnit'];
		
		//echo "<pre>";
		//print_r($JSONTechnicalSepcDecoded['definition']);
		//echo "</pre>";	
		
		$techAgain = array("accountInfo" => array('number' => '306366',
													   'secret'  => '67014a561e134597',
													   'country' => 'US',
													   'language' => 'en'),
				      	  "definition" => array('group' => $JSONTechnicalSepcDecoded['definition']['0']['group']['_'],
							 				   'header' => $JSONTechnicalSepcDecoded['definition']['0']['header']['_'],
											   'title' => $JSONTechnicalSepcDecoded['definition']['0']['title']['_'],
											   'measurementUnit' => $JSONTechnicalSepcDecoded['definition']['0']['measurementUnit']));
		
		$technicalDefinAgain = $chromeDataClient -> getTechnicalSpecificationDefinitions($techAgain);
		
		//echo "<pre>";
		//print_r($technicalDefinAgain);
		//echo "</pre>";	
	} 

	public function phpsettings() {
		phpinfo();
		 //foreach(PDO::getAvailableDrivers() as $driver) {
		//	echo $driver.'<br />';
		//}
	}

	public function MonkeyLearn() {
		require 'libs/MonkeyLearn/autoload.php';
		
		$module_id = 'cl_4LLnXuzp';
		
		//
		$ml = new MonkeyLearn\Client('c6046ac2da646a10e42c70be661ecf6fbe565c21');

		$classifierDetail = $ml->classifiers->detail($module_id);
		
		$currentCategoryies = simplexml_load_string($classifierDetail['sandbox_categories']);
		$json = json_encode($currentCategoryies);
		$array = json_decode($json,TRUE);
		
		if(count($array) > 1) {
			
		}
		
		var_dump($classifierDetail -> result);
		
		
		
		
		
	}
	
	public function ClassifyTest() {
		require 'libs/MonkeyLearn/autoload.php';
		
		$module_id = 'cl_4LLnXuzp';
		
		$ml = new MonkeyLearn\Client('c6046ac2da646a10e42c70be661ecf6fbe565c21');

		$parent_category_id = 13854345;
		
		$text_list = ["Yamaha XV1700A"];
		$module_id = 'cl_4LLnXuzp';
		$res = $ml->classifiers->classify($module_id, $text_list, true);
		
		
		
		$results = end($res-> result);
		$selectedVehicleCategory = end($results);
		
		echo "<pre>";
		echo print_r($selectedVehicleCategory);
		echo "</pre>";
		
		echo $selectedVehicleCategory['category_id'];
		//print_r($classifyJSONResponse);
		
	}
	
	public function UpdateEngineSizeCC() {
		$inventory = new InventoryObject();
		$inventory -> UpdateEngineSizeCC();
	}
	

	
	public function CRMpushTest() {

		$xml_data ='<ProspectImport>
					<Item>
					<SourceProspectId>dillon-brothers.com</SourceProspectId>
					<DealershipId>76055634</DealershipId>
					<Email>adam.schmidt@siddillon.com</Email>
					<Name>Jarrod Rowher Test</Name>
					<Phone>40278912348</Phone>
					<AltPhone />
					<SourceDate></SourceDate>
					<Address1>3848 N HWS Cleveland Blvd.</Address1>
					<Address2 />
					<City>Omaha</City>
					<State>NE</State>
					<ZipCode>68116</ZipCode>
					<VehicleType>Cruiser</VehicleType>
					<VehicleMake>Harley-Davidson</VehicleMake>
					<VehicleModel>FLHRSE3</VehicleModel>
					<VehicleYear>2007</VehicleYear>
					<Notes>Test</Notes>
					<ProspectType></ProspectType>
					<BirthDate></BirthDate>
					<CAEmployerName></CAEmployerName>
					<CAJobTitle></CAJobTitle>
					<CAHireDate></CAHireDate>
					<CAMonthlyIncome></CAMonthlyIncome>
					<CATimeAtAddress></CATimeAtAddress>
					<CARentOrMortgagePayment></CARentOrMortgagePayment>
					<CARentOrOwn></CARentOrOwn>
					</Item>
					</ProspectImport>';
	
		
		$url = 'http://pch.v-sept.com/VSEPTPCHPostService.aspx?method=AddProspect&sourceid=DillonBros';
		
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
		// Following line is compulsary to add as it is:
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "ProspectXML=" . $xml_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        curl_close($ch);

		echo $data;
		//print_r($result);
	}


  	public function twiliocatchresponse() {
		//Session::init();
		//$code = $_SESSION['RefCode'];
		
		//if($_SERVER['REQUEST_METHOD']=='POST') {
		//echo $code;
		//}
		
	  	//echo $_POST['From'];
      	$textMessage = TextMessageConversationCustomer::WithNumber($_POST['From']);
	  	$textMessage -> NewMessage = $_POST['Body'];
	  	$textMessage -> SaveCustomerResponse();	
		
		header('content-type: text/xml');
		//echo '<Response><Message>hi</Message></Response>';
	}
	
	public function catchchatresponse() {
		//echo $_POST;
		//echo $_POST['request'];
		$messageAttributes = json_decode($_POST['request'], true); 
		
		$chatConversation = ChatConversation::WithTaskID($messageAttributes['attributes']['taskID']);
		$chatConversation -> FromCustomerValue = $messageAttributes['attributes']['fromCustomer'];
		$chatConversation -> Message = $messageAttributes['text'];
		$chatConversation -> SaveMessage();
		Log::UserAgent($_POST['request']);
	}


	public function smsoutbound() {
		Log::UserAgent($_POST);
	}
	
	public function smssingle() {
		$client = new Client('AC23289f8d8a0e5a87ae60349eb24ee1b8', 'fe80215bdc30955d0be7a06d18351fbb'); 
		$messages = $client->api->v2010->accounts("AC23289f8d8a0e5a87ae60349eb24ee1b8")->messages("SM40e122670d1451714b18e7c6e5af3f75")->fetch(); 
		echo "<pre>";
		print_r($messages);
		echo "</pre>";
		
		echo 'number of segments: ' .  $messages -> numSegments;
		
		
		$response = new Twiml;
		$response->message("The Robots are coming! Head for the hills!");
		print $response;
		Session::init();
		echo $_SESSION['RefCode']; 	
		
	}
	
	public function twilioCreateChannel() {
		$uniqueID = uniqid();
		
		$chatChannel = new TwilioChatChannel();
		$chatChannel -> FriendlyName = "New Channel" .$uniqueID;
		$chatChannel -> UniqueName = "newchannel" .$uniqueID;
		
		$chatChannel -> Save();		
	}
	
	public function twilioCreateMember($channelID) {
		$channelMember = TwilioChatChannel::TwilioChannelID($channelID);
		$channelMember -> UserID = '0';
		$channelMember -> CreateChannelMember();
	}
	
	public function testchat() {
		
	}
	
	public function memberSingle() {
		$client = new Client('AC23289f8d8a0e5a87ae60349eb24ee1b8', 'fe80215bdc30955d0be7a06d18351fbb'); 

		// Retrieve the member
		$member = $client->chat
		    ->services("IScef31cfda2374e4aa4f3f9eb4678a377")
		    ->channels("CHbc97deebdd134dcea03ce18fa2ecd4a8")
		    ->members("MB229400a872574ed9b82f20886fa7cf5b")
		    ->fetch();

		echo "<pre>";
		print_r($member);
		echo "</pre>";
	}
	
	
	public function twilioworkflowcallback() {
		
		if($_POST['EventType'] == 'task.created') {
			$newChat = ChatConversation::WithTaskID($_POST['ResourceSid']);
			$newChat -> NotifyAssignedUser();
		}
		
		
	}
	
	public function messages() {

		// Initialize the client
		$client = new Client('AC23289f8d8a0e5a87ae60349eb24ee1b8', 'fe80215bdc30955d0be7a06d18351fbb');
		
		//Retrieve the messages
		$messages = $client->chat
		    ->services("IScef31cfda2374e4aa4f3f9eb4678a377")
		    ->channels("CH6f4836fb6e964ee7b7a74e3fd26bca9c")
		    ->messages
		    ->read();
		
		foreach($messages as $msgSingle) {
			
			echo '<pre>';
			print_r($msgSingle);
			//$dateTime = new $msgSingle -> dateCreated();
			//print_r($msgSingle -> dateCreated -> date);
			
			
			
			//$date = new DateTime($msgSingle -> dateCreated, new DateTimeZone('America/Chicago'));
			
			//$msgSingle -> dateCreated -> setTimezone(new DateTimeZone('America/Chicago'));
			//echo $msgSingle -> dateCreated -> format('F j, Y / g:i a'); 
			
			
			//echo $msgSingle -> dateCreated -> format('Y-m-d H:i:s');	
			echo '</pre>';
			
			//$attribtes = json_decode($msgSingle -> attributes, true); 
			//echo "<div>{$attribtes['taskID']}</div>";
		}
		
		//echo "<pre>";
		//print_r($messages);
		//echo "</pre>";
		
	}

	public function twilioevents() {
		$account_sid = 'AC23289f8d8a0e5a87ae60349eb24ee1b8'; 
		$auth_token = 'fe80215bdc30955d0be7a06d18351fbb'; 
		$client = new Client($account_sid, $auth_token); 
 
		$events = $client->taskrouter->v1->workspaces("WS47103271c50b41b49342a83c28ec6f4a") ->events->read(); 
 
 		echo "<pre>";
		print_r($events);
		echo "</pre>";
 
	}
	
	public function websocketconnect() {
		
	}
	
	public function currentWorkers() {
		$client = new Client('AC23289f8d8a0e5a87ae60349eb24ee1b8', 'fe80215bdc30955d0be7a06d18351fbb'); 

		// Retrieve the member
		$currentWorkers = $client->taskrouter -> workspaces("WS47103271c50b41b49342a83c28ec6f4a") -> workers -> read();
		
		echo "<pre>";
		print_r($currentWorkers);
		echo "</pre>";
	}
	
	public function shoppingcartalgoliapush() {
		$ecommerceProducts = new EcommerceProductList();
		foreach($ecommerceProducts -> CurrentInventorLive() as $productSingle) {
			$photos = new EcommerceProductPhotosList();
			$mainProductPhoto = $photos -> GetProductMainPhoto($productSingle['productID']);
			
			$algoliaObj = new AlgoliaHandler();
			$algoliaObj -> ecommerceProductID = $productSingle['productID'];
			$algoliaObj -> ecommerceProductPrice = $productSingle['productPrice'];
			$algoliaObj -> ecommerceProductName = $productSingle['productName'];
			$algoliaObj -> ecommerceProductSEOName = $productSingle['productSEOName'];
			$algoliaObj -> ecommerceProductStoreID = $productSingle['productRelatedStore'];
			$algoliaObj -> ecommerceProductImage = PHOTO_URL . 'ecommerce/' . $productSingle['productFolderName'] . '/'  . $mainProductPhoto['PhotoName'] . '-l.' . $mainProductPhoto['PhotoExt'];
			$algoliaObj -> AddUpdateShoppinInventory();
		}
	}
	
	public function newchat() {
		header('Content-Type: text/event-stream');
		header("Cache-Control: no-cache"); // Prevent Caching
		$currentChatList = new ChatConversationsList();
		//echo "count: " . count($currentChatList -> All());
		//echo "data: " . count($currentChatList -> All());
		//echo "data: This is a second line!\n\n";
			
		//echo "data: Hello World!\n";
		//echo "data: This is a second line!\n\n";
		echo "data: " . count($currentChatList -> All()) . "\n\n";
		echo "retry: 2000\n";
		ob_flush();
    	flush();	
	}

}
?>