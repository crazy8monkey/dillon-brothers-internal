<?php


class Communication extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> css = array(PATH . 'public/css/CommunicationController.css');
	}
	
	public function index() {
		$textMessages = new TextMessageList();
		$conversatios = new ChatConversationsList();
		$this -> view -> blueBackground = "";
		$this -> view -> textMessages = $textMessages -> All();
		$this -> view -> chatconversations = $conversatios -> All();
		$this -> view -> js = array(PATH . 'public/js/CommunicationController.js', PATH . "public/js/maskedInput.js");
		$this -> view -> title = "Communication Portal";
		$this -> view -> render('communication/index');	
	}
	
	public function create($type) {
		switch($type) {
			case "customer":
				$settingsObj = SettingsObject::Init();
				$smsNumbers = new SettingSMSNumberList();
				$this -> view -> title = "Create Outbound Text";
				$this -> view -> blueBackground = "";
				$this -> view -> settings = $settingsObj;
				$this -> view -> js = array(PATH . 'public/js/CommunicationController.js', PATH . "public/js/maskedInput.js");
				$this -> view -> smsnumbers = $smsNumbers -> All();
				$this -> view -> startJsFunction = array('CommunicationController.InitializeOutboundText();');
				$this -> view -> render('communication/createCustomer');
				break;
		}
	}	
	
	public function view($type, $id) {
		switch($type) {
			case "customer":
				$smsNumbers = new SettingSMSNumberList();
				$this -> view -> title = "Customer Detail";
				$this -> view -> blueBackground = "";
				$this -> view -> TextMessage = TextMessageConversationCustomer::WithID($id);
				$this -> view -> smsnumbers = $smsNumbers -> All();
				$this -> view -> js = array(PATH . 'public/js/CommunicationController.js', PATH . "public/js/maskedInput.js");
				$this -> view -> startJsFunction = array('CommunicationController.InitializeOutboundText();');
				$this -> view -> render('communication/viewCustomer');
				break;
			case "chatconversation":
				$channelConvoSingle = ChatConversation::WithID($id);
				$this -> view -> title = "Chat Conversation Detail";
				$this -> view -> blueBackground = "";
				$this -> view -> js = array(PATH . 'public/js/CommunicationController.js', PATH . "public/js/maskedInput.js", 'https://media.twiliocdn.com/sdk/js/chat/v1.0/twilio-chat.js');
				$this -> view -> conversationDetail = ChatConversation::WithID($id);
				$this -> view -> startJsFunction = array('CommunicationController.InitializeConversation("' . $channelConvoSingle -> UniqueChannelName. '", "' . $channelConvoSingle -> TwilioTaskID . '", "test");');
				$this -> view -> render('communication/viewChatConversation');
				break;
		}
	}
	
	
	public function save($type, $id = null) {
		switch($type) {
			case "customer":
				if(isset($id)) {
					$textMessageConvo = TextMessageConversationCustomer::WithID($id);
					$textMessageConvo -> NewMessage = $_POST['TextMessageValue'];
				} else {
					$textMessageConvo = new TextMessageConversationCustomer();	
					
					$textMessageConvo -> firstName = $_POST['customerFirstName'];
					$textMessageConvo -> lastname = $_POST['customerLastName'];
					$textMessageConvo -> phoneNumber = $_POST['customerPhoneNumber'];					
				}
				
				
				if($textMessageConvo -> Validate()) {
					$textMessageConvo -> SaveCustomer();	
				}
				break;	
				
			case "outboundtext":
				$textMessageConvo = TextMessageConversationCustomer::WithID($id);
				if(isset($_POST['TextMessageValue'])) {
					$textMessageConvo -> NewMessage = $_POST['TextMessageValue'];	
				}
				
				$textMessageConvo -> StoreDept = $_POST['storeDeptSelection'];
				
				if($textMessageConvo -> ValidateTextMessage()) {
					$textMessageConvo -> SendMessage();	
				}
				
				break;
			
		}
	}
	
	public function closeconversation($id) {
		$channelConvoSingle = ChatConversation::WithID($id);
		$channelConvoSingle -> CloseConversation();
		echo json_encode(array('conversationclosed' => true));
	}
	
	
	public function getconversation($id) {
		$messages = new ChatConversationsMessagesList();
		echo json_encode($messages -> ByChatConversation($id));
	}
	
	public function chattoken() {
		$twilio = new TwilioHandler();
		$twilio -> ChatIdentityToken = $_GET['UserName'];
		$twilio -> DeviceID = $_GET['device'];
		echo json_encode($twilio -> GenerateAccessToken());
	}
	
	
	
	

}
?>