<?php


class Settings extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$this -> view -> blueBackground = "";
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		$this -> view -> adminLogin = "";	
	}
	
	public function index() {
		$this -> view -> title = "Settings";
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> render('settings/index');
	}
	
	public function users() {
		$users = new UserList();
		$this -> view -> usersSelected = '';
		$this -> view -> title = "Users: Settings";
		$this -> view -> users = $users -> AllUsers($_SESSION['user'] -> _userId);
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> render('settings/users/list');
	}
	
	public function socialmedia() {
		$this -> view -> title = "Social Media: Settings";
		$this -> view -> socialMediaSelected = '';
		$this -> view -> socialMediaSettings = SettingsObject::Init();
		$this -> view -> startJsFunction = array('SettingsController.InitializeSocialMediaForm();');
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> render('settings/socialmedia/socialmedia');
	}
	
	public function specifications() {
		$this -> view -> title = "Specifications: Settings";
		$specs = new SpecList();
		$inventoryCategories = new InventoryCategoryList();
		$this -> view -> specSelected = '';
		$this -> view -> inventorySettings = SettingsObject::Init();
		$this -> view -> specGroups = $specs -> AllSpecGroups();
		$this -> view -> specLabels = $specs -> TotalLabelsOrder();
		$this -> view -> inventorycategories = $inventoryCategories -> AllCategoriesNoGrouped();
		$this -> view -> js = array('https://code.jquery.com/ui/1.12.1/jquery-ui.js', PATH . "public/js/SettingsController.js");
		$this -> view -> css = array('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeInventoryForm();');
		$this -> view -> render('settings/specs/list');
	}
	
	public function inventorycategories() {
		$this -> view -> title = "Inventory Categories: Settings";
		$inventoryCategories = new InventoryCategoryList();
		$this -> view -> inventoryCategoriesSelected = '';
		$this -> view -> categories = $inventoryCategories -> AllCategories();
		$this -> view -> title = "Inventory Categories: Settings";	
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeInventoryForm();');
		$this -> view -> render('settings/inventory-categories/list');
	}
	
	public function inventorycolors() {
		$generic = new GenericColorsList();
		$this -> view -> title = "Inventory Colors: Settings";
		$this -> view -> inventoryColorsSelected = '';
		$colorList = new ColorsList();
		$this -> view -> colors = $colorList -> FilteredColors();
		$this -> view -> genericColors = $generic -> All();
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeInventoryForm();');
		$this -> view -> render('settings/inventory-colors/list');
		
	}

	public function stores() {
		$stores = new StoreList();
		$this -> view -> title = "Stores: Settings";
		$this -> view -> storesSelected = '';
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.StoreEmailNotifications();');
		$this -> view -> storesSettings = $stores -> AllStores();
		$this -> view -> render('settings/stores/list');
	}
	
	public function photos() {
		$this -> view -> title = "photos: Settings";
		$this -> view -> photosSelected = '';
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> settings = SettingsObject::Init();
		$this -> view -> render('settings/photos/list');
	}
	
	public function manufactureconversions() {
		$rules = new ConversionRulesList();
		$this -> view -> dataconversion = DataConversion::WithID('16');
		$this -> view -> rules = $rules -> ConversionRulesList('16');
		$this -> view -> manufactureConversionSelected = '';
		$this -> view -> title = "Edit Manufacture Conversions";
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeNewDataConversion();');
		$this -> view -> render('settings/dataconversions/manufacture/dataconversionsManufacture');
	}
		
	public function specmapping() {
		$conversions = new ConversionRulesList();
		$this -> view -> specMappingSelected = '';
		$this -> view -> title = "Settings: Spec Mapping Conversions";
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> SpecMappingRules = $conversions -> SpecLabelRulesList();
		
		$this -> view -> render('settings/dataconversions/specmapping/dataconversionsSpec');
	}
	
	public function donation() {
		$this -> view -> title = "Settings: Donation Settings";
		$this -> view -> donationSettingsSelected = '';
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeDonationSettings();');
		$this -> view -> settings = SettingsObject::Init();
		$this -> view -> render('settings/donation');
	}
	
	public function redirects() {
		$shortURL = new ShortUrlList();
		$blogPosts = new BlogPostList();
		$eventList = new EventsList();
		$this -> view -> title = "Settings: Redirect Settings";
		$this -> view -> shortUrlSettingsSelected = '';
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeShortURLSettings();');
		$this -> view -> shortURLS = $shortURL -> All();
		$this -> view -> blogposts = $blogPosts -> AllBlogPlosts();
		$this -> view -> events = $eventList -> AllEvents();
		$this -> view -> render('settings/shorturl/list');
	}
	
	public function sms() {
		$stores = new StoreList();
		$smsNumbers = new SettingSMSNumberList();
		$this -> view -> title = "Settings: SMS";
		$this -> view -> smsSettingsSelected = '';
		$this -> view -> stores = $stores -> AllStores();
		$this -> view -> smsnumbers = $smsNumbers -> All();
		$this -> view -> settingsInfo = SettingsObject::init();
		$this -> view -> js = array(PATH  . 'public/js/maskedInput.js', PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeSMSSettings();');
		$this -> view -> render('settings/sms/index');
	}
	
	public function chat() {
		$chatChannels = new ChatChannelsList();
		$chatUsers = new ChatUsersList();
		$stores = new StoreList();
		$this -> view -> title = "Settings: Chat";
		$this -> view -> chatSettingsSelected = '';
		$this -> view -> chatChannels = $chatChannels -> All();
		$this -> view -> chatUsers = $chatUsers -> All();
		$this -> view -> stores = $stores -> AllStores();
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeChatChannel();');
		$this -> view -> render('settings/chat/index');
	}
	
	public function adduserstochat($id) {
		$users = new UserList();
		$stores = new StoreList();
		$this -> view -> title = "Viewing Chat Channel";
		$this -> view -> stores = $stores -> AllStores();
		$this -> view -> chatchanneldetails = TwilioChatChannel::WithID($id);
		$this -> view -> usersList = $users -> All();
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeSingleChatChannel();');
		$this -> view -> render('settings/chat/singlechannel');
	}
	
	
	public function create($type, $id = null) {
		switch($type) {
			case "user":
				$this -> view -> title = "Create User";
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.InitializeUserForm();');
				$this -> view -> render('settings/users/create');
				break;
			case "color";
				$generic = new GenericColorsList();
				$this -> view -> title = "Create Color";
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> startJsFunction = array('SettingsController.ColorSinglePage();');
				$this -> view -> genericColors = $generic -> All();
				$this -> view -> render('settings/inventory-colors/create');
				break;
			case "inventorycategory":
				$this -> view -> title = "New Inventory Category";
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.InventoryCategoryForm();');
				$this -> view -> render('settings/inventory-categories/createCategory');
				break;
			case "specgroup":
				$this -> view -> title = "New Spec Group";
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.NewSpecGroupForm();');
				$this -> view -> render('settings/specs/createSpecGroup');
				break;
			case "speclabel":
				$inventoryCategories = new InventoryCategoryList();
				$groups = new SpecsLabelsGroupList();
				$this -> view -> title = "New Spec Label";
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.SpecLabelForm();');
				$this -> view -> specGroups = $groups -> AllGroups();
				$this -> view -> inventorycategories = $inventoryCategories -> AllCategories();
				$this -> view -> render('settings/specs/createSpecLabel');
				break;
			case "specmapping":
				$labelList = new SpecList();
				$this -> view -> title = "New Data Conversion";
				$this -> view -> SpecList = $labelList -> SpecLabelsByGroup();
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.InitializeNewDataConversion();');
				$this -> view -> render('settings/dataconversions/specmapping/createSpecMapping');
				break;
			case "genericcolor":
				$this -> view -> title = "New Generic Color";
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.GenericColorSingle();');
				$this -> view -> render('settings/inventory-colors/generic/create');
				break;
			case "shorturl":
				$blogPosts = new BlogPostList();
				$eventList = new EventsList();
				$this -> view -> title = "New Redirect Rule";
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.InitializeShortUrlSingle();');
				$this -> view -> blogposts = $blogPosts -> AllBlogPlosts();
				$this -> view -> events = $eventList -> AllEvents();
				$this -> view -> render('settings/shorturl/create');
				break;
		}
		
	}
	

	

	public function edit($type, $id) {
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		switch($type) {
			case "user":
				$this -> view -> title = "Edit User";
				$this -> view -> UserSingle = User::WithID($id);
				$this -> view -> startJsFunction = array('SettingsController.InitializeUserForm();');
				$this -> view -> render('settings/users/edit');
				break;
			case "specmapping":
				$rules = new ConversionRulesList();
				$this -> view -> title = "Edit Data Conversion";
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> dataconversion = DataConversion::WithID($id);
				$this -> view -> rules = $rules -> ConversionRulesList($id);
				$this -> view -> startJsFunction = array('SettingsController.InitializeNewDataConversion();');
				$this -> view -> render('settings/dataconversions/specmapping/editDataConversion');
				break;
			case "label":
				$inventoryCategories = new InventoryCategoryList();
				$groups = new SpecsLabelsGroupList();
				$this -> view -> title = "Edit Label";
				$this -> view -> singlelabel = SpecLabel::WithID($id);
				$this -> view -> specGroups = $groups -> AllGroups();
				$this -> view -> inventorycategories = $inventoryCategories -> AllCategories();
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.SpecLabelForm();');
				$this -> view -> render('settings/specs/editSpecLabel');
				break;
			case "inventorycategory":
				$this -> view -> title = "Edit Inventory Category";
				$this -> view -> categorySingle = InventoryCategoryObject::WithID($id);
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.InventoryCategoryForm();');
				$this -> view -> render('settings/inventory-categories/editCategory');
				break;
			case "specgroup":
				$labels = new SpecList();
				$this -> view -> title = "Edit Label Group";
				$this -> view -> group = SpecGroup::WithID($id);
				$this -> view -> labels = $labels -> LabelsByGroup($id);
				$this -> view -> js = array('https://code.jquery.com/ui/1.12.1/jquery-ui.js', PATH . "public/js/SettingsController.js");
				$this -> view -> css = array('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.SpecGroupForm();');
				$this -> view -> render('settings/specs/editSpecGroup');
				break;
			case "color":
				$generic = new GenericColorsList();
				$colorConversions = new ColorConversionsList();
				$this -> view -> title = "Edit Color";
				$this -> view -> color = ColorObject::WithID($id);
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> startJsFunction = array('SettingsController.ColorSinglePage();');
				$this -> view -> CurrentConversions = $colorConversions -> RulesByColor($id);
				$this -> view -> genericColors = $generic -> All();
				$this -> view -> render('settings/inventory-colors/editColor');
				break;
			case "store":
				$this -> view -> title = "Edit Store Settings";
				$this -> view -> blueBackground = "";
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> storeDetails = Store::WithID($id);
				$this -> view -> startJsFunction = array('SettingsController.StoreEmailNotifications();');
				$this -> view -> render('settings/storeSettingSingle');
				break;
			case "genericcolor":
				$this -> view -> title = "Edit Generic Color";
				$this -> view -> color = GenericColorObject::WithID($id);
				$this -> view -> js = array(PATH . "public/js/SettingsController.js");
				$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
				$this -> view -> startJsFunction = array('SettingsController.GenericColorSingle();');
				$this -> view -> render('settings/inventory-colors/generic/edit');
				break;
		}
	}
	
	public function selectcolor($id) {
		
	}
	
	
	public function save($type, $id = NULL) {
		switch($type) {
			case "socialmedia":
				$settings = SettingsObject::Init();
				$settings -> FacebookList = $_POST['settingsFacebook'];
				$settings -> TwitterList = $_POST['settingsTwitter'];
				$settings -> EmailSummary = $_POST['settingsSocialMediaEmail'];
				$settings -> UrgentEmail = $_POST['settingsUrgentPosts'];
				
				if($settings -> ValidateSocialMedia()) {
					$settings -> Save("socialmedia");	
				}
				
				break;
			case "inventory":
				$settings = SettingsObject::Init();
				$settings -> InventorySpecNotifications = $_POST['settingsInventory'];
				if($settings -> ValidateInventory()) {
					$settings -> Save("inventory");	
				}
				
				break;
			case "specgroup":
				if(isset($id)) {
					$group = SpecGroup::WithID($id);	
					if(isset($_POST['labelID'])) {
						$group -> labelIDS = $_POST['labelID'];	
					}
					$group -> Visible = $_POST['groupVisible'];
				} else {
					$group = new SpecGroup();
					$group -> Visible = 0;
				}
				$group -> SpecGroupText = $_POST['groupText'];
				if($group -> Validate()) {
					$group -> Save();	
				}
				
				break;
			case 'specorder':
				$groupOrder = new SpecGroup();
				$groupOrder -> groupID = $_POST['specGroupID'];
				$groupOrder -> reorderGroups();
				
				break;
				
			case "label":
				if(isset($id)) {
					$specLabel = SpecLabel::WithID($id);	
				} else {
					$specLabel = new SpecLabel();
				}
				
				$specLabel -> groupID = $_POST['GroupID'];	
				
				if(isset($_POST['specLabelCategory'])) {
					$specLabel -> LabelCategories = $_POST['specLabelCategory'];	
				}
				
				$specLabel -> labelText = $_POST['labelText'];
				if($specLabel -> Validate()) {
					$specLabel -> Save();	
				}
				
				break;
			case "labelorder":
				$specLabel = new SpecLabel();
				$specLabel -> labelID = $_POST['labelIDS'];
				$specLabel -> reorderLabels();
				break;
			case "conversion":
				if(isset($id)) {
					$conversionType = DataConversion::WithID($id);
					$conversionType -> RuleIDS = $_POST['ConversionRuleID'];
					
					if($conversionType -> CurrentConversionType == 1) {
						$conversionType -> ConversionActions = $_POST['ConversionAction'];
						$conversionType -> KeyWordSearch = $_POST['keywordSearchFind'];	
					}
					
					if($conversionType -> CurrentConversionType == 3) {
						$conversionType -> CurrentManufactureTexts = $_POST['CurrentManufactureText'];
						
						$conversionType -> NewManufactureTexts = $_POST['NewManufactureText'];
						
					}
					
				} else {
					$conversionType = new DataConversion();	
					$conversionType -> ItemType = $_POST['SpecLabelItem'];
					$conversionType -> NewConversionType = $_POST['ConversionType'];
				}
				
				if($conversionType -> Validate()) {
					$conversionType -> Save();
				}
				
				
				break;
			case "color":
				if(isset($id)) {
					$color = ColorObject::WithID($id);
				} else {
					$color = new ColorObject();	
				}
				
				$color -> ColorText = $_POST['ColorText'];
				
				if(isset($_POST['filterColorText'])) {
					$color -> FilterText = $_POST['filterColorText'];
				}
				
				if($color -> Validate()) {
					$color -> Save();	
				}
				break;
			case "conversioncolor":
				
				if(isset($_POST['ConversionRuleID'])) {
					$rule = ConversionRule::WithID($_POST['ConversionRuleID']);	
				} else {
					$rule = new ConversionRule();
					$rule -> dataConversionID = $_POST['DataConversionID'];
				}
				
				$rule -> CurrentColorText = $_POST['CurrentText'];
				if(isset($_POST['colorID'])) {
					$rule -> colorIDS = $_POST['colorID'];	
				}
				
				
				if($rule -> Validate()) {
					if(isset($_POST['ConversionRuleID'])) {
						$rule -> SaveColors();	
					} else {
						$rule -> NewColor();	
					}
					
				}
				
				break;
			case "inventorycategory":
				if(isset($id)) {
					$inventoryCategory = InventoryCategoryObject::WithID($id);
				} else {
					$inventoryCategory = new InventoryCategoryObject();	
				}
				
				$inventoryCategory -> ParentCategoryText = $_POST['InventoryParentCategoryText'];
				$inventoryCategory -> CategoryText = $_POST['InventoryCategoryText'];
				
				
				if($inventoryCategory -> Validate()) {
					$inventoryCategory -> Save();
				}
				break;
			case "storeNotifications":
				$store = Store::WithID($id);
				$store -> ContactUsEmail = $_POST['contactUsEmail'];
				$store -> ScheduleTestRide = $_POST['OnlineOfferEmail'];
				$store -> OnlineOffer = $_POST['ScheduleTestRideEmail'];
				$store -> TradeValue = $_POST['TradeValueEmail'];
				$store -> sendToFriendEmail = $_POST['sendToFriendEmail'];
				$store -> ServicDeptEmail = $_POST['ServiceDeptNotifications'];
				$store -> PartsDeptEmail = $_POST['PartsDeptNotifications'];
				$store -> ApparelDeptEmail = $_POST['ApparelDeptNotifications'];
				
				$store -> NewWaterMarkName = $_FILES['storeWaterMarkPhoto']['name'];
				$store -> NewWaterMarkTempName = $_FILES['storeWaterMarkPhoto']['tmp_name'];
				
				
				if($store -> ValidateNotifications()) {
					$store -> SaveNotifications();
				}
				
				break;
			case "storeWaterMark":
				$store = Store::WithID($id);
				
				
				if($store -> ValidateWaterMark()) {
					$store -> SaveWaterMark();
				}
				
				break;
				
				
			case "applicationsettings":
				$settings = SettingsObject::Init();
				$settings -> NewWaterMarkImageName = $_FILES['mainWaterMarkPhoto']['name'];
				$settings -> NewWaterMarkImageTemp =  $_FILES['mainWaterMarkPhoto']['tmp_name'];
				
				if($settings -> ValidateApplicationSettings()) {
					$settings -> Save("WaterMark");	
				}
				break;
			case "donation":
				$settings = SettingsObject::Init();
				$settings -> DonationEmail = $_POST['DonationEmail'];
				
				if($settings -> ValidateDonation()) {
					$settings -> Save("Donation");
				}
				break;
			case "user":
				if(isset($id)) {
					$user = User::WithID($id);	
				} else {
					$user = new User();
				}
				
				$user -> FirstName = $_POST['userFirstName'];
				$user -> LastName = $_POST['userLastName'];
				$user -> Email = $_POST['userEmail'];
				
				$user -> ProfileImageName = $_FILES['userProfilePic']['name'];
				$user -> ProfileImageTempName = $_FILES['userProfilePic']['tmp_name'];
				
				if(isset($_POST['stores'])) {
					$user -> AssociatedStores = $_POST['stores'];	
				}
				
				
				$user -> SetPassword($_POST['userPassword']);
				
				
				if(isset($_POST['EventsViewing'])) {
					$user -> SetPermission('EventsReadOnlyOrEdit', $_POST['EventsViewing']);	
				}
				
				if(isset($_POST['PhotoAddEditDelete'])) {
					$user -> SetPermission('PhotoManagementAddEditDelete', $_POST['PhotoAddEditDelete']);	
				}
				
				if(isset($_POST['NewsletterAddEditDelete'])) {
					$user -> SetPermission('NewsletterAddEditDelete', $_POST['NewsletterAddEditDelete']);	
				}
				
				if(isset($_POST['SpecialsAddEditDelete'])) {
					$user -> SetPermission('SpecialsAddEditDelete', $_POST['SpecialsAddEditDelete']);	
				}
				
				if(isset($_POST['SpecialOEMCategoryVisible'])) {
					$user -> SetPermission('SpecialOEMCategoryVisible', $_POST['SpecialOEMCategoryVisible']);	
				}
				
				if(isset($_POST['SpecialMotorclothesCategoryVisible'])) {
					$user -> SetPermission('SpecialMotorclothesCategoryVisible', $_POST['SpecialMotorclothesCategoryVisible']);	
				}
				
				if(isset($_POST['SpecialPartsCategoryVisible'])) {
					$user -> SetPermission('SpecialPartsCategoryVisible', $_POST['SpecialPartsCategoryVisible']);	
				}
				
				if(isset($_POST['SpecialServiceCategoryVisible'])) {
					$user -> SetPermission('SpecialServiceCategoryVisible', $_POST['SpecialServiceCategoryVisible']);	
				}

				if(isset($_POST['SpecialStoreCategoryVisible'])) {
					$user -> SetPermission('SpecialStoreCategoryVisible', $_POST['SpecialStoreCategoryVisible']);	
				}
				
				if(isset($_POST['EmployeesAddEditDelete'])) {
					$user -> SetPermission('EmployeesAddEditDelete', $_POST['EmployeesAddEditDelete']);	
				}
				
				if(isset($_POST['BlogAddEditDelete'])) {
					$user -> SetPermission('BlogAddEditDelete', $_POST['BlogAddEditDelete']);	
				}

				if(isset($_POST['BlogAddEditCategories'])) {
					$user -> SetPermission('BlogAddEditCategories', $_POST['BlogAddEditCategories']);	
				}

				if(isset($_POST['BlogApproveDeclineComments'])) {
					$user -> SetPermission('BlogApproveDeclineComments', $_POST['BlogApproveDeclineComments']);	
				}

				if(isset($_POST['InventoryViewing'])) {
					$user -> SetPermission('InventoryViewing', $_POST['InventoryViewing']);	
				}

				if(isset($_POST['InventorySpecificationManagement'])) {
					$user -> SetPermission('InventorySpecificationManagement', $_POST['InventorySpecificationManagement']);	
				}
				
				if(isset($_POST['SocialMediaPermissionType'])) {
					$user -> SetPermission('SocialMediaPermissionType', $_POST['SocialMediaPermissionType']);	
				}
				
				if(isset($_POST['SettingsViewEdit'])) {
					$user -> SetPermission('SettingsViewEdit', $_POST['SettingsViewEdit']);	
				}

				if(isset($_POST['SettingsUsers'])) {
					$user -> SetPermission('SettingsUsers', $_POST['SettingsUsers']);	
				}
				
				if(isset($_POST['SettingsSocialMedia'])) {
					$user -> SetPermission('SettingsSocialMedia', $_POST['SettingsSocialMedia']);	
				}
				
				if(isset($_POST['SettingsSpecs'])) {
					$user -> SetPermission('SettingsSpecs', $_POST['SettingsSpecs']);	
				}
				
				if(isset($_POST['SettingsInventoryCateories'])) {
					$user -> SetPermission('SettingsInventoryCateories', $_POST['SettingsInventoryCateories']);	
				}	
				
				if(isset($_POST['SettingsInventoryColors'])) {
					$user -> SetPermission('SettingsInventoryColors', $_POST['SettingsInventoryColors']);	
				}
				
				if(isset($_POST['SettingsStoreSettings'])) {
					$user -> SetPermission('SettingsStoreSettings', $_POST['SettingsStoreSettings']);	
				}
				
				if(isset($_POST['SettingsPhotoSettings'])) {
					$user -> SetPermission('SettingsPhotoSettings', $_POST['SettingsPhotoSettings']);	
				}
				
				if(isset($_POST['SettingsConversionsManufacture'])) {
					$user -> SetPermission('SettingsConversionsManufacture', $_POST['SettingsConversionsManufacture']);	
				}
				
				if(isset($_POST['SettingsSpecLabelMapping'])) {
					$user -> SetPermission('SettingsSpecLabelMapping', $_POST['SettingsSpecLabelMapping']);	
				}
				
				if(isset($_POST['ShoppingCartAccess'])) {
					$user -> SetPermission('ShoppingCartAccess', $_POST['ShoppingCartAccess']);	
				}
				
				if(isset($_POST['SettingsDonations'])) {
					$user -> SetPermission('SettingsDonations', $_POST['SettingsDonations']);	
				}
				
				if(isset($_POST['SettingsShortUrls'])) {
					$user -> SetPermission('SettingsShortUrls', $_POST['SettingsShortUrls']);	
				}


				if(isset($_POST['leadsViewing'])) {
					$user -> SetPermission('leadsViewing', $_POST['leadsViewing']);	
				}

				
				if($user -> Validate()) {
					$user -> Save();
				}		
				break;
			case "colorconversions";
				$colorconverions = new ColorConversion();
				if(isset($_POST['colorConversionRule'])) {
					$colorconverions -> ColorID = $id;
					$colorconverions -> colorConversionID = $_POST['colorConversionRule'];
					$colorconverions -> Text = $_POST['colorPhraseFind'];	
				}
				
				if($colorconverions -> Validate()) {
					$colorconverions -> Save();	
				}
				
				break;
			case "genericcolor":
				if(isset($id)) {
					$genericColor = GenericColorObject::WithID($id);	
				} else {
					$genericColor = new GenericColorObject();
				}
				
				$genericColor -> NewColorText = $_POST['genericColorText'];
				
				if($genericColor -> Validate()) {
					$genericColor -> Save();
				}
				
				break;
			case "shorturl":
				if(isset($id)) {
					$shortLink = ShortLinks::WithID($id);
				} else {
					$shortLink = new ShortLinks();
				}
				
				$shortLink -> SettingIsActive = $_POST['ShortURLActive'];
				$shortLink -> SettingRedirectAction = $_POST['RedirectString'];
				$shortLink -> SettingShortURL = $_POST['shortURLText'];
				$shortLink -> RedirectIfInactive = $_POST['RedirectString'];
				$shortLink -> SettingsRedirectedContent = $_POST['redirectedContent'];
				
				if($shortLink -> ValidateSettingsSide()) {
					$shortLink -> SettingsSaveShortURL();
				}
				
				break;
			case "storesms":
				$sms = new SettingSMSNumber();
				$sms -> SMSDeptID = $_POST['smsNumberID'];
				$sms -> SalesSMSNumber = $_POST['settingsSaleDeptNumber'];
				$sms -> ServiceSMSNumber = $_POST['settingsServiceDeptNumber'];
				$sms -> PartsSMSNumber = $_POST['settingsPartsDeptNumber'];
				if($sms -> ValidateSMS()) {
					$sms -> Save('storesms');
				}
				break;
			case "outbounddisclaimer":
				$settings = SettingsObject::Init();
				$settings -> SMSDisclosure = $_POST['OutboundTextDisclaimer'];
				
				if($settings -> ValidateSMSDisclaimer()) {
					$settings -> Save("SMSDisclaimer");	
				}
				break;
			case "newchannel":
				$chatChannel = new TwilioChatChannel();
				$chatChannel -> FriendlyName = $_POST['newChatChannelName'];
				
				if(isset($_POST['relatedStoreID'])) {
					$chatChannel -> RelatedStoreID = $_POST['relatedStoreID'];
				}
				
				if($chatChannel -> Validate()) {
					$chatChannel -> Save();
				}
				break;
			case "chatusers":
				$chatChannel = TwilioChatChannel::WithID($id);
				if(isset($_POST['channelUsers'])) {
					$chatChannel -> UserIDS = $_POST['channelUsers'];	
				}
				$chatChannel -> UserNames = $_POST['userName'];
				$chatChannel -> RelatedStoreID = $_POST['relatedStoreID'];
				
				if($chatChannel -> Validate()) {
					$chatChannel -> SaveUsers();	
				}
				break;
		}
	}

	public function savenewchannel() {
		$chatChannel = new TwilioChatChannel();
		$chatChannel -> FriendlyName = $_POST['newChatChannelName'];
				
		if(isset($_POST['relatedStoreID'])) {
			$chatChannel -> RelatedStoreID = $_POST['relatedStoreID'];
		}
				
		if($chatChannel -> Validate()) {
			$chatChannel -> Save();
		}
	}

	public function feedback($id) {
		$feedBack = new FeedBackObject;
		$feedBack -> FeedBackText = $_POST['internalFeedBackNotes'];
		$feedBack -> itemID = $id;
		$feedBack -> itemType = 2;
		
		
		if($feedBack -> Validate()) {
			$feedBack -> Save();
		}
	}
	public function delete($type, $id) {
		switch($type) {
			case "specgroup":
				$group = SpecGroup::WithID($id);
				$group -> Delete();
				break;	
			case "speclabel":
				$specLabel = SpecLabel::WithID($id);
				$specLabel -> Delete();
				break;
			case "dataconversion":
				$dataConversion = DataConversion::WithID($id);
				$dataConversion -> Delete();
				break;
			case "user":
				$user = User::WithID($id);	
				$user -> Delete();
				break;
			case "colorconversion":
				$colorconverion = ColorConversion::WithID($id);
				$colorconverion -> Delete();
				break;
			case "color":
				$color = ColorObject::WithID($id);
				$color -> Delete();
				break;
			case "genericcolor":
				$genericColor = GenericColorObject::WithID($id);	
				$genericColor -> Delete();
				break;
			case "chatchannel":
				$channel = TwilioChatChannel::WithID($id);	
				$channel -> Delete();		
				break;
		}
		
	}
		


}
?>