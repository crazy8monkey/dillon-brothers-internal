<?php


class Inventory extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/InventoryController.js");
		$this -> view -> css = array(PATH . 'public/css/InventoryController.css');
	}
	
	public function index() {
		$this -> view -> title = "Inventory Portal";
		$this -> view -> blueBackground = "";
		$this -> view -> render('inventory/index');
	}
	
	public function currentinventory($type = NULL, $pageNumber = NULL) {
		$inventory = new CurrentInventoryList();
		$colorConverted = new ColorConversionsList();
		$this -> view -> blueBackground = "";
		
		if($type != NULL) {
			$this -> view -> startJsFunction = array('InventoryController.InitializeStoreInventory();');
			switch($type) {
				case "motorsports":
					$this -> view -> title = "Current Motorsport Inventory";
					$this -> view -> PageTitle = "Current Motorsport Inventory";
					$this -> view -> currentInventoryList = "Motor Sport Inventory";
					$this -> view -> storeID = "2";
					if(isset($_GET['SortBy'])) {
						$this -> view -> InventoryList = $inventory -> MotorSportInventory($pageNumber, $_GET['SortBy']);	
					} else {
						$this -> view -> InventoryList = $inventory -> MotorSportInventory($pageNumber);
					}
					
					
					break;
				case "harleydavidson":
					$this -> view -> title = "Current Harley-Davidson Inventory";
					$this -> view -> PageTitle = "Current Harley-Davidson Inventory";
					$this -> view -> currentInventoryList = "Harley-Davidson Inventory";
					$this -> view -> storeID = "3";
					if(isset($_GET['SortBy'])) {
						$this -> view -> InventoryList = $inventory -> HarleyInventory($pageNumber, $_GET['SortBy']);	
					} else {
						$this -> view -> InventoryList = $inventory -> HarleyInventory($pageNumber);	
					}
					
					break;
				case "indian":
					$this -> view -> title = "Current Indian Inventory";
					$this -> view -> PageTitle = "Current Indian Inventory";
					$this -> view -> currentInventoryList = "Indian Inventory";
					$this -> view -> storeID = "4";
					if(isset($_GET['SortBy'])) {
						$this -> view -> InventoryList = $inventory -> IndianInventory($pageNumber, $_GET['SortBy']);
					} else {
						$this -> view -> InventoryList = $inventory -> IndianInventory($pageNumber);	
					}
					
					break;
			}	
			$this -> view -> convertedcolors = $colorConverted -> ConvertedColors();
			$this -> view -> render('inventory/storeSingleInventoryList');
		} else {
			$this -> view -> title = "Select Store Inventory";	
			$this -> view -> render('inventory/currentInventoryList');
		}
		
	}
	
	public function backup($pageNumber) {
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventorySpecificationManagement') == 0) {
			$this -> redirect -> redirectPage(PATH. 'inventory');
		}
		
		$backupVins = new BackupVinsList();
		if(!isset($pageNumber)) {
			$this -> redirect -> redirectPage(PATH. 'inventory/backup/1');
		}
		$inventoryBackup = new BackupInventoryList();
		$labels = new SpecList();
		$categories = new InventoryCategoryList();
		$backupList = new BackupInventoryList();
		$this -> view -> title = "Inventory Specifications Backup";
		$this -> view -> blueBackground = "";
		$this -> view -> labels = $labels -> TotalLabels();
		$this -> view -> orphanedVins = $backupVins -> OrphanedVinNumbers();
		$this -> view -> filterCategories = $categories -> AllCategories();
		$this -> view -> ReassignBackupList = $backupList -> BackupList();
		
		$filterArray = array();
		if(isset($_GET['SearchQuery'])) {
			$filterArray['Query'] = $_GET['SearchQuery'];	
		}
		
		if(isset($_GET['category'])) {
			$filterArray['category'] = $_GET['category'];
		}
		
		if(isset($_GET['FilteredBy'])) {
			$filterArray['FilteredBy'] = $_GET['FilteredBy'];
		}
		
		
		$this -> view -> backupList = $inventoryBackup -> AllBackupList($pageNumber, $filterArray);
		$this -> view -> startJsFunction = array('InventoryController.InitializeBackupInventory();');
		$this -> view -> render('inventory/backupInventoryList');
	}
	
	public function create() {
		$inventoryCategories = new InventoryCategoryList();
		$this -> view -> title = "Create Inventory";
		$stores = new StoreList();
		$this -> view -> blueBackground = "";
		$this -> view -> inventorycategories = $inventoryCategories -> AllCategories();
		$this -> view -> stores = $stores -> AllStores();
		$this -> view -> startJsFunction = array('InventoryController.InitializeInventorySingle();');
		$this -> view -> render('inventory/createInventory');
	}
	
	public function edit($id) {
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventoryViewing') == 0) {
			$this -> redirect -> redirectPage(PATH. 'inventory/viewinventory/' . $id);
		}
			
		$specs = new SpecList();
		$photos = new InventoryPhotosList();
		$inventoryCategories = new InventoryCategoryList();
		$feedBack = new FeedBackList();
		$colorConverted = new ColorConversionsList();
		$colorList = new ColorsList();
		
		$currentInventoryItem = InventoryObject::WithID($id);
		
		$this -> view -> title = "Edit Vehicle/Inventory";	
		$this -> view -> blueBackground = "";
		$this -> view -> InventorySingle = InventoryObject::WithID($id);
		$this -> view -> photos = $photos -> ByID($id);
		$this -> view -> SpecsList = $specs -> SpecSettings();
		$this -> view -> inventorycategories = $inventoryCategories -> AllCategories();
		$this -> view -> convertedcolors = $colorConverted -> ConvertedColors();
		$this -> view -> currentColorList = $colorList -> AllColors();
		$this -> view -> feedBackList = $feedBack -> ByItem($id, 1);
		$this -> view -> startJsFunction = array('InventoryController.InitializeInventorySingle();');
		if($currentInventoryItem -> IsDummyInventoryFlag == 0) {
			$this -> view -> render('inventory/talonEditInventoryForm');	
		} else {
			$stores = new StoreList();
			$this -> view -> stores = $stores -> AllStores();
			$this -> view -> render('inventory/editDummyInventory');	
		}
		
	}

	public function viewinventory($id) {
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventoryViewing') == 1) {
			$this -> redirect -> redirectPage(PATH. 'inventory/edit/' . $id);
		}
		
		$specs = new SpecList();
		$photos = new InventoryPhotosList();
		$inventoryCategories = new InventoryCategoryList();
		$feedBack = new FeedBackList();
		$colorConverted = new ColorConversionsList();
		
		$currentInventoryItem = InventoryObject::WithID($id);
		
		$this -> view -> title = "Viewing Vehicle/Inventory";	
		$this -> view -> blueBackground = "";
		$this -> view -> InventorySingle = InventoryObject::WithID($id);
		$this -> view -> photos = $photos -> ReadOnly($id);
		$this -> view -> SpecsList = $specs -> SpecSettings();
		
		$this -> view -> specgroups = $specs -> SpecGroups();
		$this -> view -> speclabels = $specs -> LabelsByCategory($currentInventoryItem -> Category);
		$this -> view -> convertedcolors = $colorConverted -> ConvertedColors();
		
		$this -> view -> feedBackList = $feedBack -> ByItem($id, 1);
		$this -> view -> startJsFunction = array('InventoryController.InitializeInventorySingle();');
		$this -> view -> render('inventory/viewSingleInventory');	
	}
	
	public function addinventoryphotos($id) {
		$this -> view -> title = "Add Photos To Inventory";	
		$this -> view -> InventorySingle = InventoryObject::WithID($id);
		$this -> view -> render('inventory/addphotos', true);
	}
	
	public function editphoto($id) {
		$this -> view -> title = "Edit Inventory Photo";	
		$this -> view -> photoSingle = InventoryPhotos::WithID($id);
		$this -> view -> render('inventory/inventoryPhotoSingle', true);
	}
	
	public function newbackup() {
		$inventoryCategories = new InventoryCategoryList();
		$this -> view -> inventorycategories = $inventoryCategories -> AllCategories();
		$this -> view -> title = "New Backup Inventory";
		$this -> view -> blueBackground = "";
		$this -> view -> render('inventory/createBackup');
	}
	
	public function editbackup($id) {
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('InventorySpecificationManagement') == 0) {
			$this -> redirect -> redirectPage(PATH. 'inventory');
		}
		$specs = new SpecList();
		$vins = new BackupVinsList();
		$inventoryCategories = new InventoryCategoryList();
		$feedBack = new FeedBackList();
		$logs = new EditHistoryLogList();
		$backupList = new BackupInventoryList();
		$this -> view -> title = "Edit Backup Inventory";	
		$this -> view -> blueBackground = "";
		$this -> view -> BackupInventorySingle = InventoryBackup::WithID($id);
		$this -> view -> SpecsList = $specs -> SpecSettings();
		$this -> view -> inventorycategories = $inventoryCategories -> AllCategories();
		$this -> view -> feedBackList = $feedBack -> ByItem($id, 2);
		$this -> view -> logs = $logs -> LogsByBackupItem($id);
		$this -> view -> backupList = $backupList -> BackupList($id);
		$this -> view -> startJsFunction = array('InventoryController.BackupInventorySingle();');
		$this -> view -> vins = $vins -> ByBackedupID($id);
		$this -> view -> render('inventory/editBackup');
	}

	public function filteredinventory($store) {
		$inventory = new CurrentInventoryList();
		$this -> view -> startJsFunction = array('InventoryController.InitializeStoreInventory();');	
		$this -> view -> title = "Your Results";	
		$this -> view -> filteredResultsPage = "";
		$storeID = NULL;
		switch($store) {
			case "motorsports":
				$this -> view -> currentInventoryList = "Motor Sport Inventory";
				$this -> view -> currentinventorypage = 'motorsports';
				$storeID = 2;
				break;
			case "harleydavidson":
				$this -> view -> currentInventoryList = "Harley-Davidson Inventory";
				$this -> view -> currentinventorypage = 'harleydavidson';
				$storeID = 3;
				break;
			case "indian":		
				$this -> view -> currentInventoryList = "Indian Inventory";
				$this -> view -> currentinventorypage = 'indian';
				$storeID = 4;
				break;
		}	
		
		$this -> view -> storeID = $storeID;	
		
		$this -> view -> selectedValue = $_GET['value'];
		
		$this -> view -> InventoryList = $inventory -> FilteredInventory($storeID, $_GET['value']);
		$this -> view -> render('inventory/storeSingleInventoryList');	
		
	}

	
	public function save($type, $id = NULL) {
		switch($type) {
			case "current":
				$currentInventory = InventoryObject::WithID($id);
				$currentInventory -> NewMSRP = $_POST['inventoryMSRP'];
				$currentInventory -> NewMileage = $_POST['inventoryMileage'];
				$currentInventory -> Year = $_POST['inventoryYear'];
				$currentInventory -> Manufacture = $_POST['inventoryManufactur'];
				$currentInventory -> NewModel = $_POST['inventoryModel'];
				$currentInventory -> ModelFriendlyName = $_POST['inventoryModelFriendlyName'];
				$currentInventory -> SpecJSON = $_POST['SpecJSON'];
				$currentInventory -> NewInventoryActiveOption = $_POST['inventoryActive'];
				$currentInventory -> NewCategory = $_POST['InventoryCategoryID'];
				$currentInventory -> InventoryDescription = $_POST['inventoryDescription'];
				$currentInventory -> OverlayText = $_POST['inventoryOverlayText'];
				$currentInventory -> IsFeatureProduct = $_POST['inventoryFeature'];
				//$currentInventory -> IsPowerEquipment = $_POST['inventoryPowerEquipment'];
				$currentInventory -> IsInService = $_POST['inventoryInService'];
				$currentInventory -> YoutubeURL = $_POST['inventoryYoutubeURL'];
				
				if($currentInventory -> Validate()) {
					$currentInventory -> Save();	
					//$currentInventory -> UpdateAlgoStatus($id);
					//$currentInventory -> UpdateAlgoIndexItem();
					$currentInventory -> JSONRedirect();
				}
				
				break;
			case "backup":
				if(isset($id)) {
					$backup = InventoryBackup::WithID($id);	
				} else {
					$backup = new InventoryBackup();
				}
				
				$backup -> inventoryBackupYear = $_POST['inventoryYear'];
				$backup -> inventoryBackupManufactur = $_POST['inventoryManufactur'];
				$backup -> NewModelNumber = $_POST['inventoryModel'];
				$backup -> inventoryBackupModelFriendlyName = $_POST['inventoryModelFriendlyName'];
				
				$backup -> NewCategory = $_POST['InventoryCategoryID'];
				$backup -> NewEngizeCC = $_POST['inventoryEngineSizeCC'];
				$backup -> newDescription = $_POST['inventoryBackupDescription'];
				
				if(isset($id)) {
						
					$backup -> backupNewSpecs = $_POST['SpecJSON'];	
					$backup -> Update();	
					$backup -> RedirectToBackup();	
				} else {
					$backup -> SaveBackup();
				}
				
				break;
			case "addinventoryphotos":
				$inventoryPhoto = new InventoryPhotos();
				$inventoryPhoto -> inventoryID = $id;
				$inventoryPhoto -> photosTemp = $_FILES['file']['tmp_name'];
				$inventoryPhoto -> photoName = $_FILES['file']['name'];
				
				$inventoryPhoto -> Upload();				
				break;
			case "inventoryphoto":
				$inventoryPhoto = InventoryPhotos::WithID($id);
				$inventoryPhoto -> newPhotoName = $_POST['photoName'];
				$inventoryPhoto -> altTag = $_POST['altText'];
				$inventoryPhoto -> titleTag = $_POST['titleText'];
				$inventoryPhoto -> mainPhotoCheck = $_POST['mainInventoryPhoto'];
				
				if($inventoryPhoto -> Validate()) {
					$inventoryPhoto -> Save();	
				}
				break;
			case "dummyinventory":
				if(isset($id)) {
					$currentInventory = InventoryObject::WithID($id);
					$currentInventory -> IsFeatureProduct = $_POST['inventoryFeature'];
					$currentInventory -> InventoryActive = $_POST['inventoryActive'];
					$currentInventory -> SpecJSON = $_POST['SpecJSON'];
				} else {
					$currentInventory = new InventoryObject();
				}
				
				$currentInventory -> NewMSRP = $_POST['inventoryMSRP'];
				$currentInventory -> NewMileage = $_POST['inventoryMileage'];
				$currentInventory -> Year = $_POST['inventoryYear'];
				$currentInventory -> Manufacture = $_POST['inventoryManufactur'];
				$currentInventory -> Model = $_POST['inventoryModel'];
				$currentInventory -> ModelFriendlyName = $_POST['inventoryModelFriendlyName'];
				$currentInventory -> NewCategory = $_POST['InventoryCategoryID'];
				$currentInventory -> InventoryDescription = $_POST['inventoryDescription'];
				$currentInventory -> OverlayText = $_POST['inventoryOverlayText'];
				$currentInventory -> YoutubeURL = $_POST['inventoryYoutubeURL'];
				$currentInventory -> StoreID = $_POST['storeLocation'];
				$currentInventory -> EngineSizeCC = $_POST['inventoryEngineSizeCC'];
				$currentInventory -> NewCleanColor = $_POST['inventoryColorString'];
				$currentInventory -> Conditions = $_POST['condition'];
				
				if($currentInventory -> Validate("Manual")) {
					$currentInventory -> SaveDummy();	
				}
				
				break;
			case "inventorycolors":
				$currentInventory = InventoryObject::WithID($id);
				$currentInventory -> NewConvertedColors = $_POST['convertedColorID'];
				$currentInventory -> NewConvertedText = $_POST['SavedTalonColor'];
				$currentInventory -> SaveConvertedColors();
				break;
			case "newconversions":
				$currentInventory = InventoryObject::WithID($id);
				$currentInventory -> SaveNewColorConversions();
				break;
			case "reassignvin":
				$vinNumber = new BackupVinNumber();
				$vinNumber -> id = $_POST['vinIDObject'];
				$vinNumber -> inventoryBackupInventoryID = $_POST['reassignVin'];
				if($vinNumber -> Validate()) {
					$vinNumber -> MakeDecision();	
				}
				break;
			case "vinnumberassign":
				$backup = InventoryBackup::WithID($id);
				$backup -> AssociatedVinOptions = $_POST['reassignVinDecision'];
				
				if($backup -> ReassignValidate()) {
					$backup -> ReassignVinNumbers();	
				}
				break;
		}
	}

	public function feedback($id) {
		$feedBack = new FeedBackObject;
		$feedBack -> FeedBackText = $_POST['internalFeedBackNotes'];
		$feedBack -> itemID = $id;
		$feedBack -> itemType = 2;
		
		
		if($feedBack -> Validate()) {
			$feedBack -> Save();
		}
		
		
	}


	public function unlock($column, $id) {
		$currentInventory = InventoryObject::WithID($id);
		$currentInventory -> UnlockColumn = $column;
		$currentInventory -> UnLockColumn();
	}

	public function rotate($direction, $id) {
		$inventoryPhoto = InventoryPhotos::WithID($id);
		$inventoryPhoto -> rotation = $direction;
		$inventoryPhoto -> RotateImage();
	}
	
	public function applywatermark($type, $id) {
		switch($type) {
			case "single":
				$inventoryPhoto = InventoryPhotos::WithID($id);
				$inventoryPhoto -> WaterMarkSingle();
				$inventoryPhoto -> RedirectToPhotoSingle();
				break;
			case "all":
				$inventoryPhotos = new InventoryPhotos();
				$inventoryPhotos -> RelatedInventoryID = $id;
				$inventoryPhotos -> WaterMarkAllPhotosByInventory();
				break;
		}
		
	}

	public function delete($type, $id) {
		switch($type) {
			case "inventoryphoto":
				$inventoryPhoto = InventoryPhotos::WithID($id);
				$inventoryPhoto -> Delete();
				break;
			case 'inventorybackup':
				$backup = InventoryBackup::WithID($id);
				$backup -> AssociatedVinOptions = $_POST['reassignVin'];
				
				if($backup -> DeleteValidate()) {
					$backup -> Delete();	
				}
				
				break;
			case "vin":
				$vinNumber = new BackupVinNumber();
				$vinNumber -> id = $id;
				$vinNumber -> Delete();
				$vinNumber -> Redirect();
				break;
			case "dummyinventory":
				$currentInventory = InventoryObject::WithID($id);
				$currentInventory -> Delete();
				break;
		}
	}

	public function GetSpecifications($id) {
		$getSpecs = InventoryObject::WithID($id);
		$getSpecs -> GetSpecificationsSingle();
		$getSpecs -> RedirectToInventoryEdit();
	}
	
	public function GetBackupHistory($id) {
		$logs = new EditHistoryLogList();
		
		$editHistory = array();
		foreach($logs -> LogsByBackupItem($id) as $log) {
			$editedTime = explode('T', $log['edittedTimeAndDate']);
			array_push($editHistory, array('value' => $log['firstName'] . ' ' . $log['lastName'] . ' - ' . $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1])));	
		}
		
		echo json_encode($editHistory);
	}

	public function filterinventory() {
		$currentInventory = new InventoryObject();
		$currentInventory -> StoreID = $_POST['storeID'];
		$currentInventory -> filterValue = $_POST['FilteredText'];
		
		if($currentInventory -> ValidateFilters('Current')) {
			$currentInventory -> FilteredInventoryPage();	
		}
		
	}
	
	public function VerifiedBackup($id) {
		$backup = InventoryBackup::WithID($id);	
		$backup -> vehicleVerified = $_POST['verified'];
		$backup -> UpdateVerificationStatus();
	}
	
	public function filter($type) {
		switch($type) {
			case "backup":
				$inventory = new InventoryObject();
				
				$inventory -> Text = $_POST['BackupFilterResult'];
				$inventory -> category = $_POST['InventoryCategoryID'];   
				
				$inventory -> BackupEmptyEngie = $_POST['filterByOptions'];
				//EmptyCC
				
				//if($inventory -> ValidateFilters('Backup')) {
					$inventory -> FilteredBackupList();	
				//}
				
				break;
			case "current":
				break;
		}
	}
	
	public function BackupInfo($id) {
		$vins = new BackupVinsList();
		
		echo json_encode(array('savedBike' => InventoryBackup::WithID($id),
							   'VINS' => $vins -> ByBackedupID($id)));
		
	}


	public function GetCurrentInventory() {
		$inventory = new CurrentInventoryList();
		echo json_encode(array('storeInventory' => $inventory -> GetCurrentInventory($_GET['storeID'])));
	}
}
?>