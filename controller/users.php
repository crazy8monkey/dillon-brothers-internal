<?php


class Users extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		
		$loggedIn = Session::get('UserLoggedIn');
		//$currentUser = Session::get('user');
		//echo "<pre>";
		//print_r($currentUser);
		//echo "</pre>";
		//echo $currentUser -> _userId;
		//$_SESSION[''] -> GetUserId();
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/UsersController.js");
		$this -> view -> css = array(PATH . 'public/css/UsersController.css');
	}
	
	public function index() {
		Session::init();
		
		$users = new UserList();
		$this -> view -> title = "Users";
		$this -> view -> users = $users -> AllUsers($_SESSION['user'] -> _userId);
		$this -> view -> render('users/list');	
	}
	

	public function create() {
		$this -> view -> title = "Create User";
		$this -> view -> startJsFunction = array('UsersController.InitializeUserForm();');
		$this -> view -> render('users/create');
	}
	
	public function edit($id) {
		
	}
	
	public function delete($id) {
		
		
	}
	
	public function save($id = NULL) {
		if(isset($id)) {
			$user = User::WithID($id);	
		} else {
			$user = new User();
		}
		
		$user -> FirstName = $_POST['userFirstName'];
		$user -> LastName = $_POST['userLastName'];
		$user -> Username = $_POST['userNameLogin'];
		$user -> Email = $_POST['userEmail'];
		
		$user -> ProfileImageName = $_FILES['userProfilePic']['name'];
		$user -> ProfileImageTempName = $_FILES['userProfilePic']['tmp_name'];
		
		if(isset($_POST['stores'])) {
			$user -> AssociatedStores = $_POST['stores'];	
		}
		
		
		$user -> SetPassword($_POST['userPassword']);
		
		$user -> SetPermission('PhotoManagement', $_POST['permissionPhotos']);
		$user -> SetPermission('PhotoUploadVisible', $_POST['permissionPhotoVisible']);
		$user -> SetPermission('Blog', $_POST['permissionBlog']);
		$user -> SetPermission('Events', $_POST['permissionEvents']);
		$user -> SetPermission('Newsletters', $_POST['permissionNewsletters']);
		$user -> SetPermission('SocialMedia', $_POST['permissionSocialMedia']);
		$user -> SetPermission('SocialMediaSummary', $_POST['permissionSocialMediaSummary']);
		$user -> SetPermission('Specials', $_POST['permissionSpecials']);
		$user -> SetPermission('Employees', $_POST['permissionEmployees']);
		$user -> SetPermission('Settings', $_POST['permissionSettings']);
		
		if($user -> Validate()) {
			$user -> Save();
		}		
	}



}
?>