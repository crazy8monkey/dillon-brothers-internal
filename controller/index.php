<?php


class Index extends Controller {
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/LoginController.js");
		$this -> view -> css = array(PATH . 'public/css/IndexController.css');
	}
	
	
	public function index() {
		$this -> view -> title = "Admin Login: Dillon Brothers";
		$this -> view -> startJsFunction = array('LoginController.InitializeLogin();');
		$this -> view -> render('login/login');		
	}
	


}
?>