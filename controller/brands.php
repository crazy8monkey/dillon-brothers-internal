<?php


class Brands extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/BrandsController.js", PATH . "public/js/ckeditor/ckeditor.js");
		$this -> view -> css = array(PATH . 'public/css/BrandsController.css');
	}
	
	public function index() {
		$brands = new BrandsList();
		$this -> view -> title = "Brands List";
		$this -> view -> blueBackground = "";
		$this -> view -> brandsList = $brands -> AllOEMAndPartApparelBrands();
		$this -> view -> render('brands/list');
	}
	
	public function view($type, $id) {
		switch($type) {
			case "OEM":
				$content = new BrandContentList();
				$brandSingle = Brand::WithID($id);
				$this -> view -> blueBackground = "";
				$this -> view -> title = $brandSingle -> _brandName . ' Information';
				$this -> view -> OEMBrandSingle = $brandSingle;
				$this -> view -> OEMContent = $content -> ByOEMBrand($id);
				$this -> view -> startJsFunction = array('BrandsController.InitializeBrandSingle();');
				$this -> view -> render('brands/singleBrand');
				break;
			case "partapparel":
				$content = new BrandContentList();
				$brandSingle = PartsAccessoryBrand::WithID($id);
				$this -> view -> blueBackground = "";
				$this -> view -> PartApparelContent = '';
				$this -> view -> title = $brandSingle -> PartsAccessoryBrandName . ' Information';
				$this -> view -> PartApparelContent = $brandSingle;
				$this -> view -> PartApparelContentList = $content -> ByPartApparelBrand($id);
				$this -> view -> startJsFunction = array('BrandsController.InitializeBrandSingle();');
				$this -> view -> render('brands/singleBrand');
				break;
			case "pagecontent":
				$brands = new BrandsList();
				$this -> view -> blueBackground = "";
				$this -> view -> title = 'Edit Brand Content';
				$this -> view -> brandContent = BrandContent::WithID($id);
				$this -> view -> brandsList = $brands -> AllOEMAndPartApparelBrands();
				$this -> view -> startJsFunction = array('BrandsController.InitializeLandingPageSingle();');
				$this -> view -> render('brands/editContent');
				break;
		}
	}
	
	public function create($type, $id = null) {
		switch($type) {
			case "oemlandingpage":
				$brandSingle = Brand::WithID($id);
				$brands = new BrandsList();
				$this -> view -> blueBackground = "";
				$this -> view -> OEMCreate = '';
				$this -> view -> title = "Create " . $brandSingle -> _brandName . " Landing Page";
				$this -> view -> OEMBrandSingle = $brandSingle;
				$this -> view -> startJsFunction = array('BrandsController.InitializeLandingPageSingle();');
				$this -> view -> render('brands/createPage');
				break;
			case "partapparellandingpage":
				$partBrand = PartsAccessoryBrand::WithID($id);
				$this -> view -> blueBackground = "";	
				$this -> view -> PartBrandSingle = $partBrand;
				$this -> view -> PartBrandCreate = '';	
				$this -> view -> title = "Create " . $partBrand -> PartsAccessoryBrandName . " Landing Page";
				$this -> view -> startJsFunction = array('BrandsController.InitializeLandingPageSingle();');
				$this -> view -> render('brands/createPage');
				break;
			case "brand":
				
				$this -> view -> title = "New Parts/Accessories Brand";
				$this -> view -> blueBackground = "";	
				$this -> view -> startJsFunction = array('BrandsController.InitializeBrandSingle();');
				$this -> view -> render('brands/createBrand');
				break;
			//case "partapparellandingpage"
		}
	}
	
	
	
	
	public function save($type, $id = NULL) {
		switch($type) {
			case "landingpage":
				if(isset($id)) {
					$landingContent = BrandContent::WithID($id);
				} else {
					$landingContent = new BrandContent();
					$landingContent -> Brand = $_POST['brandContentSelect'];
				}
				
				$landingContent -> LandingPageName = $_POST['brandLandingPageName'];
				$landingContent -> Content = $_POST['brandLandingPageContent'];
				
				if($landingContent -> Validate()) {
					$landingContent -> Save();
				}
				
				break;
			case "partapparel":
				if(isset($id)) {
					$partsBrand = PartsAccessoryBrand::WithID($id);	
				} else {
					$partsBrand = new PartsAccessoryBrand();	
				}
				
				$partsBrand -> PartsAccessoryBrandName = $_POST['PartApparelBrandName'];
				$partsBrand -> PartsBrandImage = $_FILES['brandLogo']['name'];
				
				if(isset($_POST['apparelBrandDescription'])) {
					$partsBrand -> ApparelDescription = $_POST['apparelBrandDescription'];	
				}
				
				if(isset($_POST['partBrandDescription'])) {
					$partsBrand -> PartsDescription = $_POST['partBrandDescription'];	
				}
				
				$partsBrand -> BrandType = $_POST['BrandType'];
				
				if($partsBrand -> Validate()) {
					$partsBrand -> Save();
				}
				break;
			case "oembrand":
				$brand = Brand::WithID($id);
				$brand -> NewBrandName = $_POST['OEMbrandName'];
				$brand -> brandDescription = $_POST['brandDescription'];
				$brand -> NewBrandImage = $_FILES['brandLogo']['name'];
				
				if(isset($_FILES['alternateBrandLogo']['name'])) {
					$brand -> NewAlternateLogo = $_FILES['alternateBrandLogo']['name'];	
				}
			
				if($brand -> Validate()) {
					$brand -> Save();	
				}
				break;
		}	
		
	}
	
	public function publish($id) {
		$landingContent = BrandContent::WithID($id);
		$landingContent -> Publish();
	}
	
	public function unpublish($id) {
		$landingContent = BrandContent::WithID($id);
		$landingContent -> Unpublish();
	}
	
	public function uploadImages() {
		Session::init();
		$newImageName = date("Y", $this -> time -> NebraskaTime()) . 
						date("m", $this -> time -> NebraskaTime()) . 
						date("d", $this -> time -> NebraskaTime()) . 
						date("H", $this -> time -> NebraskaTime()) . 
						date("i", $this -> time -> NebraskaTime()) . 
						date("s", $this -> time -> NebraskaTime());
			
		$ImagePathParts = array();
		
		
		Image::MoveAndRenameImage($_FILES["upload"]["tmp_name"], 
								  $ImagePathParts,
								  $_FILES["upload"]["name"],
								  $newImageName, 
								  13);
		
		$funcNum = $_GET['CKEditorFuncNum'] ;
		$newFile = $_FILES["upload"]["name"];
			
		
		$url = PHOTO_URL . '/BrandLandingPageContent/' . $newImageName . '.' . Image::getFileExt($newFile);
		
		$message = '';
		echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
		
		
	}



}
?>