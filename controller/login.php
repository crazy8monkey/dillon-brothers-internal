<?php


class Login extends Controller {
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/LoginController.js");
		$this -> view -> css = array(PATH . 'public/css/IndexController.css');
	}
	
	//page
	public function index() {
		$this -> view -> title = "Admin Login: Dillon Brothers";
		$this -> view -> startJsFunction = array('LoginController.InitializeLogin();');
		$this -> view -> render('login/login');		
	}
	
	//page
	public function forgotpassword() {
		$this -> view -> title = "Forgot Password: Dillon Brothers";
		$this -> view -> startJsFunction = array('LoginController.InitializeEmailCheckForm();');
		$this -> view -> render('login/forgotPassword');		
	}
	
	public function newpassword() {
		$this -> view -> title = "Reset your Password: Dillon Brothers";
		if (isset($_GET['token']) && isset($_GET['userID'])) {
			$this -> view -> UserSingle = User::WithID($_GET['userID']);	
			$this -> view -> startJsFunction = array('LoginController.InitializePasswordResetForm();');	
		}	
		
		$this -> view -> render('login/passwordReset');
	}
	
	//post
	public function userlogin() {
		$userLogin = new User();
		$userLogin -> Email = $_POST['userEmailLogin'];
		$userLogin -> password = $_POST['userNamePassword'];
		$userLogin -> userLogin();
		
	} 
	
	//post
	public function emailCheck() {
		$user = new User();
		$user -> emailCheck = $_POST['userEmail'];
		
		if($user -> validateEmail()) {
			$user -> SendEmail();
		}
	}
	
	//post
	public function NewPasswordPost($id) {
		$user = User::WithID($id);
		$user -> SetPassword($_POST['userNamePassword']);
		if($user -> validatePassword()) {
			$user -> SaveNewPassword();
		}
		
	}

}
?>