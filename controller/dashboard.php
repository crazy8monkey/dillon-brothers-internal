<?php


class Dashboard extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/DashboardController.js");
		$this -> view -> css = array(PATH . 'public/css/DashboardController.css');
	}
	
	public function index() {
		Session::init();
		$this -> view -> blueBackground = "";
		$unapproveComments = new BlogPostCommentsList();
		$mediaList = new SocialMediaList();
		$inventoryList = new CurrentInventoryList();
		$analytics = new GoogleAnalytics();
		$stores = new StoreList();
		$reviews = new EcommerceProductReviewsList();
			
		$this -> view -> title = "Dillon Brothers Dashboard";	
		
		
		$showRightSiteContent = array();
		
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SocialMediaPermissionType') == 1) {
			$this -> view -> SocialMediaList = $mediaList -> SocialMediaListByUser($_SESSION['user'] -> _userId);
			
		}
		
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogApproveDeclineComments') == 1) {
			$this -> view -> unapprovecomments = $unapproveComments -> UnapproveComments();	
			array_push($showRightSiteContent, "ShowBlogComments");
		}
		
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SocialMediaPermissionType') == 0) {
			$this -> view -> AllSocialMediaList = $mediaList -> SocialMediaListsAllStores();
			array_push($showRightSiteContent, "AdminSocialMediaList");
		}
		
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('ShoppingCartAccess') == 1) {
			$this -> view -> NewReviews = $reviews -> Inactive();	
			array_push($showRightSiteContent, "ShoppingCartAccess");
		}
		
		$this -> view -> RightSideContent = $showRightSiteContent;
		$this -> view -> googleAnalytics = $analytics -> GetToken();
		$this -> view -> stores = $stores -> AllStores();
		
		$storeIDS = '';
		foreach($_SESSION["user"]->GetAssociatedStores() as $storeSingle) {
			$storeIDS .= $storeSingle['associatedStore'] . ',';	
		}
		
		$this -> view -> InventoryNeedPhotos = $inventoryList -> InventoryNeedsPhotos(rtrim($storeIDS, ','));
		$this -> view -> InventoryNeedsPricing = $inventoryList -> InventoryNeedsPricing(rtrim($storeIDS, ','));
		
		
		$this -> view -> StartOfWeek = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
		$this -> view -> EndOfWeek = date('Y-m-d',time()+( 6 - date('w'))*24*3600);
		
		
		$this -> view -> render('dashboard/index');	
	}

	public function getAnalytics() {
		$analytics = new GoogleAnalytics();
		echo json_encode($analytics -> GetAnalytics());
	}
	
	public function GetToken() {
		$analytics = new GoogleAnalytics();
		echo json_encode($analytics -> GetToken());
	}
	

}
?>