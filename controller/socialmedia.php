<?php


class SocialMedia extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/SocialMediaController.js");
		$this -> view -> css = array(PATH . 'public/css/SocialMediaController.css');
	}
	
	public function index() {
		$socialMediaList = new SocialMediaList();
		$this -> view -> blueBackground = "";
		
		$this -> view -> title = "Social Media Posts";
		$this -> view -> newposts = $socialMediaList -> NewPosts();
		$this -> view -> approvedposts = $socialMediaList -> ApprovedPosts();
		$this -> view -> declinedPosts = $socialMediaList -> DeclinedPosts();
		$this -> view -> postedposts = $socialMediaList -> PostedPosts();
		$this -> view -> mediaList = $socialMediaList -> AllPosts();
		$this -> view -> render('socialmedia/list');	
	}
	

	public function yourstore($id) {
		$store = Store::WithID($id);
		$settings = SettingsObject::Init();
		$socialMediaList = new SocialMediaList();
		$this -> view -> blueBackground = "";
		$this -> view -> title = $store -> storeName . ": Social Media List";
		$this -> view -> storeSingleInfo = $store;
		$this -> view -> requiredFacebook = $settings -> FacebookList;
		$this -> view -> requiredTwitter = $settings -> TwitterList;
		$this -> view -> totalSubmittedFacebook = $socialMediaList -> FacebookPostsByStore($id);
		$this -> view -> totalSubmittedTwitter  = $socialMediaList -> TwitterPostByStore($id);
		
		$this -> view -> render('socialmedia/storeSingle');
	}
	
	public function addpost($storeID, $type) {
		$mediaInt = NULL;	
		switch($type) {
			case "facebook":
				$mediaInt = 1;
				break;
			case "twitter":
				$mediaInt = 2;
				break;
		}
		
		
		$this -> view -> title = "New " . ucfirst($type) . " Post";
		$this -> view -> type = ucfirst($type);
		$this -> view -> storeID = $storeID;
		$this -> view -> startJsFunction = array('SocialMediaController.InitializePostForm("' . $type . '");');
		$this -> view -> mediaType = $mediaInt;
		$this -> view -> render('socialmedia/addpost');
	}

	public function save($id = NULL) {
		Session::init();	
		
		if(isset($id)) {
			$socialMedia = SocialMediaObject::WithID($id);
		} else {
			$socialMedia = new SocialMediaObject();	
			$socialMedia -> SocialMediaType = $_POST['SocialMediaType'];
			$socialMedia -> StoreID = $_POST['storeID'];
			$socialMedia -> UrgentDate = $_POST['socialMediaUrgent'];
			$socialMedia -> submittedBy = $_SESSION['user'] -> _userId;
		}
				
		$socialMedia -> PostContent = $_POST['socialMediaContent'];
		$socialMedia -> KeyWords = $_POST['socialMediaKeywords'];
		
		if($socialMedia -> Validate()) {
			$socialMedia -> Save();
			if(isset($id)) {
				$socialMedia -> RedirectToPost();	
			} else {
				$socialMedia -> RedirectToStore();	
			}
		}
		
	}
	
	public function view($type, $id) {
		switch($type) {
			case "store":
				$this -> view -> blueBackground = "";
				$store = Store::WithID($id);
				$settings = SettingsObject::Init();
				$socialMediaList = new SocialMediaList();
				
				$this -> view -> title = $store -> storeName . ": Social Media List";
				$this -> view -> storeSingleInfo = $store;
				$this -> view -> requiredFacebook = $settings -> FacebookList;
				$this -> view -> requiredTwitter = $settings -> TwitterList;
				$this -> view -> totalSubmittedFacebook = $socialMediaList -> FacebookPostsByStore($id);
				$this -> view -> totalSubmittedTwitter  = $socialMediaList -> TwitterPostByStore($id);
				
				$this -> view -> render('socialmedia/adminStoreSingle');
				break;
			case "post":
				$type = NULL;
				$this -> view -> blueBackground = "";
				$post = SocialMediaObject::WithID($id);
				
				switch($post -> SocialMediaType) {
					case 1:
						$type = "facebook";
						break;
					case 2:
						$type = "twitter";
						break;
				}
				
				
				$this -> view -> title = "Post Single";
				$this -> view -> PostSingle = $post;
				$this -> view -> startJsFunction = array('SocialMediaController.InitializePostForm("' . $type . '");');
				$this -> view -> render('socialmedia/adminPostSingle');
				break;
		}
	}
	
	public function approve($id) {
		$post = SocialMediaObject::WithID($id);
		$post -> ApprovePost();
	}
	
	public function decline($id) {
		$post = SocialMediaObject::WithID($id);
		$post -> SocialMediaType = $_POST['socialMediaType'];
		$post -> DeclineNotes = $_POST['declineNotes'];
		
		if($post -> ValidateDeclinePost()) {
			$post -> DeclinePost();	
		}
		
	}
	
	public function markasposted($id) {
		$post = SocialMediaObject::WithID($id);
		$post -> MarkAsPosted();
	}
	
	public function single($id) {
		echo json_encode(SocialMediaObject::WithID($id));
	}
	
	public function uploadImages($type) {
		$socialMedia = new SocialMediaObject();
		
		$path = NULL;
		switch($type) {
			case "facebook":
				$socialMedia -> SocialMediaType = 1;
				$path = "/Facebook/";
				break;
			case "twitter":
				$socialMedia -> SocialMediaType = 2;
				$path = "/Twitter/";
				break;
		}
		
		$newFile = $_FILES["upload"]["name"];
		$newImageName = date("Y-m-d", $this -> time -> NebraskaTime()) . '-' . date("H-i-s", $this -> time -> NebraskaTime());
		
		$socialMedia -> photoTemp = $_FILES["upload"]["tmp_name"];
		$socialMedia -> Photo = $_FILES["upload"]["name"];
		$socialMedia -> newName = $newImageName;
		
		$socialMedia -> UploadPostPhoto();
		
		$funcNum = $_GET['CKEditorFuncNum'];
		
		$url = PHOTO_URL . 'SocialMediaContent' . $path . $newImageName . '.'. Image::getFileExt($newFile);
		
		$message = '';
		echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
	}
	

}
?>