<?php


class Blog extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/BlogController.js");
		$this -> view -> css = array(PATH . 'public/css/BlogController.css');
	}
	
	public function index() {
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogAddEditDelete') == 0) {
			$this -> redirect -> redirectPage(PATH. 'dashboard');
		}
		
		$blogPosts = new BlogPostList();
		$this -> view -> blueBackground = "";
		$this -> view -> title = "Blog Posts";
		$this -> view -> blogpostlist = $blogPosts -> AllBlogPlosts();
		$this -> view -> render('blog/posts/list');
	}
	
	public function categories() {
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogAddEditCategories') == 0) {
			$this -> redirect -> redirectPage(PATH. 'blog');
		}
		
		$categoryList = new BlogCategoryList();
		$this -> view -> blueBackground = "";
		$this -> view -> title = "Blog Categories";
		$this -> view -> blogcategories = $categoryList -> AllBlogCategories();
		$this -> view -> render('blog/category/list');
	}
	
	public function comments() {
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('BlogApproveDeclineComments') == 0) {
			$this -> redirect -> redirectPage(PATH. 'blog');
		}
		
		$blogComments = new BlogPostCommentsList();
		$this -> view -> blueBackground = "";
		$this -> view -> enteredBlogs = $blogComments -> CommentsByBlogPost();
		$this -> view -> title = "Comments";
		$this -> view -> render('blog/comments/list');
	}
	
	public function create($type) {
		switch($type) {
			case "post":
				$this -> view -> blueBackground = "";
				$categories = new BlogCategoryList();
				$this -> view -> BlogCategories = $categories -> AllBlogCategories();
				$this -> view -> title = "New Blog Post";	
				
				
				$this -> view -> startJsFunction = array('BlogController.InitializeBlogPostSingle();');
				$this -> view -> render('blog/posts/create');				
				break;
				
			case "category":
				$this -> view -> title = "New Blog Category";
				$this -> view -> blueBackground = "";
				
				$this -> view -> startJsFunction = array('BlogController.InitializeBlogCategorySingle();');	
				$this -> view -> render('blog/category/create');
				break;
		}

	}
	
	public function edit($type, $id) {
		switch($type) {
			case "post":
				$this -> view -> blueBackground = "";
				$blogPost = BlogPost::WithID($id);
				$logs = new EditHistoryLogList();
				
				$photosList = new PhotosList();
				$categories = new BlogCategoryList();
				$this -> view -> title = "Edit Blog Post";	
				$this -> view -> blogPost = $blogPost;
				$this -> view -> BlogCategories = $categories -> AllBlogCategories();
				$this -> view -> photos = $photosList -> PhotosByAlbum($blogPost -> photoAlbumID);
				$this -> view -> logs = $logs -> LogsByBlog($id);
				$this -> view -> startJsFunction = array('BlogController.InitializeBlogPostSingle("' . $blogPost -> photoAlbumID . '");');
				$this -> view -> render('blog/posts/edit');
				break;
			case "category":
				$this -> view -> blueBackground = "";
				$this -> view -> title = "Edit Blog Category";	
				$this -> view -> blogCategory = BlogCategory::WithID($id);
				$this -> view -> startJsFunction = array('BlogController.InitializeBlogCategorySingle();');	
				$this -> view -> render('blog/category/edit');				
				break;
		}	
			
		
	}

	public function savepost($id = NULL) {
		
	}
	
	public function save($type, $id = NULL) {
		switch($type) {
			case "post":
				if(isset($id)) {
					$blogPost = BlogPost::WithID($id);
					$blogPost -> blogPostContent = $_POST['blogPostContent'];		
					$blogPost -> showUserNameOnPublish = $_POST['showBlogCreator'];
					$blogPost -> KeyWords = $_POST['blogkeyWords'];
					$blogPost -> OptionalURLSlug = $_POST['urlSlug'];
					
				} else {
					$blogPost = new BlogPost();
				}		
						
				if(isset($_POST['blogcategory'])) {
					$blogPost -> category = $_POST['blogcategory'];	
				}
						
				$blogPost -> blogPostName = $_POST['blogPostName'];
				
						
				if($blogPost -> Validate()) {
					$blogPost -> Save();
				}	
					
				break;
			case "publish":
				$blogPost = BlogPost::WithID($id);
				$blogPost -> Publish();
				break;
			case "unpublish":
				$blogPost = BlogPost::WithID($id);
				$blogPost -> UnPublish();
				break;
			case "category":
				if(isset($id)) {
					$blogCategory = BlogCategory::WithID($id);
				} else {
					$blogCategory = new BlogCategory();
				}		
				
				$blogCategory -> blogCategoryName = $_POST['blogCategoryName'];
				
				if($blogCategory -> Validate()) {
					$blogCategory -> Save();
				}		
				break;
			case "approvecomment":
				$comment = new BlogPostComment();
				$comment -> commentID = $id;
				$comment -> ApproveComment();
				break;
			
		}
		
				
	}


	public function delete($type, $id) {
		switch($type) {
			case "post":
				$blogPost = BlogPost::WithID($id);
				$blogPost -> Delete();		
				break;
			case "category":
				$blogCategory = BlogCategory::WithID($id);
				$blogCategory -> Delete();
				break;
			case "comment":
				$comment = new BlogPostComment();
				$comment -> commentID = $id;
				$comment -> DeleteComment();
				break;
		}
		
		
	}
	
	public function GetBlogEditHistory($id) {
		$logs = new EditHistoryLogList();
		
		$editHistory = array();
		foreach($logs -> LogsByBlog($id) as $log) {
			$editedTime = explode('T', $log['edittedTimeAndDate']);
			array_push($editHistory, array('value' => $log['firstName'] . ' ' . $log['lastName'] . ' - ' . $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1])));	
		}
		
		echo json_encode($editHistory);
	}
	
	public function getPhotos($albumID) {
		$photosList = new PhotosList();
		echo json_encode($photosList -> PhotosByAlbum($albumID));
	}
	
	public function SaveMainPhoto($blogID, $photoID) {
		$post = BlogPost::WithID($blogID);
		$post -> blogPhoto = $photoID;
		$post -> UpdateBlogMainPhoto();
	}
	
	public function uploadImages($albumID) {
		Session::init();
		$newImageName = date("Y-m-d", $this -> time -> NebraskaTime()) . '-' . date("H-i-s", $this -> time -> NebraskaTime()) . '-' . $blogName;	
			
		$photo = new Photo();
		
		$photoAlbum = PhotoAlbum::WithID($albumID);
		$photo -> AlbumID = $albumID;	
		
		
		$photo -> photoName = $newImageName;
		$photo -> fileExt = Image::getFileExt($_FILES['upload']['name']);
		$photo -> parentDirectory = $photoAlbum -> year;				
		$photo -> _albumFolderName = $photoAlbum -> AlbumFolderName;
		$photo -> AlbumType = '2';
		$photo -> altText = $blogName;
		$photo -> titleText = $blogName;
		$photo -> UploadedByID = $_SESSION['user'] -> _userId;
		$photo -> uploadedImage = $_FILES['upload']['name'];
		$photo -> uploadedTempImage = $_FILES['upload']['tmp_name'];
			
		$photo -> SavePhoto();	
			
		
		
		
		$funcNum = $_GET['CKEditorFuncNum'] ;
		$newFile = $_FILES["upload"]["name"];
			
		
		$url = PHOTO_URL . $photoAlbum -> year . '/blog/' . $photoAlbum -> AlbumFolderName . '/' . $newImageName . '-l.' . Image::getFileExt($newFile);
		
		$message = '';
		echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
		
		
	}

}
?>