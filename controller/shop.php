<?php


class shop extends Controller {
	
	private $_apparelList;	
	private $_apparelcategories;
	private $_sizes;
	private $_colors;
	private $_orders;	
	private $_productReviews;
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		$this -> view -> blueBackground = "";
		$this -> view -> adminLogin = "";
		$this -> _apparelList = new EcommerceProductList();	
		$this -> _apparelcategories = new EcommerceSettingCategoriesList();
		$this -> _sizes = new EcommerceSettingSizesList();
		$this -> _colors = new EcommerceSettingColorList();
		$this -> _orders = new EcommerceOrdersList();
		$this -> _productReviews = new ReviewList();
		$this -> view -> js = array(PATH . "public/js/ShopController.js");
		$this -> view -> css = array(PATH . 'public/css/ShopController.css');
	}
	
	public function index() {
		$this -> view -> title = "Shopping Center";
		$this -> view -> ThisYearSummary = $this -> _orders -> YearSummary(date('Y'));
		$this -> view -> render('shop/index');
	}
	
	public function products() {
		$this -> view -> title = "Products";
		$this -> view -> apparel = $this -> _apparelList -> CurrentInventory();
		$this -> view -> startJsFunction = array('ShopController.ProductsList();');
		$this -> view -> render('shop/products/list');
	}
	
	public function orders() {
		$this -> view -> title = "Orders";
		$this -> view -> orders = $this -> _orders -> OrdersList();
		$this -> view -> render('shop/orders/list');
	}
	
	public function reviews() {
		$this -> view -> title = "Submitted Reviews";
		$this -> view -> products = $this -> _apparelList -> CurrentInventory();
		$this -> view -> reviewList = $this -> _productReviews -> AllEcomerceProductReviews();
		$this -> view -> render('shop/reviews/list');
	}
	
	public function settings() {
		$this -> view -> title = "Shop Settings";
		$this -> view -> apparelcategories = $this -> _apparelcategories -> Categories();
		$this -> view -> sizes = $this -> _sizes -> Sizes();
		$this -> view -> coloroptions = $this -> _colors -> Colors();
		$this -> view -> startJsFunction = array('ShopController.EcommerceSettingsPage();');
		$this -> view -> ecommercesettings = EcommerceSettingGeneral::Init();
		$this -> view -> render('shop/settings/index');
	}
	
	public function addphotos($id) {
		$this -> view -> title = "Add Photos To Inventory";	
		$this -> view -> ApparelProduct = EcommerceProduct::WithID($id);
		$this -> view -> render('shop/products/addphotos', true);
	}
	
	public function editphoto($id) {
		$this -> view -> title = "Edit Photo";	
		$this -> view -> photoSingle = EcommerceProductPhoto::WithID($id);
		$this -> view -> render('shop/products/inventoryPhotoSingle', true);
	}
	
	public function vieworder($id) {
		$logs = new EditHistoryLogList();
		$this -> view -> title = "View Order";	
		$this -> view -> ordersingle = EcommerceOrder::WithID($id);
		$this -> view -> startJsFunction = array('ShopController.OrderSinglePage();');
		$this -> view -> logs = $logs -> LogsByEcommerceOrderItem($id);
		$this -> view -> render('shop/orders/singleorder');
	}
	
	public function create($type) {
		switch($type) {
			case "product":
				$this -> view -> title = "Create Product";
				$this -> view -> apparel = $this -> _apparelList -> CurrentInventory();
				$this -> view -> apparelcategories = $this -> _apparelcategories -> Categories();
				//$this -> view -> sizes = $this -> _sizes -> Sizes();
				//$this -> view -> coloroptions = $this -> _colors -> Colors();
				$this -> view -> startJsFunction = array('ShopController.ProductSingle();');
				$this -> view -> render('shop/products/create');
				break;
			case "category":
				$this -> view -> title = "Create Category";
				$this -> view -> startJsFunction = array('ShopController.CategorySingle();');
				$this -> view -> sizes = $this -> _sizes -> Sizes();
				$this -> view -> render('shop/settings/category/create');
				break;
			case "color":
				$this -> view -> title = "Create Color";
				$this -> view -> startJsFunction = array('ShopController.ColorSingle();');
				$this -> view -> render('shop/settings/color/create');
				break;
			case "size":
				$this -> view -> title = "Create Size";
				$this -> view -> startJsFunction = array('ShopController.SizeSingle();');
				$this -> view -> render('shop/settings/size/create');
				break;
		}	
	}
	
	public function edit($type, $id) {
		switch($type) {
			case "category":
				$this -> view -> title = "Edit Category";
				$this -> view -> startJsFunction = array('ShopController.CategorySingle();');
				$this -> view -> categorySingle = EcommerceSettingCategory::WithID($id);
				$this -> view -> sizes = $this -> _sizes -> Sizes();
				$this -> view -> render('shop/settings/category/edit');
				break;
			case "color":
				$this -> view -> title = "Edit Color";
				$this -> view -> startJsFunction = array('ShopController.ColorSingle();');
				$this -> view -> colorSingle = EcommerceSettingColor::WithID($id);
				$this -> view -> render('shop/settings/color/edit');
				break;
			case "size":
				$this -> view -> title = "Edit Size";
				$this -> view -> startJsFunction = array('ShopController.SizeSingle();');
				$this -> view -> sizeSingle = EcommerceSettingSize::WithID($id);
				$this -> view -> render('shop/settings/size/edit');
				break;
			case "product":
				$logs = new EditHistoryLogList();
				$currentPhotos = new EcommerceProductPhotosList();
				$productSingle = EcommerceProduct::WithID($id);
				$this -> view -> title = "Edit Product";
				$this -> view -> startJsFunction = array('ShopController.ProductSingle();');
				$this -> view -> logs = $logs -> LogsByEcommerceProductItem($id);
				$this -> view -> apparelcategories = $this -> _apparelcategories -> Categories();
				$this -> view -> sizes = $this -> _sizes -> SizesByCategory($productSingle -> Category);
				$this -> view -> productSingle = $productSingle;
				$this -> view -> productPhotos = $currentPhotos -> ByProduct($id);
				$this -> view -> render('shop/products/edit');
				break;
		}
	}
	
	public function filteredproducts($content) {
		$this -> view -> title = "Filtered Products";
		$this -> view -> apparel = $this -> _apparelList -> FilteredProducts($content);
		$this -> view -> filteredResultText = $content;
		$this -> view -> startJsFunction = array('ShopController.ProductsList();');
		$this -> view -> render('shop/products/list');
	}

	public function save($type, $id = null) {
		switch($type) {
			case "category":
				if(isset($id)) {
					$SettingCategory = EcommerceSettingCategory::WithID($id);
				} else {
					$SettingCategory = new EcommerceSettingCategory();	
				}
				
				$SettingCategory -> categoryName = $_POST['apparelCategoryname'];
				
				if(isset($_POST['sizes'])) {
					$SettingCategory -> SaveSizes = $_POST['sizes'];	
				}
				
				if($SettingCategory -> Validate()) {
					$SettingCategory -> Save();
				}
				
				break;
			case "color":
				if(isset($id)) {
					$SettingColor = EcommerceSettingColor::WithID($id);
				} else {
					$SettingColor = new EcommerceSettingColor();	
				}
				
				$SettingColor -> colorName = $_POST['settingColorName'];
				if($SettingColor -> Validate()) {
					$SettingColor -> Save();
				}
				break;
			case "size":
				if(isset($id)) {
					$SettingSize = EcommerceSettingSize::WithID($id);
				} else {
					$SettingSize = new EcommerceSettingSize();	
				}
				
				$SettingSize -> SizeAbbr = $_POST['apparelSizeAbbr'];
				$SettingSize -> SizeText = $_POST['apparelSizeText'];
				
				if($SettingSize -> Validate()) {
					$SettingSize -> Save();
				}
				break;
			case "product":
				if(isset($id)) {
					$Product = EcommerceProduct::WithID($id);	
					$Product -> StockNumber = $_POST['productStockNumber'];
					if(isset($_POST['productPartNumberSize'])) {
						$Product -> PartNumberSelected = $_POST['productPartNumberSize'];			
					}
				} else {
					$Product = new EcommerceProduct();		
				}
				
				$Product -> productName = $_POST['productName'];
				$Product -> MSRPPrice = $_POST['productMSRP'];
				$Product -> Price = $_POST['productPrice'];
				
				$Product -> Description = $_POST['productDescription'];
				$Product -> Category = $_POST['productCategory'];
				$Product -> NewFeaturedProduct = $_POST['featuredProduct'];
				
				if(isset($_POST['productSizeOption'])) {
					$Product -> Sizes = $_POST['productSizeOption'];	
				}
				
				
				if(isset($_POST['colorOption'])) {
					$Product -> Colors = $_POST['colorOption'];	
				}
				
				if($Product -> Validate()) {
					$Product -> Save();
				}
				
				break;
			case "productphoto":
				$ProductPhoto = new EcommerceProductPhoto();
				$ProductPhoto -> ProductID = $id;
				$ProductPhoto -> FileUploadName = $_FILES['file']['name'];
				$ProductPhoto -> FileUploadTemp = $_FILES['file']['tmp_name'];	
				$ProductPhoto -> Upload();
				
				break;
			case "photo":
				$ProductPhoto = EcommerceProductPhoto::WithID($id);
				$ProductPhoto -> newPhotoName = $_POST['photoName'];
				$ProductPhoto -> AltTag = $_POST['altText'];
				$ProductPhoto -> TitleTag = $_POST['titleText'];
				$ProductPhoto -> MainProductPhoto = $_POST['mainInventoryPhoto'];
	
				if($ProductPhoto -> Validate()) {
					$ProductPhoto -> Save();				
				}
				
				break;
			case "publish":
				$Product = EcommerceProduct::WithID($id);
				$Product -> publish();
				$Product -> RedirectToProduct();
				break;
			case "unpublish":
				$Product = EcommerceProduct::WithID($id);
				$Product -> unpublish();
				$Product -> RedirectToProduct();
				break;
			case "ecommercesettings":
				$EcommerceSettings = new EcommerceSettingGeneral();
				$EcommerceSettings -> ShippingRates = $_POST['settingFlatRate'];
				$EcommerceSettings -> EmailNotifications = $_POST['settingEmailNotificaitons'];
				
				if($EcommerceSettings -> Validate()) {
					$EcommerceSettings -> Save();	
				}
				break;
			case "markedshipped":
				$markshipped = EcommerceOrder::WithID($id);
				$markshipped -> TrackingNumber = $_POST['shipTrackingNumber'];
				$markshipped -> ShippingCompany = $_POST['shippingCompany'];
				if($markshipped -> ValidateShipStatus()) {
					$markshipped -> MarkAsShipped();	
				}
				break;
			case "approvereview":
				$review = ReviewSingle::WithID($id);
				$review -> Approve();
				break;
			case "filterproducts":
				$productFilter = new EcommerceProduct();
				$productFilter -> FilteredSubmittedContent = $_POST['FilteredText'];
				if($productFilter -> ValidateFilter()) {
					$productFilter -> FilterProducts();	
				}
				break;
		}
	}

	public function delete($type, $id) {
		switch($type) {
			case "category":
				$SettingCategory = EcommerceSettingCategory::WithID($id);
				$SettingCategory -> delete();
				break;
			case "color":
				$SettingColor = EcommerceSettingColor::WithID($id);
				$SettingColor -> delete();
				break;
			case "size":
				$SettingSize = EcommerceSettingSize::WithID($id);
				$SettingSize -> delete();
				break;
			case "product":
				$Product = EcommerceProduct::WithID($id);
				$Product -> delete();
				break;
			case "photo":
				$ProductPhoto = EcommerceProductPhoto::WithID($id);
				$ProductPhoto -> Delete();
				break;
			case "review":
				$review = ReviewSingle::WithID($id);
				$review -> Delete();
				break;
		}
	}
	
	
	public function watermark($type, $id) {
		switch($type) {
			case "single":
				$photo = EcommerceProductPhoto::WithID($id);
				$photo -> WaterMarkSingle();
				break;
			case "all":
				$photo = new EcommerceProductPhoto();
				$photo -> ProductID = $id;
				$photo -> WaterMarkAllPhotosByInventory();
				$photo -> RedirectToProduct($id);
				break;
		}
	}
	
	public function rotate($direction, $id) {
		$inventoryPhoto = EcommerceProductPhoto::WithID($id);
		$inventoryPhoto -> rotation = $direction;
		$inventoryPhoto -> RotateImage();
	}
	
	
	public function GetProductHistory($id) {
		$logs = new EditHistoryLogList();
		
		$editHistory = array();
		foreach($logs -> LogsByEcommerceProductItem($id) as $log) {
			$editedTime = explode('T', $log['edittedTimeAndDate']);
			array_push($editHistory, array('value' => $log['firstName'] . ' ' . $log['lastName'] . ' - ' . $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1])));	
		}
		
		echo json_encode($editHistory);
	}

}
?>