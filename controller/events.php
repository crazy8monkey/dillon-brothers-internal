<?php


class Events extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/EventsController.js", PATH . "public/js/colResizable-1.6.min.js", "https://cdnjs.cloudflare.com/ajax/libs/dompurify/0.8.4/purify.min.js", PATH . 'public/grapesjs/grapes.min.js');
		$this -> view -> css = array('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', PATH . 'public/css/EventsController.css', PATH . 'public/css/main.css');
	}
	
	public function index() {
		$events = new EventsList();
		$pdfs = new EventPDFList();
		$pdfList = EventPDF::WithCurrentYear(date("Y", $this -> time -> NebraskaTime()));
		$this -> view -> blueBackground = "";
		$this -> view -> title = "Events";
		$this -> view -> uploadPDFText = $pdfList -> GetUploadNotificaitonText();
		$this -> view -> uploadPDFLabel = $pdfList -> GetUploadLabel();
		
		$this -> view -> SuperEvents = $events -> SuperEvents();
		$this -> view -> SubEvents = $events -> SubEvents();
		$this -> view -> StandaloneEvents = $events -> StandaloneEvents();
		
		$this -> view -> PDFS = $pdfs -> AllYears();
		$this -> view -> startJsFunction = array('EventsController.InitializeEventsList();');
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('EventsReadOnlyOrEdit') == 0) {
			$this -> view -> render('events/listReadOnly');	
		} else {
			$this -> view -> render('events/list');	
		}
	}
	
	public function view($type, $id) {
		$this -> view -> blueBackground = "";
		$stores = new StoreList();
		$events = new EventsList();
		$logs = new EditHistoryLogList();
		$photos = new PhotosList();
		switch($type) {
			case "SuperEvent":
				$this -> view -> title = "Viewing Super Event";
				$this -> view -> EventSingle = Event::WithID($id);
				$this -> view -> logs = $logs -> LogsByEvent($id);
				$this -> view -> render('events/superevent/viewOnly');
				break;
			case "SubEvent":
				$this -> view -> title = "Viewing Sub Event";
				$this -> view -> EventSingle = Event::WithID($id);
				$this -> view -> logs = $logs -> LogsByEvent($id);
				$this -> view -> savedPhotos = $photos -> AllPhotos();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> render('events/subevent/viewOnly');
				break;
			case "Standalone":
				$this -> view -> title = "Viewing StandAlone Event";
				$this -> view -> EventSingle = Event::WithID($id);
				$this -> view -> logs = $logs -> LogsByEvent($id);
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> render('events/subevent/viewOnly');
				break;
		}
		
		
		//$this -> view -> SuperEvents = $events -> SuperEvents();
		
		
	}

	public function create($type) {
		$this -> view -> blueBackground = "";
		
		$stores = new StoreList();
		$events = new EventsList();
		$this -> view -> stores = $stores -> AllStores();
		
		$this -> view -> startJsFunction = array('EventsController.InitializeSingleEvent();');
		
		
		switch($type) {
			case "SuperEvent";
				$this -> view -> title = "Create Super Event";
				$this -> view -> render('events/superevent/create');
				break;
			case "SubEvent":
				$this -> view -> title = "Create Sub Event";
				$this -> view -> SuperEvents = $events -> SuperEvents();
				$this -> view -> render('events/subevent/create');
				break;
			case "StandAloneEvent":
				$this -> view -> title = "Create Stand Alone Event";
				$this -> view -> render('events/standalone/create');
				break;
		}
	}
	
	public function edit($type, $id) {
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('EventsReadOnlyOrEdit') == 0) {
			$this -> redirect -> redirectPage(PATH. 'events/view/' . $id);
		}
		
		$this -> view -> blueBackground = "";
		$stores = new StoreList();
		$events = new EventsList();
		$logs = new EditHistoryLogList();
		$this -> view -> logs = $logs -> LogsByEvent($id);
		
		
		switch($type) {
			case "SuperEvent":
				$this -> view -> title = "Edit Super Event";
				
				$this -> view -> EventSingle = Event::WithID($id);
				$this -> view -> startJsFunction = array('EventsController.InitializeSuperEventSingle();');
				$this -> view -> render('events/superevent/edit');	
				break;
			case "SubEvent":
				$this -> view -> title = "Edit Sub Event";
				$this -> view -> startJsFunction = array('EventsController.InitializeSingleEvent();');
				$this -> view -> SuperEvents = $events -> SuperEvents();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> EventSingle = Event::WithID($id);
				
				$this -> view -> render('events/subevent/edit');	
				break;
			case "Standalone":
				$this -> view -> title = "Edit Stand alone Event";
				$this -> view -> startJsFunction = array('EventsController.InitializeSingleEvent();');
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> EventSingle = Event::WithID($id);
				$this -> view -> render('events/standalone/edit');	
				break;
			
		}
		
		
		
		
		
		$this -> view -> SuperEvents = $events -> SuperEvents();
		
		
	}

	public function pagebuilder($type, $id) {
		$event = Event::WithID($id);
		$htmlJSON = json_decode($event -> GetJSONFile(), true);
		
		$photos = new PhotosList();
		$this -> view -> savedPhotos = $photos -> EventLoadPhotos($id);
		$this -> view -> eventID = $id;
		if(isset($htmlJSON['gjs-css']) && isset($htmlJSON['gjs-html'])) {
			$this -> view -> htmlTemplate = array("css" => $htmlJSON['gjs-css'],
					 		   				  "html" => $htmlJSON['gjs-html']);	
		}
		
		$this -> view -> render('events/pagebuilder', true);
	}
	
	
	public function save($action, $id = NULL) {
		switch($action) {
			case "superevent":
				if(isset($id)) {
					$event = Event::WithID($id);
					$event -> SuperEventPhoto = $_FILES['SuperEventPhoto']['name'];	
					$event -> seoURL = $_POST['urlSlug'];
				} else {
					$event = new Event();
				}
				$event -> eventName = $_POST['eventName'];
				$event -> SuperEventDescription = $_POST['superEventDescription'];
				$event -> SuperEventIDString = $_POST['superEventIDText'];
				$event -> PerformerInfo = $_POST['performerInfo'];
				$event -> EventPrice = $_POST['priceInfo'];
				$event -> Longitutde = $_POST['longitude'];
				$event -> Latitude = $_POST['latitude'];
				
				
				if($event -> ValidateSuperEvent()) {
					$event -> SaveSuperEvent();
				}
					
				break;
			case "subevent":
				if(isset($id)) {
					$event = Event::WithID($id);
					$event -> eventHtmlJSON = $_POST['HtmlEventJSON'];
				} else {
					$event = new Event();
				}
				
				$event -> Store = $_POST['store'];
				$event -> SuperEventID = $_POST['SuperEventID'];
				$event -> eventName = $_POST['eventName'];
				$event -> PerformerInfo = $_POST['performerInfo'];
				$event -> EventPrice = $_POST['priceInfo'];
				$event -> Longitutde = $_POST['longitude'];
				$event -> Latitude = $_POST['latitude'];
				$event -> seoURL = $_POST['urlSlug'];
				
				$event -> eventName = $_POST['eventName'];
				$event -> startDate = $_POST['eventStartDate'];
				$event -> startTime = $_POST['eventStartTime'];
				$event -> endDate = $_POST['eventEndDate'];
				$event -> endTime = $_POST['eventEndTime'];
				$event -> Store = $_POST['store'];		
						
				$event -> address = $_POST['eventAddress'];
				$event -> city = $_POST['eventCity'];
				$event -> state = $_POST['eventState'];
				$event -> zip = $_POST['eventZip'];
						
				$event -> GoogleEventDescription = $_POST['googleDescription'];
				
				if($event -> ValidateSubEvent()) {
					$event -> SaveSubEvent();
				}
				
				break;
			case "standalone":
				if(isset($id)) {
					$event = Event::WithID($id);
					$event -> eventHtmlJSON = $_POST['HtmlEventJSON'];
					$event -> seoURL = $_POST['urlSlug'];
				} else {
					$event = new Event();
				}
				
				$event -> Store = $_POST['store'];
				$event -> eventName = $_POST['eventName'];
				$event -> PerformerInfo = $_POST['performerInfo'];
				$event -> EventPrice = $_POST['priceInfo'];
				$event -> Longitutde = $_POST['longitude'];
				$event -> Latitude = $_POST['latitude'];
				
				
				$event -> eventName = $_POST['eventName'];
				$event -> startDate = $_POST['eventStartDate'];
				$event -> startTime = $_POST['eventStartTime'];
				$event -> endDate = $_POST['eventEndDate'];
				$event -> endTime = $_POST['eventEndTime'];
				$event -> Store = $_POST['store'];		
						
				$event -> address = $_POST['eventAddress'];
				$event -> city = $_POST['eventCity'];
				$event -> state = $_POST['eventState'];
				$event -> zip = $_POST['eventZip'];
						
				$event -> GoogleEventDescription = $_POST['googleDescription'];
				
				if($event -> ValidateStandaloneEvent()) {
					$event -> SaveStandaloneEvent();
				}
				
				break;
			case "single":
				if(isset($id)) {
					$event = Event::WithID($id);
					$event -> eventHtmlJSON = $_POST['HtmlEventJSON'];
					if(isset($_FILES['SuperEventPhoto']['name'])) {
						$event -> SuperEventPhoto = $_FILES['SuperEventPhoto']['name'];	
					}
					
				} else {
					$event = new Event();	
				}
						
				$event -> eventName = $_POST['eventName'];
				$event -> startDate = $_POST['eventStartDate'];
				$event -> startTime = $_POST['eventStartTime'];
				$event -> endDate = $_POST['eventEndDate'];
				$event -> endTime = $_POST['eventEndTime'];
				$event -> Store = $_POST['store'];		
						
				$event -> address = $_POST['eventAddress'];
				$event -> city = $_POST['eventCity'];
				$event -> state = $_POST['eventState'];
				$event -> zip = $_POST['eventZip'];
						
				$event -> GoogleEventDescription = $_POST['googleDescription'];
				
				$event -> PerformerInfo = $_POST['performerInfo'];
				$event -> EventPrice = $_POST['priceInfo'];
				$event -> Longitutde = $_POST['longitude'];
				$event -> Latitude = $_POST['latitude'];
				
						
				$event -> SuperEvent = $_POST['SuperEvent'];
				$event -> SubEvent = $_POST['SubEvent'];
				$event -> SuperEventID = $_POST['SuperEventID'];
				$event -> SuperEventDescription = $_POST['superEventDescription'];
				$event -> SuperEventIDString = $_POST['superEventIDText'];
						
				if($event -> Validate()) {
					$event -> Save();
				}
				break;
			case "publish":
				$event = Event::WithID($id);
				$event -> Publish();
				break;
			case "unpublish":
				$event = Event::WithID($id);
				$event -> UnPublish();
				break;
			case "uploadpdf":
				$pdf = new EventPDF();
				$pdf -> PDF = $_FILES['eventYearPDF']['name'];
				if($pdf -> Validate()) {
					$pdf -> Save();
				}
				
				break;
			
			}
				
		}

	
	public function htmlpush($id) {
		$data = file_get_contents('php://input');
		$postData = array();
		parse_str($data, $postData);
		
		$event = Event::WithHTMLID($id);
		$event -> eventHtmlJSON = json_encode(array("gjs-css" => $postData['gjs-css'],
				   			   "gjs-html" => $postData['gjs-html']));
		$event -> SaveJSON();
		
		
		//echo $postData['gjs-css'];
		
		echo json_encode(array("gjs-css" => $postData['gjs-css'],
				   			   "gjs-html" => $postData['gjs-html']));
	}

	public function htmload($id) {
		$event = Event::WithID($id);
		$htmlJSON = json_decode($event -> GetJSONFile(), true);
		
		//foreach($_GET['keys'] as $keySingle) {
		//	if($keySingle == "gjs-css") {
		//		$keySingle = $htmlJSON['gjs-css'];	
		//	}	
		//}
		
		echo json_encode(array("gjs-assets" => '',
							   "gjs-css" => $htmlJSON['gjs-css'],
					 		   "gjs-html" => $htmlJSON['gjs-html']));
	
	}

	public function selectphoto($eventID) {
		
		$this -> view -> blueBackground = "";
		$this -> view -> EventSingle = Event::WithID($eventID);
		if(!isset($_GET['albumID'])) {
			$this -> view -> title = "Select Album";
			$AllAlbums = new PhotoAlbums();
			$this -> view -> albumsList = $AllAlbums -> AllAlbums();	
			$this -> view -> albumView = '';
		} else {
			$this -> view -> title = "Select Photo";
			$photos = new PhotosList();
			$this -> view -> photosList = $photos -> PhotosByAlbum($_GET['albumID']);
		}
		
		$this -> view -> render("events/selectelement", true);
		
	}
	
	public function selectalbum($eventID) {
		$AllAlbums = new PhotoAlbums();
		$this -> view -> title = "Select Album";
		$this -> view -> selectAlbumLink = '';
		$this -> view -> albumView = '';
		$this -> view -> EventSingle = Event::WithID($eventID);
		$this -> view -> albumsList = $AllAlbums -> AllAlbums();
		$this -> view -> render("events/selectelement", true);
	}
		
		
		
	public function GetEditHistory($id) {
		$logs = new EditHistoryLogList();
		
		$editHistory = array();
		foreach($logs -> LogsByEvent($id) as $log) {
			$editedTime = explode('T', $log['edittedTimeAndDate']);
			array_push($editHistory, array('value' => $log['firstName'] . ' ' . $log['lastName'] . ' - ' . $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1])));	
		}
		
		echo json_encode($editHistory);
	}
	
	public function delete($id) {
		$event = Event::WithID($id);
		$event -> Delete();
	}

	public function uploadpictures($id) {
		//echo json_encode(print_r($_FILES['files']['name']));
		$photoAlbum = PhotoAlbum::WIthEventID($id);
		$photoAlbum -> fileUploads = $_FILES['files']['name'];
		$photoAlbum -> UploadMultiplePhotos();
	}
	


}
?>