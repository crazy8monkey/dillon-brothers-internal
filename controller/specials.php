<?php


class Specials extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		if($_SESSION["user"]->HasPermission('SpecialsAddEditDelete') == false) {
			$this -> redirect -> redirectPage(PATH. 'dashboard');
		}
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/SpecialsController.js");
		$this -> view -> css = array(PATH . 'public/css/SpecialsController.css');
	}
	
	public function index() {
		$this -> view -> title = "Specials Category";
		$this -> view -> render('specials/category');
	}
	
	private function CheckOEMCategoryPermission() {
		if($_SESSION["user"]->HasPermission('SpecialOEMCategoryVisible') == false) {
			$this -> redirect -> redirectPage(PATH. 'settings');
		}
	}
	
	private function CheckMotorClothesCategoryPermission() {
		if($_SESSION["user"]->HasPermission('SpecialMotorclothesCategoryVisible') == false) {
			$this -> redirect -> redirectPage(PATH. 'settings');
		}
	}
	
	private function CheckPartsCategoryPermission() {
		if($_SESSION["user"]->HasPermission('SpecialPartsCategoryVisible') == false) {
			$this -> redirect -> redirectPage(PATH. 'settings');
		}
	}

	private function CheckServiceCategorPermission() {
		if($_SESSION["user"]->HasPermission('SpecialServiceCategoryVisible') == false) {
			$this -> redirect -> redirectPage(PATH. 'settings');
		}
	}
	
	public function category($type) {
		$Specials = new SpecialsList();
		switch($type) {
			case "OEM":
				$this -> CheckOEMCategoryPermission();
				$brandsList = new BrandsList();
				$this -> view -> title = "Vehicle Specials";
				$this -> view -> brands = $brandsList -> AllBrands();
				
				$this -> view -> render('specials/Vehicle/Brands');
				break;
			case "Apparel":
				$this -> CheckMotorClothesCategoryPermission();
				$PartApparelBrands = new PartsApparelBrandList();
				
				$this -> view -> blueBackground = "";
				$this -> view -> brands = $PartApparelBrands -> ApparelBrands();
				$this -> view -> apparelSpecials = $Specials -> ApparelSpecialsNotBrandSpecific();
				$this -> view -> title = "Apparel Brands";
				$this -> view -> render('specials/Clothes/Brands');
				break;
			case "Parts":
				$this -> CheckPartsCategoryPermission();
				$this -> view -> blueBackground = "";
				$PartApparelBrands = new PartsApparelBrandList();
				$this -> view -> brands = $PartApparelBrands -> PartBrands();
				$this -> view -> partsSpecials = $Specials -> PartSpecialsNotBrandSpecific();
				$this -> view -> title = "Parts Brands";
				$this -> view -> render('specials/Parts/Brands');
				break;
			case "Service":
				$this -> CheckServiceCategorPermission();
				$this -> view -> title = "Service Specials";
				$this -> view -> ServiceSpecials = $Specials -> ServiceSpecials();
				$this -> view -> render('specials/Service/list');	
				break;
			case "Store":
				if($_SESSION["user"]->HasPermission('SpecialStoreCategoryVisible') == false) {
					$this -> redirect -> redirectPage(PATH. 'settings');
				}
				
				
				$this -> view -> title = "Store Specials";
				$this -> view -> storeSpecials = $Specials -> StoreSpecials();
				$this -> view -> render('specials/Stores/list');
				break;	
		}
	}
	
	public function OEM($id) {
		$this -> CheckOEMCategoryPermission();
		$this -> view -> blueBackground = "";
		$vehicleBrand = Brand::WithID($id);
		
		$this -> view -> title = $vehicleBrand -> _brandName . " OEM Specials";
		
		
		$specialsList = new SpecialsList();	
		$this -> view -> BrandSpecifics = $vehicleBrand;
		$this -> view -> SpecialsList = $specialsList -> OEMSpecialsByBrand($id);
		$this -> view -> startJsFunction = array('SpecialsController.InitializeVehicleBrandPage();');
		$this -> view -> render('specials/Vehicle/list');
	}
	
	
	
	public function brand($id, $category) {
		$specialsList = new SpecialsList();	
		switch($category) {
			case "Parts":
				$this -> CheckPartsCategoryPermission();
				$partsBrand = PartsAccessoryBrand::WithID($id);
				$this -> view -> blueBackground = "";
				$this -> view -> BrandSpecifics = $partsBrand;
				$this -> view -> title = "" . $partsBrand -> PartsAccessoryBrandName ." Specials Info";
				$this -> view -> SpecialsList = $specialsList -> PartSpecialsByBrand($id);
				$this -> view -> startJsFunction = array('SpecialsController.InitializePartsBrandForm();');
				$this -> view -> render('specials/Parts/list');
				break;
				
			case "Apparel":
				$this -> CheckMotorClothesCategoryPermission();
				$partsBrand = PartsAccessoryBrand::WithID($id);
				$this -> view -> blueBackground = "";
				$this -> view -> BrandSpecifics = $partsBrand;
				$this -> view -> title = "" . $partsBrand -> PartsAccessoryBrandName ." Specials Info";
				$this -> view -> SpecialsList = $specialsList -> ApparelSpecialsByBrand($id);
				$this -> view -> startJsFunction = array('SpecialsController.InitializePartsBrandForm();');
				$this -> view -> render('specials/Clothes/list');
				break;	
				
			case "Service":
				$this -> CheckServiceCategorPermission();
				$storeSingle = Store::WithBrand($brandName);
				$this -> view -> BrandSpecifics = $storeSingle;
				$this -> view -> brandName = $brandName;
				$this -> view -> SpecialsList = $specialsList -> ServiceSpecialsBystore($storeSingle -> GetID());
				$this -> view -> startJsFunction = array('SpecialsController.InitializePartsBrandForm();');
				$this -> view -> render('specials/Service/list');
				break;
			
			
		}
		
	}
	
	public function create($type, $optional = NULL) {
		switch($type) {
			case "OEMSpecial":
				$this -> CheckOEMCategoryPermission();
				$this -> view -> blueBackground = "";
				$brand = Brand::WithID($optional);
				$this -> view -> title = "New " .$brand -> _brandName ." Special: OEM Special";
				$this -> view -> brandDetails = $brand;
				$this -> view -> brandBreadCrumbs = $brand -> GetNewSpecialBreadCrumbs();
				$this -> view -> startJsFunction = array('SpecialsController.InitializeSpecialForm();');
				$this -> view -> render('specials/Vehicle/createSpecial');		
				break;
			case "BrandApparelSpecial":
				$this -> CheckMotorClothesCategoryPermission();
				$PartBrand = PartsAccessoryBrand::WithID($optional);
				$this -> view -> blueBackground = "";
				$this -> view -> title = "New " . $PartBrand -> PartsAccessoryBrandName ." Special: Apparel Special";
				$this -> view -> brandSpecifics = $PartBrand;
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> brandBreadCrumbs = $PartBrand -> GetNewSpecialBreadCrumbs();
				$this -> view -> startJsFunction = array('SpecialsController.InitializeSpecialForm();');
				$this -> view -> render('specials/Clothes/createSpecial');
				break;
			case "BrandPartsSpecial":
				$this -> CheckPartsCategoryPermission();
				$PartBrand = PartsAccessoryBrand::WithID($optional);
				$this -> view -> blueBackground = "";
				$this -> view -> title = "New " . $PartBrand -> PartsAccessoryBrandName ." Special: Parts Special";
				$this -> view -> brandSpecifics = $PartBrand;
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> brandBreadCrumbs = $PartBrand -> GetNewSpecialBreadCrumbs();
				$this -> view -> startJsFunction = array('SpecialsController.InitializeSpecialForm();');
				$this -> view -> render('specials/Parts/createSpecial');
				break;
			case "ServiceSpecial":
				$this -> CheckServiceCategorPermission();
				$this -> view -> blueBackground = "";
				$this -> view -> title = "New Store Service Special";
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> startJsFunction = array('SpecialsController.InitializeSpecialForm();');
				$this -> view -> render('specials/Service/createSpecial');
				break;
			case "storespecial":
				$this -> view -> blueBackground = "";
				$this -> view -> title = "New Store Special";
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> startJsFunction = array('SpecialsController.InitializeSpecialForm();');
				$this -> view -> render('specials/Stores/createSpecial');
				break;
			case "Parts":
				$this -> view -> blueBackground = "";
				$this -> view -> title = "New Parts Special";
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> startJsFunction = array('SpecialsController.InitializeSpecialForm();');
				$this -> view -> render('specials/Parts/createSpecial');
				break;
			case "Apparel":
				$this -> view -> blueBackground = "";
				$this -> view -> title = "New Apparel Special";
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> startJsFunction = array('SpecialsController.InitializeSpecialForm();');
				$this -> view -> render('specials/Clothes/createSpecial');
				break;
		}
		
		
	}
	
	public function edit($type, $id) {
		$this -> view -> startJsFunction = array('SpecialsController.InitializeSpecialForm();');
		$special = Special::WithID($id);
		$this -> view -> SpecialSingle = $special;
		$currentInventory = new CurrentInventoryList();
		$specialItems = new RelatedSpecialItemsList();
		$logs = new EditHistoryLogList();
		$this -> view -> logs = $logs -> LogsBySpecial($id);
		switch($type) {
			case "OEMSpecial":
				$this -> CheckOEMCategoryPermission();
				$this -> view -> blueBackground = "";
				$this -> view -> title = "Edit " .$special -> OEMBrandName ." Special: OEM Special";
				$this -> view -> items = $specialItems -> ItemsBySpecial($id);
				$this -> view -> inventory = $currentInventory -> ActiveInventoryBySpecial($special -> OEMBrandStoreID);	
				$this -> view -> linkedInventory = $specialItems -> LinkedInventoryBySpecial($id);
				$this -> view -> render('specials/Vehicle/editSpecial');	
				break;
			case "PartsSpecial":
				$this -> CheckPartsCategoryPermission();
				$this -> view -> blueBackground = "";
				$this -> view -> title = "Edit Parts Special";
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> items = $specialItems -> ItemsBySpecial($id);
				$this -> view -> render('specials/Parts/editSpecial');
				break;
			case "ApparelSpecial":
				$this -> CheckMotorClothesCategoryPermission();
				$this -> view -> blueBackground = "";
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> title = "Edit Apparel Special";
				$this -> view -> items = $specialItems -> ItemsBySpecial($id);
				$this -> view -> render('specials/Clothes/editSpecial');
				break;
			case "ServiceSpecial":
				$this -> CheckServiceCategorPermission();
				$this -> view -> blueBackground = "";
				$this -> view -> title = "Edit Service Special";
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				$this -> view -> items = $specialItems -> ItemsBySpecial($id);
				$this -> view -> render('specials/Service/editSpecial');
				break;
			case "StoreSpecial":
				$this -> view -> blueBackground = "";
				$this -> view -> title = "Edit Store Special";
				$stores = new StoreList();
				$this -> view -> stores = $stores -> AllStores();
				if($special -> RelatedInventoryType == 1) {
					$this -> view -> inventory = $currentInventory -> ActiveInventoryBySpecial($special -> RelatedStores());	
					$this -> view -> linkedInventory = $specialItems -> LinkedInventoryBySpecial($id);
				}
				$this -> view -> items = $specialItems -> ItemsBySpecial($id);
				$this -> view -> render('specials/Stores/editSpecial');
				break;
		}
	}
	
	
	public function addinginventorytoincentive($specialID) {
		$special = Special::WithID($specialID);
		$currentInventory = new CurrentInventoryList();
		
		$linkedInventory = new RelatedSpecialItemsList();
		
		
		
		$this -> view -> title = $special -> SpecialTitle;
		
		$this -> view -> specialInfo = $special;
		$this -> view -> inventory = $currentInventory -> ActiveInventoryBySpecial($special -> RelatedStores());
		$this -> view -> specialID = $specialID;
		
		$this -> view -> items = $linkedInventory -> ItemsBySpecial($specialID);
		$this -> view -> linkedInventory = $linkedInventory -> LinkedInventoryBySpecial($specialID);
		
		
		$this -> view -> render('specials/filterInventory', true);
	}
	
	public function GetInventoryBySpecial($storeIDS) {
		$currentInventory = new CurrentInventoryList();
		
		echo json_encode($currentInventory -> ActiveInventoryBySpecial($storeIDS));
	}
	
	
	public function save($type, $ID = NULL) {
			
		switch($type) {
			
			case "Special":
				if(isset($ID)) {
					$special = Special::WithID($ID);		
					if(isset($_POST['specialsActive'])) {
						$special -> SpecialIsActive = $_POST['specialsActive'];	
					}
					
				} else {
					$special = new Special();
					$special -> SpecialCategory = $_POST['specialType'];	
					
					if(isset($_POST['PartApparelBrandID'])) {
						$special -> PartAcessoryBrandID = $_POST['PartApparelBrandID'];	
					}
					
					if(isset($_POST['OEMBrandName'])) {
						$special -> OEMBrandID = $_POST['OEMBrandName'];	
					}	
				}
				
				$special -> RelatedInventoryType = $_POST['linkedToVehicleInventory'];	
				
				if(isset($_POST['store'])) {
					$special -> Stores = $_POST['store'];
				}
				
				$special -> SpecialImageBig = $_FILES['specialPhotoBig']['name'];
				$special -> SpecialImageSmall = $_FILES['specialPhotoSmall']['name'];
				$special -> SpecialTitle = $_POST['specialTitle'];
				
				$special -> SpecialDescription = $_POST['specialDescription'];
				$special -> SpecialDescriptionFooter = $_POST['specialDescriptionFooter'];
				
				
				
				if($special -> Validate()) {
					$special -> Save();
				}				
				break;		
			case "Vehicles":
				
				$vehicleSpecial = new VehicleSpecial();
				
				$vehicleSpecial -> category = $_POST['specialCategory'];
				$vehicleSpecial -> type = $_POST['specialType'];
				$vehicleSpecial -> TypeValue = $_POST['specialTypeValue'];
				$vehicleSpecial -> VehicleName = $_POST['specialName'];
				$vehicleSpecial -> endDate = $_POST['specialEndDate'];
				$vehicleSpecial -> description = $_POST['specialDescription'];
				$vehicleSpecial -> disclaimer = $_POST['specialDisclaimer'];
				
				$vehicleSpecial -> specialID = $_POST['SpecialID'];
				
				if($vehicleSpecial -> Validate()) {
					$vehicleSpecial -> Save();
				}
				
				break;

				
			case "StoreDetail":
				
				$storeDetail = Store::WithID($ID);
				$storeDetail -> structuredDataDescription = $_POST['structuredDataDescription'];
				
				$storeDetail -> schemaName = $_POST['schemaName'];
				$storeDetail -> schemaImageURL = $_POST['schemaImageUrl'];
				$storeDetail -> schemaDescription = $_POST['schemaDescription'];
				
				$storeDetail -> twitterTitle = $_POST['twitterMetaTagTitle'];
				$storeDetail -> twitterDescription = $_POST['twitterMetaTagDescription'];
				$storeDetail -> twitterURL = $_POST['twitterMetaTagURL'];
				$storeDetail -> twitterImageURL = $_POST['twitterMetaTagImageURL'];
				
				$storeDetail -> openGraphTitle = $_POST['openGraphMetaTagTitle'];
				$storeDetail -> openGraphType = $_POST['openGraphMetaTagType'];
				$storeDetail -> openGraphDescription = $_POST['openGraphMetaTagDescription'];
				$storeDetail -> openGraphURL = $_POST['openGraphMetaTagURL'];
				$storeDetail -> openGraphImage = $_POST['openGraphMetaTagImage'];
				
				$storeDetail -> openGraphArticleSeciton = $_POST['openGraphMetaTagArticleSection'];
				$storeDetail -> openGraphArticleTag = $_POST['openGraphMetaTagArticleTag'];
				
				$storeDetail -> Save();
				
				break;
			case "specialitems":
				$special = Special::WithID($ID);
				
				$special -> RelatedSpecialID = $_POST['relatedSpecialItemID'];
				
				$special -> RelatedSpecialType = $_POST['specialType'];
				$special -> RelatedSpecialTypeValue = $_POST['specialTypeValue'];
				$special -> RelatedSpecialExpiredDate = $_POST['specialEndDate'];
				$special -> RelatedDescription = $_POST['specialDescription'];
				$special -> RelatedDisclaimer = $_POST['specialDisclaimer'];
				
				if($special -> ValidateRelatedItemsInventory()) {
					$special -> SaveLinkedInventory();
				}
				
				break;
			case "speciallinkinventory":
				$special = Special::WithID($ID);
				
				$special -> RelatedSpecialID = $_POST['relatedSpecialItemID'];
				
				$special -> RelatedSpecialType = $_POST['specialType'];
				$special -> RelatedSpecialTypeValue = $_POST['specialTypeValue'];
				$special -> RelatedSpecialExpiredDate = $_POST['specialEndDate'];
				$special -> RelatedDescription = $_POST['specialDescription'];
				$special -> RelatedDisclaimer = $_POST['specialDisclaimer'];
				$special -> LinkedInventoryIDS = $_POST['linkedInventoryIDS'];
				
				if($special -> ValidateRelatedItemsInventory()) {
					$special -> SaveLinkedInventory();
				}
				
				
				break;
			case "newspeciallinkinventory":
				$incentive = new RelatedSpecialItem();
				
				$incentive -> RelatedSpecialID = $ID;
				$incentive -> RelatedSpecialType = $_POST['newSpecialType'];
				$incentive -> RelatedSpecialTypeValue = $_POST['newSpecialTypeValue'];
				$incentive -> RelatedSpecialExpiredDate = $_POST['newSpecialEndDate'];
				$incentive -> RelatedDescription = $_POST['newSpecialDescription'];
				$incentive -> RelatedDisclaimer = $_POST['newSpecialDisclaimer'];
				
				
				if($incentive -> ValidateRelatedItem()) {
					$incentive -> Save();
					$incentive -> RefreshPage();
				}
				
				break;
			case "inventorytoincentive":
				$incentive = RelatedSpecialItem::WithID($ID);
				
				if(isset($_POST['inventoryID'])) {
					$incentive -> LinkedRelatedInventoryID = $_POST['inventoryID'];	
				}

				if($incentive -> ValidateLinkedInventory()) {
					$incentive -> LinkInventoryToIncentive();
					
				}
				
				
				break;
		}	
			

		
	}

	public function relatedInventoryByIncentive($id) {
		$incentives = new RelatedSpecialItemsList();
		echo json_encode($incentives -> LinkedInventoryByIncentive($id));
	}

	public function delete($type, $id) {
		switch($type) {
			case "Special":
				$special = Special::WithID($id);
				$special -> delete();
				break;
				
			case "PartApparelBrand":
				$brand = PartsAccessoryBrand::WithID($id);
				$brand -> delete();
				break;
			case "incentive":
				$incentive = RelatedSpecialItem::WithID($id);
				$incentive -> delete();
				break;
		}
		
		
	}
	
	
	public function filter($type) {
		$postData = array();
		$currentInventory = new CurrentInventoryList();	
		
		if(isset($_POST['condition'])) {
			$postData['condition'] = $_POST['condition'];					
		}
				
		if(isset($_POST['Year'])) {
			$postData['Years'] = $_POST['Year'];					
		}
				
		if(isset($_POST['category'])) {
			$postData['Categories'] = $_POST['category'];					
		}
		
		if(isset($_POST['manufacture'])) {
			$postData['Manufactures'] = $_POST['manufacture'];					
		}
		
		if(isset($_POST['model'])) {
			$postData['Models'] = $_POST['model'];					
		}
		
		
		if($type == "inventory") {
			echo json_encode($currentInventory -> ActiveInventoryBySpecialFiltered($_POST['stores'], $postData));
		} else {
			echo json_encode($currentInventory -> FilterOption($_POST['stores'], $postData, $type));	
		}
		
		
		
		
	}

	public function GetLinkedInventoryByIncentive($id) {
		$incentive = new RelatedSpecialItemsList();
		echo json_encode($incentive -> LinkedInventoryByIncentive($id));
	}

	public function GetEditHistory($id) {
		$logs = new EditHistoryLogList();
		
		$editHistory = array();
		foreach($logs -> LogsBySpecial($id) as $log) {
			$editedTime = explode('T', $log['edittedTimeAndDate']);
			array_push($editHistory, array('value' => $log['firstName'] . ' ' . $log['lastName'] . ' - ' . $this -> recordedTime -> formatShortDate($editedTime[0]) . ' / ' . $this -> recordedTime -> formatTime($editedTime[1])));	
		}
		
		echo json_encode($editHistory);
	}

	public function FilterCondition() {
		
	}

}
?>