<?php


class Profile extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		
		$this -> view -> adminLogin = "";
	}
	
	public function index() {
		Session::init();		
		$this -> view -> title = "Your Profile";
		$this -> view -> profileView = "";
		$this -> view -> blueBackground = "";
		$this -> view -> js = array(PATH . "public/js/SettingsController.js");
		$this -> view -> css = array(PATH . 'public/css/SettingsController.css');
		$this -> view -> startJsFunction = array('SettingsController.InitializeUserForm();');
		$this -> view -> UserSingle = User::WithID($_SESSION['user'] -> _userId);
		$this -> view -> render('settings/users/edit');	
	}
	

	public function create() {
		$this -> view -> title = "Create User";
		
		$this -> view -> render('users/create');
	}
	
	public function edit($id) {
		$this -> view -> title = "Edit User";
		$this -> view -> startJsFunction = array('UsersController.InitializeUserForm();');
		
		$this -> view -> render('users/edit');
	}
	
	public function delete($id) {
		
		
	}
	
	public function save($id = NULL) {
		$user = User::WithID($id);	
		$user -> FirstName = $_POST['userFirstName'];
		$user -> LastName = $_POST['userLastName'];
		$user -> Email = $_POST['userEmail'];
		$user -> SetPassword($_POST['userPassword']);
		$user -> IsProfileView = true;
		
		$user -> ProfileImageName = $_FILES['userProfilePic']['name'];
		$user -> ProfileImageTempName = $_FILES['userProfilePic']['tmp_name'];
		
		
		if($user -> Validate()) {
			$user -> Save();
		}		
	}

	


}
?>