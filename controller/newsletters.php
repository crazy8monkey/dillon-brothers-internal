<?php


class Newsletters extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		if($_SESSION["user"]->HasPermission('NewsletterAddEditDelete') == false) {
			$this -> redirect -> redirectPage(PATH. 'dashboard');
		}
		
		
		$this -> view -> adminLogin = "";
		$this -> view -> js = array(PATH . "public/js/NewsletterController.js");
		$this -> view -> css = array(PATH . 'public/css/NewsletterController.css', PATH . 'public/css/main.css');
	}
	
	public function index() {
		$this -> view -> blueBackground = "";
		$this -> view -> title = "Archived Newsletters";
		$newsletters = new NewsletterList();
		$this -> view -> newsLetters = $newsletters -> Newsletters();
		$this -> view -> render('newsletters/list');
	}
	
	public function create() {
		$this -> view -> blueBackground = "";	
		$this -> view -> title = "New Newsletter";
		$this -> view -> startJsFunction = array('NewsletterController.InitializeNewsLetterForm();');
		$this -> view -> render('newsletters/create');
	}
	
	public function edit($id) {
		$this -> view -> blueBackground = "";
		$this -> view -> title = "Edit Newsletter";	
		$this -> view -> NewsLetterSingle = Newsletter::WithID($id);
		$this -> view -> startJsFunction = array('NewsletterController.InitializeNewsLetterForm();');
		$this -> view -> render('newsletters/edit');
	}

	
	public function save($id = NULL) {
		if(isset($id)) {
			$newsletter = Newsletter::WithID($id);
		} else {
			$newsletter = new Newsletter();
			$newsletter -> thumbNail = $_FILES['newsletterThumb']['name'];	
			$newsletter -> name = $_POST['newsletterName'];
			$newsletter -> date = $_POST['newsletterDate'];
		}				
				
		if($newsletter -> Validate()) {
			$newsletter -> Save();	
		}
	}
	
	
	public function publish($id) {
		$newsletter = Newsletter::WithID($id);
		$newsletter -> Publish();
	}
	
	public function unpublish($id) {
		$newsletter = Newsletter::WithID($id);
		$newsletter -> UnPublish();
	}
	

}
?>