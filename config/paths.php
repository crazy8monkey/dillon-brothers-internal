<?php

define('PATH', 'http://localhost/DillonBrothers/');
define('WEBSITE_PATH', 'http://localhost/DillonBrothersMain/');
define('LIBS', 'libs/');
define('CLASSES', 'classes/');
define('LISTS', 'lists/');

define('PHOTO_PATH', '../DillonBrothersPhotos/');
define("NEWSLETTER_PATH", "view/photos/newsletter/");


define('PHOTO_URL', 'http://localhost/DillonBrothersPhotos/');
define("NEWSLETTER_URL", "http://localhost/DillonBrothers/view/photos/newsletter/");
define('INVENTORY_VEHICLE_INFO_URL', 'http://localhost/Inventory/');


define("JSON_EVENT_PATH", "view/PageEventJSON/");
define("JSON_EVENT_URL", "http://localhost/DillonBrothers/view/PageEventJSON/");
define('INVENTORY_VEHICLE_INFO_PATH', '../Inventory/');
define('TWILIO_API_PATH', '../Twilio/');

define("MOTOR_SPORT_INVENTORY", "MUInventory_C.csv");
define("HARLEY_INVENTORY", "MUInventory_AB.csv");
define("INDIAN_INVENTORY", "MUInventory_D.csv");




define("EVENT_PREVIEW_URL", "http://localhost/DillonBrothersMain/preview/eventsingle/");

define("LIVE_SITE", false);
define("EMAIL_SUBJECT", " | Testing");

define('IP_LOOKUP', 'http://ip-api.com/#');

// email header types
define ('MYSQL_ERROR_TYPE', 'MYSQL ERROR');



//system default error message
define ('SYSTEM_ERROR_MESSAGE', 'Something went wrong with our system. Please try again');
define('STRIPE_LINK', 'https://dashboard.stripe.com/test/');


