var PhotosController = function() {

	//private
	function formatBytes(bytes,decimals) {
	   if(bytes == 0) return '0 Byte';
	   var k = 1000;
	   var dm = decimals + 1 || 3;
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
	   var i = Math.floor(Math.log(bytes) / Math.log(k));
	   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
	}
	
	function AddPhotoResponses(responses) {
		//$("#uploadingPhotosText").show();
		if(responses.errorMessage) {
			Globals.ShowErrorMessage("#AddPhotosMessage");
			Globals.LoadingIndicator("#AddPhotosLoading");
			Globals.ShowResponseMessage("#AddPhotosText", "error", responses.errorMessage);
			$("#uploadingPhotosText").hide();
			Globals.ScrollToTop("#AddPhotosMessage");
		}
		
		if(responses.progress) {
			$(".progressBar .progressContent").attr("style", "width:" + Math.ceil(data) + "%");
            $(".ProgressTotal").html("<strong>Current Progress: </strong>" + Math.ceil(data) + "%");
		}
		
		if(responses.eventPhotoSaved) {
			window.opener.$("#SavedMainPhotoResponse").show().html('Your new Event photo has been saved/uploaded, please refresh your broswer to see the changes');
			window.close();
		}
		
		if(responses.eventPhotoThumbSaved) {
			window.opener.$("#SavedThumbPhotoResponse").show().html('Your new Event photo has been saved/uploaded, please refresh your broswer to see the changes');
			window.close();
		}
		
		if(responses.blogPhotoSaved) {
			window.opener.$("#SavedPhotoResponse").show().html('Your new Blog photo has been saved/uploaded, please refresh your broswer to see the changes')
			window.close();
		}
		
		
		
		
		if(responses.redirect) {
 			$("#uploadingPhotosText").hide();
 			Globals.redirect(responses.redirect); 
 		}
	}
	
	function AddAlbumResponses(responses) {
		if(responses.errorMessage) {
			$("#ErrorMessages").html(responses.errorMessage);
		}
		
		if(responses.redirect) {
			Globals.redirect(responses.redirect); 
		}
	}
	
	function NewAlbumResponses(responses) {
		if(responses.errorMessage) {
			$("#NewAlbumErrorMessages").html(responses.errorMessage);
		}
		
		if(responses.redirect) {
 			Globals.redirect(responses.redirect); 
 		}
	}
	
	function SaveThumbResponses(responses) {
		if(responses.redirect) {
 			Globals.redirect(responses.redirect); 
 		}
	}
	
	function UploadProgress() {
		    var xhr = new window.XMLHttpRequest();
		    //Upload progress
		    //xhr.upload.addEventListener("progress", function(evt){
		    //  if (evt.lengthComputable) {
		    //    var percentComplete = evt.loaded / evt.total;
		        //Do something with upload progress
		    //    $(".ProgressTotal").html(percentComplete)
		        //console.log(percentComplete);
		    //  }
		    //}, false);
		    //Download progress
		    xhr.addEventListener("progress", function(evt){
		      if (evt.lengthComputable) {
		        var percentComplete = evt.loaded / evt.total;
		        $(".ProgressTotal").html(percentComplete)
		      }
		    }, false);
		    return xhr;
	}
	
	
		
	function EditPhotoResponses(responses) {
		if(responses.ValidationErrors) {
			var validationMessagesArray = responses.ValidationErrors;
			
			var ValidationErrors = responses.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
			   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
			}
		}
		
		if(responses.success) {
			$("input#photoName").val(responses.success.NewName);
			$("span#photoSingleName").html(responses.success.NewName + '.' + responses.success.fileExt);
			
			Globals.ShowErrorMessage("#PhotoSingleMessage");
			Globals.LoadingIndicator("#PhotoSingleLoading");
			Globals.ShowResponseMessage("#PhotoSingleText", "success", responses.success.msg)
		}
	}
	

	//public
	function InitializePhotoAlbum() {
		$("#addAlbum").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			
			Globals.AjaxPost(formURL, postData, NewFolderResponses)		
		});
	} 
	
	
	
	function InitializeAddPhotos() {
		$("#AddPhotos").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			$("input[type='submit']").attr("disabled", "disabled");
			$.ajax({
				url: formURL,
			    type: 'POST',
			    data: formData,
			    dataType: 'json',
				cache: false,
			    beforeSend: function(response){
			    	//console.log(response);
			    	//PhotosController.GetUploadPercentage();
					//setInterval(function () {PhotosController.GetUploadPercentage()}, 4000);
			    	$(".progressBar").show();
					$(".progressBar .progressContent").attr("style", "width:0%");
					$(".ProgressTotal").html("<strong>Current Progress: </strong>Processing Photos");
			    	$("#uploadingPhotosText").show();
	    		},
	    		 xhr: function(){
			        // get the native XmlHttpRequest object
			        var xhr = $.ajaxSettings.xhr() ;
			        // set the onprogress event handler
			        xhr.upload.onprogress = function(evt){ 
			        	console.log('progress', evt.loaded/evt.total*100);
			        	$(".progressBar .progressContent").attr("style", "width:" + Math.ceil(evt.loaded/evt.total*100) + "%");
            			$(".ProgressTotal").html("<strong>Processing: </strong>" + Math.ceil(evt.loaded/evt.total*100) + "%");
			        	
			        } ;
			        // set the onload event handler
			        xhr.upload.onload = function(){ 
			        	console.log('DONE!') 
			        	$(".ProgressTotal").html("<strong>Upload Complete: </strong> Wait for the page to refresh");
			        } ;
			        // return the customized object
			        return xhr ;
			    },
	    		success : AddPhotoResponses,
				cache: false,
			    contentType: false,
			    processData: false,
			    complete:function(response) {
			    	
			    }
			});
		});
		
		
		
		
		
	}
	
	
	
	function InitializeMiscPhotoAlbum() {
		$("#PhotoAlbum").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			$(".progressBar .progressContent").attr("style", "width:0%");
			
			Globals.AjaxPost(formURL, postData, AddAlbumResponses);
			
			
					
		});
		
		
		 
		
	}
	
	

	
	function MakeFileList(albumName, UploadCount) {
		var input = document.getElementById('filesToUpload');
		var list = $('#fileList');
		
		var TotalFileSize = 0;
		var LatestUploadCount = UploadCount;
		
		
		
		//for every file...
		for (var x = 0; x < input.files.length; x++) {
			
			//console.log(input.files[x].size)
			TotalFileSize += parseInt(input.files[x].size);
			$("#TotalEntered").show();
			
			//var reader = new FileReader();
			
			//$("#fileList").append("<div class='row' style='border-top: 1px solid #dcdcdc; padding-top:10px; display:none;'>" + 
			//						"<div class='col-md-4'><div class='input'><div class='inputLabel'>Image name</div><input type='text' name='photoName[]' value='" + albumName + "-" + LatestUploadCount + "' /></div></div>" +
			//						"<div class='col-md-4'><div class='input'><div class='inputLabel'>Alt</div><input type='text' name='photoAlt[]' placeholder='alt' value='" + albumName + "-" + LatestUploadCount + "' /></div></div>" +
			//						"<div class='col-md-4'><div class='input' style='margin-bottom:0px;'><div class='inputLabel'>Title</div><input type='text' name='photoTitle[]' value='" + albumName + "-" + LatestUploadCount + "' placeholder='title' /></div></div>" + 
			//					  "</div>");
			
			
			//reader.readAsDataURL(input.files[x]);
			
			//reader.onload = function () {
            
            
				//$("#fileList").append("<div class='row' style='margin-bottom:10px; border-bottom: 1px solid #dcdcdc; padding-bottom:10px;'>" + 
				//					"<div class='col-md-4'><img src='" +  this.result + "' style='width:100%' /></div><div class='col-md-8'>" + 
				//						"<div class='input'><div class='inputLabel'>Image name</div><input type='text' name='photoName[]' value='" + albumName + "-" + LatestUploadCount + "' /></div>" + 
				//						"<div class='input'><div class='inputLabel'>Alt</div><input type='text' name='photoAlt[]' placeholder='alt' value='" + albumName + "-" + LatestUploadCount + "' /></div>" + 
				//						"<div class='input' style='margin-bottom:0px;'><div class='inputLabel'>Title</div><input type='text' name='photoTitle[]' value='" + albumName + "-" + LatestUploadCount + "' placeholder='title' /></div></div>");
			
				//LatestUploadCount++;
			//}
			
		}
		
		///$("#fileList").append();
		
		//adaption of this
		//https://www.nczonline.net/blog/2012/05/08/working-with-files-in-javascript-part-1/
		$("#UploadSizeTotal").html(formatBytes(TotalFileSize));
		$("span#FileUploadTotal").html(input.files.length);
		$("input#PhotoCount").val(input.files.length);
		$(".UploadPhotosSubmit").show();
		
	}
	

	
	function ToggleVisibleImage(id, checkbox) {
		var isChecked = $(checkbox).is(':checked');
		var $switchLabel = $('.slider#' + id);
		var TooltipText;
		
		if(isChecked) {
	        selectedData = $switchLabel.attr('data-off');
	        TooltipText = "Photo not visible";
	    } else {
	    	selectedData = $switchLabel.attr('data-on');
	    	TooltipText = "Photo visible";
	    }
	    
		$.post(Globals.WebsitePath() + "photos/photovisible/" + id, {visible: selectedData}, function(result){
	    	$switchLabel.attr("data-original-title", TooltipText);
	    });
	}
	
	function InitializePhotoSingle() {
		$("#editPhoto").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, EditPhotoResponses)		
		});
	} 

	
	function InitializeYearDirectory() {
		$("#YearDirectoryForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}

	function InitializeAlbumlist() {
		$("#ArchiveAlbumForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			
			Globals.AjaxPost(formURL, postData, NewAlbumResponses)		
		});
	}

	function InitializePhotoAlbumDetails() {
		$("#PhotoAlbumDetailsForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, formData, Globals.MultipleErrorResponses)		
		});
	}
	
	function InitializeYearDirectory() {
		$("#YearForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, formData, Globals.MultipleErrorResponses)		
		});
	}
	
	function AddAlbum() {
		$("#AddAlbumElement").show();
	}
	
	function ClosePopup() {
		$("#AddAlbumElement").hide();
	}
	
	function changeThumbNail(albumID) {
		$("#PhotosInAlbum").show();
		var photos = [];
		$.getJSON(Globals.WebsitePath() +  "photos/GetPhotosOfCurrentAlbum/" + albumID, function( data ) {
			$.each( data, function( key, val ) {
				var albumPath;
				var photoPath;
				switch(val.AlbumType) {
					case '1':
						albumPath = "/events/";
						break;
					case '2':
						albumPath = "/blog/";
						break;
					case '3':
						albumPath = "/misc/"
						break;
				}
				console.log(data);
				
				photoPath = Globals.PhotosPath() + val.ParentDirectory + albumPath + val.albumFolderName + "/" + val.photoName + "-s." + val.ext;
				//var dateParts = val.edittedDate.split('-');
						
				photos.push("<a href='javascript:void(0);' onclick='PhotosController.SelectPhoto("+ val.albumID +", " + val.photoID +")'><div style='background:url(" + photoPath + ") no-repeat center;' class='PhotoThumbNail'></div></a>" );
			});
			
			$(photos.join("")).appendTo("#PhotosInAlbum .FormContent"); 
		});
	}
	
	function SelectPhoto(albumID, photoID) {	
		Globals.AjaxPost(Globals.WebsitePath() + "photos/savethumb/" + albumID, {
																					albumThumbNail : photoID
																				}, SaveThumbResponses)	
		//$("#albumThumbNail").val(photoID)
	}
	
	function closeSelectPhoto() {
		$("#PhotosInAlbum").hide();
		$("#PhotosInAlbum .FormContent").html(''); 
	}
	
	function UploadPhotos() {
		
	}
	
	function CopyUrl(text) {
		  var textArea = document.createElement("textarea");
		
		  //
		  // *** This styling is an extra step which is likely not required. ***
		  //
		  // Why is it here? To ensure:
		  // 1. the element is able to have focus and selection.
		  // 2. if element was to flash render it has minimal visual impact.
		  // 3. less flakyness with selection and copying which **might** occur if
		  //    the textarea element is not visible.
		  //
		  // The likelihood is the element won't even render, not even a flash,
		  // so some of these are just precautions. However in IE the element
		  // is visible whilst the popup box asking the user for permission for
		  // the web page to copy to the clipboard.
		  //
		
		  // Place in top-left corner of screen regardless of scroll position.
		  textArea.style.position = 'fixed';
		  textArea.style.top = 0;
		  textArea.style.left = 0;
		
		  // Ensure it has a small width and height. Setting to 1px / 1em
		  // doesn't work as this gives a negative w/h on some browsers.
		  textArea.style.width = '2em';
		  textArea.style.height = '2em';
		
		  // We don't need padding, reducing the size if it does flash render.
		  textArea.style.padding = 0;
		
		  // Clean up any borders.
		  textArea.style.border = 'none';
		  textArea.style.outline = 'none';
		  textArea.style.boxShadow = 'none';
		
		  // Avoid flash of white box if rendered for any reason.
		  textArea.style.background = 'transparent';
		
		
		  textArea.value = text;
		
		  document.body.appendChild(textArea);
		
		  textArea.select();
		
		  try {
		    var successful = document.execCommand('copy');
		    var msg = successful ? 'successful' : 'unsuccessful';
		    console.log('Copying text command was ' + msg);
		  } catch (err) {
		    console.log('Oops, unable to copy');
		  }
		
		  document.body.removeChild(textArea);
		}
	
	return {
		InitializePhotoAlbum: InitializePhotoAlbum,
		InitializeAddPhotos: InitializeAddPhotos,
		MakeFileList: MakeFileList,
		ToggleVisibleImage: ToggleVisibleImage,
		InitializePhotoSingle: InitializePhotoSingle,
		InitializeMiscPhotoAlbum: InitializeMiscPhotoAlbum,
		InitializeYearDirectory: InitializeYearDirectory,
		InitializePhotoAlbumDetails: InitializePhotoAlbumDetails,
		AddAlbum: AddAlbum,
		ClosePopup: ClosePopup,
		InitializeAlbumlist: InitializeAlbumlist,
		changeThumbNail: changeThumbNail,
		SelectPhoto: SelectPhoto,
		closeSelectPhoto: closeSelectPhoto,
		UploadPhotos: UploadPhotos,
		CopyUrl: CopyUrl,
		InitializeYearDirectory: InitializeYearDirectory 
	}
}();
