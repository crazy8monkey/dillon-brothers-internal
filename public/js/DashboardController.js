var DashboardController = function() {
	
	function GenerateDillonBrothersMainAnalytics(token) {
		gapi.analytics.ready(function() {

		  /**
		   * Authorize the user with an access token obtained server side.
		   */
		  gapi.analytics.auth.authorize({
		    'serverAuth': {
		      'access_token': token
		    }
		  });
		
		
		  /**
		   * Creates a new DataChart instance showing sessions over the past 30 days.
		   * It will be rendered inside an element with the id "chart-1-container".
		   */
		  var dataChart1 = new gapi.analytics.googleCharts.DataChart({
		    query: {
		      'ids': 'ga:147133427', // <-- Replace with the ids value for your view.
		      'start-date': '30daysAgo',
		      'end-date': 'yesterday',
		      'metrics': 'ga:sessions,ga:users',
		      'dimensions': 'ga:date'
		    },
		    chart: {
		      'container': 'chart-1-container',
		      'type': 'LINE',
		      'options': {
		        'width': '100%'
		      }
		    }
		  });
		  dataChart1.execute();

		
		});
	}	
	return {
		GenerateDillonBrothersMainAnalytics: GenerateDillonBrothersMainAnalytics
	}
}();
