var EmployeeController = function() {
	
	function InitializeEmployeeForm() {
		$("#EmployeeAddForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, formData, Globals.MultipleErrorResponses)		
		});
		//http://digitalbush.com/projects/masked-input-plugin/#demo
		$("#employeePhone").mask("(999) 999-9999",{placeholder:" "});
	}

	return {
		InitializeEmployeeForm: InitializeEmployeeForm 
	}
}();
