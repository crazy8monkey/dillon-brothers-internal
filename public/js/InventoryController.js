var InventoryController = function() {
	
	function GetSpecJSonValue() {
		return JSON.parse($("#SpecJSON").val());
	}
	

	function InitializeInventorySingle() {
		$("#SingleInventory").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});		
		
		$("#CurrentInventoryFeedBack").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, 
				function(responses) {
					if(responses.ValidationErrors) {
						var validationMessagesArray = responses.ValidationErrors;
						
						var ValidationErrors = responses.ValidationErrors.length;
						for (var i = 0; i < ValidationErrors; i++) {
						   $("#" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
						   $("#" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
						   $("#" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
						   $("#" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
						}
						
						alert("Please fix the errors on the screen");
						$("#uploadingPhotosText, #ToastMessage").hide();
						
					}
					
					if(responses.newFeedBack) {
						
						if($("#FeedBackList").html() == "There is no feed back on this item") {
							$("#FeedBackList").html('');
						}
						$("#internalFeedBackNotes").val('');
						$("#newFeedBackItem").clone().prependTo("#FeedBackList");
						$("#newFeedBackItem .bubble").html(responses.newFeedBack.Text + '<div class="caret"></div>');
						$("#newFeedBackItem .detailsBy").html(responses.newFeedBack.SubmittedByDetails);
						$("#newFeedBackItem").fadeIn();
						$("#ToastMessage").hide();
					}
				}
			);		
		});		
		
		
	}
	
	function BackupInventorySingle() {
		$("#SpecInventoryFeedBack").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, 
				function(responses) {
					if(responses.ValidationErrors) {
						var validationMessagesArray = responses.ValidationErrors;
						
						var ValidationErrors = responses.ValidationErrors.length;
						for (var i = 0; i < ValidationErrors; i++) {
						   $("#" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
						   $("#" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
						   $("#" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
						   $("#" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
						}
						
						alert("Please fix the errors on the screen");
						$("#uploadingPhotosText").hide();
						$("#ToastMessage").hide();
					}
					
					if(responses.newFeedBack) {
						
						if($("#FeedBackList").html() == "There is no feed back on this item") {
							$("#FeedBackList").html('');
						}
						
						$("#internalFeedBackNotes").val('');
						$("#newFeedBackItem").clone().prependTo("#FeedBackList");
						$("#newFeedBackItem .bubble").html(responses.newFeedBack.Text + '<div class="caret"></div>');
						$("#newFeedBackItem .detailsBy").html(responses.newFeedBack.SubmittedByDetails);
						$("#newFeedBackItem").fadeIn();
						$("#ToastMessage").hide();
					}
				}
			);		
		});
		
		
		
		$("#DeleteBackupForm, #ReassignVinForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
				
	}
	
	function FindJSONKey(labelID) {
		var JSONKey;
		
		var specJSON = GetSpecJSonValue();
		
		for (var i=0; i < specJSON.length; i++) {
			//console.log(specJSON[i].LabelID);
			if(specJSON[i].LabelID == labelID) {
				JSONKey = i;		
			}
		}
		
		return JSONKey;
	}
	
	function EditLabelSpec(key, element) {
		var specJSON = GetSpecJSonValue();
		specJSON[key]['Label'] = $(element).val();
		$("#SpecJSON").val(JSON.stringify(specJSON));
	}
	
	function escapeHtml(text) {
	  var map = {
	    '&': '&amp;',
	    '<': '&lt;',
	    '>': '&gt;',
	    '"': '&quot;',
	    "'": '&#039;'
	  };
	
	  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
	}
	
	function EditLabelContent(element,labelID) {
		var specJSON = GetSpecJSonValue();
		var JSONKey = FindJSONKey(labelID);
		
		 if (specJSON.length == 0) {	 	
		 	specJSON.push({"LabelID" : labelID, "Content": escapeHtml($(element).val())});
		 } else if(specJSON[JSONKey] == null){
		 	specJSON.push({"LabelID" : labelID, "Content": escapeHtml($(element).val())});
		 } else {
		 	specJSON[JSONKey]['Content'] = escapeHtml($(element).val());
		 }
		console.log(JSONKey);
		
		$("#SpecJSON").val(JSON.stringify(specJSON));
	}
	
	function RemoveSpecRow(key) {
		var specJSON = GetSpecJSonValue();
		
		specJSON.splice(key, 1);	
		$(".inventorySpecKey#" + key).remove();
			
		$("#SpecJSON").val(JSON.stringify(specJSON));
		
		$(".inventorySpecKey").each(function(index) {
			$(this).attr("id", index);
			
			var onclickVariable = "InventoryController.RemoveSpecRow('" + index + "')";	
			var EditKeyVariable = "InventoryController.EditLabelSpec('" + index + "', this)"
					
			$(this).find('.deleteSpec').attr("onclick", onclickVariable);
			$(this).find('.specLabel').attr("onclick", EditKeyVariable);
		});
	}
	
	function ViewBackupInventoryHistory(id) {
		$("#BackupHistoryPopup").show();
		var HistoryLog = [];
		$.getJSON(Globals.WebsitePath() +  "inventory/GetBackupHistory/" + id, function( data ) {
			$.each( data, function( key, val ) {
				HistoryLog.push("<div>" + val.value +"</div>" );
			});
			
			$(HistoryLog.join("")).appendTo("#BackupHistoryPopup .FormContent"); 
		});
	
		
	}
	
	function closeBackupHistory() {
		$("#BackupHistoryPopup").hide();
		$("#BackupHistoryPopup .FormContent").html('');
	}
	
	function AddPhotosToInventory(id) {
		newwindow=window.open(Globals.WebsitePath() + 'inventory/addinventoryphotos/' + id,'name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;
	}
	
	function ViewPhoto(id) {
		newwindow=window.open(Globals.WebsitePath() + 'inventory/editphoto/' + id,'name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;
	}
	
	function InitializePhotoUpload() {
		
	}
	
	function AddInventoryPhotoUpload() {
		$("#NewFileUploadElement").clone().show().appendTo("#PhotoUploadContent");
	}
	
	function InitializePhotoEdit() {
		$("#editPhoto").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, function(responses) {
													if(responses.ValidationErrors) {
														var validationMessagesArray = responses.ValidationErrors;
														
														var ValidationErrors = responses.ValidationErrors.length;
														for (var i = 0; i < ValidationErrors; i++) {
														   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
														   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
														   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
														   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
														}
														
														alert("Please fix the errors on the screen");
														$("#uploadingPhotosText").hide()
													}
													
													
													if(responses.saved) {
														window.close()
													}
													
												})		
		});		
	}
	
	//might do this later by putting the inventory list into knockout
	function GetStoreInventoryViewModel() {
		var self = this;
		self.currentStoreInventory = ko.observableArray([]);
		$.getJSON(Globals.WebsitePath() + 'inventory/GetCurrentInventory', {storeID: $("#storeID").val()}, function( data ) {
			self.currentStoreInventory(data.storeInventory);	
		});
		
		self.grouped = ko.computed(function () {
			var rows = [], current = [];
		    rows.push(current);
		    for (var i = 0; i < self.currentStoreInventory.length; i += 1) {
		    	current.push(self.currentStoreInventory[i]);
		            if (((i + 1) % 3) === 0) {
		                current = [];
		                rows.push(current);
		            }
		        } 
		        return rows;
	    }, self);
	
	}
	
	function InitializeStoreInventory() {
		
		
		//ko.applyBindings(new GetStoreInventoryViewModel());
		
		
		$("#FilteredForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, function(responses) {
													if(responses.ValidationErrors) {
														var validationMessagesArray = responses.ValidationErrors;
														
														var ValidationErrors = responses.ValidationErrors.length;
														var message = '';
														for (var i = 0; i < ValidationErrors; i++) {
														   message += validationMessagesArray[i].errorMessage + ' / ';
														}
														
														alert("Please fix the errors on the screen");
														$("#FilterMessageErrors").html("Errors: " + message.slice(0, -3));
													}
													
													
													if(responses.redirect) {
											 			Globals.redirect(responses.redirect); 
											 		}
													
												})		
		});		
	}
	
	function ToggleStoreDropDown() {
		$(".StoreDropDownElement").toggle();	
	}
	
	function VehicleVerifiedToggle(element, id) {
		
		var verificationValue='';
		
		if(element.checked) {
			verificationValue = 1;
			$("#vehicleVerifiedText").show();
		} else {
			verificationValue = 0;
			$("#vehicleVerifiedText").hide();
		}
		
		
		$.post(Globals.WebsitePath() + "inventory/VerifiedBackup/" + id, {verified: verificationValue}, function(result){
	    	//$switchLabel.attr("data-original-title", TooltipText);
	    });
	}
	
	function InitializeBackupInventory() {
		$("#BackupFilterList").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, function(responses) {
													if(responses.ValidationErrors) {
														var validationMessagesArray = responses.ValidationErrors;
														
														var ValidationErrors = responses.ValidationErrors.length;
														for (var i = 0; i < ValidationErrors; i++) {
														   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
														   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
														   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
														   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
														}
														
														alert("Please fix the errors on the screen");
														$("#uploadingPhotosText").hide()
													}
													
													
													if(responses.redirect) {
														Globals.redirect(responses.redirect); 
											 		}
													
												})		
		});		
		
		
		$("#ReassignVin").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function QuickBikeDetails(id) {
		$("#BackupInventoryPopup").show();
		var LinkedVins = '';
		$.getJSON(Globals.WebsitePath() +  "inventory/BackupInfo/" + id, function( data ) {
			$("#savedYear").html(data.savedBike.inventoryBackupYear);
			$("#savedMake").html(data.savedBike.inventoryBackupManufactur);
			$("#savedModelName").html(data.savedBike.inventoryBackupModelName);
			$("#savedFriendlyModelName").html(data.savedBike.inventoryBackupModelFriendlyName);
			$("#savedCategory").html(data.savedBike.CategoryName);
			
			
			$("#savedTalonYear").html(data.savedBike.TalonYear);
			$("#savedTalonMake").html(data.savedBike.TalonMake);
			$("#savedTalonModelName").html(data.savedBike.TalonModel);
			
			
			
			$.each(data.VINS, function( key, val ) {
				LinkedVins += val.backedupVinNumber + '  ';
			});
			
			$("#Vins").html(LinkedVins); 			
		});
		
	}
	
	function CloseQuickBikeDetails() {
		$("#BackupInventoryPopup").hide();
	}
	
	function OpenDeleteBikePopup(id) {
		$("#DeleteBackupPopup").show();
	}
	
	function CloseDeleteBikePopup() {
		$("#DeleteBackupPopup").hide();
	}
	
	function VinDetails(VIN, id) {
		$("#VinNumberDetails").show();
		$(".Popup #VinNumberHeader").html(VIN);
		
		$("#vinIDObject").val(id);
		
		//$("#deleteVIN").attr('href', Globals.WebsitePath() + 'inventory/delete/vin/' + id);
		
	}
	
	function CloseVinDetails() {
		$("#VinNumberDetails").hide();
	}
	
	
	function replaceQueryString(url,param,value) {
	    var re = new RegExp("([?|&])" + param + "=.*?(&|$)","i");
	    if (url.match(re))
	        return url.replace(re,'$1' + param + "=" + value + '$2');
	    else
	        return url + '&' + param + "=" + value;
	}
	
	function ChangeSortingOrder(element) {
		var urlPath = document.location.href.split('/');
		
		
		window.location.href = replaceQueryString(document.location.href, 'SortBy', $(element).val());		
	}
	
	function ManageVINNumbers() {
		$("#ManageVinNumbers").show();
	}
	
	function CloseManageVINNumbers() {
		$("#ManageVinNumbers").hide();
	}
	
	return {
		RemoveSpecRow: RemoveSpecRow,
		ViewBackupInventoryHistory: ViewBackupInventoryHistory,
		closeBackupHistory: closeBackupHistory,
		EditLabelSpec: EditLabelSpec,
		EditLabelContent: EditLabelContent,
		InitializeInventorySingle: InitializeInventorySingle,
		AddPhotosToInventory: AddPhotosToInventory,
		AddInventoryPhotoUpload: AddInventoryPhotoUpload,
		InitializePhotoUpload: InitializePhotoUpload,
		ViewPhoto: ViewPhoto,
		InitializePhotoEdit: InitializePhotoEdit,
		BackupInventorySingle: BackupInventorySingle,
		ToggleStoreDropDown: ToggleStoreDropDown ,
		InitializeStoreInventory: InitializeStoreInventory,
		VehicleVerifiedToggle: VehicleVerifiedToggle,
		InitializeBackupInventory: InitializeBackupInventory,
		QuickBikeDetails: QuickBikeDetails,
		CloseQuickBikeDetails: CloseQuickBikeDetails,
		OpenDeleteBikePopup: OpenDeleteBikePopup,
		CloseDeleteBikePopup: CloseDeleteBikePopup,
		VinDetails: VinDetails,
		CloseVinDetails: CloseVinDetails,
		ChangeSortingOrder: ChangeSortingOrder,
		ManageVINNumbers: ManageVINNumbers,
		CloseManageVINNumbers: CloseManageVINNumbers 
	}
}();
