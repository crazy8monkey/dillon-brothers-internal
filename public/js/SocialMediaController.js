var SocialMediaController = function() {

	function InitializePostForm(type) {
		CKEDITOR.replace( 'socialMediaContent', {	
			filebrowserUploadUrl: Globals.WebsitePath() + "socialmedia/uploadImages/" + type,
			filebrowserImageUploadUrl: Globals.WebsitePath() + "socialmedia/uploadImages/" + type,
		});
		
		$("#SocialMediaPostForm").submit(function(e){
			e.preventDefault();
			
			for ( instance in CKEDITOR.instances ) {
				CKEDITOR.instances[instance].updateElement();	
			}
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			
			Globals.AjaxPost(formURL, postData, ".loadingIndicator", Globals.MultipleErrorResponses)		
		});
		
		$("#DeclinePostForm").submit(function(e){
			e.preventDefault();
			
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			
			Globals.AjaxPost(formURL, postData, ".loadingIndicator", Globals.MultipleErrorResponses)		
		});
		
		
		
	}
	
	function ShowSubmittedPost(ID) {
		$.getJSON(Globals.WebsitePath() +  "socialmedia/single/" + ID, function( data ) {
			$("#PostContent").html(data.PostContent);
			
			if(data.KeyWords != '') {
				$("#KeyWords").html(data.KeyWords);	
				$("#ShowKeyWordContent").show();
			} else {
				$("#ShowKeyWordContent").hide();
			}
			
			
			$("#SubmittedByContent").html(data.submittedByFirstName + " " + data.submittedByLastName)
			
			
			$("#ShowSinglePost").show();
		});
		
		//PostContent
		
		
	}
	
	function CloseSubmittedPost() {
		$("#ShowSinglePost").hide();
	}
	
	function ShowDeclinedPostForm() {
		$("#DeclinePostPopup").show();
	}
	
	function CloseDeclinedPostForm() {
		$("#DeclinePostPopup").hide()
	}
	
	return {
		InitializePostForm: InitializePostForm,
		ShowSubmittedPost: ShowSubmittedPost,
		CloseSubmittedPost: CloseSubmittedPost,
		ShowDeclinedPostForm: ShowDeclinedPostForm,
		CloseDeclinedPostForm: CloseDeclinedPostForm
	}
}();
