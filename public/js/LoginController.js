var LoginController = function() {
	
	function LoginFormResponses(responses) {
		if(responses.errorMessage) {
			Globals.ShowErrorMessage("#LoginMessage");
			Globals.LoadingIndicator("#LoginLoading");
			$("#ToastMessage").hide();
			Globals.ShowResponseMessage("#LoadingMessage", "error", responses.errorMessage)
		}
		
		if(responses.redirect) {
			Globals.redirect(responses.redirect); 
		}
		
	}
	
	function ForgotPasswordResponses(responses) {
		if(responses.errorMessage) {
			Globals.ShowErrorMessage("#LoginMessage");
			Globals.LoadingIndicator("#LoginLoading");
			$("#ToastMessage").hide();
			Globals.ShowResponseMessage("#LoadingMessage", "error", responses.errorMessage)
		}
		
		if(responses.redirect) {
			Globals.ShowErrorMessage("#LoginMessage");
			Globals.LoadingIndicator("#LoginLoading");
			Globals.ShowResponseMessage("#LoadingMessage", "success", responses.success)
			
			Globals.redirect(responses.redirect, 3000); 
			$("#ToastMessage").hide();
		}
	}
	
	function InitializeLogin() {
		$("#loginForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, LoginFormResponses)		
		});
	} 
	
	function InitializeEmailCheckForm() {
		$("#EmailCheckForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, ForgotPasswordResponses)		
		});
	}
	
	function InitializePasswordResetForm() {
		$("#PasswordResetForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, ForgotPasswordResponses)		
		});
	}
	
	return {
		InitializeLogin: InitializeLogin,
		InitializeEmailCheckForm: InitializeEmailCheckForm,
		InitializePasswordResetForm: InitializePasswordResetForm
		
	}
}();
