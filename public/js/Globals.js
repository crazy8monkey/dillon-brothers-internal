jQuery.fn.extend({
	    toggleText: function (a, b){
	        var that = this;
	            if (that.text() != a && that.text() != b){
	                that.text(a);
	            }
	            else
	            if (that.text() == a){
	                that.text(b);
	            }
	            else
	            if (that.text() == b){
	                that.text(a);
	            }
	        return this;
	    }
});

jQuery.fn.donetyping = function(callback, delay){
  delay || (delay = 1000);
  var timeoutReference;
  var doneTyping = function(elt){
    if (!timeoutReference) return;
    timeoutReference = null;
    callback(elt);
  };

  this.each(function(){
    var self = $(this);
    self.on('keyup',function(){
      if(timeoutReference) clearTimeout(timeoutReference);
      timeoutReference = setTimeout(function(){
        doneTyping(self);
      }, delay);
    }).on('blur',function(){
      doneTyping(self);
    });
  });

  return this;
};

var Globals = function() {
	
	var elementFillOnResponse
	
	function closeMessaging() {
		$('.message').fadeOut('fast');
	}
	function hideFormOverlay() {
		$(".form-overlay").fadeOut("fast");
	}
	
	function ClosePopups() {
		$(".Overlay, .WhitePopupForm").fadeOut("fast");
	}
	
	
	function ShowErrorMessage(Element) {
		$(Element).show();
		$(Element).css("height", "36px");
	}
	
	function LoadingIndicator(Element) {
		$(Element).fadeOut("slow");
	}
	
	function ShowResponseMessage(Element, type, message) {
		switch(type) {
			case "error":
				$(Element).addClass("error");
				$(Element).removeClass("success");
				break;
			case "success":
				$(Element).addClass("success");
				$(Element).removeClass("error");
				break;
			
		}
		$(Element).fadeTo(0, 100);	
		$(Element).html(message);
		$(Element).delay(1500).fadeTo(3500, 0);
		
	}
	
	function redirect(string, delay) {
		if(delay) {
			setTimeout(function(){
				window.location.assign(string); 
			}, delay);
		} else {
			window.location.assign(string);
		}
	}
	
	function ToggleMobileMenu() {
		$(document).click(function (e) {
            if ($(e.target).closest('#hrefMobileMenu').length === 0) {
            	$("header .mobileView .menu").hide();
        	}
        });
			
		$("header .mobileView .menu").toggle();
	}
	
	function ToggleReferralMenu() {
		$(document).click(function (e) {
            if ($(e.target).closest('#hrefReferralMenu').length === 0) {
            	$(".MainMenu ul").hide();
        	}
        });
			
		$(".MainMenu ul").toggle();
	}
	
	function ToolTipMessage(msg) {
		
	}
	
	function MultipleErrorResponses(responses) {
		if(responses.ValidationErrors) {
			var validationMessagesArray = responses.ValidationErrors;
			
			var ValidationErrors = responses.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
			   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
			}
			
			alert("Please fix the errors on the screen");
			$("#uploadingPhotosText").hide();
			$(".LoadingSpinner").hide();
		}
		
		if(responses.MySqlError) {
			$("#ToastMessage").addClass('error');
 			$("#ToastMessage").hide().fadeIn().html(responses.MySqlError);
 		}
		
		if(responses.redirect) {
			$("#ToastMessage").addClass('success');
			$("#ToastMessage").removeClass('LoadingSpinner');
			
 			$("#ToastMessage").hide().fadeIn().html('Your changes has been saved, Please wait for the page to refresh');
 			Globals.redirect(responses.redirect); 
 		}
	}
	
	function removeFeedBackValidationMessage(Name) {
		$("#" + Name + " .errorMessage").html("");
		$("#" + Name + " input").removeAttr("style")
		$("#" + Name + " select").removeAttr("style")
		$("#" + Name + " textarea").removeAttr("style")
	}
	
	function removeValidationMessage(ID) {
		$("#inputID" + ID + " .errorMessage").html("");
		$("#inputID" + ID + " input").removeAttr("style")
		$("#inputID" + ID + " select").removeAttr("style")
		$("#inputID" + ID + " textarea").removeAttr("style")
	}
	
	function WebsitePath() {
		return 'http://localhost/DillonBrothers/';
	}
	
	function PhotosPath() {
		return 'http://localhost/DillonBrothersPhotos/';
	}
	
	function GetMetaDataString() {
		return $("#MetaDataString").val();
	}
	

	
	function MetaDataUpdateModifiedTime() {
		var date = new Date();
		var MetaDataValue = JSON.parse(GetMetaDataString());		
		
		var YearMonthDay = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
		var HourMinuteSecond = addZero(date.getHours()) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds());
		
		MetaDataValue['OpenGraphMetaData'].ArticleModifiedTime = YearMonthDay + "T" + HourMinuteSecond + "+6:00";
		$("#MetaDataString").val(JSON.stringify(MetaDataValue));
	}
	
	function addZero(i) {
	    if (i < 10) {
	        i = "0" + i;
	    }
	    return i;
	}
	
	function GetMonth(MonthInt) {
		var v  = MonthInt - 1
		
		
		var n = ["Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    	return n[v]
	}
	
	function FormatTime(TimeString) {
		var time = TimeString.split(':');
				
		var hours = time[0] > 12 ? time[0] - 12 : time[0];
		var minutes = time[1];
		var am_pm = time[0] >= 12 ? "PM" : "AM";
				
		return hours + ":" + minutes + " " + am_pm;
	}
	
	
	function textCounter(field, maxlimit) {  
		var text_length = $(field).val().length;
        var text_remaining = maxlimit - text_length;
		       
		console.log(text_remaining);
	     
			
	}
	
	function InitializeApp() {
		var source, chattext, last_data, chat_btn, conx_btn, disconx_btn, text;
		
		eventSource = new EventSource(Globals.WebsitePath() + 'updates/newchat');
		
		eventSource.addEventListener("message", function(event){
            console.log(last_data);
            //if(event.data != "") {
	            if(event.data != last_data && last_data != undefined){
	            	console.log(event.data);
	                var newNotificaiton = new Notification('New Chat Conversation', {
						body: 'A New Customer Chat Customer has started',
					});	        	
	            }
            //}
            last_data = event.data;
        });
	}
	
	
	return {
		redirect: redirect,
		AjaxPost: function(formURL, postData, success) {
			$.ajax({
				url: formURL,
		        type: 'POST',
		        cache: false, 
		        data: postData,
		        dataType: 'json',
				cache: false,
		        beforeSend: function(){
		            $("#ToastMessage").show();
					$("#ToastMessage").addClass('LoadingSpinner');
					$("#ToastMessage").html("<img src='" + Globals.WebsitePath() + "public/images/ajax-loader.gif' />");
    		    },
				success : success
			});	
		},
		AjaxFileUpload: function(formURL, postData, success) {
			
				$.ajax({
					url: formURL,
			        type: 'POST',
			        data: postData,
			        dataType: 'json',
					cache: false,
			        beforeSend: function() {
			        	$("#ToastMessage").show();
						$("#ToastMessage").addClass('LoadingSpinner');
						$("#ToastMessage").html("<img src='" + Globals.WebsitePath() + "public/images/ajax-loader.gif' />");
			        },
	    		    success : success,
					cache: false,
			        contentType: false,
			        processData: false
				});
			
			
		},
		removeValidationMessage: function(ID) {
			$("#inputID" + ID + " .errorMessage").html("");
			$("#inputID" + ID + "Name").remove()
		},
		ScrollToTop: function(element) {
			$('html, body').animate({
		        scrollTop: $(element).offset().top - 200
		    },1000);
		    $(".ajaxLoadingIndicator").hide();
		},
		ClosePopups: ClosePopups,
		ShowErrorMessage: ShowErrorMessage,
		LoadingIndicator: LoadingIndicator,
		ShowResponseMessage: ShowResponseMessage,
		ToggleMobileMenu: ToggleMobileMenu,
		ToggleReferralMenu: ToggleReferralMenu,
		ToolTipMessage: ToolTipMessage,
		removeValidationMessage: removeValidationMessage,
		MultipleErrorResponses: MultipleErrorResponses,
		WebsitePath: WebsitePath,
		MetaDataUpdateModifiedTime: MetaDataUpdateModifiedTime,
		GetMonth: GetMonth,
		FormatTime: FormatTime,
		PhotosPath: PhotosPath,
		textCounter: textCounter,
		removeFeedBackValidationMessage: removeFeedBackValidationMessage,
		InitializeApp: InitializeApp
	
	}	
	
}();