var EventsController = function() {

	//private
	function GetJSONRowIndex(rowID) {
		var JSONValue = GetPageJSONValue();
		var CurrentIndex
		
		for (var i=0; i<JSONValue.length; i++) {
			if(JSONValue[i].rowID == rowID) {
				CurrentIndex = i;	
			}
		}
		
		return CurrentIndex;
	}
	
	function GetJSONColumnIndex(RowIndex, ColumnID) {
		var JSONValue = GetPageJSONValue();
		var CurrentColumnIndex;
		
		for (var i=0; i < JSONValue[RowIndex]["Columns"].length; i++) {
			if(JSONValue[RowIndex].Columns[i].columnID == ColumnID) {
				CurrentColumnIndex = i;
			}
		}
		return CurrentColumnIndex;
	}
	
	function GetJSONContentIndex(RowIndex, ColumnIndex, ContentID) {
		var JSONValue = GetPageJSONValue();
		var CurrentColumnIndex;
		
		
		
		
		if(JSONValue[RowIndex].Columns[ColumnIndex]['Content'] != null) {
			for (var i=0; i < JSONValue[RowIndex]["Columns"][ColumnIndex]["Content"].length; i++) {
				if(JSONValue[RowIndex].Columns[ColumnIndex].Content[i].ContentID == ContentID) {
					CurrentContentIndex = i;		
				}
			}
			
		} else {
			CurrentContentIndex = 0;	
		}
		
			
		return CurrentContentIndex;
	}
	
	
	function ChangeColumnElements(ElementSelector, columnNumber) {
		$(ElementSelector).attr('class', 'ColumnElement col-md-'+ columnNumber + ' ui-droppable ui-sortable-handle');
		$(ElementSelector).attr('data-columnid', columnNumber);
	}
	
	
	function JSONKeyExists(obj) {
	   return obj != null
	}
	
	//public
	function InitializeSingleEvent() {
		$("#EventForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, postData, Globals.MultipleErrorResponses)		
		});
		
		//var widgets = $('#draggableWidgets').offset().top;
		
		//$(window).scroll(function() {
		//	if ( $(window).scrollTop() >= widgets ) {
		//		$('#draggableWidgets').addClass('fixedPositionWidets');
		//	} else {
		//		$('#draggableWidgets').removeClass('fixedPositionWidets');
		//	}
		//})
		
	}

	function InitializeSuperEventSingle() {
		$("#EventForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	
	function AddLayoutRow() {
		var JSONValue = GetPageJSONValue();
		
		var lastInsertedRow;
		var Order;
		
		//alert(lastInsertedRow)
		
		if(Object.keys(JSONValue).length) {
			lastInsertedRow = JSONValue[Object.keys(JSONValue).length-1].rowID;
			Order = Object.keys(JSONValue).length + 1;
		} else {
			lastInsertedRow = 0;
			Order = 1;
		}
		
		JSONValue.push({"rowID" : parseInt(lastInsertedRow + 1), "Order": Order});
		
		var NewRowID = parseInt(lastInsertedRow + 1);
		
		var AddColumnClickEvent = 'EventsController.AddColumnInRow("row' + NewRowID + '", "' + NewRowID + '")';
		var DeleteRowClickEvent = 'EventsController.DeleteRow("' + NewRowID + '")'
		
		
	
		$( "#NewRowElement" ).find("a#AddColumnInRow").attr('onclick', AddColumnClickEvent);
		$( "#NewRowElement" ).find("a#DeleteRow").attr('onclick', DeleteRowClickEvent);
		$( "#NewRowElement" ).find(".rowButton").attr('id', NewRowID);
		
		
		
		$( "#NewRowElement" ).find('.slider table').attr('id', 'rangeRow' + NewRowID);
		$( "#NewRowElement" ).clone().appendTo( "#NewContentPlacment" ).attr("id", NewRowID).removeAttr("style").find(".row.ColumnRowContent").attr("id", "row" + NewRowID).find("a#AddColumnInRow").attr('onclick', AddColumnClickEvent);
		
		
		
		//EventsController.ColumnResizable(ID);
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));		
		
		setTimeout(function(){
			EventsController.ColumnResizable(NewRowID);
			console.log('hi');
		}, 700);
	}
	
	
	
	function AddColumnInRow(Row, ID) {
		
		var element = document.getElementById(Row);
			
		EditJSONStringRow(ID, 'Add');
		
		EventsController.SortableColumns(ID);
		
		setTimeout(function(){
			ResetColumnResizable(ID, element.getElementsByClassName('ColumnElement').length, "add");	
		}, 700);
		
		EventsController.DroppableLines(ID);
	}
	
	
	function AddContentWidget(rowID, ColumnID, ContentID, Element) {
		var JSONValue = GetPageJSONValue();
		
		var CurrentIndex = GetJSONRowIndex(rowID);
		var CurrentColumnIndex = GetJSONColumnIndex(CurrentIndex, ColumnID);
		var CurrentContentIndex = GetJSONContentIndex(CurrentIndex, CurrentColumnIndex, ContentID);
		
		//JSONValue[CurrentIndex].Columns[CurrentColumnIndex].Content[CurrentContentIndex]['Value'] = encodeURIComponent($(Element).val());
		JSONValue[CurrentIndex].Columns[CurrentColumnIndex].Content[CurrentContentIndex]['Value'] = $(Element).val();
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
	}
	
	
	function DeleteColumn(rowID, ColumnID, rowIDNumber) {
	
		$("#" + rowID + " .ColumnElement#" + ColumnID).remove();
		EditJSONStringRow(rowIDNumber, 'Delete');	
		
		setTimeout(function(){
			ResetColumnResizable(rowIDNumber, $('#' + rowID + ' .ColumnElement').length, "delete");		
		}, 700);
		
		
	}
	
	function EditJSONStringRow(rowID, action) {
		var JSONValue = GetPageJSONValue();
		var CurrentRowIndex = GetJSONRowIndex(rowID);
		
		var RowCount = '';
		var className;
		var dataColumnID;
		
		if(action == 'Delete') {
			var CurrentColumnIndex = GetJSONColumnIndex(CurrentRowIndex, rowID);
			
			JSONValue[CurrentRowIndex].Columns.splice(CurrentColumnIndex,1);
			
			if(JSONValue[CurrentRowIndex]['Columns'] != null) {
				RowCount = JSONValue[CurrentRowIndex].Columns.length;				
			} else {
				RowCount = 0;
			}
			
			if(!JSONValue[CurrentRowIndex]['Columns'].length) {
				delete JSONValue[CurrentRowIndex]['Columns'];
				
			} 
		} 
		
		if(action == 'Add') {
			if(JSONValue[CurrentRowIndex]["Columns"] != null) {
				var LatestColumnIndex = parseInt((JSONValue[CurrentRowIndex].Columns.length) - 1);
				
				LatestOrder = JSONValue[CurrentRowIndex].Columns[LatestColumnIndex].Order;
				LatestColumnID = Number(JSONValue[CurrentRowIndex].Columns[LatestColumnIndex].columnID);
			} else {
				var LatestColumnIndex = 0;
				LatestOrder = Number(0);
				LatestColumnID = Number(0);
			}
			
			
			if(JSONValue[CurrentRowIndex]['Columns'] != null) {
				JSONValue[CurrentRowIndex]['Columns'].push({"columnID":parseInt(LatestColumnID) + 1,"ClassID": "", "Order": parseInt(LatestOrder + 1)});	
			} else {
				$.extend( JSONValue[CurrentRowIndex], {"Columns" : [{"columnID":parseInt(LatestColumnID + 1),"ClassID": 12, "Order": parseInt(LatestOrder + 1)}]});
			}	
			
			if(JSONValue[CurrentRowIndex]['Columns'] != null) {
				RowCount = JSONValue[CurrentRowIndex].Columns.length;				
			} else {
				RowCount = 1;
			}
			
			//var LatestOrder = 0;
			//var LatestColumnID = 0;
			
			
			
			console.log("New ColumnID: " + parseInt(LatestColumnID + 1) )
			var OnClickEvent = 'EventsController.DeleteColumn("row' + rowID + '", "'+ parseInt(LatestColumnID + 1)  + '", "' + rowID  + '")';
			
			var NewColumnElement = "<div class='ColumnElement' id='" + LatestColumnID + "' data-ColumnID='" + dataColumnID + "'>" + 
									"<div class='delete'>" +
										"<a href='javascript:void(0);'>" +
											"<i class='fa fa-trash' aria-hidden='true'></i>" +
										"</a>" +
									"</div>" +
									"<div class='Empty'><div class='AddWidget'></div></div></div>";
			
			$( "#NewColumnElement" ).clone().appendTo( "#row" +rowID ).removeAttr("style").attr("class", "ColumnElement  ui-droppable ui-sortable-handle").attr("id", parseInt(LatestColumnID + 1)).find(".delete a").attr('onclick', OnClickEvent);
			
			
		}
		
			//alert(RowCount);	
			
			//delete switch case
			switch(RowCount) {
				
				case 1:
					ChangeColumnElements("#row" +rowID + " .ColumnElement", 12);
					if(JSONValue[CurrentRowIndex]['Columns'] != null) {
						JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 12;
						JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;	
					}
					
					
					break;	
				case 2:
					
					ChangeColumnElements("#row" +rowID + " .ColumnElement", 6);	
					
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 6;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 6;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					
					break;
				case 3:
					
					ChangeColumnElements("#row" +rowID + " .ColumnElement", 4);	
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 4;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 4;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 4;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					break;
				case 4:
					ChangeColumnElements("#row" +rowID + " .ColumnElement", 3);	
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 3;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 3;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 3;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['ClassID'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['Order'] = 4;	
					
					break;
				case 5:
					$("#row" +rowID + " .ColumnElement").each(function(index) {
						if(index == 0) {
							ChangeColumnElements(this, 3);
						} else if(index == 1) {
							ChangeColumnElements(this, 3);
						} else {
							ChangeColumnElements(this, 2);
						}
					});	

					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 3;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 3;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[3]['Order'] = 4;	
					JSONValue[CurrentRowIndex].Columns[4]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[4]['Order'] = 5;
					
					break;
				case 6:
					ChangeColumnElements("#row" +rowID + " .ColumnElement", 2);	
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[3]['Order'] = 4;	
					JSONValue[CurrentRowIndex].Columns[4]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[4]['Order'] = 5;
					JSONValue[CurrentRowIndex].Columns[5]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[5]['Order'] = 6;
					break;
				case 7:
					$("#row" +rowID + " .ColumnElement").each(function(index) {
						if(index == 0) {
							ChangeColumnElements(this, 2);
						} else if(index == 1) {
							ChangeColumnElements(this, 2);
						} else if(index == 2) {
							ChangeColumnElements(this, 2);
						} else if(index == 3) {
							ChangeColumnElements(this, 2);
						} else if(index == 4) {
							ChangeColumnElements(this, 2);
						} else {
							ChangeColumnElements(this, 1);
						}
					});	
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[3]['Order'] = 4;	
					JSONValue[CurrentRowIndex].Columns[4]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[4]['Order'] = 5;
					JSONValue[CurrentRowIndex].Columns[5]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[5]['Order'] = 6;
					JSONValue[CurrentRowIndex].Columns[6]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[6]['Order'] = 7;
					break
				case 8:
					$("#row" +rowID + " .ColumnElement").each(function(index) {
						if(index == 0) {
							ChangeColumnElements(this, 2);
						} else if(index == 1) {
							ChangeColumnElements(this, 2);
						} else if(index == 2) {
							ChangeColumnElements(this, 2);
						} else if(index == 3) {
							ChangeColumnElements(this, 2);
						} else {
							ChangeColumnElements(this, 1);
						}
					});	
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[3]['Order'] = 4;	
					JSONValue[CurrentRowIndex].Columns[4]['ClassID'] = 1
					JSONValue[CurrentRowIndex].Columns[4]['Order'] = 5;
					JSONValue[CurrentRowIndex].Columns[5]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[5]['Order'] = 6;
					JSONValue[CurrentRowIndex].Columns[6]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[6]['Order'] = 7;
					JSONValue[CurrentRowIndex].Columns[7]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[7]['Order'] = 8;
					break;
				case 9:
					$("#row" +rowID + " .ColumnElement").each(function(index) {
						if(index == 0) {
							ChangeColumnElements(this, 2);
						} else if(index == 1) {
							ChangeColumnElements(this, 2);
						} else if(index == 2) {
							ChangeColumnElements(this, 2);
						} else {
							ChangeColumnElements(this, 1);
						}
					});	
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[3]['Order'] = 4;	
					JSONValue[CurrentRowIndex].Columns[4]['ClassID'] = 1
					JSONValue[CurrentRowIndex].Columns[4]['Order'] = 5;
					JSONValue[CurrentRowIndex].Columns[5]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[5]['Order'] = 6;
					JSONValue[CurrentRowIndex].Columns[6]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[6]['Order'] = 7;
					JSONValue[CurrentRowIndex].Columns[7]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[7]['Order'] = 8;
					JSONValue[CurrentRowIndex].Columns[8]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[8]['Order'] = 9;
					break;
				case 10:
					$("#row" +rowID + " .ColumnElement").each(function(index) {
						if(index == 0) {
							ChangeColumnElements(this, 2);
						} else if(index == 1) {
							ChangeColumnElements(this, 2);
						} else {
							ChangeColumnElements(this, 1);
						}
					});	
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[3]['Order'] = 4;	
					JSONValue[CurrentRowIndex].Columns[4]['ClassID'] = 1
					JSONValue[CurrentRowIndex].Columns[4]['Order'] = 5;
					JSONValue[CurrentRowIndex].Columns[5]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[5]['Order'] = 6;
					JSONValue[CurrentRowIndex].Columns[6]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[6]['Order'] = 7;
					JSONValue[CurrentRowIndex].Columns[7]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[7]['Order'] = 8;
					JSONValue[CurrentRowIndex].Columns[8]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[8]['Order'] = 9;
					JSONValue[CurrentRowIndex].Columns[9]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[9]['Order'] = 10;
					break;
				case 11:
					$("#row" +rowID + " .ColumnElement").each(function(index) {
						if(index == 0) {
							ChangeColumnElements(this, 2);
						} else {
							ChangeColumnElements(this, 1);
						}
					});	
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 2;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[3]['Order'] = 4;	
					JSONValue[CurrentRowIndex].Columns[4]['ClassID'] = 1
					JSONValue[CurrentRowIndex].Columns[4]['Order'] = 5;
					JSONValue[CurrentRowIndex].Columns[5]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[5]['Order'] = 6;
					JSONValue[CurrentRowIndex].Columns[6]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[6]['Order'] = 7;
					JSONValue[CurrentRowIndex].Columns[7]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[7]['Order'] = 8;
					JSONValue[CurrentRowIndex].Columns[8]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[8]['Order'] = 9;
					JSONValue[CurrentRowIndex].Columns[9]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[9]['Order'] = 10;
					JSONValue[CurrentRowIndex].Columns[10]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[10]['Order'] = 11;
					
					$("#" +rowID + ".AddColumn").show()	
					break;
				case 12:
					ChangeColumnElements("#row" +rowID + " .ColumnElement", 1);
					$("#" +rowID + ".AddColumn").hide()	
					JSONValue[CurrentRowIndex].Columns[0]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[0]['Order'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[1]['Order'] = 2;
					JSONValue[CurrentRowIndex].Columns[2]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[2]['Order'] = 3;
					JSONValue[CurrentRowIndex].Columns[3]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[3]['Order'] = 4;	
					JSONValue[CurrentRowIndex].Columns[4]['ClassID'] = 1
					JSONValue[CurrentRowIndex].Columns[4]['Order'] = 5;
					JSONValue[CurrentRowIndex].Columns[5]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[5]['Order'] = 6;
					JSONValue[CurrentRowIndex].Columns[6]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[6]['Order'] = 7;
					JSONValue[CurrentRowIndex].Columns[7]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[7]['Order'] = 8;
					JSONValue[CurrentRowIndex].Columns[8]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[8]['Order'] = 9;
					JSONValue[CurrentRowIndex].Columns[9]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[9]['Order'] = 10;
					JSONValue[CurrentRowIndex].Columns[10]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[10]['Order'] = 11;
					JSONValue[CurrentRowIndex].Columns[11]['ClassID'] = 1;
					JSONValue[CurrentRowIndex].Columns[11]['Order'] = 12;
					break;
			}
		
		

		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
	}
	
	
	
	function UpdateRowOrder() {
		
		var JSONValue = GetPageJSONValue();
		
		var NewOrder = 1;
		
		var NewOrderArray = []

		$("#LayoutContent .ContentRow").each(function(index) {
			NewOrderArray.push({
	            RowID : $(this).attr("id"),
	            NewOrder : NewOrder
	        });
			NewOrder++;
		});
		
		//loop through new order array 
		for(var i=0; i< NewOrderArray.length; i++) {
			//loop through current JSON Object
			for(var j=0; j< JSONValue.length; j++) {
				if(JSONValue[j].rowID == NewOrderArray[i].RowID) {
					JSONValue[j]['Order'] = NewOrderArray[i].NewOrder;
				}
				
			}
		}
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
	}
	
	function GetPageJSONValue() {
		return JSON.parse($("#HtmlEventJSON").val());
	}
	
	function ResizeRows(RowID, ColumnSizes) {
		console.log(ColumnSizes);
	}
	
	function DeleteRow(rowID) {
		var JSONValue = GetPageJSONValue();
		var CurrentIndex = GetJSONRowIndex(rowID);
		
		JSONValue.splice(CurrentIndex, 1);		
		$("#" +rowID + ".ContentRow").remove();
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
		
	}
	
	function RowSlides(e) {
		var columns = $(e.currentTarget).find("td");
		var ranges = [], total = 0, i, s, w;
			for(i = 0; i<columns.length; i++){
				w = columns.eq(i).width()-10 - (i==0?1:0);
                ranges.push(w);
				total+=w;
                
			}		 
			for(i=0; i<columns.length; i++){
                ranges[i] = 12*ranges[i]/total;
				carriage = Math.round(ranges[i])-w
				s+= Math.round(ranges[i]) + ",";			
			}		
	}
	
	
	
	
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	
	
	function photoSelectValue(id) {
		var JSONValue = GetPageJSONValue();
		
		JSONValue[getParameterByName("RowIndex")].Columns[getParameterByName("ColumnIndex")].Content[getParameterByName("ContentIndex")]["Value"] = id;
		
		window.opener.$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		window.opener.$("#EventForm").trigger("submit");
		window.close();		
	}
	
	function AlbumIDSelected(id) {
		var JSONValue = GetPageJSONValue();
		
		JSONValue[getParameterByName("RowIndex")].Columns[getParameterByName("ColumnIndex")].Content[getParameterByName("ContentIndex")]["Value"] = id;
		
		window.opener.$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
		window.opener.$("#EventForm").trigger("submit");
		
		window.close();
	}
	
	
	function SelectPhotoAlbum(eventID, Element) {		
		var CurrentRowIndex = GetJSONRowIndex($(Element).parent().parent().parent().parent().parent().parent().attr("id"));
		var CurrentColumnIndex = GetJSONColumnIndex(CurrentRowIndex, $(Element).parent().parent().parent().attr("id"));
		var CurrentContentIndex = GetJSONContentIndex(CurrentRowIndex, CurrentColumnIndex, $(Element).parent().attr("id"));
		
		newwindow=window.open(Globals.WebsitePath() + 'events/selectalbum/' + eventID + '?RowIndex=' + CurrentRowIndex + '&ColumnIndex=' + CurrentColumnIndex + '&ContentIndex=' + CurrentContentIndex,'name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;	
		

	}


	function SelectPhoto(eventID, Element, ContentValue = null) {	
		var CurrentRowIndex = GetJSONRowIndex($(Element).parent().parent().parent().parent().parent().parent().attr("id"));
		var CurrentColumnIndex = GetJSONColumnIndex(CurrentRowIndex, $(Element).parent().parent().parent().attr("id"));
		var CurrentContentIndex = GetJSONContentIndex(CurrentRowIndex, CurrentColumnIndex, $(Element).parent().attr("id"));
		
		
		newwindow=window.open(Globals.WebsitePath() + 'events/selectphoto/' + eventID + '?RowIndex=' + CurrentRowIndex + '&ColumnIndex=' + CurrentColumnIndex + '&ContentIndex=' + CurrentContentIndex,'name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;	

	}
	
	
	function selectPhotoID(photoID, RowIndex, ColumnIndex, ContentIndex, ContentID) {
		var JSONValue = GetPageJSONValue();
		
		JSONValue[RowIndex].Columns[ColumnIndex].Content[ContentIndex]["Value"] = photoID;
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
		$("#" + ContentID + ".ContentSection span.photoSelectedText").html(" Change Photo")
		
		$("#" + ContentID + ".ContentSection a").attr("onclick", "EventsController.SelectPhoto(this, " + photoID +")")
		$(".photoAlbumContent").html("")
		
		$("#AlbumPhotoSelect").hide();
	}
	
	function selectAlbumID(albumID, RowIndex, ColumnIndex, ContentIndex, ContentID) {
		var JSONValue = GetPageJSONValue();
		
		JSONValue[RowIndex].Columns[ColumnIndex].Content[ContentIndex]["Value"] = albumID;
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
		$("#" + ContentID + ".ContentSection span.photoSelectedText").html(" Change Photo Album")
		
		$("#" + ContentID + ".ContentSection a").attr("onclick", "EventsController.SelectPhotoAlbum(this, " + albumID +")")
		$(".photoAlbumContent").html("")
		
		$("#AlbumPhotoSelect").hide();
	}
	
	
	function ClosePopup() {
		$(".photoAlbumContent").html("");
		$("#SuperEventPopup .FormContent").html("")
		
		$("#AlbumPhotoSelect, #SuperEventPopup").hide();
		$("#SubEvent").prop('checked', false);
		$(".HideForSubEvent").show();
	}
	

	
	function SelectPhotoFromAlbum(PATH, ParentDirectory, albumID, AlbumName) {
		$("#AlbumPhotoSelect .Popup .formHeader .text").html("Select Event Photo");
		$("#AlbumPhotoSelect").show();
		
		var Albums = [];
		$.getJSON( "../../admin/GetPhotosByAlbum/"  + albumID, function( data ) {
			$.each( data, function( key, val ) {
				var ClassName;
				ClassName = "AlbumSingle";
				
				
				Albums.push( "<div class='" + ClassName + "' onclick='EventsController.SelectEventPhoto(" + val.photoID +")'><div style='text-align:center; margin-bottom: 5px; padding-top:10px;'><img width='100%' src='" + PATH + "view/photos/events/" + ParentDirectory + "/" + AlbumName + "/" + val.photoName + "." + val.ext +"' /></div></div>" );
			});
			
			$(Albums.join("")).appendTo( ".photoAlbumContent" ); 
			$("<a href='" + PATH + "admin/photos/events/" + ParentDirectory + "/" + AlbumName + "'><div style='text-align:center; clear: both; border:3px dashed #3366cc; padding: 10px 0px; margin-right: 15px;'>Upload Photos</div></a>").appendTo( ".photoAlbumContent" );
			
		});
	}
	
	function SelectEventPhoto(PhotoID) {
		$("#EventPhoto").val(PhotoID);
		$("#SelectPhotoText").html("Change Photo");
		
		$(".photoAlbumContent").html("")
		$("#AlbumPhotoSelect").hide();
	}
	
	function removeLastComma(strng){        
	    var n=strng.lastIndexOf(",");
	    var a=strng.substring(0,n); 
	    return a;
	}
	
	function DroppableLines(rowID) {
		$("#row" + rowID +" .ColumnElement").droppable({
			accept: '.widgetMenu .item',
			drop: handleCardDrop
		});
	}
	
	function handleCardDrop(event, ui) {
		var JSONValue = GetPageJSONValue();
		var RowID = $(event.target).parent().parent().parent().attr("id");
		
		console.log(RowID);								
		var CurrentIndex = GetJSONRowIndex(RowID)
		var CurrentColumnIndex = GetJSONColumnIndex(CurrentIndex, event.target.id);
		
		var CurrentContentID;
		
		
		if(JSONKeyExists(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content'])) {
			CurrentContentID = parseInt(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content'].length) + 1;	
		} else {
			CurrentContentID = "1";
		}
		
													
										
		var NewEditableElement;
		switch(ui.helper.context.id) {
			case "Text":
				NewEditableElement = "Text<br /><textarea style='max-width:100%' onkeyup='EventsController.AddContentWidget(" + RowID + "," + parseInt(JSONValue[CurrentIndex].Columns[CurrentColumnIndex].columnID) + ", " + CurrentContentID + ", this)'></textarea>"
				break;
			case "Header1":
				NewEditableElement = "Header 1<br /><input type='text' onkeyup='EventsController.AddContentWidget(" + RowID + "," + parseInt(JSONValue[CurrentIndex].Columns[CurrentColumnIndex].columnID) + ", " + CurrentContentID + ", this)' />";
				break;
			case "Header2":
				NewEditableElement = "Header 2<br /><input type='text' onkeyup='EventsController.AddContentWidget(" + RowID + "," + parseInt(JSONValue[CurrentIndex].Columns[CurrentColumnIndex].columnID) + ", " + CurrentContentID + ", this)' />";
				break;
			case "Header3":
				NewEditableElement = "Header 3<br /><input type='text' onkeyup='EventsController.AddContentWidget(" + RowID + "," + parseInt(JSONValue[CurrentIndex].Columns[CurrentColumnIndex].columnID) + ", " + CurrentContentID + ", this)' />";
				break;
			case "Photo":
				NewEditableElement = "Photo<br /><a href='javascript:void(0);' onclick='EventsController.SelectPhoto(" + $("#eventID").val() + ", this)'><i class='fa fa-picture-o' aria-hidden='true'></i><span class='photoSelectedText'> Select Photo</span></a>";
				break;
			case "PhotoAlbum":
				NewEditableElement = "Photo Album<br /><a href='javascript:void(0);' onclick='EventsController.SelectPhotoAlbum(" + $("#eventID").val() + ", this)'><i class='fa fa-picture-o' aria-hidden='true'></i><span class='photoSelectedText'> Select Photo Album</span></a>";
				break;
		}
										
										
										
		if(JSONKeyExists(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content'])) {
			JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content'].push({"ContentID": parseInt(Object.keys(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content']).length) + 1, "ContentType": ui.helper.context.id, "Value": ""});	
		} else {
			$.extend( JSONValue[CurrentIndex].Columns[CurrentColumnIndex], {"Content" : [{"ContentID": 1, "ContentType": ui.helper.context.id, "Value":""}]});
		}
										
		$("#row" + RowID + " #"+ event.target.id +".ColumnElement .CurrentContent").append("<div class='ContentSection' id='" + parseInt(Object.keys(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content']).length) + "' style='margin-bottom:5px;'>" +NewEditableElement + "</div>");
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
		$.post(Globals.WebsitePath() +  "events/save/html/" + $("#eventID").val(), $( "#EventForm" ).serialize());
	}
	

	
	function SortableColumns(RowID) {
		$( "#row" +RowID).sortable({
			revert: true,
			stop: function( event, ui ) {							
				var JSONValue = GetPageJSONValue();
				var NewOrder = 1;							
				var NewOrderArray = []
																
				$("#row" + RowID + " .ColumnElement").each(function(index) {
					NewOrderArray.push({
						ColumnID : $(this).attr("id"),
						NewOrder : NewOrder
					});
												
					NewOrder++;							
				});
											
				var CurrentRowIndex = GetJSONRowIndex(RowID);
																																
				//loop through new order array 
				for(var i=0; i< NewOrderArray.length; i++) {
					//loop through current JSON Object
					for(var j=0; j< JSONValue[CurrentRowIndex]["Columns"].length; j++) {
						if(JSONValue[CurrentRowIndex]["Columns"][j].columnID == NewOrderArray[i].ColumnID) {
							JSONValue[CurrentRowIndex].Columns[j]['Order'] = NewOrderArray[i].NewOrder;
						}								
					}
				}
																					
				//alert(JSON.stringify(JSONValue));
				$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
				
				setTimeout(function(){
					ResetColumnResizable(RowID, $('#row' + RowID +' .ColumnElement').length);
					//EventsController.ColumnResizable(RowID);		
				}, 700);
				
			}
				
		});
	}
	
	var ResizeRowID
	function ColumnResizable(RowID) {
		$("#rangeRow" + RowID).colResizable({
			liveDrag:true, 
			draggingClass:"rangeDrag", 
			gripInnerHtml:"<div class='rangeGrip'></div>", 
			onDrag:RowSlides,
			onResize:ResizeRows,
			partialRefresh: true,
			minWidth:110
		});		
		
		ResizeRowID = RowID;
	}
	
	function ResetColumnResizable(RowID, columnCount, AddRemoveColumn) {
		
		
		var currentTDS = $("#rangeRow" + RowID + " td").length;
		
		if(columnCount > 0) {
			$("#rangeRow" + RowID).colResizable({disable: true});	
		}
		
		if(AddRemoveColumn) {
			if(AddRemoveColumn == "add") {
				if(columnCount > 0) {
					$("#rangeRow" + RowID +" tr").append("<td></td>");	
				}
			}
			
			if(AddRemoveColumn == "delete") {
				$("#rangeRow" + RowID +" tr td:first-child").remove();
			}			
		}

		
		var tableWith = $("#rangeRow" + RowID).width();
		
		$("#rangeRow" + RowID +" tr td").css("width", (tableWith / columnCount) + "px" );
		
		
		if(columnCount > 0) {
			console.log(columnCount);
			$("#rangeRow" + RowID).colResizable({
				liveDrag:true, 
				draggingClass:"rangeDrag", 
				gripInnerHtml:"<div class='rangeGrip'></div>", 
				onDrag:RowSlides,
				onResize:ResizeRows,
				minWidth:110
			});	
			
			ResizeRowID = RowID;
		}
		
		
	}
	
	
	var ColumnSizes;
									
	function RowSlides(e) {
		//console.log(e);
		var columns = $(e.currentTarget).find("td");
		var ranges = [], total = 0, i, s, w;
		for(i = 0; i<columns.length; i++){
			w = columns.eq(i).width()-10 - (i==0?1:0);
			ranges.push(w);
			total+=w;
								                
		}		 
		for(i=0; i<columns.length; i++){
			ranges[i] = 12*ranges[i]/total;
			carriage = Math.round(ranges[i])-w
			s+= Math.round(ranges[i]) + ",";			
		}		
											
		ColumnSizes = EventsController.removeLastComma(s.replace("undefined", "")).split(",")	
	}
									
	function ResizeRows(e) {
		var ColumnID = '1';
		var columnClassName;
		var ColumnsText;
		
		var RowID = $(e.currentTarget).parent().parent().parent().attr("id");
											
		var JSONValue = GetPageJSONValue();
										
		var CurrentIndex = GetJSONRowIndex(RowID);
		
											
		$("#row" + RowID + " .ColumnElement").each(function(index) {
			$(this).attr('class', 'ColumnElement col-md-' + ColumnSizes[index]);
			$(this).data('columnid', ColumnSizes[index]);
			
			for (var i=0; i < JSONValue[CurrentIndex]["Columns"].length; i++) {
				if(JSONValue[CurrentIndex].Columns[i].columnID == $(this).attr('id')) {
					JSONValue[CurrentIndex].Columns[i]['ClassID'] = ColumnSizes[index];
				}
			}
			
			
			//JSONValue[CurrentIndex].Columns[index]['ClassID'] = ColumnSizes[index];						
		});	
											
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
	}
	

	function SelectSuperEvent(ID, EventName) {
		$("#SuperEventID").val(ID);
		$("#SuperEventSelected").html("Super Event Selected: " + EventName);
		$("#SuperEventPopup").hide();
	}
	
	function setSuperEvent(element) {
		if(element.checked) {
			$(".HideForSuperEvent").hide();	
			$(".ShowForSuperEvent").show();	
		} else {
			$(".HideForSuperEvent").show();	
			$(".ShowForSuperEvent").hide();	
		}
		
	}
	
	function UploadMainPhoto(name) {
		newwindow=window.open(Globals.WebsitePath() + 'photos/uploadSingle/' + name + '?EventMainPhoto=true&singlePhoto=true','name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;
	}
	
	function UploadThumbnailPhoto(id) {
		newwindow=window.open(Globals.WebsitePath() + 'photos/uploadSingle/' + id + '?EventThumbNailPhoto=true&singlePhoto=true','name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;
	}

	function InitializeEventsList() {
		$("#UploadPDFForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, formData,
				function(responses) {
					if(responses.ValidationErrors) {
						var validationMessagesArray = responses.ValidationErrors;
						
						var ValidationErrors = responses.ValidationErrors.length;
						for (var i = 0; i < ValidationErrors; i++) {
						   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
						   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
						}
						alert("Please fix the errors on the screen");
					}
					
					if(responses.success) {
						window.location.reload();
					}
				}
			);		
		});
	}
	
	function getEditHistory(eventID) {
		$("#EventEditHistory").show();
		var editHistory = [];
		$.getJSON(Globals.WebsitePath() +  "events/GetEditHistory/" + eventID, function( data ) {
			$.each( data, function( key, val ) {
				editHistory.push("<div>" + val.value +"</div>" );
			});
			
			$(editHistory.join("")).appendTo("#EventEditHistory .FormContent"); 
		});
	}
	
	function closeEditHistory() {
		$("#EventEditHistory").hide();
		$("#EventEditHistory .FormContent").html('');
	}
	
	return {
		AddLayoutRow: AddLayoutRow,
		InitializeSingleEvent: InitializeSingleEvent,
		InitializeSuperEventSingle: InitializeSuperEventSingle,
		AddColumnInRow: AddColumnInRow,
		DeleteColumn: DeleteColumn,
		DeleteRow: DeleteRow,
		RowSlides: RowSlides,
		removeLastComma: removeLastComma,
		ResizeRows: ResizeRows,
		UpdateRowOrder: UpdateRowOrder,
		AddContentWidget: AddContentWidget,
		SelectPhoto: SelectPhoto,
		selectPhotoID: selectPhotoID,
		ClosePopup: ClosePopup,
		SelectPhotoAlbum: SelectPhotoAlbum,
		selectAlbumID: selectAlbumID,
		SelectPhotoFromAlbum: SelectPhotoFromAlbum,
		SelectEventPhoto: SelectEventPhoto,
		DroppableLines: DroppableLines,
		SortableColumns: SortableColumns,
		ColumnResizable: ColumnResizable,
		SelectSuperEvent: SelectSuperEvent,
		setSuperEvent: setSuperEvent,
		UploadMainPhoto: UploadMainPhoto,
		InitializeEventsList: InitializeEventsList,
		getEditHistory: getEditHistory,
		closeEditHistory: closeEditHistory,
		photoSelectValue: photoSelectValue,
		AlbumIDSelected: AlbumIDSelected,
		UploadThumbnailPhoto: UploadThumbnailPhoto
	}
}();
