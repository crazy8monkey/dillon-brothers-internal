var ShopController = function() {
	
	function CategorySingle() {
		$("#categoryForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});		
	}
	
	function ColorSingle() {
		$("#ColorForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});		
	}
	
	function SizeSingle() {
		$("#SizeForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function ProductSingle() {
		$("#ProductForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});		
	}
	
	function AddPhotosToInventory(id) {
		newwindow=window.open(Globals.WebsitePath() + 'shop/addphotos/' + id,'name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;
	}
	
	function ViewPhoto(id) {
		newwindow=window.open(Globals.WebsitePath() + 'shop/editphoto/' + id,'name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;
	}
	
	function getEditHistory(id) {
		$("#ProductHistoryPopup").show();
		var HistoryLog = [];
		$.getJSON(Globals.WebsitePath() +  "shop/GetProductHistory/" + id, function( data ) {
			$.each( data, function( key, val ) {
				HistoryLog.push("<div>" + val.value +"</div>" );
			});
			
			$(HistoryLog.join("")).appendTo("#ProductHistoryPopup .FormContent"); 
		});
	}
	
	function closeInventoryHistory() {
		$("#ProductHistoryPopup").hide();
		$("#ProductHistoryPopup .FormContent").html('');
	}
	
	function EcommerceSettingsPage() {
		$("#MarkAsShippedForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function OrderSinglePage() {
		$("#MarkAsShippedForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function ProductsList() {
		$("#FilterProductList").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});		
	}
	
	function ToggleReviewList(element) {
		$(element).find(".productItemExpand").toggleText('-', '+');	
		$(element).parent().next().toggle();	
		
		$(".productItemExpand").not($(element).find(".productItemExpand")).html('+');
		$(".productReviewList").not($(element).parent().next()).hide();
	}
	
	return {
		CategorySingle: CategorySingle,
		ColorSingle: ColorSingle,
		SizeSingle: SizeSingle,
		ProductSingle: ProductSingle,
		AddPhotosToInventory: AddPhotosToInventory,
		ViewPhoto: ViewPhoto,
		getEditHistory: getEditHistory,
		closeInventoryHistory: closeInventoryHistory,
		EcommerceSettingsPage: EcommerceSettingsPage,
		OrderSinglePage: OrderSinglePage,
		ToggleReviewList: ToggleReviewList,
		ProductsList: ProductsList
	}
}();
