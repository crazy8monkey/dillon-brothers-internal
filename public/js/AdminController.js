var AdminController = function() {

	function NewFolderResponses(responses) {
		if(responses.errorMessage) {
			$("#ErrorResponses").html(responses.errorMessage);
		}
		
		if(responses.success) {
			location.reload();
		}
	}
	
	
	function MultipleErrorResponses(responses) {
		if(responses.ValidationErrors) {
			var validationMessagesArray = responses.ValidationErrors;
			
			var ValidationErrors = responses.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
			   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
			}
		}
		if(responses.redirect) {
 			Globals.redirect(responses.redirect); 
 		}
	}
	
	function NewsArchiveResponse(responses) {
		if(responses.ValidationErrors) {
			var validationMessagesArray = responses.ValidationErrors;
			
			var ValidationErrors = responses.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
			   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
			}
		}
		if(responses.redirect) {
 			Globals.redirect(responses.redirect); 
 		}
	}
	
	
	function EditPhotoResponses(responses) {
		if(responses.ValidationErrors) {
			var validationMessagesArray = responses.ValidationErrors;
			
			var ValidationErrors = responses.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
			   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
			}
		}
		
		if(responses.success) {
			$("input#photoName").val(responses.success.NewName);
			$("span#photoSingleName").html(responses.success.NewName + '.' + responses.success.fileExt);
			
			Globals.ShowErrorMessage("#PhotoSingleMessage");
			Globals.LoadingIndicator("#PhotoSingleLoading");
			Globals.ShowResponseMessage("#PhotoSingleText", "success", responses.success.msg)
		}
	}
	
	function AddPhotoResponses(responses) {
		//$("#uploadingPhotosText").show();
		if(responses.errorMessage) {
			Globals.ShowErrorMessage("#AddPhotosMessage");
			Globals.LoadingIndicator("#AddPhotosLoading");
			Globals.ShowResponseMessage("#AddPhotosText", "error", responses.errorMessage);
			$("#uploadingPhotosText").hide();
			Globals.ScrollToTop("#AddPhotosMessage");
		}
		
		if(responses.redirect) {
 			$("#uploadingPhotosText").hide();
 			Globals.redirect(responses.redirect); 
 		}
	}
	
	function SingleEventResponses(responses) {
		if(responses.ValidationErrors) {
			var validationMessagesArray = responses.ValidationErrors;
			
			var ValidationErrors = responses.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
			   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
			}
		}
		
		if(responses.redirect) {
			Globals.redirect(responses.redirect); 
		}
	}
	
	
	function InitializePhotoEvents() {
		$("#addYear").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			

			Globals.AjaxPost(formURL, postData, ".loadingIndicator", NewFolderResponses)		
		});
	}
	
	function InitializePhotoAlbum() {
		$("#addAlbum").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			
			Globals.AjaxPost(formURL, postData, ".loadingIndicator", NewFolderResponses)		
		});
	} 
	
	function InitializeEmployeeForm() {
		$("#EmployeeAddForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, formData, "#uploadingPhotosText", MultipleErrorResponses)		
		});
		
		
		//http://digitalbush.com/projects/masked-input-plugin/#demo
		$("#employeePhone").mask("(999) 999-9999",{placeholder:" "});
	}

	
	function InitializeAddPhotos() {
		$("#AddPhotos").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, formData, "#uploadingPhotosText", AddPhotoResponses)		
		});
	}
	
	
	function InitializeAddEventPage() {
		$("#CreateEventForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, ".loadingIndicator", SingleEventResponses)		
		});
	}
	
	function InitializeEditEventPage() {
		$("#EditEventForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, ".loadingIndicator", SingleEventResponses)		
		});
	}
	
	function InitializeNewsLetterForm() {
		$("#NewsArchiveForm").submit(function(e){			
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, formData, "#uploadingPhotosText", NewsArchiveResponse)
		});
	}
	
	function MakeFileList(albumName, UploadCount) {
		var input = document.getElementById('filesToUpload');
		var list = $('#fileList');
		
		var TotalFileSize = 0;
		//empty list for now...
		//while (list.hasChildNodes()) {
		//	list.removeChild(list.firstChild);
		//}
		
		
		var LatestUploadCount = UploadCount;
		
		
		
		//for every file...
		for (var x = 0; x < input.files.length; x++) {
			
			console.log(input.files[x].size)
			TotalFileSize += parseInt(input.files[x].size);
			$("#TotalEntered").show();
			
			var reader = new FileReader();
			
			reader.readAsDataURL(input.files[x]);
			
			reader.onload = function () {
            
            
				$("#fileList").append("<div class='row' style='margin-bottom:10px; border-bottom: 1px solid #dcdcdc; padding-bottom:10px;'>" + 
									"<div class='col-md-4'><img src='" +  this.result + "' style='width:100%' /></div><div class='col-md-8'>" + 
										"<div class='input'><div class='inputLabel'>Image name</div><input type='text' name='photoName[]' value='" + albumName + "-" + LatestUploadCount + "' /></div>" + 
										"<div class='input'><div class='inputLabel'>Alt</div><input type='text' name='photoAlt[]' placeholder='alt' value='" + albumName + "-" + LatestUploadCount + "' /></div>" + 
										"<div class='input' style='margin-bottom:0px;'><div class='inputLabel'>Title</div><input type='text' name='photoTitle[]' value='" + albumName + "-" + LatestUploadCount + "' placeholder='title' /></div></div>");
			
				LatestUploadCount++;
			}
			
		}
		
		//adaption of this
		//https://www.nczonline.net/blog/2012/05/08/working-with-files-in-javascript-part-1/
		$("#UploadSizeTotal").html(formatBytes(TotalFileSize));
		$("span#FileUploadTotal").html(input.files.length);
		$("input#PhotoCount").val(input.files.length);
		$(".UploadPhotosSubmit").show();
		
	}
	
	function formatBytes(bytes,decimals) {
	   if(bytes == 0) return '0 Byte';
	   var k = 1000;
	   var dm = decimals + 1 || 3;
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
	   var i = Math.floor(Math.log(bytes) / Math.log(k));
	   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
	}
	
	function ToggleVisibleImage(id, checkbox) {
		var isChecked = $(checkbox).is(':checked');
		var $switchLabel = $('.slider#' + id);
		var TooltipText;
		
		if(isChecked) {
	        selectedData = $switchLabel.attr('data-off');
	        TooltipText = "Photo not visible";
	    } else {
	    	selectedData = $switchLabel.attr('data-on');
	    	TooltipText = "Photo visible";
	    }
	    
		$.post("http://localhost/DillonBrothers/admin/photovisible/" + id, {visible: selectedData}, function(result){
	    	$switchLabel.attr("data-original-title", TooltipText);
	    });
	}
	
	function InitializePhotoSingle() {
		$("#editPhoto").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, ".loadingIndicator", EditPhotoResponses)		
		});
	} 
	
	function removeValidationMessage(ID) {
		$("#inputID" + ID + " .errorMessage").html("");
		$("#inputID" + ID + " input").removeAttr("style")
		$("#inputID" + ID + " select").removeAttr("style")
		$("#inputID" + ID + " textarea").removeAttr("style")
	}
	
	
	function AddLayoutRow() {
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		
		var lastInsertedRow;
		var Order;
		
		//alert(lastInsertedRow)
		
		if(Object.keys(JSONValue).length) {
			lastInsertedRow = JSONValue[Object.keys(JSONValue).length-1].rowID;
			Order = Object.keys(JSONValue).length + 1;
		} else {
			lastInsertedRow = 0;
			Order = 1;
		}
		
		JSONValue.push({"rowID" : parseInt(lastInsertedRow + 1), "Order": Order});
		
		var NewRowID = parseInt(lastInsertedRow + 1);
		
		
		
		var AdColumnClickEvent = 'AdminController.AddColumnInRow("row' + NewRowID + '", "' + NewRowID + '")';
		var DeleteRowClickEvent = 'AdminController.DeleteRow("' + NewRowID + '")'
		
		
		var NewRow = "<div class='ContentRow ui-sortable-handle' id='" + NewRowID + "'>" +
				 		"<div class='DragAdd'>" +
				 			"<div style='text-align:center'>" +
				 				"<i class='fa fa-arrows-v' aria-hidden='true'></i>" +	
				 			"</div>" +
				 			"<a href='javascript:void(0)' onclick='" + AdColumnClickEvent + "'>" +
					 			"<div style='text-align:center' id='" + NewRowID + "' data-toggle='tooltip' data-placement='bottom' title='Add Column' class='AddColumn'>" +
					 				"<i class='fa fa-plus-circle' aria-hidden='true'></i>" +
					 			"</div>" +
				 			"</a>" +
				 			"<div style='text-align:center'>" +
				 				"<a href='javascript:void(0);' onclick='" + DeleteRowClickEvent + "'>" +
				 					"<i class='fa fa-trash-o' aria-hidden='true'></i>" +	
				 				"</a>" +
				 			"</div>" +
				 		"</div>" +
				 		"<div id='row" + NewRowID + "' style='margin-bottom:1px; clear:both' class='row ColumnRowContent ui-sortable'></div>" +
				 	"</div>";
		
		
		
		$("#LayoutContent").append(NewRow);
		
		
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));		
	}
	
	function AddColumnInRow(Row, ID) {
		var element = document.getElementById(Row);
		var numberOfChildren = element.getElementsByClassName('ColumnElement').length;
		
		var className;
		var dataColumnID;
		
		
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		
		var CurrentIndex;
		var LatestColumnIndex;
		var LatestOrder;
		var LatestColumnID;
		
		for (var i=0; i<JSONValue.length; i++) {
			
			if(JSONValue[i].rowID == ID) {
				CurrentIndex = i;
				if(JSONValue[i]["Columns"] != null) {
					for (var c=0; c<JSONValue[i]["Columns"].length; c++) {
						LatestColumnIndex = c;
						LatestOrder = JSONValue[i].Columns[c].Order;
						LatestColumnID = JSONValue[i].Columns[c].columnID;
					}	
				} else {
					LatestColumnIndex = 0;
					LatestOrder = 0;
					LatestColumnID = 0;
				}
				
				
			}
			
			if(JSONValue[i].rowID === ID) {
				CurrentIndex = i;
				if(JSONValue[i]["Columns"] != null) {
					for (var c=0; c<JSONValue[i]["Columns"].length; c++) {
						LatestColumnIndex = c;
						LatestOrder = JSONValue[i].Columns[c].Order;
						LatestColumnID = JSONValue[i].Columns[c].columnID;
					}	
				} else {
					LatestColumnIndex = 0;
					LatestOrder = 1;
					LatestColumnID = 1;
				}
			}
		}
		
		if(JSONValue[CurrentIndex]['Columns'] != null) {
			JSONValue[CurrentIndex]['Columns'].push({"columnID":parseInt(LatestColumnID + 1),"ClassID": "", "Order": parseInt(LatestOrder + 1)});	
		} else {
			$.extend( JSONValue[CurrentIndex], {"Columns" : [{"columnID":parseInt(LatestColumnID + 1),"ClassID": 12, "Order": parseInt(LatestOrder + 1)}]});
		}
		
			
		
		switch(numberOfChildren) {
			case 0:
				className = 'col-md-12';
				dataColumnID = '12';
				$("#" +Row + " .ColumnElement").attr('data-columnid', '6');
				//JSONValue[CurrentIndex].Columns[0]['ClassID'] = 12;
				break;
			case 1:
				className = 'col-md-6';
				dataColumnID = '6';
				$("#" +Row + " .ColumnElement").attr('class', 'ColumnElement col-md-6');
				$("#" +Row + " .ColumnElement").attr('data-columnid', '6');
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 6;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 6;
				break;
			case 2:
				className = 'col-md-4';
				dataColumnID = '4';
				$("#" +Row + " .ColumnElement").attr('class', 'ColumnElement col-md-4');
				$("#" +Row + " .ColumnElement").attr('data-columnid', '4');
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 4;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 4;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 4;
				break;
			case 3:
				className = 'col-md-3';
				dataColumnID = '3';
				$("#" +Row + " .ColumnElement").attr('class', 'ColumnElement col-md-3');
				$("#" +Row + " .ColumnElement").attr('data-columnid', '3');
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 3;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 3;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 3;
				JSONValue[CurrentIndex].Columns[3]['ClassID'] = 3;	
				break;
			case 4:
				className = 'col-md-2';
				dataColumnID = '2';
				$("#" +Row + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-3');
						$(this).attr('data-columnid', '3');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-3');
						$(this).attr('data-columnid', '3');
					} else {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					}
				});
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 3;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 3;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[3]['ClassID'] = 2;	
				JSONValue[CurrentIndex].Columns[4]['ClassID'] = 2;
				
				break;
			case 5:
				className = 'col-md-2';
				dataColumnID = '2';
				$("#" +Row + " .ColumnElement").attr('class', 'ColumnElement col-md-2');
				$("#" +Row + " .ColumnElement").attr('data-columnid', '2');
				
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[3]['ClassID'] = 2;	
				JSONValue[CurrentIndex].Columns[4]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[5]['ClassID'] = 2;
				
				break;
			case 6:
				className = 'col-md-1';
				dataColumnID = '1';
				$("#" +Row + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 2) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 3) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 4) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).attr('data-columnid', '1');
					}
				});
				
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[3]['ClassID'] = 2;	
				JSONValue[CurrentIndex].Columns[4]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[5]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[6]['ClassID'] = 1;
				break;
			case 7:
				className = 'col-md-1';
				dataColumnID = '1';
				$("#" +Row + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 2) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 3) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).attr('data-columnid', '1');
					}
				});
				
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[3]['ClassID'] = 2;	
				JSONValue[CurrentIndex].Columns[4]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[5]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[6]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[7]['ClassID'] = 1;
				break;
				
			case 8:
				className = 'col-md-1';
				dataColumnID = '1';
				$("#" +Row + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 2) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).attr('data-columnid', '1');
					}
				});		
				
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[3]['ClassID'] = 1;	
				JSONValue[CurrentIndex].Columns[4]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[5]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[6]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[7]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[8]['ClassID'] = 1;		
				break;
			
			case 9:
				className = 'col-md-1';
				dataColumnID = '1';
				$("#" +Row + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).attr('data-columnid', '1');
					}
				});		
				
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[3]['ClassID'] = 1;	
				JSONValue[CurrentIndex].Columns[4]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[5]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[6]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[7]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[8]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[9]['ClassID'] = 1;
						
				break;
			case 10:
				className = 'col-md-1';
				dataColumnID = '1';
				$("#" +Row + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).attr('data-columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).attr('data-columnid', '1');
					}
				});		
				
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 2;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[3]['ClassID'] = 1;	
				JSONValue[CurrentIndex].Columns[4]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[5]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[6]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[7]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[8]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[9]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[10]['ClassID'] = 1;
						
				break;
			case 11:
				className = 'col-md-1';
				dataColumnID = '1';
				$("#" +Row + " .ColumnElement").attr('class', 'ColumnElement col-md-1');
				$("#" +Row + " .ColumnElement").attr('data-columnid', '1');
				$("#" +ID + ".AddColumn").hide()
				
				JSONValue[CurrentIndex].Columns[0]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[1]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[2]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[3]['ClassID'] = 1;	
				JSONValue[CurrentIndex].Columns[4]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[5]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[6]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[7]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[8]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[9]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[10]['ClassID'] = 1;
				JSONValue[CurrentIndex].Columns[11]['ClassID'] = 1;
						
				break;
				
		}
		
	
		var OnClickEvent = 'AdminController.DeleteColumn("' + Row + '", '+ (parseInt(numberOfChildren) + 1) + ', "' + parseInt(JSONValue.length) - 1  + '")';
		
		//alert(numberOfChildren);
		var NewColumnElement = "<div class='ColumnElement " + className + "' id='" + (parseInt(numberOfChildren) + 1) + "' data-ColumnID='" + dataColumnID + "'>" + 
									"<div class='delete'>" +
										"<a href='javascript:void(0);' onclick='" + OnClickEvent + "'>" +
											"<i class='fa fa-trash' aria-hidden='true'></i>" +
										"</a>" +
									"</div>" +
									"<div class='Empty'><div class='AddWidget'></div></div></div>";
		
		$("#" +Row).append(NewColumnElement);
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));	
	}
	
	
	function AddContentWidget(rowID, ColumnID, ContentID, Element) {
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		
		var CurrentIndex;
		var CurrentColumnIndex;
		var CurrentContentIndex;
		
		for (var i=0; i<JSONValue.length; i++) {
			if(JSONValue[i].rowID == rowID) {
				CurrentIndex = i;	
				for (var c=0; c < JSONValue[i]["Columns"].length; c++) {
					if(JSONValue[i].Columns[c].columnID == ColumnID) {
						CurrentColumnIndex = c;
						for (var d=0; d < JSONValue[i]["Columns"][c]["Content"].length; d++) {
							if(JSONValue[i].Columns[c].Content[d].ContentID == ContentID) {
								CurrentContentIndex = d;		
							}
						}
					}
				}
			}
		}
		
		JSONValue[CurrentIndex].Columns[CurrentColumnIndex].Content[CurrentContentIndex]['Value'] = $(Element).val();
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
	}
	
	function EditJsonString() {
		
	}
	
	function DeleteColumn(rowID, ColumnID, rowIDNumber) {
		var numberOfChildren = $('#' + rowID + ' .ColumnElement').length;
		
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		
		switch(numberOfChildren) {
			case 2:	
				$("#" +rowID + " .ColumnElement").attr('class', 'ColumnElement col-md-12');
				$("#" +rowID + " .ColumnElement").data('columnid', '12');
				break;
			case 3:
				$("#" +rowID + " .ColumnElement").attr('class', 'ColumnElement col-md-6');
				$("#" +rowID + " .ColumnElement").data('columnid', '6');
				break;
			case 4:
				$("#" +rowID + " .ColumnElement").attr('class', 'ColumnElement col-md-4');
				$("#" +rowID + " .ColumnElement").data('columnid', '4');
				break;
			case 5:
				$("#" +rowID + " .ColumnElement").attr('class', 'ColumnElement col-md-3');
				$("#" +rowID + " .ColumnElement").data('columnid', '3');
			
				
				break;
			case 6:
				$("#" +rowID + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-3');
						$(this).data('columnid', '3');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-3');
						$(this).data('columnid', '3');
					} else {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					}
				});
			
			
				
				break;
			case 7:
				$("#" +rowID + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 2) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 3) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 4) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 5) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 6) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).data('columnid', '1');
					}
				});	
			
			
				
				break;	
			case 8:
				$("#" +rowID + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 2) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 3) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 4) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).data('columnid', '1');
					}
				});	
			
			
				
				break;				
			case 9:
				$("#" +rowID + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 2) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 3) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).data('columnid', '1');
					}
				});	
			
			
				
				break;			
			case 10:
				$("#" +rowID + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 2) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).data('columnid', '1');
					}
				});	
			
			
				
				break;
			case 11:
				$("#" +rowID + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else if(index == 1) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).data('columnid', '1');
					}
				});	
			
			
				
				break;	
			case 12:
				$("#" +rowID + " .ColumnElement").each(function(index) {
					if(index == 0) {
						$(this).attr('class', 'ColumnElement col-md-2');
						$(this).data('columnid', '2');
					} else {
						$(this).attr('class', 'ColumnElement col-md-1');
						$(this).data('columnid', '1');
					}
				});	
			
			
				
				break;				
		}
		
		$("#" + rowID + " .ColumnElement#" + ColumnID).remove();
		//alert(numberOfChildren);
		
		

		
		
		for(var i=0; i< JSONValue.length; i++) {
			if(JSONValue[i].rowID == rowIDNumber) {
				for(var c=0; c< JSONValue[i]["Columns"].length; c++) {
					if(JSONValue[i].Columns[c].columnID == ColumnID) {
						JSONValue[i].Columns.splice(c,1);	
					}
				}
			}
		}
		
		var NewOrder = 1;
		var NewOrderArray = [];
		
		$("#" +rowID + " .ColumnElement").each(function(index) {
			NewOrderArray.push({
				ColumnID : $(this).attr("id"),
				ClassID : $(this).data("columnid"),
				NewOrder : NewOrder
			});									
			NewOrder++;
		});		
		
		
		var CurrentRowIndex;
											
		for(var j=0; j< JSONValue.length; j++) {
			if(JSONValue[j].rowID == rowIDNumber) {
				CurrentRowIndex = j;	
			}	
		}
											
		//loop through new order array 
		for(var i=0; i< NewOrderArray.length; i++) {
			//loop through current JSON Object
			for(var j=0; j< JSONValue[CurrentRowIndex]["Columns"].length; j++) {
				if(JSONValue[CurrentRowIndex]["Columns"][j].columnID == NewOrderArray[i].ColumnID) {
					JSONValue[CurrentRowIndex].Columns[j]['ClassID'] = NewOrderArray[i].ClassID;
					JSONValue[CurrentRowIndex].Columns[j]['Order'] = NewOrderArray[i].NewOrder;
				}
			
			}
		}
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
	}
	
	function UpdateRowOrder() {
		
		var JSONValue = GetPageJSONValue();
		
		var NewOrder = 1;
		
		var NewOrderArray = []

		$("#LayoutContent .ContentRow").each(function(index) {
			NewOrderArray.push({
	            RowID : $(this).attr("id"),
	            NewOrder : NewOrder
	        });
			NewOrder++;
		});
		
		//loop through new order array 
		for(var i=0; i< NewOrderArray.length; i++) {
			//loop through current JSON Object
			for(var j=0; j< JSONValue.length; j++) {
				if(JSONValue[j].rowID == NewOrderArray[i].RowID) {
					JSONValue[j]['Order'] = NewOrderArray[i].NewOrder;
				}
				
			}
		}
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
	}
	
	function GetPageJSONValue() {
		return JSON.parse($("#HtmlEventJSON").val());
	}
	
	function ResizeRows(RowID, ColumnSizes) {
		console.log(ColumnSizes);
	}
	
	function DeleteRow(rowID) {
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		var CurrentIndex = GetJSONRowIndex(rowID);
		
		JSONValue.splice(CurrentIndex, 1);		
		$("#" +rowID + ".ContentRow").remove();
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
		
	}
	
	function RowSlides(e) {
		var columns = $(e.currentTarget).find("td");
		var ranges = [], total = 0, i, s, w;
			for(i = 0; i<columns.length; i++){
				w = columns.eq(i).width()-10 - (i==0?1:0);
                ranges.push(w);
				total+=w;
                
			}		 
			for(i=0; i<columns.length; i++){
                ranges[i] = 12*ranges[i]/total;
				carriage = Math.round(ranges[i])-w
				s+= Math.round(ranges[i]) + ",";			
			}		
	}
	
	function SelectPhoto(Element, ContentValue = null) {		
		var CurrentRowIndex = GetJSONRowIndex($(Element).parent().parent().parent().parent().parent().parent().attr("id"));
		var CurrentColumnIndex = GetJSONColumnIndex(CurrentRowIndex, $(Element).parent().parent().parent().attr("id"));
		var CurrentContentIndex = GetJSONContentIndex(CurrentRowIndex, CurrentColumnIndex, $(Element).parent().attr("id"));
		
		
		$("#AlbumPhotoSelect .Popup .formHeader .text").html("Select Photo");
		$("#AlbumPhotoSelect").show();
		
		var photos = [];
		$.getJSON( "../../admin/GetPhotos", function( data ) {
			$.each( data, function( key, val ) {
				var ClassName;
				if(ContentValue) {
					if(val.photoID == ContentValue) {
						ClassName = "photoSingleSelect Selected";
					} else {
						ClassName = "photoSingleSelect";
					}
						
				}
				
				
				photos.push( "<div class='" + ClassName + "' onclick='AdminController.selectPhotoID("+ val.photoID +", "+ CurrentRowIndex +", "+ CurrentColumnIndex +", "+ CurrentContentIndex +", " + $(Element).parent().attr("id") +")' ><img src='http://localhost/DillonBrothers/view/photos/events/" + val.ParentDirectory + "/" + val.albumName +"/" + val.photoName + "-s." + val.ext +"' /></div>" );
			});
			
			$(photos.join("")).appendTo( ".photoAlbumContent" ); 
		});

	}
	
	function SelectPhotoAlbum(Element, ContentValue = null) {		
		var CurrentRowIndex = GetJSONRowIndex($(Element).parent().parent().parent().parent().parent().parent().attr("id"));
		var CurrentColumnIndex = GetJSONColumnIndex(CurrentRowIndex, $(Element).parent().parent().parent().attr("id"));
		var CurrentContentIndex = GetJSONContentIndex(CurrentRowIndex, CurrentColumnIndex, $(Element).parent().attr("id"));
		
		
		$("#AlbumPhotoSelect .Popup .formHeader .text").html("Select Photo Album");
		$("#AlbumPhotoSelect").show();
		
		var Albums = [];
		$.getJSON( "../../admin/GetPhotoAlbum", function( data ) {
			$.each( data, function( key, val ) {
				var ClassName;
				if(ContentValue) {
					if(val.albumID == ContentValue) {
						ClassName = "AlbumSingle Selected";
					} else {
						ClassName = "AlbumSingle";
					}
				} else {
					ClassName = "AlbumSingle";
				}
				
				Albums.push( "<div class='" + ClassName + "' onclick='AdminController.selectAlbumID("+ val.albumID +", "+ CurrentRowIndex +", "+ CurrentColumnIndex +", "+ CurrentContentIndex +", " + $(Element).parent().attr("id") +")'><div style='text-align:center; margin-bottom: 5px; padding-top:10px;'><img src='http://localhost/DillonBrothers/public/images/FolderIcon.png' /></div><div style='text-align:center'>" + val.albumName + " (" + val.AlbumCount+")</div></div>" );
			});
			
			$(Albums.join("")).appendTo( ".photoAlbumContent" ); 
		});

	}

	
	
	function selectPhotoID(photoID, RowIndex, ColumnIndex, ContentIndex, ContentID) {
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		
		JSONValue[RowIndex].Columns[ColumnIndex].Content[ContentIndex]["Value"] = photoID;
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
		$("#" + ContentID + ".ContentSection span.photoSelectedText").html(" Change Photo")
		
		$("#" + ContentID + ".ContentSection a").attr("onclick", "AdminController.SelectPhoto(this, " + photoID +")")
		$(".photoAlbumContent").html("")
		
		$("#AlbumPhotoSelect").hide();
	}
	
	function selectAlbumID(albumID, RowIndex, ColumnIndex, ContentIndex, ContentID) {
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		
		JSONValue[RowIndex].Columns[ColumnIndex].Content[ContentIndex]["Value"] = albumID;
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
		
		$("#" + ContentID + ".ContentSection span.photoSelectedText").html(" Change Photo Album")
		
		$("#" + ContentID + ".ContentSection a").attr("onclick", "AdminController.SelectPhotoAlbum(this, " + albumID +")")
		$(".photoAlbumContent").html("")
		
		$("#AlbumPhotoSelect").hide();
	}
	
	
	function ClosePopup() {
		$(".photoAlbumContent").html("")
		
		$("#AlbumPhotoSelect, #SuperEventPopup").hide();
	}
	
	function GetJSONRowIndex(rowID) {
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		var CurrentIndex
		
		for (var i=0; i<JSONValue.length; i++) {
			if(JSONValue[i].rowID == rowID) {
				CurrentIndex = i;	
			}
		}
		
		return CurrentIndex;
	}
	
	function GetJSONColumnIndex(RowIndex, ColumnID) {
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		var CurrentColumnIndex;
		
		for (var i=0; i < JSONValue[RowIndex]["Columns"].length; i++) {
			if(JSONValue[RowIndex].Columns[i].columnID == ColumnID) {
				CurrentColumnIndex = i;
			}
		}
		return CurrentColumnIndex;
	}
	
	function GetJSONContentIndex(RowIndex, ColumnIndex, ContentID) {
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
		var CurrentColumnIndex;
		
		
		
		
		if(JSONValue[RowIndex].Columns[ColumnIndex]['Content'] != null) {
			for (var i=0; i < JSONValue[RowIndex]["Columns"][ColumnIndex]["Content"].length; i++) {
				if(JSONValue[RowIndex].Columns[ColumnIndex].Content[i].ContentID == ContentID) {
					CurrentContentIndex = i;		
				}
			}
			
		} else {
			CurrentContentIndex = 0;	
		}
		
			
		return CurrentContentIndex;
	}
	
	function SelectPhotoFromAlbum(PATH, ParentDirectory, albumID, AlbumName) {
		$("#AlbumPhotoSelect .Popup .formHeader .text").html("Select Event Photo");
		$("#AlbumPhotoSelect").show();
		
		var Albums = [];
		$.getJSON( "../../admin/GetPhotosByAlbum/"  + albumID, function( data ) {
			$.each( data, function( key, val ) {
				var ClassName;
				ClassName = "AlbumSingle";
				
				
				Albums.push( "<div class='" + ClassName + "' onclick='AdminController.SelectEventPhoto(" + val.photoID +")'><div style='text-align:center; margin-bottom: 5px; padding-top:10px;'><img width='100%' src='" + PATH + "view/photos/events/" + ParentDirectory + "/" + AlbumName + "/" + val.photoName + "." + val.ext +"' /></div></div>" );
			});
			
			$(Albums.join("")).appendTo( ".photoAlbumContent" ); 
			$("<a href='" + PATH + "admin/photos/events/" + ParentDirectory + "/" + AlbumName + "'><div style='text-align:center; clear: both; border:3px dashed #3366cc; padding: 10px 0px; margin-right: 15px;'>Upload Photos</div></a>").appendTo( ".photoAlbumContent" );
			
		});
	}
	
	function SelectEventPhoto(PhotoID) {
		$("#EventPhoto").val(PhotoID);
		$("#SelectPhotoText").html("Change Photo");
		
		$(".photoAlbumContent").html("")
		$("#AlbumPhotoSelect").hide();
	}
	
	function removeLastComma(strng){        
	    var n=strng.lastIndexOf(",");
	    var a=strng.substring(0,n); 
	    return a;
	}
	
	function DroppableLines(rowID) {
		$("#row" + rowID +" .ColumnElement").droppable({
			accept: '.widgetMenu .item',
			drop: handleCardDrop
		});
	}
	
	function handleCardDrop(event, ui) {
		var JSONValue = GetPageJSONValue();
		var RowID = $("#" + event.target.id + ".ColumnElement").parent().parent().parent().attr("id");
		
										
		var CurrentIndex = GetJSONRowIndex(RowID)
		var CurrentColumnIndex = GetJSONColumnIndex(CurrentIndex, event.target.id);
		
		var CurrentContentID;
		
		
		if(JSONKeyExists(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content'])) {
			CurrentContentID = parseInt(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content'].length) + 1;	
		} else {
			CurrentContentID = "1";
		}
		
										
																			
										
		var NewEditableElement;
		switch(ui.helper.context.id) {
			case "Text":
				NewEditableElement = "Text<br /><textarea style='max-width:100%'></textarea>"
				break;
			case "Header1":
				NewEditableElement = "Header 1<br /><input type='text' />";
				break;
			case "Header2":
				NewEditableElement = "Header 2<br /><input type='text' />";
				break;
			case "Header3":
				NewEditableElement = "Header 3<br /><input type='text' />";
				break;
			case "Photo":
				NewEditableElement = "Photo<br /><a href='javascript:void(0);' onclick='AdminController.SelectPhoto(this)'><i class='fa fa-picture-o' aria-hidden='true'></i><span class='photoSelectedText'> Select Photo</span></a>";
				break;
			case "PhotoAlbum":
				NewEditableElement = "Photo Album<br /><a href='javascript:void(0);' onclick='AdminController.SelectPhotoAlbum(this)'><i class='fa fa-picture-o' aria-hidden='true'></i><span class='photoSelectedText'> Select Photo Album</span></a>";
				break;
		}
										
										
										
										
										
		if(JSONKeyExists(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content'])) {
			JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content'].push({"ContentID": parseInt(Object.keys(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content']).length) + 1, "ContentType": ui.helper.context.id, "Value": ""});	
		} else {
			$.extend( JSONValue[CurrentIndex].Columns[CurrentColumnIndex], {"Content" : [{"ContentID": 1, "ContentType": ui.helper.context.id, "Value":""}]});
		}
										
										
		$("#row" + RowID + " #"+ event.target.id +".ColumnElement .CurrentContent").append("<div class='ContentSection' id='" + parseInt(Object.keys(JSONValue[CurrentIndex].Columns[CurrentColumnIndex]['Content']).length) + "' style='margin-bottom:5px;'>" +NewEditableElement + "</div>");
		
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
	}
	
	function SortableColumns(RowID) {
		$( "#row" +RowID).sortable({
			revert: true,
			stop: function( event, ui ) {							
				var JSONValue = GetPageJSONValue();
				var NewOrder = 1;							
				var NewOrderArray = []
																
				$("#row" + RowID + " .ColumnElement").each(function(index) {
					NewOrderArray.push({
						ColumnID : $(this).attr("id"),
						NewOrder : NewOrder
					});
												
					NewOrder++;							
				});
											
				var CurrentRowIndex = GetJSONRowIndex(RowID);
																																
				//loop through new order array 
				for(var i=0; i< NewOrderArray.length; i++) {
					//loop through current JSON Object
					for(var j=0; j< JSONValue[CurrentRowIndex]["Columns"].length; j++) {
						if(JSONValue[CurrentRowIndex]["Columns"][j].columnID == NewOrderArray[i].ColumnID) {
							JSONValue[CurrentRowIndex].Columns[j]['Order'] = NewOrderArray[i].NewOrder;
						}								
					}
				}
																					
				//alert(JSON.stringify(JSONValue));
				$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
			}
		});
	}
	
	var ResizeRowID
	function ColumnResizable(RowID) {
		$("#rangeRow" + RowID).colResizable({
			liveDrag:true, 
			draggingClass:"rangeDrag", 
			gripInnerHtml:"<div class='rangeGrip'></div>", 
			onDrag:RowSlides,
			onResize:ResizeRows,
			minWidth:33
		});
		
		ResizeRowID = RowID;
	}
	
	
	var ColumnSizes;
									
	function RowSlides(e) {
		//console.log(e);
		var columns = $(e.currentTarget).find("td");
		var ranges = [], total = 0, i, s, w;
		for(i = 0; i<columns.length; i++){
			w = columns.eq(i).width()-10 - (i==0?1:0);
			ranges.push(w);
			total+=w;
								                
		}		 
		for(i=0; i<columns.length; i++){
			ranges[i] = 12*ranges[i]/total;
			carriage = Math.round(ranges[i])-w
			s+= Math.round(ranges[i]) + ",";			
		}		
											
		ColumnSizes = AdminController.removeLastComma(s.replace("undefined", "")).split(",")	
	}
									
	function ResizeRows(e) {
		var ColumnID = '1';
		var columnClassName;
		var ColumnsText;
		
		var RowID = $(e.currentTarget).parent().parent().parent().attr("id");
											
		var JSONValue = JSON.parse($("#HtmlEventJSON").val());
										
		var CurrentIndex = GetJSONRowIndex(RowID);
		
											
		$("#row" + RowID + " .ColumnElement").each(function(index) {
			$(this).attr('class', 'ColumnElement col-md-' + ColumnSizes[index]);
			$(this).data('columnid', ColumnSizes[index]);
			JSONValue[CurrentIndex].Columns[index]['ClassID'] = ColumnSizes[index];						
		});	
											
		$("#HtmlEventJSON").val(JSON.stringify(JSONValue));
	}
	
	
	function showSuperEvents(element) {
		
		if(element.checked) {
			$("#SuperEventPopup").show();	
			var Albums = [];
			$.getJSON( "../../admin/GetSuperEvents", function( data ) {
				$.each( data, function( key, val ) {
					var OnClick= 'AdminController.SelectSuperEvent("' + val.eventID +'", "' + val.eventName + '")';
						
					Albums.push( "<div onclick='" + OnClick + "'>" + val.eventName + "</div>");
				});
					
				$(Albums.join("")).appendTo( ".FormContent" ); 
					
			});
		}		
	}
	
	function SelectSuperEvent(ID, EventName) {
		$("#SuperEventID").val(ID);
		$("#SuperEventSelected").html("Super Event Selected: " + EventName);
		$("#SuperEventPopup").hide();
	}
	
	function JSONKeyExists(obj) {
	   return obj != null
	}
	
	
	return {
		InitializePhotoEvents: InitializePhotoEvents,
		InitializePhotoAlbum: InitializePhotoAlbum,
		InitializeAddPhotos: InitializeAddPhotos,
		MakeFileList: MakeFileList,
		ToggleVisibleImage: ToggleVisibleImage,
		InitializePhotoSingle: InitializePhotoSingle,
		removeValidationMessage: removeValidationMessage,
		AddLayoutRow: AddLayoutRow,
		InitializeAddEventPage: InitializeAddEventPage,
		AddColumnInRow: AddColumnInRow,
		InitializeEditEventPage: InitializeEditEventPage,
		DeleteColumn: DeleteColumn,
		DeleteRow: DeleteRow,
		RowSlides: RowSlides,
		removeLastComma: removeLastComma,
		ResizeRows: ResizeRows,
		UpdateRowOrder: UpdateRowOrder,
		AddContentWidget: AddContentWidget,
		SelectPhoto: SelectPhoto,
		selectPhotoID: selectPhotoID,
		ClosePopup: ClosePopup,
		SelectPhotoAlbum: SelectPhotoAlbum,
		selectAlbumID: selectAlbumID,
		SelectPhotoFromAlbum: SelectPhotoFromAlbum,
		SelectEventPhoto: SelectEventPhoto,
		DroppableLines: DroppableLines,
		SortableColumns: SortableColumns,
		ColumnResizable: ColumnResizable,
		showSuperEvents: showSuperEvents,
		SelectSuperEvent: SelectSuperEvent,
		InitializeNewsLetterForm: InitializeNewsLetterForm,
		InitializeEmployeeForm: InitializeEmployeeForm 
	}
}();
