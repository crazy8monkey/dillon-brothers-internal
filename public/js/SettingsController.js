var SettingsController = function() {
	
	var typingTimer;
	var AlreadyHit = false;
	var LabelAlreadyHit = false;

	function InitializeSocialMediaForm() {
		$("#SocialMediaForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function InventoryCategoryForm() {
		$("#InventoryCategoryForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});		
	}
	
	function ColorSelect() {
		$("#ConversionColorSelect").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});		
	}
	
	function SpecLabelForm() {
		$("#SpecLabelForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});		
	}
	
	function InitializeUserForm() {
		$("#UserSubmitForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			

			Globals.AjaxFileUpload(formURL, formData, 
				function(responses) {
					if(responses.ValidationErrors) {
						var validationMessagesArray = responses.ValidationErrors;
						
						var ValidationErrors = responses.ValidationErrors.length;
						for (var i = 0; i < ValidationErrors; i++) {
						   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
						   $("#" + validationMessagesArray[i].inputID).html(validationMessagesArray[i].errorMessage);	
						   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
						   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
						   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
						}
						
						alert("Please fix the errors on the screen");
						$("#ToastMessage").hide();
						$("#uploadingPhotosText").hide()
					}
					
					if(responses.MySqlError) {
						$("#ToastMessage").addClass('error');
			 			$("#ToastMessage").hide().fadeIn().html(responses.MySqlError);
			 		}
					
					if(responses.redirect) {
						$("#ToastMessage").addClass('success');
			 			$("#ToastMessage").hide().fadeIn().html('Your changes has been saved, Please wait for the page to refresh');
			 			Globals.redirect(responses.redirect); 
			 		}
				}
			)		
		});
	}
	
	
	function InitializeInventoryForm() {
		$("#InventorySettingsForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
		
		$("#SpecSettingsContainer").sortable({
			placeholder: "ui-state-highlight",
			stop: function( event, ui ) {
				//console.log(event);
				//console.log(event.target);	
				var groupIDs = []
				$(".specGroupContainer").each(function(index) {
					groupIDs.push($(this).attr("id"));
				});
				//console.log(groupIDs)
				
				Globals.AjaxPost(Globals.WebsitePath() + "settings/save/specorder", 
							     {
									specGroupID : groupIDs
								 }, 
							     function(responses) {
							     	if(responses.groupOrderSaved) {
							     		$("#SpecificationGroupResponses").show().html("Spec Group Order Saved").delay(1000).fadeOut();	
							     		$("#ToastMessage").hide();
							     	}
							     	
								}
				);	
			}
		});
	}
	
	function SpecGroupForm() {
		$("#SpecLabelForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
		
		
		$("#SpecLabelList").sortable({
			placeholder: "ui-state-highlight ui-state-highlight-label"
		});
	}
	
	function NewSpecGroupForm() {
		$("#SpecLabelForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
		
	}	
	
	function InitializeNewDataConversion() {
		$("#DataConversionForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function SaveLabel(Element, ID) {
		//LabelAlreadyHit
		var GroupIDValue = $(Element).parent().parent().parent().parent().parent().attr("id");
		
		$(Element).donetyping(function() {
       		if(LabelAlreadyHit == false) {
       			$(Element)
       			
       			Globals.AjaxPost(Globals.WebsitePath() + "settings/save/labelsettings/" + ID, 
							     {
									labelText : $(Element).val(),
									GroupID:  GroupIDValue
								 }, 
							     function(responses) {
							     	if(responses.SaveResponses) {
							     		if(responses.SaveResponses.NewLabel == true) {
								     		$(Element).parent().parent().attr("id", "labelID" + responses.SaveResponses.labelID);
								     		
								     		$("#" + GroupIDValue + ".LabelContentList .labelLine:last-child .labelChangeMessage").html(" | New Label Saved").delay(1000).fadeOut()
								     		$("#" + GroupIDValue + ".LabelContentList .labelLine:last-child .labelChangeMessage").attr("style", "color: #1da906");
								     		$("#" + GroupIDValue + ".LabelContentList .labelLine:last-child .ButtonActions").show();
								     		$("#" + GroupIDValue + ".LabelContentList .labelLine:last-child .DeleteLabel").show();
								     		
								     		$("#" + GroupIDValue + ".LabelContentList .labelLine:last-child .ButtonActions a").attr("onclick", "SettingsController.DeleteLabel(" + responses.SaveResponses.labelID +")");
								     		//SettingsController.DeleteLabel(<?php echo $label['labelID'] ?>)
								     		
								     		$(Element).attr("onkeyup", "SettingsController.EditLabel(this, " + responses.SaveResponses.labelID + ")");
								     		
								     		$("#" + GroupIDValue + ".LabelContentList").sortable('refresh');	
							     		} else {
							     			$("#labelID" + responses.SaveResponses.labelID + " .labelChangeMessage").attr("style", "color: #1da906");
							     			$("#labelID" + responses.SaveResponses.labelID + " .labelChangeMessage").html(" | Label Saved").delay(1000).fadeOut()
							     		}
							     		
							     		//$(value).load(Globals.WebsitePath() + "settings/type/inventory"); 
							     		LabelAlreadyHit = false;	
							    	}
								}
				);	
       			//console.log("label edit is hit");
       			
       		}
       		
			LabelAlreadyHit = true;		     
       	}, 1000)
		
		
	}
	
	function NewColor(Element) {
		$(Element).donetyping(function() {
       		if(LabelAlreadyHit == false) {
       			
       			Globals.AjaxPost(Globals.WebsitePath() + "settings/save/color", 
							     {
									ColorText : $(Element).val()
								 }, 
							     function(responses) {
							     	if(responses.NewColor) {
							     		$("#NewColorElement").clone(true).show().removeAttr("id").appendTo("#CurrentColorsList");
							     		$("#CurrentColorsList .ColorLineOption:last-child .newColorText").html(responses.NewColor.Text);
							     		$("#CurrentColorsList .ColorLineOption:last-child  input[type='checkbox']").val(responses.NewColor.ID);
							     		$("#NewColorElement input[type='text']").val('');
							     		
							     		LabelAlreadyHit = false;	
							    	}
							    	
							    	
							    	if(responses.ValidationErrors) {
											var validationMessagesArray = responses.ValidationErrors;
											
											var ValidationErrors = responses.ValidationErrors.length;
											for (var i = 0; i < ValidationErrors; i++) {
											   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
											   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
											   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
											   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
											}
											
											alert("Please fix the errors on the screen");
											$("#uploadingPhotosText").hide();
										LabelAlreadyHit = false;		
									}
							    	
								}
				);	
       			//console.log("label edit is hit");
       			
       		}
       		
			LabelAlreadyHit = true;		     
       	}, 1000)
	}


	function DeleteGroup(groupID) {
		$("#" + groupID + ".specGroupContainer").remove()
		
		Globals.AjaxPost(Globals.WebsitePath() + "settings/delete/specgroup/" + groupID, 
							     {
									GroupID : groupID
								 }, 
							     function(responses) {
							     	if(responses.Deleted) {
							     		$("#SpecificationGroupResponses").show().html("Spec Group Deleted").delay(1000).fadeOut();	
							     	}
							     	
								}
				);	
	}
	
	function DeleteLabel(labelID) {
		$("#labelID" + labelID).remove()
		
		Globals.AjaxPost(Globals.WebsitePath() + "settings/delete/speclabel/" + labelID, 
							     {
									LabelID : labelID
								 }, 
							     function(responses) {
							     	
							     	
								}
				);	
	}
	
	
	
	function StoreEmailNotifications() {
		$(".EmailNotificationSettings").each(function(index) {
			$(this).submit(function(e){
				e.preventDefault();
				
				var formURL = $(this).attr("action");
				var formData = new FormData($(this)[0]);
	
				Globals.AjaxFileUpload(formURL, formData, 
					function(responses) {
						if(responses.ValidationErrors) {
							var validationMessagesArray = responses.ValidationErrors;
							
							var ValidationErrors = responses.ValidationErrors.length;
							for (var i = 0; i < ValidationErrors; i++) {
							   $("#" + index + " .inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
							   $("#" + index + " .inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
							   $("#" + index + " .inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
							   $("#" + index + " .inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
							}
							
							alert("Please fix the errors on the screen");
							$("#uploadingPhotosText").hide()
						}
						
						if(responses.MySqlError) {
							$("#ToastMessage").addClass('error');
				 			$("#ToastMessage").hide().fadeIn().html(responses.MySqlError);
				 		}
						
						if(responses.redirect) {
							$("#ToastMessage").addClass('success');
				 			$("#ToastMessage").hide().fadeIn().html('Your changes has been saved, Please wait for the page to refresh');
				 			Globals.redirect(responses.redirect); 
				 		}
					})		
			});			
		})
		
		
		
		
		$("#WaterMarkForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, formData, Globals.MultipleErrorResponses)		
		});
		
	}
	
	function ColorSinglePage() {
		$("#ColorConversionRuleList").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
		
		$("#ColorSingleForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function GenericColorSingle() {
		$("#GenericColorSingleForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}

	function InitializeDonationSettings() {
		$("#DonationSubmission").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function AddConversionRule(type) {
		switch(type) {
			case 1:
				$("#NewConversionRuleElementSpecMapping").clone(true).show().removeAttr("id").appendTo("#ConversionRules");
				break;
			case 2:
				Globals.redirect(Globals.WebsitePath() + 'settings/create/colorrule');
				break;
			case 3:
				$("#NewConversionRuleElementInventoryManufacture").clone(true).show().removeAttr("id").appendTo("#ConversionRules");
				break;
		}
		
		
	}
	
	function AddColorRule() {
		$("#NewColorRule").clone(true).show().removeAttr("id").appendTo("#ConversionRules");
		
		
	}
	
	function ShowOtherOptions(Element) {
		if($(Element).val() == 1) {
			$("#specLabels").show();
		} else {
			$("#specLabels").hide();
		}
		
	}
	
	function SelectColorPopup(conversionID) {
		newwindow=window.open(Globals.WebsitePath() + 'settings/selectcolor/' + conversionID,'name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;
	}
	
	function OpenSpecTab() {
		$("#specSettingsContent, #specTab .selectedTab").show();
		$("#categoryTab .selectedTab,#colorTab .selectedTab, #CategorySettingsContent").hide();
		
		
	}
	
	function OpenCategoryTab() {
		$("#specSettingsContent, #specTab .selectedTab, #colorTab .selectedTab, #ColorSettingsContent").hide();
		$("#categoryTab .selectedTab, #CategorySettingsContent").show();
	}
	
	function OpenColorTab() {
		$("#colorTab .selectedTab, #ColorSettingsContent").show();
		$("#categoryTab .selectedTab, #specSettingsContent, #specTab .selectedTab, #CategorySettingsContent").hide();
		
	}
	
	function ApplicationSettings() {
		$("#ApplicationForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, postData, Globals.MultipleErrorResponses);		
		});
	}
	
	function OpenStoreEmailSettings() {
		$("#EmailNotificationSettingsTab, #emailTab .selectedTab").show();
		$("#waterMarkTab .selectedTab, #WaterMarkSettingsTab").hide();
	}
	
	function OpenStoreWaterMarkSettings() {
		$("#EmailNotificationSettingsTab, #emailTab .selectedTab").hide();
		$("#waterMarkTab .selectedTab, #WaterMarkSettingsTab").show();
	}
	
	function toggleSubCategories(type, element) {
		
		switch(type) {
			case "specials":
				var $element = $("#specialsSubCategories");		
				break;
			case "blog":
				var $element = $("#blogSubCategories");		
				break;
			case "inventory":
				var $element = $("#inventorySubPermissions");	
				break;
			case "settings":
				var $element = $("#specialSubPermissions");	
				break;
		}
		
		if(element.checked) {
			$element.show();
		} else {
			$element.hide();
			$element.find('input').removeAttr('checked')
		}
		
		
		
	}
	
	function toggleRadioSubPermission(type, element) {
		switch(type) {
			case "inventory":
				var $element = $("#inventorySubPermissions");	
				break;
		}
		
		if($(element).val() == 1) {
			$element.show();
		} else {
			$('input[name="InventorySpecificationManagement"]').removeAttr('checked')
			$element.hide();
		}
	}
	
	function ExpandGroupLabels(element) {
		$(element).parent().next().toggle();
		$(element).find('.labelExpand').toggleText('-', '+')
	}
	
	function InitializeShortURLSettings() {
		$(".ShortURLSingle").each(function() {
			$("form", this).submit(function(e){
				e.preventDefault();
				
				var formURL = $(this).attr("action");
				var postData = $(this).serialize();
				
				Globals.AjaxPost(formURL, postData, function(responses) {
					if(responses.ValidationErrors) {
						var validationMessagesArray = responses.ValidationErrors;
						
						var ValidationErrors = responses.ValidationErrors.length;
						for (var i = 0; i < ValidationErrors; i++) {
						   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
						   $("#inputID" + validationMessagesArray[i].inputID + " input", this).css("border", '1px solid #c30000');
						   $("#inputID" + validationMessagesArray[i].inputID + " select", this).css("border", '1px solid #c30000');
						   $("#inputID" + validationMessagesArray[i].inputID + " textarea", this).css("border", '1px solid #c30000');
						}
						
						alert("Please fix the errors on the screen");
						$("#uploadingPhotosText").hide();
						$("#ToastMessage").hide()
					}
					
					//if(responses.MySqlError) {
					//	$("#ToastMessage").addClass('error');
			 		//	$("#ToastMessage").hide().fadeIn().html(responses.MySqlError);
			 		//}
					
					if(responses.redirect) {
						$("#ToastMessage").addClass('success');
						$("#ToastMessage").removeClass('LoadingSpinner');
					
			 			$("#ToastMessage").hide().fadeIn().html('Your changes has been saved, Please wait for the page to refresh');
			 			Globals.redirect(responses.redirect); 
			 		}
				})		
			});
		});
	}
	
	function InitializeShortUrlSingle() {
		$("#ShortUrlSingleForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function InitializeSMSSettings() {
		$(".SMSStoreForm").each(function(index) {
			$(this).submit(function(e){
				e.preventDefault();
				
				var formURL = $(this).attr("action");
				var postData = $(this).serialize();
	
				Globals.AjaxPost(formURL, postData, 
					function(responses) {
						if(responses.ValidationErrors) {
							var validationMessagesArray = responses.ValidationErrors;
							
							var ValidationErrors = responses.ValidationErrors.length;
							for (var i = 0; i < ValidationErrors; i++) {
							   $("#" + index + ".inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
							   $("#" + index + ".inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
							   $("#" + index + ".inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
							   $("#" + index + ".inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
							}
							
							alert("Please fix the errors on the screen");
							$("#ToastMessage").hide()
						}
						
						if(responses.MySqlError) {
							$("#ToastMessage").addClass('error');
				 			$("#ToastMessage").hide().fadeIn().html(responses.MySqlError);
				 		}
						
						if(responses.redirect) {
							$("#ToastMessage").addClass('success');
				 			$("#ToastMessage").hide().fadeIn().html('Your changes has been saved, Please wait for the page to refresh');
				 			Globals.redirect(responses.redirect); 
				 		}
					})		
			});			
		})
		
		$("#OutBoundTextDisclaimerForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
		
		
		$(".settingsSaleDeptNumber, .settingsServiceDeptNumber, .settingsPartsDeptNumber").mask("(999) 999-9999",{placeholder:" "});
	}
	
	function RefreshRedirectActions(element, key) {
		if($(element).val() == '301Redirect') {
			$('optgroup.eventContentOptions' + key).show();
			$('optgroup.blogContentOptions' + key).show();
		}
		
		if($(element).val() == '410Redirect') {
			$('optgroup.eventContentOptions' + key).hide();
			$('optgroup.blogContentOptions' + key).hide();
		}
	}
	
	function InitializeChatChannel() {
		$("#ChatChannelForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function InitializeSingleChatChannel() {
		$("#UserChannelForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	return {
		InitializeSocialMediaForm: InitializeSocialMediaForm,
		InitializeInventoryForm: InitializeInventoryForm,
		SaveLabel: SaveLabel,
		DeleteGroup: DeleteGroup,
		AddConversionRule: AddConversionRule,
		DeleteLabel: DeleteLabel,
		ShowOtherOptions: ShowOtherOptions,
		SelectColorPopup: SelectColorPopup,
		NewColor: NewColor,
		ColorSelect: ColorSelect,
		InventoryCategoryForm: InventoryCategoryForm,
		SpecLabelForm: SpecLabelForm,
		SpecGroupForm: SpecGroupForm,
		NewSpecGroupForm: NewSpecGroupForm,
		OpenSpecTab: OpenSpecTab,
		OpenCategoryTab: OpenCategoryTab,
		OpenColorTab: OpenColorTab,
		StoreEmailNotifications: StoreEmailNotifications,
		ApplicationSettings: ApplicationSettings,
		OpenStoreEmailSettings: OpenStoreEmailSettings,
		OpenStoreWaterMarkSettings: OpenStoreWaterMarkSettings,
		InitializeUserForm: InitializeUserForm,
		toggleSubCategories: toggleSubCategories,
		toggleRadioSubPermission: toggleRadioSubPermission,
		AddColorRule: AddColorRule,
		ColorSinglePage: ColorSinglePage,
		GenericColorSingle: GenericColorSingle,
		InitializeNewDataConversion: InitializeNewDataConversion,
		ExpandGroupLabels: ExpandGroupLabels,
		InitializeDonationSettings: InitializeDonationSettings,
		InitializeShortURLSettings: InitializeShortURLSettings,
		InitializeShortUrlSingle: InitializeShortUrlSingle,
		RefreshRedirectActions: RefreshRedirectActions,
		InitializeSMSSettings: InitializeSMSSettings,
		InitializeChatChannel: InitializeChatChannel,
		InitializeSingleChatChannel: InitializeSingleChatChannel
	}
}();
