var CommunicationController = function() {

	function InitializeOutboundText() {
		$("#OutbountText").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
		
		$("#customerPhoneNumber").mask("(999) 999-9999",{placeholder:" "});
	}
	
	var ChatClient;
	var currentTaskID;
	var CurrentUniqueChannelName;
	
	
	
	function SetCurrentTaskID(id) {
		currentTaskID = id;
	}
	
	function GetCurrentTaskID() {
		return currentTaskID;
	}
	
	
	function InitializeConversation(uniqueChannelName, taskID, userName) {
		SetCurrentTaskID(taskID);
		$.getJSON(Globals.WebsitePath() + 'communication/chattoken', {
	        device: 'browser',
	        UserName: userName
	    }, function(data) {
	    	currentTaskID = taskID;
	    	CurrentUniqueChannelName = uniqueChannelName;
	    	
	    	chatClient = new Twilio.Chat.Client(data);
        	chatClient.getSubscribedChannels().then(JoinChannel);
        	
        	console.log(chatClient);
	    });
	    
	    
	 	var $input = $('#NewMessage');
		$input.on('keydown', function(e) {
			if (e.keyCode == 13) {
				chatChannel.sendMessage($input.val(), {'taskID': taskID, 'fromCustomer': 0});
				$input.val('');
			}
		});
	}
	
	
	function JoinChannel() {
        // Get the general chat channel, which is where all the messages are
        // sent in this simple application
        var promise = chatClient.getChannelByUniqueName(CurrentUniqueChannelName);
        promise.then(function(channel) {
           	chatChannel = channel;
            //console.log('Found general channel:');
            //console.log(chatChannel);
            setupChannel();
        });
    }
	
	function setupChannel() {
        // Join the general channel
        chatChannel.join().then(function(channel) {
        	$("#NewMessage").show();
        });

        // Listen for new messages sent to the channel
        chatChannel.on('messageAdded', function(message) {
        	console.log(message);
        	if(message.attributes.taskID == currentTaskID) {
        		printMessage(message.author, message.body, message);	
        	}
        });
    }
    
     function printMessage(fromUser, message, msgObj) {
        var className;
        if(msgObj.attributes.fromCustomer == 1) {
        	className = 'msgBubbleCustomer';
        } else {
        	className = 'msgBubbleDillonUser';
        }
        var newMessageObj = '<div class="row" style="margin:0px">' +
								'<div class="col-md-12">' +
									'<div style="clear:both">' +
										'<div class="' + className + '">' +
											 message +
											'<div class="bubbleArrow"></div>' +
										'</div>' + 
									'</div>' +								
								'</div>' +						
							'</div>';
       
        $('#messagesLogged').append(newMessageObj);
        $('#messagesLogged').scrollTop($('#messagesLogged')[0].scrollHeight);
    }
	
	function ScrollToBottom() {
		$('#ChatMessageContainer').scrollTop($('#ChatMessageContainer')[0].scrollHeight);
	}
	
	function openChatConversation(id) {
		$("#ViewingSingleConversation").show();
		
		$.getJSON(Globals.WebsitePath() + 'communication/getconversation/' + id, function(data) {
	        console.log(data);
	        $.each(data, function (key, val) {
	        	var currentDate = new Date(val.dateentered).toLocaleString();
	        	
	        	console.log(val.dateentered);
	        	
	        	if(val.endofConversation == 0) {
	        		if(val.fromCustomer == 1) {
						var customerBubble = $( "#NewCustomerBubble" );
						customerBubble.clone().appendTo( "#ConversationSingleView" ).show().removeAttr('id').attr('class', 'row CustomerRow');
						$(".CustomerRow:last-child").find('span.msgText').html(val.message);
						$(".CustomerRow:last-child").find('.dateCustomer').html(currentDate);
						
					}
					
					if(val.fromCustomer == 0) {
						var salesmanBubble = $( "#SalesManBubble" );
						salesmanBubble.clone().appendTo( "#ConversationSingleView" ).show().removeAttr('id').attr('class', 'row SalesmanRow');
						$(".SalesmanRow:last-child").find('span.msgText').html(val.message);
						$(".SalesmanRow:last-child").find('.dillonUser').html(currentDate);
					}	
	        	} else {
	        		var EndBubbleNotificaiton = $("#EndConversationIndicator");
	        		EndBubbleNotificaiton.clone().appendTo( "#ConversationSingleView" ).show().removeAttr('id').attr('class', 'row ConversationClosedIndicator');
	        		$(".ConversationClosedIndicator").html(val.message);
	        	}			
		    });
	    });
		
		
	}
	
	function SendCloseChatMessage(id) {
		chatChannel.sendMessage('Conversation Closed By Salesman', {'taskID': GetCurrentTaskID(), 'fromCustomer': 0, 'taskComplete': true});
		
		Globals.AjaxPost(Globals.WebsitePath() + 'communication/closeconversation/' + id, {}, function(responses) {
			if(responses.conversationclosed) {
				window.location.assign(Globals.WebsitePath() + 'communication');	
			}
		});
	}
	
	
	function CloseSingleConversation() {
		$("#ViewingSingleConversation").hide();
		$("#ViewingSingleConversation .FormContent").html('');
	}
	
	return {
		InitializeOutboundText: InitializeOutboundText,
		InitializeConversation: InitializeConversation,
		openChatConversation: openChatConversation,
		CloseSingleConversation: CloseSingleConversation,
		SendCloseChatMessage: SendCloseChatMessage
	}
}();
