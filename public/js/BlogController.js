var BlogController = function() {
	
	//public
	function InitializeBlogPostSingle(AlbumID = false) {
		if(AlbumID) {
			CKEDITOR.replace( 'blogPostContent', {
			
				filebrowserUploadUrl: Globals.WebsitePath() + "blog/uploadImages/" + AlbumID,
				filebrowserImageUploadUrl: Globals.WebsitePath() + "blog/uploadImages/" + AlbumID,
			});
		}
		
		
		$("#BlogPostSingleForm").submit(function(e){
			e.preventDefault();
			
			for ( instance in CKEDITOR.instances ) {
				CKEDITOR.instances[instance].updateElement();	
			}
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)	
		});
		
		//MetaDataString
	}
	
	function InitializeBlogCategorySingle() {
		$("#BlogCategorySingleForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function ExpandBlogComments(element) {
		$(element).find(".blogExpand").toggleText('-', '+');	
		$(element).next().toggle();	
		
		$(".blogExpand").not($(element).find(".blogExpand")).html('+');
		$(".CommentList").not($(element).next()).hide();
			
	}
	
	function getEditHistory(blogID) {
		$("#BlogEditHistory").show();
		var editHistory = [];
		$.getJSON(Globals.WebsitePath() +  "blog/GetBlogEditHistory/" + blogID, function( data ) {
			$.each( data, function( key, val ) {
				editHistory.push("<div>" + val.value +"</div>" );
			});
			
			$(editHistory.join("")).appendTo("#BlogEditHistory .FormContent"); 
		});
	}
	
	function GetPhotos(albumID, blogID) {
		$("#PhotosInBlog").show();
		var editHistory = [];
		$.getJSON(Globals.WebsitePath() +  "blog/getPhotos/" + albumID, function( data ) {
			$.each( data, function( key, val ) {
				
				var photoPath = Globals.PhotosPath() + val.ParentDirectory + "/blog/" + val.albumFolderName + "/" + val.photoName + "-s." + val.ext;		
						
				editHistory.push("<a href='javascript:void(0);' onclick='BlogController.SaveBlogPhoto(" + blogID +", " + val.photoID + ")'><div class='photoThumbNail' style='background:url(" + photoPath + ") no-repeat center;'></div></a>" );
			});
			
			$(editHistory.join("")).appendTo("#PhotosInBlog .FormContent"); 
		});
	}
	
	function SaveBlogPhoto(blogID, photoID) {
		Globals.AjaxPost(Globals.WebsitePath() + "blog/SaveMainPhoto/" + blogID + "/" + photoID, {}, function(responses) { });
		
		location.reload()
	}

	function closeEditHistory() {
		$("#BlogEditHistory").hide();
		$("#BlogEditHistory .FormContent").html('');
	}
	
	function closeBlogPhotoPopup() {
		$("#PhotosInBlog").hide();
		$("#PhotosInBlog .FormContent").html('');
	}
	
	function UploadMainPhoto(name) {
		newwindow=window.open(Globals.WebsitePath() + 'photos/uploadSingle/' + name + '?blogMainPhoto=true&singlePhoto=true','name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;
	}
	
	
	return {
		InitializeBlogPostSingle: InitializeBlogPostSingle,
		InitializeBlogCategorySingle: InitializeBlogCategorySingle,
		ExpandBlogComments: ExpandBlogComments,
		getEditHistory: getEditHistory,
		closeEditHistory: closeEditHistory,
		closeBlogPhotoPopup: closeBlogPhotoPopup,
		GetPhotos: GetPhotos,
		SaveBlogPhoto: SaveBlogPhoto,
		UploadMainPhoto: UploadMainPhoto
	}
}();
