var NewsletterController = function() {

	function InitializeNewsLetterForm() {
		$("#NewsArchiveForm").submit(function(e){			
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, formData, Globals.MultipleErrorResponses)
		});
	}

	
	return {
		InitializeNewsLetterForm: InitializeNewsLetterForm
	}
}();
