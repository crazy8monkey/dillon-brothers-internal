var SpecialsController = function() {
	//private
	function MultipleErrorResponses(responses) {
		
		if(responses.ValidationErrors) {
			var VehicleErrors = '';
			for (var i = 0; i < responses.ValidationErrors.length; i++) {
				VehicleErrors += "<div>" + responses.ValidationErrors[i].errorMessage + "</div>";
				
			}
			Globals.ScrollToTop("#ErrorMessages");
			$("#ErrorMessages").html(VehicleErrors)
		}
		
		if(responses.redirect) {
 			Globals.redirect(responses.redirect); 
 		}
	}

	function VehicleFormResponses(responses) {
		
	}

	//public
	function InitializeSpecialForm() {
		$("#SpecialForm").submit(function(e){			
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = new FormData($(this)[0]);
			
			Globals.AjaxFileUpload(formURL, postData, Globals.MultipleErrorResponses)	

		});
		
		$("#SpecialItemForm").submit(function(e){			
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, 
				function(responses) {
					
					if(responses.ValidationErrors) {
						var validationMessagesArray = responses.ValidationErrors;
						
						var ValueErrorIndex = null;
						var TypeErrorIndex = null;
						var ExpiredDateErrorIndex = null;
						var DescriptoinErrorIndex = null;
						var DiscliamerErrorIndex = null;
						var LinkedInventoryIndex = null;
						var DuplicateCombinationIndex = null;
						
						var ValidationErrors = responses.ValidationErrors.length;
						for (var i = 0; i < ValidationErrors; i++) {
							if(validationMessagesArray[i].hasOwnProperty('ValueErrors')) {
								ValueErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('TypeErrors')) {
								TypeErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('ExpiredErrors')) {
								ExpiredDateErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('DescriptionErrors')) {
								DescriptoinErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('DisclaimerErrors')) {
								DiscliamerErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('LinkedInventoryErrors')) {
								LinkedInventoryIndex = i
							}
							
						
							
								
						}
							
						console.log(ValueErrorIndex);	
						console.log(TypeErrorIndex);	
						console.log(ExpiredDateErrorIndex);	
						console.log(DescriptoinErrorIndex);	
						console.log(DiscliamerErrorIndex);	
						
						$("#ContentList .incentiveItem").each(function(index) {
							
							if(ValueErrorIndex != null) {
								$(this).find('.valueError').html(validationMessagesArray[ValueErrorIndex].ValueErrors[index].ValueMessage);	
							} else {
								$(this).find('.valueError').html('');	
							}
							
							if(TypeErrorIndex != null) {
								$(this).find('.typeError').html(validationMessagesArray[TypeErrorIndex].TypeErrors[index].TypeMessage);	
							} else {
								$(this).find('.typeError').html('');
							}
							
							if(ExpiredDateErrorIndex != null) {
								$(this).find('.expiredError').html(validationMessagesArray[ExpiredDateErrorIndex].ExpiredErrors[index].ExpiredMessage);
							} else {
								$(this).find('.expiredError').html('');
							}
							
							if(DescriptoinErrorIndex != null) {
								$(this).find('.descriptionError').html(validationMessagesArray[DescriptoinErrorIndex].DescriptionErrors[index].DescriptionMessage);
							} else {
								$(this).find('.descriptionError').html('');
							}
							
							if(DiscliamerErrorIndex != null) {
								$(this).find('.disclaimerError').html(validationMessagesArray[DiscliamerErrorIndex].DisclaimerErrors[index].DisclaimerMessage);
							} else {
								$(this).find('.disclaimerError').html('');
							}
							
							
							if(DuplicateCombinationIndex != null) {
								$(this).find('.duplicateCombinationError').html(validationMessagesArray[DuplicateCombinationIndex].DuplicateCombinationErrors[index].DuplicateMessage);
							} else {
								$(this).find('.duplicateCombinationError').html('');
							}
							
						});
						
						
						
						alert("Please fix the errors on the screen");
						$("#uploadingPhotosText").hide();
						$(".LoadingSpinner").hide();
					}
					
					if(responses.incentiveSaved) {
						location.reload();
					}
					
				}
			)	

		});
		

	}

	function InitializeVehicleBrandPage() {
		$("#BrandDetailsForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			

			Globals.AjaxFileUpload(formURL, formData, Globals.MultipleErrorResponses)		
		});
	}

	function InitializeVehicleForm() {
		$("#VehicleForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, MultipleErrorResponses)		
		});
	}
	
	function InitializeVehicleSingle() {
		$("#VehicleSingleForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}
	
	function InitializePartsSingle() {
		$("#PartsSingleForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});		
	}

	function InitializeIncentiveSingleForm() {
		$("#ServiceIncentive").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)		
		});
	}

	
	function InitializePartsBrandForm() {
		
	}
	
	
	function ToggleVehiclesList() {
		$("#SpecialsList").toggle();
		$("#SpecialVehiclesExpand").toggleText('-', '+');
		
		
		$("#SpecialDetailForm").hide();
		$("#SpecialDetailExpand").html('+');
	}
	
	function AddCar() {
		$( ".VehicleElement" ).clone().appendTo( "#VehiclesList" );
	}
	
	function AddItem(refresh = false) {
		$("#NewRelatedItem").clone().show().appendTo( "#ContentList" ).removeAttr("id");
		
		if(refresh) {
			$(".DropInventorySection .inventoryList").droppable({
				accept: '#FilteredInventory .inventoryItem',
				drop: InventoryDropped
			});		
		}
		
		
	}
	
	function RefreshParent() {
		opener.location.reload();
	}
	
	function LinkInventoryToIncentive(specialID) {
		newwindow = window.open(Globals.WebsitePath() + 'specials/addinginventorytoincentive/' + specialID ,'name','height=1200,width=1200');
		if (window.focus) {
			newwindow.focus()
		}
		
		//return false;
	}
	
	function OpenUpFilterContent(element) {
		$(element).next().toggle();
	} 
	
	function GenerateYearFilterItem(Html) {	
		$("#YearContent").html(Html);	
	}
	
	function GenerateCategoryFilterItems(html) {
		$("#categoryContent").html(html);	
	}
	
	
	function GenerateManufactureFilterItems(html) {
		$("#ManufactureContent").html(html);
	}
	
	function GenerateModelFilterItems(html) {					
		$("#ModelNumberContent").html(html);
	}
	
	function CheckUrlHashTag() {
		if(!window.location.hash) {
			window.location.hash = '#FilterResult';
		}
	}
	
	function FilterCondition() {
		var postData = $('#filterForm').serialize();
		
		Globals.AjaxPost(Globals.WebsitePath() + 'specials/filter/condition', postData, 
				function(responses) {
					GenerateYearFilterItem(responses.Years);
					GenerateCategoryFilterItems(responses.Categories);
					GenerateManufactureFilterItems(responses.Manufacture);
					GenerateModelFilterItems(responses.Model);	
				}
			
			
		);
	}
	
	function FilterYear() {
		var postData = $('#filterForm').serialize();
		
		Globals.AjaxPost(Globals.WebsitePath() + 'specials/filter/year', postData, 
				function(responses) {
					GenerateCategoryFilterItems(responses.Categories);
					GenerateManufactureFilterItems(responses.Manufacture);
					GenerateModelFilterItems(responses.Model);	
				}
				
		);
	}
	
	function FilterCategory() {
		var postData = $('#filterForm').serialize();
		
		Globals.AjaxPost(Globals.WebsitePath() + 'specials/filter/category', postData, 
				function(responses) {
					GenerateYearFilterItem(responses.Years);
					GenerateManufactureFilterItems(responses.Manufacture);
					GenerateModelFilterItems(responses.Model);	
				}
			
			
		);
	}
	
	function FilterManufacture() {
		var postData = $('#filterForm').serialize();
		
		Globals.AjaxPost(Globals.WebsitePath() + 'specials/filter/manufacture', postData,  
				function(responses) {
					GenerateCategoryFilterItems(responses.Categories);
					GenerateYearFilterItem(responses.Years);
					GenerateModelFilterItems(responses.Model);	
				}
			
			
		);
	}
	
	function FilterModel() {
		var postData = $('#filterForm').serialize();
		
		Globals.AjaxPost(Globals.WebsitePath() + 'specials/filter/model', postData, 
				function(responses) {
					GenerateCategoryFilterItems(responses.Categories);
					GenerateYearFilterItem(responses.Years);
					GenerateManufactureFilterItems(responses.Manufacture);
				}
			
			
		);
	}
	
	function insertParam(key, value) {
	    key = encodeURI(key); value = encodeURI(value);
	
	    var kvp = document.location.search.substr(1).split('#');
	
	    var i=kvp.length; var x; while(i--) 
	    {
	        x = kvp[i].split('=');
	
	        if (x[0]==key)
	        {
	            x[1] = value;
	            kvp[i] = x.join('=');
	            break;
	        }
	    }
	
	    if(i<0) {kvp[kvp.length] = [key,value].join('=');}
	
	    //this will reload the page, it's likely better to store this until finished
	    document.location.search = kvp.join('#'); 
	}
	
	Array.prototype.contains = function(obj) {
	    var i = this.length;
	    while (i--) {
	        if (this[i] === obj) {
	            return true;
	        }
	    }
	    return false;
	}
	function FilterInventory() {
		$("#LinkInventoryForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, 
				function(responses) {
					
					if(responses.ValidationErrors) {
						var validationMessagesArray = responses.ValidationErrors;
						
						var ValueErrorIndex = null;
						var TypeErrorIndex = null;
						var ExpiredDateErrorIndex = null;
						var DescriptoinErrorIndex = null;
						var DiscliamerErrorIndex = null;
						var LinkedInventoryIndex = null;
						var DuplicateCombinationIndex = null;
						
						var ValidationErrors = responses.ValidationErrors.length;
						for (var i = 0; i < ValidationErrors; i++) {
							if(validationMessagesArray[i].hasOwnProperty('ValueErrors')) {
								ValueErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('TypeErrors')) {
								TypeErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('ExpiredErrors')) {
								ExpiredDateErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('DescriptionErrors')) {
								DescriptoinErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('DisclaimerErrors')) {
								DiscliamerErrorIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('LinkedInventoryErrors')) {
								LinkedInventoryIndex = i
							}
							
							if(validationMessagesArray[i].hasOwnProperty('DuplicateCombinationErrors')) {
								DuplicateCombinationIndex = i
							}
							
								
						}
							
						console.log(ValueErrorIndex);	
						console.log(TypeErrorIndex);	
						console.log(ExpiredDateErrorIndex);	
						console.log(DescriptoinErrorIndex);	
						console.log(DiscliamerErrorIndex);	
						console.log(LinkedInventoryIndex);
						
						$("#ContentList .incentiveItem").each(function(index) {
							
							if(ValueErrorIndex != null) {
								$(this).find('.valueError').html(validationMessagesArray[ValueErrorIndex].ValueErrors[index].ValueMessage);	
							} else {
								$(this).find('.valueError').html('');	
							}
							
							if(TypeErrorIndex != null) {
								$(this).find('.typeError').html(validationMessagesArray[TypeErrorIndex].TypeErrors[index].TypeMessage);	
							} else {
								$(this).find('.typeError').html('');
							}
							
							if(ExpiredDateErrorIndex != null) {
								$(this).find('.expiredError').html(validationMessagesArray[ExpiredDateErrorIndex].ExpiredErrors[index].ExpiredMessage);
							} else {
								$(this).find('.expiredError').html('');
							}
							
							if(DescriptoinErrorIndex != null) {
								$(this).find('.descriptionError').html(validationMessagesArray[DescriptoinErrorIndex].DescriptionErrors[index].DescriptionMessage);
							} else {
								$(this).find('.descriptionError').html('');
							}
							
							if(DiscliamerErrorIndex != null) {
								$(this).find('.disclaimerError').html(validationMessagesArray[DiscliamerErrorIndex].DisclaimerErrors[index].DisclaimerMessage);
							} else {
								$(this).find('.disclaimerError').html('');
							}
							
							if(LinkedInventoryIndex != null) {
								$(this).find('.linkedInvetoryError').html(validationMessagesArray[LinkedInventoryIndex].LinkedInventoryErrors[index].inventoryMessage);
							} else {
								$(this).find('.linkedInvetoryError').html('');
							}
							
							if(DuplicateCombinationIndex != null) {
								$(this).find('.duplicateCombinationError').html(validationMessagesArray[DuplicateCombinationIndex].DuplicateCombinationErrors[index].DuplicateMessage);
							} else {
								$(this).find('.duplicateCombinationError').html('');
							}
							
						});
						
						
						
						alert("Please fix the errors on the screen");
						$("#uploadingPhotosText").hide()
					}
					
					if(responses.incentiveSaved) {
						opener.location.reload();
						window.close();
					}
					
					
					
				}
			);		
		});	
			
			
		
		
		var Category = '';	
		
		$("#filterForm").submit(function(e){
			e.preventDefault();
		
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(Globals.WebsitePath() + 'specials/filter/inventory', postData, 
				function(responses) {
					var BikeName = '';
					var YearSelect = '';
					
					
					$(".FilterSectionContainer").removeAttr('style');
					
					$(".Overlay").hide();
					
					$("#ModelFilterShow").show();
					
					
					$("#CurrentActiveInventory").html(responses.inventoryList);	
					
					window.location = responses.AppendUrl;
					
				}
			)	
			
		});
		
		//$( "#FilteredInventory .inventoryItem").click(function(e) {
		//	if (e.ctrlKey || e.metaKey) {
		//        $(this).toggleClass("selected");
		//    } else {
		//        $(this).addClass("selected").siblings().removeClass('selected');
		//    }
		//});
		
		$( "#FilteredInventory .inventoryItem input").click(function() {
			if(this.checked) {
				$(this).parent().addClass("selected");
			} else {
				$(this).parent().removeClass("selected");
			}
		});	
			
		

		
		
		$("#NewIncentive").submit(function(e){
			e.preventDefault();
		
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, 
				function(responses) {
					
					if(responses.ValidationErrors) {
						var ValueErrorIndex = null;
						var TypeErrorIndex = null;
						var ExpiredDateErrorIndex = null;
						var DescriptoinErrorIndex = null;
						var DiscliamerErrorIndex = null;
						var DuplicateCombinationIndex = null;
						
						for (var i = 0; i < responses.ValidationErrors.length; i++) {
							if(responses.ValidationErrors[i].errorType == 'specialTypeValue') {
								ValueErrorIndex = i;
							}
							
							if(responses.ValidationErrors[i].errorType == 'specialType') {
								TypeErrorIndex = i;
							}
							
							if(responses.ValidationErrors[i].errorType == 'ExpiredDate') {
								ExpiredDateErrorIndex = i;
							}
							
							if(responses.ValidationErrors[i].errorType == 'Description') {
								DescriptoinErrorIndex = i;
							}
							
							if(responses.ValidationErrors[i].errorType == 'Disclaimer') {
								DiscliamerErrorIndex = i;
							}
							
							
						}
								
						if(TypeErrorIndex != null) {
							$('#NewIncentiveItem .errors .typeError').html(responses.ValidationErrors[TypeErrorIndex].errorMessage);
						} else {
							$('#NewIncentiveItem .errors .typeError').html('');
						}
						
						if(ValueErrorIndex != null) {
							$('#NewIncentiveItem .errors .valueError').html(responses.ValidationErrors[ValueErrorIndex].errorMessage);
						} else {
							$('#NewIncentiveItem .errors .valueError').html('');
						}
					
						if(ExpiredDateErrorIndex != null) {
							$('#NewIncentiveItem .errors .expiredError').html(responses.ValidationErrors[ExpiredDateErrorIndex].errorMessage);
						} else {
							$('#NewIncentiveItem .errors .expiredError').html('');
						}
						
						if(DescriptoinErrorIndex != null) {
							$('#NewIncentiveItem .errors .descriptionError').html(responses.ValidationErrors[DescriptoinErrorIndex].errorMessage);
						} else {
							$('#NewIncentiveItem .errors .descriptionError').html('');
						}
						
						if(DiscliamerErrorIndex != null) {
							$('#NewIncentiveItem .errors .disclaimerError').html(responses.ValidationErrors[DiscliamerErrorIndex].errorMessage);
						} else {
							$('#NewIncentiveItem .errors .disclaimerError').html('');
						}
					
					}
					
					if(responses.incentiveSaved) {
						location.reload();
					}
					
					
				}
			)	
			
		});
		
		$("#AddInventoryToIncentive").submit(function(e){
			e.preventDefault();
		
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData,  
				function(responses) {
					
					if(responses.ValidationErrors) {
						$("#LinkedInventoryError").html(responses.ValidationErrors[0].errorMessage);
					
					}
					
					if(responses.incentiveSaved) {
						location.reload();
					}
					
					
				}
			)	
			
		});
		
		
		
		
	}
	
	function removeElements(text, selector) {
		var wrapped = $(text);
		wrapped.find(selector).remove();
		return wrapped.html();
	}
	
	function MultiSelect(e, item) {
        //Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
		if (!item.hasClass('selected')) {
			item.addClass('selected').siblings().removeClass('selected');
		}
		        
		//////////////////////////////////////////////////////////////////////
		//HERE'S HOW TO PASS THE SELECTED ITEMS TO THE `stop()` FUNCTION:
		        
		//Clone the selected items into an array
		var elements = item.parent().children('.selected').clone();
		        
		//Add a property to `item` called 'multidrag` that contains the 
		//  selected items, then remove the selected items from the source list
		//item.data('multidrag', elements).siblings('.selected').remove();
		        
		//Now the selected items exist in memory, attached to the `item`,
		//  so we can access them later when we get to the `stop()` callback
		        
		//Create the helper
		var helper = $('<li/>');
		return helper.append(elements);
    }
	
	function InventoryDropped(event, ui) {
		for (var i = 0; i < event.toElement.children.length; i++) {
			console.log(event.toElement.children[i].id);
			
			//$(this, event.target.classList0).find('.fa-picture-o').hide();
			//<i style="margin-right:5px; color:#3366cc" class="fa fa-picture-o" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<img src='http://localhost/DillonBrothersPhotos/inventory/5Y4AN12Y1HA101432/-s.' ></i>"></i>
			
			var bikeLine = event.toElement.children[i].innerHTML
			$(this, event.target.classList[0] + ' .inventoryList').append("<div class='linkedInventoryItem' id='" + event.toElement.children[i].id + "'><a href='javascript:void(0);' onclick='SpecialsController.RemoveLinkedInventory(this)'><i class='fa fa-trash-o' aria-hidden='true'></i></a>" + bikeLine + "</div>");					
		}
		
		$(this, event.target.classList[0]).find('.fa-picture-o').hide();
		$(this, event.target.classList[0]).find('input').hide();
		
		$("#FilteredInventory .inventoryItem").removeClass('selected')
		$("#FilteredInventory .inventoryItem input").removeAttr('checked');
		
		var InventoryIDS = '';
		
		var inventory = event.target.children;
			
		$.each( inventory, function( key, val ) {
			InventoryIDS += val.id + ','
		});
			
		
		console.log(event);		
		//console.log($(this, event.target + ' input[name="linkedInventoryIDS[]"]'));
		//
		$(this, event.target).parent().parent().parent().parent().find('input[name="linkedInventoryIDS[]"]').val(InventoryIDS.substring(0, InventoryIDS.length - 1));
	}
	
	function ShowLinkedInventory(incentiveID) {
		$("#LinkedInventoryPoup").show();
		var LinkedInventory = [];
		$.getJSON(Globals.WebsitePath() +  "specials/relatedInventoryByIncentive/" + incentiveID, function( data ) {
			$.each( data, function( key, val ) {
				var bikeName;
				if(val.FriendlyModelName != "") {
					bikeName = val.Year + " " + val.Manufacturer + " " + val.FriendlyModelName 
				} else {
					bikeName = val.Year + " " + val.Manufacturer + " " + val.ModelName
				}
						
				LinkedInventory.push("<div>" + bikeName + " - <strong>" + val.Stock + "</strong></div>" );
			});
			
			$(LinkedInventory.join("")).appendTo("#LinkedInventoryPoup .FormContent"); 
		});
	}
	
	
	function HideLinkedInventory() {
		$("#LinkedInventoryPoup").hide();
		$("#LinkedInventoryPoup .FormContent").html('');
	}
	
	var toggled = false;
	
	function ToggleFilters() {
		toggled = !toggled;
				
		$(".FilterSectionContainer").attr('style', toggled ? 'left:0px' : null);
		
		if(toggled) {
			$(".Overlay").show();
		} else {
			$(".Overlay").hide();
		}
	}
	
	function ApplyCheckboxes() {
		var HashValue = window.location.hash.split('&');
		
		
		
		HashValue.shift();		
		
		
		
		for (var i=HashValue.length-1; i>=0; i--) {
			if(HashValue[i].indexOf('Years=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var YearsSelected = HashValue[i];
				var YearSelectedArray = YearsSelected.split(',');
			}
			if(HashValue[i].indexOf('Category=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var CategorySelected = HashValue[i];
				var CategorySelectedArray = CategorySelected.split(',');
			}
			
			if(HashValue[i].indexOf('Manufacture=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var ManufactureSelected = HashValue[i];
				var ManufactureSelectedArray = ManufactureSelected.split(',');
			}
			
			
		}
		
		if(YearSelectedArray && YearSelectedArray.length) {
			$(".yearSelect").each(function() {
				if(YearSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			});	
		}
		
		if(CategorySelectedArray && CategorySelectedArray.length) {
			$('.categorySelect').each(function() {
				if(CategorySelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}
			});	
		}
		
		if(ManufactureSelectedArray && ManufactureSelectedArray.length) {
			$('.manufactureSelect').each(function() {
				if(ManufactureSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}
			});	
		}
		
		
		//console.log(HashValue);
		console.log(YearSelectedArray);
		console.log(CategorySelectedArray);
		console.log(ManufactureSelectedArray);
		
		
	}
	
	function RemoveLinkedInventory(element) {
		
		
		
		var InventoryIDS = '';
		
		var inventory = $(element).parent().parent().find('');
			
		$($(element).parent().parent().find('.linkedInventoryItem').not($(element).parent())).each(function() {
			InventoryIDS += $(this).attr('id') + ','
		});
					
		$(element).parent().parent().parent().parent().parent().parent().find('input[name="linkedInventoryIDS[]"]').val(InventoryIDS.substring(0, InventoryIDS.length - 1));
			
		$(element).parent().remove();	
		console.log(InventoryIDS.substring(0, InventoryIDS.length - 1));		
		
		
		
	}
	
	function SelectInventory(element, incentiveID) {
		
		var IncentiveValue = $(element).parent().parent().parent().parent().parent().parent().parent().find('.specialTypeValue').val();
		var IncentiveType = $(element).parent().parent().parent().parent().parent().parent().parent().find('.specialType').val();
		var IncentiveID = $(element).parent().parent().parent().parent().parent().parent().parent().find('.relatedSpecialItemID').val();
		
		//
		
		
		var IncentiveHeaderText = '';
		
		if(IncentiveType == "Percentage Off") {
			IncentiveHeaderText = "Apply Inventory That Applies to <strong>" + IncentiveValue + "% Off</strong>";
		} else if(IncentiveType == "Dollar Off") {
			IncentiveHeaderText = "Apply Inventory That Applies to <strong>$" + IncentiveValue + " Off</strong>";
		} else if(IncentiveType == "Special Financing") {
			IncentiveHeaderText = "Apply Inventory That Applies to <strong>" + IncentiveValue + "% APR</strong>";
		}
		
		$("#AddInventoryToIncentive").attr('action', Globals.WebsitePath() + 'specials/save/inventorytoincentive/' + IncentiveID);
		
		var InventoryIDS = [];
		
		$.getJSON(Globals.WebsitePath() +  "specials/GetLinkedInventoryByIncentive/" + incentiveID, function( data ) {
			$.each( data, function( key, val ) {
				InventoryIDS.push(val.RelatedInventoryID)
				console.log(val.RelatedInventoryID);
			});
			
			console.log(InventoryIDS);
			$('.inventoryID').each(function() {
				if(InventoryIDS.contains($(this).val())) {
					$(this).attr('checked', 'checked');
					console.log('hi');
				}
			});
			
		});
		
		
		
		
		
		$("#IncentiveHeader").html(IncentiveHeaderText);
		
		
		
		
		$("#SelectInventoryWindow").fadeIn();
	}
	
	function CloseInventoryWindow() {
		$("#SelectInventoryWindow").fadeOut();
	}
	
	function CheckAllInventory(element) {
		 var checkedStatus = element.checked;
		 $('.inventoryID').each(function () {
		 	$(this).prop('checked', checkedStatus);
		 });
	}
	
	function getEditHistory(specialID) {
		$("#SpecialEditHistory").show();
		var editHistory = [];
		$.getJSON(Globals.WebsitePath() +  "specials/GetEditHistory/" + specialID, function( data ) {
			$.each( data, function( key, val ) {
				editHistory.push("<div>" + val.value +"</div>" );
			});
			
			$(editHistory.join("")).appendTo("#SpecialEditHistory .FormContent"); 
		});
	}
	
	function closeEditHistory() {
		$("#SpecialEditHistory").hide();
		$("#SpecialEditHistory .FormContent").html('');
	}
	
	function RemoveNewIncentive(element) {
		$(element).parent().remove();
	}
	
	
	return {
		InitializeSpecialForm: InitializeSpecialForm,
		InitializeVehicleBrandPage: InitializeVehicleBrandPage,
		ToggleVehiclesList: ToggleVehiclesList,
		AddCar: AddCar,
		InitializeVehicleForm: InitializeVehicleForm,
		InitializePartsBrandForm: InitializePartsBrandForm,
		InitializeVehicleSingle: InitializeVehicleSingle,
		InitializePartsSingle: InitializePartsSingle,
		InitializeIncentiveSingleForm: InitializeIncentiveSingleForm,
		AddItem: AddItem,
		LinkInventoryToIncentive: LinkInventoryToIncentive,
		FilterInventory: FilterInventory,
		OpenUpFilterContent: OpenUpFilterContent,
		FilterCondition: FilterCondition,
		FilterYear: FilterYear,
		FilterCategory: FilterCategory,
		FilterManufacture: FilterManufacture,
		ShowLinkedInventory: ShowLinkedInventory,
		HideLinkedInventory: HideLinkedInventory,
		ToggleFilters: ToggleFilters,
		RemoveLinkedInventory: RemoveLinkedInventory,
		RefreshParent: RefreshParent,
		SelectInventory: SelectInventory,
		CloseInventoryWindow: CloseInventoryWindow,
		FilterModel: FilterModel,
		CheckAllInventory: CheckAllInventory,
		getEditHistory: getEditHistory,
		closeEditHistory: closeEditHistory,
		RemoveNewIncentive: RemoveNewIncentive
	}
}();
