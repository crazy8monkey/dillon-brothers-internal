var EmailLogController = function() {
	
	function SummaryInventoryLeads() {
		$("#InventoryLeads").show();
		$("#WebsiteSource").hide();
		$("#TypeOfEmailSection").hide();
		/*$.getJSON(Globals.WebsitePath() + 'emaillogs/summary/inventory/' + year, function( data ) {
			$(".pieChartHeader").html('Inventory Leads');
			$("#Count").html(parseInt(+data.SendToFriend + +data.OnlineOffer + +data.TradeInValue + +data.ScheduleTestRide + +data.ContactUs));
			
		 	var pieChart = document.getElementById("chart-area").getContext('2d');
			var myChart = new Chart(pieChart, {
				type: 'pie',
				data: {
					datasets: [{
						data: [
							data.SendToFriend, 
							data.OnlineOffer, 
							data.TradeInValue, 
							data.ScheduleTestRide, 
							data.ContactUs
						],
						backgroundColor: [
							'rgb(54, 162, 235)', 
							'rgb(255, 99, 132)', 
							'rgb(255, 159, 64)', 
							'rgb(255, 205, 86)', 
							'rgb(75, 192, 192)'
						],
						label: 'Dataset 1'
					}],
					labels: ["Send To A Friend", "Online Offer", "Trade In value", "Schedule Test Ride", "Contact Us"]	
				},
				options: {
					responsive: true,
				}
			});
		}); */
		
	}
	
	function SummaryWebsiteSources() {
		$("#InventoryLeads").hide();
		$("#TypeOfEmailSection").hide();
		$("#WebsiteSource").show();
		//$.getJSON(Globals.WebsitePath() + 'emaillogs/summary/sources/' + year, function( data ) {
		//	$(".pieChartHeader").html('Website Source');
		//	$("#Count").html(parseInt(+data.InventoryLeads + +data.ServiceDept));
			
		// 	var pieChart = document.getElementById("chart-area").getContext('2d');
		//	var myChart = new Chart(pieChart, {
		//		type: 'pie',
		//		data: {
		//			datasets: [{
		//				data: [
		//					data.InventoryLeads,
		//					data.ServiceDept
		//				],
		//				backgroundColor: [
		//					'rgb(54, 162, 235)',
		//					'rgb(255, 99, 132)'
		//				],
		//				label: 'Dataset 1'
		//			}],
		//			labels: ["Inventory Leads", "Service Department"]	
		//		},
		//		options: {
		//			responsive: true,
		//		}
		//	});
		//});
	}
	
	function SummaryTypeOfEmails() {
		$("#InventoryLeads").hide();
		$("#WebsiteSource").hide();
		$("#TypeOfEmailSection").show();
	}
	
	function InitializeSummaryPage(year) {
		EmailLogController.SummaryInventoryLeads(year);
	}
	
	return {
		InitializeSummaryPage: InitializeSummaryPage,
		SummaryInventoryLeads: SummaryInventoryLeads,
		SummaryWebsiteSources: SummaryWebsiteSources,
		SummaryTypeOfEmails: SummaryTypeOfEmails 
	}
}();
