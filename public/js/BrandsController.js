var BrandsController = function() {
	
	//public
	function InitializeLandingPageSingle() {
		CKEDITOR.replace( 'brandLandingPageContent', {	
			filebrowserUploadUrl: Globals.WebsitePath() + "brands/uploadImages",
			filebrowserImageUploadUrl: Globals.WebsitePath() + "brands/uploadImages",
		});
		
		$("#BrandLadingPageContent").submit(function(e){
			e.preventDefault();
			
			for ( instance in CKEDITOR.instances ) {
				CKEDITOR.instances[instance].updateElement();	
			}
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, Globals.MultipleErrorResponses)	
		});
	}
	
	function InitializeBrandSingle() {
		$("#SinlgeBrandForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var formData = new FormData($(this)[0]);
			

			Globals.AjaxFileUpload(formURL, formData,
				function(responses) {
					if(responses.ValidationErrors) {
						var validationMessagesArray = responses.ValidationErrors;
						
						var ValidationErrors = responses.ValidationErrors.length;
						for (var i = 0; i < ValidationErrors; i++) {
						   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
						   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
						   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
						   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
						}
						
						alert("Please fix the errors on the screen");
						$("#ToastMessage").hide()
					}
					if(responses.redirect) {
						$("#SavingText").html("Saving...")
						if(responses.redirect.NewCategoryValue) {
							if(responses.redirect.NewCategoryValue == 3) {
								location.reload();
							} else {
								Globals.redirect(responses.redirect.PATH);	
							}	
						} else {
							Globals.redirect(responses.redirect.PATH);	
						}
						
						 
			 			 
			 		}
				}
			);		
		});
	}
		
	return {
		InitializeLandingPageSingle: InitializeLandingPageSingle,
		InitializeBrandSingle: InitializeBrandSingle
	}
}();
