grapesjs.plugins.add('album-photo-select', function(editor, options) {
	var modal = editor.Modal;
	
	/****************** BLOCKS *************************/
	var bm = editor.BlockManager;
	bm.add('link-block', {
	    label: 'Add Album/Image',
	    attributes: {class:'gjs-fonts gjs-f-image gjs-block'},
	    category: 'Basic',
	    command: 'addAlbumView',
	    content: {
	      type:'image',
	      editable: false,
	      droppable: true,
	      style:{
	        display: 'inline-block',
	        padding: '5px',
	        'min-height': '50px',
	        'min-width': '50px'
	      }
	    },
	});
	
	/****************** COMMANDS *************************/
	var cmdm = editor.Commands;
	 cmdm.add('addAlbumView', {
	    run: function(editor, sender) {
	      alert('hi');
	    }
	});
	

})
