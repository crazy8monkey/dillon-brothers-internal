<?php

class BackupVinsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByBackedupID($id) {
		return $this -> db -> select('SELECT * FROM vinnumberbackup WHERE inventoryBackupBikeID = ' . $id);
	}
	
	public function AllVinNumbers() {
		return $this -> db -> select('SELECT * FROM vinnumberbackup');
	}
	
	public function OrphanedVinNumbers() {
		return $this -> db -> select('SELECT * FROM vinnumberbackup WHERE inventoryBackupBikeID = 0');
	}

}