<?php

class InventoryCategoryList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllCategories() {
		return $this -> db -> select('SELECT DISTINCT ParentCategory, GROUP_CONCAT(inventoryCategoryName) SubCategories, GROUP_CONCAT(inventoryCategoryID) SubCategoryIDS FROM inventorycategories GROUP BY ParentCategory');		
	}
	
	public function AllCategoriesNoGrouped() {
		return $this -> db -> select('SELECT * FROM inventorycategories');
	}

}