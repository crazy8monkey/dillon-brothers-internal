<?php

class BrandContentList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByOEMBrand($brandID) {
		return $this -> db -> select('SELECT * FROM brandpostcontent WHERE IsOEMPost = 1 AND OEMBrandID = '. $brandID);
	}
	
	public function ByPartApparelBrand($brandID) {
		return $this -> db -> select('SELECT * FROM brandpostcontent WHERE IsOEMPost = 0 AND PartBrandID = '. $brandID);
	}
	
	

}