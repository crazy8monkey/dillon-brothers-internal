<?php

class EditHistoryLogList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function LogsByBlog($id) {
		return $this -> db -> select('SELECT * FROM edithistorylog LEFT JOIN users ON edithistorylog.edittedByUserID = users.userID WHERE editHistoryType = 1 AND relatedItemID = ' . $id . '  ORDER BY edittedTimeAndDate ASC');
	}
	
	public function LogsBySpecial($id) {
		return $this -> db -> select('SELECT * FROM edithistorylog LEFT JOIN users ON edithistorylog.edittedByUserID = users.userID WHERE editHistoryType = 2 AND relatedItemID = ' . $id . '  ORDER BY edittedTimeAndDate ASC');
	}
	
	public function LogsByEvent($id) {
		return $this -> db -> select('SELECT * FROM edithistorylog LEFT JOIN users ON edithistorylog.edittedByUserID = users.userID WHERE editHistoryType = 3 AND relatedItemID = ' . $id . '  ORDER BY edittedTimeAndDate ASC');
	}
	
	public function LogsByBackupItem($id) {
		return $this -> db -> select('SELECT * FROM edithistorylog LEFT JOIN users ON edithistorylog.edittedByUserID = users.userID WHERE editHistoryType = 4 AND relatedItemID = ' . $id . '  ORDER BY edittedTimeAndDate ASC');
	}
	
	public function LogsByEcommerceProductItem($id) {
		return $this -> db -> select('SELECT * FROM edithistorylog LEFT JOIN users ON edithistorylog.edittedByUserID = users.userID WHERE editHistoryType = 5 AND relatedItemID = ' . $id . '  ORDER BY edittedTimeAndDate ASC');
	}

	public function LogsByEcommerceOrderItem($id) {
		return $this -> db -> select('SELECT * FROM edithistorylog LEFT JOIN users ON edithistorylog.edittedByUserID = users.userID WHERE editHistoryType = 6 AND relatedItemID = ' . $id . '  ORDER BY edittedTimeAndDate ASC');
	}

}