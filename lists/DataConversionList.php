<?php

class DataConversionList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllConversion() {
		return $this -> db -> select('SELECT * FROM dataconversions LEFT JOIN speclabels ON dataconversions.AssociatedItemID = speclabels.specLabelID');
	}
	
	public function ColorConversions() {
		return $this -> db -> select('SELECT * FROM dataconversions WHERE Type = 2');
	}
}