<?php

class BlogPostList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllBlogPlosts() {
		return $this -> db -> select('SELECT * FROM blogposts ORDER BY blogposts.publishedDateFull DESC');
	}
	
	public function BlogsByCategory($id) {
		return $this -> db -> select('SELECT * FROM blogcatories LEFT JOIN blogposts ON blogcatories.RelatedBlogPostID = blogposts.blogPostID WHERE blogcatories.RelatedBlogPostID = '. $id);
	}

}