<?php

class UserList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function All() {
		return $this -> db -> select('SELECT * FROM users');
	}
	
	public function AllUsers($id) {
		$list = $this -> db -> prepare('SELECT * FROM users WHERE userID <> :userID');
		$list -> execute(array(':userID' => $id));
		return $list -> fetchAll();
	}
	
	public function AllUsersStoreRelation() {
		$list =  $this -> db -> prepare('SELECT users.userID, 
											    users.UserEmail, 
										        GROUP_CONCAT(StoresProgress.StoreName) AssociatedStoreNames, 
										        GROUP_CONCAT(StoresProgress.TotalSubmittedContent) StoresProgress,
										        GROUP_CONCAT(StoresProgress.FacebookCount) SubmittedFacebookPosts,
										        GROUP_CONCAT(StoresProgress.TwitterCount) SubmittedTwitterPosts,
										        StoresProgress.RequiredFacebook,
										        StoresProgress.RequiredTwitter
										        FROM users 
													 LEFT JOIN userassociatedstore ON users.userID = userassociatedstore.UserID 
													   LEFT JOIN (
													   				SELECT storeID, 
													   					   StoreName, 
																		   ( 
																		   		(
																					(SELECT LEAST(COUNT(*),3) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID) + 
																					(SELECT LEAST(COUNT(*),3) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID)
																				) / 
																				(SELECT (socialMediaFacebook + socialMediaTwitter) TotalEntries FROM settings) 
																		   ) TotalSubmittedContent,
																		   (SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID) FacebookCount,
																		   (SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID) TwitterCount,
																		   (SELECT socialMediaFacebook FROM settings) RequiredFacebook,
																		   (SELECT socialMediaTwitter FROM settings) RequiredTwitter FROM stores
																) StoresProgress ON userassociatedstore.associatedStore = StoresProgress.storeID GROUP BY users.userID');


		$list -> execute(array(':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
							   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
		
		return 	$list -> fetchAll();
		
		
		//SELECT storeID, StoreName, 
		//							( 
		//						        ((SELECT LEAST(COUNT(*),3) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN ("2017-01-02") AND ("2017-01-07") AND StoreID = stores.storeID) + (SELECT LEAST(COUNT(*),3) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN ("2017-01-02") AND ("2017-01-07") AND StoreID = stores.storeID)) / (SELECT (socialMediaFacebook + socialMediaTwitter) TotalEntries FROM settings) 
		//						    ) TotalSubmittedContent,
		//						    (SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN ("2017-01-02") AND ("2017-01-07") AND StoreID = stores.storeID) FacebookCount,
		//						    (SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN ("2017-01-02") AND ("2017-01-07") AND StoreID = stores.storeID) TwitterCount,
		//						    (SELECT socialMediaFacebook FROM settings) RequiredFacebook,
		//						    (SELECT socialMediaTwitter FROM settings) RequiredTwitter
		//						        FROM stores
		
	}
	

}