<?php

class ChatConversationsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function All() {
		return $this -> db -> select('SELECT * FROM chatconversations LEFT JOIN users ON chatconversations.WorkerName = users.userID');
	}

}