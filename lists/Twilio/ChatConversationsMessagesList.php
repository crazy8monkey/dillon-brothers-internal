<?php

class ChatConversationsMessagesList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByChatConversation($id) {
		return $this -> db -> select('SELECT * FROM chatconversationmessages WHERE relatedConversationID = ' .$id . ' ORDER BY dateentered ASC');
	}

}