<?php

class EventsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllEvents() {
		return $this -> db -> select('SELECT * FROM events LEFT JOIN (SELECT SuperEventID, GROUP_CONCAT(SubEventID) SubEventIDS, GROUP_CONCAT(events.eventName) SubEventNames, GROUP_CONCAT(events.startDate) SubEventStartDates FROM subevents LEFT JOIN events ON subevents.SubEventID = events.eventID GROUP BY SuperEventID) SubEventData ON events.eventID = SubEventData.SuperEventID WHERE SubEvent = 0');
	}
		
	public function SuperEvents() {
		return $this -> db -> select('SELECT * FROM events WHERE SuperEvent = 1');
	}
	
	public function SubEvents() {
		return $this -> db -> select('SELECT * FROM subevents LEFT JOIN events ON subevents.SubEventID = events.eventID');
	}
	
	public function StandaloneEvents() {
		return $this -> db -> select('SELECT * FROM events WHERE SubEvent = 0 and SuperEvent = 0');
	}
	
	public function NoSubEventsList() {
		return $this -> db -> select('SELECT (eventID) UniqueIdentifier, (eventName) EventText 
										FROM (
									        SELECT eventID, eventName FROM events WHERE SubEvent = 0 AND SuperEvent = 1 UNION SELECT DISTINCT LinkedEventNameText, EventNameText FROM eventphotoalbumlink
									    ) 
									    LinkedEventTable');
	}

}