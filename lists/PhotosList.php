<?php

class PhotosList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AlbumsByYear($year) {
		return $this -> db -> select('SELECT *, (SELECT COUNT(*) FROM photos WHERE PhotoAlbums.albumID = photos.photoAlbumID) AlbumCount FROM PhotoAlbums WHERE ParentDirectory = ' . $year);
	}
	
	public function PhotosByAlbum($id) {
		return $this -> db -> select('SELECT * FROM photos LEFT JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE photoAlbumID = ' . $id);
	}
	
	public function AllPhotos() {
		return $this -> db -> select('SELECT * FROM photos LEFT JOIN photoalbums On photos.photoAlbumID = photoalbums.albumID');
	}
	
	public function EventLoadPhotos($id) {
		$photosLoad = '';
		foreach($this -> db -> select('SELECT * FROM photos LEFT JOIN photoalbums On photos.photoAlbumID = photoalbums.albumID WHERE photoalbums.albumEventID = ' . $id) as $photoSingle) {
			$photosLoad .= '"'. PHOTO_URL . $photoSingle['ParentDirectory'] . "/events/" . $photoSingle['albumFolderName'] . '/' . $photoSingle['photoName'] . '-l.' . $photoSingle['ext'] . '",';	
		}

		return rtrim($photosLoad, ',');
	}

}