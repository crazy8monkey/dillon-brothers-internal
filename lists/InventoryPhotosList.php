<?php

class InventoryPhotosList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByID($id) {
		return $this -> db -> select('SELECT * FROM inventoryphotos WHERE relatedInventoryID = ' . $id);
	}
		
	public function ReadOnly($id) {
		return $this -> db -> select('SELECT * FROM inventoryphotos WHERE relatedInventoryID = ' . $id . ' ORDER BY MainProductPhoto DESC');
	}

}