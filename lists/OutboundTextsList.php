<?php

class OutboundTextsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByCustomer($id) {
		return $this -> db -> select('SELECT * FROM outboundtextmessages 
												LEFT JOIN users ON outboundtextmessages.textsentbyUserID = users.userID 
												LEFT JOIN settingsmsnumbers ON outboundtextmessages.twilioNumberUsed = settingsmsnumbers.smsdeptID WHERE relatedCustomerID = ' . $id . ' ORDER BY textmessagesent ASC');
	}

}