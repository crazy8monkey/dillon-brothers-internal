<?php

class YearPhotosList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function Years() {
		return $this -> db -> select('SELECT * FROM yearphotos ORDER BY YearText DESC');
	}
		

}