<?php

class RelatedSpecialItemsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
		
	public function ItemsBySpecial($id) {
		return $this -> db -> select('SELECT * FROM relatedspecialitems LEFT JOIN inventoryrelatedspecials ON relatedspecialitems.RelatedSpecialItemID = inventoryrelatedspecials.RelatedIncentiveID WHERE relatedspecialitems.relatedSpecialID = ' . $id .' GROUP BY relatedspecialitems.RelatedSpecialItemID');
	}
	
	public function LinkedInventoryBySpecial($id) {
		return $this -> db -> select('SELECT * FROM inventoryrelatedspecials LEFT JOIN inventory ON inventoryrelatedspecials.RelatedInventoryID = inventory.inventoryID WHERE inventoryrelatedspecials.RelatedSpecialID  = ' . $id);
	}
	
	public function LinkedInventoryByIncentive($id) {
		return $this -> db -> select('SELECT * FROM inventoryrelatedspecials LEFT JOIN inventory ON inventoryrelatedspecials.RelatedInventoryID = inventory.inventoryID WHERE inventoryrelatedspecials.RelatedIncentiveID = ' . $id);	
	}

}