<?php

class CurrentInventoryList extends BaseObjectList {
	
	private $_sortBySQl;
	private $_sortByPagination;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	private function OrderByString($stringResult) {
		switch($stringResult) {
			case "YearDesc":
				$this -> _sortBySQl = "inventory.Year DESC";
				$this -> _sortByPagination = 'SortBy=YearDesc';
				break;
			case "YearAsc":
				$this -> _sortBySQl = "inventory.Year ASC";
				$this -> _sortByPagination = 'SortBy=YearAsc';
				break;
			case "ManufactureAsc":
				$this -> _sortBySQl = "inventory.Manufacturer ASC";
				$this -> _sortByPagination = 'SortBy=ManufactureAsc';
				break;
			case "ManufactureDesc":
				$this -> _sortBySQl = "inventory.Manufacturer Desc";
				$this -> _sortByPagination = 'SortBy=ManufactureDesc';
				break;
			case "PriceDesc":
				$this -> _sortBySQl = "inventory.MSRP Desc";
				$this -> _sortByPagination = 'SortBy=PriceDesc';
				break;
			case "PriceAsc":
				$this -> _sortBySQl = "inventory.MSRP ASC";
				$this -> _sortByPagination = 'SortBy=PriceAsc';
				break;
			default:
				$this -> _sortBySQl = "inventory.VehicleInfoChecked ASC";
				break;
		}	
		
	}
	
	public function MotorSportInventory($pageVariable, $sortBy = NULL) {
		//SELECT * FROM inventory LEFT OUTER JOIN muirelatedcolors ON inventory.inventoryID = muirelatedcolors.relatedMUInventoryID	
			
			
		$page = $pageVariable;
		$this -> userLimit = "42";
		$orderByText = '';
		$SortByQueryString = '';
		$this -> OrderByString($sortBy);

		
		
		if($page) {
			//First Item to display on this page
			$this -> start = ($page - 1) * $this -> userLimit;
		} else {
			//if no page variable is given, set start to 0
			$this -> start = 0;
		}
		
		$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM inventory WHERE InventoryStoreID = 2") -> fetchColumn();	
		
		$currentMotorSportInventory = $this -> db -> prepare('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor  FROM inventory  
																       			LEFT JOIN 
															(
																SELECT relatedMUInventoryID, GROUP_CONCAT(relatedMUIColorID) ColorIDS FROM muirelatedcolors GROUP BY relatedMUInventoryID
															) InventoryColors ON inventory.inventoryID = InventoryColors.relatedMUInventoryID 
															LEFT JOIN colors ON FIND_IN_SET(colors.colorID, InventoryColors.ColorIDS)
																	   
																	    LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1
																        WHERE inventory.InventoryStoreID = 2 GROUP BY inventory.inventoryID ORDER BY ' .$this -> _sortBySQl. ' LIMIT :start, :end');
		
		$currentMotorSportInventory -> bindValue(':start', (int) $this -> start, PDO::PARAM_INT);
		$currentMotorSportInventory -> bindValue(':end', (int) $this -> userLimit, PDO::PARAM_INT);
		
		$currentMotorSportInventory -> execute();
		
		$CurrentInventoryArray = array();
		$CurrentInventoryArray['inventory-list'] = $currentMotorSportInventory -> fetchAll();
		$CurrentInventoryArray['pagination'] = $this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "inventory/currentinventory/motorsports/", $page, $this -> _sortByPagination);
		
		return $CurrentInventoryArray;
	}
	
	public function HarleyInventory($pageVariable, $sortBy = NULL) {
		$page = $pageVariable;
		$this -> userLimit = "42";	
		$orderByText = '';
		$SortByQueryString = '';
		$this -> OrderByString($sortBy);
		
		
		if($page) {
			//First Item to display on this page
			$this -> start = ($page - 1) * $this -> userLimit;
		} else {
			//if no page variable is given, set start to 0
			$this -> start = 0;
		}
		
		$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM inventory WHERE InventoryStoreID = 3") -> fetchColumn();	
		
		$currentHarleyInventory = $this -> db -> prepare('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor FROM inventory 
																		LEFT JOIN 
															(
																SELECT relatedMUInventoryID, GROUP_CONCAT(relatedMUIColorID) ColorIDS FROM muirelatedcolors GROUP BY relatedMUInventoryID
															) InventoryColors ON inventory.inventoryID = InventoryColors.relatedMUInventoryID 
															LEFT JOIN colors ON FIND_IN_SET(colors.colorID, InventoryColors.ColorIDS) 
																        LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1
																        WHERE inventory.InventoryStoreID = 3 GROUP BY inventory.inventoryID ORDER BY ' .$this -> _sortBySQl. ' LIMIT :start, :end');
		
		$currentHarleyInventory -> bindValue(':start', (int) $this -> start, PDO::PARAM_INT);
		$currentHarleyInventory -> bindValue(':end', (int) $this -> userLimit, PDO::PARAM_INT);
		
		$currentHarleyInventory -> execute();
		
		$CurrentInventoryArray = array();
		$CurrentInventoryArray['inventory-list'] = $currentHarleyInventory -> fetchAll();
		$CurrentInventoryArray['pagination'] = $this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "inventory/currentinventory/harleydavidson/", $page, $this -> _sortByPagination);
		
		return $CurrentInventoryArray;
	}
	
	public function IndianInventory($pageVariable, $sortBy = NULL) {
		$page = $pageVariable;
		$this -> userLimit = "42";	
		
		$SortByQueryString = '';
		$this -> OrderByString($sortBy);
		
		if($page) {
			//First Item to display on this page
			$this -> start = ($page - 1) * $this -> userLimit;
		} else {
			//if no page variable is given, set start to 0
			$this -> start = 0;
		}
		
		$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM inventory WHERE InventoryStoreID = 4") -> fetchColumn();			
		
		$currentIndianInventory = $this -> db -> prepare('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor FROM inventory 
																					LEFT JOIN 
															(
																SELECT relatedMUInventoryID, GROUP_CONCAT(relatedMUIColorID) ColorIDS FROM muirelatedcolors GROUP BY relatedMUInventoryID
															) InventoryColors ON inventory.inventoryID = InventoryColors.relatedMUInventoryID 
															LEFT JOIN colors ON FIND_IN_SET(colors.colorID, InventoryColors.ColorIDS) 
																        LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1
																        WHERE inventory.InventoryStoreID = 4 GROUP BY inventory.inventoryID ORDER BY ' . $this -> _sortBySQl . ' LIMIT :start, :end');
		
		$currentIndianInventory -> bindValue(':start', (int) $this -> start, PDO::PARAM_INT);
		$currentIndianInventory -> bindValue(':end', (int) $this -> userLimit, PDO::PARAM_INT);
		
		$currentIndianInventory -> execute();
		
		$CurrentInventoryArray = array();
		$CurrentInventoryArray['inventory-list'] = $currentIndianInventory -> fetchAll();
		$CurrentInventoryArray['pagination'] = $this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "inventory/currentinventory/indian/", $page, $this -> _sortByPagination);
		
		return $CurrentInventoryArray;	
	}
		
	public function CheckInventoryColor($text) {
		$sth = 	$this -> db -> prepare('SELECT * FROM inventory WHERE Color LIKE :Color');
		$sth -> execute(array(":Color" => '%'. $text . '%'));
		return $sth -> fetchAll();
	}	
	
	public function CheckInventoryManufacture($text) {
		$sth = 	$this -> db -> prepare('SELECT * FROM inventory WHERE Manufacturer = :Manufacturer AND VehicleInfoChecked = 1');
		$sth -> execute(array(":Manufacturer" => $text));
		return $sth -> fetchAll();
	}
	
	public function VehicleInfoFoundInventory() {
		return $this -> db -> select('SELECT * FROM inventory WHERE VehicleInfoChecked = 1');
	}
	
	public function InventoryNeedsPhotos($storeIDS) {
		return $this -> db -> select('SELECT inventory.Conditions, inventory.inventoryID, inventory.VinNumber, inventory.Stock, inventory.Year, inventory.Manufacturer, inventory.ModelName, inventory.FriendlyModelName, inventory.MarkAsSoldDate, stores.StoreName, stores.storeID FROM inventory LEFT JOIN (SELECT relatedInventoryID, COUNT(*) PhotoCount FROM inventoryphotos GROUP BY relatedInventoryID) InventoryPhotos ON inventory.inventoryID = InventoryPhotos.relatedInventoryID LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID WHERE InventoryPhotos.relatedInventoryID IS NULL AND inventory.InventoryStoreID IN (' . $storeIDS . ')');
	}
	
	public function InventoryNeedsPricing($storeIDS) {
		return $this -> db -> select('SELECT inventory.Conditions, inventory.inventoryID, inventory.VinNumber, inventory.Stock, inventory.Year, inventory.Manufacturer, inventory.ModelName, inventory.FriendlyModelName, inventory.MarkAsSoldDate, stores.StoreName, stores.storeID FROM inventory LEFT JOIN (SELECT relatedInventoryID, COUNT(*) PhotoCount FROM inventoryphotos GROUP BY relatedInventoryID) InventoryPhotos ON inventory.inventoryID = InventoryPhotos.relatedInventoryID LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID WHERE inventory.MSRP <= 400 AND inventory.InventoryStoreID IN (' . $storeIDS . ')');
	}
	

	public function ActiveInventoryBySpecial($id) {
		return $this -> db -> select('SELECT * FROM inventory LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 WHERE inventory.IsInventoryActive = 1 AND inventory.InventoryStoreID IN (' . $id . ') ORDER BY inventory.Year DESC');
	}

	private $yearSelected;

	public function FilterOption($id, $postData = array(), $type) {
		
		$returnYears = false;
		$returnCategories = false;
		$returnManufactures = false;
		$returnModels = false;
		
		$filters = array();
		
		$sqlWHERE = NULL;
		$sqlWHEREYears = NULL;
		$sqlWHEREManufactures = NULL;
		$sqlWHERECategories = NULL;
		$sqlWHEREModels = NULL;
		
		if(isset($postData['condition'])) {
			$conditions = NULL;
			foreach($postData['condition'] as $conditionSingle) {
				$conditions .= $conditionSingle . ',';	
			}
			$conditionQuery = rtrim($conditions, ',');
		}
				
		if(isset($postData['Years'])) {
			$years = NULL;
			foreach($postData['Years'] as $year) {
				$years .=  '"'. $year . '",';	
			}
			$yearQuery = rtrim($years, ',');			
		} 

		if(isset($postData['Categories'])) {
			$categoryQuery = NULL;
			foreach($postData['Categories'] as $categorySingle) {
				$categoryQuery .= $categorySingle . ',';	
			}
			$categoryQuery = rtrim($categoryQuery, ',');			
		}

		if(isset($postData['Manufactures'])) {
			$manufactures = NULL;
			foreach($postData['Manufactures'] as $manufactureSingle) {
				$manufactures .=  $manufactureSingle . '|';						
			}
			$manufactureQuery = rtrim($manufactures, '|');			
		}
		
		if(isset($postData['Models'])) {
			$models = NULL;
			foreach($postData['Models'] as $modelSingle) {
				$models .=  $modelSingle . '|';						
			}
			$modelQuery = rtrim($models, '|');			
		}
		
		
		
		//$sqlWHERE .= " AND inventory.Conditions IN (" . $conditionQuery . ")";
		//$sqlWHERE .= " AND inventory.Year IN (" . $yearQuery . ")";
		//$sqlWHERE .= " AND inventory.Category IN (" . $categoryQuery . ")";
		//$sqlWHERE .= " AND inventory.Manufacturer IN ('" . $manufactureQuery . "')";	
		
		
		//$sqlWHEREYears .= " AND inventory.Category IN (" . $categoryQuery . ")";
		//$sqlWHEREYears .= " AND inventory.Manufacturer IN ('" . $manufactureQuery . "')";			
		
		
		if(isset($conditionQuery)) {
			$sqlWHEREYears .= " AND inventory.Conditions IN (" . $conditionQuery . ")";
			$sqlWHEREManufactures .= " AND inventory.Conditions IN (" . $conditionQuery . ")";
			$sqlWHERECategories .= " AND inventory.Conditions IN (" . $conditionQuery . ")";
			$sqlWHEREModels .= " AND inventory.Conditions IN (" . $conditionQuery . ")";
		}
		
		if(isset($categoryQuery)) {
			$sqlWHEREYears .= " AND inventory.Category IN (" . $categoryQuery . ")";
			$sqlWHEREManufactures .= " AND inventory.Category IN (" . $categoryQuery . ")";
			$sqlWHEREModels .= " AND inventory.Category IN (" . $categoryQuery . ")";
		}
		
		if(isset($manufactureQuery)) {
			$sqlWHEREYears .= " AND inventory.Manufacturer REGEXP '" . $manufactureQuery . "'";
			$sqlWHERECategories .= " AND inventory.Manufacturer REGEXP '" . $manufactureQuery . "'";
			$sqlWHEREModels .= " AND inventory.Manufacturer REGEXP '" . $manufactureQuery . "'";
		}
		
		if(isset($yearQuery)) {
			$sqlWHEREManufactures .= " AND inventory.Year IN (" . $yearQuery . ")";
			$sqlWHERECategories .= " AND inventory.Year IN (" . $yearQuery . ")";
			$sqlWHEREModels .= " AND inventory.Year IN (" . $yearQuery . ")";
		}
		
		if(isset($modelQuery)) {
			$sqlWHEREYears .= " AND inventory.ModelName REGEXP '" . $modelQuery . "'";
			$sqlWHEREManufactures  .= " AND inventory.ModelName REGEXP '" . $modelQuery . "'";
			$sqlWHERECategories  .= " AND inventory.ModelName REGEXP '" . $modelQuery . "'";
		}
		
		
		
		
		$yearFiltered = $this -> db -> select('SELECT * FROM inventory LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID WHERE inventory.IsInventoryActive = 1 ' . $sqlWHEREYears . ' AND inventory.InventoryStoreID IN (' . $id . ') ORDER BY Year DESC');
		
		$manufactureFiltered = $this -> db -> select('SELECT * FROM inventory LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID WHERE inventory.IsInventoryActive = 1 ' . $sqlWHEREManufactures . ' AND inventory.InventoryStoreID IN (' . $id . ') ORDER BY Year DESC');
		
		$categoryFiltered = $this -> db -> select('SELECT * FROM inventory LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID WHERE inventory.IsInventoryActive = 1 ' . $sqlWHERECategories . ' AND inventory.InventoryStoreID IN (' . $id . ') ORDER BY Year DESC');
		
		$modelFiltered = $this -> db -> select('SELECT * FROM inventory LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID WHERE inventory.IsInventoryActive = 1 ' . $sqlWHEREModels . ' AND inventory.InventoryStoreID IN (' . $id . ') ORDER BY Year DESC');
		
		
		$Years = array_column($yearFiltered, 'Year');	
		
		$yearFilterHtml = NULL;
		foreach(array_unique($Years, SORT_REGULAR) as $newFilterYear) {
				
			$checkedString = NULL;	
			if(isset($postData['Years'])) {
				if(in_array($newFilterYear, $postData['Years'])) {
					$checkedString = ' checked ';	
				}	
			}
			
			$yearFilterHtml .='<div><input type="checkbox" name="Year[]" onclick="SpecialsController.FilterYear()" class="yearSelect" '.$checkedString.'value="' . $newFilterYear . '" style="float:left; margin-right: 2px;" />' . $newFilterYear . '</div>';	
		}
		
		$filters['Years'] = $yearFilterHtml;	
		
		$categories = array_column($categoryFiltered, 'inventoryCategoryName');
		$categoryIDs = array_column($categoryFiltered, 'inventoryCategoryID');
		
		
		$categoryFilterHtml = NULL;
		$categoryID =array_unique($categoryIDs, SORT_REGULAR);
		
		foreach(array_unique($categories, SORT_REGULAR) as $key => $newCategoryFilter) {
			$checkedCategoryString = NULL;
			
			if(isset($postData['Categories'])) {
				if(in_array($categoryID[$key], $postData['Categories'])) {
					$checkedCategoryString = ' checked ';	
				}	
			}
			
			$categoryFilterHtml .='<div><input type="checkbox" name="category[]" class="categorySelect" onclick="SpecialsController.FilterCategory()" class="yearSelect" '.$checkedCategoryString.'value="' . $categoryID[$key] . '" style="float:left; margin-right: 2px;" />' . $newCategoryFilter . '</div>';
		}
		
		//$filters['CategoryIDS'] = ;
		$filters['Categories'] = $categoryFilterHtml;
		
		$Manufacture = array_column($manufactureFiltered, 'Manufacturer');
		
		
		$manufactureFilterHtml = NULL;
		
		foreach(array_unique($Manufacture, SORT_REGULAR) as $manufactureSingle) {
			$checkedManufactureString = NULL;
			
			if(isset($postData['Manufactures'])) {
				if(in_array($manufactureSingle, $postData['Manufactures'])) {
					$checkedManufactureString = ' checked ';	
				}	
			}
			
			$manufactureFilterHtml .='<div><input type="checkbox" name="manufacture[]" class="manufactureSelect" onclick="SpecialsController.FilterManufacture()" '.$checkedManufactureString.'value="' . $manufactureSingle . '" style="float:left; margin-right: 2px;" value="" />' . $manufactureSingle . '</div>';
		}
		
		$filters['Manufacture'] = $manufactureFilterHtml;	
		
		$Model = array_column($modelFiltered, 'ModelName');
		
		$modelFilterHtml = NULL;
		foreach(array_unique($Model, SORT_REGULAR) as $modelSingle) {
			
			
			$modelFilterHtml .= '<div><input type="checkbox" name="model[]" class="modelSelect" onclick="SpecialsController.FilterModel()" value="'. $modelSingle . '" style="float:left; margin-right: 2px;" />'. $modelSingle . '</div>';
		}	
		
		
		$filters['Model'] = $modelFilterHtml;
		
		
		return $filters;
	}

	
	
	
	public function ActiveInventoryBySpecialFiltered($id, $postData = array()) {
		$results = array();	
		$SqlWHERE = NULL;
		
		$url = "#FilerApplied";
		
		if(isset($postData['condition'])) {
			$conditions = NULL;
			foreach($postData['condition'] as $conditionSingle) {
				$conditions .= $conditionSingle . ',';	
			}
			$conditionQuery = rtrim($conditions, ',');
			
			$SqlWHERE .= " AND inventory.Conditions IN (" . $conditionQuery . ")";			
		}

		if(isset($postData['Years'])) {
			$years = NULL;
			foreach($postData['Years'] as $year) {
				$years .= $year . ',';	
			}
			$yearQuery = rtrim($years, ',');
			$url .= '&Years=' . $yearQuery;
			//echo $yearQuery;
			$SqlWHERE .= " AND inventory.Year IN (" . $yearQuery . ")";				
		}

		if(isset($postData['Categories'])) {
			$categoryQuery = NULL;
			foreach($postData['Categories'] as $categorySingle) {
				$categoryQuery .= $categorySingle . ',';	
			}
			
			$categoryQuery = rtrim($categoryQuery, ',');
			
			$SqlWHERE .= " AND inventory.Category IN (" . $categoryQuery . ")";	
			$url .= '&Category=' . $categoryQuery;			
		}

		if(isset($postData['Manufactures'])) {
			$manufactures = NULL;
			foreach($postData['Manufactures'] as $manufactureSingle) {
				$manufactures .= $manufactureSingle . '|';	
			}
			$manufactureQuery = rtrim($manufactures, '|');
					
			$SqlWHERE .= " AND inventory.Manufacturer REGEXP '" . $manufactureQuery . "'";	
			$url .= '&Manufacture=' . str_replace('"','', $manufactureQuery);			
		}
		


		if(isset($postData['Models'])) {
			$modelQuery = NULL;
			foreach($postData['Models'] as $modelSingle) {
				$modelQuery .= $modelSingle . '|';	
			}
			$modelQuery = rtrim($modelQuery, '|');
					
			$SqlWHERE .= " AND inventory.ModelName REGEXP '" . $modelQuery . "'";	
			$url .= '&Model=' . str_replace('"','', $modelQuery);
		}
		
		
		
		$resultList = $this -> db -> select('SELECT * FROM inventory LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1
														WHERE inventory.IsInventoryActive = 1' . $SqlWHERE. ' AND inventory.InventoryStoreID IN (' . $id . ') ORDER BY inventory.Year DESC');
		
		$filtereHTML = NULL;
		
	
			
		foreach(array_chunk($resultList, 6, true) as $inventorySix)  {
			$filtereHTML .='<div class="row">';		
			
			foreach($inventorySix as $inventorySingle) {
				$filtereHTML .='<div class="col-md-2">';
				$filtereHTML .='<div class="bikeNameElement">';
				$filtereHTML .='<input type="checkbox" name="inventoryID[]" class="inventoryID" value="'. $inventorySingle['inventoryID'] .'" />';
				$filtereHTML .='<div class="image">';
				$filtereHTML .='<img src="' . PHOTO_URL . 'inventory/'. $inventorySingle['VinNumber'] .'/'. $inventorySingle['inventoryPhotoName'] .'-s.' . $inventorySingle['inventoryPhotoExt'] .'" />';
				$filtereHTML .='</div>';
				$filtereHTML .='<div class="text">';
						
				if($inventorySingle['FriendlyModelName']) {
					$filtereHTML .= $bikeName = $inventorySingle['Year'] . ' ' .  $inventorySingle['Manufacturer'] . ' ' . $inventorySingle['FriendlyModelName'] . ' | <strong>' . $inventorySingle['Stock'] . '</strong>';
				} else {
					$filtereHTML .= $bikeName = $inventorySingle['Year'] . ' ' .  $inventorySingle['Manufacturer'] . ' ' . $inventorySingle['ModelName'] . ' | <strong>' . $inventorySingle['Stock'] . '</strong>';
				}
				$filtereHTML .='</div>';
				$filtereHTML .='</div>';
				$filtereHTML .='</div>';
			}
						
			$filtereHTML .='</div>';	
		}
		
		
		$results['inventoryList'] = $filtereHTML;	
		
	
		
	
		$results['AppendUrl'] = $url;
		
		
		
		
		
		
		return $results;
	
	}

	
	
	public function FilteredInventory($storeID, $value) {

		$CurrentInventoryArray = array();
		$CurrentInventoryArray['inventory-list'] = $this -> db -> select("SELECT * FROM (
																							SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor 
																							FROM inventory 
																													LEFT JOIN 
															(
																SELECT relatedMUInventoryID, GROUP_CONCAT(relatedMUIColorID) ColorIDS FROM muirelatedcolors GROUP BY relatedMUInventoryID
															) InventoryColors ON inventory.inventoryID = InventoryColors.relatedMUInventoryID 
															LEFT JOIN colors ON FIND_IN_SET(colors.colorID, InventoryColors.ColorIDS) 
																							LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 GROUP BY inventory.inventoryID
																						) CurrentInventoryTable WHERE CONCAT_WS(Year, Manufacturer, ModelName, VinNumber, FriendlyModelName, Stock) LIKE '%" . str_replace(" ", "", $value) . "%' AND InventoryStoreID = " . $storeID);
		//
		return $CurrentInventoryArray;
	}

	public function GetCurrentInventory($storeID) {
		return $this -> db -> select('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor FROM inventory 
																		LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
																        LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1
																        WHERE inventory.InventoryStoreID = ' .$storeID. ' GROUP BY inventory.inventoryID');
	}

	
	public function DillonBrothersMainLiveInventory() {
		return $this -> db -> select('SELECT * FROM inventory 
														LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
														INNER JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1
														INNER JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
														INNER JOIN stores ON inventory.InventoryStoreID = stores.storeID
														WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL');						
	}
	
}