<?php

class ConversionRulesList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ConversionRulesList($id) {
		return $this -> db -> select('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), NULL) SelectedColorText FROM conversionrules LEFT JOIN colors ON FIND_IN_SET(colors.colorID, conversionrules.NewColorText) WHERE relatedDataConversionID = ' . $id . ' GROUP BY conversionrules.ruleID');		
	}
	
	public function SpecLabelRules() {
		return $this -> db -> select('SELECT * FROM conversionrules LEFT JOIN dataconversions ON conversionrules.relatedDataConversionID = dataconversions.dataConversionID LEFT JOIN speclabels ON dataconversions.AssociatedItemID = speclabels.specLabelID');
	}
	
	public function ColorChangesRules() {
		return $this -> db -> select('SELECT * FROM conversionrules WHERE CurrentColorText IS NOT NULL');
	}
	
	public function ManufactureChangesRules() {
		return $this -> db -> select('SELECT * FROM conversionrules WHERE CurrentManufactureText IS NOT NULL');
	}
	
	
	public function SpecLabelRulesList() {
		$limit = $this->db->prepare("SET @@group_concat_max_len = 100000;");
		$limit -> execute();
		
		
		return $this -> db -> select('SELECT specGroupText, GROUP_CONCAT(labelText) RelatedSpecLabels, GROUP_CONCAT(dataConversionID) RelatedDataConversionIDs FROM dataconversions LEFT JOIN speclabels ON dataconversions.AssociatedItemID = speclabels.specLabelID LEFT JOIN specgroups ON speclabels.relatedSpecGroupID = specgroups.specGroupID WHERE dataconversions.Type = 1 GROUP BY specgroups.specGroupID');
	}
}