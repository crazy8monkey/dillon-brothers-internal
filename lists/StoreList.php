<?php

class StoreList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllStores() {
		return $this -> db -> select('SELECT * FROM stores');
	}
	
	public function SocialMediaProgress() {
		$list = $this -> db -> prepare('SELECT storeID, StoreName, 
									( 
								        ((SELECT LEAST(COUNT(*),3) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID) + 
								         (SELECT LEAST(COUNT(*),3) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID)) / 
								         (SELECT (socialMediaFacebook + socialMediaTwitter) TotalEntries FROM settings) 
								    ) TotalSubmittedContent,
								    (SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID) FacebookCount,
								    (SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID) TwitterCount,
								    (SELECT socialMediaFacebook FROM settings) RequiredFacebook,
								    (SELECT socialMediaTwitter FROM settings) RequiredTwitter
								        FROM stores');	
			
		$list -> execute(array(':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
							   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
		
		return 	$list -> fetchAll();
	}
	

}