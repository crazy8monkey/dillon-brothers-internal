<?php

class InventoryBackupHistoryList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function HistoryLog($id) {
		return $this -> db -> select('SELECT * FROM inventorybackuphistory LEFT JOIN users ON inventorybackuphistory.editedByUserID = users.userID WHERE backedupInventoryBackupID = ' . $id);
	}
		

}