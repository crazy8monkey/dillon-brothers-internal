<?php

class SocialMediaList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function SocialMediaListByUser($userID) {
		$list = $this -> db -> prepare('SELECT *, ( 
													((SELECT LEAST(COUNT(*), (SELECT socialMediaFacebook FROM settings)) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = userassociatedstore.associatedStore) + 
													 (SELECT LEAST(COUNT(*), (SELECT socialMediaTwitter FROM settings)) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = userassociatedstore.associatedStore)) / (SELECT (socialMediaFacebook + socialMediaTwitter) WeeklyProgress FROM settings) 
												  ) WeeklyPercentage FROM userassociatedstore INNER JOIN stores ON userassociatedstore.associatedStore = stores.storeID WHERE UserID = :userID');	
		$list -> execute(array(':userID' => $userID,
							   ':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
							   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
		
		return 	$list -> fetchAll();

		//SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 1 LIMIT 3
		
		//SELECT (socialMediaFacebook + socialMediaTwitter) WeeklyProgress FROM settings
		
		//$this -> view -> StartOfWeek = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
		//$this -> view -> EndOfWeek = date('Y-m-d',time()+( 6 - date('w'))*24*3600);
		
	}

	public function SocialMediaListsAllStores() {
		$list = $this -> db -> prepare('SELECT storeID, StoreName, ( ((SELECT LEAST(COUNT(*), (SELECT socialMediaFacebook FROM settings)) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID) + (SELECT LEAST(COUNT(*), (SELECT socialMediaTwitter FROM settings)) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = stores.storeID)) / (SELECT (socialMediaFacebook + socialMediaTwitter) TotalEntries FROM settings) ) TotalSubmittedContent FROM stores');	
			
		$list -> execute(array(':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
							   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
		
		return 	$list -> fetchAll();
		
		
	}

	public function FacebookPostsByStore($storeID) {
		$list = $this -> db -> prepare('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND StoreID = :storeID');	
		$list -> execute(array(':storeID' => $storeID,
							   ':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
							   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
		
		return 	$list -> fetchAll();
	}
	
	public function TwitterPostByStore($storeID) {
		$list = $this -> db -> prepare('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND StoreID = :storeID');	
		$list -> execute(array(':storeID' => $storeID,
							   ':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
							   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
		
		return 	$list -> fetchAll();
	}
	
	public function NewPosts() {
		return $this -> db -> select('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID LEFT JOIN stores ON socialmediaposts.StoreID = stores.storeID WHERE isPosted = 0 AND isApproved = 0 AND isDeclined = 0 ORDER BY isUrgent DESC');
	}
	
	public function ApprovedPosts() {
		return $this -> db -> select('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID LEFT JOIN stores ON socialmediaposts.StoreID = stores.storeID WHERE isPosted = 0 AND isApproved = 1 ORDER BY isUrgent DESC');
	}
	
	public function DeclinedPosts() {
		return $this -> db -> select('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID LEFT JOIN stores ON socialmediaposts.StoreID = stores.storeID WHERE isPosted = 0 AND isDeclined = 1 ORDER BY isUrgent DESC');
	}
	
	public function PostedPosts() {
		return $this -> db -> select('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID LEFT JOIN stores ON socialmediaposts.StoreID = stores.storeID WHERE isPosted = 1 ORDER BY isUrgent DESC');
	}
	
	public function AllPosts() {
		return $this -> db -> select('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID LEFT JOIN stores ON socialmediaposts.StoreID = stores.storeID ORDER BY submittedDate DESC');
	}
	
	

}