<?php

class FeedBackList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByItem($id, $type) {
		return $this -> db -> select("SELECT * FROM internalfeedback LEFT JOIN users ON internalfeedback.feedbackBy = users.userID WHERE associatedFeedbackID = " .$id . " AND itemType = " . $type . ' ORDER BY feedBackSubmitted DESC');
	}


}