<?php

class SpecList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function SpecSettings() {
		
		$limit = $this->db->prepare("SET @@group_concat_max_len = 200000;");
		$limit -> execute();
		
		return $this -> db -> select('SELECT * FROM specgroups 
											   LEFT JOIN 
											   		(
														SELECT DISTINCT relatedSpecGroupID, 
														GROUP_CONCAT(specLabelID) LabelsByGroup, 
														COALESCE(GROUP_CONCAT(RelatedCatorgies SEPARATOR " / "), GROUP_CONCAT(labelText SEPARATOR " / ")) GrabbedLabelNames,
    													GROUP_CONCAT(labelOrder) LabelOrders FROM 
															( 
																SELECT *, COALESCE(CONCAT(labelText, " - ", GROUP_CONCAT(inventorycategories.inventoryCategoryName) ," - ",GROUP_CONCAT(inventorycategories.inventoryCategoryID)), labelText) RelatedCatorgies
                                                                FROM speclabels 
																		  LEFT JOIN inventorycategories ON FIND_IN_SET(inventorycategories.inventoryCategoryID, speclabels.LinkedInventoryCategories) GROUP BY specLabelID 
															) SpecLabelsByGroupTable GROUP BY SpecLabelsByGroupTable.relatedSpecGroupID
													) RelatedSpecLabelContent ON specgroups.specGroupID = RelatedSpecLabelContent.relatedSpecGroupID ORDER BY specgroups.GroupOrder ASC');
		

	}
	
	public function TotalLabels() {
		return $this -> db -> select("SELECT * FROM speclabels");
	}
	
	public function TotalLabelsOrder() {
		return $this -> db -> select("SELECT * FROM speclabels ORDER BY labelOrder ASC");
	}
		
	public function SpecLabelsByGroup() {
		return $this -> db -> select('SELECT * FROM speclabels LEFT JOIN specgroups ON speclabels.relatedSpecGroupID = specgroups.specGroupID');
	}	

	public function LabelsByGroup($id) {
		return $this -> db -> select('SELECT * FROM speclabels WHERE relatedSpecGroupID = ' . $id . ' ORDER BY labelOrder ASC');
	}

	public function SpecGroups() {
		return $this -> db -> select('SELECT * FROM specgroups WHERE GroupVisible = 1 ORDER BY GroupOrder ASC');
	}

	public function AllSpecGroups() {
		return $this -> db -> select('SELECT * FROM specgroups ORDER BY GroupOrder ASC');
	}

	public function LabelsByCategory($id) {
		return $this -> db -> select('SELECT * FROM speclabels WHERE FIND_IN_SET (' . $id. ', LinkedInventoryCategories) ORDER BY labelOrder ASC');	
	}

}