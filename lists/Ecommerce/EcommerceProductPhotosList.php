<?php

class EcommerceProductPhotosList extends BaseObjectList {
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByProduct($id) {
		return $this -> db -> select('SELECT * FROM ecommercephotos WHERE relatedProductID = ' . $id);
	}

	public function GetProductMainPhoto($id) {
		$mainPhoto = $this -> db -> prepare('SELECT * FROM ecommercephotos WHERE relatedProductID = :productID AND productMainProductPhoto = 1');
		$mainPhoto -> execute(array(":productID" => $id));	
		return $mainPhoto -> fetch();
	}

}