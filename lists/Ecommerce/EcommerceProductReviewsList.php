<?php

class EcommerceProductReviewsList extends BaseObjectList {
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function Inactive() {
		return $this -> db -> select('SELECT * FROM reviews WHERE reviewActive = 0');
	}


}