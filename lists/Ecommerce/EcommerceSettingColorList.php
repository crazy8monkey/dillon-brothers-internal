<?php

class EcommerceSettingColorList extends BaseObjectList {
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function Colors() {
		return $this -> db -> select('SELECT * FROM ecommercesettingcolors');
	}



}