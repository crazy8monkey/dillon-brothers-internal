<?php

class EcommerceProductList extends BaseObjectList {
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function CurrentInventory() {
		return $this -> db -> select('SELECT * FROM ecommerceproducts');
	}
	
	public function CurrentInventorLive() {
		return $this -> db -> select('SELECT * FROM ecommerceproducts WHERE productVisible = 1');
	}
	
	public function ByCategory($id) {
		return $this -> db -> select('SELECT * FROM ecommerceproducts WHERE productCategory = ' . $id);
	}
	
	public function FilteredProducts($content) {
		$sth = 	$this -> db -> prepare('SELECT * FROM ecommerceproducts WHERE productName LIKE :productName');
		$sth -> execute(array(":productName" => '%'. $content . '%'));
		return $sth -> fetchAll();
	}


}