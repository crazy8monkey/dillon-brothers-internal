<?php

class EcommerceSettingCategoriesList extends BaseObjectList {
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function Categories() {
		return $this -> db -> select('SELECT * FROM ecommercesettingcategories');
	}
	



}