<?php

class EcommerceProductSavedSizesList extends BaseObjectList {
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByProduct($id) {
		return $this -> db -> select('SELECT * FROM ecommercesavedsizes WHERE relatedProductID = ' . $id);
	}



}