<?php

class EcommerceSettingSizesList extends BaseObjectList {
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function Sizes() {
		return $this -> db -> select('SELECT * FROM ecommercesettingsizes');
	}

	public function SelectedSizes($ids) {
		return $this -> db -> select('SELECT * FROM ecommercesettingsizes WHERE settingSizeID IN (' . $ids . ')');
	}
	
	public function SizesByCategory($id) {
		$category = EcommerceSettingCategory::WithID($id);
		if(!empty($category -> CurrentSavedSizes)) {
			return $this -> db -> select('SELECT * FROM ecommercesettingsizes WHERE settingSizeID IN (' . $category -> CurrentSavedSizes . ')');	
		}
	}


}