<?php

class EcommerceOrdersList extends BaseObjectList {
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function OrdersList() {
		return $this -> db -> select('SELECT * FROM ecommerceorders ORDER BY DateEntered DESC');
	}
	
	public function YearSummary($year) {
			
		$yearSummary = $this -> db -> prepare('SELECT (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startJanuary AND DateEntered <= :endJanuary AND shippedStatus = 0) JanuaryPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startJanuary AND DateEntered <= :endJanuary AND shippedStatus = 1) JanuaryShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startJanuary AND DateEntered <= :endJanuary), 0) JanuaryTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startFebruary AND DateEntered <= :endFebruary AND shippedStatus = 0) FebruaryPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startFebruary AND DateEntered <= :endFebruary AND shippedStatus = 1) FebruaryShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startFebruary AND DateEntered <= :endFebruary), 0) FebruaryTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startMarch AND DateEntered <= :endMarch AND shippedStatus = 0) MarchPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startMarch AND DateEntered <= :endMarch AND shippedStatus = 1) MarchShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startMarch AND DateEntered <= :endMarch), 0) MarchTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startApril AND DateEntered <= :endApril AND shippedStatus = 0) AprilPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startApril AND DateEntered <= :endApril AND shippedStatus = 1) AprilShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startApril AND DateEntered <= :endApril), 0) AprilTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startMay AND DateEntered <= :endMay AND shippedStatus = 0) MayPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startMay AND DateEntered <= :endMay AND shippedStatus = 1) MayShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startMay AND DateEntered <= :endMay), 0) MayTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startJune AND DateEntered <= :endJune AND shippedStatus = 0) JunePendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startJune AND DateEntered <= :endJune AND shippedStatus = 1) JuneShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startJune AND DateEntered <= :endJune), 0) JuneTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startJuly AND DateEntered <= :endJuly AND shippedStatus = 0) JulyPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startJuly AND DateEntered <= :endJuly AND shippedStatus = 1) JulyShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startJuly AND DateEntered <= :endJuly), 0) JulyTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startAugust AND DateEntered <= :endAugust AND shippedStatus = 0) AugustPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startAugust AND DateEntered <= :endAugust AND shippedStatus = 1) AugustShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startAugust AND DateEntered <= :endAugust), 0) AugustTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startSeptember AND DateEntered <= :endSeptember AND shippedStatus = 0) SeptemberPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startSeptember AND DateEntered <= :endSeptember AND shippedStatus = 1) SeptemberShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startSeptember AND DateEntered <= :endSeptember), 0) SeptemberTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startOctober AND DateEntered <= :endOctober AND shippedStatus = 0) OctoberPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startOctober AND DateEntered <= :endOctober AND shippedStatus = 1) OctoberShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startOctober AND DateEntered <= :endOctober), 0) OctoberTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startNovember AND DateEntered <= :endNovember AND shippedStatus = 0) NovemberPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startNovember AND DateEntered <= :endNovember AND shippedStatus = 1) NovemberShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startNovember AND DateEntered <= :endNovember), 0) NovemberTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startDecember AND DateEntered <= :endDecember AND shippedStatus = 0) DecemberPendingCount, 
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered >= :startDecember AND DateEntered <= :endDecember AND shippedStatus = 1) DecemberShippedCount,
													  COALESCE((SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered >= :startDecember AND DateEntered <= :endDecember), 0) DecemberTotalSales,
													  (SELECT COUNT(*) FROM ecommerceorders WHERE DateEntered LIKE :currentYear) TotalOrders,
													  (SELECT sum(grandTotal) FROM ecommerceorders WHERE DateEntered LIKE :currentYear) TotalSales
													  FROM ecommerceorders LIMIT 1');
		$yearSummary -> execute(array(":startJanuary" => $year . '-01-01',
									  ":endJanuary" => $year . '-01-31',
									  ":startFebruary" => $year . '-02-01',
									  ":endFebruary" => $year . '-02-28',
									  ":startMarch" => $year . '-03-01',
									  ":endMarch" => $year . '-03-31',
									  ":startApril" => $year . '-04-01',
									  ":endApril" => $year . '-04-30',
									  ":startMay" => $year . '-05-01',
									  ":endMay" => $year . '-05-31',
									  ":startJune" => $year . '-06-01',
									  ":endJune" => $year . '-06-30',
									  ":startJuly" => $year . '-07-01',
									  ":endJuly" => $year . '-07-31',
									  ":startAugust" => $year . '-08-01',
									  ":endAugust" => $year . '-08-31',
									  ":startSeptember" => $year . '-09-01',
									  ":endSeptember" => $year . '-09-30',
									  ":startOctober" => $year . '-10-01',
									  ":endOctober" => $year . '-10-31',
									  ":startNovember" => $year . '-11-01',
									  ":endNovember" => $year . '-11-30',
									  ":startDecember" => $year . '-12-01',
									  ":endDecember" => $year . '-12-31',
									  ":currentYear" => '%'. $year . '%'));
		return $yearSummary -> fetch();
	}

}