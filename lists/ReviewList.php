<?php

class ReviewList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllEcomerceProductReviews() {
		return $this -> db -> select('SELECT * FROM reviews WHERE reviewType = 1 ORDER BY DateEntered DESC');
	}
}