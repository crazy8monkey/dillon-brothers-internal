<?php

class EventPDFList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct(){
        parent::__construct();
    }
		
	public function AllYears() {
		return $this -> db -> select('SELECT * FROM eventpdflist ORDER BY eventYearList DESC');
	}

}