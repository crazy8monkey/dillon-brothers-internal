<?php

class BackupInventoryList extends BaseObjectList {
	
	public $Text;
	public $category;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllBackupList($pageVariable, $filteredResults = array()) {
		
		$page = $pageVariable;
		$this -> userLimit = "20";
		
		
		if($page) {
			//First Item to display on this page
			$this -> start = ($page - 1) * $this -> userLimit;
		} else {
			//if no page variable is given, set start to 0
			$this -> start = 0;
		}
		
		$searchQueryUrl = '';
		$countQuery = NULL;
		$HavingSearchQuery = NULL;
		$SqlWhereQuery = NULL;
		
		if(isset($filteredResults['Query'])) {
			$countQuery	.= ' SearchBikeFilter LIKE "%' . $filteredResults['Query'] .'%" AND';
			$HavingSearchQuery .='HAVING SearchBikeFilter LIKE "%' . $filteredResults['Query']. '%" ';
			$searchQueryUrl .= 'SearchQuery=' . $filteredResults['Query'] . '&';
		}
		
		if(isset($filteredResults['category'])) {
			$countQuery	.= ' inventoryBackupCategory = ' . $filteredResults['category'] .' AND';
			$SqlWhereQuery = ' inventorybackupsystem.inventoryBackupCategory = ' . $filteredResults['category'] . ' AND';
			$searchQueryUrl .= 'category=' . $filteredResults['category'] . '&';
		}
		
		if(isset($filteredResults['FilteredBy'])) {
			if($filteredResults['FilteredBy'] == 'EmptyCC') {
				$SqlWhereQuery = ' inventorybackupsystem.backupEngineSizeCC = 0 AND';	
				$countQuery	.= ' backupEngineSizeCC = 0 AND';
				$searchQueryUrl .= 'FilteredBy=EmptyCC&';
			}
			
			if($filteredResults['FilteredBy'] == 'SpecVerified') {
				$SqlWhereQuery = ' inventorybackupsystem.BackupVehicleVerified = 1 AND';	
				$countQuery	.= ' BackupVehicleVerified = 1 AND';
				$searchQueryUrl .= 'FilteredBy=SpecVerified&';
			}
			
			if($filteredResults['FilteredBy'] == 'SpecNotVerified') {
				$SqlWhereQuery = ' inventorybackupsystem.BackupVehicleVerified = 0 AND';	
				$countQuery	.= ' BackupVehicleVerified = 0 AND';
				$searchQueryUrl .= 'FilteredBy=SpecNotVerified&';
			}
			
			if($filteredResults['FilteredBy'] == 'EmptyFriendlyName') {
				$SqlWhereQuery = ' inventorybackupsystem.inventoryBackupModelFriendlyName = " " AND';	
				$countQuery	.= ' inventoryBackupModelFriendlyName = " " AND';
				$searchQueryUrl .= 'FilteredBy=EmptyFriendlyName';
			}
			
			
			
			
			
		}
		
		$sqlString = "";
		if($SqlWhereQuery != NULL) {
			$sqlString = ' WHERE' . rtrim($SqlWhereQuery, ' AND'); 
		}	
		
		//' . (isset($filteredResults['category']) ?  : '') . '
		//(isset($filteredResults['Query']) ? : '')
		//echo rtrim($countQuery, ' AND');
		
		//($countQuery != NULL ? ' WHERE' . rtrim($countQuery, ' AND') : '')
		$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM (SELECT *, (CASE WHEN inventorybackupsystem.inventoryBackupModelFriendlyName != ' ' THEN CONCAT(inventorybackupsystem.inventoryBackupYear, ' ', inventorybackupsystem.inventoryBackupManufactur, ' ', inventorybackupsystem.inventoryBackupModelName, ' ', inventorybackupsystem.inventoryBackupModelFriendlyName) ELSE
													        	CONCAT(inventorybackupsystem.inventoryBackupYear, ' ', inventorybackupsystem.inventoryBackupManufactur, ' ', inventorybackupsystem.inventoryBackupModelName)
													        END
													    
													    ) SearchBikeFilter FROM inventorybackupsystem) BackupVehicleList". ($countQuery != NULL ? ' WHERE' . rtrim($countQuery, ' AND') : '')) -> fetchColumn();	
		
		$backupList = $this -> db -> prepare('SELECT *, CONCAT(users.firstName, " ", users.lastName) LastEditedPerson, 
														GROUP_CONCAT(DISTINCT specLabelID) AllSpecLabels,
														( 
													        CASE WHEN inventorybackupsystem.inventoryBackupModelFriendlyName != " " THEN 
													        	CONCAT(inventorybackupsystem.inventoryBackupYear, " ", inventorybackupsystem.inventoryBackupManufactur, " ", inventorybackupsystem.inventoryBackupModelName, " ", inventorybackupsystem.inventoryBackupModelFriendlyName)
													        ELSE
													        	CONCAT(inventorybackupsystem.inventoryBackupYear, " ", inventorybackupsystem.inventoryBackupManufactur, " ", inventorybackupsystem.inventoryBackupModelName)
													        END
													    
													    ) SearchBikeFilter FROM inventorybackupsystem 
														LEFT JOIN (
															SELECT DISTINCT relatedItemID, MAX(edittedTimeAndDate) edittedTimeAndDate, MAX(edittedByUserID) edittedByUserID FROM edithistorylog WHERE editHistoryType = 4 GROUP BY relatedItemID
														) HistoryActivityTable ON inventorybackupsystem.inventoryBackupID = HistoryActivityTable.relatedItemID
														LEFT JOIN users ON HistoryActivityTable.edittedByUserID = users.userID
                                                        LEFT JOIN inventorycategories ON inventorybackupsystem.inventoryBackupCategory = inventorycategories.inventoryCategoryID LEFT JOIN speclabels ON FIND_IN_SET(inventorybackupsystem.inventoryBackupCategory, speclabels.LinkedInventoryCategories)'. $sqlString . ' GROUP BY inventorybackupsystem.inventoryBackupID ' . $HavingSearchQuery . 'ORDER BY inventoryBackupYear DESC LIMIT :start, :end');
		
				
																		
																		
		$backupList -> bindValue(':start', (int) $this -> start, PDO::PARAM_INT);
		$backupList -> bindValue(':end', (int) $this -> userLimit, PDO::PARAM_INT);
		
		
		$backupList -> execute();
		
		

		//echo $searchQueryUrl;
		
		$paginationCounterString = $page;
		//$paginationCounterString = $searchQueryUrl;
		
		$backupListArrray = array();
		$backupListArrray['backup-list'] = $backupList -> fetchAll();
		$backupListArrray['pagination'] = $this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "inventory/backup/", $paginationCounterString, rtrim($searchQueryUrl, '&'));
		
		return $backupListArrray;
		
	}

	public function CheckInventoryManufacture($text) {
		$sth = 	$this -> db -> prepare('SELECT * FROM inventorybackupsystem WHERE inventoryBackupManufactur = :Manufacturer');
		$sth -> execute(array(":Manufacturer" => $text));
		return $sth -> fetchAll();
	}

	public function BackupList($id = NULL) {
		if($id != NULL) {
			return $this -> db -> select("SELECT * FROM inventorybackupsystem WHERE inventoryBackupID NOT IN (" . $id . ") ORDER BY inventoryBackupYear DESC");	
		} else {
			return $this -> db -> select("SELECT * FROM inventorybackupsystem ORDER BY inventoryBackupYear DESC");
		}
		
	}
	
	public function BackuplinkedInventory() {
		return $this -> db -> select('SELECT * FROM inventory LEFT JOIN vinnumberbackup ON inventory.VinNumber = vinnumberbackup.backedupVinNumber LEFT JOIN inventorybackupsystem ON vinnumberbackup.inventoryBackupBikeID = inventorybackupsystem.inventoryBackupID');
	}
	
	public function NonMonkeyLearnBackedUpInventory() {
		return $this -> db -> select("SELECT * FROM inventorybackupsystem WHERE backupVehicleSentToMonkeyLearn = 0 AND BackupVehicleVerified = 1");
	}
	

}