<?php

class ColorsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllColors() {
		return $this -> db -> select('SELECT * FROM colors');
	}
	
	public function ByFilteredColor($text) {
		return $this -> db -> select('SELECT * FROM colors WHERE FilterColor = "' . $text . '"');
	}
	
	public function FilteredColors() {
		return $this -> db -> select('SELECT DISTINCT FilterColor, GROUP_CONCAT(ColorText) ColorTexts, GROUP_CONCAT(colorID) ColorIDS FROM colors GROUP BY FilterColor');
	}
	
	public function RelatedInventoryColors($inventoryID) {
		return $this -> db -> select('SELECT * FROM muirelatedcolors LEFT JOIN colors ON muirelatedcolors.relatedMUIColorID = colors.colorID WHERE muirelatedcolors.relatedMUInventoryID = ' . $inventoryID);
	}

}