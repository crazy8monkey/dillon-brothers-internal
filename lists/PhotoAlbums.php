<?php

class PhotoAlbums extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AlbumsByYear() {
		return $this -> db -> select('SELECT DISTINCT ParentDirectory From photoalbums');
	}
	
	public function AllAlbums() {
		return $this -> db -> select('SELECT *, (SELECT COUNT(*) FROM photos WHERE photoAlbumID = photoalbums.albumID) AlbumCount FROM photoalbums LEFT JOIN photos ON photoalbums.albumThumbNail = photos.photoID');
	}
	
	public function AlbumsByYearAndType($year, $type) {
		$albumsList = $this -> db -> prepare('SELECT *, (SELECT COUNT(*) FROM photos WHERE photoAlbumID = photoalbums.albumID) PhotoCount FROM photoalbums LEFT JOIN photos ON photoalbums.albumThumbNail = photos.photoID WHERE ParentDirectory = :ParentDirectory AND AlbumType = :AlbumType');
		$albumsList -> execute(array(":ParentDirectory" => $year, ":AlbumType" => $type));
		return $albumsList -> fetchAll();
	}

	public function PhotosByAlbums($id) {
		return $this -> db -> select('SELECT * FROM photos WHERE photoAlbumID = ' . $id);
	}
}