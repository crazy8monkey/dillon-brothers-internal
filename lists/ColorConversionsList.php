<?php

class ColorConversionsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function RulesByColor($id) {
		return $this -> db -> select('SELECT * FROM colorconversions WHERE relatedColorID = ' . $id);		
	}
	
	public function ConvertedColors() {
		return $this -> db -> select('SELECT * FROM colorconversions LEFT JOIN colors ON colorconversions.relatedColorID = colors.colorID');
	}
	

}