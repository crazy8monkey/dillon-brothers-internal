<?php

class EmployeeList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function Employees() {
		return $this -> db -> select('SELECT * FROM employees LEFT JOIN employeestorerelation On employees.EmployeeID = employeestorerelation.EmployeeID LEFT JOIN stores On employeestorerelation.RelatedStoreID = stores.storeID ORDER BY stores.StoreName DESC');
	}

}