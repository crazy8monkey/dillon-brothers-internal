<?php

class EmailLogList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function LogSummary($year) {
		$yearSummary = $this -> db -> prepare('SELECT (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJanuary AND EnteredTime <= :endJanuary AND LeadStoreID = 3) HarleyJanuaryCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJanuary AND EnteredTime <= :endJanuary AND LeadStoreID = 2) MotorSportJanuaryCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJanuary AND EnteredTime <= :endJanuary AND LeadStoreID = 4) IndianJanuaryCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJanuary AND EnteredTime <= :endJanuary AND TypeOfLead = 8) DonationRequestsJanuaryCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startFebruary AND EnteredTime <= :endFebruary AND LeadStoreID = 3) HarleyFebrauryCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startFebruary AND EnteredTime <= :endFebruary AND LeadStoreID = 2) MotorSportFebrauryCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startFebruary AND EnteredTime <= :endFebruary AND LeadStoreID = 4) IndianFebrauryCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startFebruary AND EnteredTime <= :endFebruary AND TypeOfLead = 8) DonationRequestsFebrauryCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startMarch AND EnteredTime <= :endMarch AND LeadStoreID = 3) HarleyMarchCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startMarch AND EnteredTime <= :endMarch AND LeadStoreID = 2) MotorSportMarchCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startMarch AND EnteredTime <= :endMarch AND LeadStoreID = 4) IndianMarchCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startMarch AND EnteredTime <= :endMarch AND TypeOfLead = 8) DonationRequestsCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startApril AND EnteredTime <= :endApril AND LeadStoreID = 3) HarleyAprilCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startApril AND EnteredTime <= :endApril AND LeadStoreID = 2) MotorSportAprilCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startApril AND EnteredTime <= :endApril AND LeadStoreID = 4) IndianAprilCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startApril AND EnteredTime <= :endApril AND TypeOfLead = 8) DonationRequestsAprilCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startMay AND EnteredTime <= :endMay AND LeadStoreID = 3) HarleyMayCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startMay AND EnteredTime <= :endMay AND LeadStoreID = 2) MotorSportMayCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startMay AND EnteredTime <= :endMay AND LeadStoreID = 4) IndianMayCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startMay AND EnteredTime <= :endMay AND TypeOfLead = 8) DonationRequestsMayCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJune AND EnteredTime <= :endJune AND LeadStoreID = 3) HarleyJuneCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJune AND EnteredTime <= :endJune AND LeadStoreID = 2) MotorSportJuneCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJune AND EnteredTime <= :endJune AND LeadStoreID = 4) IndianJuneCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJune AND EnteredTime <= :endJune AND TypeOfLead = 8) DonationRequestsJuneCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJuly AND EnteredTime <= :endJuly AND LeadStoreID = 3) HarleyJulyCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJuly AND EnteredTime <= :endJuly AND LeadStoreID = 2) MotorSportJulyCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJuly AND EnteredTime <= :endJuly AND LeadStoreID = 4) IndianJulyCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startJuly AND EnteredTime <= :endJuly AND TypeOfLead = 8) DonationRequestsJulyCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startAugust AND EnteredTime <= :endAugust AND LeadStoreID = 3) HarleyAugustCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startAugust AND EnteredTime <= :endAugust AND LeadStoreID = 2) MotorSportAugustCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startAugust AND EnteredTime <= :endAugust AND LeadStoreID = 4) IndianAugustCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startAugust AND EnteredTime <= :endAugust AND TypeOfLead = 8) DonationRequestsAugustCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startSeptember AND EnteredTime <= :endSeptember AND LeadStoreID = 3) HarleySeptCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startSeptember AND EnteredTime <= :endSeptember AND LeadStoreID = 2) MotorSportSeptCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startSeptember AND EnteredTime <= :endSeptember AND LeadStoreID = 4) IndianSeptCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startSeptember AND EnteredTime <= :endSeptember AND TypeOfLead = 8) DonationRequestsSeptCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startOctober AND EnteredTime <= :endOctober AND LeadStoreID = 3) HarleyOctoberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startOctober AND EnteredTime <= :endOctober AND LeadStoreID = 2) MotorSportOctoberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startOctober AND EnteredTime <= :endOctober AND LeadStoreID = 4) IndianOctoberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startOctober AND EnteredTime <= :endOctober AND TypeOfLead = 8) DonationRequestsOctoberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startNovember AND EnteredTime <= :endNovember AND LeadStoreID = 3) HarleyNovemberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startNovember AND EnteredTime <= :endNovember AND LeadStoreID = 2) MotorSportNovemberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startNovember AND EnteredTime <= :endNovember AND LeadStoreID = 4) IndianNovemberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startNovember AND EnteredTime <= :endNovember AND TypeOfLead = 8) DonationRequestsNovemberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startDecember AND EnteredTime <= :endDecember AND LeadStoreID = 3) HarleyDecemberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startDecember AND EnteredTime <= :endDecember AND LeadStoreID = 2) MotorSportDecemberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startDecember AND EnteredTime <= :endDecember AND LeadStoreID = 4) IndianDecemberCount,
													  (SELECT COUNT(*) FROM emaillogs WHERE EnteredTime >= :startDecember AND EnteredTime <= :endDecember AND TypeOfLead = 8) DonationRequestsDecemberCount
													  FROM emaillogs LIMIT 1');
													  
		$yearSummary -> execute(array(":startJanuary" => $year . '-01-01',
									  ":endJanuary" => $year . '-01-31',
									  ":startFebruary" => $year . '-02-01',
									  ":endFebruary" => $year . '-02-28',
									  ":startMarch" => $year . '-03-01',
									  ":endMarch" => $year . '-03-31',
									  ":startApril" => $year . '-04-01',
									  ":endApril" => $year . '-04-30',
									  ":startMay" => $year . '-05-01',
									  ":endMay" => $year . '-05-31',
									  ":startJune" => $year . '-06-01',
									  ":endJune" => $year . '-06-30',
									  ":startJuly" => $year . '-07-01',
									  ":endJuly" => $year . '-07-31',
									  ":startAugust" => $year . '-08-01',
									  ":endAugust" => $year . '-08-31',
									  ":startSeptember" => $year . '-09-01',
									  ":endSeptember" => $year . '-09-30',
									  ":startOctober" => $year . '-10-01',
									  ":endOctober" => $year . '-10-31',
									  ":startNovember" => $year . '-11-01',
									  ":endNovember" => $year . '-11-30',
									  ":startDecember" => $year . '-12-01',
									  ":endDecember" => $year . '-12-31'));
		return $yearSummary -> fetch();
				
	}

	public function SummaryInventoryLeads($year) {
		$inventoryLeadCount = array();
		$inventoryLeadCount['SendToFriend'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE TypeOfLead = 1 AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();
		$inventoryLeadCount['OnlineOffer'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE TypeOfLead = 2 AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();			
		$inventoryLeadCount['TradeInValue'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE TypeOfLead = 3 AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();	
		$inventoryLeadCount['ScheduleTestRide'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE TypeOfLead = 4 AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();
		$inventoryLeadCount['ContactUs'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE TypeOfLead = 5 AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();			
		return $inventoryLeadCount;
	}

	public function SummaryWebsiteSource($year) {
		$websiteSourceCount = array();
		$websiteSourceCount['DillonBrothersMain'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE SourceOfLead = 1 AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();
		return $websiteSourceCount;
	}

	public function SummaryTypeOfEmails($year) {
		$TypeOfEmails = array();
		$TypeOfEmails['InventoryLeads'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE TypeOfLead IN (1,2,3,4,5) AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();
		$TypeOfEmails['ServiceDept'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE TypeOfLead = 6 AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();
		$TypeOfEmails['PartsDept'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE TypeOfLead = 7 AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();
		$TypeOfEmails['DonationRequests'] = $this -> db -> query('SELECT COUNT(emailLogID) FROM emaillogs WHERE TypeOfLead = 8 AND EnteredTime >= "' . $year . '-01-01"  AND EnteredTime <= "' . $year . '-12-31"') -> fetchColumn();
		return $TypeOfEmails;
	}
	
	public function EmaiListAll() {
		return $this -> db -> select('SELECT * FROM emaillogs WHERE isActive = 1 ORDER BY EnteredTime DESC');
	}

}