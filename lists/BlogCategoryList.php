<?php

class BlogCategoryList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllBlogCategories() {
		return $this -> db -> select('SELECT * FROM blogpostcategories');
	}
	
	public function PostsByCategory($id) {
		return $this -> db -> select('SELECT * FROM blogcatories LEFT JOIN blogposts ON blogcatories.RelatedBlogPostID = blogposts.blogPostID WHERE blogcatories.BlogCategoryID = '. $id);
	}

}