<?php

class SettingSMSNumberList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function All() {
		return $this -> db -> select('SELECT * FROM settingsmsnumbers LEFT JOIN stores ON settingsmsnumbers.relatedStoreID = stores.storeID');
	}
	
	

}