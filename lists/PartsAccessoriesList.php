<?php

class PartsAcessoriesList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function PartsBySpecial($specialID) {
		return $this -> db -> select('SELECT * FROM partsaccessoryspecials WHERE MainSpecialID = ' . $specialID . ' ORDER BY ExpiredDate ASC');
	}
	

}