<?php

class SpecialsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function OEMSpecialsByBrand($id) {				
		return $this -> db -> select('SELECT * FROM specials 
										LEFT JOIN specialrelatedstores ON specials.SpecialID  = specialrelatedstores.CreatedSpecialID
										LEFT JOIN brands ON specials.OEMBrandID = brands.BrandID 
										LEFT JOIN stores on brands.Store = stores.storeID WHERE specials.OEMBrandID = ' . $id . ' ORDER BY specials.SpecialEndDate ASC');
	}
	
	public function ApparelSpecialsByBrand($id) {
		return $this -> db -> select('SELECT DISTINCT specialrelatedstores.CreatedSpecialID, GROUP_CONCAT(DISTINCT stores.StoreName ORDER BY stores.StoreID) AS RelatedStores, specials.SpecialTitle, specials.SpecialEndDate, specials.specialType FROM specialrelatedstores LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID WHERE specials.PartsApparelID = '. $id . ' AND specials.specialType = 2 GROUP BY SpecialID ORDER BY specials.SpecialEndDate ASC');
	}
	
	public function ApparelSpecialsNotBrandSpecific() {
		return $this -> db -> select('SELECT DISTINCT specialrelatedstores.CreatedSpecialID, GROUP_CONCAT(DISTINCT stores.StoreName ORDER BY stores.StoreID) AS RelatedStores, specials.SpecialTitle, specials.SpecialEndDate, specials.specialType FROM specialrelatedstores LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID WHERE specials.PartsApparelID = 0 AND specials.specialType = 2 GROUP BY SpecialID ORDER BY specials.SpecialEndDate ASC');
	}
	
	public function PartSpecialsByBrand($id) {
		return $this -> db -> select('SELECT DISTINCT specialrelatedstores.CreatedSpecialID, GROUP_CONCAT(DISTINCT stores.StoreName ORDER BY stores.StoreID) AS RelatedStores, specials.SpecialTitle, specials.SpecialEndDate, specials.specialType FROM specialrelatedstores LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID WHERE specials.PartsApparelID = '. $id . ' AND specials.specialType = 3 GROUP BY SpecialID ORDER BY specials.SpecialEndDate ASC');
	}

	public function PartSpecialsNotBrandSpecific() {
		return $this -> db -> select('SELECT DISTINCT specialrelatedstores.CreatedSpecialID, GROUP_CONCAT(DISTINCT stores.StoreName ORDER BY stores.StoreID) AS RelatedStores, specials.SpecialTitle, specials.SpecialEndDate, specials.specialType FROM specialrelatedstores LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID WHERE specials.PartsApparelID = 0 AND specials.specialType = 3 GROUP BY SpecialID ORDER BY specials.SpecialEndDate ASC');
	}
	
	public function ServiceSpecialsBystore() {
		return $this -> db -> select('SELECT DISTINCT specialrelatedstores.CreatedSpecialID, 
											GROUP_CONCAT(DISTINCT stores.Name ORDER BY stores.StoreID) AS RelatedStores, 
											specials.SpecialTitle, 
											specials.IsServiceSpecial, 
											specials.SpecialEndDate FROM specialrelatedstores 
											LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID 
											LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID WHERE specials.IsServiceSpecial = 1 GROUP BY SpecialID ORDER BY specials.SpecialEndDate ASC');
	}

	public function ServiceSpecials() {
		return $this -> db -> select('SELECT DISTINCT specialrelatedstores.CreatedSpecialID, GROUP_CONCAT(DISTINCT stores.StoreName ORDER BY stores.StoreID) AS RelatedStores, specials.SpecialTitle, specials.SpecialEndDate FROM specialrelatedstores LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID WHERE specials.specialType = 4 GROUP BY SpecialID ORDER BY specials.SpecialEndDate ASC');
	}

	public function StoreSpecials() {
		return $this -> db -> select('SELECT DISTINCT specialrelatedstores.CreatedSpecialID, GROUP_CONCAT(DISTINCT stores.StoreName ORDER BY stores.StoreID) AS RelatedStores, specials.SpecialTitle, specials.SpecialEndDate FROM specialrelatedstores LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID WHERE specials.specialType = 5 GROUP BY SpecialID ORDER BY specials.SpecialEndDate ASC');
	}
	
	public function SpecialsByPartsApparelBrand($id) {
		return $this -> db -> select('SELECT DISTINCT specialrelatedstores.CreatedSpecialID, GROUP_CONCAT(DISTINCT stores.StoreName ORDER BY stores.StoreID) AS RelatedStores, specials.SpecialTitle, specials.SpecialEndDate, specials.specialType FROM specialrelatedstores LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID WHERE specials.PartsApparelID = '. $id . ' GROUP BY SpecialID ORDER BY specials.SpecialEndDate ASC');
	}

	public function SpecialsByPartApparelBrand($id) {
		return $this -> db -> select('SELECT * FROM specials WHERE PartsApparelID = ' . $id);
	}

	

}