<?php

class ServiceIncentivesList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function IncentivesBySpecial($specialID) {
		return $this -> db -> select('SELECT * FROM serviceincentives WHERE MainSpecialID = ' . $specialID . ' ORDER BY serviceIncentiveExpiredDate ASC');
	}
	

}