<?php

class PartsApparelBrandList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function PartBrands() {
		return $this -> db -> select('SELECT * FROM partaccessorybrand WHERE BrandType IN (1,3)');
	}
	
	public function ApparelBrands() {
		return $this -> db -> select('SELECT * FROM partaccessorybrand WHERE BrandType IN (2,3)');
	}
	
	
	public function Brands() {
		return $this -> db -> select('SELECT * FROM partsbrands');
	}
	

}