<?php

class BlogPostCommentsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	
	public function UnapproveComments() {
		return $this -> db -> select('SELECT * FROM blogcomments LEFT JOIN blogposts ON blogcomments.postID = blogposts.blogPostID WHERE commentVisible = 0 ORDER BY blogcomments.commentEnteredDate DESC');
	}
	
	public function CommentsByBlogPost() {
		//virtual table
		//SELECT postID, GROUP_CONCAT(commentFullName) CommentFullNames, GROUP_CONCAT(commentNotesSection) CommenNotes, GROUP_CONCAT(commentEnteredDate) CommentEnteredDates, GROUP_CONCAT(commentEnteredTime) CommentEnteredTimes, GROUP_CONCAT(blogCommentID) CommentIDs, GROUP_CONCAT(commentVisible) CommentVisibles FROM blogcomments GROUP BY postID
		
		//
		return $this -> db -> select('SELECT * FROM blogposts LEFT JOIN (SELECT postID, GROUP_CONCAT(commentFullName) CommentFullNames, 
																					    GROUP_CONCAT(commentNotesSection) CommenNotes, 
																					    GROUP_CONCAT(commentEnteredDate) CommentEnteredDates, 
																					    GROUP_CONCAT(commentEnteredTime) CommentEnteredTimes, 
																					    GROUP_CONCAT(blogCommentID) CommentIDs, 
																					    GROUP_CONCAT(commentVisible) CommentVisibles FROM blogcomments GROUP BY postID) 
																					    BlogPostsCommentsContent ON blogposts.blogPostID = BlogPostsCommentsContent.postID');
		
	}

}