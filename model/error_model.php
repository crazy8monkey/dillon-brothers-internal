<?php

class Error_Model Extends Model {
	
	public function _construct() {
		parent::_contruct();
	}
	
	public function errorLogInsert($errorLog) {
		
		
		
			if ($this -> validate -> emptyInput($errorLog['error-name'])) {
				$this -> json -> outputJqueryJSONObject('firstName', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("First Name")));
			} else if($this -> validate -> notUsingLetters($errorLog['error-name'])) {
				$this -> json -> outputJqueryJSONObject('firstNameOnlyLetters', $this -> msg->flashMessage('message error', $this -> msg -> onlyLetters()));
			} else if ($this -> validate -> emptyInput($errorLog['error-email'])) {
				$this -> json -> outputJqueryJSONObject('emptyEmail', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Email")));
			}  else if($this -> validate -> correctEmailFormat($errorLog['error-email'])) {
				$this -> json -> outputJqueryJSONObject('wrongEmailFormat', $this -> msg->flashMessage('message error', $this -> msg -> emailFormatRequiredMessage()));
			} else if ($this -> validate -> emptyInput($errorLog['error-description'])) {
				$this -> json -> outputJqueryJSONObject('errorDescription', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Error description")));
			} else {
				
				$this -> json -> multipleJSONOjbects(array("finishedForm" => $this -> msg -> flashMessage('message success', "Thank you for filling out the form, We will look into it into the issue"), 
													   "redirect" => PATH));
				
				$msg = "<tr><td style='vertical-align:top; font-weight:bold'>Name:</td><td>". $errorLog['error-name'] ."</td></tr>" .
					   "<tr><td style='vertical-align:top; font-weight:bold'>Email:</td><td>". $errorLog['error-email'] ."</td></tr>" .
					   "<tr><td style='vertical-align:top; font-weight:bold'>Description:</td><td>". $errorLog['error-description'] ."</td></tr>";
					   
				$errorLogMessage =  
			   "\nName: " . $errorLog['error-name'] . 
			   "\nEmail: " . $errorLog['error-email'] . 
			   "\nDescription: " . $errorLog['error-description'] . "\n\n";
				
					   
							   
				Email::sendErrorEmail($msg);
				
				$TrackError = new EmailServerError();
				$TrackError -> message = "404 error message" . $errorLogMessage;
				$TrackError -> type = "404 ERROR MESSAGE";
				$TrackError -> SendMessage();
							
				
			}
			
	}


}
